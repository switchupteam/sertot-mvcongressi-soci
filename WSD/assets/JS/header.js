﻿$(document).ready(function () {
    var headerHeight = $('header').height();
    //var headerStickyHeight = $('header').addClass('sticky').height();
    //$('header').removeClass('sticky').height();

    //if ($('header').length) {
    //    $('main').css('margin-top', headerHeight);
    //}

    if($(this).scrollTop() > 251) {
        $('header').addClass('sticky');
        $('main').css('margin-top', headerHeight);

    }
        else {
        $('header').removeClass('sticky');
        $('main').css('margin-top', 0);

    }

    $(window).scroll(function () {
        if ($(this).scrollTop() > 251) {
            $('header').addClass('sticky');
            $('main').css('margin-top', headerHeight);
            $('.back-to-top').addClass('visible')
        }
        else {
            $('header').removeClass('sticky');
            $('main').css('margin-top', 0);
            $('.back-to-top').removeClass('visible')
        }
    });

    // Multi level bootstrap menu
    $('.dropdown-menu a.dropdown-toggle').on('click', function (e) {
        if (!$(this).next().hasClass('show')) {
            $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
        }
        var $subMenu = $(this).next(".dropdown-menu");
        $subMenu.toggleClass('show');

        $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
            $('.dropdown-submenu .show').removeClass("show");
        });

        return false;
    });

});
