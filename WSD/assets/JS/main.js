
$(document).ready(function () {
    $('header .menu').on('click', openMenu)

    $('header .top-bar li a').on('click', fontSize)
    
    $('#navigation .content .menu').on('click', openSubmenu)
    $('#navigation .content .menu .menu_2').on('click', openSubmenu2)

    $(window).on('resize', handleResize)
    })


function handleResize(){
    // .......................................... calcolo solo a menu aperto
    if (menu_open == true){
        var menu_width;
        var body_width;
        var doc_width = $(window).width();
        // .......................................... pagina di 480 o meno, rapporto 5:1
        if (doc_width <= 480){
            body_width = (doc_width/6)*1;   menu_width = (doc_width/6)*5;
            // .......................................... pagina di 992 o meno, rapporto 2:2
            } else if (doc_width < 992){
            body_width = (doc_width/4)*2;   menu_width = (doc_width/4)*2;
            // .......................................... pagina oltre i 992, rapporto 3:1
            } else {
            body_width = (doc_width/4)*3;   menu_width = (doc_width/4)*1;
            }
        $('header').css('margin-left',menu_width+'px');
        $('header').css('width',body_width+'px');
        $('main').css('margin-left',menu_width+'px');
        $('main').css('width',body_width+'px');
        $('footer').css('margin-left',menu_width+'px');
        $('footer').css('width',body_width+'px');
        $('#navigation').css('width',menu_width+'px');
        }
    }


/*
    <!-- ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: navigation -->
    <div id="navigation">
        <div class="content">
            <ul class="menus">
                <li class="menu">
                    <a href="#" class="title"> LIVELLO 1 </a>
                    <ul class="menu2nd">
                        <li><a href="#"> VOCE MENU </a></li>
                        <li class="menu_2">
                            <a href="#" class="title"> LIVELLO 2</a>
                            <ul class="menu3rd">
                                <li><a href="#"> VOCE MENU </a></li>



    #navigation
        .content
            ul.menus
                li.menu --------------------------> click
                    a.title
                    ul.menu2nd
                        li
                            a
                        li.menu_2 --------------------------> click
                            a.title
                            ul.menu3rd
                                li
                                    a



*/


function fontSize(e){
    var pressed = $(this).attr('class');
    $('html').attr('class','')
    $('html').attr('class', 'fs_'+pressed)



}

var menu_open = false;
function openMenu(e){
    e.stopImmediatePropagation();
    e.preventDefault();
    // .......................................... vars
    var menu_width;
    var body_width;
    var doc_width = $(window).width();
    // .......................................... pagina di 480 o meno, rapporto 5:1
    if (doc_width <= 480){
        body_width = (doc_width/6)*1;   menu_width = (doc_width/6)*5;
        // .......................................... pagina di 992 o meno, rapporto 2:2
        } else if (doc_width < 992){
        body_width = (doc_width/4)*2;   menu_width = (doc_width/4)*2;
        // .......................................... pagina oltre i 992, rapporto 3:1
        } else {
        body_width = (doc_width/4)*3;   menu_width = (doc_width/4)*1;
        }



    // .......................................... set sizes
    if (menu_open == false){
        $('header').stop().animate({'margin-left': menu_width , 'width': body_width+'px'}, 400, function(){ });
        $('header .top-bar').stop().delay(10).animate({ 'opacity': '.4' }, 400, function () { });
        $('header .logo-bar').stop().delay(10).animate({ 'opacity': '.4' }, 400, function () { });
        $('header .menu-bar').stop().delay(10).animate({ 'opacity': '.4' }, 400, function () { });
        $('main').stop().animate({ 'opacity': '.4', 'margin-left': menu_width,  'width': body_width+'px'}, 400, function(){ });
        $('footer').stop().animate({ 'opacity': '.4', 'margin-left': menu_width,  'width': body_width+'px'}, 400, function(){ });
        $('#navigation').stop().delay(10).animate({'width': menu_width+'px'}, 400, function(){ });
        $('header').on('click', openMenu);
        $('main').on('click', openMenu);
        $('footer').on('click', openMenu);


        } else {
        $('header').stop().delay(10).animate({'margin-left': 0, 'width': '100%'}, 400, function(){ });
        $('header .top-bar').stop().delay(10).animate({ 'opacity': '1'}, 400, function(){ });
        $('header .logo-bar').stop().delay(10).animate({ 'opacity': '1'}, 400, function(){ });
        $('header .menu-bar').stop().delay(10).animate({ 'opacity': '1'}, 400, function(){ });
        $('main').stop().delay(10).animate({ 'opacity': '1', 'margin-left': 0,  'width': '100%'}, 400, function(){ });
        $('footer').stop().delay(10).animate({ 'opacity': '1', 'margin-left': 0,  'width': '100%'}, 400, function(){ });
        $('#navigation').stop().animate({'width': '0px'}, 400, function(){ });

        $('header').off('click', openMenu);
        $('main').off('click', openMenu);
        $('footer').off('click', openMenu);

        $('#navigation .content .menu').find('.menu2nd').stop().animate({'height': '0px'}, 400, function(){ });
        $('#navigation .content .menu').removeClass('opened')
        }
    menu_open = !menu_open;

}
