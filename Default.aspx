﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>




<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="robots" content="index,follow" />
    <meta name="rating" content="General" />
    <title><%= AresConfig.NomeSito %> | <%= title %></title>

    <link rel="icon" href="favicon.ico" type="image/x-icon" />
   <%-- <link rel="apple-touch-icon" sizes="57x57" href="/asset/img/apple-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="60x60" href="/asset/img/apple-icon-60x60.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="/asset/img/apple-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="/asset/img/apple-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="/asset/img/apple-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="/asset/img/apple-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="/asset/img/apple-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="/asset/img/apple-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="/asset/img/apple-icon-180x180.png" />
    <link rel="icon" type="image/png" sizes="192x192" href="/asset/img/android-icon-192x192.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="/asset/img/favicon-16x16.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="/asset/img/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="96x96" href="/asset/img/favicon-96x96.png" />--%>
    <meta name="msapplication-TileColor" content="#ffffff" />
    <%--<meta name="msapplication-TileImage" content="/asset/img/ms-icon-144x144.png" />--%>
    <meta name="theme-color" content="#ffffff" />

    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&family=Oswald:wght@700&display=swap" rel="stylesheet" />    

    <link href="/asset/lib/bootstrap/css/bootstrap.custom.css" rel="stylesheet" type="text/css" />
    <link href="/asset/css/reset.css" rel="stylesheet" type="text/css" />
    <link href="/asset/css/main.css" rel="stylesheet" type="text/css" />
    <link href="/asset/css/ZeusTypeFoundry.css" rel="stylesheet" type="text/css" />

    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script defer src="/asset/lib/font-awesome/js/fontawesome-all.min.js"></script>

    <link href="/asset/lib/slick/slick-theme.css" rel="stylesheet" />
    <link href="/asset/lib/slick/slick.css" rel="stylesheet" />

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-159327270-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() { dataLayer.push(arguments); }
        gtag('js', new Date());

        gtag('config', 'UA-159327270-1');
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <header>
            <dlc:Header ID="Header" runat="server" />
        </header>
        <main class="home">
            <asp:PlaceHolder runat="server" ID="SliderPlaceHolder">
                <div class="slider">
                    <asp:Repeater runat="server" ID="Slider_Repeater" DataSourceID="SliderDataSource">
                        <ItemTemplate>
                            <%--<div class="boxSlide">
                                <div class="immagine">
                                    <img src="/ZeusInc/DigiSign/Img1/<%# Eval("Immagine1") %>" alt="<%# Eval("Immagine12Alt") %>" />
                                    <div class="foregroundOpaco"></div>
                                </div>

                                <div class="boxTesto">
                                    <div class="contentContainer">
                                        <div class="container">
                                            <div class="innerBox">
                                                <h6 class="data"><%# Eval("Contenuto1") %></h6>
                                                <h4 class="testo"><%# Eval("Contenuto2") %></h4>
                                                <h2 class="titolo"><%# Eval("Identificativo") %></h2>
                                                <div class="link borderless coloriInvertiti bold">
                                                    <a href="<%# Eval("Link") %>">Apri Dettagli</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container arrowContainer">
                                        <a class="sliderArrow leftArrow">
                                            <i class="fas fa-chevron-left"></i>
                                        </a>
                                        <a class="sliderArrow rightArrow">
                                            <i class="fas fa-chevron-right"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>--%>
                        </ItemTemplate>
                    </asp:Repeater>
                    <asp:SqlDataSource runat="server" ID="SliderDataSource" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>" OnSelected="SliderDataSource_Selected" />
                </div>
            </asp:PlaceHolder>

            <asp:PlaceHolder runat="server" ID="BoxBannerPlaceHolder">
                <div class="boxBanner">
                    <div class="container">
                        <div class="row">
                            <asp:Repeater runat="server" ID="BoxBanner_Repeater" DataSourceID="BoxBannerDataSource">
                                <ItemTemplate>
                                    <div class="col-xs-12 col-md-6">
                                        <a class="box" href="<%# Eval("Link") %>">
                                            <div class="immagine">
                                                <%--<img src="/ZeusInc/DigiSign/Img1/<%# Eval("Immagine1") %>" alt="<%# Eval("Immagine12Alt") %>" />--%>
                                                <img src="/asset/img/default-boxbanner.jpg" />
                                            </div>

                                            <div class="wrapperTesto">
                                                <p class="testo"><%# Eval("Contenuto2") %></p>
                                                <h6 class="titolo"><%# Eval("Identificativo") %></h6>
                                            </div>
                                        </a>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                            <asp:SqlDataSource runat="server" ID="BoxBannerDataSource" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>" OnSelected="BoxBannerDataSource_Selected" />
                        </div>
                    </div>
                </div>
            </asp:PlaceHolder>

            <div class="chiSiamo">
                <div class="container">
                    <div class="titolo">
                        <h4>Chi siamo</h4>
                        <hr class="bianco" />
                    </div>
                    <div class="testo">
                        <p>
                            <%= contenutoHomeLibero %>
                        </p>
                    </div>
                </div>
            </div>

            <div class="ultimeNews" id="ultime-news">
                <div class="container">
                    <div class="container">
                        <div class="titolo">
                            <h4>Ultime news</h4>
                            <hr />
                        </div>
                        <div class="elencoNews">
                            <asp:Repeater runat="server" ID="UltimeNews_Repeater" DataSourceID="SqlDataSource_UltimeNews">
                                <HeaderTemplate>
                                    <div class="row">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="col-md-4">
                                        <div class="news">
                                            <div class="boxTitolo">
                                                <h6 class="data"><%# ((DateTime)Eval("PWI_Area1")).GetDataItaliana("dd MMM yyyy") %></h6>
                                                <a class="linkNews" href="<%# Eval("Url") %>">
                                                    <h5 class="titolo"><%# Eval("Titolo") %></h5>
                                                </a>
                                                <hr class="left-position small-margin" />
                                            </div>
                                            <p class="testo"><%# Eval("DescBreve1") %></p>
                                        </div>
                                    </div>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </div>
                                </FooterTemplate>
                            </asp:Repeater>
                            <asp:SqlDataSource runat="server" ID="SqlDataSource_UltimeNews" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="sponsor" style="display: none">
                <div class="container">
                    <div class="sponsorBox">
                        <div class="titolo">
                            <h4>Partner</h4>
                            <hr />
                        </div>
                        <div class="elencoLoghi">
                            <asp:Repeater runat="server" ID="SocietaScentifichePartner_Repeater" DataSourceID="SqlDataSource_SocietaScentifichePartner">
                                <HeaderTemplate>
                                    <div class="row">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="col-sm-6 col-lg-3">
                                        <a href="<%# Eval("Link1") %>">
                                            <img src="/ZeusInc/Brands/Img1/<%# Eval("Immagine1") %>" alt="<%# Eval("Immagine1Alt") %>" />
                                        </a>
                                    </div>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </div>
                                </FooterTemplate>
                            </asp:Repeater>
                            <asp:SqlDataSource runat="server" ID="SqlDataSource_SocietaScentifichePartner" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>" />
                        </div>
                    </div>
                    <div class="sponsorBox">
                        <div class="titolo">
                            <h4>Sponsor</h4>
                            <hr />
                        </div>
                        <div class="elencoLoghiFlex">
                            <asp:Repeater runat="server" ID="Sponsor_Repeater" DataSourceID="SqlDataSource_Sponsor">
                                <ItemTemplate>
                                    <h2><%# Container.ItemIndex + 1 %></h2>
                                    <a href="<%# Eval("Link1") %>" class="item-sponsor">
                                        <img src="/ZeusInc/Brands/Img1/<%# Eval("Immagine1") %>" alt="<%# Eval("Immagine1Alt") %>" />
                                    </a>
                                </ItemTemplate>
                            </asp:Repeater>
                            <asp:SqlDataSource runat="server" ID="SqlDataSource_Sponsor" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>" />
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <footer>
            <dlc:Footer ID="Footer" runat="server" />
        </footer>
    </form>

    <%--SCRIPTS--%>
    <script src="/asset/lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="asset/lib/slick/slick.min.js"></script>

    <%--CUSTOM--%>
    <script src="/asset/js/main.js"></script>
    <script src="/asset/js/slider.js"></script>

    <%--COOKIE POLICY--%>
    <script type="text/javascript" src="/asset/js/cookiechoices.js"></script>
    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function (event) {
            cookieChoices.showCookieConsentBar('I cookie ci aiutano ad erogare servizi di qualità. Utilizzando i nostri servizi, l\'utente accetta le nostre modalità d\'uso dei cookie.',
                'Chiudi', 'Cookie policy', '/Web/59/Cookie-policy');

            manageSponsor();

        });


        function manageSponsor() {
            //Prendere la lista degli sponsor
            const listOfSponsors = document.getElementsByClassName("item-sponsor");

            //se > 6
            if (listOfSponsors.length > 6) {
                //prendere il resto e metterlo a capo
                let resto = listOfSponsors.length % 6;

                for (let i = 1; i <= resto; i++) {
                    listOfSponsors[i + 6].classList.add("br")
                }

            }
        }

    </script>
</body>
</html>
