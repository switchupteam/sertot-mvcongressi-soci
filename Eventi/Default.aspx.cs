﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.HtmlControls;

public partial class Corsi_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        /*****************************************************/
        /**************** INIZIO SEO *************************/
        /*****************************************************/
        HtmlMeta description = new HtmlMeta();
        HtmlMeta keywords = new HtmlMeta();
        description.Name = "description";
        keywords.Name = "keywords";
        Page.Title = "Corsi e congressi | " + AresConfig.NomeSito;
        description.Content = "";
        keywords.Content = "";
        HtmlHead head = Page.Header;
        head.Controls.Add(description);
        head.Controls.Add(keywords);
        /*****************************************************/
        /**************** FINE SEO ***************************/
        /*****************************************************/

        var XRI = 0;
        int.TryParse(Request.QueryString["XRI"], out XRI);
        if (XRI == 0)
            XRI = 41;
        SetCatTitle(XRI);
        BindPubblicazioni(XRI);
    }

    string strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
    private void BindPubblicazioni(int XRI)
    {
        //Load Books
        var books = new List<book>();

        string sqlQuery = string.Empty;
        //var provincie = new List<ProvinciaItem>();
        using (SqlConnection connection = new SqlConnection(strConnessione))
        {
            connection.Open();
            SqlCommand command = connection.CreateCommand();

            sqlQuery = @"SELECT ID1Publisher, Titolo, ZeusIdModulo, Immagine1, DescBreve1, Contenuto1, ImgBeta_Path, ID2Categoria1, Sottotitolo AS Link, UrlRewrite
                          FROM vwPublisher_Public_DtlArea1
                            INNER JOIN tbPZ_ImageRaider on ZeusIdModuloIndice = ZeusIdModulo + '1'
                          WHERE ZeusIdModulo = @ZeusIdModulo AND ID2Categoria1 = @ID2Catagoria1
                          ORDER BY PWI_Area1 DESC";

            command.CommandText = sqlQuery;
            command.Parameters.Add("@ZeusIdModulo", System.Data.SqlDbType.VarChar);
            command.Parameters["@ZeusIdModulo"].Value = "PBCOR";
            command.Parameters.Add("@ID2Catagoria1", System.Data.SqlDbType.Int);
            command.Parameters["@ID2Catagoria1"].Value = XRI;

            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                var book = new book()
                {
                    Image = !string.IsNullOrEmpty(reader["Immagine1"].ToString()) ? string.Concat(reader["ImgBeta_Path"].ToString().Replace("~", ""), reader["Immagine1"].ToString()) : "",
                    Title = reader["Titolo"].ToString(),
                    Authors = reader["Contenuto1"].ToString(),
                    Desc = reader["DescBreve1"].ToString(),
                    Link = string.Format("/{0}/{1}/{2}", "Corso", reader["ID1Publisher"].ToString(), reader["UrlRewrite"].ToString())
                };
                books.Add(book);
            }

            if (books.Count == 0)
            {
                NessunElemento.Visible = true;
                rptBooks.Visible = false;
            }
            else
            {
                rptBooks.DataSource = books;
                rptBooks.DataBind();
            }

            reader.Close();
            connection.Close();
        }
    }

    private void SetCatTitle(int XRI)
    {
        string sqlQuery = string.Empty;
        using (SqlConnection connection = new SqlConnection(strConnessione))
        {
            connection.Open();
            SqlCommand command = connection.CreateCommand();

            sqlQuery = @"SELECT [Categoria], [Descrizione1] FROM tbCategorie 
                            WHERE ID1Categoria = @ID1Categoria ";

            command.CommandText = sqlQuery;
            command.Parameters.Add("@ID1Categoria", System.Data.SqlDbType.Int);
            command.Parameters["@ID1Categoria"].Value = XRI;

            SqlDataReader reader = command.ExecuteReader();

            if (reader.Read())
            {
                litCatName.Text = reader["Categoria"].ToString();
                litDescription.Text = reader["Descrizione1"].ToString();
            };
            reader.Close();
            connection.Close();
        }

    }

    public class book
    {
        public string Image { get; set; }
        public string Title { get; set; }
        public string Authors { get; set; }
        public string Desc { get; set; }
        public string Link { get; set; }
    }
}