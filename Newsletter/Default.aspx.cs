﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Xml;
using System.Data.SqlClient;

public partial class Newsletter_Default : System.Web.UI.Page
{
    private string configXmlPath = HttpContext.Current.Server.MapPath("/App_Data/config.xml");
    private string languageXmlPath = HttpContext.Current.Server.MapPath("/Newsletter/ZML_Newsletter.xml");
    private string newsletterDestinatario = string.Empty;
    private string newsletterOggetto = string.Empty;
    private string myLang = string.Empty;
    private string emailBody = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        AresUtilities myAresUtilities = new AresUtilities();
        myLang = myAresUtilities.GetLangRewritted();
        TitoloPaginaLabel.Text = "Newsletter";

        Page.Title = "Newsletter | " + AresConfig.NomeSito ;
        NewsletterContent.Lang = myLang;
        NewsletterContent.ComponentName = "PageContent";

        //get newsletter data from config.xml
        XmlDocument config = new XmlDocument();
        config.Load(configXmlPath);
        XmlNode node = config.DocumentElement.SelectSingleNode("config/NewsletterDestinatario");
        newsletterDestinatario = node.InnerText;
        node = config.DocumentElement.SelectSingleNode("config/NewsletterOggetto");
        newsletterOggetto = node.InnerText;

        //get fields translation
        string languagePath = "ZML_Newsletter/Lang[@id='" + myLang + "']";
        readLanguageXml(languageXmlPath, languagePath);

        //se focus on SubscribeLinkButton
        SetFocus(SubscribeLinkButton.UniqueID);

        //get email body
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;
        using (SqlConnection connection = new SqlConnection(strConnessione))
        {
            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = @"SELECT Contenuto1 FROM vwPageDesigner_Public_Dtl WHERE ZeusIDModulo='PDNWS' AND ZeusAssetType = 'MailContent' AND ZeusLangCode = @Lang";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@Lang", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Lang"].Value = myLang;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    emailBody = reader["Contenuto1"].ToString();
                }

                reader.Close();
                connection.Close();
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
            }
        }//fine Using
    }

    private void SetFocus(string ID)
    {
        Page.Form.DefaultButton = ID;
        Page.Form.DefaultFocus = ID;
    }

    // read the language xml file, looping every child
    // and filling the corresponding label with child inner text
    private bool readLanguageXml(string FileName, string XPath)
    {
        try
        {
            delinea myDelinea = new delinea();
            XmlDocument mydoc = new XmlDocument();
            mydoc.Load(FileName);
            XmlNodeList nodelist = mydoc.SelectNodes(XPath);
            for (int i = 0; i < nodelist.Count; i++)
            {
                foreach (XmlNode parentNode in nodelist)
                {
                    foreach (XmlNode childNode in parentNode)
                    {
                        switch (childNode.Name)
                        {
                            case "ZML_Newsletter_Nome":
                                NomeLabel.Text = childNode.InnerText.ToString();
                                break;

                            case "ZML_Newsletter_Cognome":
                                CognomeLabel.Text = childNode.InnerText.ToString();
                                break;

                            case "ZML_Newsletter_EMail":
                                EmailLabel.Text = childNode.InnerText.ToString();
                                break;

                            case "ZML_Newsletter_Invia":
                                SubscribeLinkButton.Text = childNode.InnerText.ToString();
                                break;

                            case "ZML_Newsletter_Obbligatorio":
                                NomeRequiredFieldValidator.ErrorMessage = childNode.InnerText.ToString();
                                CognomeRequiredFieldValidator.ErrorMessage = childNode.InnerText.ToString();
                                MailRequiredFieldValidator.ErrorMessage = childNode.InnerText.ToString();
                                CondizioniRequiredFieldValidator.ErrorMessage = childNode.InnerText.ToString();
                                break;

                            case "ZML_Newsletter_AccettoCondizioni":
                                CondizioniLabel.Text = childNode.InnerText.ToString();
                                break;

                            case "ZML_Newsletter_MailSent":
                                MailSentLabel.Text = childNode.InnerText.ToString();
                                break;

                        }//fine switch

                    }//fine foreach

                }//fine foreach

                break;

            }//fine for

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }

    }// fine ReadXml


    protected void SubscribeLinkButton_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string body = string.Empty;
            string bodyDetail = string.Empty;

            body = "Dear  " + NomeTextBox.Text + " " + CognomeTextBox.Text + ", ";
            body += "<br/>";
            body += "<br/>";
            body += emailBody;

            bodyDetail = body;
            bodyDetail += "<br/>";
            bodyDetail += "<br/>";
            bodyDetail += "SUBSCRIPTION DETAILS:";
            bodyDetail += "<br/>";
            bodyDetail += "First name:" + NomeTextBox.Text;
            bodyDetail += "<br/>";
            bodyDetail += "Last name:" + CognomeTextBox.Text;
            bodyDetail += "<br/>";
            bodyDetail += "Email address:" + EmailTextBox.Text;

            if (SendMail(newsletterDestinatario, EmailTextBox.Text, body, newsletterOggetto) && SendMail(EmailTextBox.Text, newsletterDestinatario, bodyDetail, newsletterOggetto))
            {
                ContentPanel.Visible = false;
                MailSentPanel.Visible = true;
            }
            else
            {
                if (!InvalidEmailLabel.Visible)
                {
                    this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "scrMail01", "self.parent.location='/System/Message.aspx?Lang=" + myLang + "';", true);
                }
            }
        }//fine if
    }

    protected bool SendMail(string from, string to, string body, string subject)
    {
        try
        {
            Zeus.Mail myMail = new Zeus.Mail();

            try
            {
                myMail.MailToSend.To.Add(new MailAddress(to.Trim()));
            }
            catch (FormatException)
            {
                //address is invalid
                InvalidEmailLabel.Visible = true;
                return false;
            }

            myMail.MailToSend.From = new MailAddress(from);

            myMail.MailToSend.IsBodyHtml = true;
            myMail.MailToSend.Subject = subject;
            myMail.MailToSend.Body = body;

            myMail.SendMail(Zeus.Mail.MailSendMode.Async);

            return true;
        }
        catch (Exception e)
        {
            return false;
        }

    }
}