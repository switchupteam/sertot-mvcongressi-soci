﻿<%@ Page Language="C#" MasterPageFile="~/Master/LayoutMain.master" CodeFile="Default.aspx.cs" Inherits="Newsletter_Default" Async="true" %>

<%@ Register Src="~/SiteAssets/ReadNewsletterComponent.ascx" TagName="ComponentReader" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">

        function CheckCondizioni(sender, args) {

            if (document.getElementById("<%=ConfermaCheckBox.ClientID%>").checked)
                args.IsValid = true;
            else
                args.IsValid = false;

            return;
        }

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="AreaTitle" runat="Server">
    <asp:Label ID="TitoloPaginaLabel" runat="server" />

</asp:Content>



<asp:Content ID="Content3" ContentPlaceHolderID="AreaContentMain" runat="Server">
    <asp:Panel runat="server" ID="ContentPanel">
        <asp:HiddenField ID="LangHf" runat="server" />
        <div class="contacts-pagedesigner">

            <div class="container">
                <div class="row">
                    <div class="col-sm-6 form-horizontal">
                        <uc1:ComponentReader runat="server" ID="NewsletterContent" />
                    </div>
                    <div class="col-sm-6 form-horizontal">
                        <div class="form-group ">
                            <asp:Label ID="NomeLabel" runat="server" class="TFY_Testo1 control-label col-md-3" Text="Nome *" for="form-name" />
                            <div class="col-md-9">
                                <asp:TextBox ID="NomeTextBox" runat="server" CssClass="TFY_Testo1 form-control" name="form-name" />
                                <asp:RequiredFieldValidator CssClass="alert-text" ID="NomeRequiredFieldValidator" runat="server" SetFocusOnError="true" Display="Dynamic"
                                    ControlToValidate="NomeTextBox" ErrorMessage="Obbligatorio." Font-Bold="True" ValidationGroup="myValidation"> </asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group ">
                            <asp:Label ID="CognomeLabel" runat="server" class="TFY_Testo1 control-label col-md-3" Text="Cognome *" for="form-surname" />
                            <div class="col-md-9">
                                <asp:TextBox ID="CognomeTextBox" runat="server" CssClass="TFY_Testo1 form-control" name="form-surname" />
                                <asp:RequiredFieldValidator CssClass="alert-text" ID="CognomeRequiredFieldValidator" runat="server" SetFocusOnError="true" Display="Dynamic"
                                    ControlToValidate="CognomeTextBox" ErrorMessage="Obbligatorio." Font-Bold="True" ValidationGroup="myValidation"> </asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group ">
                            <asp:Label ID="EmailLabel" runat="server" class="TFY_Testo1 control-label col-md-3" Text="Indirizzo E-Mail *" for="form-email" />
                            <div class="col-md-9">
                                <asp:TextBox ID="EmailTextBox" runat="server" CssClass="TFY_Testo1 form-control" name="form-email" />
                                <asp:RequiredFieldValidator CssClass="alert-text" ID="MailRequiredFieldValidator" runat="server" SetFocusOnError="true" Display="Dynamic"
                                    ControlToValidate="EmailTextBox" ErrorMessage="Obbligatorio." Font-Bold="True" ValidationGroup="myValidation"> </asp:RequiredFieldValidator>
                                <asp:Label runat="server" ID="InvalidEmailLabel" CssClass="alert-text" Style="font-weight: bold; display: inline;" Visible="false">Invalid E-Mail</asp:Label>
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-sm-offset-3 col-sm-9">
                                <div class="HIC_LabelCorpoTesto1">
                                    <asp:CheckBox ID="ConfermaCheckBox" runat="server" />
                                    <a href="/Web/57/Privacy-policy" target="_blank">
                                        <asp:Label ID="CondizioniLabel" runat="server" class="TFY_Testo1" Text="Accetto le condizioni" />
                                    </a>
                                    <asp:CustomValidator CssClass="alert-text" ID="CondizioniRequiredFieldValidator" runat="server" Font-Bold="True"
                                        ErrorMessage="Obbligatorio" ClientValidationFunction="CheckCondizioni" ValidationGroup="myValidation"
                                        Display="Dynamic" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-sm-offset-3 col-sm-9">
                                <asp:LinkButton ID="SubscribeLinkButton" runat="server" Text="Subscribe" OnClick="SubscribeLinkButton_Click"
                                    CssClass="btn btn-default" ValidationGroup="myValidation" CausesValidation="true" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </asp:Panel>

    <asp:Panel runat="server" ID="MailSentPanel" Visible="false">
        <div class="row paddingBottom20">
            <div class="col-sm-12">
                <span class="TFY_Testo1">
                    <asp:Label runat="server" ID="MailSentLabel" />
                </span>
            </div>
        </div>
    </asp:Panel>

    <style type="text/css">
        h2 span {
            font-size: 3rem;
        }

        .form-horizontal span {
            font-size: 1.7rem;
        }

        .form-horizontal input {
            font-size: 1.9rem;
        }

        .btn {
            border: 1px solid #D8D8D8;
            font-size: 1.6rem;
            padding: 5px 15px;
            text-decoration: none;
        }

        .btn:hover{
            background-color: #D8D8D8;
            color: #000;
        }
    </style>
</asp:Content>

