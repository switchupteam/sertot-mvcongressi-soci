﻿<%@ Page Language="C#" MasterPageFile="~/Master/LayoutAreaSoci.master" CodeFile="Default.aspx.cs" Inherits="AreaSoci_Default" %>

<%@ Register Src="~/UserControl/MenuSoci.ascx" TagName="MenuSoci" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/NewsViewer.ascx" TagName="NewsViewer" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/PanelRinnovo.ascx" TagName="PanelRinnovo" TagPrefix="uc1" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Web.Security" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="AreaTitle" runat="Server">
    <%= titolo %>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="AreaContentMain" runat="Server">
    <section class="areaSociDefault">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <div class="LAY01A_Content1">

                        <%--<uc1:PanelRinnovo ID="PanelRinnovo1" runat="server" />--%>
                        <uc1:NewsViewer ID="NewsViewer" runat="server" />
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="LAY01A_Content2">
                        <uc1:MenuSoci ID="MenuSoci1" runat="server" />
                    </div>
                    <div class="FloatClr"></div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
