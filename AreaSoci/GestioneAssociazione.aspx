﻿<%@ Page Language="C#" MasterPageFile="~/Master/LayoutAreaSoci.master" Title="Sertot" Theme="Zeus" %>

<%@ Register Src="~/UserControl/MenuSoci.ascx" TagName="Menu_Soci" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/PagamentiSocio.ascx" TagName="PagamentiSocio" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/PanelRinnovo.ascx" TagName="PanelRinnovo" TagPrefix="uc1" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Web.Security" %>

<script runat="server">
    public string titolo = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();

        //if (new SertotUtility().IsCurrentUserAutenticated())
        //    Response.Redirect("/Login/Login.aspx?ReturnUrl=/AreaSoci/GestioneAssociazione.aspx");


        Page.Title = "Gestione associazione - Sertot";
        titolo = "Gestione associazione Sertot";

    }//fine Page_Load



    protected void RenewalButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("RinnovoQuota.aspx");
    }
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="AreaHeader" Runat="Server">HEADER
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AreaMenu" Runat="Server">MENU
</asp:Content>--%>
<asp:Content ID="Content4" ContentPlaceHolderID="AreaTitle" runat="Server">
    <%= titolo %>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="AreaContentMain" runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="LAY01A_Content1">
                    <p>
                        Per rinnovare la quota tramite bonifico bancario effettuare il versamento di € 150 per i soci ordinari e di € 50 per i soci aggregati indicando nella causale nome- cognome e annualità
                        <br />
                        I dati sono i seguenti 
                    </p>
                    <table border="1" width="100%">
                        <tr>
                            <td>
                                <p style="text-align:center"><strong>S.I.C.P.</strong></p>
                            </td>
                            <td>
                                <p style="text-align:center">UNICREDIT PARMA - P.LE SANTACROCE</p>
                            </td>
                            <td>
                                <p style="text-align:center">IT94 U 02008 12710 000040814653</p>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <p>
                        E’ necessario inviare copia del bonifico a <a href="mailto:sertot@mvcongressi.it">sertot@mvcongressi.it</a>
                    </p>

                    <br />
                    <br />

                    <%--<asp:HyperLink NavigateUrl="/AreaSoci/RinnovoQuota.aspx" ID="RenewalButton" runat="server" Visible="false" OnClick="RenewalButton_Click" ></asp:HyperLink>--%>
                    <uc1:PanelRinnovo ID="PanelRinnovo1" runat="server" />
                    <%--<br />--%>

                    <uc1:PagamentiSocio ID="PagamentiSocio" runat="server" />
                </div>
            </div>
            <div class="col-lg-3">
                <div class="LAY01A_Content2">
                    <uc1:Menu_Soci ID="Menu_Soci1" runat="server" />
                </div>
                <div class="FloatClr"></div>
            </div>
        </div>
    </div>
</asp:Content>
<%--<asp:Content ID="Content8" ContentPlaceHolderID="AreaFooter" runat="Server">
    FOOTER
</asp:Content>--%>