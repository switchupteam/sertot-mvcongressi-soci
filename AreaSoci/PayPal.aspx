﻿<%@ Page Language="C#" MasterPageFile="~/Master/LayoutAreaSoci.master" CodeFile="PayPal.aspx.cs" Inherits="AreaSoci_PayPal" %>

<%@ Register Src="~/UserControl/MenuSoci.ascx" TagName="Menu_Soci" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/PayPal.ascx" TagName="PayPal" TagPrefix="uc1" %>


<asp:Content runat="server" ID="Content1" ContentPlaceHolderID="head">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="AreaTitle" runat="Server">
    Pagamento con PayPal
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="AreaContentMain">
    <section class="payPal">
        <div class="row">
            <div class="col-md-9">
                <uc1:PayPal ID="PayPal1" runat="server" />
            </div>
            <div class="col-md-3">

                <div class="LAY01A_Content2">
                    <uc1:Menu_Soci ID="Menu_Soci1" runat="server" />
                </div>
                <div class="FloatClr"></div>
            </div>
        </div>
    </section>
</asp:Content>



<asp:Content runat="server" ID="Content3" ContentPlaceHolderID="script">
</asp:Content>
