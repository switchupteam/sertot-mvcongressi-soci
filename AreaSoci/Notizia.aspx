﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Master/LayoutAreaSoci.master" CodeFile="Notizia.aspx.cs" Inherits="AreaSoci_Notizia" %>

<%@ Register Src="~/UserControl/SideMenu_NewsSoci.ascx" TagName="SideMenu_NewsSoci" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/MenuSoci.ascx" TagName="Menu_Soci" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="AreaHeader" Runat="Server">
</asp:Content>--%>
<asp:Content ID="Content3" ContentPlaceHolderID="AreaTitle" runat="Server">
    <%= titolo %>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AreaContentMain" runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="LAY01A_Content1 TFY_Testo1">
                    <div class="TFY_Titolo1">
                        <%= titoloNotizia %>
                        <br />
                        <br />
                    </div>
                    <%--<asp:FormView ID="FormView2" runat="server" DataKeyNames="ID1Publisher" DataSourceID="dsNotizia" EnableModelValidation="True">--%>
                        <%--<ItemTemplate>--%>
                    <asp:Panel runat="server" ID="ContentPanel">
                            <%--<asp:Label ID="Contenuto1Label" runat="server" Text='<%# Bind("Contenuto1") %>' />--%>
                            <asp:Label ID="Contenuto1_Label" runat="server" />

                            <asp:Placeholder runat="server" ID="DocumentoPlaceholder" Visible="false">
                                <div class="link">
                                    <a href="<%= urlDocumento %>" target="_blank"><%= testoDocumento %></a>
                                </div>
                            </asp:Placeholder>

                            <%--<asp:HiddenField ID="hfTitolo" runat="server" Value='<%# Eval("Titolo") %>' OnDataBinding="Titolo_DataBinding" />--%>

<%--                            <asp:HiddenField ID="TitoloBrowserHidenField" runat="server" Value='<%# Eval("TitoloBrowser") %>' OnDataBinding="TitoloBrowserHidenField_DataBinding" />
                            <asp:HiddenField ID="hfTagDescription" runat="server" Value='<%# Eval("TagDescription") %>' OnDataBinding="TagDescription_DataBinding" />
                            <asp:HiddenField ID="hfTagKeywords" runat="server" Value='<%# Eval("TagKeywords") %>' OnDataBinding="TagKeywords_DataBinding" />--%>

                            <%--<asp:HiddenField ID="hfContenuto2" runat="server" Value='<%# Eval("Contenuto2") %>' OnDataBinding="Contenuto2_DataBinding" />--%>
                    </asp:Panel>
                        <%--</ItemTemplate>--%>
                    <%--</asp:FormView>--%>
<%--                    <asp:SqlDataSource ID="dsNotizia" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                            SelectCommand="SELECT 
                              [ID1Publisher], 
                              [Titolo], 
                              [TagDescription], 
                              [TagKeywords], 
                              [Contenuto1], 
                              [Contenuto2],
                              TitoloBrowser 
                          FROM [vwPublisher_Public_DtlArea1] 
                          WHERE ([ID1Publisher] = @ID1Publisher)"
                        OnSelected="dsNotizia_Selected">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="0" Name="AttivoArea1" Type="Int32" />
                            <asp:QueryStringParameter DefaultValue="0" Name="ID1Publisher"
                                QueryStringField="XRI" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>--%>

                </div>
            </div>
            <div class="col-lg-3">
                <div class="LAY01A_Content2">
                    <div class="ZITC020 TFY_Testo1">
                        <asp:Label ID="Contenuto2_Label" runat="server" />
                    </div>
                    <div class="ZITC020">
                        <uc1:Menu_Soci ID="Menu_Soci1" runat="server" />
                    </div>
                    <%--<uc1:SideMenu_NewsSoci ID="SideMenu_NewsSoci1" runat="server" />--%>
                </div>
                <div class="FloatClr"></div>
            </div>
        </div>
    </div>
</asp:Content>
