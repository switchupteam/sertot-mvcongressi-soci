﻿<%@ Page Language="C#" MasterPageFile="~/Master/LayoutAreaSoci.master" %>

<%@ Register Src="~/UserControl/MenuSoci.ascx" TagName="Menu_Soci" TagPrefix="uc1" %>

<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Web.Security" %>
<%@ Import Namespace="System.Net.Mail" %>


<script runat="server">

    public string titolo = String.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();


        // Se capito su questa pagina, ma non c'è bisogno di rinnovo, redirect alla pagina di gestione
        if (!clsGenericUtils.IsSocioOnorario(Membership.GetUser().ProviderUserKey.ToString()))
        {
            if (!clsGenericUtils.NeedsRenewal(Membership.GetUser().ProviderUserKey.ToString()))
                Response.Redirect("GestioneAssociazione.aspx");
        }



        TitleField.Value = "Rinnovo iscrizione";
        titolo = "Rinnovo iscrizione";
        Page.Title = "Sertot - Rinnovo iscrizione";
        MembershipUser user = Membership.GetUser();



        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        using (SqlConnection connection = new SqlConnection(strConnessione))
        {
            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                string sqlQuery = "SELECT [UserId], [CognomeNome], [Email] FROM [vwSoci_Lst] WHERE UserId = @UserId";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@UserId", System.Data.SqlDbType.VarChar);
                command.Parameters["@UserId"].Value = user.ProviderUserKey.ToString();

                SqlDataReader reader = command.ExecuteReader();

                if (reader.Read())
                {
                    if (reader["CognomeNome"] != null)
                    {
                        var labelNomeCognomeSocio = (Label)FormView1.FindControl("NomeCognomeSocio");
                        labelNomeCognomeSocio.Text = reader["CognomeNome"].ToString();

                        var labelEmailSocio = (HiddenField)FormView1.FindControl("EmailSocio");
                        labelEmailSocio.Value = reader["Email"].ToString();
                    }
                }
                reader.Close();
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
            }
        }
        //((DropDownList)FormView1.FindControl("DropDownListSocio")).SelectedValue = user.ProviderUserKey.ToString();
        //((DropDownList)FormView1.FindControl("DropDownListSocio")).Enabled = false;
        UID.Value = user.ProviderUserKey.ToString();

        int iAnno, iMese, iDicembre = 12;

        iAnno = int.Parse(DateTime.Now.ToString("yyyy"));           // Recupero anno ...
        iMese = int.Parse(DateTime.Now.ToString("MM"));             // ... e mese corrente

        if (iMese == iDicembre)                         // Se siamo a dicembre, dobbiano rinnovare
            iAnno++;                                    // per l'anno prossimo

        string lastYear = new delinea().GetRenewYear(DateTime.Now.Year).ToString();
        ((Label)FormView1.FindControl("LabelLastYear")).Text = iAnno <= Convert.ToInt32(lastYear) ? lastYear : iAnno.ToString();

    } // Page_Load



    protected void PagamentoSqlDataSource_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        var email = ((HiddenField)FormView1.FindControl("EmailSocio")).Value;

        if (!string.IsNullOrWhiteSpace(email))
        {
            var utenti = Membership.FindUsersByEmail(email);
            SendMail(utenti, email);
            //TODO: scommentare per inviare mail a Sertot
            SendMail(utenti, ConfigurationManager.AppSettings["MailSertot"]);
            //SendMail(utenti, "web@switchup.it");
        }

        if (e.AffectedRows == 0 || e.Exception != null)
            Response.Redirect("~/System/Message.aspx?InsertErr");

        Response.Redirect("GestioneAssociazione.aspx");

    } // PagamentoSqlDataSource_Inserted

    protected void UtenteCreazione(object sender, EventArgs e)
    {
        HiddenField UtenteCreazione = (HiddenField)sender;
        UtenteCreazione.Value = Membership.GetUser().ProviderUserKey.ToString();

    }


    protected void DataOggi(object sender, EventArgs e)
    {
        HiddenField DataCreazione = (HiddenField)sender;
        DataCreazione.Value = DateTime.Now.GetDataItaliana();

    }//fine DataOggi

    protected void InsertButton_Click(object sender, EventArgs e)
    {
        //HiddenField AnnoHiddenField = (HiddenField)FormView1.FindControl("AnnoHiddenField");
        HiddenField UserIdHiddenField = (HiddenField)FormView1.FindControl("UserIdHiddenField");
        HiddenField PagamentoEurHiddenField = (HiddenField)FormView1.FindControl("PagamentoEurHiddenField");
        DropDownList DropDownListQuota = (DropDownList)FormView1.FindControl("DropDownListQuota");

        //HiddenField AnnoProgressivoHiddenField = (HiddenField)FormView1.FindControl("AnnoProgressivoHiddenField");
        Label LabelLastYear = (Label)FormView1.FindControl("LabelLastYear");


        if (Page.IsValid)
        {
            //int NextYear = DateTime.Now.Year + 1;
            //AnnoHiddenField.Value = NextYear.ToString();

            MembershipUser user = Membership.GetUser();
            UserIdHiddenField.Value = user.ProviderUserKey.ToString();

            PagamentoEurHiddenField.Value = GetPrice(DropDownListQuota.SelectedItem.Value);


            //ANNO PROGRESSIVO
            //String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
            //using (SqlConnection connection = new SqlConnection(strConnessione))
            //{
            //    try
            //    {
            //        connection.Open();
            //        SqlCommand command = connection.CreateCommand();

            //        string sqlQuery = @"SELECT anno, AnnoProgressivo 
            //                            FROM tbQuoteAnnuali
            //                            WHERE AnnoProgressivo IS NOT NULL AND AnnoProgressivo != ''
            //                                AND Anno = @Anno
            //                            ORDER BY Anno DESC, AnnoProgressivo DESC";
            //        command.CommandText = sqlQuery;
            //        command.Parameters.Clear();
            //        command.Parameters.AddWithValue("@Anno", LabelLastYear.Text);

            //        SqlDataReader reader = command.ExecuteReader();
            //        if (reader.Read())
            //        {
            //            if (!string.IsNullOrWhiteSpace(reader["AnnoProgressivo"].ToString()))
            //            {
            //                string oldAnnoProg = reader["AnnoProgressivo"].ToString();
            //                string[] oldAnnoProgSplit = oldAnnoProg.Split('/');

            //                string newAnnoProg = string.Empty;
            //                if (oldAnnoProgSplit[1].Equals(LabelLastYear.Text))
            //                {
            //                    string num = (Convert.ToInt32(oldAnnoProgSplit[0]) + 1).ToString();
            //                    string numResult = num.Count() == 1 ? "000" + num
            //                        : num.Count() == 2 ? "00" + num
            //                        : num.Count() == 3 ? "0" + num
            //                        : num;

            //                    newAnnoProg = string.Format("{0}/{1}", numResult, oldAnnoProgSplit[1]);
            //                }
            //                else
            //                    newAnnoProg = string.Format("{0}/{1}", "0001", LabelLastYear.Text);

            //                AnnoProgressivoHiddenField.Value = newAnnoProg;
            //            }
            //            else
            //                AnnoProgressivoHiddenField.Value = string.Format("{0}/{1}", "0001", LabelLastYear.Text);
            //        }

            //        reader.Close();
            //    }
            //    catch (Exception p)
            //    {
            //        Response.Write(p.ToString());
            //    }
            //}
        }
    }

    private string GetPrice(string ID1Pagina)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;
        string myReturn = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {


            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();


                sqlQuery = "SELECT QuotaTotale FROM vwQuote_Lst WHERE IDQuota=@ID2QuotaCosto";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID2QuotaCosto", System.Data.SqlDbType.Int);
                command.Parameters["@ID2QuotaCosto"].Value = ID1Pagina;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (reader["QuotaTotale"] != null)
                        myReturn = reader["QuotaTotale"].ToString();
                }
                reader.Close();
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
            }
        }//fine Using


        return myReturn;

    }//fine

    private bool SendMail(MembershipUserCollection AllUsers, string Email)
    {
        try
        {
            System.Net.Configuration.SmtpSection section = ConfigurationManager.GetSection("system.net/mailSettings/smtp") as System.Net.Configuration.SmtpSection;

            MailMessage mail = new MailMessage();

            // ------------- se il destinatario è LISTA allora spedisco all'elenco destinatati previsti
            //if (Email == "LISTA")
            //{
            //    mail.To.Add(new MailAddress("support@switchup.it"));                
            //}
            //else
            //{
            mail.To.Add(new MailAddress(Email));
            //}

            // Hop: 20170328 aggiunto per "log"
            //mail.Bcc.Add(new MailAddress("web@switchup.it"));

            mail.From = new MailAddress(section.From, "Sito Web Società Emiliano Romagnola Triveneta di Ortopedia e Traumatologia ");
            mail.Subject = "Conferma pagamento quota associativa Società Emiliano Romagnola Triveneta di Ortopedia e Traumatologia ";

            mail.IsBodyHtml = true;
            string myBody = string.Empty;

            foreach (MembershipUser myUser in AllUsers)
            {
                myBody += "Per accedere all'Area Soci del sito web dovrai utilizzare le seguenti credenziali:";
                myBody += "<br />";
                myBody += "<br />";
                myBody += "UserName: " + myUser.UserName;
                myBody += "<br />";
                myBody += "Password: " + myUser.GetPassword();
                myBody += "<br />";
                myBody += "Indirizzo E-Mail associato: " + myUser.Email;
                myBody += "<br />";
                myBody += "<br />";
                myBody += "La password può essere modificata in qualunque momento all’interno dell’Area Soci.";
                myBody += "<br />";
                myBody += "<br />";
                myBody += "<br />";
                myBody += "Un cordiale saluto";
                myBody += "<br />";
                myBody += "<br />";
                myBody += "La segreteria Società Emiliano Romagnola Triveneta di Ortopedia e Traumatologia ";
            }

            mail.Body = myBody;
            SmtpClient client = new SmtpClient();
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.Port = 587;
            client.EnableSsl = true;
            client.SendAsync(mail, string.Empty);

            return true;
        }
        catch (Exception p)
        {

            Response.Write(p.ToString());
            return false;
        }

    }//fine SendMail

    protected void SqlDataSourcePagamenti_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {

    }
</script>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AreaTitle" runat="Server">
    <%= titolo %>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="AreaContentMain" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <asp:HiddenField ID="TitleField" runat="server" />
                <asp:HiddenField ID="UID" runat="server" />
                <div class="LAY01A_Content1">

                    <asp:FormView ID="FormView1" runat="server" DataSourceID="SqlDataSourcePagamenti" RenderOuterTable="false" EnableModelValidation="True" DefaultMode="Insert">

                        <InsertItemTemplate>
                            <div class="BlockBox">
                                <div class="BlockBoxHeader">
                                    <asp:Label ID="ZML_BoxCategoria" runat="server" CssClass="TFY_Sottotitolo3" Text="Dati pagamento"></asp:Label>
                                </div>
                                <table width="100%">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="Label3" SkinID="FieldDescription" runat="server" Text="Rinnovo anno"></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:Label ID="LabelLastYear" SkinID="FieldDescription" runat="server" Text='<%# Bind("Anno") %>'></asp:Label>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="NRegistro" SkinID="FieldDescription" runat="server" Text="Socio"></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:Label runat="server" ID="NomeCognomeSocio" Text=""></asp:Label>
                                            <asp:HiddenField runat="server" ID="EmailSocio" Value=""></asp:HiddenField>

                                            <%--<asp:DropDownList AppendDataBoundItems="true" ID="DropDownListSocio" runat="server" DataSourceID="dsSocio" DataTextField="CognomeNome" DataValueField="UserId" SelectedValue='<%# Bind("UserID") %>'>
                                                <asp:ListItem Text="&gt;  Non inserito" Selected="True" Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="dsSocio" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                                SelectCommand="SELECT [UserId], [CognomeNome] FROM [vwSoci_Lst] ORDER BY [CognomeNome]"></asp:SqlDataSource>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" ErrorMessage="Obbligatorio"
                                                runat="server" ControlToValidate="DropDownListSocio" SkinID="ZSSM_Validazione01"
                                                Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>--%>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="Label2" SkinID="FieldDescription" runat="server" Text="Tipo pagamento"></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:DropDownList AppendDataBoundItems="true" ID="DropDownList1" runat="server" DataSourceID="dsMetodoPagamento" DataTextField="MetodoPagamento" DataValueField="ID1MetodoPagamento" SelectedValue='<%# Bind("ID2MetodoPagamento") %>'>
                                                <asp:ListItem Text="&gt;  Non inserito" Selected="True" Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="dsMetodoPagamento" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                                SelectCommand="SELECT [ID1MetodoPagamento], [MetodoPagamento],[PubOrderSoci] FROM [tbMetodiPagamento] WHERE [PubOrderSoci]>0 ORDER BY [PubOrderSoci]"></asp:SqlDataSource>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Obbligatorio"
                                                runat="server" ControlToValidate="DropDownList1" SkinID="ZSSM_Validazione01"
                                                Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="Label1" SkinID="FieldDescription" runat="server" Text="Quota"></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:DropDownList AppendDataBoundItems="true" ID="DropDownListQuota" runat="server" DataSourceID="dsQuota" DataTextField="DescPrezzo" DataValueField="IDQuota" SelectedValue='<%# Bind("Id2Quota") %>'>
                                                <asp:ListItem Text="&gt;  Non inserito" Selected="True" Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="dsQuota" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                                SelectCommand="SELECT [IDQuota], [Descrizione], [QuotaTotale], [Descrizione]+' - '+ CONVERT(varchar(10),[QuotaTotale]) AS [DescPrezzo] FROM [vwQuote_Lst] ORDER BY [Descrizione]"></asp:SqlDataSource>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Obbligatorio"
                                                runat="server" ControlToValidate="DropDownListQuota" SkinID="ZSSM_Validazione01"
                                                Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="Label5" SkinID="FieldDescription" runat="server" Text="Dettagli pagamento e altre note"></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <style>
                                                .TextBox {
                                                    height: auto;
                                                }
                                            </style>
                                            <asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine" Rows="5" Columns="50" CssClass="Multiline"
                                                Text='<%# Bind("NoteSocio") %>' />
                                        </td>
                                    </tr>

                                </table>
                                <br />
                                <asp:HiddenField ID="RecordNewUserHiddenField" runat="server" Value='<%# Bind("RecordNewUser", "{0}") %>'
                                    OnDataBinding="UtenteCreazione" />
                                <asp:HiddenField ID="RecordNewDateHiddenField" Visible="false" runat="server" Value='<%# Bind("RecordNewDate", "{0}") %>'
                                    OnDataBinding="DataOggi" />

                                <%--<asp:HiddenField ID="AnnoHiddenField" runat="server" Value='<%# Bind("Anno") %>' />--%>
                                <asp:HiddenField ID="UserIdHiddenField" runat="server" Value='<%# Bind("UserID") %>' />
                                <asp:HiddenField ID="PagamentoEurHiddenField" runat="server" Value='<%# Bind("PagamentoEur") %>' />
                            </div>

                            <div align="center">
                                <%--<asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" OnClick="InsertButton_Click"
                                    CommandName="Insert" Text="Inserisci" SkinID="ZSSM_Button01" />
                                &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server"
                                    CausesValidation="False" CommandName="Cancel" Text="Annulla" SkinID="ZSSM_Button01" />--%>
                                <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" OnClick="InsertButton_Click" CssClass="ZSSM_Button01_LinkButton"
                                    CommandName="Insert" Text="Inserisci" SkinID="ZSSM_Button01" />
                                &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CssClass="ZSSM_Button01_LinkButton"
                                    CausesValidation="False" CommandName="Cancel" Text="Annulla" SkinID="ZSSM_Button01" />
                            </div>

                        </InsertItemTemplate>
                    </asp:FormView>
                    <asp:SqlDataSource ID="SqlDataSourcePagamenti" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                        InsertCommand="INSERT INTO [tbQuoteAnnuali] ([UserID], [ID2QuotaCosto], [Anno], [PagamentoEur], [NoteSocio], [ID2MetodoPagamento], [RecordNewUser], [RecordNewDate], [ZeusIsAlive]) 
                                        VALUES (@UserID, @ID2Quota, @Anno, @PagamentoEur, @NoteSocio, @ID2MetodoPagamento, @RecordNewUser, @RecordNewDate, 1) SELECT @XRI = SCOPE_IDENTITY();"
                        OnInserted="PagamentoSqlDataSource_Inserted">

                        <InsertParameters>
                            <asp:Parameter Direction="Output" Name="XRI" Type="Int32" />
                            <asp:Parameter Name="UserID" />
                            <asp:Parameter Name="ID2Quota" Type="Int32" />
                            <asp:Parameter Name="ID2MetodoPagamento" Type="Int32" />
                            <asp:Parameter Name="PagamentoEur" Type="Decimal" />
                            <asp:Parameter Name="Anno" Type="Int16" />
                            <asp:Parameter Name="NoteSocio" Type="String" />
                            <asp:Parameter Name="RecordNewUser" />
                            <asp:Parameter Name="RecordNewDate" Type="DateTime" />
                        </InsertParameters>
                    </asp:SqlDataSource>

                </div>
            </div>
            <div class="col-lg-3">

                <div class="LAY01A_Content2">
                    <uc1:Menu_Soci ID="Menu_Soci1" runat="server" />
                </div>
                <div class="FloatClr"></div>
            </div>
        </div>
    </div>
</asp:Content>
