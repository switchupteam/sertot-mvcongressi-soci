﻿<%@ Page Language="C#" MasterPageFile="~/Master/LayoutAreaSoci.master" CodeFile="PagamentoTerminato.aspx.cs" Inherits="AreaSoci_PagamentoTerminato" %>

<asp:Content runat="server" ID="Content1" ContentPlaceHolderID="head">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="AreaTitle" runat="Server">
    <%= titolo %>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="AreaContentMain">
    <section class="pagamentoTerminato">
        <div class="container">
            <p class="testo"><%= HttpUtility.HtmlDecode(testo) %></p>
        </div>
    </section>
</asp:Content>



<asp:Content runat="server" ID="Content3" ContentPlaceHolderID="script">
</asp:Content>
