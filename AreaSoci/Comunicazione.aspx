﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/LayoutAreaSoci.master" %>

<%@ Register Src="~/UserControl/SideMenu_CommSoci.ascx" TagName="SideMenu_CommSoci" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/MenuSoci.ascx" TagName="Menu_Soci" TagPrefix="uc1" %>

<script runat="server">

    public string categoria = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (new SertotUtility().IsCurrentUserAutenticated())
        //    Response.Redirect("/Login/Login.aspx?ReturnUrl=/AreaSoci/Comunicazione.aspx");
    }//fine Page_Load

    protected void dsNotizia_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {
        if ((e.AffectedRows <= 0)
            || (e.Exception != null))
            Response.Redirect("~/System/Message.aspx?SelectErr");

        Page.Title = "Comunicazioni ai Soci - Sertot";

    }//fine dsNotizia_Selected

    protected void TagDescription_DataBinding(object sender, EventArgs e)
    {
        HiddenField hfTagDescription = (HiddenField)sender;

        HtmlMeta hm = new HtmlMeta();
        hm.Name = "Description";
        hm.Content = hfTagDescription.Value;
        Page.Header.Controls.Add(hm);

    }// fine hfTagDescription_DataBinding


    protected void TagKeywords_DataBinding(object sender, EventArgs e)
    {
        HiddenField hfTagKeywords = (HiddenField)sender;

        HtmlMeta hm = new HtmlMeta();
        hm.Name = "Keywords";
        hm.Content = hfTagKeywords.Value;
        Page.Header.Controls.Add(hm);

    }// fine hfTagKeywords_DataBinding

    protected void Titolo_DataBinding(object sender, EventArgs e)
    {
        HiddenField hfTitolo = (HiddenField)sender;
        categoria = "Comunicazioni ai Soci";
        TitoloNotizia.Text = hfTitolo.Value;
    }

    protected void Contenuto2_DataBinding(object sender, EventArgs e)
    {
        HiddenField hfContenuto2 = (HiddenField)sender;
        Contenuto2_Label.Text = hfContenuto2.Value;
    }

    protected void TitoloBrowserHidenField_DataBinding(object sender, EventArgs e)
    {
        //HiddenField TitoloBrowserHidenField = (HiddenField)sender;
        //Page.Title = TitoloBrowserHidenField.Value + " - Sertot";
    }

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="AreaHeader" Runat="Server">
</asp:Content>--%>
<asp:Content ID="Content3" ContentPlaceHolderID="AreaTitle" runat="Server">
    <%= categoria %>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AreaContentMain" runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="LAY01A_Content1 TFY_Testo1">
                    <div class="TFY_Titolo1">
                        <asp:Label ID="TitoloNotizia" runat="server" /><br />
                        <br />
                    </div>
                    <asp:FormView ID="FormView1" runat="server"
                        DataKeyNames="ID1Publisher" DataSourceID="dsNotizia"
                        EnableModelValidation="True">
                        <ItemTemplate>

                            <asp:Label ID="Contenuto1Label" runat="server" Text='<%# Bind("Contenuto1") %>' />

                            <asp:HiddenField ID="hfTitolo" runat="server" Value='<%# Eval("Titolo") %>' OnDataBinding="Titolo_DataBinding" />
                            <asp:HiddenField ID="TitoloBrowserHidenField" runat="server" Value='<%# Eval("TitoloBrowser") %>'
                                OnDataBinding="TitoloBrowserHidenField_DataBinding" />

                            <asp:HiddenField ID="hfTagDescription" runat="server" Value='<%# Eval("TagDescription") %>' OnDataBinding="TagDescription_DataBinding" />
                            <asp:HiddenField ID="hfTagKeywords" runat="server" Value='<%# Eval("TagKeywords") %>' OnDataBinding="TagKeywords_DataBinding" />

                            <asp:HiddenField ID="hfContenuto2" runat="server" Value='<%# Eval("Contenuto2") %>' OnDataBinding="Contenuto2_DataBinding" />


                        </ItemTemplate>
                    </asp:FormView>
                    <asp:SqlDataSource ID="dsNotizia" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                        SelectCommand="SELECT 
                          [ID1Publisher], 
                          [Titolo], 
                          [TagDescription], 
                          [TagKeywords], 
                          [Contenuto1], 
                          [Contenuto2],
                          TitoloBrowser 
                      FROM [vwPublisher_Public_DtlArea1] 
                      WHERE ([ID1Publisher] = @ID1Publisher) AND (ZeusIdModulo='PBCOS')"
                        OnSelected="dsNotizia_Selected">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="0" Name="AttivoArea1" Type="Int32" />
                            <asp:QueryStringParameter DefaultValue="0" Name="ID1Publisher"
                                QueryStringField="XRI" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>

                </div>
            </div>
            <div class="col-lg-3">
                <div class="LAY01A_Content2">
                    <div class="ZITC020 TFY_Testo1">
                        <asp:Label ID="Contenuto2_Label" runat="server" />
                    </div>
                    <div class="ZITC020">
                        <uc1:Menu_Soci ID="Menu_Soci1" runat="server" />
                    </div>
                    <uc1:SideMenu_CommSoci ID="SideMenu_CommSoci1" runat="server" />
                </div>
                <div class="FloatClr"></div>
            </div>
        </div>
    </div>
</asp:Content>
