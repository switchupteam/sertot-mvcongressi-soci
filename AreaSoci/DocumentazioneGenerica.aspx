﻿<%@ Page Language="C#" MasterPageFile="~/Master/LayoutAreaSoci.master" %>

<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register Src="~/UserControl/MenuSoci.ascx" TagName="Menu_Soci" TagPrefix="uc1" %>
<script runat="server">
    public string titolo = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (string.IsNullOrEmpty(Request.QueryString["XRI"]))
        //    Response.Redirect("~/System/Message.aspx?0");

        //try
        //{
        //    Convert.ToInt32(Request.QueryString["XRI"]);
        //}
        //catch (Exception)
        //{
        //    Response.Redirect("~/System/Message.aspx?0");
        //}
        //if (!CheckZID(Request.QueryString["XRI"], "PDWEB"))
        //{
        //    Response.Redirect("/System/Message.aspx?0");

        //}



        titolo = "Documentazione Generica";
        Page.Title = titolo + " - Sertot";
    }//fine Page_Load

    protected void TagKeywordsHiddenField_DataBinding(object sender, EventArgs e)
    {
        HiddenField TagKeywordsHiddenField = (HiddenField)sender;

        HtmlMeta hm = new HtmlMeta();
        hm.Name = "Keywords";
        hm.Content = TagKeywordsHiddenField.Value;
        Page.Header.Controls.Add(hm);

    }//TagDescriptionHiddenField_DataBinding

    protected void TagDescriptionHiddenField_DataBinding(object sender, EventArgs e)
    {
        HiddenField agDescriptionHiddenField = (HiddenField)sender;

        HtmlMeta hm = new HtmlMeta();
        hm.Name = "Description";
        hm.Content = agDescriptionHiddenField.Value;
        Page.Header.Controls.Add(hm);

    }//TagDescriptionHiddenField_DataBinding



    protected void PageDesignerSqlDataSource_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.AffectedRows == 0 || e.Exception != null)
            Response.Redirect("~/System/Message.aspx?SelectErr");
    }


    private Boolean CheckZID(string ID1Pagina, string ZID)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;
        string myReturn = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {


            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();


                sqlQuery = "SELECT ZeusIdModulo FROM tbPageDesigner WHERE ID1Pagina=@ID1Pagina";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID1Pagina", System.Data.SqlDbType.Int);
                command.Parameters["@ID1Pagina"].Value = ID1Pagina;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (reader["ZeusIdModulo"].ToString().Equals(ZID))
                        return true;
                }
                reader.Close();
            }
            catch (Exception p)
            {
                return false;
            }
            return false;
        }//fine Using


        //return false;

    }//fine GetIDParentCategory

    protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        try
        {
            string myZeusIdModulo = string.Empty;
            string myRewrite = string.Empty;




            if (e.Item.ItemType == ListItemType.Item ||
            e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //HtmlAnchor myHtmlLink = e.Item.FindControl("CollegamentoIntestazione") as HtmlAnchor;
                //myHtmlLink.HRef = UrlRewriteXML(myZeusIdModulo, "ITA", myRewrite, myID);

                Repeater Repeater2 = e.Item.FindControl("Repeater2") as Repeater;
                Repeater2.DataSource = GetData(string.Format(@"SELECT  
                                                                [Titolo], 
                                                                [Allegato1_File]
                                                            FROM [vwPublisher_Dtl] 
                                                            WHERE (ZeusIdModulo='PBDCG') 
                                                                AND (ID2Categoria1={0}) 
                                                                AND (ZeusLangCode = '" + (Request.QueryString["Lang"] == "ENG" ? "ENG" : "ITA") + @"') 
                                                                AND (ZeusIsAlive = 1) 
                                                                AND (AttivoArea1 > 0) ", ((System.Data.DataRowView)e.Item.DataItem).Row["ID1Categoria"].ToString()));
                Repeater2.DataBind();
            }

        }
        catch (Exception p)
        {
            // Response.Redirect("~/System/Message.aspx?SelectErr");
            Response.Write(p.ToString());
        }
    }

    public System.Data.DataTable GetData(string query)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(strConnessione))
        {
            using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand())
            {
                cmd.CommandText = query;
                using (System.Data.SqlClient.SqlDataAdapter sda = new System.Data.SqlClient.SqlDataAdapter())
                {
                    cmd.Connection = con;
                    sda.SelectCommand = cmd;
                    using (System.Data.DataSet ds = new System.Data.DataSet())
                    {
                        System.Data.DataTable dt = new System.Data.DataTable();
                        sda.Fill(dt);
                        return dt;
                    }
                }
            }
        }
    }

</script>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="AreaHeader" Runat="Server">HEADER
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AreaMenu" Runat="Server">MENU
</asp:Content>--%>
<asp:Content ID="Content4" ContentPlaceHolderID="AreaTitle" runat="Server">
    <%= titolo %>
</asp:Content>



<asp:Content ID="Content5" ContentPlaceHolderID="AreaContentMain" runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="LAY01A_Content1">
                    <div class="SMN07D">
                        <asp:Repeater runat="server" ID="Repeater1" OnItemDataBound="Repeater1_ItemDataBound" DataSourceID="PageDesignerSqlDataSource">
                            <ItemTemplate>

                                <div class="SMN07D_LayTitolo">
                                    <h2 class="SMN07D_TxtTitolo"><%# Eval("Categoria") %></h2>
                                </div>
                                <asp:Repeater runat="server" ID="Repeater2">
                                    <HeaderTemplate>
                                        <div class="SMN07D_LayElenco">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div class="SMN07D_List"><a href="<%# Eval("Allegato1_File","/ZeusInc/Publisher/Documents/{0}") %>" target="_blank"><%# Eval("Titolo") %></a></div>

                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </div>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <%--<asp:Label ID="Contenuto1Label" runat="server" Text='<%# Eval("Contenuto1") %>' />--%>
                            </ItemTemplate>
                        </asp:Repeater>
                        <asp:SqlDataSource ID="PageDesignerSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                            SelectCommand="SELECT        dbo.vwCategorie_All.Livello, dbo.vwCategorie_All.Categoria, dbo.vwPublisher_Dtl.Titolo, dbo.vwCategorie_All.AttivoArea1 AS AttivoArea1Categoria,  dbo.vwPublisher_Dtl.AttivoArea1 AS AttivoArea1CategoriaDownload, dbo.vwPublisher_Dtl.Allegato1_File, dbo.vwCategorie_All.ZeusIdModulo , dbo.vwCategorie_All.ID1Categoria FROM            dbo.vwCategorie_All INNER JOIN dbo.vwPublisher_Dtl ON dbo.vwCategorie_All.ID1Categoria = dbo.vwPublisher_Dtl.ID2Categoria1 WHERE     (dbo.vwCategorie_All.Livello = 3) AND (dbo.vwCategorie_All.ZeusIdModulo = 'CTDCG') AND (dbo.vwCategorie_All.ZeusLangCode = @ZeusLangCode) AND (dbo.vwPublisher_Dtl.AttivoArea1 > 0)">
                            <SelectParameters>
                                <asp:QueryStringParameter DefaultValue="ITA" Name="ZeusLangCode" QueryStringField="Lang" Type="String" ConvertEmptyStringToNull="True" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="LAY01A_Content2">
                    <uc1:Menu_Soci ID="Menu_Soci1" runat="server" />

                </div>
                <div class="FloatClr"></div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        if (!($('.SMN07D_LayTitolo').length > 0)) {
            $('.LAY01A_Content1').html("<div class=\"LAY01A_Content1 TFY_Testo1\">Nessun elemento presente</div>");
        }


    </script>
</asp:Content>
