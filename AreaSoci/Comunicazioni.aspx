﻿<%@ Page Language="C#" MasterPageFile="~/Master/LayoutAreaSoci.master" Title="Sertot" %>

<%@ Register Src="~/UserControl/MenuSoci.ascx" TagName="Menu_Soci" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/CommViewer.ascx" TagName="CommViewer" TagPrefix="uc1" %>

<script runat="server">
    public string titolo = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (new SertotUtility().IsCurrentUserAutenticated())
        //    Response.Redirect("/Login/Login.aspx?ReturnUrl=/AreaSoci/Comunicazioni.aspx");


        Page.Title = "Comunicazioni Soci - Sertot";
        titolo = "Comunicazioni Soci Sertot";

    }//fine Page_Load


</script>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="AreaHeader" Runat="Server">HEADER
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AreaMenu" Runat="Server">MENU
</asp:Content>--%>
<asp:Content ID="Content4" ContentPlaceHolderID="AreaTitle" runat="Server">
    <%= titolo %>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="AreaContentMain" runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="LAY01A_Content1">

                    <uc1:CommViewer ID="CommViewer" runat="server" />
                </div>
            </div>
            <div class="col-lg-3">
                <div class="LAY01A_Content2">
                    <uc1:Menu_Soci ID="Menu_Soci1" runat="server" />
                </div>
                <div class="FloatClr"></div>
            </div>
        </div>
    </div>
</asp:Content>
