﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class AreaSoci_PagamentoTerminato : System.Web.UI.Page
{
    public string titolo = "Pagamento ";
    public string testo = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["tipo"] != null)
        {
            if (Request.QueryString["tipo"].ToString().Contains("confermato"))
            {
                titolo += "confermato";
                testo = string.Format("Il pagamento della quota {0} è avvenuto con successo."
                    , Request.QueryString["tipo"].ToString().Contains("ordinario") ? "da socio ordinario"
                    : Request.QueryString["tipo"].ToString().Contains("aggregato") ? "da socio aggregato" : string.Empty);
            }
            else if (Request.QueryString["tipo"].ToString().Contains("fallito"))
            {
                titolo += "fallito";
                testo = string.Format("Il pagamento della quota {0} non è andato a buon fine. <br />Ti invitiamo a riprovare e, in caso il problema si ripresentasse, a contattarci all'indirizzo <a href=\"sertot@mvcongressi.it\">sertot@mvcongressi.it</a>."
                    , Request.QueryString["tipo"].ToString().Contains("ordinario") ? "da socio ordinario"
                    : Request.QueryString["tipo"].ToString().Contains("aggregato") ? "da socio aggregato" : string.Empty);
            }
        }


        /*****************************************************/
        /**************** INIZIO SEO *************************/
        /*****************************************************/
        HtmlMeta description = new HtmlMeta();
        HtmlMeta keywords = new HtmlMeta();
        description.Name = "description";
        keywords.Name = "keywords";
        Page.Title = titolo + " | " + AresConfig.NomeSito;
        description.Content = titolo;
        keywords.Content = titolo;
        HtmlHead head = Page.Header;
        head.Controls.Add(description);
        head.Controls.Add(keywords);
        /*****************************************************/
        /**************** FINE SEO ***************************/
        /*****************************************************/
    }
}