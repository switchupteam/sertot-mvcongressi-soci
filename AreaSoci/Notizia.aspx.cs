﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class AreaSoci_Notizia : System.Web.UI.Page
{
    public string titolo = string.Empty;
    public string titoloNotizia = string.Empty;

    public string titoloNews = string.Empty;
    //public string contenuto1 = string.Empty;
    //public string contenuto2 = string.Empty;
    public string urlDocumento = string.Empty;
    public string testoDocumento = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (new SertotUtility().IsCurrentUserAutenticated())
        //    Response.Redirect("/Login/Login.aspx?ReturnUrl=/AreaSoci/Notizia.aspx");

        if (Request.QueryString["XRI"] != null)
        {
            titolo = "Notizie riservate ai Soci";
            
            Page.Title = "Notizie riservate ai Soci - Sertot";

            string strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(strConnessione))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                string sqlQuery = @"SELECT [ID1Publisher], [Titolo], [TagDescription], [TagKeywords], [Contenuto1], [Contenuto2], TitoloBrowser, Allegato1_Selettore, Allegato1_File, Allegato1_UrlEsterno, Allegato1_Desc
                              FROM [vwPublisher_Public_DtlArea1]
                              WHERE [ID1Publisher] = @ID1Publisher";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID1Publisher", System.Data.SqlDbType.Int);
                command.Parameters["@ID1Publisher"].Value = Request.QueryString["XRI"];

                SqlDataReader reader = command.ExecuteReader();

                if (reader.Read())
                {
                    titoloNews = reader["Titolo"].ToString();
                    titolo = titoloNews;
                    titoloNotizia = titolo;
                    Contenuto1_Label.Text = reader["Contenuto1"].ToString();
                    Contenuto2_Label.Text = reader["Contenuto2"].ToString();

                    if (!string.IsNullOrWhiteSpace(reader["Allegato1_Selettore"].ToString()))
                    {
                        if (reader["Allegato1_Selettore"].ToString().Equals("FIL") && !string.IsNullOrWhiteSpace(reader["Allegato1_File"].ToString()))
                        {
                            DocumentoPlaceholder.Visible = true;

                            urlDocumento = "/ZeusInc/Publisher/Documents/" + reader["Allegato1_File"].ToString();
                            testoDocumento = !string.IsNullOrWhiteSpace(reader["Allegato1_Desc"].ToString()) ? reader["Allegato1_Desc"].ToString() : "Scarica il documento";
                        }
                        else if (reader["Allegato1_Selettore"].ToString().Equals("LNK") && !string.IsNullOrWhiteSpace(reader["Allegato1_UrlEsterno"].ToString()))
                        {
                            DocumentoPlaceholder.Visible = true;

                            urlDocumento = reader["Allegato1_UrlEsterno"].ToString();
                            testoDocumento = !string.IsNullOrWhiteSpace(reader["Allegato1_Desc"].ToString()) ? reader["Allegato1_Desc"].ToString() : "Vai al sito";
                        }
                    }


                    /*****************************************************/
                    /**************** INIZIO SEO *************************/
                    /*****************************************************/
                    HtmlMeta description = new HtmlMeta();
                    HtmlMeta keywords = new HtmlMeta();
                    description.Name = "description";
                    keywords.Name = "keywords";
                    Page.Title = reader["TitoloBrowser"].ToString() + " | " + AresConfig.NomeSito;
                    description.Content = reader["TagDescription"].ToString();
                    keywords.Content = reader["TagKeywords"].ToString();
                    HtmlHead head = Page.Header;
                    head.Controls.Add(description);
                    head.Controls.Add(keywords);
                    /*****************************************************/
                    /**************** FINE SEO ***************************/
                    /*****************************************************/

                    reader.Close();
                    connection.Close();
                }
                else
                {
                    reader.Close();
                    connection.Close();
                    Response.Redirect("~/System/Message.aspx?0");
                }
            }
        }
    }

    protected void dsNotizia_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {
        if ((e.AffectedRows <= 0)
            || (e.Exception != null))
            Response.Redirect("~/System/Message.aspx?SelectErr");

        Page.Title = "Notizie riservate ai Soci - Sertot";

    }//fine dsNotizia_Selected

    protected void TagDescription_DataBinding(object sender, EventArgs e)
    {
        HiddenField hfTagDescription = (HiddenField)sender;

        HtmlMeta hm = new HtmlMeta();
        hm.Name = "Description";
        hm.Content = hfTagDescription.Value;
        Page.Header.Controls.Add(hm);

    }// fine hfTagDescription_DataBinding


    protected void TagKeywords_DataBinding(object sender, EventArgs e)
    {
        HiddenField hfTagKeywords = (HiddenField)sender;

        HtmlMeta hm = new HtmlMeta();
        hm.Name = "Keywords";
        hm.Content = hfTagKeywords.Value;
        Page.Header.Controls.Add(hm);

    }// fine hfTagKeywords_DataBinding

    protected void Titolo_DataBinding(object sender, EventArgs e)
    {
        HiddenField hfTitolo = (HiddenField)sender;
        titolo = "Notizie riservate ai Soci";
        titoloNotizia = hfTitolo.Value;
    }

    protected void Contenuto2_DataBinding(object sender, EventArgs e)
    {
        HiddenField hfContenuto2 = (HiddenField)sender;
        Contenuto2_Label.Text = hfContenuto2.Value;
    }

    protected void TitoloBrowserHidenField_DataBinding(object sender, EventArgs e)
    {
        //HiddenField TitoloBrowserHidenField = (HiddenField)sender;
        //Page.Title = TitoloBrowserHidenField.Value + " - Sertot";
    }

}