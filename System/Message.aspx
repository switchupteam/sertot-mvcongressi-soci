﻿<%@ Page Language="C#" MasterPageFile="~/Master/LayoutMain.master" CodeFile="Message.aspx.cs" Inherits="System_Message" %>

<asp:Content runat="Server" ID="Content1" ContentPlaceHolderID="head">
    <meta name="robots" content="noindex,nofollow" />
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="AreaTitle">    
</asp:Content>
<asp:Content runat="server" ID="Content3" ContentPlaceHolderID="AreaContentMain">

    <section class="systemMessage">
        <div class="container">       
            <h2><%= titolo %></h2>
            <hr />

            <p><%= testo %></p>

            <asp:PlaceHolder runat="server" ID="BackPlaceHolder" Visible="false">
                <a href="#" onclick="window.history.back();"><%= linkBackDesc %></a>
            </asp:PlaceHolder>

            <asp:PlaceHolder runat="server" ID="LinkStatic1PlaceHolder" Visible="false">
                <a href="<%= linkStatic1Url %>"><%= linkStatic1Desc %></a>
            </asp:PlaceHolder>

            <asp:PlaceHolder runat="server" ID="LinkStatic2PlaceHolder" Visible="false">
                <a href="<%= linkStatic2Url %>"><%= linkStatic2Desc %></a>
            </asp:PlaceHolder>           
         
        </div>
    </section>

</asp:Content>
