﻿using System;
using System.Web.UI.HtmlControls;
using System.Xml;

public partial class System_Message : System.Web.UI.Page
{
    protected string titolo = string.Empty;
    protected string testo = string.Empty;
    protected string linkBackDesc = string.Empty;
    protected string linkStatic1Desc = string.Empty;
    protected string linkStatic1Url = string.Empty;
    protected string linkStatic2Desc = string.Empty;
    protected string linkStatic2Url = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        AresUtilities aresUtil = new AresUtilities();
        string lang = aresUtil.GetLangRewritted();

        string filePath = Server.MapPath("~/App_Data/SystemMessagePublic.xml");
        string xmlPath = "/SystemMessage/Tipologia/Messaggio[@id='Generico']/Lang[@id='" + lang.Replace("'", "&apos;") + "']/";

        if (!string.IsNullOrWhiteSpace(Request.QueryString["Msg"]))
            xmlPath = "/SystemMessage/Tipologia/Messaggio[@id='" + Request.QueryString["Msg"].ToString().Replace("'", "&apos;") + "']/Lang[@id='" + lang.Replace("'", "&apos;") + "']/";       

        XmlDocument doc = new XmlDocument();
        doc.Load(filePath);

        XmlNode titoloNode = doc.DocumentElement.SelectSingleNode(xmlPath + "Titolo");
        if (titoloNode != null)
        {
            titolo = titoloNode.InnerText;
        }

        XmlNode testoNode = doc.DocumentElement.SelectSingleNode(xmlPath + "Testo");
        if (testoNode != null)
        {
            testo = testoNode.InnerText;
        }

        XmlNode linkBackDescNode = doc.DocumentElement.SelectSingleNode(xmlPath + "LinkBackDesc");
        if (linkBackDescNode != null && linkBackDescNode.InnerText.Length > 0)
        {
            linkBackDesc = linkBackDescNode.InnerText;
            BackPlaceHolder.Visible = true;
        }

        XmlNode linkStatic1DescNode = doc.DocumentElement.SelectSingleNode(xmlPath + "LinkStatic1Desc");
        if (linkStatic1DescNode != null && linkStatic1DescNode.InnerText.Length > 0)
        {
            linkStatic1Desc = linkStatic1DescNode.InnerText;
            LinkStatic1PlaceHolder.Visible = true;

            XmlNode linkStatic1UrlNode = doc.DocumentElement.SelectSingleNode(xmlPath + "LinkStatic1Url");
            if (linkStatic1UrlNode != null && linkStatic1UrlNode.InnerText.Length > 0)
            {
                linkStatic1Url = linkStatic1UrlNode.InnerText;
            }
            else
            {
                linkStatic1Url = "/";
            }
        }        

        XmlNode linkStatic2DescNode = doc.DocumentElement.SelectSingleNode(xmlPath + "LinkStatic2Desc");
        if (linkStatic2DescNode != null && linkStatic2DescNode.InnerText.Length > 0)
        {
            linkStatic2Desc = linkStatic2DescNode.InnerText;
            LinkStatic2PlaceHolder.Visible = true;

            XmlNode linkStatic2UrlNode = doc.DocumentElement.SelectSingleNode(xmlPath + "LinkStatic2Url");
            if (linkStatic2UrlNode != null && linkStatic2UrlNode.InnerText.Length > 0)
            {
                linkStatic2Url = linkStatic2UrlNode.InnerText;
            }
            else
            {
                linkStatic2Url = "/";
            }
        }

        /*****************************************************/
        /**************** INIZIO SEO *************************/
        /*****************************************************/
        HtmlMeta description = new HtmlMeta();
        HtmlMeta keywords = new HtmlMeta();
        description.Name = "description";
        keywords.Name = "keywords";
        Page.Title = titolo + " | " + AresConfig.NomeSito;
        description.Content = "";
        keywords.Content = "";
        HtmlHead head = (HtmlHead)Page.Header;
        head.Controls.Add(description);
        head.Controls.Add(keywords);
        /*****************************************************/
        /**************** FINE SEO ***************************/
        /*****************************************************/
    }
}