﻿<%@ Page Language="C#" MasterPageFile="~/Master/LayoutMain.master" AutoEventWireup="true" Async="true" CodeFile="Utente.aspx.cs" Inherits="Registrazione_Utente" %>
<%@ Register Src="~/UserControl/PayPal.ascx" TagName="PayPal" TagPrefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="AreaTitle" runat="Server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="AreaContentMain" runat="Server">
    <section class="registrazione">
        <div class="container">
            <h2 class="titolo">
                <asp:Literal ID="litTitle" runat="server" Text="Associarsi"></asp:Literal></h2>
            <hr />

            <asp:Panel ID="Step1Panel" runat="server">
                <div class="panel1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="boxIscriviti">
                                <h4 class="titolo">Iscriviti</h4>
                                <hr />

                                <table width="100%" border="0" cellspacing="0" cellpadding="3">
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label8" runat="server" CssClass="HIC_LabelFieldDescription1">Indirizzo E-Mail *</asp:Label>

                                            <asp:TextBox ID="EmailTextBox" runat="server" CssClass="HIC_TextBox1" Columns="50"
                                                MaxLength="100"></asp:TextBox>

                                            <div class="linkButton">
                                                <asp:Button ID="ProcediButton" runat="server" Text="Procedi" CssClass="HIC_Button1"
                                                    OnClick="ProcediButton_Click" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right"></td>
                                        <td align="left">
                                            <asp:RequiredFieldValidator Display="Dynamic" CssClass="HIC_Validator1" ID="UserNameRequired"
                                                runat="server" ControlToValidate="EmailTextBox" ErrorMessage="Obbligatorio"> </asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="MailRegularExpressionValidator" runat="server"
                                                CssClass="HIC_Validator1" ErrorMessage="Indirizzo email non valido" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                ControlToValidate="EmailTextBox" Display="Dynamic"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="left">
                                            <br />
                                            <p>Per procedere all'iscrizione inserisci il tuo indirizzo email, in questo modo verrai indirizzato al form di iscrizione dove dovrai inserire i tuoi dati personali e potrai scegliere la modalità di pagamento</p>
                                            <br />                                            
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <br />                                
                            </div>
                        </div>
                    </div>

                    <div class="richText">
                        <%= webComponentIscrizioneRinnovo %>
                    </div>
                </div>
            </asp:Panel>

            <asp:Panel ID="ExistUserPanel" runat="server" Visible="false">
                <br />
                <div class="TFY_Testo1" align="left">
                    <p>
                        L’indirizzo email
                        <asp:Label ID="EmailLabel" runat="server"></asp:Label>
                        è già presente.
                    </p>                    
                </div>
            </asp:Panel>

            <asp:Panel ID="ExistUserNamePanel" runat="server" Visible="false">
                <br />
                <div class="TFY_Testo1" align="left">
                    <p>
                        L'utente che si sta cercando di creare è già presente.                        
                    </p>                    
                </div>
            </asp:Panel>



            <asp:Panel ID="Step2Panel" runat="server" Visible="false">
                <div class="row">
                <div class="panel2 col-md-7">
                    <div class="TFY_Titolo1">
                        <h6>
                            <asp:Label ID="Utente_Label" runat="server" Text="Richiesta di associazione"></asp:Label></h6>
                    </div>
                    <br />
                    <table cellpadding="3" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td align="right">
                                <asp:Label CssClass="HIC_LabelFieldDescription1" ID="Label15" runat="server" Text="Titolo *"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox CssClass="HIC_TextBox1" ID="ProfessioneTextBox" runat="server" Columns="50"
                                    MaxLength="50"></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="HIC_Validator1" Display="Dynamic" ID="RequiredFieldValidator5"
                                    runat="server" ControlToValidate="ProfessioneTextBox" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label CssClass="HIC_LabelFieldDescription1" ID="Nome_Label" runat="server" Text="Nome *"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox CssClass="HIC_TextBox1" ID="NomeTextBox" runat="server" Columns="50"
                                    MaxLength="50"></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="HIC_Validator1" Display="Dynamic" ID="RequiredFieldValidator2"
                                    runat="server" ControlToValidate="NomeTextBox" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label CssClass="HIC_LabelFieldDescription1" ID="Cognome_Label" runat="server"
                                    Text="Cognome *"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox CssClass="HIC_TextBox1" ID="CognomeTextBox" runat="server" Columns="50"
                                    MaxLength="50"></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="HIC_Validator1" Display="Dynamic" ID="RequiredFieldValidator1"
                                    runat="server" ControlToValidate="CognomeTextBox" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                        <tr>
                            <td align="right">
                                <asp:Label CssClass="HIC_LabelFieldDescription1" ID="Label2" runat="server" Text="Indirizzo *"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox CssClass="HIC_TextBox1" ID="IndirizzoTextBox" runat="server" Columns="50"
                                    MaxLength="50"></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="HIC_Validator1" Display="Dynamic" ID="RequiredFieldValidator7"
                                    runat="server" ControlToValidate="IndirizzoTextBox" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label CssClass="HIC_LabelFieldDescription1" ID="Label3" runat="server" Text="Citta *"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox CssClass="HIC_TextBox1" ID="CittaTextBox1" runat="server" Columns="50"
                                    MaxLength="50"></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="HIC_Validator1" Display="Dynamic" ID="RequiredFieldValidator8"
                                    runat="server" ControlToValidate="CittaTextBox1" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                        <tr>
                            <td align="right">
                                <asp:Label CssClass="HIC_LabelFieldDescription1" ID="Label6" runat="server" Text="CAP *"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox CssClass="HIC_TextBox1" ID="CAPTextBox" runat="server" Columns="5"
                                    MaxLength="5"></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="HIC_Validator1" Display="Dynamic" ID="RequiredFieldValidator4"
                                    runat="server" ControlToValidate="CAPTextBox" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                        <tr>
                            <td align="right">
                                <asp:Label CssClass="HIC_LabelFieldDescription1" ID="Label5" runat="server" Text="Provincia *"></asp:Label>
                            </td>
                            <td align="left">
                                <%--<asp:TextBox CssClass="HIC_TextBox1" ID="ProvinciaEstesaTextBox" runat="server" Columns="50"
                        MaxLength="50"></asp:TextBox>--%>
                                <%--<asp:RequiredFieldValidator CssClass="HIC_Validator1" Display="Dynamic" ID="RequiredFieldValidator6"
                        runat="server" ControlToValidate="ProvinciaEstesaTextBox" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>--%>
                                <%--<img src="../SiteImg/Spc.gif" width="10px" />--%>
                                <%--<asp:Label CssClass="HIC_LabelFieldDescription1" ID="Label6" runat="server" Text="Provincia *"></asp:Label>--%>
                                
                                <%--<asp:TextBox CssClass="HIC_TextBox1" ID="ProvinciaSiglaTextBox" runat="server" Columns="3"
                                    MaxLength="2"></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="HIC_Validator1" Display="Dynamic" ID="RequiredFieldValidator17"
                                    runat="server" ControlToValidate="ProvinciaSiglaTextBox" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>--%>

                                <asp:DropDownList ID="ProvinciaPersonaleDropdown" runat="server" DataSourceID="dsProvincie1"
                                    DataTextField="Provincia" DataValueField="Sigla" AppendDataBoundItems="True">
                                    <asp:ListItem Selected="True" Value="0">&gt; Seleziona</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ErrorMessage="Obbligatorio"
                                    runat="server" ControlToValidate="ProvinciaPersonaleDropdown" CssClass="HIC_Validator1"
                                    Display="Dynamic" InitialValue="0">
                                </asp:RequiredFieldValidator>
                                <asp:SqlDataSource ID="dsProvincie1" runat="server"
                                    ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                    SelectCommand="SELECT [Provincia],[Sigla] FROM [MVCongressi-sertot].[dbo].[tbProvince] ORDER BY Provincia"></asp:SqlDataSource>

                            </td>
                        </tr>

                        <tr>
                            <td align="right">
                                <asp:Label CssClass="HIC_LabelFieldDescription1" ID="Label4" runat="server" Text="Telefono *"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox CssClass="HIC_TextBox1" ID="TelefonoTextBox1" runat="server" Columns="20"
                                    MaxLength="20"></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="HIC_Validator1" Display="Dynamic" ID="RequiredFieldValidator9"
                                    runat="server" ControlToValidate="TelefonoTextBox1" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label CssClass="HIC_LabelFieldDescription1" ID="Label12" runat="server" Text="Cellulare"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox CssClass="HIC_TextBox1" ID="CellulareTextBox1" runat="server" Columns="20"
                                    MaxLength="20"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label CssClass="HIC_LabelFieldDescription1" ID="Label16" runat="server" Text="Fax"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox CssClass="HIC_TextBox1" ID="FaxTextBox1" runat="server" Columns="20"
                                    MaxLength="20"></asp:TextBox>
                            </td>
                        </tr>

                        <tr>
                            <td align="right">
                                <asp:Label CssClass="HIC_LabelFieldDescription1" ID="Label22" runat="server" Text="P.IVA"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox CssClass="HIC_TextBox1" ID="PIVATextBox" runat="server" Columns="50"
                                    MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>

                        <%--<tr style="display:none;">
                        <td align="right">
                            <asp:Label CssClass="HIC_LabelFieldDescription1" ID="Label7" runat="server" Text="Nazione *"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="NazioneDropDownList" runat="server" DataSourceID="dsNazioni"
                                CssClass="HIC_Dropdown1" DataTextField="Nazione" DataValueField="ID1Nazione">
                                <asp:ListItem Text="&gt;  Seleziona" Selected="True" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="dsNazioni" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                SelectCommand="SELECT [ID1Nazione], [Nazione] FROM [tbNazioni] ORDER BY [Nazione]">
                            </asp:SqlDataSource>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" ErrorMessage="Obbligatorio"
                                runat="server" ControlToValidate="NazioneDropDownList" CssClass="HIC_Validator1"
                                Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
                        </td>
                    </tr>--%>
                        <tr>
                            <td align="right">
                                <asp:Label CssClass="HIC_LabelFieldDescription1" ID="Label1" runat="server" Text="E-Mail"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:Label ID="EmailInfoLabel" runat="server" CssClass="HIC_LabelFieldValue1"></asp:Label>
                            </td>
                        </tr>

                        <tr>
                            <td align="right">
                                <asp:Label CssClass="HIC_LabelFieldDescription1" ID="Label17" runat="server" Text="Codice Fiscale *"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox CssClass="HIC_TextBox1" ID="CodiceFiscaleTextBox1" runat="server" Columns="20"
                                    MaxLength="16"></asp:TextBox>
                            </td>
                        </tr>

                        <tr>
                            <td align="right">
                                <asp:Label CssClass="HIC_LabelFieldDescription1" ID="Label18" runat="server" Text="Citta di nascita *"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox CssClass="HIC_TextBox1" ID="CittaDiNascitaTextBox1" runat="server" Columns="20"
                                    MaxLength="20"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label CssClass="HIC_LabelFieldDescription1" ID="Label21" runat="server" Text="Provincia di nascita *"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox CssClass="HIC_TextBox1" ID="ProvinciaDiNascitaTextBox1" runat="server" Columns="20"
                                    MaxLength="2"></asp:TextBox>
                            </td>
                        </tr>


                        <tr>
                            <td align="right">
                                <asp:Label CssClass="HIC_LabelFieldDescription1" ID="Label19" runat="server" Text="Data di nascita (GG-MM-AAAA) *"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox CssClass="HIC_TextBox1" ID="DataDiNascitaTextBox1" runat="server" Columns="20"
                                    MaxLength="20"></asp:TextBox>
                            </td>
                        </tr>

                        <tr>
                            <td></td>
                            <td>
                                <p>Dichiara di essere iscritto all'Ordine dei Medici</p>
                            </td>
                        </tr>


                        <tr>
                            <td align="right">
                                <asp:Label CssClass="HIC_LabelFieldDescription1" ID="Label20" runat="server" Text="Provincia di *"></asp:Label>
                            </td>
                            <td align="left">
                                <%--<asp:TextBox CssClass="HIC_TextBox1" ID="ProvinciaEstesaTextBox" runat="server" Columns="50"
                        MaxLength="50"></asp:TextBox>--%>
                                <%--<asp:RequiredFieldValidator CssClass="HIC_Validator1" Display="Dynamic" ID="RequiredFieldValidator6"
                        runat="server" ControlToValidate="ProvinciaEstesaTextBox" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>--%>
                                <%--<img src="../SiteImg/Spc.gif" width="10px" />--%>
                                <%--<asp:Label CssClass="HIC_LabelFieldDescription1" ID="Label6" runat="server" Text="Provincia *"></asp:Label>--%>

                             <%--   <asp:TextBox CssClass="HIC_TextBox1" ID="ProvinciaTextBox" runat="server" Columns="3"
                                    MaxLength="2"></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="HIC_Validator1" Display="Dynamic" ID="RequiredFieldValidator6"
                                    runat="server" ControlToValidate="ProvinciaTextBox" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>--%>

                                <asp:DropDownList ID="ProvinciaDropdown" runat="server" DataSourceID="dsProvincie"
                                    DataTextField="Provincia" DataValueField="Sigla" AppendDataBoundItems="True">
                                    <asp:ListItem Selected="True" Value="0">&gt; Seleziona</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ErrorMessage="Obbligatorio"
                                    runat="server" ControlToValidate="ProvinciaDropdown" CssClass="HIC_Validator1"
                                    Display="Dynamic" InitialValue="0">
                                </asp:RequiredFieldValidator>
                                <asp:SqlDataSource ID="dsProvincie" runat="server"
                                    ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                    SelectCommand="SELECT [Provincia],[Sigla] FROM [MVCongressi-sertot].[dbo].[tbProvince] ORDER BY Provincia"></asp:SqlDataSource>

                            </td>
                        </tr>


                        
                        <tr>
                            <td></td>
                            <td>
                                <p>* Dichiara di accettare lo Statuto e il Regolamento dell'Associazione</p>
                                <br />
                            </td>
                        </tr>


                        <tr>
                            <td align="right">
                                <asp:Label CssClass="HIC_LabelFieldDescription1" ID="PasswordLabel22" runat="server"
                                    AssociatedControlID="Password">Password * </asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="Password" runat="server" TextMode="Password" Columns="50" MaxLength="50"
                                    CssClass="HIC_TextBox1"></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" CssClass="HIC_Validator1" ID="PasswordRequired"
                                    runat="server" ControlToValidate="Password" ToolTip="La password è obbligatoria."
                                    ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                                <asp:CompareValidator CssClass="HIC_Validator1" ID="PasswordCompare" runat="server"
                                    ControlToCompare="Password" ControlToValidate="ConfirmPassword" Display="Dynamic"
                                    ErrorMessage="Password e conferma password non coincidono"></asp:CompareValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label CssClass="HIC_LabelFieldDescription1" ID="ConfirmPasswordLabel" runat="server"
                                    AssociatedControlID="ConfirmPassword">Conferma password * </asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password" Columns="50"
                                    MaxLength="50" CssClass="HIC_TextBox1"></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="ConfirmPasswordRequired" CssClass="HIC_Validator1"
                                    runat="server" ControlToValidate="ConfirmPassword" ToolTip="La password di conferma è obbligatoria."
                                    ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                        <tr>
                            <td align="left" colspan="2">
                                <h6><span class="TFY_Titolo2">Iscrizione annuale e pagamento</span></h6>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="Label11" CssClass="HIC_LabelFieldDescription1" runat="server" Text="Label">Quota *</asp:Label>
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ID2QuotaCostoDropDownList" runat="server" DataSourceID="dsQuotaCosto"
                                    DataTextField="DescPrezzo" DataValueField="IDQuota" AppendDataBoundItems="True">
                                    <asp:ListItem Selected="True" Value="0">&gt; Seleziona</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="ID2QuotaCostoRequiredFieldValidator" ErrorMessage="Obbligatorio"
                                    runat="server" ControlToValidate="ID2QuotaCostoDropDownList" CssClass="HIC_Validator1"
                                    Display="Dynamic" InitialValue="0">
                                </asp:RequiredFieldValidator>
                                <asp:SqlDataSource ID="dsQuotaCosto" runat="server"
                                    ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                    SelectCommand="
                                    SELECT [Descrizione], [IDQuota] , [Descrizione]+' - '+ CONVERT(varchar(10),[QuotaTotale]) + ' €' AS [DescPrezzo] 
                                    FROM [vwQuote_Lst] WHERE ZeusIsAlive = 1"></asp:SqlDataSource>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="top">
                                <asp:Label ID="Label10" CssClass="HIC_LabelFieldDescription1" runat="server" Text="Label">Metodo di pagamento *</asp:Label>
                            </td>
                            <td align="left" class="labelDisplayInline">
                                <asp:RadioButtonList runat="server" ID="PagamentoRadioButtonList" CssClass="paymentMethods" DataSourceID="PagamentoSqlDataSource" 
                                    DataTextField="LabelPagamentoFull" DataValueField="ID1MetodoPagamento" AppendDataBoundItems="True">                                    
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Obbligatorio"
                                    runat="server" ControlToValidate="PagamentoRadioButtonList" CssClass="HIC_Validator1"
                                    Display="Dynamic" InitialValue="0">
                                </asp:RequiredFieldValidator>

                                <%--<asp:DropDownList ID="PagamentoDropDownList" runat="server" DataSourceID="PagamentoSqlDataSource"
                                    DataTextField="MetodoPagamento" DataValueField="ID1MetodoPagamento" AppendDataBoundItems="True">
                                    <asp:ListItem Selected="True" Value="0">&gt; Seleziona</asp:ListItem>
                                </asp:DropDownList>--%>
<%--                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Obbligatorio"
                                    runat="server" ControlToValidate="PagamentoDropDownList" CssClass="HIC_Validator1"
                                    Display="Dynamic" InitialValue="0">
                                </asp:RequiredFieldValidator>--%>
                                <asp:SqlDataSource ID="PagamentoSqlDataSource" runat="server"
                                    ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                    SelectCommand="SELECT [MetodoPagamento], [ID1MetodoPagamento], [PubOrderSoci], 
	                                                    CASE ID1MetodoPagamento 
		                                                    WHEN 2 THEN 'Intestato a: SERTOT <br />
                                                                         Banca: CARIPARMA - AG. 1 PARMA <br />
                                                                         IBAN: IT67 G 06230 12701 000084208891 <br />
                                                                         Causale: Nome e Cognome + Quota associativa sertot + Anno'
		                                                    ELSE 'Riceverai il link per il pagamento con PayPal nell''e-mail di riepilogo una volta compilato questa form'
	                                                    END AS LabelPagamento
	                                                    , '<strong>' + [MetodoPagamento] + '</strong><br/>' + CASE ID1MetodoPagamento 
		                                                    WHEN 2 THEN 'Intestato a: SERTOT <br />
                                                                         Banca: CARIPARMA - AG. 1 PARMA <br />
                                                                         IBAN: IT67 G 06230 12701 000084208891 <br />
                                                                         Causale: Nome e Cognome + Quota associativa sertot + Anno'
		                                                    ELSE 'Riceverai il link per il pagamento con PayPal nell''e-mail di riepilogo una volta compilato questa form'
	                                                    END AS LabelPagamentoFull
                                                    FROM [tbMetodiPagamento] WHERE PubOrderSoci > 0 ORDER BY [PubOrderSoci]"></asp:SqlDataSource>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label CssClass="HIC_LabelFieldDescription1" ID="Label13" runat="server" Text="Note pagamento / Dati per intestazione ricevuta"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox CssClass="Multiline " ID="NotePagamentoTextBox" runat="server" TextMode="MultiLine" Rows="5" Columns="50"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </div>
                    <div class="col-md-5">
      <%--                      <div class="boxRinnova">
                                <h4 class="titolo">Rinnova la tua iscrizione</h4>
                                <hr />

                                <p>Se sei già iscritto è sufficiente accedere all’Area Soci e rinnovare la quota annuale.</p>
                                <br />

                                <div class="link">
                                    <a href="/AreaSoci/">Area Soci</a>
                                </div>
                            </div>--%>
                                                            <h4 class="titolo">Pagamento della quota associativa</h4>
                                <hr />

                            <uc1:PayPal ID="PayPal1" runat="server" />

                        </div>
                    <div class="col-md-12">
                        <table cellpadding="3" cellspacing="0" border="0" width="100%">
                    <tr>
                            <td colspan="2">
                                <br />
                                <div class="richText small">
                                    <%= webComponentIscrizioneOnline %>
                                </div>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label CssClass="HIC_LabelFieldDescription1" ID="Label7" runat="server"
                                    AssociatedControlID="PrivacyTextBox">Consenti * </asp:Label>
                            </td>
                            <td align="left">
                                <asp:CheckBox ID="PrivacyTextBox" runat="server" CssClass="PrivacyTextBox"></asp:CheckBox>
                                <asp:CustomValidator runat="server" ID="CheckBoxRequired" EnableClientScript="true"
                                    OnServerValidate="CheckBoxRequired_ServerValidate"
                                    ClientValidationFunction="CheckBoxRequired_ClientValidate" CssClass="HIC_Validator1">Accettazione sulla clausola della Privacy obbligatoria</asp:CustomValidator>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                <div class="richText small">
                                    <p>
                                        Consenso all’inserimento nel sito web della Sertot dei dati personali (Nome, Cognome, Città, E-mail)
                                    </p>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label CssClass="HIC_LabelFieldDescription1" ID="Label9" runat="server"
                                    AssociatedControlID="NewsCheckBox">Consenti</asp:Label>
                            </td>
                            <td align="left">
                                <asp:CheckBox ID="NewsCheckBox" runat="server"></asp:CheckBox>
                            </td>
                        </tr>
                            <tr>
                            <td align="center" colspan="2">
                                <asp:Label CssClass="HIC_LabelValidazione1" ID="FirstStepErrorLabel" runat="server"
                                    Text="ATTENZIONE: l'utente non è stato creato correttamente.<br />" Visible="false" />
                                <div class="linkButton">
                                    <asp:Button ID="CreateButton" runat="server" Text="Invia richiesta"
                                        OnClick="CreateButton_Click" CausesValidation="true" />
                                </div>
                            </td>
                        </tr>
                                </table>
                        </div>
                    </div>
            </asp:Panel>

            <asp:Panel ID="Step3Panel" runat="server" Visible="false">
                <div class="panel3">
                    <div class="richText">
                        L’utente è stato registrato con successo. E’ stata inviata un messaggio e-mail di promemoria all’indirizzo associato.
                        <br />
                        <br />
                        Dati account:<br />
                        <br />
                        UserName:
                        <asp:Label ID="UserNameLabel" runat="server"></asp:Label><br />
                        Password:
                        <asp:Label ID="PasswordLabel" runat="server"></asp:Label><br />
                        <br />
                        Indirizzo E-Mail per recupero password:
                        <asp:HyperLink ID="EmailHyperLink" runat="server"></asp:HyperLink><br />
                        <br />
                        Importo quota: 
                        <asp:Label ID="QuotaLabel" runat="server"></asp:Label><br />
                        <br />
                        L'accesso all'area riservata sarà disponibile dopo l'approvazione dell'account.
                    <%--Puoi aggiornare in qualunque momento il tuo profilo utente, la password e l’indirizzo
                    E-Mail nell’area riservata.<br />
                    <br />
                    <a href="/Login/">Accedi all’area riservata </a>
                    <br />--%>
                    </div>
                </div>
            </asp:Panel>
            <script type="text/javascript">
                function CheckBoxRequired_ClientValidate(sender, e) {
                    e.IsValid = jQuery(".PrivacyTextBox input:checkbox").is(':checked');
                }
            </script>
        </div>
            
    </section>

    <style type="text/css">
        .titolo {
            text-align: center !important;
        }

        .paymentMethods input[type='radio'] {
            margin-right: 10px;
        }
    </style>

</asp:Content>

