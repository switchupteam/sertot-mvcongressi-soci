﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Web.Security;
using System.Configuration;
using System.Net.Mail;
using System.Web.UI.WebControls;
using System.Transactions;

public partial class Registrazione_Utente : System.Web.UI.Page
{
    protected string lang = string.Empty;
    protected string webComponentIscrizioneRinnovo = string.Empty;
    protected string webComponentIscrizioneOnline = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        AresUtilities aresUtil = new AresUtilities();
        lang = aresUtil.GetLangRewritted();


        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        using (SqlConnection connection = new SqlConnection(strConnessione))
        {
            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                string sqlQuery = @"SELECT Contenuto1
                                    FROM tbWebComponent
                                    WHERE ZeusIsAlive = 1 AND ZeusLangCode = @ZeusLangCode AND ZeusId = @ZeusId";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.VarChar);
                command.Parameters["@ZeusLangCode"].Value = lang;
                command.Parameters.Add("@ZeusId", System.Data.SqlDbType.VarChar);
                command.Parameters["@ZeusId"].Value = "57bd0dbf-9570-4eb4-bc2b-c55cb7e04f0d";

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                    webComponentIscrizioneRinnovo = reader["Contenuto1"].ToString();

                reader.Close();
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
                return;
            }
        }



        using (SqlConnection connection = new SqlConnection(strConnessione))
        {
            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                string sqlQuery = @"SELECT Contenuto1
                                    FROM tbWebComponent
                                    WHERE ZeusIsAlive = 1 AND ZeusLangCode = @ZeusLangCode AND ZeusId = @ZeusId";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.VarChar);
                command.Parameters["@ZeusLangCode"].Value = lang;
                command.Parameters.Add("@ZeusId", System.Data.SqlDbType.VarChar);
                command.Parameters["@ZeusId"].Value = "99f8b981-85d1-46a8-b316-f7e3146759aa";

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                    webComponentIscrizioneOnline = reader["Contenuto1"].ToString();

                reader.Close();
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
                return;
            }
        }

    }


    private string GetLang()
    {
        try
        {
            delinea myDelinea = new delinea();
            string Lang = "ITA";

            if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
                if (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang"))
                    Lang = Request.QueryString["Lang"];

            return Lang;
        }
        catch (Exception)
        {
            return "ITA";
        }
    }//fine GetLang

    private string AccessoAreeRiservate(MembershipUser myUser)
    {
        if (myUser != null)
        {
            if (!myUser.IsApproved)
                return "negato";

            else if (!myUser.IsLockedOut)
                return "consentito";

            if ((!myUser.IsApproved) && (myUser.IsLockedOut))
                return "bloccato";


        }//fine if


        return string.Empty;
    }//fine AccessoAreeRiservate

    private bool SendMail(System.Web.Security.MembershipUserCollection AllUsers, string Email)
    {
        try
        {
            System.Net.Configuration.SmtpSection section = ConfigurationManager.GetSection("system.net/mailSettings/smtp") as System.Net.Configuration.SmtpSection;

            MailMessage mail = new MailMessage();

            // ------------- se il destinatario è LISTA allora spedisco all'elenco destinatati previsti
            //if (Email == "LISTA")
            //{
            //    mail.To.Add(new MailAddress("support@switchup.it"));                
            //}
            //else
            //{
            mail.To.Add(new MailAddress(Email));
            //}

            // Hop: 20170328 aggiunto per "log"
            //mail.Bcc.Add(new MailAddress("web@switchup.it"));

            mail.From = new MailAddress(section.From, "Sito Web Sertot");
            mail.Subject = "Invio richiesta di associazione Sertot";

            mail.IsBodyHtml = true;
            string myBody = string.Empty;

            switch (GetLang())
            {

                default:
                    myBody = Server.HtmlEncode("Caro socio,");
                    myBody += "<br />";
                    myBody += "Ti ringrazio per esserti iscritto alla nostra associazione.";
                    myBody += "<br />";
                    myBody += "<br />";
                    myBody += "Nei prossimi giorni riceverai una e-mail di conferma dell’approvazione della Tua richiesta.";
                    myBody += "<br />";
                    myBody += "A quel punto, per terminare la procedura, sarai invitato a versare la quota associativa annuale tramite bonifico o paypal.";
                    myBody += "<br />";
                    myBody += "<br />";
                    myBody += "Per qualsiasi necessità ci puoi contattare a questo indirizzo e-mail: <a href=\"mailto:sertot@mvcongressi.it\">sertot@mvcongressi.it</a>.";
                    myBody += "<br />";
                    myBody += "<br />";
                    myBody += "Nome: " + NomeTextBox.Text;
                    myBody += "<br />";
                    myBody += "Cognome: " + CognomeTextBox.Text;
                    myBody += "<br />";
                    myBody += "Email di registrazione: " + EmailTextBox.Text;
                    myBody += "<br />";
                    myBody += "<br />";
                    myBody += "<br />";

                    foreach (MembershipUser myUser in AllUsers)
                    {

                        try
                        {
                            myBody += "Indirizzo E-Mail associato: " + myUser.Email;
                            myBody += "<br />";
                            myBody += "Stato: In attesa di approvazione";
                            myBody += "<br />";
                            myBody += "<br />";
                        }
                        catch (System.Web.Security.MembershipPasswordException)
                        {
                        }

                    }//fine foreach

                    myBody += Server.HtmlEncode("Richiesta di associazione del ");
                    myBody += DateTime.Now.ToString("d");
                    myBody += " alle ore ";
                    myBody += DateTime.Now.Hour + "." + DateTime.Now.Minute + "." + DateTime.Now.Second;
                    myBody += Server.HtmlEncode(" inoltrata dall’indirizzo IP ");
                    myBody += Request.ServerVariables["REMOTE_ADDR"];
                    break;

            }//fine switch


            mail.Body = myBody;
            SmtpClient client = new SmtpClient();
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.Port = 587;
            client.EnableSsl = true;
            client.SendAsync(mail, string.Empty);

            return true;
        }
        catch (Exception p)
        {

            Response.Write(p.ToString());
            return false;
        }

    }//fine SendMail

    protected void ProcediButton_Click(object sender, EventArgs e)
    {
        Step1Panel.Visible = false;
        //TitoloPagina_Label.Text = "Iscrizione e rinnovo - Step 2";
        litTitle.Text = "Iscrizione nuovo Socio";

        if (Membership.FindUsersByEmail(EmailTextBox.Text).Count > 0)
        {
            EmailLabel.Text = EmailTextBox.Text;
            ExistUserPanel.Visible = true;


        }
        else
        {
            Step2Panel.Visible = true;
            EmailInfoLabel.Text = EmailTextBox.Text;
        }

    }//fine ProcediButton_Click

    private bool CreateUser()
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        //ANNO PROGRESSIVO
        //var annoProgressivo = string.Empty;
        string lastYear = new delinea().GetRenewYear(DateTime.Now.Year).ToString();
        //using (SqlConnection connection = new SqlConnection(strConnessione))
        //{
        //    try
        //    {
        //        delinea myDelinea = new delinea();
        //        //var lastYear = (myDelinea.GetLastRenewYear() + 1).ToString();

        //        connection.Open();
        //        SqlCommand command = connection.CreateCommand();

        //        command.CommandText = @"SELECT anno, AnnoProgressivo 
        //                                FROM tbQuoteAnnuali
        //                                WHERE AnnoProgressivo IS NOT NULL AND AnnoProgressivo != ''
        //                                    AND Anno = @Anno
        //                                ORDER BY Anno DESC, AnnoProgressivo DESC";
        //        command.Parameters.Clear();
        //        command.Parameters.AddWithValue("@Anno", lastYear);

        //        SqlDataReader reader = command.ExecuteReader();
        //        if (reader.Read())
        //        {
        //            if (!string.IsNullOrWhiteSpace(reader["AnnoProgressivo"].ToString()))
        //            {
        //                string oldAnnoProg = reader["AnnoProgressivo"].ToString();
        //                string[] oldAnnoProgSplit = oldAnnoProg.Split('/');

        //                string newAnnoProg = string.Empty;
        //                if (oldAnnoProgSplit[1].Equals(lastYear))
        //                {
        //                    string num = (Convert.ToInt32(oldAnnoProgSplit[0]) + 1).ToString();
        //                    string numResult = num.Count() == 1 ? "000" + num
        //                        : num.Count() == 2 ? "00" + num
        //                        : num.Count() == 3 ? "0" + num
        //                        : num;

        //                    newAnnoProg = string.Format("{0}/{1}", numResult, oldAnnoProgSplit[1]);
        //                }
        //                else
        //                    newAnnoProg = string.Format("{0}/{1}", "0001", lastYear);

        //                annoProgressivo = newAnnoProg;
        //            }
        //            else
        //                annoProgressivo = string.Format("{0}/{1}", "0001", lastYear);
        //        }

        //        reader.Close();
        //    }
        //    catch (Exception p)
        //    {
        //        Response.Write(p.ToString());
        //    }
        //}
        //FINE ANNO PROGRESSIVO




        SqlTransaction SQLTx = null;

        // Gestione della transazione per la membership (processo separato, indi transazione separata)
        using (TransactionScope txScope = new TransactionScope())
        {
            try
            {
                //SertotUtility c = new SertotUtility();
                //string nuovoCodiceSocio = c.NuovoCodiceSocio();
                string newMemberUserName = NomeTextBox.Text.ToString().Trim().ToLower() + "." + CognomeTextBox.Text.ToString().Trim().ToLower();
                UserNameLabel.Text = newMemberUserName;

                // Creazione nuovo utente
                MembershipCreateStatus status;
                MembershipUser newUser = Membership.CreateUser(newMemberUserName, Password.Text, EmailTextBox.Text, null, null, true, out status);

                // Eventuale gestione di errori, lanciando un'eccezione non passa dall'istruzione "txScope.Complete();" che esegue effettivamente il commit delle funzioni di membership
                if (newUser == null)
                {
                    ExistUserNamePanel.Visible = true;
                    return false;
                    //throw new Exception("Utente membership non creato");          // Utente membership non creato
                }

                newUser.IsApproved = true;
                newUser.UnlockUser();
                object userGUID = newUser.ProviderUserKey;


                // Se invece va tutto bene si prosegue con la gestione della transazione delle tabelle di "business"
                using (SqlConnection connection = new SqlConnection(strConnessione))
                {
                    connection.Open();
                    SqlCommand SQLCmd = connection.CreateCommand();

                    SQLTx = connection.BeginTransaction();
                    SQLCmd.Transaction = SQLTx;

                    SQLCmd.Parameters.Clear();
                    InsertProfiliPersonali(ref SQLCmd, userGUID, newMemberUserName);
                    SQLCmd.ExecuteNonQuery();

                    SQLCmd.Parameters.Clear();
                    InsertProfiliSocietari(ref SQLCmd, userGUID, newMemberUserName);
                    SQLCmd.ExecuteNonQuery();

                    SQLCmd.Parameters.Clear();
                    //InsertProfiliMarketing(ref SQLCmd, userGUID, newMemberUserName, nuovoCodiceSocio);
                    InsertProfiliMarketing(ref SQLCmd, userGUID, newMemberUserName);
                    SQLCmd.ExecuteNonQuery();

                    SQLCmd.Parameters.Clear();
                    InsertQuoteAnnuali(ref SQLCmd, userGUID, newMemberUserName, lastYear);
                    SQLCmd.ExecuteNonQuery();

                    SQLTx.Commit();
                }//fine using

                txScope.Complete();
            }
            catch (Exception ex)
            {
                if (SQLTx != null)
                    SQLTx.Rollback();

                throw;
            }
        }

        return true;
    }//fine CreateUser

    protected void InsertProfiliPersonali(ref SqlCommand SQLCmd, object userGUID, string userName)
    {
        string sqlQuery = string.Empty;
        object CreatorGUID = userGUID;

        sqlQuery = "SET DATEFORMAT dmy; INSERT INTO [tbProfiliPersonali]";
        sqlQuery += " ([UserId],[Cognome],[Nome],[CodiceFiscale],[Qualifica],[Professione]";
        sqlQuery += " ,[Sesso],[Indirizzo],[Numero],[Citta],[Comune],[Cap],[ProvinciaEstesa]";
        sqlQuery += " ,[ProvinciaSigla],[ID2Nazione],[NascitaData],[NascitaCitta],[NascitaProvinciaSigla]";
        sqlQuery += " ,[NascitaID2Nazione],[Telefono1],[Telefono2],[Fax],[Note],[UserMaster],[RecordNewUser]";
        sqlQuery += " ,[RecordNewDate]";
        sqlQuery += " ,[ZeusJolly1],[ZeusJolly2],[ImportCode])";
        sqlQuery += " VALUES ";
        sqlQuery += " (@UserId,@Cognome,@Nome,@CodiceFiscale,@Qualifica,@Professione";
        sqlQuery += " ,@Sesso,@Indirizzo,@Numero,@Citta,@Comune,@Cap,@ProvinciaEstesa";
        sqlQuery += " ,@ProvinciaSigla,@ID2Nazione,@NascitaData,@NascitaCitta,@NascitaProvinciaSigla";
        sqlQuery += " ,@NascitaID2Nazione,@Telefono1,@Telefono2,@Fax,@Note,@UserMaster,@RecordNewUser";
        sqlQuery += " ,@RecordNewDate";
        sqlQuery += " ,@ZeusJolly1,@ZeusJolly2,@ImportCode)";

        SQLCmd.CommandText = sqlQuery;
        SQLCmd.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
        SQLCmd.Parameters["@UserId"].Value = userGUID;

        SQLCmd.Parameters.Add("@Cognome", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@Cognome"].Value = CognomeTextBox.Text;

        SQLCmd.Parameters.Add("@Nome", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@Nome"].Value = NomeTextBox.Text;

        SQLCmd.Parameters.Add("@CodiceFiscale", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@CodiceFiscale"].Value = CodiceFiscaleTextBox1.Text.ToString();

        SQLCmd.Parameters.Add("@Qualifica", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@Qualifica"].Value = ProfessioneTextBox.Text;

        SQLCmd.Parameters.Add("@Professione", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@Professione"].Value = string.Empty;

        SQLCmd.Parameters.Add("@Sesso", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@Sesso"].Value = "X";

        SQLCmd.Parameters.Add("@Indirizzo", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@Indirizzo"].Value = IndirizzoTextBox.Text.ToString();

        SQLCmd.Parameters.Add("@Numero", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@Numero"].Value = string.Empty;

        SQLCmd.Parameters.Add("@Citta", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@Citta"].Value = CittaTextBox1.Text.ToString();

        SQLCmd.Parameters.Add("@Cap", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@Cap"].Value = CAPTextBox.Text.ToString();

        SQLCmd.Parameters.Add("@Comune", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@Comune"].Value = string.Empty;

        SQLCmd.Parameters.Add("@ProvinciaEstesa", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@ProvinciaEstesa"].Value = ProvinciaPersonaleDropdown.Text;

        SQLCmd.Parameters.Add("@ProvinciaSigla", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@ProvinciaSigla"].Value = ProvinciaPersonaleDropdown.SelectedValue;

        //SQLCmd.Parameters.Add("@ID2Nazione", System.Data.SqlDbType.Int);
        //SQLCmd.Parameters["@ID2Nazione"].Value = NazioneDropDownList.SelectedValue;

        var dataConvertita = Convert.ToDateTime((DataDiNascitaTextBox1.Text)).ToShortDateString();
        if (!string.IsNullOrWhiteSpace(dataConvertita))
        {
            SQLCmd.Parameters.Add("@NascitaData", System.Data.SqlDbType.SmallDateTime);
            SQLCmd.Parameters["@NascitaData"].Value = dataConvertita;
        }

        SQLCmd.Parameters.Add("@NascitaCitta", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@NascitaCitta"].Value = CittaDiNascitaTextBox1.Text.ToString();

        SQLCmd.Parameters.Add("@NascitaProvinciaSigla", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@NascitaProvinciaSigla"].Value = ProvinciaDiNascitaTextBox1.Text.ToString();

        SQLCmd.Parameters.Add("@NascitaID2Nazione", System.Data.SqlDbType.Int);
        SQLCmd.Parameters["@NascitaID2Nazione"].Value = 1;

        SQLCmd.Parameters.Add("@ID2Nazione", System.Data.SqlDbType.Int);
        SQLCmd.Parameters["@ID2Nazione"].Value = 1;

        SQLCmd.Parameters.Add("@Telefono1", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@Telefono1"].Value = string.Empty;

        SQLCmd.Parameters.Add("@Telefono2", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@Telefono2"].Value = string.Empty;

        SQLCmd.Parameters.Add("@Fax", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@Fax"].Value = string.Empty;

        SQLCmd.Parameters.Add("@Note", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@Note"].Value = string.Empty;

        SQLCmd.Parameters.Add("@UserMaster", System.Data.SqlDbType.UniqueIdentifier);
        SQLCmd.Parameters["@UserMaster"].Value = CreatorGUID;

        SQLCmd.Parameters.Add("@RecordNewUser", System.Data.SqlDbType.UniqueIdentifier);
        SQLCmd.Parameters["@RecordNewUser"].Value = CreatorGUID;

        SQLCmd.Parameters.Add("@RecordNewDate", System.Data.SqlDbType.SmallDateTime);
        SQLCmd.Parameters["@RecordNewDate"].Value = DateTime.Now;

        SQLCmd.Parameters.Add("@ZeusJolly1", System.Data.SqlDbType.Int);
        SQLCmd.Parameters["@ZeusJolly1"].Value = 0;

        SQLCmd.Parameters.Add("@ZeusJolly2", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@ZeusJolly2"].Value = string.Empty;

        SQLCmd.Parameters.Add("@ImportCode", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@ImportCode"].Value = string.Empty;
    }
    protected void InsertProfiliSocietari(ref SqlCommand SQLCmd, object userGUID, string userName)
    {
        string sqlQuery = string.Empty;
        object CreatorGUID = userGUID;

        sqlQuery = "SET DATEFORMAT dmy; INSERT INTO [tbProfiliSocietari]";
        sqlQuery += " ([UserId],[ID2BusinessBook],[RagioneSociale],[PartitaIVA],[S1_Indirizzo]";
        sqlQuery += " ,[S1_Numero],[S1_Citta],[S1_Comune],[S1_Cap],[S1_ProvinciaEstesa],[S1_ProvinciaSigla]";
        sqlQuery += " ,[S1_ID2Nazione],[S2_Indirizzo],[S2_Numero],[S2_Citta],[S2_Comune],[S2_Cap],[S2_ProvinciaEstesa]";
        sqlQuery += " ,[S2_ProvinciaSigla],[S2_ID2Nazione],[Telefono1],[Telefono2],[Fax],[WebSite]";
        sqlQuery += " ,[RecordNewUser],[RecordNewDate],[ZeusJolly1],[ZeusJolly2],[ImportCode])";
        sqlQuery += " VALUES ";
        sqlQuery += " (@UserId,@ID2BusinessBook,@RagioneSociale,@PartitaIVA,@S1_Indirizzo";
        sqlQuery += " ,@S1_Numero,@S1_Citta,@S1_Comune,@S1_Cap,@S1_ProvinciaEstesa,@S1_ProvinciaSigla";
        sqlQuery += " ,@S1_ID2Nazione,@S2_Indirizzo,@S2_Numero,@S2_Citta,@S2_Comune,@S2_Cap,@S2_ProvinciaEstesa";
        sqlQuery += " ,@S2_ProvinciaSigla,@S2_ID2Nazione,@Telefono1,@Telefono2,@Fax,@WebSite";
        sqlQuery += " ,@RecordNewUser,@RecordNewDate,@ZeusJolly1,@ZeusJolly2,@ImportCode)";
        SQLCmd.CommandText = sqlQuery;
        SQLCmd.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
        SQLCmd.Parameters["@UserId"].Value = userGUID;

        SQLCmd.Parameters.Add("@ID2BusinessBook", System.Data.SqlDbType.Int);
        SQLCmd.Parameters["@ID2BusinessBook"].Value = 0;

        SQLCmd.Parameters.Add("@RagioneSociale", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@RagioneSociale"].Value = String.Empty;

        SQLCmd.Parameters.Add("@PartitaIVA", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@PartitaIVA"].Value = PIVATextBox.Text.ToString();

        SQLCmd.Parameters.Add("@S1_Indirizzo", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@S1_Indirizzo"].Value = IndirizzoTextBox.Text.ToString();

        SQLCmd.Parameters.Add("@S1_Numero", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@S1_Numero"].Value = string.Empty;

        SQLCmd.Parameters.Add("@S1_Citta", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@S1_Citta"].Value = CittaTextBox1.Text.ToString();

        SQLCmd.Parameters.Add("@S1_Comune", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@S1_Comune"].Value = string.Empty;

        SQLCmd.Parameters.Add("@S1_Cap", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@S1_Cap"].Value = CAPTextBox.Text;

        SQLCmd.Parameters.Add("@S1_ProvinciaEstesa", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@S1_ProvinciaEstesa"].Value = ProvinciaDropdown.Text;

        SQLCmd.Parameters.Add("@S1_ProvinciaSigla", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@S1_ProvinciaSigla"].Value = ProvinciaDropdown.SelectedValue;

        SQLCmd.Parameters.Add("@S1_ID2Nazione", System.Data.SqlDbType.Int);
        SQLCmd.Parameters["@S1_ID2Nazione"].Value = 1;

        SQLCmd.Parameters.Add("@S2_Indirizzo", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@S2_Indirizzo"].Value = string.Empty;

        SQLCmd.Parameters.Add("@S2_Numero", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@S2_Numero"].Value = string.Empty;

        SQLCmd.Parameters.Add("@S2_Citta", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@S2_Citta"].Value = string.Empty;

        SQLCmd.Parameters.Add("@S2_Comune", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@S2_Comune"].Value = string.Empty;

        SQLCmd.Parameters.Add("@S2_Cap", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@S2_Cap"].Value = string.Empty;

        SQLCmd.Parameters.Add("@S2_ProvinciaEstesa", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@S2_ProvinciaEstesa"].Value = string.Empty;

        SQLCmd.Parameters.Add("@S2_ProvinciaSigla", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@S2_ProvinciaSigla"].Value = string.Empty;

        SQLCmd.Parameters.Add("@S2_ID2Nazione", System.Data.SqlDbType.Int);
        SQLCmd.Parameters["@S2_ID2Nazione"].Value = 1;

        SQLCmd.Parameters.Add("@Telefono1", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@Telefono1"].Value = TelefonoTextBox1.Text.ToString();

        SQLCmd.Parameters.Add("@Telefono2", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@Telefono2"].Value = CellulareTextBox1.Text.ToString();

        SQLCmd.Parameters.Add("@Fax", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@Fax"].Value = FaxTextBox1.Text.ToString();

        SQLCmd.Parameters.Add("@WebSite", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@WebSite"].Value = string.Empty;

        SQLCmd.Parameters.Add("@RecordNewUser", System.Data.SqlDbType.UniqueIdentifier);
        SQLCmd.Parameters["@RecordNewUser"].Value = CreatorGUID;

        SQLCmd.Parameters.Add("@RecordNewDate", System.Data.SqlDbType.SmallDateTime);
        SQLCmd.Parameters["@RecordNewDate"].Value = DateTime.Now;

        SQLCmd.Parameters.Add("@ZeusJolly1", System.Data.SqlDbType.Int);
        SQLCmd.Parameters["@ZeusJolly1"].Value = 0;

        SQLCmd.Parameters.Add("@ZeusJolly2", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@ZeusJolly2"].Value = string.Empty;

        SQLCmd.Parameters.Add("@ImportCode", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@ImportCode"].Value = string.Empty;
    }
    //protected void InsertProfiliMarketing(ref SqlCommand SQLCmd, object userGUID, string userName, string codiceSocio)
    protected void InsertProfiliMarketing(ref SqlCommand SQLCmd, object userGUID, string userName)
    {
        string sqlQuery = string.Empty;
        object CreatorGUID = userGUID;

        //sqlQuery = "SET DATEFORMAT dmy; INSERT INTO [tbProfiliMarketing]";
        //sqlQuery += " ([UserId],[ID2Categoria1],[ConsensoDatiPersonali],[ConsensoDatiPersonali2],[Comunicazioni1]";
        //sqlQuery += ",[Comunicazioni2],[Comunicazioni3],[Comunicazioni4],[Comunicazioni5],[Comunicazioni6]";
        //sqlQuery += ",[Comunicazioni7],[Comunicazioni8],[Comunicazioni9],[Comunicazioni10],[RecordNewUser]";
        //sqlQuery += ",[RecordNewDate],[ZeusJolly1],[ZeusJolly2],[ImportCode],[RegCausal], [CodiceSocioNum], [CodiceSocioStr], [DataRichiestaSocio], [StatusSocio])";
        //sqlQuery += " VALUES ";
        //sqlQuery += " (@UserId,@ID2Categoria1,@ConsensoDatiPersonali,@ConsensoDatiPersonali2,@Comunicazioni1";
        //sqlQuery += " ,@Comunicazioni2,@Comunicazioni3,@Comunicazioni4,@Comunicazioni5,@Comunicazioni6";
        //sqlQuery += " ,@Comunicazioni7,@Comunicazioni8,@Comunicazioni9,@Comunicazioni10,@RecordNewUser";
        //sqlQuery += " ,@RecordNewDate,@ZeusJolly1,@ZeusJolly2,@ImportCode,'PUB', @CodiceSocioNum, @CodiceSocioStr, @DataRichiestaSocio, 'RIC')";
        sqlQuery = "SET DATEFORMAT dmy; INSERT INTO [tbProfiliMarketing]";
        sqlQuery += " ([UserId],[ID2Categoria1],[ConsensoDatiPersonali],[ConsensoDatiPersonali2],[Comunicazioni1]";
        sqlQuery += ",[Comunicazioni2],[Comunicazioni3],[Comunicazioni4],[Comunicazioni5],[Comunicazioni6]";
        sqlQuery += ",[Comunicazioni7],[Comunicazioni8],[Comunicazioni9],[Comunicazioni10],[RecordNewUser]";
        sqlQuery += ",[RecordNewDate],[ZeusJolly1],[ZeusJolly2],[ImportCode],[RegCausal], [DataRichiestaSocio], [StatusSocio])";
        sqlQuery += " VALUES ";
        sqlQuery += " (@UserId,@ID2Categoria1,@ConsensoDatiPersonali,@ConsensoDatiPersonali2,@Comunicazioni1";
        sqlQuery += " ,@Comunicazioni2,@Comunicazioni3,@Comunicazioni4,@Comunicazioni5,@Comunicazioni6";
        sqlQuery += " ,@Comunicazioni7,@Comunicazioni8,@Comunicazioni9,@Comunicazioni10,@RecordNewUser";
        sqlQuery += " ,@RecordNewDate,@ZeusJolly1,@ZeusJolly2,@ImportCode,'PUB', @DataRichiestaSocio, 'RIC')";
        SQLCmd.CommandText = sqlQuery;
        SQLCmd.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
        SQLCmd.Parameters["@UserId"].Value = userGUID;

        SQLCmd.Parameters.Add("@ID2Categoria1", System.Data.SqlDbType.Int);
        SQLCmd.Parameters["@ID2Categoria1"].Value = 0;

        SQLCmd.Parameters.Add("@ConsensoDatiPersonali", System.Data.SqlDbType.Bit);
        SQLCmd.Parameters["@ConsensoDatiPersonali"].Value = true;

        SQLCmd.Parameters.Add("@ConsensoDatiPersonali2", System.Data.SqlDbType.Bit);
        SQLCmd.Parameters["@ConsensoDatiPersonali2"].Value = NewsCheckBox.Checked;

        // codice SocioString

        //SQLCmd.Parameters.Add("@CodiceSocioStr", System.Data.SqlDbType.NVarChar);
        //SQLCmd.Parameters["@CodiceSocioStr"].Value = codiceSocio;

        //SQLCmd.Parameters.Add("@CodiceSocioNum", System.Data.SqlDbType.Int);
        //SQLCmd.Parameters["@CodiceSocioNum"].Value = Convert.ToInt32(codiceSocio);

        SQLCmd.Parameters.Add("@Comunicazioni1", System.Data.SqlDbType.Bit);
        SQLCmd.Parameters["@Comunicazioni1"].Value = 0;

        SQLCmd.Parameters.Add("@Comunicazioni2", System.Data.SqlDbType.Bit);
        SQLCmd.Parameters["@Comunicazioni2"].Value = 0;

        SQLCmd.Parameters.Add("@Comunicazioni3", System.Data.SqlDbType.Bit);
        SQLCmd.Parameters["@Comunicazioni3"].Value = 0;

        SQLCmd.Parameters.Add("@Comunicazioni4", System.Data.SqlDbType.Bit);
        SQLCmd.Parameters["@Comunicazioni4"].Value = 0;

        SQLCmd.Parameters.Add("@Comunicazioni5", System.Data.SqlDbType.Bit);
        SQLCmd.Parameters["@Comunicazioni5"].Value = 0;

        SQLCmd.Parameters.Add("@Comunicazioni6", System.Data.SqlDbType.Bit);
        SQLCmd.Parameters["@Comunicazioni6"].Value = 0;

        SQLCmd.Parameters.Add("@Comunicazioni7", System.Data.SqlDbType.Bit);
        SQLCmd.Parameters["@Comunicazioni7"].Value = 0;

        SQLCmd.Parameters.Add("@Comunicazioni8", System.Data.SqlDbType.Bit);
        SQLCmd.Parameters["@Comunicazioni8"].Value = 0;

        SQLCmd.Parameters.Add("@Comunicazioni9", System.Data.SqlDbType.Bit);
        SQLCmd.Parameters["@Comunicazioni9"].Value = 0;

        SQLCmd.Parameters.Add("@Comunicazioni10", System.Data.SqlDbType.Bit);
        SQLCmd.Parameters["@Comunicazioni10"].Value = 0;

        SQLCmd.Parameters.Add("@RecordNewUser", System.Data.SqlDbType.UniqueIdentifier);
        SQLCmd.Parameters["@RecordNewUser"].Value = CreatorGUID;

        SQLCmd.Parameters.Add("@RecordNewDate", System.Data.SqlDbType.SmallDateTime);
        SQLCmd.Parameters["@RecordNewDate"].Value = DateTime.Now;

        SQLCmd.Parameters.Add("@ZeusJolly1", System.Data.SqlDbType.Int);
        SQLCmd.Parameters["@ZeusJolly1"].Value = 0;

        SQLCmd.Parameters.Add("@ZeusJolly2", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@ZeusJolly2"].Value = string.Empty;


        SQLCmd.Parameters.Add("@ImportCode", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@ImportCode"].Value = string.Empty;

        DateTime Data = DateTime.Now;
        SQLCmd.Parameters.Add("@DataRichiestaSocio", System.Data.SqlDbType.DateTime);
        SQLCmd.Parameters["@DataRichiestaSocio"].Value = Data.ToString();
    }
    protected void InsertQuoteAnnuali(ref SqlCommand SQLCmd, object userGUID, string userName, string anno)
    {
        string sqlQuery = string.Empty;
        object CreatorGUID = userGUID;

        sqlQuery = "SET DATEFORMAT dmy; INSERT INTO [tbQuoteAnnuali]";
        sqlQuery += " ([UserId],[ID2QuotaCosto],[Anno],[ID2MetodoPagamento],[NoteSocio]";
        sqlQuery += ",[PagamentoEur],[RecordNewUser],[RecordNewDate],[ZeusIsAlive])";
        sqlQuery += " VALUES ";
        sqlQuery += " (@UserId,@ID2QuotaCosto,@Anno,@ID2MetodoPagamento,@NoteSocio";
        sqlQuery += " ,@PagamentoEur,@RecordNewUser,@RecordNewDate,1)";
        SQLCmd.CommandText = sqlQuery;

        SQLCmd.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
        SQLCmd.Parameters["@UserId"].Value = userGUID;

        SQLCmd.Parameters.Add("@RecordNewUser", System.Data.SqlDbType.UniqueIdentifier);
        SQLCmd.Parameters["@RecordNewUser"].Value = userGUID;

        SQLCmd.Parameters.Add("@ID2QuotaCosto", System.Data.SqlDbType.Int);
        SQLCmd.Parameters["@ID2QuotaCosto"].Value = ID2QuotaCostoDropDownList.SelectedItem.Value;

        SQLCmd.Parameters.Add("@Anno", System.Data.SqlDbType.SmallInt);
        SQLCmd.Parameters["@Anno"].Value = anno;

        SQLCmd.Parameters.Add("@ID2MetodoPagamento", System.Data.SqlDbType.Int);
        //SQLCmd.Parameters["@ID2MetodoPagamento"].Value = PagamentoDropDownList.SelectedItem.Value;
        SQLCmd.Parameters["@ID2MetodoPagamento"].Value = PagamentoRadioButtonList.SelectedItem.Value;

        SQLCmd.Parameters.Add("@NoteSocio", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@NoteSocio"].Value = NotePagamentoTextBox.Text;

        DateTime Data = DateTime.Now;
        SQLCmd.Parameters.Add("@RecordNewDate", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@RecordNewDate"].Value = Data.ToString();

        //decimal appDec;

        //if (!Decimal.TryParse(ImportoTextBox.Text, NumberStyles.AllowDecimalPoint, new CultureInfo("it-IT"), out appDec))
        //{
        //    appDec = 0;
        //}

        SQLCmd.Parameters.Add("@PagamentoEur", System.Data.SqlDbType.Decimal);
        SQLCmd.Parameters["@PagamentoEur"].Value = GetQuotePrice(SQLCmd.Connection);
    }

    protected void CreateButton_Click(object sender, EventArgs e)
    {

        FirstStepErrorLabel.Visible = false;

        if (Password.Text.Length < Membership.MinRequiredPasswordLength)
        {
            FirstStepErrorLabel.Text = "La password deve essere lunga almeno " +
                Membership.MinRequiredPasswordLength.ToString() + " caratteri<br />";
            FirstStepErrorLabel.Visible = true;
            return;
        }

        if (!CreateUser())
            return;


        SendMail(Membership.FindUsersByEmail(EmailTextBox.Text), EmailTextBox.Text);
        //TODO: INVIARE EMAIL ANCHE AD MVCONGRESSI
        SendMail(Membership.FindUsersByEmail(EmailTextBox.Text), ConfigurationManager.AppSettings["MailSertot"]);

        //SendMail(System.Web.Security.Membership.FindUsersByEmail(EmailTextBox.Text), EmailTextBox.Text);

        // ------------ se mando a LISTA allora automaticamente invio alla lista prevista
        //SendMail(Membership.FindUsersByEmail(EmailTextBox.Text), "LISTA");

        Page.Title = "Invio richiesta di associazione Sertot";
        litTitle.Text = "Iscrizione nuovo Socio";
        Step2Panel.Visible = false;
        Step3Panel.Visible = true;

        try
        {
            PasswordLabel.Text = Membership.GetUser(UserNameLabel.Text).GetPassword();
        }
        catch (Exception) { }
        EmailHyperLink.NavigateUrl = "mailto:" + EmailTextBox.Text;
        EmailHyperLink.Text = EmailTextBox.Text;
        QuotaLabel.Text = ID2QuotaCostoDropDownList.SelectedItem.Text;

    }//fine CreateButton_Click

    protected void CheckBoxRequired_ServerValidate(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = PrivacyTextBox.Checked;
    }



    /// <summary>
    /// Recupera la quota assicurativa dalla tabella.
    ///  Siccome si fanno le cose in transazione, è necessario passargli la stessa connessione usata per gestire la transazione.   
    /// </summary>
    private decimal GetQuotePrice(SqlConnection connection)
    {
        return 0;
    }
}