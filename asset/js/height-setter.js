﻿var maxElementHeight = 0;

$(document).ready(function () {
    $(window).on('load', function () {
        setTimeout(function () {
            SetAltezzaTitoloBox('main .boxBanner .box .wrapperTesto');
        }, 50);
    });

    $(window).resize(function () {
        SetAltezzaTitoloBox('main .boxBanner .box .wrapperTesto');
    });
});

function SetAltezzaTitoloBox(list, minHeight) {
    var listaBox = $(list);
    listaBox.height('auto');

    if (minHeight !== undefined) {
        minHeight = parseInt(minHeight) - 30;
    }
    else {
        minHeight = 0;
    }

    $.each(listaBox, function (key, item) {
        var itemHeight = $(item).height() + 30;
        if (minHeight > 0 && minHeight > itemHeight && minHeight > maxElementHeight) {
            maxElementHeight = minHeight;
        }
        else if ($(item).height() > maxElementHeight) {
            maxElementHeight = $(item).height();
        }
    });

    listaBox.height(maxElementHeight);
    maxElementHeight = 0;
}