﻿$(document).ready(function () {
    $('main.home .slider').slick({
        infinite: true,
        autoplay: true,
        autoplaySpeed: 6000,
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 1000,
        arrows: true,
        nextArrow: $('main.home .slider .boxTesto .rightArrow'),
        prevArrow: $('main.home .slider .boxTesto .leftArrow'),
        dots: true
    });
});