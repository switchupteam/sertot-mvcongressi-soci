﻿$(document).ready(function () {
    $(window).on('load', function () {
        setTimeout(function () {
            SetAltezzaUgualeLarghezza($('section.gallery_Dettaglio .elencoFoto .foto .immagine'));
            SetAltezzaImmagini($('section.gallery_Dettaglio .elencoFoto .foto .immagine'));
        }, 50);
    });


    $(window).resize(function () {
        SetAltezzaUgualeLarghezza($('section.gallery_Dettaglio .elencoFoto .foto .immagine'));
        SetAltezzaImmagini($('section.gallery_Dettaglio .elencoFoto .foto .immagine'));
    });


    //$('section.galleryDettaglio .elencoFoto .foto .immagine').on('click', function () {
    //    OpenPhotoswipe('section.galleryDettaglio .elencoFoto .foto .immagine img', $(this).children('img')[0]);
    //});


});


function OpenPhotoswipe(percorsoImg, clickedImg) {
    var pswpElement = document.querySelectorAll('.pswp')[0];

    var fotoList = $(percorsoImg);
    var items = [];
    for (ii = 0; ii < fotoList.length; ii++) {
        var newitem = {
            src: fotoList[ii].src,
            w: fotoList[ii].naturalWidth,
            h: fotoList[ii].naturalHeight
        };

        items.push(newitem);
    }


    var options = {
        index: fotoList.index(clickedImg),
        history: true,
        focus: false,
        escKey: true,
        closeOnScroll: false,
        clickToCloseNonZoomable: false,
        showAnimationDuration: 500,
        hideAnimationDuration: 500
    };


    $(".pswp").css("display", "block");
    var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
    gallery.init();
}

function SetAltezzaUgualeLarghezza(lista) {
    $.each(lista, function (key, boxImg) {
        $(boxImg).css('height', $(boxImg).width());

        $(window).resize(function () {
            $(boxImg).css('height', $(boxImg).width());
        });
    });
}

function SetAltezzaImmagini(lista) {
    $.each(lista, function (key, boxImg) {
        var boxImgWidth = $(boxImg).width();

        var img = $(boxImg).find('img');
        var imgWidth = img.width();
        var imgHeight = img.height();

        $('<img>').attr('src', $(img).attr('src')).on('load', function () {
            var imgNaturalWidth = this.width;
            var imgNaturalHeight = this.height;

            if (imgWidth < imgHeight || imgWidth < boxImgWidth) {
                //var parent = $(img).parent();
                $(boxImg).removeClass('horizontalImg');
                $(boxImg).addClass('verticalImg');
            }
            else {
                $(boxImg).removeClass('verticalImg');
                $(boxImg).addClass('horizontalImg');
            }
        });
    });
}
