﻿<%@ Page Language="C#" MasterPageFile="~/Master/LayoutMain.master" CodeFile="Content.aspx.cs" Inherits="Web_Content" Async="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="AreaTitle" runat="Server">
    <%--<%= titolo %>--%>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="AreaContentMain" runat="Server">
    <section class="istituzionali">
        <div class="container">
            <div class="innerBox">
<%--                <asp:Panel runat="server" ID="ImgHeadline_Panel" Visible="false">
                    <div class="immagineHeadline">
                        <img src="/ZeusInc/PageDesigner/Img1/<%= immagine1 %>" alt="<%= immagine1Alt %>" />
                    </div>
                </asp:Panel>--%>

                <div class="firstTitle">
                    <h2 class="titolo"><%= titolo %></h2>
                    <hr class="left-position" />
                </div>

                <div class="introduzione">
                    <div class="row">
                        <asp:PlaceHolder runat="server" ID="phImg" Visible="false">
                            <div class="col-md-5">
                                <div class="immagine">
                                    <img src="/ZeusInc/PageDesigner/Img1/<%= immagine3 %>" alt="<%= immagine3Alt %>" />
                                </div>
                            </div>
                        </asp:PlaceHolder>

                        <div class="col testo" runat="server">
                            <div class="richText">
                                <%= contenuto1 %>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="richText">
                    <%= contenuto2 %>
                </div>
            </div>
        </div>

        <asp:PlaceHolder runat="server" ID="LinkDownloadContent" Visible="false">
            <div class="linkDownload">
                <div class="container">
                    <div class="richText">
                        <%= contenuto3 %>
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>

        <asp:PlaceHolder runat="server" ID="phFurtherContent" Visible="false">
            <div class="sezioneRossa">
                <div class="container">
                    <div class="richText">
                        <%= furtherContent %>
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
    </section>

</asp:Content>
