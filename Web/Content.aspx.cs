﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.HtmlControls;

public partial class Web_Content : System.Web.UI.Page
{
    protected string titolo = string.Empty;
    protected string contenuto1 = string.Empty;
    protected string contenuto2 = string.Empty;
    protected string contenuto3 = string.Empty;
    protected string contenuto4 = string.Empty;
    protected string immagine1 = string.Empty;
    protected string immagine1Alt = string.Empty;
    protected string immagine3 = string.Empty;
    protected string immagine3Alt = string.Empty;
    protected string furtherContent = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Request.QueryString["XRI"]))
        {
            Response.Redirect("~/System/Message.aspx?0");
        }

        AresUtilities aresUtilities = new AresUtilities();
        string lang = aresUtilities.GetLangRewritted();

        string strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(strConnessione))
        {
            connection.Open();
            SqlCommand command = connection.CreateCommand();

            sqlQuery = @"SELECT TitoloPagina, TitoloBrowser, Contenuto1, Contenuto2, Contenuto3, Contenuto4, TagDescription, TagKeywords, Immagine1, Immagine12Alt, Immagine3, Immagine34Alt                            
                        FROM tbPageDesigner 
                        LEFT OUTER JOIN tbPZ_ImageRaider on ZeusIdModuloIndice = ZeusIdModulo + '1'
                        WHERE  ID1Pagina = @ID1Pagina 
                        AND ZeusIdModulo = 'PDWEB'
                        AND tbPageDesigner.ZeusLangCode = @ZeusLangCode ";

            // ------------------- Check per Anteprima su pagine non pubblicate, controllo se la pagina chiamante è Zeus ----
            if (!AresUtilities.CheckPreviousPageContains("PageDesigner/Content_Lst.aspx"))
                sqlQuery += " AND ZeusIsAlive = 1 AND PW_Area1 = 1 AND(GETDATE() BETWEEN PWI_Area1 AND PWF_Area1) ";
            // ------------------- Fine Check per Anteprima --------------------------------------------

            command.CommandText = sqlQuery;
            command.Parameters.Add("@ID1Pagina", System.Data.SqlDbType.Int);
            command.Parameters["@ID1Pagina"].Value = Request.QueryString["XRI"];
            command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.NVarChar);
            command.Parameters["@ZeusLangCode"].Value = lang;

            SqlDataReader reader = command.ExecuteReader();

            if (reader.Read())
            {
                titolo = reader["TitoloPagina"].ToString();
                contenuto1 = reader["Contenuto1"].ToString();
                contenuto2 = reader["Contenuto2"].ToString();
                contenuto3 = reader["Contenuto3"].ToString();
                contenuto4 = reader["Contenuto4"].ToString();
                immagine1 = reader["Immagine1"].ToString();
                immagine3 = reader["Immagine3"].ToString();

                if (!string.IsNullOrEmpty(contenuto3))
                    LinkDownloadContent.Visible = true;

                //if (!string.IsNullOrEmpty(immagine1) && immagine1 != "ImgNonDisponibile.jpg")
                //{
                //    immagine1Alt = reader["Immagine12Alt"].ToString();
                //    ImgHeadline_Panel.Visible = true;
                //    //mdcontent.Attributes["class"] = "col-md-7 ml-auto";
                //}

                if (!string.IsNullOrEmpty(immagine3) && immagine3.Substring(immagine3.LastIndexOf('/') + 1) != "ImgNonDisponibile.jpg")
                {
                    immagine3Alt = reader["Immagine34Alt"].ToString();
                    phImg.Visible = true;
                    //mdcontent.Attributes["class"] = "col-md-7 ml-auto";
                }
                //else
                //{
                //    mdcontent.Attributes["class"] = "";
                //}


                if (!string.IsNullOrEmpty(StripHtml(contenuto4)))
                {
                    furtherContent = contenuto4;
                    phFurtherContent.Visible = true;
                }



                /*****************************************************/
                /**************** INIZIO SEO *************************/
                /*****************************************************/
                HtmlMeta description = new HtmlMeta();
                HtmlMeta keywords = new HtmlMeta();
                description.Name = "description";
                keywords.Name = "keywords";
                Page.Title = reader["TitoloBrowser"].ToString() + " | " + AresConfig.NomeSito;
                description.Content = reader["TagDescription"].ToString();
                keywords.Content = reader["TagKeywords"].ToString();
                HtmlHead head = Page.Header;
                head.Controls.Add(description);
                head.Controls.Add(keywords);
                /*****************************************************/
                /**************** FINE SEO ***************************/
                /*****************************************************/

                reader.Close();
                connection.Close();
            }
            else
            {
                reader.Close();
                connection.Close();
                Response.Redirect("~/System/Message.aspx?0");
            }
        }


        //rptBottoni.DataSource = new List<dynamic>() {
        //    new { imgsrc = "/asset/img/bilanci.png",  href = "#"},
        //      new { imgsrc = "/asset/img/statuto.png",  href = "#"}

        //};
        //rptBottoni.DataBind();

    }


    private List<clsLink> GetLink()
    {
        List<clsLink> lstTmp = null;

        StringBuilder sbSQL = new StringBuilder();

        sbSQL.AppendFormat("  ");
        sbSQL.AppendFormat("  ");
        sbSQL.AppendFormat("  ");
        sbSQL.AppendFormat("  ");
        sbSQL.AppendFormat("  ");
        sbSQL.AppendFormat("  ");

        using (SqlConnection connection = new SqlConnection(clsGenericUtils.strConnessione))
            try
            {

                connection.Open();
                SqlCommand sqlCmd = connection.CreateCommand();

                sqlCmd.CommandText = sbSQL.ToString();

                //                sqlCmd.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.NVarChar);
                //                sqlCmd.Parameters["@ZeusLangCode"].Value = paramLang;                 

                SqlDataReader reader = sqlCmd.ExecuteReader();

                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        // reader["CatNonno"].ToString();                       

                    }
                }
            }
            catch (Exception)
            {
                throw;
            }



        return lstTmp;
    }


    protected class clsLink
    {
        public int iIDPagina { get; set; }
        public string sUrlRewrite { get; set; }
        public string sPublicUrl
        {
            get
            {
                return string.Format("/{0}/{1}", iIDPagina, sUrlRewrite);
            }
        }

        public string sImg { get; set; }
        public string sPublicImg { get; set; }
    }

    protected string StripHtml(string value)
    {
        if (string.IsNullOrEmpty(value))
            return string.Empty;
        else
        {
            return Regex.Replace(value, "<.*?>", string.Empty).Trim();
        }

    }

}