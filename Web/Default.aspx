﻿<%@ Page Language="C#" MasterPageFile="~/Master/LayoutMain.master" CodeFile="Default.aspx.cs" Inherits="Web_Default" %>

<asp:Content ID="Content2" ContentPlaceHolderID="AreaTitle" runat="Server">
    Chi siamo
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="AreaContentMain" runat="Server">
    <section class="istituzionali">
        <div class="container mb-5 position-relative">
            <img src="/asset/img/chi_siamo.png" style="position: absolute; left: -75px; top: 0; width: 530px;" />
            <div class="row mb-5">
                <div class="col-md-7 ml-auto">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer a fringilla dolor, et pellentesque tellus. Nullam dolor dui, venenatis nec
                    </p>
                </div>
            </div>
            <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer a fringilla dolor, et pellentesque tellus. Nullam dolor dui, venenatis necà</h3>
            <p>
               Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer a fringilla dolor, et pellentesque tellus. Nullam dolor dui, venenatis nec
            </p>
        </div>

        <div style="background: url('/asset/img/grey_bar.jpg') left top repeat-x, url('/asset/img/grey_bar.jpg') left bottom repeat-x; padding: 5rem 0; background-color: #730F11; margin-bottom: 4rem;">
            <div class="container">
                <h6 style="color: #fff;" class="mb-5">La nostra storia</h6>
                <h5 style="color: #fff;">1998</h5>
                <p style="color: #fff;" class="mb-5">
                   Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer a fringilla dolor, et pellentesque tellus. Nullam dolor dui, venenatis nec
                </p>
            </div>
        </div>

        <div class="container text-center">
            <a href="#" style="display: inline-block; margin-right: 100px;">
                <img src="/asset/img/bilanci.png" style="max-width: 250px;" />
            </a>
            <a href="#" style="display: inline-block">
                <img src="/asset/img/statuto.png" style="max-width: 250px;" />
            </a>
        </div>

    </section>
</asp:Content>
