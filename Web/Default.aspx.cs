﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Web_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        /*****************************************************/
        /**************** INIZIO SEO *************************/
        /*****************************************************/
        HtmlMeta description = new HtmlMeta();
        HtmlMeta keywords = new HtmlMeta();
        description.Name = "description";
        keywords.Name = "keywords";
        Page.Title = "Chi siamo | " + AresConfig.NomeSito;
        description.Content = "";
        keywords.Content = "";
        HtmlHead head = Page.Header;
        head.Controls.Add(description);
        head.Controls.Add(keywords);
        /*****************************************************/
        /**************** FINE SEO ***************************/
        /*****************************************************/
    }
}