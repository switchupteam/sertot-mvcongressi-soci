﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per Account_Email_Edt
/// </summary>
public partial class Account_Email_Edt : System.Web.UI.Page
{

    #region Fields

    private string Lang = "ITA";

    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs args)
    {
        delinea myDelinea = new delinea();

        if (Membership.GetUser() == null)
            Response.Redirect("~/Zeus/System/Message.aspx?NoUser");

        if (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang") && Request.QueryString["Lang"] != null)
            Response.Redirect("~/System/Message.aspx?0");

        Lang = Request.QueryString["Lang"] ?? Lang;
        if (Lang.Length == 0)
            Lang = "ITA";

        UID.Value = Membership.GetUser().ProviderUserKey.ToString();

        if (!Page.IsPostBack)
            EmailTextBox.Text = Membership.GetUser().Email.ToString();        
    }

    protected void UpdateEmailButton_OnClick(object sender, EventArgs args)
    {
        try
        {
            MembershipUser user = Membership.GetUser();
            user.Email = EmailTextBox.Text.ToString();
            Membership.UpdateUser(user);
            Response.Redirect("~/Account/");
        }
        catch (Exception p)
        {
        }
    }

    protected void MemberShipFormView_DataBound(object sender, EventArgs e)
    {
        ReadXML_Localization("PROFILI", Lang);
    }

    #endregion

    #region Methods

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";
            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode + "/ACCOUNT";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Profili.xml"));

            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                            if (myLabel != null)
                                myLabel.Text = childNode.InnerText;
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name + "Label");

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }

            LinkButton ModificaLinkButton = (LinkButton)myDelinea.FindControlRecursive(Page, "UpdateButton");
            ModificaLinkButton.Text = (mydoc.SelectSingleNode(myXPath + "/" + "UpdateButton").InnerText);
            Page.Title = (mydoc.SelectSingleNode(myXPath + "/" + "TitoloPaginaAccountEmail").InnerText);
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    #endregion

}