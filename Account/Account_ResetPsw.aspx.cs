﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per Account_ResetPsw
/// </summary>
public partial class Account_ResetPsw : System.Web.UI.Page
{

    #region Fields

    private string Lang = "ITA";
    private static int MAXLENPASSWORD = 8;

    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();

        if (Membership.GetUser() == null)
            Response.Redirect("~/Zeus/System/Message.aspx?NoUser");

        UID.Value = Membership.GetUser().ProviderUserKey.ToString();

        if (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang") && Request.QueryString["Lang"] != null)
            Response.Redirect("~/System/Message.aspx?0");

        Lang = Request.QueryString["Lang"] ?? Lang;
        if (Lang.Length == 0)
            Lang = "ITA";
    }

    protected void CambiaPasswordLinkButton_Click(object sender, EventArgs e)
    {
        try
        {
            if (CambiaPasswordCustomValidator.IsValid)
            {
                MembershipUser user = Membership.GetUser();
                user.ChangePassword(user.GetPassword().ToString(), TextBoxNewPass.Text.ToString());
                Membership.UpdateUser(user);
                Response.Redirect("~/Account/");
            }
        }
        catch (System.ArgumentException p)
        {
            CambiaPasswordCustomValidator.IsValid = false;

            if (Membership.MinRequiredNonAlphanumericCharacters > 0)
                CambiaPasswordCustomValidator.ErrorMessage = "Caratteri speciali richiesti: almeno " + Membership.MinRequiredNonAlphanumericCharacters;
        }
    }

    protected void CambiaPasswordCustomValidator_ServerValidate(object sender, ServerValidateEventArgs args)
    {
        CambiaPasswordCustomValidator.ErrorMessage = string.Empty;

        if (TextBoxNewPass.Text.Length < Membership.MinRequiredPasswordLength)
        {
            args.IsValid = false;
            CambiaPasswordCustomValidator.ErrorMessage = "Lunghezza minima password: " + Membership.MinRequiredPasswordLength + " caratteri alfanumerici";
        }
        if (!TextBoxNewPass.Text.Equals(TextBoxConfNewPass.Text))
        {
            args.IsValid = false;
            CambiaPasswordCustomValidator.ErrorMessage = "Password e Conferma password non coincidono";
        }
    }

    protected void MemberShipFormView_DataBound(object sender, EventArgs e)
    {
        ReadXML_Localization("PROFILI", Lang);
    }

    #endregion

    #region Methods

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";
            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode + "/ACCOUNT";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Profili.xml"));

            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                            if (myLabel != null)
                                myLabel.Text = childNode.InnerText;
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name + "Label");

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }

            LinkButton ModificaLinkButton = (LinkButton)myDelinea.FindControlRecursive(Page, "CambiaPasswordLinkButton");
            ModificaLinkButton.Text = (mydoc.SelectSingleNode(myXPath + "/" + "CambiaPasswordLinkButton").InnerText);
            Page.Title = (mydoc.SelectSingleNode(myXPath + "/" + "TitoloPaginaAccountResetPsw").InnerText);
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    #endregion
    
}