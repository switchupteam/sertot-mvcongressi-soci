﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per Account_Dtl
/// </summary>
public partial class Account_Account_Dtl : System.Web.UI.UserControl
{

    #region Fields

    private string lang = string.Empty;

    #endregion

    #region Properties

    public string Lang
    {
        set { lang = value; }
        get { return lang; }
    }

    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();

        if (Membership.GetUser() == null)
            Response.Redirect("~/Zeus/System/Message.aspx?NoUser");

        UID.Value = Membership.GetUser().ProviderUserKey.ToString();
    }

    protected void DataUltimoBloccoLabel_Databinding(object sender, EventArgs e)
    {
        Label DataUltimoBloccoLabel = (Label)sender;
        if (DataUltimoBloccoLabel.Text.Equals("01/01/1754 0.00.00"))
            DataUltimoBloccoLabel.Text = "Mai bloccato";
    }

    protected void MemberShipSqlDataSource_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.AffectedRows == 0 || e.Exception != null)
            Response.Redirect("~/System/Message.aspx?SelectErr");
    }

    protected void MemberShipFormView_DataBound(object sender, EventArgs e)
    {
        ReadXML_Localization("PROFILI", lang);
    }

    #endregion

    #region Methods

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";
            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode + "/ACCOUNT";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Profili.xml"));

            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);
            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                            if (myLabel != null)
                            {
                                myLabel.Text = childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name + "Label");

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                            }
                        }
                        catch (Exception p)
                        {
                        }
                    }
                }
            }

            LinkButton ModificaEMailLinkButton = (LinkButton)myDelinea.FindControlRecursive(Page, "ModificaEMailLinkButton");
            ModificaEMailLinkButton.Text = (mydoc.SelectSingleNode(myXPath + "/" + "ModificaEMailLinkButton").InnerText);
            LinkButton ModificaPasswordLinkButton = (LinkButton)MemberShipFormView.FindControl("ModificaPasswordLinkButton");
            ModificaPasswordLinkButton.Text = (mydoc.SelectSingleNode(myXPath + "/" + "ModificaPasswordLinkButton").InnerText);
        }
        catch (Exception p)
        {
            return;
        }
    }

    #endregion
    
}