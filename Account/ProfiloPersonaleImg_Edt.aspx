﻿<%@ Page Language="C#" MasterPageFile="~/Master/LayoutAreaSoci.master" Title="Sertot" CodeFile="ProfiloPersonaleImg_Edt.aspx.cs" Inherits="Account_ProfiloPersonaleImg_Edt" %>

<%@ Register Src="~/UserControl/MenuSoci.ascx" TagName="Menu_Soci" TagPrefix="uc1" %>
<%@ Register Src="ProfiloPersonaleImg_Edt.ascx" TagName="ProfiloPersonaleImg_Edt" TagPrefix="uc1" %>

<asp:Content ID="Content4" ContentPlaceHolderID="AreaTitle" runat="Server">
    Aggiornamento Immagine Socio
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="AreaContentMain" runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="LAY01A_Content1">
                    <uc1:ProfiloPersonaleImg_Edt ID="ProfiloPersonaleImg_Edt1" runat="server" />
                </div>
            </div>
            <div class="col-lg-3">
                <div class="LAY01A_Content2">
                    <uc1:Menu_Soci ID="Menu_Soci1" runat="server" />
                </div>
                <div class="FloatClr"></div>
            </div>
        </div>
    </div>
</asp:Content>
