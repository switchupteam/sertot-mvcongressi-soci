﻿<%@ Control Language="C#"
    CodeFile="ProfiloSocietario_Dtl.ascx.cs"
    Inherits="Account_ProfiloSocietario_Dtl" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<asp:FormView ID="ProfiloSocietarioFormView" runat="server" Width="100%" OnDataBound="ProfiloSocietarioFormView_DataBound">
    <ItemTemplate>
        <div class="BXT_001">
            <div class="TFY_Titolo3">
                <asp:Label ID="ProfilosocietarioLabel" runat="server" CssClass="TFY_Sottotitolo3" Text="Affiliazione"></asp:Label>
            </div>
        </div>
        <table border="0" cellpadding="2" cellspacing="1">
            <tr>
                <td>
                    <asp:Label ID="RagioneSocialeDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Istituto:</asp:Label>
                    <asp:Label ID="RagioneSocialeLabel" runat="server" CssClass="HIC_LabelCorpoTesto1" Text='<%# Eval("RagioneSociale") %>'>
                    </asp:Label>
                </td>
            </tr>
            <asp:Panel ID="ZMCF_BusinessBook" runat="server">
                <tr>
                    <td>
                        <asp:Label ID="IdentificativoDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Azienda collegata:</asp:Label>
                        <asp:Label ID="IdentificativoLabel" runat="server" CssClass="HIC_LabelCorpoTesto1" Text='<%# Eval("Identificativo") %>'>
                        </asp:Label>
                    </td>
                </tr>
            </asp:Panel>
<%--            <tr>
                <td>
                    <asp:Label ID="PartitaIVADescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Partita IVA:</asp:Label>
                    <asp:Label ID="PartitaIVALabel" runat="server" CssClass="HIC_LabelCorpoTesto1" Text='<%# Eval("PartitaIVA") %>'>
                    </asp:Label>
                </td>
            </tr>--%>
            <tr>
                <td>
                    <asp:Label ID="S1_IndirizzoDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Istituto - Indirizzo:</asp:Label>
                    <asp:Label ID="S1_IndirizzoLabel" runat="server" CssClass="HIC_LabelCorpoTesto1" Text='<%# Eval("S1_Indirizzo") %>'>
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="S1_NumeroDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Istituto - Numero:</asp:Label>
                    <asp:Label ID="S1_NumeroLabel" runat="server" CssClass="HIC_LabelCorpoTesto1" Text='<%# Eval("S1_Numero") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="S1_CittaDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Istituto - Città:</asp:Label>
                    <asp:Label ID="S1_CittaLabel" runat="server" CssClass="HIC_LabelCorpoTesto1" Text='<%# Eval("S1_Citta") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="S1_ComuneDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Istituto - Comune:</asp:Label>
                    <asp:Label ID="S1_ComuneLabel" runat="server" CssClass="HIC_LabelCorpoTesto1" Text='<%# Eval("S1_Comune") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="S1_CapDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Istituto - CAP:</asp:Label>
                    <asp:Label ID="S1_CapLabel" runat="server" CssClass="HIC_LabelCorpoTesto1" Text='<%# Eval("S1_Cap") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="S1_ProvinciaEstesaDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Istituto - Provincia:</asp:Label>
                    <asp:Label ID="S1_ProvinciaEstesaLabel" runat="server" CssClass="HIC_LabelCorpoTesto1" Text='<%# Eval("S1_ProvinciaEstesa") %>'>
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="S1_ProvinciaSiglaDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Istituto - Provincia (sigla):</asp:Label>
                    <asp:Label ID="S1_ProvinciaSiglaLabel" runat="server" CssClass="HIC_LabelCorpoTesto1" Text='<%# Eval("S1_ProvinciaSigla") %>'>
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="S1_NazioneDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Istituto - Nazione:</asp:Label>
                    <asp:Label ID="S1_NazioneLabel" runat="server" CssClass="HIC_LabelCorpoTesto1" Text='<%# Eval("S1_Nazione") %>'
                        OnDataBinding="Clear">
                    </asp:Label>
                </td>
            </tr>
            <asp:Panel ID="ContinentePanel" runat="server" Visible="false">
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="S1_ContinenteDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Istituto - Continente: </asp:Label>
                        <asp:Label ID="S1_ContinenteLabel" CssClass="HIC_LabelCorpoTesto1" runat="server" Text='<%# Eval("S1_Continente") %>'>
                        </asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <%--<tr>
                <td>
                    <asp:Label ID="S2_IndirizzoDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Sede legale - Indirizzo:</asp:Label>
                    <asp:Label ID="S2_IndirizzoLabel" runat="server" CssClass="HIC_LabelCorpoTesto1" Text='<%# Eval("S2_Indirizzo") %>'>
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="S2_NumeroDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Sede legale - Numero:</asp:Label>
                    <asp:Label ID="S2_NumeroLabel" runat="server" CssClass="HIC_LabelCorpoTesto1" Text='<%# Eval("S2_Numero") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="S2_CittaDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server"> Sede legale - Citta:</asp:Label>
                    <asp:Label ID="S2_CittaLabel" runat="server" CssClass="HIC_LabelCorpoTesto1" Text='<%# Eval("S2_Citta") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="S2_ComuneDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Sede legale - Comune:</asp:Label>
                    <asp:Label ID="S2_ComuneLabel" runat="server" CssClass="HIC_LabelCorpoTesto1" Text='<%# Eval("S2_Comune") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="S2_CapDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Sede legale - CAP:</asp:Label>
                    <asp:Label ID="S2_CapLabel" runat="server" CssClass="HIC_LabelCorpoTesto1" Text='<%# Eval("S2_Cap") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="S2_ProvinciaEstesaDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Sede legale - Provincia:</asp:Label>
                    <asp:Label ID="S2_ProvinciaEstesaLabel" CssClass="HIC_LabelCorpoTesto1" runat="server" Text='<%# Eval("S2_ProvinciaEstesa") %>'>
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="S2_ProvinciaSiglaDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Sede legale - Provincia (sigla):</asp:Label>
                    <asp:Label ID="S2_ProvinciaSiglaLabel" CssClass="HIC_LabelCorpoTesto1" runat="server" Text='<%# Eval("S2_ProvinciaSigla") %>'>
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="S2_NazioneDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Sede legale - Nazione:</asp:Label>
                    <asp:Label ID="S2_NazioneLabel" CssClass="HIC_LabelCorpoTesto1" runat="server" Text='<%# Eval("S2_Nazione") %>'
                        OnDataBinding="Clear">
                    </asp:Label>
                </td>
            </tr>
            <asp:Panel ID="ContinenteSedePanel" runat="server" Visible="false">
                <tr>
                    <td>
                        <asp:Label ID="S2_ContinenteDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Sede legale - Continente:</asp:Label>
                        <asp:Label ID="S2_ContinenteLabel" CssClass="HIC_LabelCorpoTesto1" runat="server" Text='<%# Eval("S2_Continente") %>'>
                        </asp:Label>
                    </td>
                </tr>
            </asp:Panel>--%>
<%--            <tr>
                <td>
                    <asp:Label ID="Telefono3DescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Telefono principale:</asp:Label>
                    <asp:Label ID="Telefono1Label" CssClass="HIC_LabelCorpoTesto1" runat="server" Text='<%# Eval("Telefono1") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Telefono4DescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Telefono alternativo:</asp:Label>
                    <asp:Label ID="Telefono2Label" CssClass="HIC_LabelCorpoTesto1" runat="server" Text='<%# Eval("Telefono2") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="FaxDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Fax:</asp:Label>
                    <asp:Label ID="FaxLabel" CssClass="HIC_LabelCorpoTesto1" runat="server" Text='<%# Eval("Fax") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="WebSiteLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">WebSite:</asp:Label>
                    <asp:HyperLink ID="WebSiteHyperLink" CssClass="HIC_LabelCorpoTesto1" runat="server" NavigateUrl='<%# Eval("WebSite") %>'
                        Text='<%# Eval("WebSite") %>' Target="_blank"></asp:HyperLink>
                </td>
            </tr>--%>
            <tr>
                <td align="center" colspan="2">
                    <asp:Label ID="EnabledLabel" runat="server" Visible="false" Text="Attenzione: dati non presenti."></asp:Label>
                </td>
            </tr>
        </table>
        <div style="text-align: center">
            <%--<asp:LinkButton ID="ModificaLinkButton" runat="server" CssClass="HIC_LinkButton1" Text="Modifica"
                PostBackUrl='<%# Eval("UserId","ProfiloSocietario_Edt.aspx" ) %>'></asp:LinkButton>--%>
            <asp:LinkButton ID="ModificaLinkButton" runat="server" CssClass="ZSSM_Button01_LinkButton" Text="Modifica"
                PostBackUrl='<%# Eval("UserId","ProfiloSocietario_Edt.aspx" ) %>'></asp:LinkButton>
        </div>
    </ItemTemplate>
</asp:FormView>
<asp:SqlDataSource ID="ProfiloSocietarioSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
    SelectCommand="SELECT [UserId], [RagioneSociale], [PartitaIVA], [S1_Indirizzo], [S1_Numero],
     [S1_Citta], [S1_Comune], [S1_Cap], [S1_ProvinciaEstesa], [S1_ProvinciaSigla], [S1_ID2Nazione], 
     [S1_Nazione], [S1_Continente], [S2_Indirizzo], [S2_Numero], [S2_Citta], [S2_Comune], [S2_Cap], 
     [S2_ProvinciaEstesa], [S2_ProvinciaSigla], [S2_ID2Nazione], [S2_Nazione], [S2_Continente], [Telefono1],
      [Telefono2], [Fax], [WebSite],[Identificativo] FROM [vwProfiliSocietari]"></asp:SqlDataSource>
