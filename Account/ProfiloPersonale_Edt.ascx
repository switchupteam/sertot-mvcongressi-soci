﻿<%@ Control Language="C#"
    CodeFile="ProfiloPersonale_Edt.ascx.cs"
    Inherits="Account_ProfiloPersonale_Edt_UC" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<asp:HiddenField ID="UIDHiddenField" runat="server" />
<asp:FormView ID="ProfiloPersonaleFormView" DefaultMode="Edit" runat="server" Width="100%">
    <EditItemTemplate>
        <asp:SqlDataSource ID="dsNazioni2" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
            SelectCommand="SELECT [ID1Nazione], [Nazione] FROM [tbNazioni] ORDER BY [Nazione]"></asp:SqlDataSource>
        <div class="ZITC010">
            <div class="TFY_Titolo3">
                <asp:Label ID="UtenteLabel21" runat="server" CssClass="TFY_Sottotitolo3" Text="Utente"></asp:Label>
            </div>
            <table cellpadding="2" cellspacing="1" border="0">
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="CognomeDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Cognome *</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxCognome" Text='<%# Bind("Cognome") %>' runat="server" Columns="50"
                            MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator class="HIC_Validator1" Display="Dynamic" ID="RequiredFieldValidator1"
                            runat="server" ControlToValidate="TextBoxCognome" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="NomeDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Nome *</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxNome" runat="server" Text='<%# Bind("Nome") %>' Columns="50"
                            MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator class="HIC_Validator1" Display="Dynamic" ID="RequiredFieldValidator2"
                            runat="server" ControlToValidate="TextBoxNome" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                    </td>
                </tr>

                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="ProfessioneDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Professione</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxQualifica" Text='<%# Bind("Professione") %>' runat="server"
                            Columns="20" MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="QualificaDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Qualifica</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBox1" Text='<%# Bind("Qualifica") %>' runat="server"
                            Columns="10" MaxLength="10"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="SessoDescriptionLabel" runat="server" Text="Sesso"></asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:DropDownList ID="SessoDropDownlist" runat="server" SelectedValue='<%# Bind("Sesso") %>' CssClass="HIC_DropDown1">
                            <asp:ListItem Text="Non inserito" Value="X"></asp:ListItem>
                            <asp:ListItem Text="Maschio" Value="M"></asp:ListItem>
                            <asp:ListItem Text="Femmina" Value="F"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="CodiceFiscaleDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Codice fiscale personale</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxCodiceFiscale" Text='<%# Bind("CodiceFiscale") %>' runat="server"
                            Columns="16" MaxLength="16"></asp:TextBox>
                        <%--<asp:RegularExpressionValidator class="HIC_Validator1" Display="Dynamic" ID="CodiceFiscaleRegularExpressionValidator"
                            runat="server" ControlToValidate="TextBoxCodiceFiscale" ErrorMessage="Codice fiscale non corretto"
                            ValidationExpression="^[A-Za-z]{6}\d{2}[A-Za-z]\d{2}[A-Za-z]\d{3}[A-Za-z]$"></asp:RegularExpressionValidator>--%>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="PartitaIVADescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Partita IVA</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxPartitaIVA" Text='<%# Bind("PartitaIVA") %>' runat="server"
                            Columns="16" MaxLength="16"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>
        <div class="ZITC010">
            <div class="TFY_Titolo3">
                <asp:Label ID="Label5" runat="server" CssClass="TFY_Sottotitolo3" Text="Residenza"></asp:Label>
            </div>
            <table cellpadding="2" cellspacing="1" border="0">
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="IndirizzoDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Indirizzo</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxIndirizzo" Text='<%# Bind("Indirizzo") %>' runat="server"
                            Columns="50" MaxLength="50"></asp:TextBox>
                        <asp:Label ID="NumeroDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server"> Numero</asp:Label>
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxNumero" Text='<%# Bind("Numero") %>' runat="server" Columns="10"
                            MaxLength="10"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="CittaDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Città</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxCitta" Text='<%# Bind("Citta") %>' runat="server" Columns="50"
                            MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="ComuneDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Comune</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxComune" runat="server" Columns="50" MaxLength="50" Text='<%# Bind("Comune") %>'></asp:TextBox>
                        <asp:Label ID="CapDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">&nbsp;CAP</asp:Label>
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxCAP" runat="server" Columns="5" MaxLength="5" Text='<%# Bind("Cap") %>'></asp:TextBox>
                        <asp:RangeValidator class="HIC_Validator1" Display="Dynamic" ID="RangeValidator1"
                            runat="server" ControlToValidate="TextBoxCAP" ErrorMessage="CAP non corretto"
                            MaximumValue="99999" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="ProvinciaDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Provincia</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxProvincia" Text='<%# Bind("ProvinciaEstesa") %>' runat="server"
                            Columns="50" MaxLength="50"></asp:TextBox>
                        <asp:Label ID="ProvinciaSiglaDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">&nbsp;Sigla provincia</asp:Label>
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxSiglaProvincia" runat="server" Columns="2" MaxLength="2"
                            Text='<%# Bind("ProvinciaSigla") %>'></asp:TextBox>
                        <asp:RegularExpressionValidator class="HIC_Validator1" Display="Dynamic" ID="RegularExpressionValidator3"
                            runat="server" ControlToValidate="TextBoxSiglaProvincia" ErrorMessage="Sigla provincia non corretta"
                            ValidationExpression="^[A-Za-z]{2}$"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="NazioneDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Nazione</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:DropDownList ID="DropDownListNazione" runat="server" DataSourceID="dsNazioni2"
                            SelectedValue='<%# Bind("ID2Nazione") %>'
                            DataValueField="ID1Nazione"
                            DataTextField="Nazione">
                            <asp:ListItem Text="&gt;  Non inserita" Selected="True" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </div>
        <div class="ZITC010">
            <div class="TFY_Titolo3">
                <asp:Label ID="NascitaLabel14" runat="server" CssClass="TFY_Sottotitolo3" Text="Nascita"></asp:Label>
            </div>
            <table cellpadding="2" cellspacing="1" border="0">
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="NascitaDataDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Data</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxData" Text='<%# Bind("NascitaData") %>' runat="server" Columns="10"
                            MaxLength="10" OnDataBinding="TextBoxData_DataBinding"></asp:TextBox>
                        <asp:RegularExpressionValidator class="HIC_Validator1" Display="Dynamic" ID="RegularExpressionValidator2"
                            runat="server" ControlToValidate="TextBoxData" ErrorMessage="Fomato data richiesto: GG/MM/AAAA"
                            ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="NascitaCittaDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Città</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxCittaNascita" Text='<%# Bind("NascitaCitta") %>' runat="server"
                            Columns="50" Rows="50"></asp:TextBox>
                        <asp:Label ID="NascitaProvinciaDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">&nbsp;Sigla provincia</asp:Label>
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxProvinciaNascita" Text='<%# Bind("NascitaProvinciaSigla") %>'
                            runat="server" Columns="2" MaxLength="2"></asp:TextBox>
                        <asp:RegularExpressionValidator class="HIC_Validator1" Display="Dynamic" ID="RegularExpressionValidator5"
                            runat="server" ControlToValidate="TextBoxProvinciaNascita" ErrorMessage="Sigla provincia non corretta"
                            ValidationExpression="^[A-Za-z]{2}$"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="NascitaNazioneDEscriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Nazione</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:DropDownList ID="DropDownListNazioneNascita" runat="server" DataSourceID="dsNazioni2"
                            SelectedValue='<%# Bind("NascitaID2Nazione") %>' DataTextField="Nazione" DataValueField="ID1Nazione">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </div>
        <div class="ZITC010">
            <div class="TFY_Titolo3">
                <asp:Label ID="ContattiLabel14" runat="server" CssClass="TFY_Sottotitolo3" Text="Contatti"></asp:Label>
            </div>
            <table cellpadding="2" cellspacing="1" border="0">
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Telefono1DescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Telefono</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxTelefonoCasa" Text='<%# Bind("Telefono1") %>' runat="server"
                            Columns="20" MaxLength="20"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Telefono2DescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Telefono cellulare</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxTelefonoCellulare" Text='<%# Bind("Telefono2") %>' runat="server"
                            Columns="20" Rows="20"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="FaxDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Fax</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxFax" runat="server" Text='<%# Bind("Fax") %>' Columns="20"
                            MaxLength="20"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="NoteDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Note</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <CustomWebControls:TextArea ID="TextArea1" runat="server" Text='<%# Bind("Note") %>'
                            Width="300" Rows="5" Height="100" TextMode="MultiLine" MaxLength="199"></CustomWebControls:TextArea>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="AffiliazioneDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Affiliazione</asp:Label>
                    </td>
                    <td>
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxAffiliazione" runat="server" Text='<%# Bind("Affiliazione") %>' Columns="50"
                            MaxLength="200"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="SitoWebDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Sito web</asp:Label>
                    </td>
                    <td>
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxSitoWeb" runat="server" Text='<%# Bind("SitoWeb") %>' Columns="50"
                            MaxLength="200"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>
        <asp:HiddenField ID="RecordEdtDate" runat="server" Value='<%# Bind("RecordEdtDate") %>'
            OnDataBinding="RecordEdtDate_DataBinding" />
        <asp:HiddenField ID="RecordEdtUser" runat="server" Value='<%# Bind("RecordEdtUser") %>'
            OnDataBinding="RecordEdtUser_DataBinding" />
        <div align="center" style="margin: 0; padding: 0 0 8px 0; border: 0px; display: block;">
            <%--<asp:LinkButton ID="UpdateButton" runat="server" CssClass="HIC_LinkButton1" CommandName="Update"
                Text="Salva dati"></asp:LinkButton>--%>
            <asp:LinkButton ID="UpdateButton" runat="server" CssClass="ZSSM_Button01_LinkButton" CommandName="Update"
                Text="Salva dati"></asp:LinkButton>
        </div>
        <asp:HiddenField ID="zdtRecordNewUser" runat="server" Value='<%# Eval("RecordNewUser") %>' />
        <asp:HiddenField ID="zdtRecordNewDate" runat="server" Value='<%# Eval("RecordNewDate") %>' />
        <asp:HiddenField ID="zdtRecordEdtUser" runat="server" Value='<%# Eval("RecordEdtUser") %>' />
        <asp:HiddenField ID="zdtRecordEdtDate" runat="server" Value='<%# Eval("RecordEdtDate") %>' />
    </EditItemTemplate>
</asp:FormView>
<asp:SqlDataSource ID="ProfiloPersonaleSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
    SelectCommand="SELECT [UserId], [Cognome], [Nome], [CodiceFiscale], [PartitaIVA], [Qualifica],[Professione],
    [Sesso], [Indirizzo], [Numero], [Citta], 
    [Cap], [ProvinciaSigla], [ID2Nazione],[Comune], [NascitaData],[ProvinciaEstesa], [NascitaProvinciaSigla], 
    [NascitaID2Nazione],[NascitaCitta], [Telefono1], [Telefono2], [Fax], [Note], [Affiliazione], [SitoWeb],
    [RecordNewDate],[RecordNewUser],[RecordEdtDate],[RecordEdtUser] FROM [tbProfiliPersonali]"
    UpdateCommand=" SET DATEFORMAT dmy; UPDATE tbProfiliPersonali SET Cognome =@Cognome, Nome =@Nome,CodiceFiscale =@CodiceFiscale,PartitaIVA=@PartitaIVA,
     Qualifica =@Qualifica,Professione=@Professione,Sesso =@Sesso, Indirizzo =@Indirizzo, Numero =@Numero, Citta =@Citta, Comune =@Comune, Cap =@Cap,
      ProvinciaEstesa =@ProvinciaEstesa, ProvinciaSigla =@ProvinciaSigla, ID2Nazione =@ID2Nazione, NascitaData =@NascitaData,
       NascitaCitta =@NascitaCitta, NascitaProvinciaSigla =@NascitaProvinciaSigla, NascitaID2Nazione =@NascitaID2Nazione, 
       Telefono1 =@Telefono1, Telefono2 =@Telefono2, Fax =@Fax, Note =@Note, Affiliazione = @Affiliazione, SitoWeb = @SitoWeb,
       RecordEdtUser =@RecordEdtUser, RecordEdtDate =@RecordEdtDate WHERE [UserID] =@UserID"
    OnUpdated="ProfiloPersonaleSqlDataSource_Updated">
    <UpdateParameters>
        <asp:Parameter Name="Cognome" Type="string" />
        <asp:Parameter Name="Nome" Type="string" />
        <asp:Parameter Name="CodiceFiscale" Type="string" />
        <asp:Parameter Name="PartitaIVA" Type="string" />
        <asp:Parameter Name="Qualifica" Type="string" />
        <asp:Parameter Name="Professione" Type="string" />
        <asp:Parameter Name="Sesso" Type="string" />
        <asp:Parameter Name="Indirizzo" Type="string" />
        <asp:Parameter Name="Numero" />
        <asp:Parameter Name="Citta" Type="string" />
        <asp:Parameter Name="Cap" />
        <asp:Parameter Name="ProvinciaSigla" Type="string" />
        <asp:Parameter Name="ID2Nazione" />
        <asp:Parameter Name="NascitaData" Type="datetime" />
        <asp:Parameter Name="NascitaProvinciaSigla" Type="string" />
        <asp:Parameter Name="NascitaID2Nazione" />
        <asp:Parameter Name="Telefono1" />
        <asp:Parameter Name="Telefono2" />
        <asp:Parameter Name="Fax" />
        <asp:Parameter Name="Note" />
        <asp:Parameter Name="Affiliazione" />
        <asp:Parameter Name="SitoWeb" />
        <asp:Parameter Name="RecordEdtUser" />
        <asp:Parameter Name="RecordEdtDate" Type="datetime" />
        <asp:ControlParameter ControlID="UIDHiddenField" Name="UserID" />
    </UpdateParameters>
</asp:SqlDataSource>
