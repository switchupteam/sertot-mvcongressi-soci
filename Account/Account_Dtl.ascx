<%@ Control Language="C#"
    CodeFile="Account_Dtl.ascx.cs"
    Inherits="Account_Account_Dtl" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<asp:HiddenField ID="UID" runat="server" />
<asp:Panel ID="UserAlivePanel" runat="server" Visible="true">
    <asp:FormView ID="MemberShipFormView" runat="server"
        DefaultMode="ReadOnly"
        DataSourceID="MemberShipSqlDataSource"
        OnDataBound="MemberShipFormView_DataBound"
        Width="100%" CellPadding="0">
        <ItemTemplate>
            <div class="BXT_001">
                <div class="TFY_Titolo3">
                    <asp:Label ID="AccountUtenteTitleLabel" runat="server" CssClass="TFY_Sottotitolo3" Text="Account utente"></asp:Label>
                </div>
            </div>
            <div class="ZITC010">
                <table border="0" cellpadding="2" cellspacing="1">
                    <tr>
                        <td>
                            <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="UserNameDescriptionLabel" runat="server" Text="Label">UserName:</asp:Label>
                            <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="UserNameLabel" runat="server" Text='<%# Eval("UserName", "{0}") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="EMailDescriptionLabel" runat="server" Text="Label">E-mail: </asp:Label>
                            <asp:HyperLink CssClass="HIC_LabelCorpoTesto1" ID="EmailHyperLink" runat="server"
                                NavigateUrl='<%# Eval("Email", "mailto:{0}") %>'
                                Text='<%# Eval("Email", "{0}") %>'
                                Target='<%# Eval("Email", "{0}") %>'></asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="DataCambioPasswordDescriptionLabel"
                                CssClass="HIC_LabelCorpoTesto1" runat="server" Text="Label">Data ultimo cambio password:</asp:Label>
                            <asp:Label ID="DataCambiamentoPasswordLabel" CssClass="HIC_LabelCorpoTesto1" runat="server"
                                Text='<%# Eval("LastPasswordChangedDate", "{0:g}") %>'></asp:Label>
                        </td>
                    </tr>
                </table>
                <div style="text-align: center">
                    <%--<asp:LinkButton ID="ModificaEMailLinkButton" runat="server" CssClass="HIC_LinkButton1"
                        Text="Modifica E-Mail" PostBackUrl='<%# Eval("UserId","Account_Email_Edt.aspx" ) %>'>
                    </asp:LinkButton>
                    <img src="../SiteImg/Spc.gif" width="10px" height="10px" />
                    <asp:LinkButton ID="ModificaPasswordLinkButton" runat="server" CssClass="HIC_LinkButton1" Text="Cambia password"
                        PostBackUrl='<%# Eval("UserId","Account_ResetPsw.aspx" ) %>'></asp:LinkButton>--%>

                    <asp:LinkButton ID="ModificaEMailLinkButton" runat="server" CssClass="ZSSM_Button01_LinkButton"
                        Text="Modifica E-Mail" PostBackUrl='<%# Eval("UserId","Account_Email_Edt.aspx" ) %>'>
                    </asp:LinkButton>
                    <img src="../SiteImg/Spc.gif" width="10px" height="10px" />
                    <asp:LinkButton ID="ModificaPasswordLinkButton" runat="server" CssClass="ZSSM_Button01_LinkButton" Text="Cambia password"
                        PostBackUrl='<%# Eval("UserId","Account_ResetPsw.aspx" ) %>'></asp:LinkButton>
                </div>
            </div>
        </ItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="MemberShipSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SELECT aspnet_Membership.Password, aspnet_Membership.Email
        , aspnet_Membership.IsApproved, aspnet_Membership.CreateDate, aspnet_Membership.LastLoginDate
        , aspnet_Membership.LastPasswordChangedDate
        , aspnet_Membership.LastLockoutDate, aspnet_Membership.FailedPasswordAttemptCount, aspnet_Users.UserName
        , aspnet_Membership.UserId , aspnet_Membership.IsLockedOut FROM aspnet_Membership INNER JOIN aspnet_Users ON aspnet_Membership.UserId = aspnet_Users.UserId 
        WHERE (aspnet_Users.UserId = @UserID)"
        OnSelected="MemberShipSqlDataSource_Selected">
        <SelectParameters>
            <asp:ControlParameter Name="UserID" ControlID="UID" PropertyName="Value" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Panel>
