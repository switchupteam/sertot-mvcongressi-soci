﻿<%@ Control Language="C#" CodeFile="ProfiloPersonaleImg_Dtl.ascx.cs" Inherits="Account_ProfiloPersonaleImg_Dtl" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<asp:FormView ID="ProfiloPersonaleImgFormView" runat="server" Width="100%">
    <ItemTemplate>
        <div class="BXT_001">
            <div class="TFY_Titolo3">
                <asp:Label ID="ProfiloPersonaleImgLabel" runat="server" CssClass="TFY_Sottotitolo3" Text="Immagine"></asp:Label>
            </div>
        </div>
        <table border="0" cellpadding="2" cellspacing="1">
            <tr>
                <td align="center" colspan="2">
                    <img runat="server" src='<%# Eval("ImmagineSocio") %>' />
                </td>
            </tr>            
            <tr>
                <td align="center" colspan="2">
                    <asp:Label ID="EnabledLabel" runat="server" Visible="false" Text="Attenzione: dati non presenti."></asp:Label>
                </td>
            </tr>
        </table>
        <div style="text-align: center">            
            <asp:LinkButton ID="ModificaLinkButton" runat="server" CssClass="ZSSM_Button01_LinkButton" Text="Aggiorna"
                PostBackUrl='<%# Eval("UserId","ProfiloPersonaleImg_Edt.aspx?ZIM=PFIMG" ) %>'></asp:LinkButton>
        </div>
    </ItemTemplate>
</asp:FormView>
<asp:SqlDataSource ID="ProfiloPersonaleImgSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
    SelectCommand="SELECT [UserId], '/ZeusInc/Account/ImgSoci/Img1/' + [ImmagineSocio] AS ImmagineSocio FROM [vwProfiliPersonali]"></asp:SqlDataSource>
