﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProfiloPersonaleImg_Edt.ascx.cs" Inherits="Account_ProfiloPersonaleImg_Edt" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<script type="text/javascript">
    function OnClientModeChange(editor) {
        var mode = editor.get_mode();
        var doc = editor.get_document();
        var head = doc.getElementsByTagName("HEAD")[0];
        var link;

        switch (mode) {
            case 1: //remove the external stylesheet when displaying the content in Design mode    
                //var external = doc.getElementById("external");
                //head.removeChild(external);
                break;
            case 2:
                break;
            case 4: //apply your external css stylesheet to Preview mode    
                link = doc.createElement("LINterK");
                link.setAttribute("href", "/SiteCss/Telerik.css");
                link.setAttribute("rel", "stylesheet");
                link.setAttribute("type", "text/css");
                link.setAttribute("id", "external");
                head.appendChild(link);
                break;
        }
    }

    function editorCommandExecuted(editor, args) {
        if (!$telerik.isChrome)
            return;
        var dialogName = args.get_commandName();
        var dialogWin = editor.get_dialogOpener()._dialogContainers[dialogName];
        if (dialogWin) {
            var cellEl = dialogWin.get_contentElement() || dialogWin.ui.contentCell || dialogWin.ui.content,
                frame = dialogWin.get_contentFrame();
            frame.onload = function () {
                cellEl.style.cssText = "";
                dialogWin.autoSize();
            }
        }
    }
</script>

<asp:HiddenField ID="UIDHiddenField" runat="server" />

<asp:FormView ID="ProfiloPersonaleImgFormView" DefaultMode="Edit" runat="server" Width="100%">
    <EditItemTemplate>
        <asp:SqlDataSource ID="dsNazioni2" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
            SelectCommand="SELECT [ID1Nazione], [Nazione] FROM [tbNazioni] ORDER BY [Nazione]"></asp:SqlDataSource>
        <div class="ZITC010">
            <%--<div class="TFY_Titolo3">
                <asp:Label ID="AziendaTitleLabel" runat="server" CssClass="TFY_Sottotitolo3" Text="Azienda / ente / società"></asp:Label>
            </div>--%>
            <table cellpadding="2" cellspacing="1" border="0" style="width: 100%;">
                <tr>
                    <td class="BlockBoxValue">

                        <asp:Panel ID="ZMCF_Immagine1" runat="server">
                            <%--<dlc:ImageRaider ID="ImageRaider1" runat="server" ZeusIdModuloIndice="1" ZeusLangCode="ITA"
                                BindPzFromDB="true" IsEditMode="true" DefaultEditBetaImage='<%# Eval("ImmagineSocio") %>'
                                DefaultEditGammaImage='<%# Eval("Immagine2") %>'
                                ImageAlt='<%# Eval("Immagine12Alt") %>' OnDataBinding="ImageRaider_DataBinding" />--%>
                            <dlc:ImageRaider ID="ImageRaiderCrop1" runat="server" ZeusIdModuloIndice="1" ZeusLangCode="ITA"
                                BindPzFromDB="true" IsEditMode="true" DefaultEditBetaImage='<%# Eval("ImmagineSocio") %>'
                                ImageAlt='<%# Eval("ImmagineSocioAlt") %>' OnDataBinding="ImageRaider_DataBinding" />
                            <asp:HiddenField ID="FileNameBetaHiddenField" runat="server" Value='<%# Bind("ImmagineSocio") %>' />
                            <%--<asp:HiddenField ID="FileNameGammaHiddenField" runat="server" Value='<%# Bind("Immagine2") %>' />--%>
                            <asp:HiddenField ID="Immagine12AltHiddenField" runat="server" Value='<%# Bind("ImmagineSocioAlt") %>' />
                            <asp:CustomValidator ID="CustomValidator1" runat="server" OnServerValidate="FileUploadCustomValidator_ServerValidate"
                                Display="Dynamic" SkinID="ZSSM_Validazione01" ValidationGroup="myValidation"
                                Visible="false"></asp:CustomValidator>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </div>
        <asp:HiddenField ID="RecordEdtDate" runat="server" Value='<%# Bind("RecordEdtDate") %>'
            OnDataBinding="RecordEdtDate_DataBinding" />
        <asp:HiddenField ID="RecordEdtUser" runat="server" Value='<%# Bind("RecordEdtUser") %>'
            OnDataBinding="RecordEdtUser_DataBinding" />
        <div align="center" style="margin: 0; padding: 0 0 8px 0; border: 0px; display: block;">

            <asp:LinkButton ID="UpdateButton" CssClass="ZSSM_Button01_LinkButton" runat="server" CommandName="Update"
                Text="Salva dati" OnClick="Save_File_Upload"></asp:LinkButton>
        </div>
        <asp:HiddenField ID="zdtRecordNewUser" runat="server" Value='<%# Eval("RecordNewUser") %>' />
        <asp:HiddenField ID="zdtRecordNewDate" runat="server" Value='<%# Eval("RecordNewDate") %>' />
        <asp:HiddenField ID="zdtRecordEdtUser" runat="server" Value='<%# Eval("RecordEdtUser") %>' />
        <asp:HiddenField ID="zdtRecordEdtDate" runat="server" Value='<%# Eval("RecordEdtDate") %>' />
    </EditItemTemplate>
</asp:FormView>
<asp:SqlDataSource ID="ProfiloPersonaleImgSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
    SelectCommand="SELECT ImmagineSocio,ImmagineSocioAlt,RecordNewUser,RecordNewDate,RecordEdtUser, RecordEdtDate
        FROM tbProfiliPersonali"
    UpdateCommand=" SET DATEFORMAT dmy; 
        UPDATE tbProfiliPersonali 
        SET ImmagineSocio=@ImmagineSocio, ImmagineSocioAlt=@ImmagineSocioAlt, RecordEdtUser=@RecordEdtUser, RecordEdtDate=@RecordEdtDate
        WHERE [UserID] =@UserID"
    OnUpdated="ProfiloPersonaleImgSqlDataSource_Updated">
    <UpdateParameters>
        <asp:Parameter Name="ImmagineSocio" />
        <asp:Parameter Name="ImmagineSocioAlt" />
        <asp:Parameter Name="RecordEdtUser" />
        <asp:Parameter Name="RecordEdtDate" Type="DateTime" />
        <asp:ControlParameter ControlID="UIDHiddenField" Name="UserID" />
    </UpdateParameters>
</asp:SqlDataSource>
