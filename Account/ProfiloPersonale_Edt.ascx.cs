﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per ProfiloPersonale_Edt
/// </summary>
public partial class Account_ProfiloPersonale_Edt_UC : System.Web.UI.UserControl
{

    #region Fields

    private string UserID = string.Empty;
    private string myRedirectPath = string.Empty;
    private string lang = "ITA";

    #endregion

    #region Properties

    public string UID
    {
        set { UserID = value; }
        get { return UserID; }
    }

    public string RedirectPath
    {
        set { myRedirectPath = value; }
        get { return myRedirectPath; }
    }

    public string Lang
    {
        set { lang = value; }
        get { return lang; }
    }

    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            BindTheData();
            ReadXML_Localization("PROFILI", lang);
        }
    }

    protected void RecordEdtUser_DataBinding(object sender, EventArgs e)
    {
        HiddenField UtenteCreazione = (HiddenField)sender;
        MembershipUser user = Membership.GetUser();
        object userKey = user.ProviderUserKey;
        UtenteCreazione.Value = userKey.ToString();
    }

    protected void RecordEdtDate_DataBinding(object sender, EventArgs e)
    {
        HiddenField DataCreazione = (HiddenField)sender;
        DataCreazione.Value = DateTime.Now.ToString();
    }

    protected void TextBoxData_DataBinding(object sender, EventArgs e)
    {
        TextBox TextBoxData = (TextBox)sender;
        try
        {
            TextBoxData.Text = TextBoxData.Text.Substring(0, 10);
        }
        catch (Exception) { }
    }

    protected void ProfiloPersonaleSqlDataSource_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.Exception == null)
            if (myRedirectPath.Length > 0)
                Response.Redirect(myRedirectPath);
            else Response.Redirect("~/Zeus/System/Message.aspx");
    }

    #endregion

    #region Methods

    public bool BindTheData()
    {
        try
        {
            ProfiloPersonaleSqlDataSource.SelectCommand += " WHERE ([UserID] = '" + UserID + "')";
            ProfiloPersonaleFormView.DataSourceID = "ProfiloPersonaleSqlDataSource";
            ProfiloPersonaleFormView.DataBind();
            UIDHiddenField.Value = UserID;

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";
            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode + "/PROFILO_PERSONALE";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Profili.xml"));

            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                            if (myLabel != null)
                                myLabel.Text = childNode.InnerText;
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name + "Label");

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }

            LinkButton UpdateButton = (LinkButton)ProfiloPersonaleFormView.FindControl("UpdateButton");
            UpdateButton.Text = (mydoc.SelectSingleNode(myXPath + "/" + "UpdateButton").InnerText);
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    #endregion
       
}