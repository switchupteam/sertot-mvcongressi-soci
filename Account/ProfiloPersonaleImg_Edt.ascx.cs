﻿using ASP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_ProfiloPersonaleImg_Edt : System.Web.UI.UserControl
{
    #region Fields

    private string UserID = string.Empty;
    private string myRedirectPath = string.Empty;
    private string lang = "ITA";

    #endregion

    #region Properties

    public string UID
    {
        set { UserID = value; }
        get { return UserID; }
    }

    public string RedirectPath
    {
        set { myRedirectPath = value; }
        get { return myRedirectPath; }
    }

    public string Lang
    {
        set { lang = value; }
        get { return lang; }
    }

    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindTheData();
            //ReadXML_Localization("PROFILI", lang);
        }
    }

    protected void RecordEdtUser_DataBinding(object sender, EventArgs e)
    {
        HiddenField UtenteCreazione = (HiddenField)sender;
        MembershipUser user = Membership.GetUser();
        object userKey = user.ProviderUserKey;

        UtenteCreazione.Value = userKey.ToString();
    }

    protected void RecordEdtDate_DataBinding(object sender, EventArgs e)
    {
        HiddenField DataCreazione = (HiddenField)sender;
        DataCreazione.Value = DateTime.Now.ToString();
    }

    protected void TextBoxNumero_DataBinding(object sender, EventArgs e)
    {
        TextBox TextBoxNumero = (TextBox)sender;
        try
        {
            TextBoxNumero.Text = TextBoxNumero.Text.Substring(0, 10);
        }
        catch (Exception) { }
    }

    protected void ProfiloPersonaleImgSqlDataSource_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.Exception == null)
            if (myRedirectPath.Length > 0)
                Response.Redirect(myRedirectPath);
            else Response.Redirect("~/Zeus/System/Message.aspx");
    }

    #endregion

    #region Methods

    public bool BindTheData()
    {
        try
        {
            ProfiloPersonaleImgSqlDataSource.SelectCommand += " WHERE ([UserID] = '" + UserID + "')";
            ProfiloPersonaleImgFormView.DataSourceID = "ProfiloPersonaleImgSqlDataSource";
            ProfiloPersonaleImgFormView.DataBind();
            UIDHiddenField.Value = UserID;

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";
            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode + "/AFFILIAZIONE";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Profili.xml"));

            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                            if (myLabel != null)
                                myLabel.Text = childNode.InnerText;
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name + "Label");

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }

            LinkButton UpdateButton = (LinkButton)ProfiloPersonaleImgFormView.FindControl("UpdateButton");
            UpdateButton.Text = (mydoc.SelectSingleNode(myXPath + "/" + "UpdateButton").InnerText);
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }


    //##############################################################################################################
    //################################################ FILE UPLOAD #################################################
    //############################################################################################################## 


    protected void ImageRaider_DataBinding(object sender, EventArgs e)
    {
        ImageRaider ImageRaider1 = (ImageRaider)sender;
        ImageRaider1.SetDefaultEditBetaImage();
        ImageRaider1.SetDefaultEditGammaImage();

    }

    protected void FileUploadCustomValidator_ServerValidate(object sender, ServerValidateEventArgs args)
    {
        try
        {
            ImageRaider ImageRaider1 = (ImageRaider)ProfiloPersonaleImgFormView.FindControl("ImageRaider1");
            LinkButton InsertButton = (LinkButton)ProfiloPersonaleImgFormView.FindControl("InsertButton");

            if (ImageRaider1.isValid())
                InsertButton.CommandName = "Update";
            else
                InsertButton.CommandName = string.Empty;

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine FileUploadCustomValidator


    protected void Save_File_Upload(object sender, EventArgs e)
    {
        try
        {
            HiddenField FileNameBetaHiddenField = (HiddenField)ProfiloPersonaleImgFormView.FindControl("FileNameBetaHiddenField");
            //HiddenField FileNameGammaHiddenField = (HiddenField)ProfiloPersonaleImgFormView.FindControl("FileNameGammaHiddenField");
            HiddenField Immagine12AltHiddenField = (HiddenField)ProfiloPersonaleImgFormView.FindControl("Immagine12AltHiddenField");
            ImageRaider ImageRaider1 = (ImageRaider)ProfiloPersonaleImgFormView.FindControl("ImageRaiderCrop1");

            LinkButton InsertButton = (LinkButton)ProfiloPersonaleImgFormView.FindControl("UpdateButton");

            if (InsertButton.CommandName != string.Empty)
            {

                if (ImageRaider1.GenerateBeta(string.Empty))
                    FileNameBetaHiddenField.Value = ImageRaider1.ImgBeta_FileName;
                else FileNameBetaHiddenField.Value = ImageRaider1.DefaultBetaImage;
            }//fine if command

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }//fine Save_File_Upload    


    #endregion

}