﻿<%@ Control Language="C#" ClassName="ProfiloSocietario_Edt" 
    CodeFile="ProfiloSocietario_Edt.ascx.cs"
    Inherits="Account_ProfiloSocietario_Edt_UC"%>

<%@ Import Namespace="System.Data.SqlClient" %>

<asp:HiddenField ID="UIDHiddenField" runat="server" />
<asp:FormView ID="ProfiloSocietarioFormView" DefaultMode="Edit" runat="server" Width="100%">
    <EditItemTemplate>
        <asp:SqlDataSource ID="dsNazioni2" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
            SelectCommand="SELECT [ID1Nazione], [Nazione] FROM [tbNazioni] ORDER BY [Nazione]">
        </asp:SqlDataSource>
        <div class="ZITC010">
            <div class="TFY_Titolo3">
                <asp:Label ID="AziendaTitleLabel" runat="server" CssClass="TFY_Sottotitolo3" Text="Azienda / ente / società"></asp:Label></div>
            <table cellpadding="2" cellspacing="1" border="0">
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="RagioneSocialeDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server" Text="Label">Ragione sociale</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxCognome" Text='<%# Bind("RagioneSociale") %>' runat="server"
                            Columns="50" MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                <asp:Panel ID="ZMCF_BusinessBook" runat="server">
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="IdentificativoDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server" Text="Label">Azienda collegata</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:DropDownList ID="AziendaCollegataDropDownList" runat="server" DataSourceID="AziendaCollegataSqlDataSource"
                                CssClass="HIC_DropDown1" DataTextField="Identificativo" DataValueField="ID1BsnBook"
                                AppendDataBoundItems="true" SelectedValue='<%# Bind("ID2BusinessBook") %>'>
                                <asp:ListItem Text="&gt; Non inserita" Value="0" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="AziendaCollegataSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                SelectCommand="SELECT [ID1BsnBook],[Identificativo]
                                FROM [tbBusinessBook]
                                WHERE [PW_Zeus1]=1"></asp:SqlDataSource>
                        </td>
                    </tr>
                </asp:Panel>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="PartitaIVADescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server" Text="Label">Partita IVA o codice fiscale</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxNome" runat="server" Text='<%# Bind("PartitaIVA") %>' Columns="50"
                            MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="S1_IndirizzoDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server" Text="Label">Indirizzo</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxIndirizzo" Text='<%# Bind("S1_Indirizzo") %>' runat="server"
                            Columns="50" MaxLength="50"></asp:TextBox>
                        <asp:Label ID="S1_NumeroDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server" Text="Label">Numero</asp:Label>
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxNumero" Text='<%# Bind("S1_Numero") %>' runat="server" Columns="10"
                            MaxLength="10"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="S1_CittaDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server" Text="Label">Città</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxCitta" Text='<%# Bind("S1_Citta") %>' runat="server" Columns="50"
                            MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="S1_ComuneDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server" Text="Label">Comune</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxComune" runat="server" Columns="50" MaxLength="50" Text='<%# Bind("S1_Comune") %>'></asp:TextBox>
                        <asp:Label ID="S1_CapDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server" Text="Label">CAP</asp:Label>
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxCAP" runat="server" Columns="5" MaxLength="5" Text='<%# Bind("S1_Cap") %>'></asp:TextBox>
                        <asp:RangeValidator class="HIC_Validator1" Display="Dynamic" ID="RangeValidator1"
                            runat="server" ControlToValidate="TextBoxCAP" ErrorMessage="CAP non corretto"
                            MaximumValue="99999" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="S1_ProvinciaEstesaDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server" Text="Label">Provincia</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxProvincia" Text='<%# Bind("S1_ProvinciaEstesa") %>' runat="server"
                            Columns="50" MaxLength="50"></asp:TextBox>
                        <asp:Label ID="S1_ProvinciaSiglaDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server" Text="Label">Sigla provincia</asp:Label>
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxSiglaProvincia" runat="server" Columns="2" MaxLength="2"
                            Text='<%# Bind("S1_ProvinciaSigla") %>'></asp:TextBox>
                        <asp:RegularExpressionValidator class="HIC_Validator1" Display="Dynamic" ID="RegularExpressionValidator3"
                            runat="server" ControlToValidate="TextBoxSiglaProvincia" ErrorMessage="Sigla provincia non corretta"
                            ValidationExpression="^[A-Za-z]{2}$"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="S1_NazioneDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server" Text="Label">Nazione</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:DropDownList ID="DropDownListNazione" runat="server" DataSourceID="dsNazioni2"
                            DataTextField="Nazione" DataValueField="ID1Nazione" SelectedValue='<%# Bind("S1_ID2Nazione") %>'>
                            <asp:ListItem Text="&gt;  Non inserita" Selected="True" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                            SelectCommand="SELECT [ID1Nazione], [Nazione] FROM [tbNazioni] ORDER BY [Nazione]">
                        </asp:SqlDataSource>
                    </td>
                </tr>
            </table>
        </div>
       <%-- <div class="ZITC010">
            <div class="TFY_Titolo3">
                <asp:Label ID="SedeLegaleTitleLabel" runat="server" CssClass="TFY_Sottotitolo3" Text="Sede legale"></asp:Label></div>
            <table cellpadding="2" cellspacing="1" border="0">
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="S2_IndirizzoDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server" Text="Label">Indirizzo</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxData" Text='<%# Bind("S2_Indirizzo") %>' runat="server" Columns="50"
                            MaxLength="50"></asp:TextBox>
                        <asp:Label ID="S2_NumeroDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server" Text="Label">Numero</asp:Label>
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxNumeroUfficio" runat="server" Text='<%# Bind("S2_Numero") %>'
                            OnDataBinding="TextBoxNumero_DataBinding" Columns="10" MaxLength="10"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="S2_CittaDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server" Text="Label">Città</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxCittaUfficio" Text='<%# Bind("S2_Citta") %>' runat="server"
                            Columns="50" MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="S2_ComuneDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server" Text="Label">Comune</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxComuneUfficio" Text='<%# Bind("S2_Comune") %>' runat="server"
                            Columns="50" MaxLength="50"></asp:TextBox>
                        <asp:Label ID="S2_CapDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server" Text="Label">CAP</asp:Label>
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxCapUfficio" Text='<%# Bind("S2_Cap") %>' runat="server" Columns="5"
                            MaxLength="5"></asp:TextBox>
                        <asp:RangeValidator class="HIC_Validator1" Display="Dynamic" ID="RangeValidator3"
                            runat="server" ControlToValidate="TextBoxCapUfficio" ErrorMessage="CAP non corretto"
                            MaximumValue="99999" MinimumValue="0"></asp:RangeValidator>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="S2_ProvinciaEstesaDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server" Text="Label">Provincia</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxProvinciaUfficio" runat="server" Columns="50" MaxLength="50"
                            Text='<%# Bind("S2_ProvinciaEstesa") %>'></asp:TextBox>
                        <asp:Label ID="S2_ProvinciaSiglaDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server" Text="Label">Sigla provincia</asp:Label>
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxSiglaUfficio" runat="server" Columns="2" MaxLength="2" Text='<%# Bind("S2_ProvinciaSigla") %>'></asp:TextBox>
                        <asp:RegularExpressionValidator class="HIC_Validator1" Display="Dynamic" ID="RegularExpressionValidator7"
                            runat="server" ControlToValidate="TextBoxSiglaUfficio" ErrorMessage="Sigla provincia non corretta"
                            ValidationExpression="^[A-Za-z]{2}$"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="S2_NazioneDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Nazione</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:DropDownList ID="DropDownListNazioneNascita" runat="server" DataSourceID="dsNazioni2"
                                DataTextField="Nazione" DataValueField="ID1Nazione" SelectedValue='<%# Bind("S2_ID2Nazione") %>'>
                            </asp:DropDownList>
                        </td>
                    </tr>
            </table>
        </div>--%>
        <div class="ZITC010">
            <div class="TFY_Titolo3">
                <asp:Label ID="ContattiTitleLabel" runat="server" CssClass="TFY_Sottotitolo3" Text="Contatti"></asp:Label></div>
            <table cellpadding="2" cellspacing="1" border="0">
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Telefono3DescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Telefono principale</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxTelefonoCasa" Text='<%# Bind("Telefono1") %>' runat="server"
                            Columns="20" MaxLength="20"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Telefono4DescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Telefono alternativo</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxTelefonoCellulare" Text='<%# Bind("Telefono2") %>' runat="server"
                            Columns="20" Rows="20"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="FaxDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Fax</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox CssClass="HIC_TextBox1" ID="TextBoxFax" runat="server" Text='<%# Bind("Fax") %>' Columns="20"
                            MaxLength="20"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="WebSiteLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Sito web</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox CssClass="HIC_TextBox1" ID="WebSiteTextBox" runat="server" Text='<%# Bind("WebSite") %>' Columns="75"
                            MaxLength="100"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="WebSiteRegularExpressionValidator" runat="server"
                            ValidationExpression="(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?"
                            ErrorMessage="Indirizzo web non corretto" ControlToValidate="WebSiteTextBox"
                            class="HIC_Validator1" Display="Dynamic"></asp:RegularExpressionValidator>
                    </td>
                </tr>
            </table>
        </div>
        <asp:HiddenField ID="RecordEdtDate" runat="server" Value='<%# Bind("RecordEdtDate") %>'
            OnDataBinding="RecordEdtDate_DataBinding" />
        <asp:HiddenField ID="RecordEdtUser" runat="server" Value='<%# Bind("RecordEdtUser") %>'
            OnDataBinding="RecordEdtUser_DataBinding" />
        <div align="center" style="margin: 0; padding: 0 0 8px 0; border: 0px; display: block;">
            <%--<asp:LinkButton ID="UpdateButton" CssClass="HIC_LinkButton1" runat="server" CommandName="Update"
                Text="Salva dati"></asp:LinkButton>--%>
            <asp:LinkButton ID="UpdateButton" CssClass="ZSSM_Button01_LinkButton" runat="server" CommandName="Update"
                Text="Salva dati"></asp:LinkButton>            
        </div>
        <asp:HiddenField ID="zdtRecordNewUser" runat="server" Value='<%# Eval("RecordNewUser") %>' />
        <asp:HiddenField ID="zdtRecordNewDate" runat="server" Value='<%# Eval("RecordNewDate") %>' />
        <asp:HiddenField ID="zdtRecordEdtUser" runat="server" Value='<%# Eval("RecordEdtUser") %>' />
        <asp:HiddenField ID="zdtRecordEdtDate" runat="server" Value='<%# Eval("RecordEdtDate") %>' />
    </EditItemTemplate>
</asp:FormView>
<%--<asp:SqlDataSource ID="ProfiloSocietarioSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
    SelectCommand="SELECT RagioneSociale, PartitaIVA, S1_Indirizzo, S1_Numero, S1_Citta, S1_Comune, 
    S1_Cap, S1_ProvinciaEstesa, S1_ProvinciaSigla, S1_ID2Nazione,S2_Indirizzo, S2_Numero, 
    S2_Citta, S2_Comune, S2_Cap, S2_ProvinciaEstesa, S2_ProvinciaSigla, S2_ID2Nazione, Telefono1, 
    Telefono2, Fax,WebSite,RecordNewUser, RecordNewDate, RecordEdtUser, RecordEdtDate ,ID2BusinessBook
    FROM tbProfiliSocietari" UpdateCommand=" SET DATEFORMAT dmy; UPDATE tbProfilisocietari 
    SET RagioneSociale=@RagioneSociale, PartitaIVA=@PartitaIVA, 
    S1_Indirizzo=@S1_Indirizzo, S1_Numero=@S1_Numero, S1_Citta=@S1_Citta, 
    S1_Comune=@S1_Comune, S1_Cap=@S1_Cap, S1_ProvinciaEstesa=@S1_ProvinciaEstesa, 
    S1_ProvinciaSigla=@S1_ProvinciaSigla, S1_ID2Nazione=@S1_ID2Nazione,S2_Indirizzo=@S2_Indirizzo, 
    S2_Numero=@S2_Numero, S2_Citta=@S2_Citta, S2_Comune=@S2_Comune, S2_Cap=@S2_Cap, 
    S2_ProvinciaEstesa=@S2_ProvinciaEstesa, S2_ProvinciaSigla=@S2_ProvinciaSigla, S2_ID2Nazione=@S2_ID2Nazione, 
    Telefono1=@Telefono1, Telefono2=@Telefono2, Fax=@Fax,WebSite=@WebSite, RecordEdtUser=@RecordEdtUser, 
    RecordEdtDate=@RecordEdtDate, ID2BusinessBook=@ID2BusinessBook WHERE [UserID] =@UserID"
    OnUpdated="ProfiloSocietarioSqlDataSource_Updated">--%>
<asp:SqlDataSource ID="ProfiloSocietarioSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
    SelectCommand="SELECT RagioneSociale, S1_Indirizzo, S1_Numero, S1_Citta, S1_Comune, 
    S1_Cap, S1_ProvinciaEstesa, S1_ProvinciaSigla, S1_ID2Nazione,S2_Indirizzo, S2_Numero, 
    S2_Citta, S2_Comune, S2_Cap, S2_ProvinciaEstesa, S2_ProvinciaSigla, S2_ID2Nazione, Telefono1, 
    Telefono2, Fax,WebSite,RecordNewUser, RecordNewDate, RecordEdtUser, RecordEdtDate ,ID2BusinessBook
    FROM tbProfiliSocietari" UpdateCommand=" SET DATEFORMAT dmy; UPDATE tbProfilisocietari 
    SET RagioneSociale=@RagioneSociale, PartitaIVA=@PartitaIVA, 
    S1_Indirizzo=@S1_Indirizzo, S1_Numero=@S1_Numero, S1_Citta=@S1_Citta, 
    S1_Comune=@S1_Comune, S1_Cap=@S1_Cap, S1_ProvinciaEstesa=@S1_ProvinciaEstesa, 
    S1_ProvinciaSigla=@S1_ProvinciaSigla, S1_ID2Nazione=@S1_ID2Nazione,
    RecordEdtUser=@RecordEdtUser, RecordEdtDate=@RecordEdtDate, ID2BusinessBook=@ID2BusinessBook WHERE [UserID] =@UserID"
    OnUpdated="ProfiloSocietarioSqlDataSource_Updated">
    <UpdateParameters>
        <asp:Parameter Name="RagioneSociale" />
        <%--<asp:Parameter Name="PartitaIVA" />--%>
        <asp:Parameter Name="S1_Indirizzo" />
        <asp:Parameter Name="S1_Numero" />
        <asp:Parameter Name="S1_Citta" />
        <asp:Parameter Name="S1_Comune" />
        <asp:Parameter Name="S1_Cap" />
        <asp:Parameter Name="S1_ProvinciaEstesa" />
        <asp:Parameter Name="S1_ProvinciaSigla" />
        <asp:Parameter Name="S1_ID2Nazione" />
        <%--<asp:Parameter Name="S2_Indirizzo" />
        <asp:Parameter Name="S2_Numero" />
        <asp:Parameter Name="S2_Citta" />
        <asp:Parameter Name="S2_Comune" />
        <asp:Parameter Name="S2_Cap" />
        <asp:Parameter Name="S2_ProvinciaEstesa" />
        <asp:Parameter Name="S2_ProvinciaSigla" />
        <asp:Parameter Name="S2_ID2Nazione" />--%>
        <%--<asp:Parameter Name="Telefono1" />
        <asp:Parameter Name="Telefono2" />
        <asp:Parameter Name="Fax" />
        <asp:Parameter Name="Website" />--%>
        <asp:Parameter Name="ID2BusinessBook" />
        <asp:Parameter Name="RecordEdtUser" />
        <asp:Parameter Name="RecordEdtDate" Type="DateTime" />
        <asp:ControlParameter ControlID="UIDHiddenField" Name="UserID" />
    </UpdateParameters>
</asp:SqlDataSource>
