﻿using ASP;
using System;
using System.Data.SqlClient;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per Default
/// </summary>
public partial class Account_Default : System.Web.UI.Page
{

    #region Fields

    private string Lang = "ITA";

    public string titolo = string.Empty;

    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();

        if (Membership.GetUser() == null)
            Response.Redirect("~/Login/Login.aspx?ReturnUrl=" + Server.UrlEncode("/Account/"));

        if (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang") && Request.QueryString["Lang"] != null)
            Response.Redirect("~/System/Message.aspx?0");

        Lang = Request.QueryString["Lang"] ?? Lang;
        if (Lang.Length == 0)
            Lang = "ITA";

        titolo = "Profilo utente";

        ReadXML_Localization("PROFILI", Lang);

        ProfiloPersonale_Dtl1.UID =
            ProfiloSocietario_Dtl1.UID =
            ProfiloMarketing_Dtl1.UID =
            ProfiloPersonaleCV_Dtl1.UID =
            ProfiloPersonaleImg_Dtl1.UID = Membership.GetUser().ProviderUserKey.ToString();

        ProfiloPersonale_Dtl1.Lang = Lang;
        ProfiloSocietario_Dtl1.Lang = Lang;
        Account_Dtl1.Lang = Lang;
        ProfiloMarketing_Dtl1.Lang = Lang;
        ProfiloPersonaleCV_Dtl1.Lang = Lang;
        ProfiloPersonaleImg_Dtl1.Lang = Lang;
    }

    #endregion

    #region Methods

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";
            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode + "/DEFAULT";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Profili.xml"));

            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                            if (myLabel != null)
                                myLabel.Text = childNode.InnerText;
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name + "Label");

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
           
            Page.Title = (mydoc.SelectSingleNode(myXPath + "/" + "TitoloPaginaDefault").InnerText);
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    #endregion

}