﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProfiloPersonaleCV_Edt.ascx.cs" Inherits="Account_ProfiloPersonaleCV_Edt" %>

<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<script type="text/javascript">
    function OnClientModeChange(editor) {
        var mode = editor.get_mode();
        var doc = editor.get_document();
        var head = doc.getElementsByTagName("HEAD")[0];
        var link;

        switch (mode) {
            case 1: //remove the external stylesheet when displaying the content in Design mode    
                //var external = doc.getElementById("external");
                //head.removeChild(external);
                break;
            case 2:
                break;
            case 4: //apply your external css stylesheet to Preview mode    
                link = doc.createElement("LINterK");
                link.setAttribute("href", "/SiteCss/Telerik.css");
                link.setAttribute("rel", "stylesheet");
                link.setAttribute("type", "text/css");
                link.setAttribute("id", "external");
                head.appendChild(link);
                break;
        }
    }

    function editorCommandExecuted(editor, args) {
        if (!$telerik.isChrome)
            return;
        var dialogName = args.get_commandName();
        var dialogWin = editor.get_dialogOpener()._dialogContainers[dialogName];
        if (dialogWin) {
            var cellEl = dialogWin.get_contentElement() || dialogWin.ui.contentCell || dialogWin.ui.content,
                frame = dialogWin.get_contentFrame();
            frame.onload = function () {
                cellEl.style.cssText = "";
                dialogWin.autoSize();
            }
        }
    }
</script>

<asp:HiddenField ID="UIDHiddenField" runat="server" />
<asp:FormView ID="ProfiloPersonaleCVFormView" DefaultMode="Edit" runat="server" Width="100%">
    <EditItemTemplate>
        <asp:SqlDataSource ID="dsNazioni2" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
            SelectCommand="SELECT [ID1Nazione], [Nazione] FROM [tbNazioni] ORDER BY [Nazione]"></asp:SqlDataSource>
        <div class="ZITC010">
            <%--<div class="TFY_Titolo3">
                <asp:Label ID="AziendaTitleLabel" runat="server" CssClass="TFY_Sottotitolo3" Text="Azienda / ente / società"></asp:Label>
            </div>--%>
            <table cellpadding="2" cellspacing="1" border="0" style="width: 100%;">
                <tr>
                    <td class="BlockBoxValue">
                        <%--<CustomWebControls:TextArea ID="TextAreaCurriculumVitae" runat="server" Text='<%# HttpUtility.HtmlEncode(Bind("CurriculumVitae").ToString()) %>' 
                            Width="100%" Rows="10" Height="600" TextMode="MultiLine"></CustomWebControls:TextArea>--%>

                        <telerik:RadEditor
                            Language="it-IT" ID="RadEditor1" runat="server"
                            DocumentManager-DeletePaths="~/ZeusInc/Account/Documents"
                            DocumentManager-SearchPatterns="*.*"
                            DocumentManager-ViewPaths="~/ZeusInc/Account/Documents"
                            DocumentManager-MaxUploadFileSize="52428800"
                            DocumentManager-UploadPaths="~/ZeusInc/Account/Documents"
                            FlashManager-DeletePaths="~/ZeusInc/Account/Media"
                            FlashManager-MaxUploadFileSize="10240000"
                            FlashManager-ViewPaths="~/ZeusInc/Account/Media"
                            FlashManager-UploadPaths="~/ZeusInc/Account/Media"
                            ImageManager-DeletePaths="~/ZeusInc/Account/Images"
                            ImageManager-ViewPaths="~/ZeusInc/Account/Images"
                            ImageManager-MaxUploadFileSize="10240000"
                            ImageManager-SearchPatterns="*.gif, *.png, *.jpg, *.jpe, *.jpeg"
                            ImageManager-UploadPaths="~/ZeusInc/Account/Images"
                            ImageManager-ViewMode="Grid"
                            MediaManager-DeletePaths="~/ZeusInc/Account/Media"
                            MediaManager-MaxUploadFileSize="10240000"
                            MediaManager-SearchPatterns="*.asf, *.asx, *.wma, *.wmv, *.avi, *.wav, *.mpeg, *.mpg, *.mpe, *.mov, *.mp3, *.m3u, *.mid, *.midi, *.rm, *.rma"
                            MediaManager-ViewPaths="~/ZeusInc/Account/Media"
                            MediaManager-UploadPaths="~/ZeusInc/Account/Media"
                            TemplateManager-SearchPatterns="*.html,*.htm"
                            ContentAreaMode="iframe"
                            OnClientCommandExecuted="editorCommandExecuted"
                            OnClientModeChange="OnClientModeChange"
                            Content='<%# Bind("CurriculumVitae") %>'
                            ToolsFile="~/Account/RadEditor1.xml"
                            LocalizationPath="~/App_GlobalResources"
                            AllowScripts="true" RenderMode="Classic" ToolbarMode="Default" EnableViewState="False"
                            Width="100%" Height="600px">
                            <CssFiles>
                                <telerik:EditorCssFile Value="~/asset/css/ZeusTypeFoundry.css" />
                            </CssFiles>
                        </telerik:RadEditor>
                    </td>
                </tr>
            </table>
        </div>
        <asp:HiddenField ID="RecordEdtDate" runat="server" Value='<%# Bind("RecordEdtDate") %>'
            OnDataBinding="RecordEdtDate_DataBinding" />
        <asp:HiddenField ID="RecordEdtUser" runat="server" Value='<%# Bind("RecordEdtUser") %>'
            OnDataBinding="RecordEdtUser_DataBinding" />
        <div align="center" style="margin: 0; padding: 0 0 8px 0; border: 0px; display: block;">

            <asp:LinkButton ID="UpdateButton" CssClass="ZSSM_Button01_LinkButton" runat="server" CommandName="Update"
                Text="Salva dati"></asp:LinkButton>
        </div>
        <asp:HiddenField ID="zdtRecordNewUser" runat="server" Value='<%# Eval("RecordNewUser") %>' />
        <asp:HiddenField ID="zdtRecordNewDate" runat="server" Value='<%# Eval("RecordNewDate") %>' />
        <asp:HiddenField ID="zdtRecordEdtUser" runat="server" Value='<%# Eval("RecordEdtUser") %>' />
        <asp:HiddenField ID="zdtRecordEdtDate" runat="server" Value='<%# Eval("RecordEdtDate") %>' />
    </EditItemTemplate>
</asp:FormView>
<asp:SqlDataSource ID="ProfiloPersonaleCVSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
    SelectCommand="SELECT CurriculumVitae,RecordNewUser,RecordNewDate,RecordEdtUser, RecordEdtDate
        FROM tbProfiliPersonali"
    UpdateCommand=" SET DATEFORMAT dmy; 
        UPDATE tbProfiliPersonali 
        SET CurriculumVitae=@CurriculumVitae, RecordEdtUser=@RecordEdtUser, RecordEdtDate=@RecordEdtDate
        WHERE [UserID] =@UserID"
    OnUpdated="ProfiloPersonaleCVSqlDataSource_Updated">
    <UpdateParameters>
        <asp:Parameter Name="CurriculumVitae" />
        <asp:Parameter Name="RecordEdtUser" />
        <asp:Parameter Name="RecordEdtDate" Type="DateTime" />
        <asp:ControlParameter ControlID="UIDHiddenField" Name="UserID" />
    </UpdateParameters>
</asp:SqlDataSource>
