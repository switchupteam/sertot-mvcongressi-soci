﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_ProfiloPersonaleCV_Edt : System.Web.UI.Page
{
    #region Fields

    private string Lang = "ITA";

    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();

        if (Membership.GetUser() == null)
            Response.Redirect("~/Zeus/System/Message.aspx?NoUser");

        ProfiloPersonaleCV_Edt1.UID = Membership.GetUser().ProviderUserKey.ToString();
        ProfiloPersonaleCV_Edt1.RedirectPath = "~/Account/";

        Page.Title = "Aggiornamento Affiliazione - Sertot";

        if (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang") && Request.QueryString["Lang"] != null)
            Response.Redirect("~/System/Message.aspx?0");

        Lang = Request.QueryString["Lang"] ?? Lang;
        if (Lang.Length == 0)
            Lang = "ITA";

        ProfiloPersonaleCV_Edt1.Lang = Lang;
        ReadXML_Localization("PROFILI", Lang);
    }

    #endregion

    #region Methods

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";
            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode + "/AFFILIAZIONE";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Profili.xml"));

            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                            if (myLabel != null)
                                myLabel.Text = childNode.InnerText;
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name + "Label");

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }

            Page.Title = (mydoc.SelectSingleNode(myXPath + "/" + "TitoloPaginaEdt").InnerText);
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    #endregion

}