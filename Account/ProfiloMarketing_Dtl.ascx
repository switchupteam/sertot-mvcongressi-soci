﻿<%@ Control Language="C#"
    CodeFile="ProfiloMarketing_Dtl.ascx.cs"
    Inherits="Account_ProfiloMarketing_Dtl" %>

<asp:FormView ID="ProfiloMarketingFormView" runat="server" Width="100%"
    OnDataBound="ProfiloMarketingFormView_DataBound">
    <ItemTemplate>
        <div class="BXT_001">
            <div class="TFY_Titolo3">
                <asp:Label ID="ProfiloMarketingTitleLabel" runat="server" CssClass="TFY_Sottotitolo3" Text="Preferenze"></asp:Label>
            </div>
        </div>
        <table>
            <asp:Panel ID="ZMCF_Categoria1" runat="server">
                <tr>
                    <td>
                        <asp:Label ID="CategoriaDescrizioneLabel" CssClass="HIC_LabelCorpoTesto1" runat="server" Text="Categoria:"></asp:Label>
                        <asp:Label ID="CategoriaLabel" runat="server" CssClass="HIC_LabelCorpoTesto1"></asp:Label>
                        <asp:DropDownList ID="ID2CategoriaDropDownList" runat="server" Visible="false" AppendDataBoundItems="True">
                            <asp:ListItem Text="> Seleziona" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:HiddenField ID="ID2CategoriaHiddenField" runat="server" Value='<%# Eval("ID2Categoria1") %>' />
                        <asp:SqlDataSource ID="dsCategoria" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"></asp:SqlDataSource>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="ZMCF_ConsensoDatiPersonali" runat="server">
                <tr>
                    <td class="BlockBoxValue" colspan="2">
                        <asp:CheckBox ID="CheckBoxDatiPersonali" runat="server" Checked='<%# Bind("ConsensoDatiPersonali") %>'
                            Enabled="false" />
                        <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="ConsensoDati1DescrizioneLabel" runat="server" Text="Consenso trattamento dati personali"></asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="ZMCF_ConsensoDatiPersonali2" runat="server">
                <tr>
                    <td class="BlockBoxValue" colspan="2">
                        <asp:CheckBox ID="CheckBoxDatiPersonali2" runat="server" Checked='<%# Bind("ConsensoDatiPersonali2") %>'
                            Enabled="false" />
                        <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="ConsensoDati2DescrizioneLabel" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="ZMCF_Comunicazioni1" runat="server">
                <tr>
                    <td class="BlockBoxValue" colspan="2">
                        <asp:CheckBox ID="Comunicazioni1CheckBox" runat="server" Checked='<%# Bind("Comunicazioni1") %>'
                            Enabled="false" />
                        <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="ConsensoComunicazioni1DescrizioneLabel" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="ZMCF_Comunicazioni2" runat="server">
                <tr>
                    <td class="BlockBoxValue" colspan="2">
                        <asp:CheckBox ID="Comunicazioni2CheckBox" runat="server" Checked='<%# Bind("Comunicazioni2") %>'
                            Enabled="false" />
                        <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="ConsensoComunicazioni2DescrizioneLabel" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="ZMCF_Comunicazioni3" runat="server">
                <tr>
                    <td class="BlockBoxValue" colspan="2">
                        <asp:CheckBox ID="Comunicazioni3CheckBox" runat="server" Checked='<%# Bind("Comunicazioni3") %>'
                            Enabled="false" />
                        <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="ConsensoComunicazioni3DescrizioneLabel" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="ZMCF_Comunicazioni4" runat="server">
                <tr>
                    <td class="BlockBoxValue" colspan="2">
                        <asp:CheckBox ID="Comunicazioni4CheckBox" runat="server" Checked='<%# Bind("Comunicazioni4") %>'
                            Enabled="false" />
                        <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="ConsensoComunicazioni4DescrizioneLabel" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="ZMCF_Comunicazioni5" runat="server">
                <tr>
                    <td class="BlockBoxValue" colspan="2">
                        <asp:CheckBox ID="Comunicazioni5CheckBox" runat="server" Checked='<%# Bind("Comunicazioni5") %>'
                            Enabled="false" />
                        <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="ConsensoComunicazioni5DescrizioneLabel" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="ZMCF_Comunicazioni6" runat="server">
                <tr>
                    <td class="BlockBoxValue" colspan="2">
                        <asp:CheckBox ID="Comunicazioni6CheckBox" runat="server" Checked='<%# Bind("Comunicazioni6") %>'
                            Enabled="false" />
                        <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="ConsensoComunicazioni6DescrizioneLabel" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="ZMCF_Comunicazioni7" runat="server">
                <tr>
                    <td class="BlockBoxValue" colspan="2">
                        <asp:CheckBox ID="Comunicazioni7CheckBox" runat="server" Checked='<%# Bind("Comunicazioni7") %>'
                            Enabled="false" />
                        <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="ConsensoComunicazioni7DescrizioneLabel" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="ZMCF_Comunicazioni8" runat="server">
                <tr>
                    <td class="BlockBoxValue" colspan="2">
                        <asp:CheckBox ID="Comunicazioni8CheckBox" runat="server" Checked='<%# Bind("Comunicazioni8") %>'
                            Enabled="false" />
                        <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="ConsensoComunicazioni8DescrizioneLabel" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="ZMCF_Comunicazioni9" runat="server">
                <tr>
                    <td class="BlockBoxValue" colspan="2">
                        <asp:CheckBox ID="Comunicazioni9CheckBox" runat="server" Checked='<%# Bind("Comunicazioni9") %>'
                            Enabled="false" />
                        <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="ConsensoComunicazioni9DescrizioneLabel" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="ZMCF_Comunicazioni10" runat="server">
                <tr>
                    <td class="BlockBoxValue" colspan="2">
                        <asp:CheckBox ID="Comunicazioni10CheckBox" runat="server" Checked='<%# Bind("Comunicazioni10") %>'
                            Enabled="false" />
                        <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="ConsensoComunicazioni10DescrizioneLabel" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                    </td>
                </tr>
            </asp:Panel>

        </table>
        <div style="text-align: center">
            <%--<asp:LinkButton ID="ModificaLinkButton" runat="server" CssClass="HIC_LinkButton1" Text="Modifica"
                PostBackUrl='<%# Eval("UserId","ProfiloMarketing_Edt.aspx" ) %>'></asp:LinkButton>--%>
            <asp:LinkButton ID="ModificaLinkButton" runat="server" CssClass="ZSSM_Button01_LinkButton" Text="Modifica"
                PostBackUrl='<%# Eval("UserId","ProfiloMarketing_Edt.aspx" ) %>'></asp:LinkButton>
        </div>

    </ItemTemplate>
</asp:FormView>
<asp:SqlDataSource ID="ProfiloMarketingSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
    SelectCommand="SELECT  UserId,ConsensoDatiPersonali,ConsensoDatiPersonali2, 
     Comunicazioni1,Comunicazioni2,Comunicazioni3,Comunicazioni4,Comunicazioni5
     ,Comunicazioni6,Comunicazioni7,Comunicazioni8,Comunicazioni9,Comunicazioni10,ID2Categoria1,RegCausal
    FROM tbProfiliMarketing"></asp:SqlDataSource>
