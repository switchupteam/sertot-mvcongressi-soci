﻿<%@ Control Language="C#" CodeFile="ProfiloPersonaleCV_Dtl.ascx.cs" Inherits="Account_ProfiloPersonaleCV_Dtl" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<asp:FormView ID="ProfiloPersonaleCVFormView" runat="server" Width="100%">
    <ItemTemplate>
        <div class="BXT_001">
            <div class="TFY_Titolo3">
                <asp:Label ID="ProfiloPersonaleCVLabel" runat="server" CssClass="TFY_Sottotitolo3" Text="Curriculum Vitae"></asp:Label>
            </div>
        </div>
        <table border="0" cellpadding="2" cellspacing="1">
            <tr>
                <td>
                    <%--<asp:Label ID="RagioneSocialeDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Curriculum:</asp:Label>--%>
                    <asp:Label ID="RagioneSocialeLabel" runat="server" CssClass="HIC_LabelCorpoTesto1" Text='<%# Eval("CurriculumVitae").ToString().TruncateString()  %>'>
                    </asp:Label>
                </td>
            </tr>            
            <tr>
                <td align="center" colspan="2">
                    <asp:Label ID="EnabledLabel" runat="server" Visible="false" Text="Attenzione: dati non presenti."></asp:Label>
                </td>
            </tr>
        </table>
        <div style="text-align: center">            
            <asp:LinkButton ID="ModificaLinkButton" runat="server" CssClass="ZSSM_Button01_LinkButton" Text="Aggiorna"
                PostBackUrl='<%# Eval("UserId","ProfiloPersonaleCV_Edt.aspx" ) %>'></asp:LinkButton>
        </div>
    </ItemTemplate>
</asp:FormView>
<asp:SqlDataSource ID="ProfiloPersonaleCVSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
    SelectCommand="SELECT [UserId], [CurriculumVitae] FROM [vwProfiliPersonali]"></asp:SqlDataSource>
