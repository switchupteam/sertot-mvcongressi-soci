﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per ProfiloSocietario_Dtl
/// </summary>
public partial class Account_ProfiloSocietario_Dtl : System.Web.UI.UserControl
{

    #region Fields

    private string UserID = string.Empty;
    private string lang = string.Empty;

    #endregion

    #region Properties

    public string UID
    {
        set { UserID = value; }
        get { return UserID; }
    }

    public string Lang
    {
        set { lang = value; }
        get { return lang; }
    }

    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
            BindTheData();     
    }

    protected void Clear(object sender, EventArgs e)
    {
        Label Nazione = (Label)sender;
        if (Nazione.Text.Contains(">"))
            Nazione.Text = "";
    }

    protected void ProfiloSocietarioFormView_DataBound(object sender, EventArgs e)
    {
        ReadXML_Localization("PROFILI", lang);
    }

    #endregion

    #region Methods

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";
            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode + "/AFFILIAZIONE";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Profili.xml"));

            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);
            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);                            

                            if (myLabel != null)
                            {
                                myLabel.Text = childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name + "Label");

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                            }
                        }
                        catch (Exception p)
                        {
                        }
                    }
                }
            }

            LinkButton ModificaLinkButton = (LinkButton)ProfiloSocietarioFormView.FindControl("ModificaLinkButton");
            ModificaLinkButton.Text = (mydoc.SelectSingleNode(myXPath + "/" + "ModificaLinkButton").InnerText);
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return;
        }
    }
    
    public bool BindTheData()
    {
        try
        {
            ProfiloSocietarioSqlDataSource.SelectCommand += " WHERE ([UserID] = '" + UserID + "')";
            ProfiloSocietarioFormView.DataSourceID = "ProfiloSocietarioSqlDataSource";
            ProfiloSocietarioSqlDataSource.DataBind();
            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    #endregion    

}