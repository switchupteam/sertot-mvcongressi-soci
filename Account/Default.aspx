﻿<%@ Page Language="C#" MasterPageFile="~/Master/LayoutAreaSoci.master" Title="Sertot" CodeFile="Default.aspx.cs" Inherits="Account_Default" %>

<%@ Register Src="~/UserControl/MenuSoci.ascx" TagName="Menu_Soci" TagPrefix="uc1" %>

<%@ Register Src="Account_Dtl.ascx" TagName="Account_Dtl" TagPrefix="uc1" %>
<%@ Register Src="ProfiloPersonale_Dtl.ascx" TagName="ProfiloPersonale_Dtl" TagPrefix="uc2" %>
<%@ Register Src="ProfiloSocietario_Dtl.ascx" TagName="ProfiloSocietario_Dtl" TagPrefix="uc3" %>
<%@ Register Src="ProfiloMarketing_Dtl.ascx" TagName="ProfiloMarketing_Dtl" TagPrefix="uc4" %>
<%@ Register Src="ProfiloPersonaleCV_Dtl.ascx" TagName="ProfiloPersonaleCV_Dtl" TagPrefix="uc5" %>
<%@ Register Src="ProfiloPersonaleImg_Dtl.ascx" TagName="ProfiloPersonaleImg_Dtl" TagPrefix="uc6" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AreaTitle" runat="Server">
    <%= titolo %>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="AreaContentMain" runat="Server">
    <section class="accountDefault">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="boxSezione">
                                <uc2:ProfiloPersonale_Dtl ID="ProfiloPersonale_Dtl1" runat="server" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="boxSezione">
                                <uc3:ProfiloSocietario_Dtl ID="ProfiloSocietario_Dtl1" runat="server" />
                            </div>
                        </div>                        
                        <div class="col-md-6">
                            <div class="boxSezione">
                                <uc1:Account_Dtl ID="Account_Dtl1" runat="server" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="boxSezione">
                                <uc4:ProfiloMarketing_Dtl ID="ProfiloMarketing_Dtl1" runat="server" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="boxSezione">
                                <uc5:ProfiloPersonaleCV_Dtl ID="ProfiloPersonaleCV_Dtl1" runat="server" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="boxSezione">
                                <uc6:ProfiloPersonaleImg_Dtl ID="ProfiloPersonaleImg_Dtl1" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="LAY01A_Content2">
                        <uc1:Menu_Soci ID="Menu_Soci1" runat="server" />
                    </div>
                    <div class="FloatClr"></div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
