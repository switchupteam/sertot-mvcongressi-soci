﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per ProfiloMarketing_Dtl
/// </summary>
public partial class Account_ProfiloMarketing_Dtl : System.Web.UI.UserControl
{

    #region Fields

    private string userID = string.Empty;
    private string lang = string.Empty;

    #endregion

    #region Properties

    public string UID
    {
        set { userID = value; }
        get { return userID; }
    }

    public string Lang
    {
        set { lang = value; }
        get { return lang; }
    }

    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindTheData();
        }
    }

    protected void ProfiloMarketingFormView_DataBound(object sender, EventArgs e)
    {
        ReadXML("PROFILI", lang);
        ReadXML_Localization("PROFILI", lang);
    }

    #endregion

    #region Methods

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";
            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode + "/PROFILO_MARKETING";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Profili.xml"));

            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);
            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                            if (myLabel != null)
                            {
                                myLabel.Text = childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name + "Label");

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                            }
                        }
                        catch (Exception p)
                        {
                        }
                    }
                }
            }

            LinkButton ModificaLinkButton = (LinkButton)ProfiloMarketingFormView.FindControl("ModificaLinkButton");
            ModificaLinkButton.Text = (mydoc.SelectSingleNode(myXPath + "/" + "ModificaLinkButton").InnerText);
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return;
        }
    }

    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_Profili.xml"));

            Panel ZMCF_Categoria1 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Categoria1");
            
            if (ZMCF_Categoria1 != null)
                ZMCF_Categoria1.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Categoria1").InnerText);

            Panel ZMCF_ConsensoDatiPersonali = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_ConsensoDatiPersonali");
            
            if (ZMCF_ConsensoDatiPersonali != null)
                ZMCF_ConsensoDatiPersonali.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_ConsensoDatiPersonali").InnerText);

            Panel ZMCF_ConsensoDatiPersonali2 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_ConsensoDatiPersonali2");

            if (ZMCF_ConsensoDatiPersonali2 != null)
                ZMCF_ConsensoDatiPersonali2.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_ConsensoDatiPersonali2").InnerText);

            Panel ZMCF_Comunicazioni1 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Comunicazioni1");

            if (ZMCF_Comunicazioni1 != null)
                ZMCF_Comunicazioni1.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni1").InnerText);

            Panel ZMCF_Comunicazioni2 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Comunicazioni2");

            if (ZMCF_Comunicazioni2 != null)
                ZMCF_Comunicazioni2.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni2").InnerText);

            Panel ZMCF_Comunicazioni3 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Comunicazioni3");
            
            if (ZMCF_Comunicazioni3 != null)
                ZMCF_Comunicazioni3.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni3").InnerText);

            Panel ZMCF_Comunicazioni4 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Comunicazioni4");

            if (ZMCF_Comunicazioni4 != null)
                ZMCF_Comunicazioni4.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni4").InnerText);

            Panel ZMCF_Comunicazioni5 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Comunicazioni5");

            if (ZMCF_Comunicazioni5 != null)
                ZMCF_Comunicazioni5.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni5").InnerText);

            Panel ZMCF_Comunicazioni6 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Comunicazioni6");
            
            if (ZMCF_Comunicazioni6 != null)
                ZMCF_Comunicazioni6.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni6").InnerText);

            Panel ZMCF_Comunicazioni7 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Comunicazioni7");

            if (ZMCF_Comunicazioni7 != null)
                ZMCF_Comunicazioni7.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni7").InnerText);

            Panel ZMCF_Comunicazioni8 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Comunicazioni8");
            
            if (ZMCF_Comunicazioni8 != null)
                ZMCF_Comunicazioni8.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni8").InnerText);

            Panel ZMCF_Comunicazioni9 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Comunicazioni9");
            
            if (ZMCF_Comunicazioni9 != null)
                ZMCF_Comunicazioni9.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni9").InnerText);

            Panel ZMCF_Comunicazioni10 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Comunicazioni10");
            
            if (ZMCF_Comunicazioni10 != null)
                ZMCF_Comunicazioni10.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni10").InnerText);

            SetQueryOfID2CategoriaDropDownList(mydoc.SelectSingleNode(myXPath + "ZMCD_ZIMCategoria1").InnerText
                          , Lang, mydoc.SelectSingleNode(myXPath + "ZMCD_Cat1Liv").InnerText);

            return true;
        }
        catch (Exception)
        {
            return false;
        }

    }

    private bool SetQueryOfID2CategoriaDropDownList(string ZeusIdModulo, string ZeusLangCode, string PZV_Cat1Liv)
    {
        try
        {
            if (ZeusIdModulo.Length == 0)
                return false;

            if (ZeusLangCode.Length == 0)
                return false;

            if (PZV_Cat1Liv.Length == 0)
                return false;

            SqlDataSource dsCategoria = (SqlDataSource)ProfiloMarketingFormView.FindControl("dsCategoria");
            DropDownList ID2CategoriaDropDownList = (DropDownList)ProfiloMarketingFormView.FindControl("ID2CategoriaDropDownList");
            HiddenField ID2CategoriaHiddenField = (HiddenField)ProfiloMarketingFormView.FindControl("ID2CategoriaHiddenField");
            Label CategoriaLabel = (Label)ProfiloMarketingFormView.FindControl("CategoriaLabel");

            if ((ID2CategoriaHiddenField != null) &&
                (ID2CategoriaHiddenField.Value.Equals("0")))
            {
                CategoriaLabel.Text = "Non inserita";
                return true;
            }

            switch (PZV_Cat1Liv)
            {
                case "123":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv1Liv2Liv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "Catliv1Liv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;

                case "23":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv2Liv3]";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;

                default:
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;
            }

            ID2CategoriaDropDownList.SelectedIndex = ID2CategoriaDropDownList.Items.IndexOf(ID2CategoriaDropDownList.Items.FindByValue(ID2CategoriaHiddenField.Value));

            CategoriaLabel.Text = ID2CategoriaDropDownList.SelectedItem.Text;
            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    private string GetCausalHumanReadble(string RegCasual)
    {
        if (RegCasual.Length == 0)
            return string.Empty;

        string CausalHumanReadble = string.Empty;

        try
        {
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("~/App_Data/RegistrationCausal.xml"));
            CausalHumanReadble = mydoc.SelectSingleNode("RootCasual/RegDescription[@RegCausal='" + RegCasual + "']").InnerText;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

        return CausalHumanReadble;
    }
        
    public bool BindTheData()
    {
        try
        {
            ProfiloMarketingSqlDataSource.SelectCommand += " WHERE ([userID] = '" + userID + "')";
            ProfiloMarketingFormView.DataSourceID = "ProfiloMarketingSqlDataSource";
            ProfiloMarketingSqlDataSource.DataBind();

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    #endregion

}