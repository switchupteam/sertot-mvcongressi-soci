﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per ProfiloPersonale_Dtl
/// </summary>
public partial class Account_ProfiloPersonale_Dtl : System.Web.UI.UserControl
{

    #region Fields

    private string userID = string.Empty;
    private string lang = string.Empty;

    #endregion

    #region Properties

    public string UID
    {
        set { userID = value; }
        get { return userID; }
    }

    public string Lang
    {
        set { lang = value; }
        get { return lang; }
    }

    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindTheData();
        }
    }

    protected void SessoLabel_DataBinding(object sender, EventArgs e)
    {
        Label SessoLabel = (Label)sender;

        switch (SessoLabel.Text)
        {
            case "M":
                if(lang == "ENG")
                    SessoLabel.Text = "Male";
                else
                    SessoLabel.Text = "Maschio";
                break;

            case "F":
                if(lang == "ENG")
                    SessoLabel.Text = "Female";
                else
                    SessoLabel.Text = "Femmina";
                break;

            default:
                SessoLabel.Text = "";
                break;
        }
    }

    protected void ProfiloPersonaleFormView_DataBound(object sender, EventArgs e)
    {
        ReadXML_Localization("PROFILI", lang);
    }

    #endregion

    #region Methods

    public bool BindTheData()
    {
        try
        {
            ProfiloPersonaleSqlDataSource.SelectCommand += " WHERE (vwProfiliPersonali.[UserID] = '" + userID + "')";
            ProfiloPersonaleFormView.DataSourceID = "ProfiloPersonaleSqlDataSource";
            ProfiloPersonaleSqlDataSource.DataBind();
            LinkButton ModificaLinkButton = (LinkButton)ProfiloPersonaleFormView.FindControl("ModificaLinkButton");
            //((LinkButton)ModificaLinkButton).PostBackUrl += "?Lang=" + lang;

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }
    
    protected void Clear(object sender, EventArgs e)
    {
        Label Nazione = (Label)sender;
        if (Nazione.Text.Contains(">"))
            Nazione.Text = "";
    }

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";
            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode + "/PROFILO_PERSONALE";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Profili.xml"));

            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);
            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                            if (myLabel != null)
                            {
                                myLabel.Text = childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name + "Label");

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                            }
                        }
                        catch (Exception p)
                        {
                        }
                    }
                }
            }

            LinkButton ModificaLinkButton = (LinkButton)ProfiloPersonaleFormView.FindControl("ModificaLinkButton");
            ModificaLinkButton.Text = (mydoc.SelectSingleNode(myXPath + "/" +"ModificaLinkButton").InnerText);
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return;
        }
    }

    #endregion

}