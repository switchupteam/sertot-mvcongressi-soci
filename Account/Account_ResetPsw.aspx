﻿<%@ Page Language="C#" MasterPageFile="~/Master/LayoutAreaSoci.master" Title="Sertot"
    CodeFile="Account_ResetPsw.aspx.cs"
    Inherits="Account_ResetPsw" %>

<%@ Register Src="~/UserControl/MenuSoci.ascx" TagName="Menu_Soci" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AreaTitle" runat="Server">
    Modifica password utente
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="AreaContentMain" runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="LAY01A_Content1">
                    <asp:HiddenField ID="UID" runat="server" />
                    <asp:FormView ID="MemberShipFormView" runat="server"
                        DefaultMode="ReadOnly"
                        DataSourceID="MemberShipSqlDataSource"
                        OnDataBound="MemberShipFormView_DataBound"
                        Width="100%" CellPadding="0">
                        <ItemTemplate>
                            <div class="ZITC010">
                                <div class="TFY_Titolo3">
                                    <asp:Label ID="AccountUtenteTitleLabel" runat="server" CssClass="TFY_Sottotitolo3" Text="Account utente"></asp:Label>
                                </div>
                                <table border="0" cellpadding="2" cellspacing="1">
                                    <tr>
                                        <td>
                                            <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="UserNameDescriptionLabel" runat="server" Text="Label">UserName:</asp:Label>
                                            <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="UserNameLabel" runat="server" Text='<%# Eval("UserName", "{0}") %>'></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="EMailDescriptionLabel" runat="server" Text="Label">E-mail: </asp:Label>
                                            <asp:HyperLink CssClass="HIC_LabelCorpoTesto1" ID="EmailHyperLink" runat="server"
                                                NavigateUrl="mailto:" Text='<%# Eval("Email", "{0}") %>'></asp:HyperLink>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ItemTemplate>
                    </asp:FormView>
                    <asp:SqlDataSource ID="MemberShipSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                        SelectCommand="SELECT aspnet_Membership.Password, aspnet_Membership.Email
        , aspnet_Membership.IsApproved, aspnet_Membership.CreateDate, aspnet_Membership.LastLoginDate
        , aspnet_Membership.LastPasswordChangedDate
        , aspnet_Membership.LastLockoutDate, aspnet_Membership.FailedPasswordAttemptCount, aspnet_Users.UserName
        , aspnet_Users.UserId AS Expr1, aspnet_Membership.IsLockedOut FROM aspnet_Membership INNER JOIN aspnet_Users ON aspnet_Membership.UserId = aspnet_Users.UserId WHERE (aspnet_Users.UserId = @UserID)">
                        <SelectParameters>
                            <asp:ControlParameter Name="UserID" ControlID="UID" DefaultValue="0" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:Label ID="Msg" runat="server" Visible="false" SkinID="Validazione01"></asp:Label><br />
                    <asp:Panel ID="PasswordPanel" runat="server">
                        <div class="ZITC010">
                            <div class="TFY_Titolo3">
                                <asp:Label ID="InsirisciPasswordTitleLabel" runat="server" CssClass="TFY_Sottotitolo3" Text="Inserisci nuova password"></asp:Label>
                            </div>
                            <table border="0" cellpadding="2" cellspacing="1">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="NuovaPasswordDescription" CssClass="HIC_LabelCorpoTesto1" runat="server" Text="Nuova password"></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:TextBox ID="TextBoxNewPass" runat="server" Columns="30" TextMode="Password"
                                            CssClass="HIC_TextBox1"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Obbligatorio"
                                            ControlToValidate="TextBoxNewPass" CssClass="HIC_Validator1" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:CustomValidator ID="CambiaPasswordCustomValidator" runat="server" CssClass="HIC_Validator1"
                                            Display="Dynamic" OnServerValidate="CambiaPasswordCustomValidator_ServerValidate"></asp:CustomValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ConfermaNuovaPasswordDescription" CssClass="HIC_LabelCorpoTesto1" runat="server" Text="Conferma nuova password"></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:TextBox ID="TextBoxConfNewPass" runat="server" Columns="30" TextMode="Password"
                                            CssClass="HIC_TextBox1"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Obbligatorio"
                                            ControlToValidate="TextBoxConfNewPass" class="HIC_Validator1" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div align="center">
                            <%--<asp:LinkButton CssClass="HIC_LinkButton1" ID="CambiaPasswordLinkButton" runat="server"
                                OnClick="CambiaPasswordLinkButton_Click">Cambia password</asp:LinkButton>--%>
                            <asp:LinkButton CssClass="ZSSM_Button01_LinkButton" ID="CambiaPasswordLinkButton" runat="server"
                                OnClick="CambiaPasswordLinkButton_Click">Cambia password</asp:LinkButton>
                        </div>
                        <br />
                    </asp:Panel>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="LAY01A_Content2">
                    <uc1:Menu_Soci ID="Menu_Soci1" runat="server" />
                </div>
                <div class="FloatClr"></div>
            </div>
        </div>
    </div>
</asp:Content>
