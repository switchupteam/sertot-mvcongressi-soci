﻿<%@ Control Language="C#" 
    CodeFile="ProfiloMarketing_Edt.ascx.cs"
    Inherits="Account_ProfiloMarketing_Edt_UC"%>

<asp:HiddenField ID="UIDHiddenField" runat="server" />
<asp:FormView ID="ProfiloMarketingFormView" 
    DefaultMode="Edit" 
    runat="server" 
    Width="100%"
    OnDataBound="ProfiloMarketingFormView_DataBound"
    >
    <EditItemTemplate>
        <div class="ZITC010">
            <div class="TFY_Titolo3">
                <asp:Label ID="PreferenzeTitle" runat="server" CssClass="TFY_Sottotitolo3" Text="Preferenze"></asp:Label></div>
            <table>
            
             <asp:Panel ID="ZMCF_Categoria1" runat="server">
                        <tr>
                            <td class="BlockBoxValue" colspan="2">
                                <asp:Label ID="CategoriaDescrizioneLabel" CssClass="HIC_LabelCorpoTesto1" runat="server" Text="Categoria"></asp:Label>
                                <asp:DropDownList ID="ID2CategoriaDropDownList" runat="server" OnSelectedIndexChanged="ID2CategoriaDropDownList_SelectIndexChange" CssClass="HIC_DropDown1"
                                    AppendDataBoundItems="True">
                                      <asp:ListItem Text="" Value="0" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:HiddenField ID="ID2CategoriaHiddenField" runat="server" Value='<%# Bind("ID2Categoria1") %>' />
                                <asp:SqlDataSource ID="dsCategoria" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>">
                                </asp:SqlDataSource>
                            </td>
                        </tr>
                    </asp:Panel>
                        <asp:Panel ID="ZMCF_ConsensoDatiPersonali" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="CheckBoxDatiPersonali" runat="server" Checked='<%# Bind("ConsensoDatiPersonali") %>' />
                                    <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="ConsensoDati1DescrizioneLabel" runat="server" Text="Consenso trattamento dati personali"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_ConsensoDatiPersonali2" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="CheckBoxDatiPersonali2" runat="server" Checked='<%# Bind("ConsensoDatiPersonali2") %>' />
                                    <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="ConsensoDati2DescrizioneLabel" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Comunicazioni1" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="Comunicazioni1CheckBox" runat="server" Checked='<%# Bind("Comunicazioni1") %>' />
                                    <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="ConsensoComunicazioni1DescrizioneLabel" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Comunicazioni2" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="Comunicazioni2CheckBox" runat="server" Checked='<%# Bind("Comunicazioni2") %>' />
                                    <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="ConsensoComunicazioni2DescrizioneLabel" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Comunicazioni3" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="Comunicazioni3CheckBox" runat="server"  Checked='<%# Bind("Comunicazioni3") %>'/>
                                    <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="ConsensoComunicazioni3DescrizioneLabel" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Comunicazioni4" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="Comunicazioni4CheckBox" runat="server"  Checked='<%# Bind("Comunicazioni4") %>'/>
                                    <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="ConsensoComunicazioni4DescrizioneLabel" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Comunicazioni5" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="Comunicazioni5CheckBox" runat="server"  Checked='<%# Bind("Comunicazioni5") %>' />
                                    <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="ConsensoComunicazioni5DescrizioneLabel" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Comunicazioni6" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="Comunicazioni6CheckBox" runat="server"  Checked='<%# Bind("Comunicazioni6") %>' />
                                    <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="ConsensoComunicazioni6DescrizioneLabel" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Comunicazioni7" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="Comunicazioni7CheckBox" runat="server"  Checked='<%# Bind("Comunicazioni7") %>' />
                                    <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="ConsensoComunicazioni7DescrizioneLabel" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Comunicazioni8" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="Comunicazioni8CheckBox" runat="server"  Checked='<%# Bind("Comunicazioni8") %>' />
                                    <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="ConsensoComunicazioni8DescrizioneLabel" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Comunicazioni9" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="Comunicazioni9CheckBox" runat="server"  Checked='<%# Bind("Comunicazioni9") %>' />
                                    <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="ConsensoComunicazioni9DescrizioneLabel" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Comunicazioni10" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="Comunicazioni10CheckBox" runat="server"  Checked='<%# Bind("Comunicazioni10") %>' />
                                    <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="ConsensoComunicazioni10DescrizioneLabel" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                          <%--<tr>
                                <td class="BlockBoxValue" colspan="2">
                                <asp:Label ID="ProfessioneDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Causale registrazione:</asp:Label>
                                    <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="CausaleLabel" runat="server" Text='<%# GetCausalHumanReadble(Eval("RegCausal").ToString()) %>'></asp:Label>
                                </td>
                            </tr>--%>
                    </table>
            <asp:HiddenField ID="RecordEdtDate" runat="server" Value='<%# Bind("RecordEdtDate") %>'
                OnDataBinding="RecordEdtDate_DataBinding" />
            <asp:HiddenField ID="RecordEdtUser" runat="server" Value='<%# Bind("RecordEdtUser") %>'
                OnDataBinding="RecordEdtUser_DataBinding" />
        </div>
        <div align="center" style="margin:0;padding:0 0 8px 0;border:0px;display:block;">
            <%--<asp:LinkButton ID="UpdateButton" runat="server" CssClass="HIC_LinkButton1" CommandName="Update"
                Text="Salva dati"></asp:LinkButton>--%>
            <asp:LinkButton ID="UpdateButton" runat="server" CssClass="ZSSM_Button01_LinkButton" CommandName="Update"
                Text="Salva dati"></asp:LinkButton>            
        </div>
        <asp:HiddenField ID="zdtRecordNewUser" runat="server" Value='<%# Eval("RecordNewUser") %>' />
        <asp:HiddenField ID="zdtRecordNewDate" runat="server" Value='<%# Eval("RecordNewDate") %>' />
        <asp:HiddenField ID="zdtRecordEdtUser" runat="server" Value='<%# Eval("RecordEdtUser") %>' />
        <asp:HiddenField ID="zdtRecordEdtDate" runat="server" Value='<%# Eval("RecordEdtDate") %>' />
    </EditItemTemplate>
</asp:FormView>
<asp:SqlDataSource ID="ProfiloMarketingSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
    SelectCommand="SELECT  ConsensoDatiPersonali,ConsensoDatiPersonali2
    ,Comunicazioni1,Comunicazioni2,Comunicazioni3,Comunicazioni4,Comunicazioni5
    ,Comunicazioni6,Comunicazioni7,Comunicazioni8,Comunicazioni9,Comunicazioni10
    ,RecordNewDate,RecordNewUser,RecordEdtDate,RecordEdtUser ,[ID2Categoria1],RegCausal 
    FROM tbProfiliMarketing"
    UpdateCommand=" SET DATEFORMAT dmy; UPDATE [tbProfiliMarketing] SET  
     [ConsensoDatiPersonali]=@ConsensoDatiPersonali,
    [ConsensoDatiPersonali2]=@ConsensoDatiPersonali2, 
    Comunicazioni1=@Comunicazioni1,
    Comunicazioni2=@Comunicazioni2,
    Comunicazioni3=@Comunicazioni3,
    Comunicazioni4=@Comunicazioni4,
    Comunicazioni5=@Comunicazioni5,
    Comunicazioni6=@Comunicazioni6,
    Comunicazioni7=@Comunicazioni7,
    Comunicazioni8=@Comunicazioni8,
    Comunicazioni9=@Comunicazioni9,
    Comunicazioni10=@Comunicazioni10,
    [RecordEdtUser] = @RecordEdtUser, 
    [RecordEdtDate] = @RecordEdtDate ,
    [ID2Categoria1]=@ID2Categoria1
    WHERE [UserID] =@UserID"
    OnUpdated="ProfiloMarketingSqlDataSource_Updated">
    <UpdateParameters>
    <asp:Parameter Name="ID2Categoria1" Type="Int32" />
        <asp:Parameter Name="ConsensoDatiPersonali" Type="Boolean" />
        <asp:Parameter Name="ConsensoDatiPersonali2" Type="Boolean" />
        <asp:Parameter Name="Comunicazioni1" Type="Boolean" />
        <asp:Parameter Name="Comunicazioni2" Type="Boolean" />
        <asp:Parameter Name="Comunicazioni3" Type="Boolean" />
        <asp:Parameter Name="Comunicazioni4" Type="Boolean" />
        <asp:Parameter Name="Comunicazioni5" Type="Boolean" />
        <asp:Parameter Name="Comunicazioni6" Type="Boolean" />
        <asp:Parameter Name="Comunicazioni7" Type="Boolean" />
        <asp:Parameter Name="Comunicazioni8" Type="Boolean" />
        <asp:Parameter Name="Comunicazioni9" Type="Boolean" />
        <asp:Parameter Name="Comunicazioni10" Type="Boolean" />
        <asp:Parameter Name="RecordEdtUser" />
        <asp:Parameter Name="RecordEdtDate" Type="DateTime" />
        <asp:ControlParameter ControlID="UIDHiddenField" Name="UserID"  />
    </UpdateParameters>
</asp:SqlDataSource>
