﻿<%@ Page Language="C#" MasterPageFile="~/Master/LayoutAreaSoci.master" Title="Sertot"
    CodeFile="Account_Email_Edt.aspx.cs"
    Inherits="Account_Email_Edt" %>

<%@ Register Src="~/UserControl/MenuSoci.ascx" TagName="Menu_Soci" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AreaTitle" runat="Server">
    Aggiornamento indirizzo E-Mail
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="AreaContentMain" runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <asp:HiddenField ID="UID" runat="server" />
                <div class="LAY01A_Content1">
                    <asp:FormView ID="MemberShipFormView" runat="server"
                        DefaultMode="ReadOnly"
                        DataSourceID="MemberShipSqlDataSource"
                        OnDataBound="MemberShipFormView_DataBound"
                        Width="100%" CellPadding="0">
                        <ItemTemplate>
                            <div class="ZITC010">
                                <div class="TFY_Titolo3">
                                    <asp:Label ID="AccountUtenteTitleLabel" runat="server" CssClass="TFY_Sottotitolo3" Text="Account utente"></asp:Label>
                                </div>
                                <table border="0" cellpadding="2" cellspacing="1">
                                    <tr>
                                        <td>
                                            <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="UserNameDescriptionLabel" runat="server" Text="Label">UserName:</asp:Label>
                                            <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="UserNameLabel" runat="server" Text='<%# Eval("UserName", "{0}") %>'></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label CssClass="HIC_LabelCorpoTesto1" ID="EMailDescriptionLabel" runat="server" Text="Label">E-mail: </asp:Label>
                                            <asp:HyperLink CssClass="HIC_LabelCorpoTesto1" ID="EmailHyperLink" runat="server"
                                                NavigateUrl="mailto:" Text='<%# Eval("Email", "{0}") %>'></asp:HyperLink>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ItemTemplate>
                    </asp:FormView>
                    <asp:SqlDataSource ID="MemberShipSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                        SelectCommand="SELECT aspnet_Membership.Password, aspnet_Membership.Email
        , aspnet_Membership.IsApproved, aspnet_Membership.CreateDate, aspnet_Membership.LastLoginDate
        , aspnet_Membership.LastPasswordChangedDate
        , aspnet_Membership.LastLockoutDate, aspnet_Membership.FailedPasswordAttemptCount, aspnet_Users.UserName
        , aspnet_Users.UserId AS Expr1, aspnet_Membership.IsLockedOut FROM aspnet_Membership INNER JOIN aspnet_Users ON aspnet_Membership.UserId = aspnet_Users.UserId WHERE (aspnet_Users.UserId = @UserID)">
                        <SelectParameters>
                            <asp:ControlParameter Name="UserID" ControlID="UID" DefaultValue="0" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <br />
                    <div class="ZITC010">
                        <div class="TFY_Titolo3">
                            <asp:Label ID="ModificaIndirizzoTitleLabel" runat="server" CssClass="TFY_Sottotitolo3" Text="Modifica indirizzo E-Mail"></asp:Label>
                        </div>
                        <table border="0" cellpadding="2" cellspacing="1">
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label ID="IndirizzoEMailDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server" Text="Indirizzo E-Mail *"></asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:TextBox ID="EmailTextBox" runat="server" Columns="50" CssClass="HIC_TextBox1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="EmailTextBox"
                                        ErrorMessage="Indirizzo E-Mail non corretto" CssClass="HIC_Validator1" Display="Dynamic"
                                        ValidationExpression="^((([a-z]|[0-9]|!|#|$|%|&|'|\*|\+|\-|/|=|\?|\^|_|`|\{|\||\}|~)+(\.([a-z]|[0-9]|!|#|$|%|&|'|\*|\+|\-|/|=|\?|\^|_|`|\{|\||\}|~)+)*)@((((([a-z]|[0-9])([a-z]|[0-9]|\-){0,61}([a-z]|[0-9])\.))*([a-z]|[0-9])([a-z]|[0-9]|\-){0,61}([a-z]|[0-9])\.(af|ax|al|dz|as|ad|ao|ai|aq|ag|ar|am|aw|au|at|az|bs|bh|bd|bb|by|be|bz|bj|bm|bt|bo|ba|bw|bv|br|io|bn|bg|bf|bi|kh|cm|ca|cv|ky|cf|td|cl|cn|cx|cc|co|km|cg|cd|ck|cr|ci|hr|cu|cy|cz|dk|dj|dm|do|ec|eg|sv|gq|er|ee|et|fk|fo|fj|fi|fr|gf|pf|tf|ga|gm|ge|de|gh|gi|gr|gl|gd|gp|gu|gt| gg|gn|gw|gy|ht|hm|va|hn|hk|hu|is|in|id|ir|iq|ie|im|il|it|jm|jp|je|jo|kz|ke|ki|kp|kr|kw|kg|la|lv|lb|ls|lr|ly|li|lt|lu|mo|mk|mg|mw|my|mv|ml|mt|mh|mq|mr|mu|yt|mx|fm|md|mc|mn|ms|ma|mz|mm|na|nr|np|nl|an|nc|nz|ni|ne|ng|nu|nf|mp|no|om|pk|pw|ps|pa|pg|py|pe|ph|pn|pl|pt|pr|qa|re|ro|ru|rw|sh|kn|lc|pm|vc|ws|sm|st|sa|sn|cs|sc|sl|sg|sk|si|sb|so|za|gs|es|lk|sd|sr|sj|sz|se|ch|sy|tw|tj|tz|th|tl|tg|tk|to|tt|tn|tr|tm|tc|tv|ug|ua|ae|gb|us|um|uy|uz|vu|ve|vn|vg|vi|wf|eh|ye|zm|zw|com|edu|gov|int|mil|net|org|biz|info|name|pro|aero|coop|museum|arpa))|(((([0-9]){1,3}\.){3}([0-9]){1,3}))|(\[((([0-9]){1,3}\.){3}([0-9]){1,3})\])))$"></asp:RegularExpressionValidator>
                                    <asp:RequiredFieldValidator ID="MailRequiredFieldValidator" runat="server" ErrorMessage="Obbligatorio"
                                        ControlToValidate="EmailTextBox" Display="Dynamic" CssClass="HIC_Validator1"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div align="center">
                        <%--<asp:LinkButton ID="UpdateButton" CssClass="HIC_LinkButton1" runat="server" OnClick="UpdateEmailButton_OnClick">Salva dati</asp:LinkButton>--%>
                        <asp:LinkButton ID="UpdateButton" CssClass="ZSSM_Button01_LinkButton" runat="server" OnClick="UpdateEmailButton_OnClick">Salva dati</asp:LinkButton>
                    </div>
                    <br />

                </div>
            </div>
            <div class="col-lg-3">
                <div class="LAY01A_Content2">
                    <uc1:Menu_Soci ID="Menu_Soci1" runat="server" />
                </div>
                <div class="FloatClr"></div>
            </div>
        </div>
    </div>
</asp:Content>
