﻿<%@ Control Language="C#"
    CodeFile="ProfiloPersonale_Dtl.ascx.cs"
    Inherits="Account_ProfiloPersonale_Dtl" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<asp:FormView ID="ProfiloPersonaleFormView" runat="server" DataKeyField="UserId"
    DataSourceID="ProfiloPersonaleSqlDataSource" Width="100%"
    OnDataBound="ProfiloPersonaleFormView_DataBound">
    <ItemTemplate>
        <div class="BXT_001">
            <div class="TFY_Titolo3">
                <asp:Label ID="ProfiloPersonaleTitleLabel" runat="server" CssClass="TFY_Sottotitolo3" Text="Profilo personale"></asp:Label>
            </div>
        </div>
        <table border="0" cellpadding="2" cellspacing="1">
            <tr>
                <td>
                    <asp:Label ID="CognomeDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server"
                        Text="Cognome:"></asp:Label>
                    <asp:Label ID="CognomeLabel" runat="server" CssClass="HIC_LabelCorpoTesto1" Text='<%# Eval("Cognome") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="NomeDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Nome:</asp:Label>
                    <asp:Label ID="NomeLabel" runat="server" CssClass="HIC_LabelCorpoTesto1" Text='<%# Eval("Nome") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="CodiceFiscaleDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Codice fiscale:</asp:Label>
                    <asp:Label ID="CodiceFiscaleLabel" runat="server" CssClass="HIC_LabelCorpoTesto1"
                        Text='<%# Eval("CodiceFiscale") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="PartitaIVADescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Partita IVA:</asp:Label>
                    <asp:Label ID="PartitaIVALabel" runat="server" CssClass="HIC_LabelCorpoTesto1"
                        Text='<%# Eval("PartitaIVA") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="QualificaDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Qualifica:</asp:Label>
                    <asp:Label ID="QualificaLabel" runat="server" CssClass="HIC_LabelCorpoTesto1" Text='<%# Eval("Qualifica") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="ProfessioneDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Professione:</asp:Label>
                    <asp:Label ID="ProfessioneLabel" runat="server" CssClass="HIC_LabelCorpoTesto1" Text='<%# Eval("Professione") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="SessoDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Sesso:</asp:Label>
                    <asp:Label ID="SessoLabel" runat="server" CssClass="HIC_LabelCorpoTesto1" Text='<%# Eval("Sesso") %>'
                        OnDataBinding="SessoLabel_DataBinding"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="IndirizzoDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Indirizzo:</asp:Label>
                    <asp:Label ID="IndirizzoLabel" runat="server" CssClass="HIC_LabelCorpoTesto1" Text='<%# Eval("Indirizzo") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="NumeroDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Numero:</asp:Label>
                    <asp:Label ID="NumeroLabel" runat="server" CssClass="HIC_LabelCorpoTesto1" Text='<%# Eval("Numero") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="CittaDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Città:</asp:Label>
                    <asp:Label ID="CittaLabel" runat="server" CssClass="HIC_LabelCorpoTesto1" Text='<%# Eval("Citta") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="CapDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">CAP:</asp:Label>
                    <asp:Label ID="CapLabel" runat="server" CssClass="HIC_LabelCorpoTesto1" Text='<%# Eval("Cap") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="ProvinciaSiglaDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Provincia (sigla):</asp:Label>
                    <asp:Label ID="ProvinciaSiglaLabel" CssClass="HIC_LabelCorpoTesto1" runat="server"
                        Text='<%# Eval("ProvinciaSigla") %>'>
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="NazioneDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Nazione:</asp:Label>
                    <asp:Label ID="NazioneLabel" runat="server" CssClass="HIC_LabelCorpoTesto1"
                        Text='<%# (Request.QueryString["Lang"] == "ITA" || Request.QueryString["Lang"] == null ? Eval("Nazione") : Eval("Nazione_ENG")) %>'
                        OnDataBinding="Clear"></asp:Label>
                </td>
            </tr>
            <asp:Panel ID="ContinentePanel" runat="server" Visible="false">
                <tr>
                    <td class="BlockBoxDescription">Continente:
                    </td>
                    <td class="BlockBoxValue">
                        <asp:Label ID="ContinenteLabel" runat="server" Text='<%# Eval("Continente") %>'>
                        </asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <tr>
                <td>
                    <asp:Label ID="NascitaDataDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Data di nascita:</asp:Label>
                    <asp:Label ID="NascitaDataLabel" runat="server" CssClass="HIC_LabelCorpoTesto1" Text='<%# Eval("NascitaData","{0:d}") %>'>
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="NascitaProvinciaDescriptionLabel" CssClass="HIC_LabelCorpoTesto1"
                        runat="server">Provincia di nascita (sigla):</asp:Label>
                    <asp:Label ID="NascitaProvinciaSiglaLabel" CssClass="HIC_LabelCorpoTesto1" runat="server"
                        Text='<%# Eval("NascitaProvinciaSigla") %>'>
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="NascitaNazioneDEscriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Nazione di nascita:</asp:Label>
                    <asp:Label ID="NascitaNazioneLabel" CssClass="HIC_LabelCorpoTesto1" runat="server"
                        Text='<%# Eval("NascitaNazione") %>' OnDataBinding="Clear">
                    </asp:Label>
                </td>
            </tr>
            <asp:Panel ID="ContinenteNascitaPanel" runat="server" Visible="false">
                <tr>
                    <td>
                        <asp:Label ID="NascitaContinenteDescriptionLabel" CssClass="HIC_LabelCorpoTesto1"
                            runat="server">Continente di nascita:</asp:Label>
                        <asp:Label ID="NascitaContinenteLabel" runat="server" Text='<%# Eval("NascitaContinente") %>'></asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <tr>
                <td>
                    <asp:Label ID="Telefono1DescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server"> Telefono:</asp:Label>
                    <asp:Label ID="Telefono1Label" CssClass="HIC_LabelCorpoTesto1" runat="server" Text='<%# Eval("Telefono1") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Telefono2DescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Telefono cellulare:</asp:Label>
                    <asp:Label ID="Telefono2Label" CssClass="HIC_LabelCorpoTesto1" runat="server" Text='<%# Eval("Telefono2") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="FaxDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Fax:</asp:Label>
                    <asp:Label ID="FaxLabel" CssClass="HIC_LabelCorpoTesto1" runat="server" Text='<%# Eval("Fax") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="NoteDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Note:</asp:Label>
                    <asp:Label ID="NoteLabel" runat="server" CssClass="HIC_LabelCorpoTesto1" Text='<%# Eval("Note") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="AffiliazioneDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Affiliazione:</asp:Label>
                    <asp:Label ID="AffiliazioneLabel" runat="server" CssClass="HIC_LabelCorpoTesto1" Text='<%# Eval("Affiliazione") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="SitoWebDescriptionLabel" CssClass="HIC_LabelCorpoTesto1" runat="server">Sito web:</asp:Label>
                    <asp:Label ID="SitoWebLabel" runat="server" CssClass="HIC_LabelCorpoTesto1" Text='<%# Eval("SitoWeb") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <asp:Label ID="EnabledLabel" runat="server" Visible="false" Text="Attenzione: dati non presenti."></asp:Label>
                </td>
            </tr>
            <tr>
            </tr>
        </table>
        <div style="text-align: center">
            <%--<asp:LinkButton ID="ModificaLinkButton" runat="server" CssClass="HIC_LinkButton1" Text="Modifica"
                PostBackUrl='<%# Eval("UserId","ProfiloPersonale_Edt.aspx" ) %>'></asp:LinkButton>--%>
            <asp:LinkButton ID="ModificaLinkButton" runat="server" CssClass="ZSSM_Button01_LinkButton" Text="Modifica"
                PostBackUrl='<%# Eval("UserId","ProfiloPersonale_Edt.aspx" ) %>'></asp:LinkButton>
        </div>        
    </ItemTemplate>
</asp:FormView>
<asp:SqlDataSource ID="ProfiloPersonaleSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
    SelectCommand="SELECT vwProfiliPersonali.[UserId], [Cognome], [Nome], [CodiceFiscale], [PartitaIVA], [Qualifica],[Professione],
        [Cap], [ProvinciaSigla], [Nazione],Nazione_ENG, [Continente], 
        [NascitaData], [NascitaProvinciaSigla], [NascitaNazione],NascitaNazione_ENG, [NascitaContinente], 
        [Fax], [Note],[Sesso], [Affiliazione], [SitoWeb]
	    , CASE WHEN tbProfiliMarketing.ConsensoDatiPersonali2 = 0 THEN '-' ELSE vwProfiliPersonali.Telefono1 END AS Telefono1
	    , CASE WHEN tbProfiliMarketing.ConsensoDatiPersonali2 = 0 THEN '-' ELSE vwProfiliPersonali.Telefono2 END AS Telefono2
	    , CASE WHEN tbProfiliMarketing.ConsensoDatiPersonali2 = 0 THEN '-' ELSE vwProfiliPersonali.Indirizzo END AS Indirizzo	
	    , CASE WHEN tbProfiliMarketing.ConsensoDatiPersonali2 = 0 THEN '-' ELSE vwProfiliPersonali.Numero END AS Numero	
	    , CASE WHEN tbProfiliMarketing.ConsensoDatiPersonali2 = 0 THEN '-' ELSE vwProfiliPersonali.Citta END AS Citta
    FROM [vwProfiliPersonali]
        INNER JOIN tbProfiliMarketing ON tbProfiliMarketing.UserId = vwProfiliPersonali.UserId"></asp:SqlDataSource>
