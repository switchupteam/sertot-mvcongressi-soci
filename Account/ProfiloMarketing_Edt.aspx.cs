﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per ProfiloMarketing_Edt
/// </summary>
public partial class Account_ProfiloMarketing_Edt : System.Web.UI.Page
{

    #region Fields

    private string Lang = "ITA";

    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();

        if (Membership.GetUser() == null)
            Response.Redirect("~/Zeus/System/Message.aspx?NoUser");

        if (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang") && Request.QueryString["Lang"] != null)
            Response.Redirect("~/System/Message.aspx?0");

        Lang = Request.QueryString["Lang"] ?? Lang;
        if (Lang.Length == 0)
            Lang = "ITA";

        ReadXML_Localization("PROFILI", Lang);

        ProfiloMarketing_Edt1.UID = Membership.GetUser().ProviderUserKey.ToString();
        ProfiloMarketing_Edt1.RedirectPath = "~/Account/";
        ProfiloMarketing_Edt1.Lang = Lang;
    }

    #endregion

    #region Methods

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";
            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode + "/PROFILO_MARKETING";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Profili.xml"));

            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                            if (myLabel != null)
                                myLabel.Text = childNode.InnerText;
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name + "Label");

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }

            //LinkButton ModificaLinkButton = (LinkButton)myDelinea.FindControlRecursive(Page, "UpdateButton");
            //ModificaLinkButton.Text = (mydoc.SelectSingleNode(myXPath + "/" + "UpdateButton").InnerText);
            Page.Title = (mydoc.SelectSingleNode(myXPath + "/" + "TitoloPaginaProfiloMarketingEdt").InnerText);
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    #endregion

}