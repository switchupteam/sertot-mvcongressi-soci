﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_ProfiloPersonaleImg_Dtl : System.Web.UI.UserControl
{
    #region Fields

    private string UserID = string.Empty;
    private string lang = string.Empty;

    #endregion

    #region Properties

    public string UID
    {
        set { UserID = value; }
        get { return UserID; }
    }

    public string Lang
    {
        set { lang = value; }
        get { return lang; }
    }

    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
            BindTheData();
    }

    #endregion

    #region Methods

    public bool BindTheData()
    {
        try
        {
            ProfiloPersonaleImgSqlDataSource.SelectCommand += " WHERE ([UserID] = '" + UserID + "')";
            ProfiloPersonaleImgFormView.DataSourceID = "ProfiloPersonaleImgSqlDataSource";
            ProfiloPersonaleImgSqlDataSource.DataBind();
            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    #endregion    

}