﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

public class clsGenericUtils {
    public static string strConnessione {
        get {
            return System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        }
    }



    public static bool IsSocioOnorario(string sUserId) {
        bool result = false;

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        using (SqlConnection connection = new SqlConnection(strConnessione)) {
            try {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                string sqlQuery = @"SELECT aspnet_Roles.RoleName
                                    FROM aspnet_Roles
                                        INNER JOIN aspnet_UsersInRoles ON aspnet_UsersInRoles.RoleId = aspnet_Roles.RoleId
                                    WHERE aspnet_UsersInRoles.UserId = @UserId";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@UserId"].Value = new Guid(sUserId);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read()) {
                    if (reader["RoleName"] != null && reader["RoleName"].ToString().Equals("SociOnorari")) {
                        result = true;
                        break;
                    }
                }

                reader.Close();
            }
            catch (Exception p) {
                throw;
            }
        }//fine Using


        return result;
    }

    /// <summary>
    /// Il bottone per mostrare il messaggio di rinnovo inizia a comparire a dicembre dell'anno precedente a quello da rinnovare.
    /// </summary>
    public static bool NeedsRenewal(string sUserId) {
        int iAnno, iMese;
        int iDicembre = 12;

        int iUltimoRinnovo = GetLastRenewal(sUserId);                      // Recupero l'anno dell'ultimo rinnovo. Ritorna 0 se non ne esistono

        iAnno = int.Parse(DateTime.Now.ToString("yyyy"));           // Recupero anno ...
        iMese = int.Parse(DateTime.Now.ToString("MM"));             // ... e mese corrente

        if (iMese == iDicembre)                         // Se siamo a dicembre, dobbiano rinnovare
            iAnno++;                                    // per l'anno prossimo

        if (iAnno > iUltimoRinnovo)                     // Se non esistono rinnovi con anno maggiore o uguale a quello in corso, allora mostro il bottone.
            return true;

        return false;
    }

    /// <summary>
    /// Recupera l'ultimo rinnovo dell'utente specificato. Ritorna 0 se non trova rinnovi
    /// </summary>
    private static int GetLastRenewal(string sUserId) {
        StringBuilder sbSQL = new StringBuilder();

        sbSQL.AppendFormat(" SELECT ISNULL (( ");
        sbSQL.AppendFormat(" 	SELECT TOP 1 Anno ");
        sbSQL.AppendFormat(" 		FROM tbQuoteAnnuali ");
        sbSQL.AppendFormat(" 	WHERE UserID = @UserID ");
        sbSQL.AppendFormat(" 		AND ZeusIsAlive = 1  ");
        sbSQL.AppendFormat(" 	ORDER BY Anno DESC), 0 ");
        sbSQL.AppendFormat(" ) AnnoUltimaQuota ");
        sbSQL.AppendFormat("  ");

        using (SqlConnection connection = new SqlConnection(clsGenericUtils.strConnessione))
            try {

                connection.Open();
                SqlCommand sqlCmd = connection.CreateCommand();
                sqlCmd.CommandText = sbSQL.ToString();

                sqlCmd.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
                sqlCmd.Parameters["@UserId"].Value = new Guid(sUserId);

                var varAnnoUltimaQuota = sqlCmd.ExecuteScalar();
                int iAnnoUltimaQuota;

                if (int.TryParse(varAnnoUltimaQuota.ToString(), out iAnnoUltimaQuota))
                    return iAnnoUltimaQuota;

            }
            catch (Exception ex) {
                throw;
            }

        return 0;
    }





}