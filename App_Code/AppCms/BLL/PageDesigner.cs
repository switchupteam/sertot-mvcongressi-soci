﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZeusDataSetTableAdapters;

/// <summary>
/// Summary description for PageDesigner
/// </summary>
namespace AppCms.BLL
{
    public class PageDesigner
    {
        private vwPageDesigner_Public_DtlTableAdapter pageDesignerAdapter = null;

        protected vwPageDesigner_Public_DtlTableAdapter PageDesignerAdapter
        {
            get
            {
                if (pageDesignerAdapter == null)
                {
                    pageDesignerAdapter = new vwPageDesigner_Public_DtlTableAdapter();
                    pageDesignerAdapter.Connection.ConnectionString = Configurations.Configuration.ZeusConnectionString;
                }
                return pageDesignerAdapter;
            }
        }


        public ZeusDataSet.vwPageDesigner_Public_DtlDataTable GetAll(string ZeusIdModule)
        {
            return PageDesignerAdapter.GetData(ZeusIdModule, "ITA");
        }

        public void PostMessage(string ZeusIdModule, string Title, string Message)
        {
            PageDesignerAdapter.InsertPageDesigner(ZeusIdModule, "ITA", Title, Message, DateTime.Now, true, new DateTime(2060, 1, 1), true, true, DateTime.Now, new DateTime(2060, 1, 1));
        
        }


    }
}