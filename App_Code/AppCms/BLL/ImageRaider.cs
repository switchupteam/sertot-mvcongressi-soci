﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZeusDataSetTableAdapters;

/// <summary>
/// Summary description for ImageRaider
/// </summary>
/// 
namespace AppCms.BLL
{
    public class ImageRaider
    {
        public enum RaiderIndex
        {
            Image1, Image2, Image3, Image4
        }

        private tbPZ_ImageRaiderTableAdapter imageraiderAdapter = null;

        protected tbPZ_ImageRaiderTableAdapter ImageRaiderAdapter
        {
            get
            {
                if (imageraiderAdapter == null)
                {
                    imageraiderAdapter = new tbPZ_ImageRaiderTableAdapter();
                    imageraiderAdapter.Connection.ConnectionString = Configurations.Configuration.ZeusConnectionString;
                }
                return imageraiderAdapter;
            }
        }

        /// <summary>
        /// It returns the relative image folder with ~
        /// </summary>
        /// <param name="RaiderIndex"></param>
        /// <returns></returns>
        public string GetRelativePath(RaiderIndex RaiderIndex, string ZeusIdModule)
        {
            ZeusDataSet.tbPZ_ImageRaiderRow row = null;
            switch (RaiderIndex)
            {
                case (RaiderIndex.Image1):
                    row = GetImageRaiderRow(ZeusIdModule, "1");
                    return (row != null ? (row.IsImgBeta_PathNull() ? row.ImgPreview_BetaDefault : row.ImgBeta_Path) : "");
                case (RaiderIndex.Image2):
                    row = GetImageRaiderRow(ZeusIdModule, "1");
                    return (row != null ? (row.IsImgGamma_PathNull() ? row.ImgPreview_GammaDefault : row.ImgGamma_Path) : "");
                case (RaiderIndex.Image3):
                    row = GetImageRaiderRow(ZeusIdModule, "2");
                    return (row != null ? (row.IsImgBeta_PathNull() ? row.ImgPreview_BetaDefault : row.ImgBeta_Path) : "");
                case (RaiderIndex.Image4):
                    row = GetImageRaiderRow(ZeusIdModule, "2");
                    return (row != null ? (row.IsImgGamma_PathNull() ? row.ImgPreview_GammaDefault : row.ImgGamma_Path) : "");
                default:
                    return string.Empty;
            }


        }

        /// <summary>
        /// It returns the relative image path with ~
        /// </summary>
        /// <param name="RaiderIndex"></param>
        /// <returns></returns>
        public string GetRelativeUrlDefaultImage(RaiderIndex RaiderIndex, string ZeusIdModule)
        {
            ZeusDataSet.tbPZ_ImageRaiderRow row = null;
            switch (RaiderIndex)
            {
                case (RaiderIndex.Image1):
                    row = GetImageRaiderRow(ZeusIdModule, "1");
                    return (row != null ? row.ImgPreview_BetaDefault : null);
                case (RaiderIndex.Image2):
                    row = GetImageRaiderRow(ZeusIdModule, "1");
                    return (row != null ? row.ImgPreview_GammaDefault : null);
                case (RaiderIndex.Image3):
                    row = GetImageRaiderRow(ZeusIdModule, "2");
                    return (row != null ? row.ImgPreview_BetaDefault : null);
                case (RaiderIndex.Image4):
                    row = GetImageRaiderRow(ZeusIdModule, "2");
                    return (row != null ? row.ImgPreview_GammaDefault : null);
                
                default:
                    return null;
            }


        }
        private ZeusDataSet.tbPZ_ImageRaiderRow GetImageRaiderRow(string module, string index)
        {
            var rows = ImageRaiderAdapter.GetData(module + index, "ITA").Rows;
            if (rows.Count == 0)
                return null;
            else
                return rows[0] as ZeusDataSet.tbPZ_ImageRaiderRow;
        }




    }

}
