﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZeusDataSetTableAdapters;

/// <summary>
/// Summary description for Publisher
/// </summary>
/// 
namespace AppCms.BLL
{
    public class Publisher
    {
        private vwPublisher_Public_LstArea1TableAdapter publisherAdapter = null;

        protected vwPublisher_Public_LstArea1TableAdapter PublisherAdapter
        {
            get
            {
                if (publisherAdapter == null)
                {
                    publisherAdapter = new vwPublisher_Public_LstArea1TableAdapter();
                    publisherAdapter.Connection.ConnectionString = Configurations.Configuration.ZeusConnectionString;
                }
                return publisherAdapter;
            }
        }


        public ZeusDataSet.vwPublisher_Public_LstArea1DataTable GetAll(string ZeusIdModule)
        {
         
            return PublisherAdapter.GetData(ZeusIdModule, "ITA");
        }

        public void PostMessage(string ZeusIdModule, string Title, string Message)
        {
            PublisherAdapter.InsertPublisher(ZeusIdModule, "ITA", Title, Message, DateTime.Now, true, new DateTime(2060, 1, 1), true, true, DateTime.Now, new DateTime(2060, 1, 1));

        }

    }
}