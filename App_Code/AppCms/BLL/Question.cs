﻿using QuestionnaireDataSetTableAdapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Question
/// </summary>
namespace AppCms.BLL
{
    public class Question
    {
        private tbQuestionsTableAdapter questionsAdapter = null;

        protected tbQuestionsTableAdapter QuestionsAdapter
        {
            get
            {
                if (questionsAdapter == null)
                {
                    questionsAdapter = new tbQuestionsTableAdapter();
                    questionsAdapter.Connection.ConnectionString = AppCms.Configurations.Configuration.ZeusConnectionString;
                }
                return questionsAdapter;
            }
        }


        public QuestionnaireDataSet.tbQuestionsDataTable GetAll()
        {
            return QuestionsAdapter.GetAll();
        }


        public QuestionnaireDataSet.tbQuestionsDataTable GetAllActive(int ID1Questionnaire)
        {
            return QuestionsAdapter.GetQuestionsByQuestionnaire(ID1Questionnaire);
        }

        public List<QuestionItem> GetAllQuestionActive()
        {
            Questionnaire q = new Questionnaire();
            QuestionnaireDataSet.tbQuestionnairesDataTable dt = q.GetAllActive();
            if (dt != null && dt.Rows.Count > 0)
            {
                QuestionnaireDataSet.tbQuestionnairesRow questionnaire = dt.Rows[0] as QuestionnaireDataSet.tbQuestionnairesRow;

                Answer answer = new Answer();
                List<QuestionItem> questions = new List<QuestionItem>();
                foreach (QuestionnaireDataSet.tbQuestionsRow question in GetAllActive(questionnaire.ID1Questionnaire))
                {
                    QuestionItem qi = new QuestionItem() { ID = question.ID1Question, Question = question.Text, IsRadio = question.IsRadio, HasOpenAnswer = question.HasOpenAnswer };
                    foreach (QuestionnaireDataSet.tbAnswersRow a in answer.GetAllActive(question.ID1Question))
                    {
                        qi.Answers.Add(new AnswerItem() { ID = a.ID1Answer, Answer = a.Text });
                    }


                    questions.Add(qi);
                }

                return questions;
            }
            else
                return null;


        }



    }
}