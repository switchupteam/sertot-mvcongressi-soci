﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZeusDataSetTableAdapters;

/// <summary>
/// Summary description for Device
/// Changed
/// </summary>
/// 

namespace AppCms.BLL
{
    public class Device
    {
        private DevicesTableAdapter deviceAdapter = null;

        protected DevicesTableAdapter DeviceAdapter
        {
            get
            {   


                if (deviceAdapter == null)
                {
                    deviceAdapter = new DevicesTableAdapter();
                    deviceAdapter.Connection.ConnectionString = Configurations.Configuration.ZeusConnectionString;
                }
                return deviceAdapter;
            }
        }


        public ZeusDataSet.DevicesDataTable GetAll()
        {
            return DeviceAdapter.GetData();
        }

        private bool ExistsDevice(string deviceId, string type)
        {
            return DeviceAdapter.GetDeviceBy(deviceId, type).Rows.Count > 0;
        }

        public void SaveDevice(string deviceId, string type)
        {
            DateTime lastSeen = DateTime.Now;
            if (ExistsDevice(deviceId, type))
                DeviceAdapter.UpdateDeviceLastSeen(lastSeen, deviceId, type);
            else
               
            DeviceAdapter.AddDevice(deviceId, type, lastSeen);

        
        }





    }
}