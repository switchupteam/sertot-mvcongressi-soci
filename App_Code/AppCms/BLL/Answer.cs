﻿using QuestionnaireDataSetTableAdapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Answer
/// </summary>
 namespace AppCms.BLL
{
public class Answer
 {
        private tbAnswersTableAdapter questionsAdapter = null;
        private tbAnswerResultsTableAdapter answerResultsAdapter = null;
        private tbOpenAnswersTableAdapter openAnswersTableAdapter = null;
        protected tbAnswersTableAdapter AnswersAdapter
        {
            get
            {
                if (questionsAdapter == null)
                {
                    questionsAdapter = new tbAnswersTableAdapter();
                    questionsAdapter.Connection.ConnectionString = AppCms.Configurations.Configuration.ZeusConnectionString;
                }
                return questionsAdapter;
            }
        }

        protected tbAnswerResultsTableAdapter AnswerResultsAdapter
        {
            get
            {
                if (answerResultsAdapter == null)
                {
                    answerResultsAdapter = new tbAnswerResultsTableAdapter();
                    answerResultsAdapter.Connection.ConnectionString = AppCms.Configurations.Configuration.ZeusConnectionString;
                }
                return answerResultsAdapter;
            }
        }
        protected tbOpenAnswersTableAdapter OpenAnswersTableAdapter
        {
            get
            {
                if (openAnswersTableAdapter == null)
                {
                    openAnswersTableAdapter = new tbOpenAnswersTableAdapter();
                    openAnswersTableAdapter.Connection.ConnectionString = AppCms.Configurations.Configuration.ZeusConnectionString;
                }
                return openAnswersTableAdapter;
            }
        }


        public QuestionnaireDataSet.tbAnswersDataTable GetAllActive(int ID1Question)
        {
            return AnswersAdapter.GetAnswersByQuestion(ID1Question);
        }

        public void AddAnswerFromQuestionnaire(int ID1Answer)
        {
            AnswerResultsAdapter.Insert(ID1Answer, DateTime.Now);
        
        }


        public void AddOpenAnswer(int ID1Question, string Answer)
        {
            try
            {
                OpenAnswersTableAdapter.InsertQuery(Answer, ID1Question);
            }
            catch
            { }
            
        }

    }
}