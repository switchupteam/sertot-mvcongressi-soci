﻿using QuestionnaireDataSetTableAdapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Questionnaire
/// </summary>
/// 
namespace AppCms.BLL
{
    public class Questionnaire
    {
        private tbQuestionnairesTableAdapter questionnairesAdapter = null;

        protected tbQuestionnairesTableAdapter QuestionnairesAdapter
        {
            get
            {
                if (questionnairesAdapter == null)
                {
                    questionnairesAdapter = new tbQuestionnairesTableAdapter();
                    questionnairesAdapter.Connection.ConnectionString = AppCms.Configurations.Configuration.ZeusConnectionString;
                }
                return questionnairesAdapter;
            }
        }


        public QuestionnaireDataSet.tbQuestionnairesDataTable GetAll()
        {
            return QuestionnairesAdapter.GetAll();
        }


        public QuestionnaireDataSet.tbQuestionnairesDataTable GetAllActive()
        {
            return QuestionnairesAdapter.GetActive();
        }

     


    }
}