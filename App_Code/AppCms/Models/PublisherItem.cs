﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PublisherItem
/// </summary>
/// 
namespace AppCms.Models
{
    public class PublisherItem
    {

        public int id { get; set; }
        public string title { get; set; }
        public string desc { get; set; }
        public string body { get; set; }
        public string html { get; set; }
        public DateTime? date { get; set; }
        public DateTime? date2 { get; set; }
        public string image { get; set; }
        public string image2 { get; set; }
        public string jolly { get; set; }
        public string share { get; set; }
        public string subtitle { get; set; }
        public string exturl { get; set; }
    }
}