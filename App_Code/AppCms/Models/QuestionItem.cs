﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for QuestionItem
/// </summary>
public class QuestionItem
{
    public QuestionItem()
    {
        this.Answers = new List<AnswerItem>();
    }
    public string Question { get; set; }
    public int ID { get; set; }
    public bool IsRadio { get; set; }
    public bool HasOpenAnswer { get; set; }
    public IList<AnswerItem> Answers { get; set; }
}

public class AnswerItem
{
    public int ID { get; set; }
    public string Answer { get; set; }

}



