﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PostElementRequest
/// </summary>
public class PutElementRequest
{
	public PutElementRequest()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    [MaxLength(100)]
    public string title { get; set; }
    public string content { get; set; }
}