﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PageDesignerItem
/// </summary>
/// 
namespace AppCms.Models
{
    public class PageDesignerItem
    {
        public int id { get; set; }
        public string title { get; set; }
        public string body { get; set; }
        public string html { get; set; }
        public string image { get; set; }
        public string image2 { get; set; }
        public string jolly { get; set; }
        public string share { get; set; }
        public string url
        {
            get
            {
                return string.Format("page/{0}", id);
            }
        }
    }
}