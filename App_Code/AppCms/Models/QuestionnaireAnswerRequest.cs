﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DeviceRequest
/// </summary>
/// 
namespace AppCms.Models
{
    public class QuestionnaireAnswerRequest
    {

        public int[] AnwerIds { get; set; }
        public OpenAnswer[] OpenAnswers { get; set; }

    }

    public class OpenAnswer
    {
        public int QuestionId { get; set; }
        public string Answer { get; set; }
    }
}