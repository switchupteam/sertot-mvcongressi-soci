﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DeviceRequest
/// </summary>
/// 
namespace AppCms.Models
{
    public class DeviceRequest
    {
        [RegularExpression("^([a-zA-Z0-9_.-]+)$", ErrorMessage = "Invalid Device Id")]
        [Required]
        public string deviceId { get; set; }
        //Device Types allowed values are
        [MaxLength(3)]
        [RegularExpression("^(IOS|AND|WIN)$", ErrorMessage = "Invalid device type: allowed values are IOS,AND and WIN")]
        public string type { get; set; }
    }
}