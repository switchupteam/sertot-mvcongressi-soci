﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for WebApiConfig
/// </summary>
/// 
namespace AppCms
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {

            // Web API routes
            config.MapHttpAttributeRoutes();


            foreach (var m in Configurations.Configuration.PublisherModules)
            {
                config.Routes.MapHttpRoute(
                    name: "ConfigApiPu" + m.Route,
                    routeTemplate: string.Concat("api/", m.Route, "/{id}"),
                    defaults: new { id = RouteParameter.Optional, controller = "pu" }
                );

            }

            foreach (var m in Configurations.Configuration.PageDesignerModules)
            {
                config.Routes.MapHttpRoute(
                    name: "ConfigApiPd" + m.Route,
                    routeTemplate: string.Concat("api/", m.Route, "/{id}"),
                    defaults: new { id = RouteParameter.Optional, controller = "pd" }
                );
            }

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

        }
    }

}
