﻿using AppCms.BLL;
using AppCms.Models;
using AppCms.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using AppCms.Extensions;

/// <summary>
/// Summary description for PageController
/// </summary>
/// 
namespace AppCms.Controllers
{
    public class PdController : ApiController
    {

        public HttpResponseMessage Get()
        {

            var module = GetCurrentModule();
            if (module == null)
                return Request.CreateResponse(HttpStatusCode.NotFound);

            var response = Request.CreateResponse(HttpStatusCode.OK, GetPages(module));
            response.Headers.CacheControl = new System.Net.Http.Headers.CacheControlHeaderValue()
            {
                NoCache = true,
                NoStore = true
            };

            return response;
        }

        private AppCms.Configurations.Module GetCurrentModule()
        {
            var moduleRoute = Request.RequestUri.Segments[2].ToString();
            moduleRoute = moduleRoute.Remove(moduleRoute.Length - 1);
            AppCms.Configurations.Module module = Configurations.Configuration.PageDesignerModules.FirstOrDefault(f => f.Route == moduleRoute);
            return module;

        }

        public IHttpActionResult Get(string id)
        {
            var module = GetCurrentModule();
            if (module == null)
                return NotFound();

            var jollyItem = GetPages(module).FirstOrDefault(f => f.jolly == id);
            if (jollyItem == null)
                return NotFound();
            else
                return Ok(jollyItem);
        }

        private PageDesignerItem[] GetPages(AppCms.Configurations.Module module)
        {
            PageDesigner pd = new PageDesigner();
            CustomHttpUtility http = new CustomHttpUtility();
            var domain = Configurations.Configuration.Domain;

            var urlFolder = Configurations.Configuration.GetImagePath(module.ZeusId, ImageRaider.RaiderIndex.Image1);
            var defaultImage = Configurations.Configuration.GetDefaultImagePath(module.ZeusId, ImageRaider.RaiderIndex.Image1);
            var urlFolder2 = Configurations.Configuration.GetImagePath(module.ZeusId, ImageRaider.RaiderIndex.Image2);
            var defaultImage2 = Configurations.Configuration.GetDefaultImagePath(module.ZeusId, ImageRaider.RaiderIndex.Image2);

            return (from e in pd.GetAll(module.ZeusId).AsEnumerable()
                    select new PageDesignerItem()
                    {
                        id = e.ID1Pagina,
                        body = (e.IsDescBreve1Null() ? "" : e.DescBreve1.EncodeString().RemoveLineBreak()),
                        image = (e.IsImmagine1Null() || string.IsNullOrWhiteSpace(e.Immagine1) || string.IsNullOrWhiteSpace(urlFolder) ? null : (http.CheckFileExist(string.Concat(urlFolder, e.Immagine1)) ? string.Concat(urlFolder, e.Immagine1) : defaultImage)),
                        image2 = (e.IsImmagine2Null() || string.IsNullOrWhiteSpace(e.Immagine2) || string.IsNullOrWhiteSpace(urlFolder2) ? null : (http.CheckFileExist(string.Concat(urlFolder2, e.Immagine2)) ? string.Concat(urlFolder2, e.Immagine2) : defaultImage2)),
                        title = e.TitoloPagina,
                        jolly = (e.IsZeusJolly2Null() ? "" : e.ZeusJolly2),
                        share = (e.IsUrlRewriteNull() ? "" : string.Format("https://{0}/Web/{1}/{2}", domain, e.ID1Pagina, e.UrlRewrite)),
                        html = (e.IsContenuto3Null() ? "" : e.Contenuto3),
                    }).ToArray();


        }


        [HttpPut]
        public IHttpActionResult Put(PutElementRequest model)
        {
            IEnumerable<string> headerValues = Request.Headers.GetValues("X-SwitchUp");
            if (headerValues.FirstOrDefault() != "00-225455-452114")
                return BadRequest();

            if (ModelState.IsValid)
            {
                AppCms.Configurations.Module module = GetCurrentModule();
                PageDesigner pd = new PageDesigner();
                pd.PostMessage(module.ZeusId, model.title.RemoveHtml(), model.content.RemoveHtml());
                return Ok("Successfully posted!");
            }
            else
                return BadRequest();

        }

    }





}