﻿using AppCms.BLL;
using AppCms.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for DevicesController
/// </summary>
/// 
namespace AppCms.Controllers
{
    public class DeviceController : ApiController
    {
        private Device db = new Device();

        [HttpGet]
        public IHttpActionResult Get()
        {
            var deviceDT = db.GetAll();


            if (deviceDT.Rows.Count == 0)
                return NotFound();
            else
                return Ok(deviceDT.AsEnumerable().Select(s => new { deviceId = s.deviceId, type = s.type, lastSeen = s.lastSeen }).ToArray());
        }


        [HttpPut]
        public IHttpActionResult Put(DeviceRequest deviceRequest)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.SaveDevice(deviceRequest.deviceId, deviceRequest.type);
                    return Ok();
                }
                catch
                {
                    throw new Exception("Unable to register device for push notifications");
                }
            }
            else
                return BadRequest(ModelState);


        }
    }
}