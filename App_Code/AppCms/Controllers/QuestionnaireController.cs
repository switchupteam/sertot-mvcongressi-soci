﻿using AppCms.BLL;
using AppCms.Extensions;
using AppCms.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for QuestionnaireController
/// </summary>
/// 
namespace AppCms.Controllers
{
    public class QuestionnaireController : ApiController
    {
        public QuestionnaireController()
        {
            //
            // TODO: Add constructor logic here
            //
        }


        [HttpGet]
        public HttpResponseMessage Get()
        {
            Question question = new Question();
            //string html = "<form method=\"post\">";
            List<QuestionItem> questions = question.GetAllQuestionActive();
            //if (questions != null)
            //    foreach (var q in questions)
            //    {
            //        html += string.Format("<br /><label>{0}</label>", q.Question.EncodeString());
            //        foreach (var a in q.Answers)
            //            html += string.Format("<span><input type=\"checkbox\" name=\"answ{0}\" value=\"{2}\" />{1}</span>", a.ID, a.Answer.EncodeString(), q.ID);

            //    }

            //html += "<br /><input type=\"submit\" value=\"submit\"></form>";


            //var response = new HttpResponseMessage();
            //response.Content = new StringContent(html);
            //response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            //return response;

            return  Request.CreateResponse(HttpStatusCode.OK, questions);


        }

        //[HttpPost]
        //public HttpResponseMessage Post([FromBody]  FormDataCollection formData)
        //{
        //    Answer a = new Answer();
        //    foreach (var keypair in formData)
        //    { 
        //        var questionId = 0;
        //        var answerId = 0 ;
        //        var key = keypair.Key.Replace("answ", "");
        //        if (int.TryParse(key, out answerId) && int.TryParse(keypair.Value, out questionId))
        //            a.AddAnswerFromQuestionnaire(answerId);
        //    }
        //    var html = "<div><form method=\"get\">Grazie per aver partecipato al sondaggio clicca su ok per ritornare al questionario</div><div><input type='submit' value='ok' /></form></div>";
        //    var response = new HttpResponseMessage();
        //    response.Content = new StringContent(html);
        //    response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
        //    return response;
        //}

        [HttpPost]
        public IHttpActionResult Post(QuestionnaireAnswerRequest model)
        {
            if (ModelState.IsValid)
            {
                Answer a = new Answer();
                foreach (var id in model.AnwerIds)
                    a.AddAnswerFromQuestionnaire(id);

                if (model.OpenAnswers != null && model.OpenAnswers.Count() > 0)
                {
                    foreach (OpenAnswer o in model.OpenAnswers)
                        a.AddOpenAnswer(o.QuestionId, o.Answer);
                
                }


                return Ok();

            }
            else
                return BadRequest();
        
        
        }



    }
}