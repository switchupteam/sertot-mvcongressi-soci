﻿using AppCms.BLL;
using AppCms.Models;
using AppCms.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using AppCms.Extensions;
using System.Data.SqlClient;

/// <summary>
/// Summary description for NewsController
/// </summary>
/// 
namespace AppCms.Controllers
{
    public class PuController : ApiController
    {

        public HttpResponseMessage Get()
        {
            var module = GetCurrentModule();
            if (module == null)
                return Request.CreateResponse(HttpStatusCode.NotFound);
            var response = Request.CreateResponse(HttpStatusCode.OK, GetFeedItemsDbRaw(module));
            response.Headers.CacheControl = new System.Net.Http.Headers.CacheControlHeaderValue()
            {
                NoCache = true,
                NoStore = true
            };

            return response;

        }

        [HttpGet]
        public IHttpActionResult Get(string id)
        {
            var module = GetCurrentModule();
            if (module == null)
                return NotFound();
            var jollyItem = GetFeedItemsDbRaw(module).FirstOrDefault(f => f.jolly == id);
            if (jollyItem == null)
                return NotFound();
            else
                return Ok(jollyItem);
        }

        private PublisherItem[] GetFeedItems(AppCms.Configurations.Module module)
        {

            Publisher pub = new Publisher();
            CustomHttpUtility http = new CustomHttpUtility();
            var urlFolder = Configurations.Configuration.GetImagePath(module.ZeusId, ImageRaider.RaiderIndex.Image1);
            var defaultImage = Configurations.Configuration.GetDefaultImagePath(module.ZeusId, ImageRaider.RaiderIndex.Image1);
            var urlFolder2 = Configurations.Configuration.GetImagePath(module.ZeusId, ImageRaider.RaiderIndex.Image2);
            var defaultImage2 = Configurations.Configuration.GetDefaultImagePath(module.ZeusId, ImageRaider.RaiderIndex.Image2);
            var domain = Configurations.Configuration.Domain;


            return (from e in pub.GetAll(module.ZeusId).AsEnumerable()
                    select new PublisherItem()
                    {
                        id = e.ID1Publisher,
                        body = (e.IsDescBreve1Null() ? null : e.DescBreve1.EncodeString().RemoveLineBreak()),
                        image = (e.IsImmagine1Null() || string.IsNullOrWhiteSpace(e.Immagine1) || string.IsNullOrWhiteSpace(urlFolder) ? null : (http.CheckFileExist(string.Concat(urlFolder, e.Immagine1)) ? string.Concat(urlFolder, e.Immagine1) : defaultImage)),
                        image2 = (e.IsImmagine2Null() || string.IsNullOrWhiteSpace(e.Immagine2) || string.IsNullOrWhiteSpace(urlFolder2) ? null : (http.CheckFileExist(string.Concat(urlFolder2, e.Immagine2)) ? string.Concat(urlFolder2, e.Immagine2) : defaultImage2)),
                        title = e.Titolo,
                        jolly = (e.IsZeusJolly2Null() ? "" : e.ZeusJolly2),
                        share = (e.IsUrlRewriteNull() ? "" : string.Format("https://{0}/News/{1}/{2}", domain, e.ID1Publisher, e.UrlRewrite)),
                        exturl = (e.IsFonte_UrlNull() ? "" : e.Fonte_Url),
                        subtitle = (e.IsSottotitoloNull() ? "" : e.Sottotitolo),
                        date = e.PWI_Area1,
                        date2 = e.PWI_Area2,
                        html = (e.IsContenuto3Null() ? "" : e.Contenuto3)
                    }).ToArray();



        }


        private PublisherItem[] GetFeedItemsDbRaw(AppCms.Configurations.Module module)
        {

         
            CustomHttpUtility http = new CustomHttpUtility();
            var urlFolder = Configurations.Configuration.GetImagePath(module.ZeusId, ImageRaider.RaiderIndex.Image1);
            var defaultImage = Configurations.Configuration.GetDefaultImagePath(module.ZeusId, ImageRaider.RaiderIndex.Image1);
            var urlFolder2 = Configurations.Configuration.GetImagePath(module.ZeusId, ImageRaider.RaiderIndex.Image2);
            var defaultImage2 = Configurations.Configuration.GetDefaultImagePath(module.ZeusId, ImageRaider.RaiderIndex.Image2);
            var domain = Configurations.Configuration.Domain;

            ZeusDataSet.vwPublisher_Public_LstArea1DataTable dt = new ZeusDataSet.vwPublisher_Public_LstArea1DataTable();
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(strConnessione))
            {
                var query = @"SELECT        ID1Publisher, ID2Categoria1, Titolo, CatLiv2Liv3, PWI_Area1, PWF_Area1, PWI_Area2, PWF_Area2, Archivio, ZeusIdModulo, ZeusLangCode, PWI_Area3, PWF_Area3, Immagine1, DescBreve1, Immagine2, 
                                            ZeusJolly2, UrlRewrite, Sottotitolo, Fonte_Url, Contenuto3
                                FROM            vwPublisher_Public_LstArea1
                                WHERE        (ZeusIdModulo = @ZeusIdModulo) AND (ZeusLangCode = @ZeusLangCode)";

                SqlDataAdapter dataAdpter = new SqlDataAdapter(query, conn);
                dataAdpter.SelectCommand.Parameters.AddWithValue("@ZeusIdModulo", module.ZeusId);
                dataAdpter.SelectCommand.Parameters.AddWithValue("@ZeusLangCode", "ITA");
                
                dataAdpter.Fill(dt);
            }

            return (from e in dt.AsEnumerable()
                    select new PublisherItem()
                    {
                        id = e.ID1Publisher,
                        body = (e.IsDescBreve1Null() ? null : e.DescBreve1.EncodeString().RemoveLineBreak()),
                        image = (e.IsImmagine1Null() || string.IsNullOrWhiteSpace(e.Immagine1) || string.IsNullOrWhiteSpace(urlFolder) ? null : (http.CheckFileExist(string.Concat(urlFolder, e.Immagine1)) ? string.Concat(urlFolder, e.Immagine1) : defaultImage)),
                        image2 = (e.IsImmagine2Null() || string.IsNullOrWhiteSpace(e.Immagine2) || string.IsNullOrWhiteSpace(urlFolder2) ? null : (http.CheckFileExist(string.Concat(urlFolder2, e.Immagine2)) ? string.Concat(urlFolder2, e.Immagine2) : defaultImage2)),
                        title = e.Titolo,
                        jolly = (e.IsZeusJolly2Null() ? "" : e.ZeusJolly2),
                        share = (e.IsUrlRewriteNull() ? "" : string.Format("https://{0}/News/{1}/{2}", domain, e.ID1Publisher, e.UrlRewrite)),
                        exturl = (e.IsFonte_UrlNull() ? "" : e.Fonte_Url),
                        subtitle = (e.IsSottotitoloNull() ? "" : e.Sottotitolo),
                        date = e.PWI_Area1,
                        date2 = e.PWI_Area2,
                        html = (e.IsContenuto3Null() ? "" : e.Contenuto3)
                    }).ToArray();



        }


        [HttpPut]
        public IHttpActionResult Put(PutElementRequest model)
        {


            IEnumerable<string> headerValues = Request.Headers.GetValues("X-SwitchUp");
            if (headerValues.FirstOrDefault() != "00-225455-452114")
                return BadRequest();

            if (ModelState.IsValid)
            {
                AppCms.Configurations.Module module = GetCurrentModule();
                Publisher pd = new Publisher();
                pd.PostMessage(module.ZeusId, model.title.RemoveHtml(), model.content.RemoveHtml());
                return Ok("Successfully posted!");
            }
            else
                return BadRequest();

        }

        private AppCms.Configurations.Module GetCurrentModule()
        {
            var moduleRoute = Request.RequestUri.Segments[2].ToString();
            moduleRoute = moduleRoute.Remove(moduleRoute.Length - 1);
            AppCms.Configurations.Module module = Configurations.Configuration.PublisherModules.FirstOrDefault(f => f.Route == moduleRoute);
            return module;

        }

    }
}