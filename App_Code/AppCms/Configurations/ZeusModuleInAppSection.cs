﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ZeusModuleInAppSection
/// </summary>
/// 
namespace AppCms.Configurations
{
    public class ZeusModuleInAppSection : ConfigurationSection
    {

        //[ConfigurationProperty("ZeusIdModuleNews", DefaultValue = "", IsRequired = true)]
        //public string ZeusIdModuleNews
        //{
        //    get
        //    {
        //        return (string)this["ZeusIdModuleNews"];
        //    }
        //    set
        //    {
        //        this["ZeusIdModuleNews"] = value;
        //    }
        //}

        //[ConfigurationProperty("ZeusIdModuleGalleries", DefaultValue = "", IsRequired = true)]
        //public string ZeusIdModuleGalleries
        //{
        //    get
        //    {
        //        return (string)this["ZeusIdModuleGalleries"];
        //    }
        //    set
        //    {
        //        this["ZeusIdModuleGalleries"] = value;
        //    }
        //}

        //[ConfigurationProperty("ZeusIdModuleLogos", DefaultValue = "", IsRequired = true)]
        //public string ZeusIdModuleLogos
        //{
        //    get
        //    {
        //        return (string)this["ZeusIdModuleLogos"];
        //    }
        //    set
        //    {
        //        this["ZeusIdModuleLogos"] = value;
        //    }
        //}

        //[ConfigurationProperty("ZeusIdModuleSpeakers", DefaultValue = "", IsRequired = true)]
        //public string ZeusIdModuleSpeakers
        //{
        //    get
        //    {
        //        return (string)this["ZeusIdModuleSpeakers"];
        //    }
        //    set
        //    {
        //        this["ZeusIdModuleSpeakers"] = value;
        //    }
        //}

        //[ConfigurationProperty("ZeusIdModulePageDesigner", DefaultValue = "", IsRequired = true)]
        //public string ZeusIdModulePageDesigner
        //{
        //    get
        //    {
        //        return (string)this["ZeusIdModulePageDesigner"];
        //    }
        //    set
        //    {
        //        this["ZeusIdModulePageDesigner"] = value;
        //    }
        //}

        [ConfigurationProperty("ZeusDomain", DefaultValue = "cms.switchup.it", IsRequired = true)]
        public string ZeusDomain
        {
            get
            {
                return (string)this["ZeusDomain"];
            }
            set
            {
                this["ZeusDomain"] = value;
            }
        }


        [ConfigurationProperty("modules", IsRequired = false, IsKey = false, IsDefaultCollection = true)]
        public ModuleCollection Modules
        {
            get { return ((ModuleCollection)(base["modules"])); }
            set { base["modules"] = value; }
        }

    }

    [ConfigurationCollection(typeof(Module), CollectionType = ConfigurationElementCollectionType.BasicMapAlternate)]
    public class ModuleCollection : ConfigurationElementCollection
    {
        internal const string ModulePropertyRoute = "module";

        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.BasicMapAlternate; }
        }

        protected override string ElementName
        {
            get { return ModulePropertyRoute; }
        }

        protected override bool IsElementName(string elementName)
        {
            return (elementName == ModulePropertyRoute);
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((Module)element).Route;
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new Module();
        }

        public override bool IsReadOnly()
        {
            return false;
        }

    }

    public class Module : ConfigurationElement
    {
        [ConfigurationProperty("route")]
        public string Route
        {
            get { return (string)base["route"]; }
            set { base["route"] = value; }
        }

        [ConfigurationProperty("type")]
        public string Type
        {
            get { return (string)base["type"]; }
            set { base["type"] = value; }
        }

        [ConfigurationProperty("zeusid")]
        public string ZeusId
        {
            get { return (string)base["zeusid"]; }
            set { base["zeusid"] = value; }
        }

    }
}
