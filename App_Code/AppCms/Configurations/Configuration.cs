﻿using AppCms.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Configuration
/// </summary>
/// 
namespace AppCms.Configurations
{
    public static class Configuration
    {
        public static string ZeusConnectionString
        {
            get
            {
                return System.Configuration.ConfigurationManager.
                       ConnectionStrings["ZeusConnectionString"].ConnectionString;
            }
        }


        private static ModuleCollection ModuleCollection
        {
            get {
                return GetZeusModuleInAppSection().Modules;
            
            }
        
        }





        public static Module[] PageDesignerModules
        {
            get {

                var modules = (from Module m in ModuleCollection
                               where m.Type == "PD"
                               select m).ToArray();
                return modules;
            
            }
        
        }

        public static Module[] PublisherModules
        {
            get {
                var modules = (from Module m in ModuleCollection
                               where m.Type == "PU"
                               select m).ToArray();
                return modules;
            
            
            }
        
        
        }


        public static string Domain
        {
            get { return GetZeusModuleInAppSection().ZeusDomain; }
        
        }


        //public static string ZeusIdModuleNews
        //{
        //    get { return GetZeusModuleInAppSection().ZeusIdModuleNews; }

        //}

        //public static string ZeusIdModuleGalleries
        //{
        //    get { return GetZeusModuleInAppSection().ZeusIdModuleGalleries; }

        //}

        //public static string ZeusIdModuleSpeakers
        //{
        //    get { return GetZeusModuleInAppSection().ZeusIdModuleSpeakers; }

        //}

        //public static string ZeusIdModuleLogos
        //{
        //    get { return GetZeusModuleInAppSection().ZeusIdModuleLogos; }

        //}

        //public static string ZeusIdModulePageDesigner
        //{
        //    get { return GetZeusModuleInAppSection().ZeusIdModulePageDesigner; }

        //}


        //#region Speakers

        public static string GetImagePath(string ZeusIdModule, ImageRaider.RaiderIndex RaiderIndex)
        {
            ImageRaider raider = new ImageRaider();
            var relativePath = raider.GetRelativePath(RaiderIndex, ZeusIdModule);
            if (!string.IsNullOrWhiteSpace(relativePath))
                return relativePath.Replace("~", string.Concat("http://", GetZeusModuleInAppSection().ZeusDomain));
            else
                return null;
        }
        public static string GetDefaultImagePath(string ZeusIdModule, ImageRaider.RaiderIndex RaiderIndex)
        {
            ImageRaider raider = new ImageRaider();
            var relativeUrl = raider.GetRelativeUrlDefaultImage(RaiderIndex, ZeusIdModule);
            if (!string.IsNullOrWhiteSpace(relativeUrl))
                return relativeUrl.Replace("~", string.Concat("http://", GetZeusModuleInAppSection().ZeusDomain));
            return null;
        }

        //public static string GetSpeakersImage2Path()
        //{
        //    ImageRaider raider = new ImageRaider();
        //    var relativePath = raider.GetRelativePath(ImageRaider.RaiderIndex.SpeakersImage2);
        //    if (!string.IsNullOrWhiteSpace(relativePath))
        //        return relativePath.Replace("~", string.Concat("http://", GetZeusModuleInAppSection().ZeusDomain));
        //    else
        //        return null;
        //}
        //public static string GetDefaultSpeakersImage2Path()
        //{
        //    ImageRaider raider = new ImageRaider();
        //    var relativeUrl = raider.GetRelativeUrlDefaultImage(ImageRaider.RaiderIndex.SpeakersImage2);
        //    if (!string.IsNullOrWhiteSpace(relativeUrl))
        //        return relativeUrl.Replace("~", string.Concat("http://", GetZeusModuleInAppSection().ZeusDomain));
        //    return null;
        //}

        //#endregion

        //#region News
        //public static string GetNewsImage1Path()
        //{
        //    ImageRaider raider = new ImageRaider();
        //    var relativePath = raider.GetRelativePath(ImageRaider.RaiderIndex.NewsImage1);
        //    if (!string.IsNullOrWhiteSpace(relativePath))
        //        return relativePath.Replace("~", string.Concat("http://", GetZeusModuleInAppSection().ZeusDomain));
        //    else
        //        return null;
        //}
        //public static string GetDefaultNewsImage1Path()
        //{
        //    ImageRaider raider = new ImageRaider();
        //    var relativeUrl = raider.GetRelativeUrlDefaultImage(ImageRaider.RaiderIndex.NewsImage1);
        //    if (!string.IsNullOrWhiteSpace(relativeUrl))
        //        return relativeUrl.Replace("~", string.Concat("http://", GetZeusModuleInAppSection().ZeusDomain));
        //    return null;
        //}
        //public static string GetNewsImage2Path()
        //{
        //    ImageRaider raider = new ImageRaider();
        //    var relativePath = raider.GetRelativePath(ImageRaider.RaiderIndex.NewsImage2);
        //    if (!string.IsNullOrWhiteSpace(relativePath))
        //        return relativePath.Replace("~", string.Concat("http://", GetZeusModuleInAppSection().ZeusDomain));
        //    else
        //        return null;
        //}
        //public static string GetDefaultNewsImage2Path()
        //{
        //    ImageRaider raider = new ImageRaider();
        //    var relativeUrl = raider.GetRelativeUrlDefaultImage(ImageRaider.RaiderIndex.NewsImage2);
        //    if (!string.IsNullOrWhiteSpace(relativeUrl))
        //        return relativeUrl.Replace("~", string.Concat("http://", GetZeusModuleInAppSection().ZeusDomain));
        //    return null;
        //}
        //#endregion

        //#region Galleries
        //public static string GetGalleriesImage1Path()
        //{
        //    ImageRaider raider = new ImageRaider();
        //    var relativePath = raider.GetRelativePath(ImageRaider.RaiderIndex.GalleriesImage1);
        //    if (!string.IsNullOrWhiteSpace(relativePath))
        //        return relativePath.Replace("~", string.Concat("http://", GetZeusModuleInAppSection().ZeusDomain));
        //    else
        //        return null;
        //}
        //public static string GetDefaultGalleriesImage1Path()
        //{
        //    ImageRaider raider = new ImageRaider();
        //    var relativeUrl = raider.GetRelativeUrlDefaultImage(ImageRaider.RaiderIndex.GalleriesImage1);
        //    if (!string.IsNullOrWhiteSpace(relativeUrl))
        //        return relativeUrl.Replace("~", string.Concat("http://", GetZeusModuleInAppSection().ZeusDomain));
        //    return null;
        //}
        //public static string GetGalleriesImage2Path()
        //{
        //    ImageRaider raider = new ImageRaider();
        //    var relativePath = raider.GetRelativePath(ImageRaider.RaiderIndex.GalleriesImage2);
        //    if (!string.IsNullOrWhiteSpace(relativePath))
        //        return relativePath.Replace("~", string.Concat("http://", GetZeusModuleInAppSection().ZeusDomain));
        //    else
        //        return null;
        //}
        //public static string GetDefaultGalleriesImage2Path()
        //{
        //    ImageRaider raider = new ImageRaider();
        //    var relativeUrl = raider.GetRelativeUrlDefaultImage(ImageRaider.RaiderIndex.GalleriesImage2);
        //    if (!string.IsNullOrWhiteSpace(relativeUrl))
        //        return relativeUrl.Replace("~", string.Concat("http://", GetZeusModuleInAppSection().ZeusDomain));
        //    return null;
        //}
        //#endregion

        //#region Logos
        //public static string GetLogosImage1Path()
        //{
        //    ImageRaider raider = new ImageRaider();
        //    var relativePath = raider.GetRelativePath(ImageRaider.RaiderIndex.LogosImage1);
        //    if (!string.IsNullOrWhiteSpace(relativePath))
        //        return relativePath.Replace("~", string.Concat("http://", GetZeusModuleInAppSection().ZeusDomain));
        //    else
        //        return null;
        //}
        //public static string GetDefaultLogosImage1Path()
        //{
        //    ImageRaider raider = new ImageRaider();
        //    var relativeUrl = raider.GetRelativeUrlDefaultImage(ImageRaider.RaiderIndex.LogosImage1);
        //    if (!string.IsNullOrWhiteSpace(relativeUrl))
        //        return relativeUrl.Replace("~", string.Concat("http://", GetZeusModuleInAppSection().ZeusDomain));
        //    return null;
        //}
        //#endregion



        //#region Pages
        //public static string GetPageDesignerImage1Path()
        //{
        //    ImageRaider raider = new ImageRaider();
        //    var relativePath = raider.GetRelativePath(ImageRaider.RaiderIndex.PageDesignerImage1);
        //    if (!string.IsNullOrWhiteSpace(relativePath))
        //        return relativePath.Replace("~", string.Concat("http://", GetZeusModuleInAppSection().ZeusDomain));
        //    else
        //        return null;
        //}
        //public static string GetDefaultPageDesignerImage1Path()
        //{
        //    ImageRaider raider = new ImageRaider();
        //    var relativeUrl = raider.GetRelativeUrlDefaultImage(ImageRaider.RaiderIndex.PageDesignerImage1);
        //    if (!string.IsNullOrWhiteSpace(relativeUrl))
        //        return relativeUrl.Replace("~", string.Concat("http://", GetZeusModuleInAppSection().ZeusDomain));
        //    return null;
        //}
        //public static string GetPageDesignerImage2Path()
        //{
        //    ImageRaider raider = new ImageRaider();
        //    var relativePath = raider.GetRelativePath(ImageRaider.RaiderIndex.PageDesignerImage2);
        //    if (!string.IsNullOrWhiteSpace(relativePath))
        //        return relativePath.Replace("~", string.Concat("http://", GetZeusModuleInAppSection().ZeusDomain));
        //    else
        //        return null;
        //}
        //public static string GetDefaultPageDesignerImage2Path()
        //{
        //    ImageRaider raider = new ImageRaider();
        //    var relativeUrl = raider.GetRelativeUrlDefaultImage(ImageRaider.RaiderIndex.PageDesignerImage2);
        //    if (!string.IsNullOrWhiteSpace(relativeUrl))
        //        return relativeUrl.Replace("~", string.Concat("http://", GetZeusModuleInAppSection().ZeusDomain));
        //    return null;
        //}
        //#endregion


        private static ZeusModuleInAppSection GetZeusModuleInAppSection()
        {
            return (ZeusModuleInAppSection)System.Configuration.ConfigurationManager.GetSection("ZeusModuleInAppGroup/ZeusModuleInApp");
        }

    }

}
