﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PushSharp;
using PushSharp.Android;
using PushSharp.Core;
using System.IO;
using PushSharp.Apple;

/// <summary>
/// Summary description for NotificationService
/// </summary>
/// 
namespace AppCms.Notifications
{
    public class NotificationService
    {

        public void Send(string deviceId, string type, string Title, string Message)
        {
            var push = new PushBroker();
            //Wire up the events for all the services that the broker registers
            push.OnNotificationSent += NotificationSent;
            push.OnChannelException += ChannelException;
            push.OnServiceException += ServiceException;
            push.OnNotificationFailed += NotificationFailed;
            push.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
            push.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;
            push.OnChannelCreated += ChannelCreated;
            push.OnChannelDestroyed += ChannelDestroyed;
            List<DeviceItem> rows = new List<DeviceItem>() { new DeviceItem() { type = type, deviceId = deviceId, lastSeen = DateTime.Now } };
            foreach (var r in rows)
            {
                if (r.type == "AND")
                {
                    //---------------------------
                    // ANDROID GCM NOTIFICATIONS
                    //---------------------------
                    //Configure and start Android GCM
                    //IMPORTANT: The API KEY comes from your Google APIs Console App, 
                    //under the API Access section, 
                    //  by choosing 'Create new Server key...'
                    //  You must ensure the 'Google Cloud Messaging for Android' service is 
                    //enabled in your APIs Console
                    push.RegisterGcmService(new
                     GcmPushChannelSettings("AIzaSyDrYezSuJjPk5tomUU7pXPe9lvArlmhzL8"));
                    //Fluent construction of an Android GCM Notification
                    //IMPORTANT: For Android you MUST use your own RegistrationId 
                    //here that gets generated within your Android app itself!


                    push.QueueNotification(new GcmNotification().ForDeviceRegistrationId(r.deviceId).WithJson("{'title':'" + Title + "','message':'" + Message + "'}"));

                }
                else if (r.type == "IOS")
                {
                    //-------------------------
                    // APPLE NOTIFICATIONS
                    //-------------------------
                    //Configure and start Apple APNS
                    // IMPORTANT: Make sure you use the right Push certificate.  Apple allows you to
                    //generate one for connecting to Sandbox, and one for connecting to Production.  You must
                    // use the right one, to match the provisioning profile you build your
                    //   app with!
                    try
                    {

                        var appleCert = File.ReadAllBytes(HttpContext.Current.Server.MapPath("/Resources/Dev_Cert_AppCms.p12"));
                        //IMPORTANT: If you are using a Development provisioning Profile, you must use
                        // the Sandbox push notification server 
                        //  (so you would leave the first arg in the ctor of ApplePushChannelSettings as
                        // 'false')
                        //  If you are using an AdHoc or AppStore provisioning profile, you must use the 
                        //Production push notification server
                        //  (so you would change the first arg in the ctor of ApplePushChannelSettings to 
                        //'true')
                        push.RegisterAppleService(new ApplePushChannelSettings(false, appleCert, ""));
                        //Extension method
                        //Fluent construction of an iOS notification
                        //IMPORTANT: For iOS you MUST MUST MUST use your own DeviceToken here that gets
                        // generated within your iOS app itself when the Application Delegate
                        //  for registered for remote notifications is called, 
                        // and the device token is passed back to you
                        push.QueueNotification(new AppleNotification()
                                                    .ForDeviceToken(r.deviceId)//the recipient device id
                                                    .WithAlert(Message)//the message
                                                    );

                        //.WithBadge(1)


                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
            push.StopAllServices(waitForQueuesToFinish: true);
        }




        //Currently it will raise only for android devices
        static void DeviceSubscriptionChanged(object sender,
        string oldSubscriptionId, string newSubscriptionId, INotification notification)
        {
            //Do something here
        }

        //this even raised when a notification is successfully sent
        static void NotificationSent(object sender, INotification notification)
        {
            //Do something here

        }

        //this is raised when a notification is failed due to some reason
        static void NotificationFailed(object sender,
        INotification notification, Exception notificationFailureException)
        {
            //Do something here
        }

        //this is fired when there is exception is raised by the channel
        static void ChannelException
            (object sender, IPushChannel channel, Exception exception)
        {
            //Do something here
        }

        //this is fired when there is exception is raised by the service
        static void ServiceException(object sender, Exception exception)
        {
            //Do something here
        }

        //this is raised when the particular device subscription is expired
        static void DeviceSubscriptionExpired(object sender,
        string expiredDeviceSubscriptionId,
            DateTime timestamp, INotification notification)
        {
            //Do something here
        }

        //this is raised when the channel is destroyed
        static void ChannelDestroyed(object sender)
        {
            //Do something here
        }

        //this is raised when the channel is created
        static void ChannelCreated(object sender, IPushChannel pushChannel)
        {
            //Do something here
        }
    }


    public class DeviceItem
    {
        public string deviceId { get; set; }
        public string type { get; set; }
        public DateTime lastSeen { get; set; }
    }
}
