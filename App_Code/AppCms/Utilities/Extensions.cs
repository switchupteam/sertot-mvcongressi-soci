﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

/// <summary>
/// Summary description for Extensions
/// </summary>
/// 
namespace AppCms.Extensions
{
    public static class Extensions
    {
        public static string RemoveLineBreak(this string value)
        {
            return value.Replace(System.Environment.NewLine, "<br />");
        }

        public static string EncodeString(this string value)
        {
            return HttpUtility.HtmlEncode(value);
        }

        public static string RemoveHtml(this string value)
        {
            return HtmlRemoval.StripTagsRegexCompiled(value);
        
        }

    }

    public static class HtmlRemoval
    {
        public static string StripTagsRegex(string source)
        {
            return Regex.Replace(source, "<.*?>", string.Empty);
        }

        static Regex _htmlRegex = new Regex("<.*?>", RegexOptions.Compiled);

        public static string StripTagsRegexCompiled(string source)
        {
            return _htmlRegex.Replace(source, string.Empty);
        }
        public static string StripTagsCharArray(string source)
        {
            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < source.Length; i++)
            {
                char let = source[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }
    }
}