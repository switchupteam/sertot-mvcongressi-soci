﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Xml.Linq;

/// <summary>
/// Summary description for Communicator
/// </summary>
public class Communicator
{
    private string OraInizio = string.Empty;
    private string OraFine = string.Empty;
    private string DataInizio = string.Empty;
    private string DataFine = string.Empty;
    private string AgentStatus = string.Empty;
    private string LastRunUpdate = string.Empty;
    private string OpSpreadTime = string.Empty;
    private string PZD_MailPerLotto = "5";
    private string ZeusIdModulo = "COMNL";

    private bool DEBUG_MODE = false;

    private bool MANUAL_MODE = false;

	public Communicator()
	{
		
    }//fine Communicator

    public Communicator(string myZeusIdModulo)
    {
        ZeusIdModulo = myZeusIdModulo;

    }//fine Communicator

    public Communicator(string myZeusIdModulo, bool myDEBUG_MODE)
    {
        ZeusIdModulo = myZeusIdModulo;
        DEBUG_MODE = myDEBUG_MODE;

    }//fine Communicator

    public Communicator(string myZeusIdModulo,bool myDEBUG_MODE, bool myMANUAL_MODE)
    {
        ZeusIdModulo = myZeusIdModulo;
        DEBUG_MODE = myDEBUG_MODE;
        MANUAL_MODE = myMANUAL_MODE;

    }//fine Communicator





    private bool SendMailDebug(string From, string To, string Body)
    {
        try
        {

            Zeus.Mail myMail = new Zeus.Mail();
            MailMessage mail = new MailMessage();


            myMail.MailToSend.To.Add(new MailAddress(To));
            myMail.MailToSend.From = new MailAddress(From);
            myMail.MailToSend.Subject = "DEBUG";
            myMail.MailToSend.IsBodyHtml = true;
            myMail.MailToSend.Body = Body;

            //SmtpClient client = new SmtpClient();
            //client.Send(mail);

            myMail.SendMail(Zeus.Mail.MailSendMode.Sync);
            return true;
        }
        catch (Exception)
        {
            // System.Web.HttpContext.Current.Response.Write(p.ToString());
            return false;
        }

    }//fine SendMail



    private bool GetAllConfigurationData()
    {
        
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        try
        {

            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();


                sqlQuery = "SET DATEFORMAT dmy; SELECT [OraInizio],[OraFine],[DataInizio],[DataFine]";
                sqlQuery += " ,[AgentStatus],[LastRunUpdate],[OpSpreadTime]";
                sqlQuery += " FROM [tbAgents_Dashboard]";
                sqlQuery += " WHERE [Agent]='COMMUNICATOR'";
                
                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (reader["OraInizio"] != DBNull.Value)
                        OraInizio = reader["OraInizio"].ToString();

                    if (reader["OraFine"] != DBNull.Value)
                        OraFine = reader["OraFine"].ToString();

                    if (reader["DataInizio"] != DBNull.Value)
                        DataInizio = Convert.ToDateTime( reader["DataInizio"]).ToString("d");

                    if (reader["DataFine"] != DBNull.Value)
                        DataFine = Convert.ToDateTime(reader["DataFine"]).ToString("d");

                    if (reader["AgentStatus"] != DBNull.Value)
                        AgentStatus = reader["AgentStatus"].ToString();

                    if (reader["LastRunUpdate"] != DBNull.Value)
                        LastRunUpdate =Convert.ToDateTime( reader["LastRunUpdate"]).ToString();

                    if (reader["OpSpreadTime"] != DBNull.Value)
                        OpSpreadTime = Convert.ToInt32(reader["OpSpreadTime"]).ToString();

                }//fine while

                reader.Close();

                //sqlQuery = "SET DATEFORMAT dmy; SELECT [PZD_MailPerLotto]";
                //sqlQuery += " FROM [tbPZ_Communicator]";
                //sqlQuery += " WHERE [ZeusIdModulo]='"+ZeusIdModulo+"'";

                //if ((DEBUG_MODE) && (MANUAL_MODE))
                // System.Web.HttpContext.Current.Response.Write("DEB " + sqlQuery+"<br />");

                //command.CommandText = sqlQuery;

                //reader = command.ExecuteReader();

                //while (reader.Read())
                //    if (reader["PZD_MailPerLotto"] != DBNull.Value)
                //        PZD_MailPerLotto =Convert.ToInt32(reader["PZD_MailPerLotto"]).ToString();

                //reader.Close();

                return true;

            }//fine Using
        }
        catch (Exception p)
        {

            if (DEBUG_MODE)
            {
                if (!MANUAL_MODE)
                    SendMailDebug("debug@sport-news.it", "debug@sport-news.it", p.ToString());
                else
                 System.Web.HttpContext.Current.Response.Write(p.ToString());
            }
            return false;
        }

    }//fine GetAllConfigurationData



    private bool CheckWorkTime()
    {

        if (OraInizio.Equals(OraFine))
            return true;

        bool IsInTime = false;

        try
        {

            DateTime StartDate = DateTime.Now;
            DateTime EndDate = DateTime.Now;

            if (Convert.ToInt32(OraInizio.Replace(":", string.Empty)) < Convert.ToInt32(OraFine.Replace(":", string.Empty)))
            {
                 StartDate = Convert.ToDateTime(DateTime.Now.ToString("d") + " " + OraInizio);
                 EndDate = Convert.ToDateTime(DateTime.Now.ToString("d") + " " + OraFine);
            }
            else
            {
                 StartDate = Convert.ToDateTime(DateTime.Now.ToString("d") + " " + OraInizio);
                 EndDate = Convert.ToDateTime(DateTime.Now.AddDays(1).ToString("d") + " " + OraFine);
            }

            long dateStartDiffVal = 0;
            long dateEndDiffVal = 0;

            TimeSpan ts1 = new TimeSpan(DateTime.Now.Ticks - StartDate.Ticks);
            TimeSpan ts2 = new TimeSpan(EndDate.Ticks - DateTime.Now.Ticks);

            dateStartDiffVal = (long)ts1.TotalMinutes;
            dateEndDiffVal = (long)ts2.TotalMinutes;

            if ((DEBUG_MODE) && (MANUAL_MODE))
              System.Web.HttpContext.Current.Response.Write("DEB dateStartDiffVal:" + dateStartDiffVal + " - dateEndDiffVal:" + dateEndDiffVal + "<br />");

            if ((dateStartDiffVal >= 0)
                && (dateEndDiffVal >= 0))
                IsInTime = true;

            if (IsInTime)
            {
                IsInTime = false;

                StartDate = Convert.ToDateTime(DataInizio);
                EndDate = Convert.ToDateTime(DataFine);

                dateStartDiffVal = 0;
                dateEndDiffVal = 0;

                ts1 = new TimeSpan(DateTime.Now.Ticks - StartDate.Ticks);
                ts2 = new TimeSpan(EndDate.Ticks - DateTime.Now.Ticks);

                dateStartDiffVal = (long)ts1.TotalDays;
                dateEndDiffVal = (long)ts2.TotalDays;

                if ((DEBUG_MODE) && (MANUAL_MODE))
                  System.Web.HttpContext.Current.Response.Write("DEB dateStartDiffVal:" + dateStartDiffVal + " - dateEndDiffVal:" + dateEndDiffVal + "<br />");

                if ((dateStartDiffVal >= 0)
                    && (dateEndDiffVal >= 0))
                    IsInTime = true;
                
            }

        }
        catch (Exception p)
        {

            if (DEBUG_MODE)
            {
                if (!MANUAL_MODE)
                    SendMailDebug("debug@sport-news.it", "debug@sport-news.it", p.ToString());
                else
                    System.Web.HttpContext.Current.Response.Write(p.ToString());
            }
            return false;
        }

        return IsInTime;
    }//fine CheckWorkTime


    private bool CheckOpSpreadTime()
    {
        try
        {
            if ((DEBUG_MODE) && (MANUAL_MODE))
             System.Web.HttpContext.Current.Response.Write("DEB " + LastRunUpdate+"<br />");


            TimeSpan ts1 = new TimeSpan(DateTime.Now.Ticks - Convert.ToDateTime(LastRunUpdate).Ticks);

            if (Convert.ToInt32(OpSpreadTime) > (int)ts1.TotalMinutes)
                return true;


        }
        catch (Exception p)
        {
            if (DEBUG_MODE)
            {
                if (!MANUAL_MODE)
                    SendMailDebug("debug@sport-news.it", "debug@sport-news.it", p.ToString());
                else
                    System.Web.HttpContext.Current.Response.Write(p.ToString());
            }
        }

        return false;

    }//fine CheckOpSpreadTime


    private bool UpdateLastRunStart()
    {

        if ((DEBUG_MODE) && (MANUAL_MODE))
           System.Web.HttpContext.Current.Response.Write("DEB UpdateLastRunStart()<br />");

        try
        {

            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(
               System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SET DATEFORMAT dmy; UPDATE [tbAgents_Dashboard] ";
                sqlQuery += " SET LastRunStart=@LastRunStart";
                sqlQuery += " WHERE [Agent]='COMMUNICATOR'";
                command.Parameters.Add("@LastRunStart", System.Data.SqlDbType.SmallDateTime);
                command.Parameters["@LastRunStart"].Value = DateTime.Now;
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();


            }//fine Using

            return true;
        }
        catch (Exception p)
        {
            if (DEBUG_MODE)
            {
                if (!MANUAL_MODE)
                    SendMailDebug("debug@sport-news.it", "debug@sport-news.it", p.ToString());
                else
                    System.Web.HttpContext.Current.Response.Write(p.ToString());
            }
            return false;
        }
    }//fine UpdateLastRunStart


    private bool UpdateLastRunEnd()
    {
        if ((DEBUG_MODE) && (MANUAL_MODE))
         System.Web.HttpContext.Current.Response.Write("DEB UpdateLastRunEnd()<br />");

        try
        {

            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(
               System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SET DATEFORMAT dmy; UPDATE [tbAgents_Dashboard] ";
                sqlQuery += " SET LastRunEnd=@LastRunEnd";
                sqlQuery += " WHERE [Agent]='COMMUNICATOR'";
                command.Parameters.Add("@LastRunEnd", System.Data.SqlDbType.SmallDateTime);
                command.Parameters["@LastRunEnd"].Value = DateTime.Now;
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();


            }//fine Using

            return true;
        }
        catch (Exception p)
        {
            if (DEBUG_MODE)
            {
                if (!MANUAL_MODE)
                    SendMailDebug("debug@sport-news.it", "debug@sport-news.it", p.ToString());
                else
                    System.Web.HttpContext.Current.Response.Write(p.ToString());
            }
            return false;
        }
    }//fine UpdateLastRunEnd


    private bool UpdateLastRunUpdate()
    {
        if ((DEBUG_MODE) && (MANUAL_MODE))
         System.Web.HttpContext.Current.Response.Write("DEB UpdateLastRunUpdate()<br />");

        try
        {

            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(
               System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SET DATEFORMAT dmy; UPDATE [tbAgents_Dashboard] ";
                sqlQuery += " SET LastRunUpdate=@LastRunUpdate";
                sqlQuery += " WHERE [Agent]='COMMUNICATOR'";
                command.Parameters.Add("@LastRunUpdate", System.Data.SqlDbType.SmallDateTime);
                command.Parameters["@LastRunUpdate"].Value = DateTime.Now;
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();


            }//fine Using

            return true;
        }
        catch (Exception p)
        {
            if (DEBUG_MODE)
            {
                if (!MANUAL_MODE)
                    SendMailDebug("debug@sport-news.it", "debug@sport-news.it", p.ToString());
                else
                    System.Web.HttpContext.Current.Response.Write(p.ToString());
            }
            return false;
        }
    }//fine UpdateLastRunUpdate



    private bool UpdateStatus(string STATUS)
    {
        if ((DEBUG_MODE) && (MANUAL_MODE))
         System.Web.HttpContext.Current.Response.Write("DEB UpdateStatus()<br />");

        try
        {

            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(
               System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SET DATEFORMAT dmy; UPDATE [tbAgents_Dashboard] ";
                sqlQuery += " SET AgentStatus=@AgentStatus";
                sqlQuery += " WHERE [Agent]='COMMUNICATOR'";
                command.Parameters.Add("@AgentStatus", System.Data.SqlDbType.NVarChar);
                command.Parameters["@AgentStatus"].Value = STATUS;
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();


            }//fine Using

            return true;
        }
        catch (Exception p)
        {

            if (DEBUG_MODE)
            {
                if (!MANUAL_MODE)
                    SendMailDebug("debug@sport-news.it", "debug@sport-news.it", p.ToString());
                else
                    System.Web.HttpContext.Current.Response.Write(p.ToString());
            }
            return false;
        }
    }//fine UpdateLastRunStart

    private bool IsThereMoreWorks()
    {

        if ((DEBUG_MODE) && (MANUAL_MODE))
            System.Web.HttpContext.Current.Response.Write("DEB IsThereMoreWorks()<br />");


        bool MoreWorks = false;

        try
        {

            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(
               System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SET DATEFORMAT dmy; SELECT Count([ID1Recipient])";
                sqlQuery += " FROM [tbCommunicator_TempRecipients] ";
                sqlQuery += " WHERE [SendType]='SCHED'";
                sqlQuery += " AND DATEDIFF(minute, SendMail_StartDate, GETDATE())>=0";
                command.CommandText = sqlQuery;

                if((DEBUG_MODE)&&(MANUAL_MODE))
                  System.Web.HttpContext.Current.Response.Write("DEB "+sqlQuery+"<br />");

                int HowMuchWork = (int)command.ExecuteScalar();

                if (HowMuchWork > 0)
                    MoreWorks = true;

            }//fine Using
           
        }
        catch (Exception p)
        {

            if (DEBUG_MODE)
            {
                if (!MANUAL_MODE)
                    SendMailDebug("debug@sport-news.it", "debug@sport-news.it", p.ToString());
                else
                    System.Web.HttpContext.Current.Response.Write(p.ToString());
            }
           
        }

        return MoreWorks;
    
    }//fine IsThereMoreWorks






    private bool CloseTicketLog()
    {
        if ((DEBUG_MODE) && (MANUAL_MODE))
          System.Web.HttpContext.Current.Response.Write("DEB CloseTicketLog()<br />");

        SqlTransaction transaction = null;
        SqlDataReader reader = null;

        try
        {

            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(
               System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                transaction = connection.BeginTransaction();
                command.Transaction = transaction;



                sqlQuery = "SELECT ID1LogMessage";
                sqlQuery += " FROM tbCommunicator_LogMessage";
                sqlQuery += " WHERE Status='SND' AND SendType='SCHED'";
                command.CommandText = sqlQuery;


                reader = command.ExecuteReader();

                System.Collections.ArrayList myArray=new System.Collections.ArrayList();

                while (reader.Read())
                {
                    if (reader["ID1LogMessage"] != DBNull.Value)
                        myArray.Add(Convert.ToInt32(reader["ID1LogMessage"]).ToString());
                }//fine while

                reader.Close();


                string[] ID1LogMessage = (string[])myArray.ToArray(typeof(string));


                for (int i = 0; i < ID1LogMessage.Length; ++i)
                {
                    sqlQuery = "SELECT COUNT(ID2LogMessage)";
                    sqlQuery += " FROM tbCommunicator_TempRecipients";
                    sqlQuery += " WHERE ID2LogMessage=" + ID1LogMessage[i];
                    command.CommandText = sqlQuery;


                    if ((int)command.ExecuteScalar() <= 0)
                    {

                        command.Parameters.Clear();
                        sqlQuery = "SET DATEFORMAT dmy; UPDATE [tbCommunicator_LogMessage] ";
                        sqlQuery += " SET [SendDateEnd]=@SendDateEnd";
                        sqlQuery += " ,Status='OK'";
                        sqlQuery += " WHERE ID1LogMessage=" + ID1LogMessage[i];

                        command.Parameters.Add("@SendDateEnd", System.Data.SqlDbType.SmallDateTime);
                        command.Parameters["@SendDateEnd"].Value = DateTime.Now;

                        command.CommandText = sqlQuery;
                        command.ExecuteNonQuery();
                    }
                }
                   
                transaction.Commit();
            }//fine Using

            return true;
        }
        catch (Exception p)
        {


            

            if (DEBUG_MODE)
            {
                if (!MANUAL_MODE)
                    SendMailDebug("debug@sport-news.it", "debug@sport-news.it", p.ToString());
                else
                    System.Web.HttpContext.Current.Response.Write(p.ToString());
            }

            if (reader != null)
                reader.Close();

            if (transaction != null)
                transaction.Rollback();

            return false;
        }

    }//fine CloseTicketLog



    //private bool ErrorTicketLog(string ID1LogMessage)
    //{
    //    if ((DEBUG_MODE) && (MANUAL_MODE))
    //      System.Web.HttpContext.Current.Response.Write("DEB ErrorTicketLog()<br />");

    //    try
    //    {

    //        string sqlQuery = string.Empty;

    //        using (SqlConnection connection = new SqlConnection(
    //           System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString))
    //        {

    //            connection.Open();
    //            SqlCommand command = connection.CreateCommand();

    //            sqlQuery = "SET DATEFORMAT dmy; UPDATE [tbCommunicator_LogMessage] ";
    //            sqlQuery += " SET [SendDateEnd]=@SendDateEnd";
    //            sqlQuery += " ,Status='ERR'";
    //            sqlQuery += " WHERE ID1LogMessage=" + ID1LogMessage;

    //            command.Parameters.Add("@SendDateEnd", System.Data.SqlDbType.SmallDateTime);
    //            command.Parameters["@SendDateEnd"].Value = DateTime.Now;

    //            command.CommandText = sqlQuery;
    //            command.ExecuteNonQuery();


    //        }//fine Using

    //        return true;
    //    }
    //    catch (Exception p)
    //    {

    //        if (DEBUG_MODE)
    //        {
    //            if (!MANUAL_MODE)
    //                SendMailDebug("debug@sport-news.it", "debug@sport-news.it", p.ToString());
    //            else
    //                System.Web.HttpContext.Current.Response.Write(p.ToString());
    //        }
    //        return false;
    //    }

    //}//fine ErrorTicketLog

    //private bool ReopenTicketLog(string ID1LogMessage)
    //{
    //    if((DEBUG_MODE)&&(MANUAL_MODE))
    //      System.Web.HttpContext.Current.Response.Write("DEB ErrorTicketLog()<br />");
    //    try
    //    {

    //        string sqlQuery = string.Empty;

    //        using (SqlConnection connection = new SqlConnection(
    //           System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString))
    //        {

    //            connection.Open();
    //            SqlCommand command = connection.CreateCommand();

    //            sqlQuery = "SET DATEFORMAT dmy; UPDATE [tbCommunicator_LogMessage] ";
    //            sqlQuery += " SET [SendDateEnd]=@SendDateEnd";
    //            sqlQuery += " ,Status='SND'";
    //            sqlQuery += " WHERE ID1LogMessage=" + ID1LogMessage;

    //            command.Parameters.Add("@SendDateEnd", System.Data.SqlDbType.SmallDateTime);
    //            command.Parameters["@SendDateEnd"].Value = DateTime.Now;

    //            command.CommandText = sqlQuery;
    //            command.ExecuteNonQuery();


    //        }//fine Using

    //        return true;
    //    }
    //    catch (Exception p)
    //    {

    //        if (DEBUG_MODE)
    //        {
    //            if (!MANUAL_MODE)
    //                SendMailDebug("debug@sport-news.it", "debug@sport-news.it", p.ToString());
    //            else
    //                System.Web.HttpContext.Current.Response.Write(p.ToString());
    //        }
    //        return false;
    //    }

    //}//fine ReopenTicketLog


    private int GetInviati(string ID1LogMessage)
    {

       
        int Inviati = 0;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString))
        {

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();



                sqlQuery = "SET DATEFORMAT dmy; SELECT [DestinatariInviati]";
                sqlQuery += " FROM [tbCommunicator_LogMessage]";
                sqlQuery += " WHERE [ID1LogMessage] = " + ID1LogMessage;
                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();


                if (!reader.HasRows)
                {
                    reader.Close();
                    return 0;
                }

                while (reader.Read())
                {

                    if (!reader.IsDBNull(0))
                        Inviati = reader.GetInt32(0);

                }//fine while

                reader.Close();

            }
            catch (Exception p)
            {

                if (DEBUG_MODE)
                {
                    if (!MANUAL_MODE)
                        SendMailDebug("debug@sport-news.it", "debug@sport-news.it", p.ToString());
                    else
                        System.Web.HttpContext.Current.Response.Write(p.ToString());
                }

            }


        }//fine Using

        return Inviati;

    }//fine GetInviati


    private int GetTotali(string ID1LogMessage)
    {


        int Totali = 0;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString))
        {

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();



                sqlQuery = "SET DATEFORMAT dmy; SELECT [DestinatariTotale]";
                sqlQuery += " FROM [tbCommunicator_LogMessage]";
                sqlQuery += " WHERE [ID1LogMessage] = " + ID1LogMessage;
                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();


                if (!reader.HasRows)
                {
                    reader.Close();
                    return 0;
                }

                while (reader.Read())
                {

                    if (!reader.IsDBNull(0))
                        Totali = reader.GetInt32(0);

                }//fine while

                reader.Close();

            }
            catch (Exception p)
            {

                if (DEBUG_MODE)
                {
                    if (!MANUAL_MODE)
                        SendMailDebug("debug@sport-news.it", "debug@sport-news.it", p.ToString());
                    else
                        System.Web.HttpContext.Current.Response.Write(p.ToString());
                }

            }


        }//fine Using

        return Totali;

    }//fine GetTotali


    string RemoveBetween(string s, string begin, string end)
    {
        System.Text.RegularExpressions.Regex regex =
            new System.Text.RegularExpressions.Regex(string.Format("\\{0}.*?\\{1}", begin, end));
        return regex.Replace(s, string.Empty);
    }

    private string ReplaceOurTagFromProfiliPersonali(string Contenuto, string Cognome, string Nome, string Sesso)
    {       
        try
        {
            Contenuto = Contenuto.Replace("(((COGNOME)))", Cognome);
            Contenuto = Contenuto.Replace("(((NOME)))", Nome);

            switch (Sesso.ToUpper())
            {
                case "M":
                    Contenuto = RemoveBetween(Contenuto, "(((#W#", ")))");
                    Contenuto = RemoveBetween(Contenuto, "(((#F#", ")))");
                    Contenuto = RemoveBetween(Contenuto, "(((#X#", ")))");
                    Contenuto = Contenuto.Replace("(((#M#", string.Empty);
                    Contenuto = Contenuto.Replace(")))", string.Empty);
                    break;

                case "F":
                    Contenuto = RemoveBetween(Contenuto, "(((#W#", ")))");
                    Contenuto = RemoveBetween(Contenuto, "(((#M#", ")))");
                    Contenuto = RemoveBetween(Contenuto, "(((#X#", ")))");
                    Contenuto = Contenuto.Replace("(((#F#", string.Empty);
                    Contenuto = Contenuto.Replace(")))", string.Empty);
                    break;

                default:
                    Contenuto = RemoveBetween(Contenuto, "(((#W#", ")))");
                    Contenuto = RemoveBetween(Contenuto, "(((#F#", ")))");
                    Contenuto = RemoveBetween(Contenuto, "(((#M#", ")))");
                    Contenuto = Contenuto.Replace("(((#X#", string.Empty);
                    Contenuto = Contenuto.Replace(")))", string.Empty);
                    break;
            }

            Contenuto = Contenuto.Replace("(())", string.Empty);
        }
        catch (Exception p)
        {
            if (DEBUG_MODE)
            {
                if (!MANUAL_MODE)
                    SendMailDebug("debug@sport-news.it", "debug@sport-news.it", p.ToString());
                else
                    System.Web.HttpContext.Current.Response.Write(p.ToString());
            }
        }
        return Contenuto;

    }

    private bool ClearTempRecipientsAndUpdateLog(string UserId,
        string ID1LogMessage, string DestinatariInviati, string DestinatarioEmail)
    {       
        SqlTransaction transaction = null;
        SqlDataReader reader = null;

        try
        {
            string ID1Log = string.Empty;
            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(
               System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                transaction = connection.BeginTransaction();
                command.Transaction = transaction;

                sqlQuery = "SET DATEFORMAT dmy; UPDATE [tbCommunicator_LogMessage] ";
                sqlQuery += "SET [DestinatariInviati]= CASE WHEN DestinatariInviati IS NULL THEN 1 ELSE (DestinatariInviati + 1) END";
                sqlQuery += ",[SendDateLastUpdate]=@SendDateLastUpdate";
                sqlQuery += " WHERE ID1LogMessage=" + ID1LogMessage;

                command.Parameters.Add("@SendDateLastUpdate", System.Data.SqlDbType.SmallDateTime);
                command.Parameters["@SendDateLastUpdate"].Value = DateTime.Now;
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();
                command.Parameters.Clear();

                sqlQuery = "SET DATEFORMAT dmy; DELETE FROM [tbCommunicator_TempRecipients] ";              
                sqlQuery += " WHERE Destinatario_Email = @Destinatario_Email";
                command.Parameters.Add("@Destinatario_Email", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Destinatario_Email"].Value = DestinatarioEmail;
                
                sqlQuery += " AND [SendType]='SCHED'";
                sqlQuery += " AND ID2LogMessage=" + ID1LogMessage;

                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();
                
                transaction.Commit();
            }

            return true;
        }
        catch (Exception p)
        {
            System.Web.HttpContext.Current.Response.Write(p.ToString());

            if (transaction != null)
            {
                if (reader != null)
                    reader.Close();

                transaction.Rollback();
            }

            if (DEBUG_MODE)
            {
                if (!MANUAL_MODE)
                    SendMailDebug("debug@sport-news.it", "debug@sport-news.it", p.ToString());
                else
                    System.Web.HttpContext.Current.Response.Write(p.ToString());
            }
            return false;
        }
    }

    private bool SendMailAndWriteLog()
    {
        if((DEBUG_MODE)&&(MANUAL_MODE))
         System.Web.HttpContext.Current.Response.Write("DEB SendMailAndWriteLog()<br />");

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString))
        {
            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                if ((DEBUG_MODE) && (MANUAL_MODE))
                 System.Web.HttpContext.Current.Response.Write("DEB PZD_MailPerLotto" + PZD_MailPerLotto     + "<br />");

                if (PZD_MailPerLotto.Length <= 0)
                    return false;

                sqlQuery = @"BEGIN TRAN; 
                            DECLARE @DataLettura Datetime;

                            SELECT @DataLettura = GETDATE();

                            SET DATEFORMAT dmy; 

                            SELECT TOP (" + PZD_MailPerLotto + @") ID2LogMessage
	                            ,Destinatario_UserId,Destinatario_Email
	                            ,Destinatario_Cognome,Destinatario_Nome,Destinatario_Sesso
	                            ,Titolo,Sender_Oggetto,Sender_Nome,Sender_Email
	                            ,SMTP_Server,SMTP_User,SMTP_Password,Contenuto1
                            FROM tbCommunicator_TempRecipients
                            WHERE SendType='SCHED' AND DATEDIFF(minute, SendMail_StartDate, @DataLettura)>=0 
                                AND (DataLettura IS NULL OR DATEDIFF(minute, DataLettura, @DataLettura) > 60); 

                            UPDATE tbCommunicator_TempRecipients
                            set DataLettura = @DataLettura
                            WHERE ID1Recipient IN (SELECT TOP (" + PZD_MailPerLotto + @") ID1Recipient
                                                    FROM tbCommunicator_TempRecipients
                                                    WHERE SendType='SCHED' AND DATEDIFF(minute, SendMail_StartDate, @DataLettura)>=0 
                                                        AND (DataLettura IS NULL OR DATEDIFF(minute, DataLettura, @DataLettura) > 60))
                            COMMIT;";

                if ((DEBUG_MODE) && (MANUAL_MODE))
                  System.Web.HttpContext.Current.Response.Write("DEB " + sqlQuery + "<br />");

                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();

                System.Collections.ArrayList myArrayList = new System.Collections.ArrayList();
                while (reader.Read())
                {
                    string[] myData = new string[14];

                    if (reader["Destinatario_UserId"] != DBNull.Value)
                        myData[0] = reader["Destinatario_UserId"].ToString();
                    else myData[0] = string.Empty;

                    if (reader["Destinatario_Email"] != DBNull.Value)
                        myData[1] = reader["Destinatario_Email"].ToString();
                    else myData[1] = string.Empty;

                    if (reader["Destinatario_Cognome"] != DBNull.Value)
                        myData[2] = reader["Destinatario_Cognome"].ToString();
                    else myData[2] = string.Empty;

                    if (reader["Destinatario_Nome"] != DBNull.Value)
                        myData[3] = reader["Destinatario_Nome"].ToString();
                    else myData[3] = string.Empty;

                    if (reader["Destinatario_Sesso"] != DBNull.Value)
                        myData[4] = reader["Destinatario_Sesso"].ToString();
                    else myData[4] = string.Empty;

                    if (reader["Titolo"] != DBNull.Value)
                        myData[5] = reader["Titolo"].ToString();
                    else myData[5] = string.Empty;

                    if (reader["Sender_Oggetto"] != DBNull.Value)
                        myData[6] = reader["Sender_Oggetto"].ToString();
                    else myData[6] = string.Empty;

                    if (reader["Sender_Nome"] != DBNull.Value)
                        myData[7] = reader["Sender_Nome"].ToString();
                    else myData[7] = string.Empty;

                    if (reader["Sender_Email"] != DBNull.Value)
                        myData[8] = reader["Sender_Email"].ToString();
                    else myData[8] = string.Empty;

                    if (reader["SMTP_Server"] != DBNull.Value)
                        myData[9] = reader["SMTP_Server"].ToString();
                    else myData[9] = string.Empty;

                    if (reader["SMTP_User"] != DBNull.Value)
                        myData[10] = reader["SMTP_User"].ToString();
                    else myData[10] = string.Empty;

                    if (reader["SMTP_Password"] != DBNull.Value)
                        myData[11] = reader["SMTP_Password"].ToString();
                    else myData[11] = string.Empty;

                    if (reader["Contenuto1"] != DBNull.Value)
                        myData[12] = reader["Contenuto1"].ToString();
                    else myData[12] = string.Empty;

                    if (reader["ID2LogMessage"] != DBNull.Value)
                        myData[13] = reader["ID2LogMessage"].ToString();
                    else
                    {
                        reader.Close();
                        return false;
                    }

                    myArrayList.Add(myData);
                }

                reader.Close();

                string[][] TempRecipients = (string[][])myArrayList.ToArray(typeof(string[]));
                if (TempRecipients.Length <= 0)
                    return false;
                int Destinatario_UserId = 0;
                int Destinatario_Email = 1;
                int Destinatario_Cognome = 2;
                int Destinatario_Nome = 3;
                int Destinatario_Sesso = 4;
                int Titolo = 5;
                int Sender_Oggetto = 6;
                int Sender_Nome = 7;
                int Sender_Email = 8;
                int SMTP_Server = 9;
                int SMTP_User = 10;
                int SMTP_Password = 11;
                int Contenuto1 = 12;
                int ID2LogMessage = 13;

                int Inviati = 0;

                System.Xml.Linq.XElement MyXml = null;
                
                    for (int i = 0; i < TempRecipients.Length; ++i)
                    {                        
                        try
                        {
                            MyXml = XElement.Load(System.Web.HttpContext.Current.Server.MapPath("~/ZeusInc/TrackingMailSent/Report_") 
                                + TempRecipients[i][ID2LogMessage] + ".xml");
                        }
                        catch (Exception)
                        {
                            MyXml = new XElement("MailSent");
                            MyXml.Save(System.Web.HttpContext.Current.Server.MapPath("~/ZeusInc/TrackingMailSent/Report_") 
                                + TempRecipients[i][ID2LogMessage] + ".xml");
                        }

                        Inviati = GetInviati(TempRecipients[i][ID2LogMessage]);

                    string Contenuto = ReplaceOurTagFromProfiliPersonali(TempRecipients[i][Contenuto1], TempRecipients[i][Destinatario_Cognome]
                        , TempRecipients[i][Destinatario_Nome], TempRecipients[i][Destinatario_Sesso]);

                    string Oggetto = ReplaceOurTagFromProfiliPersonali(
                        TempRecipients[i][Sender_Oggetto], TempRecipients[i][Destinatario_Cognome]
                        , TempRecipients[i][Destinatario_Nome], TempRecipients[i][Destinatario_Sesso]);

                        if (SendMail(TempRecipients[i][Sender_Email]
                            , TempRecipients[i][Destinatario_Email]
                            , Contenuto
                            , TempRecipients[i][Sender_Nome]
                            , Oggetto
                            , string.Empty
                            , TempRecipients[i][SMTP_Server]
                            , TempRecipients[i][SMTP_User]
                            , TempRecipients[i][SMTP_Password]
                            ))
                        {
                            ++Inviati;
                            if (TempRecipients[i][Destinatario_UserId] != string.Empty)
                            {
                                var M = from a in MyXml.Elements("Mail")
                                        where a.Element("UserId").Value.Equals(TempRecipients[i][Destinatario_UserId])
                                        select a;

                                if (M.Count() > 0)
                                {
                                    M.First().Element("Esito").Value = "Inviato";
                                    M.First().Element("DataInvio").Value = DateTime.Now.ToString();
                                }                                
                            }
                            else
                            {
                                var M = from a in MyXml.Elements("Mail")
                                        where a.Element("EMail").Value.Equals(TempRecipients[i][Destinatario_Email])
                                        select a;

                                if (M.Count() > 0)
                                {
                                    M.First().Element("Esito").Value = "Inviato";
                                    M.First().Element("DataInvio").Value = DateTime.Now.ToString();
                                }  
                            }

                            MyXml.Save(System.Web.HttpContext.Current.Server.MapPath("~/ZeusInc/TrackingMailSent/Report_") 
                                + TempRecipients[i][ID2LogMessage] + ".xml");
                    }
                    else
                    {
                        return false;
                    }
                    if (!ClearTempRecipientsAndUpdateLog(TempRecipients[i][Destinatario_UserId],
                        TempRecipients[i][ID2LogMessage], Inviati.ToString(), TempRecipients[i][Destinatario_Email]))
                        return false;
                    }
                   
                    CloseTicketLog();

                  return true;
            }
            catch (Exception p)
            {             
                if (DEBUG_MODE)
                {
                    if (!MANUAL_MODE)
                        SendMailDebug("debug@sport-news.it", "debug@sport-news.it", p.ToString());
                    else
                        System.Web.HttpContext.Current.Response.Write(p.ToString());
                }
                return false;
            }
        }
    }

    private bool SendMail(string From, string To, string Body,
       string Alias, string Subject, string fileName,
       string mySmtpClient, string UserName, string Password)
    {
        System.Web.HttpContext.Current.Response.Write(Environment.NewLine + "sendmail");

        Zeus.Mail myMail = new Zeus.Mail();
        
        try
        {
            myMail.MailToSend.To.Add(new MailAddress(To));
        }
        catch (Exception p)
        {
            System.Web.HttpContext.Current.Response.Write(p.ToString());
            return true;
        }

        try
        {
            myMail.MailToSend.From = new MailAddress(From, Alias);
            myMail.MailToSend.Subject = Subject;
            myMail.MailToSend.IsBodyHtml = true;
            myMail.MailToSend.Body = Body;

            if (fileName.Length > 0)
            {
                Attachment data = new Attachment(fileName);
                System.Net.Mime.ContentDisposition disposition = data.ContentDisposition;
                disposition.CreationDate = System.IO.File.GetCreationTime(fileName);
                disposition.ModificationDate = System.IO.File.GetLastWriteTime(fileName);
                disposition.ReadDate = System.IO.File.GetLastAccessTime(fileName);
                myMail.MailToSend.Attachments.Add(data);
            }

            if (mySmtpClient.Length <= 0)
                return false;

            //SmtpClient client = new SmtpClient(mySmtpClient);

            myMail.ExtSMTPSettings.Host = mySmtpClient;
            if ((UserName.Length > 0) && (Password.Length > 0))
            {
                //client.Credentials = new System.Net.NetworkCredential(UserName, Password);
                myMail.ExtSMTPSettings.Credentials = new System.Net.NetworkCredential(UserName, Password);
            }


            myMail.SendMail(Zeus.Mail.MailSendMode.Sync);

            return true;
        }
        catch (Exception p)
        {           
            if (DEBUG_MODE)
            {
                if (!MANUAL_MODE)
                    SendMailDebug("debug@sport-news.it", "debug@sport-news.it", p.ToString());
                else
                    System.Web.HttpContext.Current.Response.Write(p.ToString());
            }

            return false;
        }
    }

    public void CommunicatorDoWork()
    {
        if ((DEBUG_MODE) && (!MANUAL_MODE))
            SendMail("sa@delinea.it", "sa@delinea.it", "I'm working 1", string.Empty, string.Empty, string.Empty,
                "mail.delinea.it", string.Empty, string.Empty);

        if ((DEBUG_MODE) && (MANUAL_MODE))
         System.Web.HttpContext.Current.Response.Write("START<br />");
                
        if (!GetAllConfigurationData())
            return;
        if ((DEBUG_MODE) && (!MANUAL_MODE))
          SendMail("sa@delinea.it", "sa@delinea.it", "I'm working 1/2", string.Empty, string.Empty, string.Empty,
              "mail.delinea.it", string.Empty, string.Empty);

        if ((DEBUG_MODE) && (MANUAL_MODE))
            System.Web.HttpContext.Current.Response.Write("DEB Step1<br />");

        if (!CheckWorkTime())
            return;

        if((DEBUG_MODE)&&(!MANUAL_MODE))
          SendMail("sa@delinea.it", "sa@delinea.it", "I'm working 2", string.Empty, string.Empty, string.Empty,
              "mail.delinea.it", string.Empty, string.Empty);

        if((DEBUG_MODE)&&(MANUAL_MODE))
          System.Web.HttpContext.Current.Response.Write("DEB Step2<br />");

        bool myLoop = true;

        switch(AgentStatus.ToUpper())
        {
            case "WORK":

                if (CheckOpSpreadTime())
                {
                    UpdateLastRunStart();
                    UpdateStatus("WORK");
                    SendMailAndWriteLog();
                    UpdateLastRunUpdate();
                    UpdateStatus("OKPAR");

                    while (myLoop)
                    {
                        if (IsThereMoreWorks())
                        {
                            if (CheckWorkTime())
                            {
                                UpdateStatus("WORK");
                                if(!SendMailAndWriteLog())
                                    myLoop=false;
                                UpdateLastRunUpdate();
                                UpdateStatus("OKPAR");
                            }
                            else
                            {
                                myLoop = false;
                                UpdateLastRunEnd();
                                UpdateStatus("OKPAR");
                            }
                        }
                        else
                        {
                            myLoop = false;
                            UpdateLastRunEnd();
                            UpdateStatus("OKTOT");
                        }
                    }
                }

                break;

            case "ERROR":

                UpdateLastRunStart();
                    UpdateStatus("WORK");
                    SendMailAndWriteLog();
                    UpdateLastRunUpdate();
                    UpdateStatus("OKPAR");

                    while (myLoop)
                    {
                        if (IsThereMoreWorks())
                        {
                            if (CheckWorkTime())
                            {
                                UpdateStatus("WORK");
                                if (!SendMailAndWriteLog())
                                    myLoop = false;
                                UpdateLastRunUpdate();
                                UpdateStatus("OKPAR");
                            }
                            else
                            {
                                myLoop = false;
                                UpdateLastRunEnd();
                                UpdateStatus("OKPAR");
                            }
                        }
                        else
                        {
                            myLoop = false;
                            UpdateLastRunEnd();
                            UpdateStatus("OKTOT");
                        }
                    }
                break;

            case "OKTOT":

                UpdateLastRunStart();

                if (IsThereMoreWorks())
                {
                    UpdateStatus("WORK");
                    SendMailAndWriteLog();
                    UpdateLastRunUpdate();
                    UpdateStatus("OKPAR");

                    while (myLoop)
                    {
                        if (IsThereMoreWorks())
                        {
                            if (CheckWorkTime())
                            {
                                UpdateStatus("WORK");
                                if (!SendMailAndWriteLog())
                                    myLoop = false;
                                UpdateLastRunUpdate();
                                UpdateStatus("OKPAR");
                            }
                            else
                            {
                                myLoop = false;
                                UpdateLastRunEnd();
                                UpdateStatus("OKPAR");
                            }
                        }
                        else
                        {
                            myLoop = false;
                            UpdateLastRunEnd();
                            UpdateStatus("OKTOT");
                        }
                    }//fine while

                }//fine if
                else UpdateLastRunEnd();
               
                break;

            case "OKPAR":

                UpdateLastRunStart();
                UpdateStatus("WORK");
                SendMailAndWriteLog();
                UpdateLastRunUpdate();
                UpdateStatus("OKPAR");


               

                while (myLoop)
                {
                    if (IsThereMoreWorks())
                    {
                        if (CheckWorkTime())
                        {
                            UpdateStatus("WORK");
                            if (!SendMailAndWriteLog())
                                myLoop = false;
                            UpdateLastRunUpdate();
                            UpdateStatus("OKPAR");
                        }
                        else
                        {
                            myLoop = false;
                            UpdateLastRunEnd();
                            UpdateStatus("OKPAR");
                        }
                    }
                    else
                    {
                        myLoop = false;
                        UpdateLastRunEnd();
                        UpdateStatus("OKTOT");
                    }
                }//fine while
                break;
        
        }//fine switch


        if ((DEBUG_MODE) && (!MANUAL_MODE))
          SendMail("sa@delinea.it", "sa@delinea.it", "I'm working END", string.Empty, string.Empty, string.Empty, "mail.delinea.it", string.Empty, string.Empty);


        if ((DEBUG_MODE) && (MANUAL_MODE)) 
            System.Web.HttpContext.Current.Response.Write("END");

        return;
    }//fine CommunicatorDoWork




    public void PrintDB2()
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        try
        {

            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();


                sqlQuery = "SET DATEFORMAT dmy;";
                sqlQuery += "INSERT INTO [Test]([Descrizione],[Data])";
                sqlQuery += " SELECT [Cognome],GETDATE() FROM [tbProfiliPersonali]";
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();


            }//fine Using
        }
        catch (Exception p)
        {

            //System.Web.HttpContext.Current.Response.Write(p.ToString());

        }

        return;
    }//fine PrintDB2

}//fine classe 
