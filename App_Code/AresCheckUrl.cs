﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for AresCheckUrl
/// </summary>
public class AresCheckUrl : IHttpModule
{
    public AresCheckUrl()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public void Init(HttpApplication App)
    {
        App.BeginRequest += new System.EventHandler(Rewrite_URL);
    }


    public void Rewrite_URL(object sender, System.EventArgs args)
    {
        HttpApplication App = (HttpApplication)sender;
        string PagRichiesta = App.Request.Path.ToLower();
        AresUtilities myAresUtilities = new AresUtilities();
        if (myAresUtilities.ThereAreDuplicates())
            //App.Context.RewritePath("~/System/Message.aspx?0");
            App.Response.Redirect("~/it/System/Message.aspx?78");
			
		
    }

    public void Dispose() { } 
}//fine classe
