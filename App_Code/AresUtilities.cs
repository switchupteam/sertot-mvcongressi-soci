﻿using System;
using System.Collections;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;


public class AresUtilities
{
    public enum ZeusModulo : int
    {
        PageDesigner = 0,
        Publisher = 1,
        PublicPage = 2
    }

    #region Constructor
    public AresUtilities()
    {


    }
    #endregion

    #region Methods
    public string GetLangRewritted()
    {
        string myLang = string.Empty;
        string DefaultLang = "/en";


        myLang = "ITA";
        return myLang;


        //MULTILINGUA
        if (
           (System.Web.HttpContext.Current.Request.RawUrl.Trim().Length > 1)
               &&
           ((System.Web.HttpContext.Current.Request.RawUrl.Substring(0, 4).ToLower().Equals("/it/")) || (System.Web.HttpContext.Current.Request.RawUrl.Substring(0, 4).ToLower().Equals("/en/")))
               )
        {
            if (System.Web.HttpContext.Current.Request.QueryString["Lang"] != null)
            {
                if (System.Web.HttpContext.Current.Request.QueryString["Lang"].ToUpper().Equals("ENG"))
                {
                    myLang = "ENG";
                }
                else if (System.Web.HttpContext.Current.Request.QueryString["Lang"].ToUpper().Equals("ITA"))
                {
                    myLang = "ITA";
                }
                else
                {
                    string debug = System.Web.HttpContext.Current.Request.QueryString["Lang"].ToLower();
                    // BUG Login: se scade il timeuot si viene reindirizzati alla login con tutti i parametri presenti nell'URL;
                    // quindi compaiono 2 ITA: uno del rewrite, uno passato. Permetto quindi che vengano passati 2 lang=ITA
                    if (System.Web.HttpContext.Current.Request.QueryString["Lang"].ToLower().Equals("ita,ita"))
                    {
                        myLang = "ITA";
                    }
                    else
                    {
                        System.Web.HttpContext.Current.Response.Redirect("~/it/System/Message.aspx?ErrLang");
                    }
                }
            }
            else
            {
                //System.Web.HttpContext.Current.Response.Redirect("~/it/System/Message.aspx?ErrLang");
            }
        }
        else
        {
            try
            {
                if (HttpContext.Current.Request.UserLanguages != null)
                {
                    string[] languages = HttpContext.Current.Request.UserLanguages;

                    string language = languages[0].ToLowerInvariant().Trim();
                    if (language.Substring(0, 2).ToLower().Equals("it"))
                    {
                        //myLang = "ITA";

                        System.Web.HttpContext.Current.Response.Status = "301 Moved Permanently";
                        System.Web.HttpContext.Current.Response.AddHeader("Location", "/it" + System.Web.HttpContext.Current.Request.RawUrl);
                        System.Web.HttpContext.Current.Response.End();
                    }
                    else
                    {
                        System.Web.HttpContext.Current.Response.Status = "301 Moved Permanently";
                        System.Web.HttpContext.Current.Response.AddHeader("Location", DefaultLang + System.Web.HttpContext.Current.Request.RawUrl);
                        System.Web.HttpContext.Current.Response.End();
                    }
                }
                else
                {
                    System.Web.HttpContext.Current.Response.Status = "301 Moved Permanently";
                    System.Web.HttpContext.Current.Response.AddHeader("Location", DefaultLang + System.Web.HttpContext.Current.Request.RawUrl);
                    System.Web.HttpContext.Current.Response.End();
                }
            }
            catch (ArgumentException)
            {
                System.Web.HttpContext.Current.Response.Status = "301 Moved Permanently";
                System.Web.HttpContext.Current.Response.AddHeader("Location", DefaultLang + System.Web.HttpContext.Current.Request.RawUrl);
                System.Web.HttpContext.Current.Response.End();
            }
        }

        return myLang;
    }



    public string GetLangMaster(string MyPageURL, int XRI, System.Collections.Specialized.NameValueCollection QueryString)
    {
        // ---------------------------- 
        string Lang = "";
        ZeusModulo myZeusModulo = GetModuleFromURL(MyPageURL);

        Lang = GetLangfromDB(myZeusModulo, XRI.ToString());

        //if (Lang.Length == 0)
        //{
        //    if (QueryString["Lang"] != null)
        //    {
        //        if (AntiSQLInjectionLeft(QueryString, "Lang"))
        //        {
        //            Lang = QueryString["Lang"];

        //        }
        //    }
        //}

        if (Lang.Length == 0)
            return "ITA";
        else
            return Lang;


    }

    public string GetLang(ZeusModulo Module, int XRI, System.Collections.Specialized.NameValueCollection QueryString)
    {
        // ---------------------------- 
        string Lang = "";

        Lang = GetLangfromDB(Module, XRI.ToString());

        if (Lang.Length == 0)
        {
            if (QueryString["Lang"] != null)
            {
                if (AntiSQLInjectionLeft(QueryString, "Lang"))
                {
                    Lang = QueryString["Lang"];

                }
            }
        }

        if (Lang.Length == 0)
            return "ITA";
        else
            return Lang;


    }

    private ZeusModulo GetModuleFromURL(string MyPageURL)
    {
        if (MyPageURL != "")
        {
            // PUBLISHER NOTIZIA ////////////////////////////////////////////////////////////////////////////
            if (MyPageURL.ToLower().IndexOf("/newsdet/") > -1)
            {
                return ZeusModulo.Publisher;
            }
            // PUBLISHER NOTIZIE ////////////////////////////////////////////////////////////////////////////
            if (MyPageURL.ToLower().IndexOf("/news/") > -1)
            {
                return ZeusModulo.Publisher;
            }
            // PAGEDESIGNER WEB ////////////////////////////////////////////////////////////////////////////
            if (MyPageURL.ToLower().IndexOf("/web/") > -1)
            {
                return ZeusModulo.PageDesigner;
            }

            return ZeusModulo.PublicPage;
        }

        return ZeusModulo.PublicPage;
    }


    private string GetLangfromDB(ZeusModulo Module, string XRI)
    {
        string table = string.Empty;
        string KeyName = string.Empty;
        string Lang = string.Empty;

        switch (Module)
        {
            case ZeusModulo.PageDesigner:
                table = "tbPageDesigner";
                KeyName = "ID1Pagina";
                break;
            case ZeusModulo.Publisher:
                table = "tbPublisher";
                KeyName = "ID1Publisher";
                break;
        }

        if (table.Length > 0)
        {

            try
            {
                String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionStringReadOnly"].ConnectionString;

                string sqlQuery = string.Empty;

                using (SqlConnection connection = new SqlConnection(
                   strConnessione))
                {

                    connection.Open();
                    SqlCommand command = connection.CreateCommand();

                    sqlQuery = "SELECT ZeusLangCode FROM " + table + " WHERE " + KeyName + " = @XRI";
                    command.CommandText = sqlQuery;
                    command.Parameters.Add("@XRI", System.Data.SqlDbType.Int);
                    command.Parameters["@XRI"].Value = XRI;

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                        if (reader["ZeusLangCode"] != DBNull.Value)
                        {
                            Lang = reader["ZeusLangCode"].ToString();
                        }

                    reader.Close();
                    return Lang;

                }//fine using


            }
            catch (Exception p)
            {
                return "";

            }
        }
        return table;


    }//fine CheckIsAdminUser

    public bool AntiSQLInjectionLeft(System.Collections.Specialized.NameValueCollection QueryString, string param)
    {
        if (QueryString.Count == 0)
            return false;

        //String[] myStrArr = new String[QueryString.Count];
        //QueryString.CopyTo(myStrArr, 0);
        //QueryString.

        foreach (string item in QueryString.AllKeys)
        {
            if (item != null)
            {
                if (item.ToLower().Equals(param.ToLower())) // && IsQueryStringSecure(QueryString[param])
                    return true;
            }
        }

        return false;
    }//fine AntiSQLInjectionLeft


    public bool ThereAreDuplicates()
    {
        bool Duplicates = false;
        foreach (string k in System.Web.HttpContext.Current.Request.QueryString.Keys)
        {
            int times = System.Web.HttpContext.Current.Request.QueryString.GetValues(k).Length;
            if (times > 1)
            {
                //System.Web.HttpContext.Current.Response.Write("Key " + k + " appears " + times + " times.<br />");
                if (!(k.ToLower().Equals("lang")) && (times == 2))
                    Duplicates = true;
            }
        }
        return Duplicates;
    }//fine ThereAreDuplicates

    public void redirectToPublicErrorPage()
    {
        HttpContext.Current.Response.Redirect("~/" + GetLangRewritted().Substring(0, 2).ToLower() + "/System/Message.aspx?0");
    }

    /// <summary>
    /// Controlla la pagina precedente contenga la stringa url
    /// </summary>
    /// <param name="url">stringa da verificare se nell'url precedente</param>
    /// <returns></returns>
    public static bool CheckPreviousPageContains(string url)
    {
        if (System.Web.HttpContext.Current.Request.UrlReferrer != null)
        {
            string PaginaChiamante = System.Web.HttpContext.Current.Request.UrlReferrer.ToString();
            if (PaginaChiamante.Contains(url) == true)
            {
                return true;
            }
        }
        return false;
    }
    #endregion
}
