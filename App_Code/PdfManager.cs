﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;

/// <summary>
/// Summary description for PdfManager
/// </summary>
public class PdfManager
{
    public PdfManager()
    {
    }

    public string CreaRicevutaPagamentoQuota(string idUtente, string annoProgressivo = null, string anno = null)
    {
        string numRicevuta = string.Empty;
        string data = string.Empty;
        string nome = string.Empty;
        string cognome = string.Empty;
        string via = string.Empty;
        string CAP = string.Empty;
        string citta = string.Empty;
        string provincia = string.Empty;
        string natoA = string.Empty;
        string natoIl = string.Empty;
        string codFiscale = string.Empty;
        string pIva = string.Empty;
        string importoQuota = string.Empty;
        string importoTestualeQuota = string.Empty;
        string annoQuota = string.Empty;

        string strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        using (SqlConnection connection = new SqlConnection(strConnessione))
        {
            connection.Open();
            SqlCommand command = connection.CreateCommand();
            string sqlQuery = @"SELECT Nome, Cognome, Indirizzo, Cap, Citta, ProvinciaSigla, CodiceFiscale, NascitaData, NascitaCitta, PartitaIVA
                                FROM tbProfiliPersonali 
                                WHERE UserId = @UserId";
            command.CommandText = sqlQuery;
            command.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
            command.Parameters["@UserId"].Value = new Guid(idUtente);

            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows && reader.Read())
            {
                nome = reader["Nome"].ToString();
                cognome = reader["Cognome"].ToString();
                via = reader["Indirizzo"].ToString();
                CAP = reader["Cap"].ToString();
                citta = reader["Citta"].ToString();
                provincia = reader["ProvinciaSigla"].ToString();
                codFiscale = reader["CodiceFiscale"].ToString();
                pIva = reader["PartitaIVA"].ToString();
                //natoA = reader["NascitaCitta"].ToString();

                //var dataDiNascita = reader["NascitaData"].ToString();
                //if (!string.IsNullOrWhiteSpace(dataDiNascita))
                //    natoIl = Convert.ToDateTime(dataDiNascita).ToString("dd/MM/yyyy");
            }
            reader.Close();


            sqlQuery = @"SELECT TOP (1) Anno, AnnoRicevuta, AnnoProgressivo, QuotaAssociativa, QuotaAssociativaTestuale
                        FROM tbQuoteAnnuali
	                        INNER JOIN tbQuote ON tbQuote.IDQuota = tbQuoteAnnuali.ID2QuotaCosto
                        WHERE UserId = @UserId AND Anno = @Anno
                        ORDER BY AnnoProgressivo DESC";
            command.Parameters.Clear();
            command.CommandText = sqlQuery;
            command.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
            command.Parameters["@UserId"].Value = new Guid(idUtente);
            command.Parameters.Add("@Anno", System.Data.SqlDbType.Int);
            command.Parameters["@Anno"].Value = anno;

            reader = command.ExecuteReader();
            if (reader.HasRows && reader.Read())
            {
                numRicevuta = !string.IsNullOrWhiteSpace(annoProgressivo) ? annoProgressivo : reader["AnnoProgressivo"].ToString();
                importoQuota = reader["QuotaAssociativa"].ToString();
                importoTestualeQuota = importoQuota + " " + reader["QuotaAssociativaTestuale"].ToString();
                annoQuota = !string.IsNullOrWhiteSpace(anno) ? anno : (Convert.ToInt32(reader["Anno"].ToString()) + 1).ToString();
            }
            reader.Close();
        }

        string filename = string.Format("RICEVUTA_Sertot_{0}.pdf", numRicevuta.Replace("/", "-"));
        string pdfTemplate = HttpContext.Current.Server.MapPath("/asset/files/RICEVUTA_Sertot_dyn.pdf");
        string newPdf = HttpContext.Current.Server.MapPath("/ZeusInc/Account/RicevuteSoci/" + filename);

        PdfReader pdfReader = new PdfReader(pdfTemplate);
        using (MemoryStream memoryStream = new MemoryStream())
        {
            byte[] bytes;
            using (PdfStamper pdfStamper = new PdfStamper(pdfReader, memoryStream))
            {
                pdfStamper.Writer.CloseStream = false;
                AcroFields pdfFormFields = pdfStamper.AcroFields;

                pdfFormFields.SetField("NumRicevuta", numRicevuta);
                pdfFormFields.SetField("Data", DateTime.Now.ToString("dd/MM/yyyy"));
                pdfFormFields.SetField("Nome", nome);
                pdfFormFields.SetField("Cognome", cognome);
                pdfFormFields.SetField("Via", via);
                pdfFormFields.SetField("CAP", CAP);
                pdfFormFields.SetField("Citta", citta);
                pdfFormFields.SetField("Provincia", provincia);
                //pdfFormFields.SetField("NatoA", natoA);
                //pdfFormFields.SetField("NatoIl", natoIl);
                pdfFormFields.SetField("CodFiscale", codFiscale);
                pdfFormFields.SetField("Piva", pIva);
                pdfFormFields.SetField("ImportoQuota", importoQuota);
                pdfFormFields.SetField("ImportoTestualeQuota", importoTestualeQuota);
                pdfFormFields.SetField("AnnoQuota", annoQuota);

                pdfStamper.Close();

                bytes = memoryStream.ToArray();
            }

            using (FileStream fileStream = new FileStream(newPdf, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                fileStream.Write(bytes, 0, bytes.Length);
            }
        }


        return filename;
    }
}