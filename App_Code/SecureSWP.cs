﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Linq;


public class SecureSWP
{
   

    public SecureSWP()
	{
		
	}

    #region Fields
    



    #endregion


    #region AntiSQLInjection


    public static bool IsQueryStringSecure(string TexttoValidate)
    {
        string TextVal;

        TextVal = TexttoValidate;

        bool IsSecure = true;

       string[] strDirtyQueryString = { "xp_", ";", "--", "<", ">", "script", "iframe", "delete", "drop", "exec", "=" };

        foreach (string item in strDirtyQueryString)
        {
            if (TextVal.IndexOf(item) != -1)
            {
                IsSecure = false;
            }
        }


 

        return IsSecure;
    } // IsQueryStringSecure


    public bool IsParametersSecure(System.Collections.Specialized.NameValueCollection QueryString)
    {
        
        // Parametri permessi, in minuscolo
        string[] strRightParameters = { "returnurl", "lang", "xri", "pid", "xre", "uid", "xri1", "xri2", "xri3", "zim", "url" };

        bool IsOk = false;

        foreach (string item in QueryString.AllKeys)
        {

            if (item != null)
            {

                IsOk = false;
                foreach (string itemParam in strRightParameters)
                {
                   if (item.ToLower().Equals(itemParam.ToLower()))
                    {
                        IsOk = true;
                    }
                }

                if (IsOk == false)
                    return false;
                    
            }
        }


        return IsOk;
    } // IsParametersSecure


    public bool IsParametersPolluted(System.Collections.Specialized.NameValueCollection QueryString)
    {


        bool Corrrect = true;
            foreach (string k in QueryString.Keys)
            {
                int times = QueryString.GetValues(k).Length;
                if (times > 1)
                {
                    Corrrect = false;
                }
            }
            return Corrrect;
       

    } // IsParametersPolluted

    //public bool IsParametersPollutedTest(System.Collections.Specialized.NameValueCollection QueryString)
    //{


    //    bool Corrrect = true;
    //    foreach (string k in QueryString.Keys)
    //    {
    //        System.Web.HttpContext.Current.Response.Write("Key: " + k + " <br />");
    //        int times = QueryString.GetValues(k).Length;
    //        if (times > 1)
    //        {
    //            System.Web.HttpContext.Current.Response.Write("Key " + k + " appears " + times + " times.<br />");
    //            Corrrect = false;
    //        }
    //    }
    //    return Corrrect;


    //    // //   new string[10];

    //    //int counter = 0;

    //    //foreach (string item in QueryString.AllKeys)
    //    //{
    //    //    ArrayParameters[counter] = item;
    //    //    counter++;
    //    //}

    //    // if (ArrayParameters.Distinct().Count() != QueryString.Count)
    //    //   return false;



    //} // IsParametersPolluted

    public bool AntiSQLInjectionRewriteSWP(System.Collections.Specialized.NameValueCollection QueryString)
    {
    // in caso di Rewrite controlliamo solo le Pollution e i parametri corretti


        if (QueryString.Count == 0)
            return true;

        // controllo che i parametri siano tra quelli accettati
        if (IsParametersSecure(QueryString) == false)
            return false;

        // controllo che i parametri non siano "polluted"
        if (IsParametersPolluted(QueryString) == false)
            return false;

        return true;

    }//fine AntiSQLInjectionNew
 


    public bool AntiSQLInjectionSWP(System.Collections.Specialized.NameValueCollection QueryString, string param, string Type)
    {
        // Type:
        // Int, Uid, String (controlliamo anche i caratteri indesiderati)


        if (QueryString.Count == 0)
            return true;

        if (param.Length == 0)
            return false;

        if (Type.Length == 0)
            return false;

        // controllo che i parametri siano tra quelli accettati
        if (IsParametersSecure(QueryString) == false)
            return false;

        // controllo che i parametri non siano "polluted"
        if (IsParametersPolluted(QueryString) == false)
            return false;

        bool myReturn = false;

        switch (Type.ToLower())
        {

            case "int":
                try
                {
                  //  if (QueryString[param].Length > 0)
                   //     Convert.ToInt32(QueryString[param]);

                    myReturn = true;
                }
                catch (Exception)
                { }
                break;

            case "uid":
                try
                {
                    if (QueryString[param].Length > 0)
                        new Guid(QueryString[param]);

                    myReturn = true;
                }
                catch (Exception)
                { }
                break;

            case "string":
                try
                {
                    if (QueryString[param].Length > 0)
                        QueryString[param].ToString();
                    // per le string controllo anche i testi contenuti
                    if (IsQueryStringSecure(QueryString[param]))
                    {
                        myReturn = true;
                    }
                }
                catch (Exception)
                { }
                break;


        }//fine switch



        return myReturn;

    }//fine AntiSQLInjectionNew


    #endregion

}