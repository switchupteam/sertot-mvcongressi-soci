﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// CLASSI PER LA SERIALIZZAZIONE E DESERIALIZZAZIONE DEI DATI DA FILE XML DI CONFIGURAZIONE
/// </summary>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
public partial class Configuratore
{
    private ConfiguratoreSitoWeb[] sitoWebField;

    [System.Xml.Serialization.XmlElementAttribute("SitoWeb")]
    public ConfiguratoreSitoWeb[] SitoWeb
    {
        get
        {
            return this.sitoWebField;
        }
        set
        {
            this.sitoWebField = value;
        }
    }
}

[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ConfiguratoreSitoWeb
{
    private ConfiguratoreSitoWebData dataField;

    private ConfiguratoreSitoWebHomePageSEO homePageSEOField;

    private ConfiguratoreSitoWebModuloContatti moduloContattiField;

    private string nomeField;

    private string ruoloField;

    private bool multilinguaField;

    public ConfiguratoreSitoWebData Data
    {
        get
        {
            return this.dataField;
        }
        set
        {
            this.dataField = value;
        }
    }

    public ConfiguratoreSitoWebHomePageSEO HomePageSEO
    {
        get
        {
            return this.homePageSEOField;
        }
        set
        {
            this.homePageSEOField = value;
        }
    }

    public ConfiguratoreSitoWebModuloContatti ModuloContatti
    {
        get
        {
            return this.moduloContattiField;
        }
        set
        {
            this.moduloContattiField = value;
        }
    }

    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string nome
    {
        get
        {
            return this.nomeField;
        }
        set
        {
            this.nomeField = value;
        }
    }

    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string ruolo
    {
        get
        {
            return this.ruoloField;
        }
        set
        {
            this.ruoloField = value;
        }
    }

    [System.Xml.Serialization.XmlAttributeAttribute()]
    public bool multilingua
    {
        get
        {
            return this.multilinguaField;
        }
        set
        {
            this.multilinguaField = value;
        }
    }
}

[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ConfiguratoreSitoWebData
{
    private string nomeSitoField;

    private string urlField;

    public string NomeSito
    {
        get
        {
            return this.nomeSitoField;
        }
        set
        {
            this.nomeSitoField = value;
        }
    }

    public string URL
    {
        get
        {
            return this.urlField;
        }
        set
        {
            this.urlField = value;
        }
    }
}

[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ConfiguratoreSitoWebHomePageSEO
{
    private ConfiguratoreSitoWebHomePageSEOITA iTAField;

    private ConfiguratoreSitoWebHomePageSEOENG eNGField;

    public ConfiguratoreSitoWebHomePageSEOITA ITA
    {
        get
        {
            return this.iTAField;
        }
        set
        {
            this.iTAField = value;
        }
    }

    public ConfiguratoreSitoWebHomePageSEOENG ENG
    {
        get
        {
            return this.eNGField;
        }
        set
        {
            this.eNGField = value;
        }
    }
}

[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ConfiguratoreSitoWebHomePageSEOITA
{
    private string titleField;

    private string descriptionField;

    private string keywordsField;

    public string Title
    {
        get
        {
            return this.titleField;
        }
        set
        {
            this.titleField = value;
        }
    }

    public string Description
    {
        get
        {
            return this.descriptionField;
        }
        set
        {
            this.descriptionField = value;
        }
    }

    public string Keywords
    {
        get
        {
            return this.keywordsField;
        }
        set
        {
            this.keywordsField = value;
        }
    }
}

[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ConfiguratoreSitoWebHomePageSEOENG
{
    private string titleField;

    private string descriptionField;

    private string keywordsField;

    public string Title
    {
        get
        {
            return this.titleField;
        }
        set
        {
            this.titleField = value;
        }
    }

    public string Description
    {
        get
        {
            return this.descriptionField;
        }
        set
        {
            this.descriptionField = value;
        }
    }

    public string Keywords
    {
        get
        {
            return this.keywordsField;
        }
        set
        {
            this.keywordsField = value;
        }
    }
}

[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ConfiguratoreSitoWebModuloContatti
{
    private string emailField;

    private string nomeField;

    private string oggettoField;

    public string Email
    {
        get
        {
            return this.emailField;
        }
        set
        {
            this.emailField = value;
        }
    }

    public string Nome
    {
        get
        {
            return this.nomeField;
        }
        set
        {
            this.nomeField = value;
        }
    }

    public string Oggetto
    {
        get
        {
            return this.oggettoField;
        }
        set
        {
            this.oggettoField = value;
        }
    }
}