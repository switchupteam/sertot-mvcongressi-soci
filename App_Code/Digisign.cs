﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using System.Data.SqlClient;
using System.Xml;
using System.Net;
using System.IO;

/// <summary>
/// Summary description for Digisign
/// </summary>
public class Digisign_Zeus
{
    public Digisign_Zeus()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    

    private bool ExportXML_Planner()
    {
        try
        {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();


                string sqlQuery = "SELECT [Duration],[Immagine1],[Contenuto1],[Contenuto2],[Link]";
                sqlQuery += " FROM [vwDigiSing_Dtl]";
                sqlQuery += " WHERE [AttivoArea1]>0";
                
                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();

                if (!reader.HasRows)
                {
                    reader.Close();
                    return false;
                }

                XmlDocument xmlDoc = new XmlDocument();
                XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", null);
                XmlElement rootNode = xmlDoc.CreateElement("content");
                rootNode.SetAttribute("Name", "SLIDESHOW TITLE");
                xmlDoc.InsertBefore(xmlDeclaration, xmlDoc.DocumentElement);
                xmlDoc.AppendChild(rootNode);


                XmlElement itemNode = xmlDoc.CreateElement("item");
                XmlElement copyNode = xmlDoc.CreateElement("copy");
                
                XmlCDataSection CDATANode ;

                string FONT_TITOLO = "<font color=\"#d02f36\" size=\"60\">";
                string FONT_DESCRIZIONE = "<font color=\"#666666\" size=\"40\">";

                string Contenuto1 = string.Empty;
                string Contenuto2 = string.Empty;

                while (reader.Read())
                {


                    if (reader["Immagine1"] != DBNull.Value)
                        itemNode.SetAttribute("Image","../../ZeusInc/DigiSign/Img1/"+ reader["Immagine1"].ToString());
                    else itemNode.SetAttribute("Image", string.Empty);


                    if (reader["Duration"] != DBNull.Value)
                        itemNode.SetAttribute("Delay", (Convert.ToInt16(reader["Duration"]) * 1000).ToString());
                    else itemNode.SetAttribute("Delay", string.Empty);

                  

                    if (reader["Link"] != DBNull.Value)
                        itemNode.SetAttribute("Link", reader["Link"].ToString());
                    else itemNode.SetAttribute("Link", string.Empty);


                    if (reader["Contenuto1"] != DBNull.Value)
                        Contenuto1 = FONT_TITOLO + reader["Contenuto1"].ToString() + "</font>" + "<br>";

                    if (reader["Contenuto2"] != DBNull.Value)
                        Contenuto2 = FONT_DESCRIZIONE + reader["Contenuto2"].ToString() + "</font>";

                    CDATANode = xmlDoc.CreateCDataSection(Contenuto1+Contenuto2);

                    copyNode.AppendChild(CDATANode);
                    itemNode.AppendChild(copyNode);
                    rootNode.AppendChild(itemNode);

                    itemNode = xmlDoc.CreateElement("item");
                    copyNode = xmlDoc.CreateElement("copy");
                
                }//fine while

                reader.Close();


                xmlDoc.Save(System.Web.HttpContext.Current.Server.MapPath("~/ZeusInc/DigiSign/Data/Planner.xml"));

                
                return true;

            }//fine Using
        }
        catch (Exception p)
        {
            System.Web.HttpContext.Current.Response.Write(p.ToString());
            return false;
        }

    }//fine ExportXML_Planner


    //private bool ExportXML_News()
    //{
    //    try
    //    {

    //        DigiSign ParmaDaily = new DigiSign();
    //        string[][] myArray = (string[][])ParmaDaily.GetXMLData();

    //        if (myArray != null)
    //        {

    //            XmlDocument xmlDoc = new XmlDocument();
    //            XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", null);
    //            XmlElement rootNode = xmlDoc.CreateElement("news");
    //            xmlDoc.InsertBefore(xmlDeclaration, xmlDoc.DocumentElement);
    //            xmlDoc.AppendChild(rootNode);


    //            XmlElement itemNode = xmlDoc.CreateElement("item");
    //            XmlElement dateNode = xmlDoc.CreateElement("date");
    //            XmlElement titleNode = xmlDoc.CreateElement("title");
    //            XmlElement urlNode = xmlDoc.CreateElement("url");

    //            for(int i=0; i<myArray.Length; ++i)
    //            {
    //                //System.Web.HttpContext.Current.Response.Write("Titolo:"+myArray[i][0]+"<br />");
    //                //System.Web.HttpContext.Current.Response.Write("Data"+myArray[i][1]+"<br />");
    //                //System.Web.HttpContext.Current.Response.Write("url"+myArray[i][2]+"<br />");
    //                //System.Web.HttpContext.Current.Response.Write("<br />");


    //                titleNode.InnerText = System.Web.HttpContext.Current.Server.HtmlDecode(myArray[i][0]);
    //                titleNode.InnerText = System.Text.RegularExpressions.Regex.Replace(titleNode.InnerText, "<.*?>", string.Empty);
    //                dateNode.InnerText = System.Web.HttpContext.Current.Server.HtmlDecode(myArray[i][1]);
    //                urlNode.InnerText = System.Web.HttpContext.Current.Server.HtmlDecode(myArray[i][2]);

    //                itemNode.AppendChild(dateNode);
    //                itemNode.AppendChild(titleNode);
    //                itemNode.AppendChild(urlNode);
    //                rootNode.AppendChild(itemNode);


    //                itemNode = xmlDoc.CreateElement("item");
    //                dateNode = xmlDoc.CreateElement("date");
    //                titleNode = xmlDoc.CreateElement("title");
    //                urlNode = xmlDoc.CreateElement("url");

    //            }//fine for

    //            xmlDoc.Save(System.Web.HttpContext.Current.Server.MapPath("~/ZeusInc/DigiSign/Data/News.xml"));

    //        }//fine if


    //        return true;

    //    }
    //    catch (Exception p)
    //    {
    //        System.Web.HttpContext.Current.Response.Write(p.ToString());
    //        return false;
    //    }

    //}//fine ExportXML_News


    private bool SaveGoogleWeather()
    {
        try
        {

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://www.google.com/ig/api?weather=Parma&hl=it");

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            Stream receiveStream = response.GetResponseStream();

            StreamReader oReader = new StreamReader(receiveStream, System.Text.Encoding.ASCII);

            
            //XmlDocument mySourceDoc = new XmlDocument();
            //mySourceDoc.Load(receiveStream);


            //mySourceDoc.Save();
            string myPath = System.Web.HttpContext.Current.Server.MapPath("~/zeusinc/digisign/data/GoogleWeather.xml");

            StreamWriter oWriter = new StreamWriter(myPath);
            oWriter.Write(oReader.ReadToEnd());

            oWriter.Close();
            oReader.Close();
            receiveStream.Close();
            response.Close();
            

            return true;
        }
        catch (Exception p)
        {
            System.Web.HttpContext.Current.Response.Write(p.ToString());
            return false;
        }

    }//fine SaveGoogleWeather




    public void DoWork()
    {
        ExportXML_Planner();
        //ExportXML_News();
        SaveGoogleWeather();

    }//fine doWork



}//fine classe
