﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

/// <summary>
/// Summary description for AresConfig
/// </summary>
public class AresConfig
{
    public static string XMLfilePath = "~/App_Data/configuratore.xml";

    //editable fields
    private static string nomeSito = "Società Emiliano Romagnola Triveneta di Ortopedia e Traumatologia";
    public static string NomeSito
    {
        get
        {
            return nomeSito;
        }

        private set
        {
            nomeSito = value;
        }
    }

    public static string GetBaseTitle()
    {
        return readConfigSingle("Data", "NomeSito");
    }

    public static string GetUrl()
    {
        return readConfigSingle("Data", "URL");
    }

    public static Dictionary<string, string> GetHomePageSEO(string lang)
    {
        return readConfigLang("HomePageSEO", lang);
    }

    public static string GetNomeMail()
    {
        return readConfigSingle("ModuloContatti", "Nome");
    }

    public static string GetEmail()
    {
        return readConfigSingle("ModuloContatti", "Email");
    }

    public static string GetOggettoMail()
    {
        return readConfigSingle("ModuloContatti", "Oggetto");
    }

    private static string readConfigSingle(string type, string parameter)
    {
        XDocument doc = XDocument.Load(System.Web.HttpContext.Current.Server.MapPath(XMLfilePath));
        IEnumerable<XElement> SitoWeb = (from element in doc.Root.Elements("SitoWeb") where (string)element.Attribute("nome") == nomeSito select element);

        string name = string.Empty;

        foreach (XElement element in SitoWeb.Descendants(type))
        {
            IEnumerable<XElement> children = element.Descendants();

            foreach (XElement child in children)
            {
                if (child.Name.LocalName == parameter)
                {
                    name = child.Value;
                }
            }
        }

        return name;
    }

    private static Dictionary<string, string> readConfigLang(string type, string lang)
    {
        XDocument doc = XDocument.Load(System.Web.HttpContext.Current.Server.MapPath(XMLfilePath));
        IEnumerable<XElement> SitoWeb = (from element in doc.Root.Elements("SitoWeb") where (string)element.Attribute("nome") == nomeSito select element);

        Dictionary<string, string> allItems = new Dictionary<string, string>();

        foreach (XElement elem in SitoWeb.Descendants(type))
        {
            IEnumerable<XElement> langs = elem.Descendants(lang);

            foreach (XElement language in langs)
            {
                IEnumerable<XElement> row = language.Descendants();

                foreach (XElement element in row)
                {
                    allItems.Add(element.Name.LocalName, element.Value);
                }
            }
        }

        return allItems;
    }

}