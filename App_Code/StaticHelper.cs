﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

public static class StaticHelper
{
    public static string GetDataItaliana(this DateTime data, string formato = null)
    {
        try
        {
            if (formato == null)
                formato = "dd/MM/yyyy";

            return data.ToString(formato, new CultureInfo("it-IT"));
        }
        catch (Exception ex)
        {
            return data.ToString("dd/MM/yyyy", new CultureInfo("it-IT"));
        }
    }

    public static string TruncateString(this string text, int maxChar = 300)
    {
        string result = text;

        if (result.Length > maxChar)
            result = result.Substring(0, maxChar) + "...";

        return result;
    }
}