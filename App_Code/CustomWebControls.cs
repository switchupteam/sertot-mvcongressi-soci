﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Collections;
using System.Web.UI.HtmlControls;
using System.Text;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Security.Permissions;
using System.Web;




namespace CustomWebControls
{
    /// <summary>
    /// Extended TextBox control that applies javascript validation to Multiline TextBox
    /// ATTENZIONE:Per il corretto utilizzo di questo componente e' necessario il file textArea.js nella cartella /SiteJS
    /// </summary>
    
    
    public class TextArea: TextBox
    {
        /// <summary>
        /// Override PreRender to include custom javascript
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            if (MaxLength > 0 && TextMode == TextBoxMode.MultiLine)
            {
                // Add javascript handlers for paste and keypress
                Attributes.Add("onkeypress","doKeypress(this);");
                Attributes.Add("onbeforepaste","doBeforePaste(this);");
                Attributes.Add("onpaste","doPaste(this);");
                // Add attribute for access of maxlength property on client-side
                Attributes.Add("maxLength", this.MaxLength.ToString());
                // Register client side include - only once per page
                if (!Page.ClientScript.IsClientScriptIncludeRegistered("TextArea"))
                {
                    Page.ClientScript.RegisterClientScriptInclude("TextArea", ResolveClientUrl("~/Zeus/SiteJS/textArea.js"));
                }
            }
            base.OnPreRender(e);
        }
    }//fine classe


    //#################################################################################################################

    public class DataListCounter : DataList
    {
        private int recordCount;
        private int currentIndex;
        private int pageSize;

        string sqlQuery = string.Empty;
        string tableName = string.Empty;

        // proprietà per la definizione di un template da utilizzare quando non ci sono dati

        private ITemplate emptyTemplate;

        [TemplateInstance(TemplateInstance.Single)]
        public ITemplate EmptyTemplate
        {
            get { return emptyTemplate; }
            set { emptyTemplate = value; }
        }



        protected override void Render(HtmlTextWriter output)
        {
            // se non ci sono dati, istanzio il template

            if (this.Items.Count == 0 && EmptyTemplate != null)
                EmptyTemplate.InstantiateIn(this);

            base.Render(output);

        }

        public void BindTheData()
        {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
            SqlConnection conn = new SqlConnection(strConnessione);

            if (sqlQuery.Length > 0)
            {
                SqlDataAdapter dataAdpter = new SqlDataAdapter(sqlQuery, conn);
                DataSet dataSet = new DataSet();

                if (!Page.IsPostBack)
                {
                    dataAdpter.Fill(dataSet);
                    recordCount = Convert.ToInt32(dataSet.Tables[0].Rows.Count);
                    dataSet = new DataSet();
                }

                dataAdpter.Fill(dataSet, currentIndex, pageSize, tableName);
                base.DataSource = dataSet.Tables[tableName].DefaultView;
                base.DataBind();
                conn.Close();

            }

            base.DataBind();
        }//fine BindTheData



        public int getRecordCount()
        {
            return recordCount;
        }


        public void setCurrentIndex(int myCurrentIndex)
        {
            currentIndex = myCurrentIndex;
        }

        public int getCurrentIndex()
        {
            return currentIndex;
        }


        public void setPageSize(int myPageSize)
        {
            pageSize = myPageSize;
        }

        public int getPageSize()
        {
            return pageSize;
        }


        public void setSqlQuery(string mySql)
        {
            sqlQuery = mySql;
        }

        public string getSqlQuery()
        {
            return sqlQuery;
        }


        public void setSqlTable(string myTable)
        {
            tableName = myTable;
        }

        public string getSqlTable()
        {
            return tableName;
        }




    }//fine classe

/*#############################################################################################################*/



    ///// <summary>
    ///// Percentage Complete 
    ///// <example>
    ///// 	<Controls:PercentageComplete id="PercentageComplete2" runat="server" Count="65" FillColor="#0099FF" Gradient="false" cellspacing="2" TextFontColor="#FF6600" TextBold="true" TextFontSize="2" RemainingColor="#ABD0BC" />	
    /////		<Controls:PercentageComplete id="PercentageComplete1" runat="server" Count="79" Text="79% occupied"  />
    /////		<Controls:PercentageComplete id="Percentagecomplete4" runat="server" Count="80" Text="8 of 10" StartColor="#6868B9" EndColor="#ABD0BC" BoxBorderColor="#FF6600" CellSpacing="0" TextFontFamily="Times New Roman" TextFontColor="White" TextFontSize="3"	BorderType="double" BoxBorderWidth="3" />
    /////		<Controls:PercentageComplete id="Percentagecomplete3" runat="server" Count="55" StartColor="#B7B748" EndColor="#E5E5E5"	BoxBorderColor="green" BgColor="#E5E5E5" BorderType="dotted" />
    /////		<Controls:PercentageComplete id="Percentagecomplete5" runat="server" Count="90" StartColor="#FF6600" EndColor="#0099FF"	BoxBorderColor="Red" CellPadding="3" cellspacing="1" TextFontFamily="Verdana" TextBold="True" TextFontColor="#FF9999" TextFontSize="3" BorderType="inset" GradientVertical="True" BoxBorderWidth="1"  />
    /////		<Controls:PercentageComplete id="Percentagecomplete6" runat="server" Count="45" StartColor="#99FF00" EndColor="#6600FF"	BoxBorderColor="Red" CellPadding="4" BgColor="#E8E8FF" BorderType="dashed" TextFontFamily="Tahoma" TextFontColor="red" TextFontSize="2" />
    ///// </example>
    ///// OUTPUT HTML:
    ///// <code>
    /////		<table cellspacing="0" cellpadding="0" style="border:1 solid #547095;background-color:white" width="95%" align="center" >
    /////			<tr >
    /////				<td width="100%">
    /////					<div style="position:absolute;background-color:#FF7979;width:<%# DataBinder.Eval(Container, "DataItem.SubmissionStatisticsTransactionCmpnt__PercentComplete") %>%;filter:progid:DXImageTransform.Microsoft.Gradient(GradientType=0,StartColorStr=#E5E5E5, EndColorStr=#FF7979);"  align="center"><font size="1">&nbsp;</font></div> 
    /////					<div style="position:relative;width:100%" align="center" ><font size="1" color=#3D526D><asp:Label id="lblPercentComplete" Runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SubmissionStatisticsTransactionCmpnt__PercentComplete") %>' />%</font></div>									
    /////				</td>								
    /////			</tr>						
    /////		</table>	

    //public class PercentageComplete : WebControl, INamingContainer
    //{
    //    private string borderColor = "#547095";
    //    private string borderType = "solid";
    //    private string borderWidth = "1";
    //    private string bgColor = "white";
    //    private string controlWidth = "95%";
    //    private string cellpadding = "0";
    //    private string cellspacing = "0";
    //    private string controlAlignment = "center";
    //    private string fillColor = "#FF7979";
    //    private string startColor = "#E5E5E5";
    //    private string endColor = "#FF7979";
    //    private bool gradientVertical = false;
    //    private string textFontSize = "1";
    //    private string textFontFamily = "verdana";
    //    private string textFontColor = "#3D526D";
    //    private bool textBold = false;
    //    private decimal count = 0;
    //    private string text = "";
    //    private bool displayText = true;
    //    private bool gradient = true;
    //    private string remainingColor = "";

    //    /// <summary>
    //    /// Integer or decimal value, mandatory, which tells how much percentage is completed.
    //    /// </summary>
    //    public decimal Count { set { count = value; } get { return count; } }
    //    /// <summary>
    //    /// You can override default percentage text with this.. e.g. if 19% is completed, then
    //    /// it will show "19%" text in middle, but you change text to "2 of 10" something like this.
    //    /// </summary>
    //    public string Text { set { text = value; } get { return text; } }
    //    /// <summary>
    //    /// By default it is true, set to false, and no text will be displayed in middle.
    //    /// </summary>
    //    public bool DisplayText { set { displayText = value; } get { return displayText; } }
    //    /// <summary>
    //    /// Box Border Color, this can be overwritten by Remaining Color.
    //    /// </summary>		
    //    public string BgColor { set { bgColor = value; } get { return bgColor; } }
    //    /// <summary>
    //    /// Box Width (default 100%)
    //    /// </summary>
    //    public string ControlWidth { set { controlWidth = value; } get { return controlWidth; } }
    //    /// <summary>
    //    /// Cellpadding of Table. default 0.
    //    /// </summary>
    //    public string Cellpadding { set { cellpadding = value; } get { return cellpadding; } }
    //    /// <summary>
    //    /// Cellspacing of Table. default 0.
    //    /// </summary>
    //    public string Cellspacing { set { cellspacing = value; } get { return cellspacing; } }
    //    /// <summary>
    //    /// Box Alignment in the container, by default center
    //    /// </summary>
    //    public string ControlAlignment { set { controlAlignment = value; } get { return controlAlignment; } }
    //    /// <summary>
    //    /// Box Border Color (E.g. red,#c0c0c0)
    //    /// </summary>		
    //    public string BoxBorderColor { set { borderColor = value; } get { return borderColor; } }
    //    /// <summary>
    //    /// Box Border Type i.e. (solid, dashed, dotted, ridge, inset, outset)
    //    /// </summary>
    //    public string BorderType { set { borderType = value; } get { return borderType; } }
    //    /// <summary>
    //    /// Box Border Width i.e. (0, 1, 2.. )
    //    /// </summary>
    //    public string BoxBorderWidth { set { borderWidth = value; } get { return borderWidth; } }
    //    /// <summary>
    //    /// Percentage Completed Color.
    //    /// </summary>
    //    public string FillColor { set { fillColor = value; } get { return fillColor; } }
    //    /// <summary>
    //    /// If percentage complete is 60%, then remaining 40% will have this color in background, no gradient supported for remaining color.
    //    /// </summary>
    //    public string RemainingColor { set { remainingColor = value; } get { return remainingColor; } }
    //    /// <summary>
    //    /// By default gradient effect is true, set this to false, and fill color will take preference over startcolor and endcolor
    //    /// </summary>		 
    //    public bool Gradient { set { gradient = value; } get { return gradient; } }
    //    /// <summary>
    //    /// By default gradient effect, and this is start color of gradient effect.
    //    /// </summary>
    //    public string StartColor { set { startColor = value; } get { return startColor; } }
    //    /// <summary>
    //    /// By default gradient effect, and this is end color of gradient effect.
    //    /// </summary>
    //    public string EndColor { set { endColor = value; } get { return endColor; } }
    //    /// <summary>
    //    /// By default gradient horizontal is true, you can set to to vertical instead of horizontal (default is false)
    //    /// </summary>
    //    public bool GradientVertical { set { gradientVertical = value; } get { return gradientVertical; } }
    //    /// <summary>
    //    /// Font Size of Text displayed in middle. 
    //    /// </summary>
    //    public string TextFontSize { set { textFontSize = value; } get { return textFontSize; } }
    //    /// <summary>
    //    /// Font family/face of text displayed in middle.
    //    /// </summary>
    //    public string TextFontFamily { set { textFontFamily = value; } get { return textFontFamily; } }
    //    /// <summary>
    //    /// Font color of text displayed in middle.
    //    /// </summary>
    //    public string TextFontColor { set { textFontColor = value; } get { return textFontColor; } }
    //    /// <summary>
    //    /// Whether displayed text in middle should be bold or not. (default false)
    //    /// </summary>
    //    public bool TextBold { set { textBold = value; } get { return textBold; } }


    //    public PercentageComplete()
    //        : base()
    //    {

    //    }

    //    protected override void Render(HtmlTextWriter output)
    //    {
    //        base.Render(output);
    //        string s = " ";
    //        s += "<table cellspacing='" + cellspacing + "' cellpadding='" + cellpadding + "' style='border:" + borderWidth + " " + borderType + " " + borderColor + ";background-color:" + bgColor + "' width='" + controlWidth + "' align='" + controlAlignment + "' >";
    //        s += "	<tr >";
    //        s += "	<td width='100%' style='background-color:" + remainingColor + "' >";
    //        s += " 		<div style='position:absolute;background-color:" + fillColor + ";width:" + count + "%;";
    //        if (gradient == true)
    //        {
    //            s += "      filter:progid:DXImageTransform.Microsoft.Gradient(GradientType=" + (gradientVertical == true ? "1" : "0") + ",StartColorStr=" + startColor + ", EndColorStr=" + endColor + ")";
    //        }
    //        s += "		;'  align='center'><font  size='" + TextFontSize + "'>&nbsp;</font></div> ";
    //        string textPart = "&nbsp;";
    //        if (displayText == true)
    //        {
    //            if (text == "")
    //                textPart = count.ToString() + "%";
    //            else
    //                textPart = text;
    //        }

    //        s += "		<div style='position:relative;width:100%' align='center' ><font " + (textBold == true ? "style='font-weight:bold'" : "") + " size='" + textFontSize + "' color='" + textFontColor + "' face='" + textFontFamily + "' >" + textPart + "</font></div>";
    //        s += " 	</td>";
    //        s += "</tr>";
    //        s += "</table>";
    //        output.Write(s);
    //    }
    //}
    
}//fine namespace