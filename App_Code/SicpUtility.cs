﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Web.Security;

/// <summary>
/// Descrizione di riepilogo per AierespsaUtility
/// </summary>
public class SertotUtility
{
    public SertotUtility()
    {

    }   

    public string ImpostaApprovato(string _IDPagamento)
    {
        int rows = 0;

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        try
        {
            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                command.CommandText = "UPDATE [tbQuoteAnnuali] SET [DataVerifica] = @Today WHERE [ID1QuotaAnnuale] = @ID1QuotaAnnuale";

                command.Parameters.Add("@ID1QuotaAnnuale", System.Data.SqlDbType.Int);
                command.Parameters["@ID1QuotaAnnuale"].Value = Convert.ToInt32(_IDPagamento);

                DateTime Data = new DateTime();
                Data = DateTime.Now;

                command.Parameters.Add("@Today", System.Data.SqlDbType.DateTime);
                command.Parameters["@Today"].Value = Data.ToString();

                rows = command.ExecuteNonQuery();




                return rows.ToString();
            }//fine Using
        }
        catch (Exception p)
        {

            return p.Message;
        }
    } // Fine ImpostaApprovato

    public string NuovoCodiceSocio()
    {
        int rows = 0;

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        int myMaxCodice = 0;
        string myCodiceSocioStr = string.Empty;
        string sqlQuery = string.Empty;

        try
        {
            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = @"SELECT MAX (CONVERT(INT, CodiceSocioStr)) AS CodiceSocioStr
                            FROM tbProfiliMarketing
                                INNER JOIN vwAccount_Lst_All ON vwAccount_Lst_All.UserId = tbProfiliMarketing.UserId
                            WHERE vwAccount_Lst_All.ZeusIsAlive = 1";
                command.CommandText = sqlQuery;
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (reader["CodiceSocioStr"] != null && reader["CodiceSocioStr"].ToString().Length > 0)
                        myMaxCodice = Convert.ToInt32(reader["CodiceSocioStr"]);
                    else
                        myMaxCodice = 1;
                }
                reader.Close();

                myMaxCodice++;
                switch (myMaxCodice.ToString().Length)
                {
                    case 1:
                        myCodiceSocioStr = "0000" + myMaxCodice.ToString();
                        break;
                    case 2:
                        myCodiceSocioStr = "000" + myMaxCodice.ToString();
                        break;
                    case 3:
                        myCodiceSocioStr = "00" + myMaxCodice.ToString();
                        break;
                    case 4:
                        myCodiceSocioStr = "0" + myMaxCodice.ToString();
                        break;
                    default:
                        myCodiceSocioStr = myMaxCodice.ToString();
                        break;
                }

                if (Convert.ToInt32(myCodiceSocioStr) < 100001)
                    myCodiceSocioStr = "1" + myCodiceSocioStr;


                return myCodiceSocioStr;
            }//fine Using
        }
        catch (Exception p)
        {

            return "0";
        }
    } // Fine ImpostaCodiceSocio

    public string StatoPagamentiUtente(string _UserID)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        String sqlQuery = string.Empty;
        string Risposta = "0";
        string Anno = "0";
        string AnnoCorrente = DateTime.Now.Year.ToString();
        string MeseCorrente = DateTime.Now.Month.ToString();
        try
        {
            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                ////////////////////////////////////////////// Controllo Se esiste un pagamento per quell'utente e di che hanno è /////////////////////////////////
                sqlQuery = "SELECT MAX(Anno) AS MaxAnno FROM dbo.tbPagamenti GROUP BY UserID HAVING (UserID = '" + _UserID.ToString() + "')";

                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    Anno = reader["MaxAnno"].ToString();
                }
                reader.Close();

                // Se non ha pagamenti restituisco "NESSUN PAGAMENTO"
                if (Anno == "0")
                    return "OK";


                // Se anno è il corrente restituisco "OK"
                if (Anno == AnnoCorrente)
                    return "OK";

                // Se anno è il precedente e siamo nei primi 3 mesi restituisco "SCADENZA" altrimenti restituisco "SCADUTO"
                if (Convert.ToInt32(Anno) == (Convert.ToInt32(AnnoCorrente) - 1))
                {

                    if (Convert.ToInt32(MeseCorrente) < 4)
                    {
                        return ("SCADENZA");
                    }
                    else
                    {
                        return ("SCADUTO");
                    }
                }

                // Se anno è ancora precedente restituisco "SCADUTO"
                if (Convert.ToInt32(Anno) < (Convert.ToInt32(AnnoCorrente) - 1))
                    return "SCADUTO";

                return "ERRORE";

            }//fine Using
        }
        catch (Exception p)
        {
            return p.Message;
        }
    } // Fine StatoPagamentiUtente



    public string CheckVisibilityDocumentoConvegno(int _IDPublisher)
    {
        try
        {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
            string sqlQuery = string.Empty;

            string risposta = string.Empty;

            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                // Ricavo le date di pubblicazione
                sqlQuery = @"SELECT     ID1Publisher, PW_Area1, PW_Area2, DATEDIFF(minute, PWI_Area2, GETDATE()) AS InizioArea2, DATEDIFF(minute, GETDATE(), PWF_Area2) AS FineArea2, 
                      DATEDIFF(minute, PWI_Area1, GETDATE()) AS InizioArea1, DATEDIFF(minute, GETDATE(), PWF_Area1) AS FineArea1, PWI_Area1
FROM         dbo.vwPublic_DocumentiConvegni
WHERE     (ID1Publisher =" + _IDPublisher.ToString() + ")";

                command.CommandText = sqlQuery;

                int InizioArea1 = 0;
                int FineArea1 = 0;
                string Area1 = string.Empty;
                string DataInizioArea1 = string.Empty;

                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();

                    InizioArea1 = Convert.ToInt32(reader["InizioArea1"]);
                    FineArea1 = Convert.ToInt32(reader["FineArea1"]);
                    Area1 = (reader["PW_Area1"].ToString());
                    DataInizioArea1 = (reader["PWI_Area1"].ToString());

                }
                else
                {
                    return "";
                }


                // -----------------controllo i diritti utente
                bool IsSocioConvegni = false;
                bool IsZeusAdmin = false;

                MembershipUser user = Membership.GetUser();
                // controllo che tipo di utente sono
                if (user != null)
                {
                    object userKey = user.ProviderUserKey;
                    IsSocioConvegni = Roles.IsUserInRole(user.UserName, "SociConvegni");
                    IsZeusAdmin = Roles.IsUserInRole(user.UserName, "ZeusAdmin");
                    IsSocioConvegni = IsZeusAdmin | IsSocioConvegni;
                }

                if (IsSocioConvegni == false)
                {
                    if ((Area1 == "True") && (InizioArea1 >= 0) && (FineArea1 >= 0))
                    {
                        risposta = "OK";
                    }
                    else
                    {
                        risposta = DataInizioArea1;
                    }

                }
                else
                {
                    risposta = "OK";
                }


                reader.Close();
                return risposta;
            }//fine Using
        }
        catch (Exception p)
        {
            return "Errore=" + p.Message;
        }
    } // CheckVisibilityDocumentoConvegno

    public string CheckVisibilityVerbali()
    {
        try
        {
            string sqlQuery = string.Empty;
            string risposta = string.Empty;

            // -----------------controllo i diritti utente
            bool IsZeusAdmin = false;
            bool IsSocio = false;

            MembershipUser user = Membership.GetUser();
            // controllo che tipo di utente sono
            if (user != null)
            {
                object userKey = user.ProviderUserKey;
                IsZeusAdmin = Roles.IsUserInRole(user.UserName, "ZeusAdmin");
                IsSocio = Roles.IsUserInRole(user.UserName, "Soci");
                IsSocio = IsZeusAdmin | IsSocio;
            }


            if (IsSocio == false)
            {

                risposta = "NO";

            }
            else
            {
                risposta = "OK";
            }

            return risposta;

        }
        catch (Exception p)
        {
            return "Errore=" + p.Message;
        }
    } // CheckVisibilityVerbali


    public string CheckVisibilityUfficioPresidenza()
    {
        try
        {
            string sqlQuery = string.Empty;
            string risposta = string.Empty;

            // -----------------controllo i diritti utente
            bool IsUfficioPresidenza = false;

            MembershipUser user = Membership.GetUser();
            // controllo che tipo di utente sono
            if (user != null)
            {
                object userKey = user.ProviderUserKey;
                IsUfficioPresidenza = Roles.IsUserInRole(user.UserName, "UfficioPresidenza");
            }


            if (IsUfficioPresidenza == false)
            {

                risposta = "NO";

            }
            else
            {
                risposta = "OK";
            }

            return risposta;

        }
        catch (Exception p)
        {
            return "Errore=" + p.Message;
        }
    } // CheckVisibilityUfficioPresidenza

    public string GetZeusIdModulo(int _IDPublisher)
    {
        try
        {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
            string sqlQuery = string.Empty;

            string risposta = string.Empty;

            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();


                sqlQuery = @"SELECT ZeusIdModulo
FROM         dbo.vwPublisher_Public_DtlArea1
WHERE     (ID1Publisher =" + _IDPublisher.ToString() + ")";

                command.CommandText = sqlQuery;
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    reader.Read();
                    return reader["ZeusIdModulo"].ToString();
                }

                reader.Close();
                return "";
            }//fine Using
        }
        catch (Exception p)
        {
            return "Errore=" + p.Message;
        }
    } // GetZeusIdModulo

}