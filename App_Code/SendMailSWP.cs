﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Net.Mail;
using System.Globalization;
using System.Text.RegularExpressions;


namespace Zeus
{


    public class Mail
    {

        public enum MailSendMode : int
        {
            Sync = 0,
            Async = 1
        }

        #region Properties



        public MailMessage MailToSend { get; set; }
        public SmtpClient ExtSMTPSettings { get; set; }

        #endregion


        #region Constructor
        public Mail()
        {
            MailToSend = new MailMessage();
            ExtSMTPSettings = new SmtpClient();
            //ExtSMTPSettings.Host = string.Empty;
        }
        #endregion

        #region Fields

        private static string hostName = "email-smtp.us-east-1.amazonaws.com";


        #endregion



        #region Events


        #endregion

        #region Methods


        // public void SendMailAsync(MailMessage mail)
        // 1 - Se il mittente è vuoto o non valido viene usato il mittente inserito nel file ZCF_mailSettings.config
        // 2 - Viene letto il file ZCF_mailSettings.config, se l'host è quello indicato nella proprietà hostName, ossia il server Amazon, viene impostata la trasmissione SSL porta 587


        public void SendMail(MailSendMode SendMode)
        {
            //if (ExtSMTPSettings.Host.Length > 0)
            //{

            //    if (SendMode == MailSendMode.Async)
            //        ExtSMTPSettings.SendAsync(MailToSend, string.Empty);
            //    else
            //        ExtSMTPSettings.Send(MailToSend);
            //}
            //else
            //{
            //----------------------- per smtp Amazon -----------------------
            System.Net.Configuration.SmtpSection section = ConfigurationManager.GetSection("system.net/mailSettings/smtp") as System.Net.Configuration.SmtpSection;

            //mail.From = new MailAddress("sviluppo@switchup.it", "Sito Web HD TECH");
            //Leggo il mittente inserito nel file /ZCF_mailSettings.config
            //if ((MailToSend.From.Address.ToString().Length == 0) || (!IsValidEmail(MailToSend.From.Address.ToString())))
            MailToSend.From = new MailAddress(section.From, AresConfig.NomeSito);

            MailToSend.IsBodyHtml = true;

            SmtpClient client = new SmtpClient();


            string host = section.Network.Host;

            if (host.Equals(hostName))
            {
                //    client.UseDefaultCredentials = false;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                //   client.Credentials = new System.Net.NetworkCredential("AKIAI2OQ6GZ6JT7ZNKFA", "AvCnSK+5Qt+OMMlv+43fhdYh/0IrTCCvD2ESj078pnQ2");
                client.Port = 587;
                client.EnableSsl = true;
            }
            //-------------------- fine smtp Amazon ------------------------

            if (SendMode == MailSendMode.Async)
                client.SendAsync(MailToSend, string.Empty);
            else
                client.Send(MailToSend);
            //}
        }





        bool invalid = false;

        private bool IsValidEmail(string strIn)
        {
            invalid = false;
            if (String.IsNullOrEmpty(strIn))
                return false;

            // Use IdnMapping class to convert Unicode domain names.
            strIn = Regex.Replace(strIn, @"(@)(.+)$", this.DomainMapper);
            if (invalid)
                return false;

            // Return true if strIn is in valid e-mail format.
            return Regex.IsMatch(strIn,
                   @"^(?("")(""[^""]+?""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                   @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9]{2,17}))$",
                   RegexOptions.IgnoreCase);
        }

        private string DomainMapper(Match match)
        {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            try
            {
                domainName = idn.GetAscii(domainName);
            }
            catch (ArgumentException)
            {
                invalid = true;
            }
            return match.Groups[1].Value + domainName;
        }
        #endregion



    }
}