﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HtmlAgilityPack;

/// <summary>
/// Summary description for HtmlSplitClass
/// </summary>
public class HtmlSplitClass {
    public char[] WORD_SEPARATOR {
        get { return new char[] { ' ' }; }
    }
    public string[] SENTENCE_SEPARATOR {
      //  get { return new string[] { ". ", ".<br>", ".<br/>", ".<br />" }; }
        get { return new string[] { "&nbsp;</span></p>" }; }
    }

    public string StrToSplit { get; private set; }
    public int NOfSubs { get; private set; }

    /// <summary>
    /// Lista di stringhe ottenute dalla divisione
    /// </summary>
    public List<string> strLstOutResult { get; private set; }
    /// <summary>
    /// Lista di oggetti che rappresentano le stringhe ottenute dalla divisione
    /// </summary>
    private List<SentenceElement> seLstOutResult { get; set; }

    /// <summary>
    /// Lunghezza della frase di input, espressa rispettivamente in caratteri, parole e periodi.
    /// </summary>
    public int charCount {
        get { return string.IsNullOrEmpty(StrToSplit) ? 0 : StrToSplit.Length; }
    }
    public int wordCount {
        get {
            return string.IsNullOrEmpty(StrToSplit) ? 0 : StrToSplit.Split(WORD_SEPARATOR).Count();
        }
    }
    public int sentenceCount {
        get {
            return string.IsNullOrEmpty(StrToSplit) ? 0 : StrToSplit.Split(SENTENCE_SEPARATOR, StringSplitOptions.None).Count();
        }
    }



    public HtmlSplitClass(string strToSplit) {
        StrToSplit = strToSplit;
        strLstOutResult = new List<string>();
        seLstOutResult = new List<SentenceElement>();
    }

    /// <summary>
    /// Esegue lo split della stringa <see cref="StrToSplit"/> passata alla classe
    /// </summary>
    /// <param name="nOfSubs"></param>
    /// <returns></returns>
    public byte DoSplit(int nOfSubs) {
        if (nOfSubs == 0)
            return 1;       // Hop: dividere una stringa in 0 parti?

        NOfSubs = nOfSubs;
        int chrNum = charCount / nOfSubs;           // Numero medio di caratteri per ogni periodo
        int chrStartPosition = 0;

        for (int i = 0; i < NOfSubs; i++) {
            int chrStartFind = chrNum * (i + 1);

            if (chrStartFind < chrStartPosition)
                chrStartFind = chrStartPosition;

            int chrSepPosition = -1;
            int j = 0, sentSepIndex = 0;

            while (j <= SENTENCE_SEPARATOR.Count() - 1) {
                int chrPosTmp = StrToSplit.IndexOf(SENTENCE_SEPARATOR[j], chrStartFind);

                if (chrPosTmp != -1) {
                    if (chrSepPosition == -1) {
                        chrSepPosition = chrPosTmp;
                        sentSepIndex = j;
                    }
                    else {
                        if (chrPosTmp < chrSepPosition) {
                            chrSepPosition = chrPosTmp;
                            sentSepIndex = j;
                        }
                    }
                }
                j++;
            }


            if (chrSepPosition == -1)
                chrSepPosition = StrToSplit.Length;
            else
                chrSepPosition += SENTENCE_SEPARATOR[sentSepIndex].Length;


            if (chrStartPosition > StrToSplit.Length)
                break;

            SentenceElement senElm = new SentenceElement(StrToSplit, chrStartPosition, chrSepPosition);
            seLstOutResult.Add(senElm);
            chrStartPosition = senElm.chrEnd;
        }


        if (chrStartPosition < StrToSplit.Length)
            seLstOutResult.Add(new SentenceElement(StrToSplit, chrStartPosition, StrToSplit.Length));

        strLstOutResult = new List<string>();
        foreach (SentenceElement seCrs in seLstOutResult) {
            strLstOutResult.Add(seCrs.strOutNoBR);
        }

        return 0;
    }


    /// <summary>
    /// Classe che rappresenta il singolo periodo.
    /// Quando si istanziano oggetti di questa classe, vengono salvati l'intera stringa da splittare, la posizione del primo carattere del periodo e la posizione del separatore.
    /// Se NON ci sono tag del tipo <see cref="TAGS_TO_ENCLOSE"/> aperti all'interno del periodo, allora il separatore coincide con l'ultimo carattere del periodo (<see cref="chrEnd"/>),
    /// altrimenti si cerca la chiusura del tag per chiudere anche il periodo.
    /// </summary>
    private class SentenceElement {
        /// <summary>
        /// Descrive i tag <br> da togliere all'inizio di ogni periodo
        /// </summary>
        public string[] BR_TO_DELETE {
            get { return new string[] { "<br>", "<br/>", "<br />" }; }
        }

        /// <summary>
        /// Sono i tag html per i quali non si può spezzare la stringa, ma occorre arrivare alla chiusura del tag
        /// </summary>
        public string[] TAGS_TO_ENCLOSE {
//            get { return new string[] { "a", "img", "span" }; }
            get { return new string[] { "a", "img" }; }
        }

        public int chrStart { get; private set; }               // Carattere dal quale inizia effettivamente il periodo
        public int chrEnd { get; private set; }                 // Carattere sul quale finisce effettivamente il periodo
        public int chrSepPosition { get; private set; }         // Posizione del separatore. Normalmente dovrebbe coincidere con chrEnd, ma dipende effettivamente dai tag html che lo precedono.
        public string strIn { get; private set; }       // Stringa di input, è la stringa INTERA
        /// <summary>
        /// Stringa di output. Viene utilizzato il namespace HtmlAgilityPack per chiudere i tag aperti.
        /// </summary>
        public string strOut {
            get {
                HtmlDocument htmlDoc;
                htmlDoc = new HtmlDocument();
                htmlDoc.OptionFixNestedTags = true;
                htmlDoc.LoadHtml(strIn.Substring(chrStart, chrEnd - chrStart));

                return htmlDoc.DocumentNode.OuterHtml;
            }
        }

        /// <summary>
        /// Stringa di output senza l'eventuale primo tag <br>
        /// </summary>
        public string strOutNoBR {
            get {

                string sTmp = strOut;

                foreach (string strCrs in BR_TO_DELETE) {
                    if (sTmp.StartsWith(strCrs)) {
                        sTmp = sTmp.Substring(0, strCrs.Length);
                        break;
                    }
                }

                return sTmp;
            }
        }


        /// <summary>
        /// Inizializza una nuova istanza della classe <see cref="SentenceElement" />.
        /// Viene contestualmente cercato l'ultimo carattere "buono" per il periodo (che dipende dai tag aperti).
        /// </summary>
        public SentenceElement(string strIn, int chrStart, int chrSepPosition) {
            this.strIn = strIn;
            this.chrStart = chrStart;
            this.chrSepPosition = chrSepPosition;
            chrEnd = chrSepPosition;

            foreach (string crsTag in TAGS_TO_ENCLOSE) {
                int iTmp = GetNextOpenTagPos(crsTag);

                if (iTmp > chrEnd)
                    chrEnd = iTmp;
            }
        }

        /// <summary>
        /// Verifica che il tag passato sia aperto nel periodo corrente e ne cerca la chiusura, prima nel periodo corrente, poi in quello successivo.
        /// </summary>
        /// <param name="tagToSearch">Nome del tag da cercare</param>
        /// <returns></returns>
        private int GetNextOpenTagPos(string tagToSearch) {
            if (chrStart > strIn.Length)
                System.Diagnostics.Debug.Assert(false);

            string mySearch = strIn.Substring(chrStart, chrSepPosition - chrStart);     // Periodo nel quale fare la ricerca, è solo una parte della stringa intera
            int crsTagPos = mySearch.LastIndexOf("<" + tagToSearch);            // Cerco l'apertura del tag...

            if (crsTagPos != -1) {                                              //...se la trovo...
                int crsTagEndPos = mySearch.IndexOf("</" + tagToSearch, crsTagPos);     // ...cerco la chiusura nel periodo corrente.

                if (crsTagEndPos == -1) {                               // ...se manca la chiusura,
                    crsTagEndPos = strIn.IndexOf("</" + tagToSearch, chrSepPosition);           // la cerco nel resto della stringa.
                    return strIn.IndexOf(">", crsTagEndPos) + 1;                                // E qui MI ASPETTO CI SIA!!!!
                }
                else {
                    return chrEnd;                  // Se trovo la chiusura nel periodo corrente, vuol dire che il carattere di fine periodo è giusto
                }
            }
            return 0;                               // Se non trovo nemmeno l'apertura, allora salto tutto!
        }
    }

}