﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Net.Mail;
using System.Globalization;
using System.Text.RegularExpressions;


namespace Zeus
{
    public enum SqlType
    {
        Int = System.Data.SqlDbType.Int,
        SmallDateTime = System.Data.SqlDbType.SmallDateTime,
        NVarChar = System.Data.SqlDbType.NVarChar,
        NChar = System.Data.SqlDbType.NChar,
        UniqueIdentifier = System.Data.SqlDbType.UniqueIdentifier,
        Decimal = System.Data.SqlDbType.Decimal
    }
        
    public class MSSQLCrudSWP
    {


        #region Constructor
        public MSSQLCrudSWP()
        {
            sda = new System.Data.SqlClient.SqlDataAdapter();

        }
        #endregion

        #region Properties
        private System.Data.SqlClient.SqlDataAdapter sda { get; set; }
        #endregion
        
        #region Fields

        

        #endregion

        #region Events


        #endregion

        #region Methods

        public void SetConnectionString(string _ConnectionString)
        {
            sda.SelectCommand = new System.Data.SqlClient.SqlCommand(_ConnectionString);

        } // SetConnectionString

        public void AddParameter(string ParamName, Zeus.SqlType ParamType, object Val, int Size)
        {
            try
            {
                if (sda.SelectCommand == null)
                    throw new ArgumentNullException("SetConnectionString before than Parameters");

                // -------------- Metto questo controllo perché voglio che in caso di stringa la sua Lunghezza non deve essere maggiore di SIZE
                // -------------- Mentre ora il comportamento è quello di tagliarla
                if ((ParamType == Zeus.SqlType.NChar) || (ParamType == Zeus.SqlType.NVarChar))
                    if (Val.ToString().Length > 0)
                        if (Val.ToString().Length > Size)
                        {
                            throw new ArgumentNullException("Error on Insert Parameter: String Length > Size");
                        }

                System.Data.SqlDbType myParamType = (System.Data.SqlDbType)ParamType;

                sda.SelectCommand.Parameters.Add(ParamName, myParamType);
                sda.SelectCommand.Parameters[ParamName].SqlDbType = myParamType;
                sda.SelectCommand.Parameters[ParamName].Size = Size;
                sda.SelectCommand.Parameters[ParamName].Value = Val;
                
            }
            catch
            {
                throw new ArgumentNullException("Error in AddParameter with Size.");
            }
        } // AddParameter

        public void AddParameter(string ParamName, Zeus.SqlType ParamType, object Val)
        {
            try 
            {
                if (sda.SelectCommand == null)
                    throw new ArgumentNullException("SetConnectionString before than Parameters");

                System.Data.SqlDbType myParamType = (System.Data.SqlDbType)ParamType;

                sda.SelectCommand.Parameters.Add(ParamName, myParamType);
                sda.SelectCommand.Parameters[ParamName].SqlDbType = myParamType;
                sda.SelectCommand.Parameters[ParamName].Value = Val;
                   }
            catch
            {
                throw new ArgumentNullException("Error in AddParameter.");
            }
        } // AddParameter
        
        public System.Data.DataTable GetData()
        {
            try
            {
            if (sda.SelectCommand.ToString().Length == 0)
                throw new ArgumentNullException("Command not inserted.");

            if (sda.SelectCommand == null)
                throw new ArgumentNullException("Command not inserted.");


            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
            using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(strConnessione))
            {
                      using (System.Data.SqlClient.SqlDataAdapter _sda = sda)
                        {
                            _sda.SelectCommand.Connection = con;
                        using (System.Data.DataSet ds = new System.Data.DataSet())
                        {
                            System.Data.DataTable dt = new System.Data.DataTable();
                           // System.Data.SqlClient.SqlCommand test = (System.Data.SqlClient.SqlCommand)_sda.SelectCommand;
                           // System.Web.HttpContext.Current.Response.Write(test.CommandText + "<br />");
                            _sda.Fill(dt);
                            return dt;
                        }
                    }
                
            }
             }
            catch
            {
                throw new ArgumentNullException("Error in GetData.");
            }
        } // GetData

        #endregion



    }
}