﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Xml;
using System.Xml.XPath;
using System.Net.Mail;


/// <summary>
/// Summary description for AdFarm
/// </summary>
public class AdFarm
{


    string CONNECTION_STRING = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
    //static string CONNECTION_STRING = "Data Source=62.149.153.15;Initial Catalog=MSSql34429;Persist Security Info=True;User ID=MSSql34429;Password=13d184e8";

//    string SAVE_XML_PATH = @"E:\DevSites_Maya\Zeus.it\ZeusInc_Zeus\AdFarm\Tracking_Log\";
    string SAVE_XML_PATH = System.Web.HttpContext.Current.Server.MapPath("~/ZeusInc/AdFarm/Tracking_Log/");

    private string OraInizio = string.Empty;
    private string OraFine = string.Empty;
    private string DataInizio = string.Empty;
    private string DataFine = string.Empty;
    private string AgentStatus = string.Empty;
    private string LastRunUpdate = string.Empty;
    private string OpSpreadTime = string.Empty;
    private string ZeusIdModulo = "ADFRM";

    private bool DEBUG_MODE = true;


    private DateTime FreezeDate = DateTime.Now;


    public AdFarm()
    {

    }//fine AdFarm

    public AdFarm(string myZeusIdModulo)
    {
        ZeusIdModulo = myZeusIdModulo;


    }//fine AdFarm

    public AdFarm(string myZeusIdModulo, bool myDEBUG_MODE)
    {
        ZeusIdModulo = myZeusIdModulo;
        DEBUG_MODE = myDEBUG_MODE;


    }//fine AdFarm


    private bool GetAllConfigurationData()
    {
        string sqlQuery = string.Empty;

        try
        {

            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();


                sqlQuery = "SET DATEFORMAT dmy; SELECT [OraInizio],[OraFine],[DataInizio],[DataFine]";
                sqlQuery += " ,[AgentStatus],[LastRunUpdate],[OpSpreadTime]";
                sqlQuery += " FROM [tbAgents_Dashboard]";
                sqlQuery += " WHERE [Agent]='ADFARM'";

                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (reader["OraInizio"] != DBNull.Value)
                        OraInizio = reader["OraInizio"].ToString();

                    if (reader["OraFine"] != DBNull.Value)
                        OraFine = reader["OraFine"].ToString();

                    if (reader["DataInizio"] != DBNull.Value)
                        DataInizio = Convert.ToDateTime(reader["DataInizio"]).ToString("d");

                    if (reader["DataFine"] != DBNull.Value)
                        DataFine = Convert.ToDateTime(reader["DataFine"]).ToString("d");

                    if (reader["AgentStatus"] != DBNull.Value)
                        AgentStatus = reader["AgentStatus"].ToString();

                    if (reader["LastRunUpdate"] != DBNull.Value)
                        LastRunUpdate = Convert.ToDateTime(reader["LastRunUpdate"]).ToString();

                    if (reader["OpSpreadTime"] != DBNull.Value)
                        OpSpreadTime = Convert.ToInt32(reader["OpSpreadTime"]).ToString();

                }//fine while

                reader.Close();


                return true;

            }//fine Using
        }
        catch (Exception p)
        {

            if (DEBUG_MODE)
                System.Web.HttpContext.Current.Response.Write(p.ToString());
            return false;
        }

    }//fine GetAllConfigurationData

    private bool CheckWorkTime()
    {

        if (OraInizio.Equals(OraFine))
            return true;

        bool IsInTime = false;

        try
        {

            DateTime StartDate = DateTime.Now;
            DateTime EndDate = DateTime.Now;

            if (Convert.ToInt32(OraInizio.Replace(":", string.Empty)) < Convert.ToInt32(OraFine.Replace(":", string.Empty)))
            {
                StartDate = Convert.ToDateTime(DateTime.Now.ToString("d") + " " + OraInizio);
                EndDate = Convert.ToDateTime(DateTime.Now.ToString("d") + " " + OraFine);
            }
            else
            {
                StartDate = Convert.ToDateTime(DateTime.Now.ToString("d") + " " + OraInizio);
                EndDate = Convert.ToDateTime(DateTime.Now.AddDays(1).ToString("d") + " " + OraFine);
            }

            long dateStartDiffVal = 0;
            long dateEndDiffVal = 0;

            TimeSpan ts1 = new TimeSpan(DateTime.Now.Ticks - StartDate.Ticks);
            TimeSpan ts2 = new TimeSpan(EndDate.Ticks - DateTime.Now.Ticks);

            dateStartDiffVal = (long)ts1.TotalMinutes;
            dateEndDiffVal = (long)ts2.TotalMinutes;

            if (DEBUG_MODE)
                System.Web.HttpContext.Current.Response.Write("<br />DEB dateStartDiffVal:" + dateStartDiffVal + " - dateEndDiffVal:" + dateEndDiffVal + "<br />");

            if ((dateStartDiffVal >= 0)
                && (dateEndDiffVal >= 0))
                IsInTime = true;

            if (IsInTime)
            {
                IsInTime = false;

                StartDate = Convert.ToDateTime(DataInizio);
                EndDate = Convert.ToDateTime(DataFine);

                dateStartDiffVal = 0;
                dateEndDiffVal = 0;

                ts1 = new TimeSpan(DateTime.Now.Ticks - StartDate.Ticks);
                ts2 = new TimeSpan(EndDate.Ticks - DateTime.Now.Ticks);

                dateStartDiffVal = (long)ts1.TotalDays;
                dateEndDiffVal = (long)ts2.TotalDays;

                if (DEBUG_MODE)
                    System.Web.HttpContext.Current.Response.Write("<br />DEB dateStartDiffVal:" + dateStartDiffVal + " - dateEndDiffVal:" + dateEndDiffVal + "<br />");

                if ((dateStartDiffVal >= 0)
                    && (dateEndDiffVal >= 0))
                    IsInTime = true;

            }

        }
        catch (Exception p)
        {

            if (DEBUG_MODE)
                System.Web.HttpContext.Current.Response.Write(p.ToString());
            return false;
        }

        return IsInTime;
    }//fine CheckWorkTime

    private bool CheckOpSpreadTime()
    {
        try
        {
            if (DEBUG_MODE)
                System.Web.HttpContext.Current.Response.Write("<br />DEB " + LastRunUpdate + "<br />");


            TimeSpan ts1 = new TimeSpan(DateTime.Now.Ticks - Convert.ToDateTime(LastRunUpdate).Ticks);


            if (DEBUG_MODE)
                System.Web.HttpContext.Current.Response.Write("<br />DEB " + Convert.ToInt32(OpSpreadTime) + " > " + (int)ts1.TotalMinutes + "<br />");
            if (Convert.ToInt32(OpSpreadTime) > (int)ts1.TotalMinutes)
                return true;


        }
        catch (Exception p)
        {
            if (DEBUG_MODE)
                System.Web.HttpContext.Current.Response.Write(p.ToString());

        }

        return false;

    }//fine CheckOpSpreadTime

    private bool UpdateLastRunStart()
    {

        if (DEBUG_MODE)
            System.Web.HttpContext.Current.Response.Write("<br />DEB UpdateLastRunStart()<br />");

        try
        {

            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SET DATEFORMAT dmy; UPDATE [tbAgents_Dashboard] ";
                sqlQuery += " SET LastRunStart=@LastRunStart";
                sqlQuery += " WHERE [Agent]='ADFARM'";
                command.Parameters.Add("@LastRunStart", System.Data.SqlDbType.SmallDateTime);
                command.Parameters["@LastRunStart"].Value = DateTime.Now;
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();


            }//fine Using

            return true;
        }
        catch (Exception p)
        {
            if (DEBUG_MODE)
                System.Web.HttpContext.Current.Response.Write(p.ToString());

            return false;
        }
    }//fine UpdateLastRunStart

    private bool UpdateLastRunEnd()
    {
        if (DEBUG_MODE)
            System.Web.HttpContext.Current.Response.Write("<br />DEB UpdateLastRunEnd()<br />");

        try
        {

            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SET DATEFORMAT dmy; UPDATE [tbAgents_Dashboard] ";
                sqlQuery += " SET LastRunEnd=@LastRunEnd";
                sqlQuery += " WHERE [Agent]='ADFARM'";
                command.Parameters.Add("@LastRunEnd", System.Data.SqlDbType.SmallDateTime);
                command.Parameters["@LastRunEnd"].Value = DateTime.Now;
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();


            }//fine Using

            return true;
        }
        catch (Exception p)
        {
            if (DEBUG_MODE)
                System.Web.HttpContext.Current.Response.Write(p.ToString());
            return false;
        }
    }//fine UpdateLastRunEnd

    private bool UpdateLastRunUpdate()
    {
        if (DEBUG_MODE)
            System.Web.HttpContext.Current.Response.Write("<br />DEB UpdateLastRunUpdate()<br />");

        try
        {

            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SET DATEFORMAT dmy; UPDATE [tbAgents_Dashboard] ";
                sqlQuery += " SET LastRunUpdate=@LastRunUpdate";
                sqlQuery += " WHERE [Agent]='ADFARM'";
                command.Parameters.Add("@LastRunUpdate", System.Data.SqlDbType.SmallDateTime);
                command.Parameters["@LastRunUpdate"].Value = DateTime.Now;
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();


            }//fine Using

            return true;
        }
        catch (Exception p)
        {
            if (DEBUG_MODE)
                System.Web.HttpContext.Current.Response.Write(p.ToString());

            return false;
        }
    }//fine UpdateLastRunUpdate

    private bool UpdateStatus(string STATUS)
    {
        if (DEBUG_MODE)
            System.Web.HttpContext.Current.Response.Write("<br />DEB UpdateStatus()<br />");

        try
        {

            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(
               CONNECTION_STRING))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SET DATEFORMAT dmy; UPDATE [tbAgents_Dashboard] ";
                sqlQuery += " SET AgentStatus=@AgentStatus";
                sqlQuery += " WHERE [Agent]='ADFARM'";
                command.Parameters.Add("@AgentStatus", System.Data.SqlDbType.NVarChar);
                command.Parameters["@AgentStatus"].Value = STATUS;
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();


            }//fine Using

            return true;
        }
        catch (Exception p)
        {

            if (DEBUG_MODE)
                System.Web.HttpContext.Current.Response.Write(p.ToString());

            return false;
        }
    }//fine UpdateLastRunStart

    private ArrayList GetAdFarm_Campaign()
    {
        ArrayList AdFarm_Campaign = new ArrayList();

        try
        {

            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(
               CONNECTION_STRING))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT [ID1Campaign],[Identificativo] ";
                sqlQuery += " FROM [vwAdfarm_Campaigns_Tracking]";



                command.CommandText = sqlQuery;
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    string[] myData = new string[2];

                    if (reader["ID1Campaign"] != DBNull.Value)
                        myData[0] = Convert.ToInt32(reader["ID1Campaign"]).ToString();
                    else myData[0] = string.Empty;

                    if (reader["Identificativo"] != DBNull.Value)
                        myData[1] = reader["Identificativo"].ToString();
                    else myData[1] = string.Empty;

                    AdFarm_Campaign.Add(myData);

                }//fine while

                reader.Close();
                command.Dispose();

            }//fine Using

        }
        catch (Exception p)
        {
            if (DEBUG_MODE)
                System.Web.HttpContext.Current.Response.Write(p.ToString());
        }

        return AdFarm_Campaign;

    }//fine GetAdFarm_Campaign

    public DataSet ConvertDataReaderToDataSet(SqlDataReader reader)
    {
        DataSet dataSet = new DataSet();

        do
        {

            DataTable schemaTable = reader.GetSchemaTable();
            DataTable dataTable = new DataTable();

            if (schemaTable != null)
            {

                for (int i = 0; i < schemaTable.Rows.Count; i++)
                {
                    DataRow dataRow = schemaTable.Rows[i];
                    string columnName = (string)dataRow["ColumnName"]; //+ "<C" + i + "/>";
                    DataColumn column = new DataColumn(columnName, (Type)dataRow["DataType"]);


                    

                    if (column.ColumnName.ToLower().Equals("eventtype"))
                        column.ColumnMapping = MappingType.Attribute;
                    else if (column.ColumnName.ToLower().Equals("ID1Campaign"))
                        column.ColumnMapping = MappingType.Attribute;
                    else if (column.ColumnName.ToLower().Equals("ID1AdTrack"))
                        column.ColumnMapping = MappingType.Attribute;
                    else if (column.ColumnName.ToLower().Equals("ID2Planner"))
                        column.ColumnMapping = MappingType.Attribute;
                    else if (column.ColumnName.ToLower().Equals("ID2Slot"))
                        column.ColumnMapping = MappingType.Attribute;
                    else if (column.ColumnName.ToLower().Equals("ID2Banner"))
                        column.ColumnMapping = MappingType.Attribute;
                    else column.ColumnMapping = MappingType.Element;

                    dataTable.Columns.Add(column);
                }

                dataSet.Tables.Add(dataTable);


                while (reader.Read())
                {
                    DataRow dataRow = dataTable.NewRow();

                    for (int i = 0; i < reader.FieldCount; i++)
                        dataRow[i] = reader.GetValue(i);

                    dataTable.Rows.Add(dataRow);

                    //if (reader["ID1AdTrack"] != DBNull.Value)
                    //    DeleteRecord(Convert.ToInt32(reader["ID1AdTrack"]).ToString());

                }
            }
            else
            {
                DataColumn column = new DataColumn("RowsAffected");

                //Console.Write(column.ColumnName.ToLower() + " ");

                if (column.ColumnName.ToLower().Equals("eventtype"))
                    column.ColumnMapping = MappingType.Attribute;
                else if (column.ColumnName.ToLower().Equals("ID1Campaign"))
                    column.ColumnMapping = MappingType.Attribute;
                else if (column.ColumnName.ToLower().Equals("ID1AdTrack"))
                    column.ColumnMapping = MappingType.Attribute;
                else if (column.ColumnName.ToLower().Equals("ID2Planner"))
                    column.ColumnMapping = MappingType.Attribute;
                else if (column.ColumnName.ToLower().Equals("ID2Slot"))
                    column.ColumnMapping = MappingType.Attribute;
                else if (column.ColumnName.ToLower().Equals("ID2Banner"))
                    column.ColumnMapping = MappingType.Attribute;
                else column.ColumnMapping = MappingType.Element;

                dataTable.Columns.Add(column);
                dataSet.Tables.Add(dataTable);
                DataRow dataRow = dataTable.NewRow();
                dataRow[0] = reader.RecordsAffected;
                dataTable.Rows.Add(dataRow);
            }
        }
        while (reader.NextResult());
        return dataSet;
    }//fine ConvertDataReaderToDataSet

    //private bool DeleteRecord(string ID1AdTrack)
    //{
    //    try
    //    {

    //        string sqlQuery = string.Empty;

    //        using (SqlConnection connection = new SqlConnection(
    //           CONNECTION_STRING))
    //        {

    //            connection.Open();
    //            SqlCommand command = connection.CreateCommand();

    //            sqlQuery = "SET DATEFORMAT dmy; DELETE ";
    //            sqlQuery += " FROM tbAdFarm_Tracking";
    //            sqlQuery += " WHERE  ID1AdTrack=" + ID1AdTrack;
    //            command.CommandText = sqlQuery;
    //            command.ExecuteNonQuery();


    //        }//fine Using

    //        return true;
    //    }
    //    catch (Exception p)
    //    {

    //        if (DEBUG_MODE)
    //            System.Web.HttpContext.Current.Response.Write(p.ToString());

    //        return false;
    //    }

    //}//fine DeleteRecord

    private bool DeleteAll(string ID1Campaign)
    {
        try
        {

            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(
               CONNECTION_STRING))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SET DATEFORMAT dmy; DELETE ";
                sqlQuery += " FROM tbAdFarm_Tracking";
                sqlQuery += " WHERE DATEDIFF(second,DateEvent,'" + FreezeDate.ToString().Replace(".", ":") + "')>0 ";
                sqlQuery += " AND ID2Campaign=" + ID1Campaign;
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();


            }//fine Using

            return true;
        }
        catch (Exception p)
        {

            if (DEBUG_MODE)
                System.Web.HttpContext.Current.Response.Write(p.ToString());

            return false;
        }

    }//fine DeleteAll

  
  

    private bool IsUnderBudgetSaldo(string ID1Campaign)
    {
        bool UnderBudgetSaldo = false;

        try
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                string sqlQuery = "SELECT [ID1Campaign]";
                sqlQuery += " FROM [tbAdFarm_Campaigns] ";
                sqlQuery += " WHERE  ID1Campaign=" + ID1Campaign;
                sqlQuery += " AND  [SendAvviso_BudgetSaldo] >= [Budget_Saldo] AND [SendAvviso_BudgetSaldo]=1";

                command.CommandText = sqlQuery;
                SqlDataReader reader = command.ExecuteReader();

                UnderBudgetSaldo = reader.HasRows;
                reader.Close();

            }//fine Using
        }
        catch (Exception p)
        {
            if (DEBUG_MODE)
                System.Web.HttpContext.Current.Response.Write(p.ToString());
        }


        return UnderBudgetSaldo;

    }//fine IsUnderBudgetSaldo

    private bool IsAllowToSendBudgetSaldoEmail(string ID1Campaign)
    {
        bool AllowToSendBudgetSaldoEmail = false;

        try
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                string sqlQuery = "SELECT SendAvviso_AllowSendBudgetSaldo";
                sqlQuery += " FROM [tbAdFarm_Campaigns] WHERE SendAvviso_BudgetSaldo=1 AND ID1Campaign=" + ID1Campaign;



                command.CommandText = sqlQuery;
                SqlDataReader reader = command.ExecuteReader();


                if (!reader.HasRows)
                {
                    reader.Close();
                    return AllowToSendBudgetSaldoEmail;
                }


                while (reader.Read())
                    if (reader["SendAvviso_AllowSendBudgetSaldo"] != DBNull.Value)
                        AllowToSendBudgetSaldoEmail = Convert.ToBoolean(reader["SendAvviso_AllowSendBudgetSaldo"]);

                reader.Close();




            }//fine Using
        }
        catch (Exception p)
        {
            if (DEBUG_MODE)
                System.Web.HttpContext.Current.Response.Write(p.ToString());
        }


        return AllowToSendBudgetSaldoEmail;

    }//fine IsAllowToSendBudgetSaldoEmail

    private bool IsOverData(string ID1Campaign)
    {
        bool OverData = false;

        try
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                string sqlQuery = "SELECT Avviso_Data,SendAvvisoLog_Data";
                sqlQuery += " FROM [tbAdFarm_Campaigns] WHERE (SendAvviso_Data=1 OR SendAvviso_AllowSendData=1) AND ID1Campaign=" + ID1Campaign;

                //if (DEBUG_MODE)
                //    System.Web.HttpContext.Current.Response.Write(sqlQuery);

                command.CommandText = sqlQuery;
                SqlDataReader reader = command.ExecuteReader();

                long dateDiffValAvviso_Data = -1;
                long dateDiffValSendAvvisoLog_Data = 0;

                while (reader.Read())
                {
                    if (reader["Avviso_Data"] != DBNull.Value)
                    {

                        TimeSpan ts = new TimeSpan(DateTime.Now.Ticks - Convert.ToDateTime(reader["Avviso_Data"]).Ticks);
                        dateDiffValAvviso_Data = (long)ts.TotalMinutes;
                    }

                    if (reader["SendAvvisoLog_Data"] != DBNull.Value)
                    {
                        TimeSpan ts = new TimeSpan(DateTime.Now.Ticks - Convert.ToDateTime(reader["SendAvvisoLog_Data"]).Ticks);
                        dateDiffValSendAvvisoLog_Data = (long)ts.TotalMinutes;

                    }


                }//fine while

                reader.Close();


                if ((dateDiffValAvviso_Data >= 0)
                    && (dateDiffValSendAvvisoLog_Data >= 0))
                {
                    OverData = true;
                    if (DEBUG_MODE)
                        System.Web.HttpContext.Current.Response.Write("<br />Is over data DiffValAvviso_Data:" + dateDiffValAvviso_Data + " - dateDiffValSendAvvisoLog_Data:" + dateDiffValSendAvvisoLog_Data);
                }
                else if (DEBUG_MODE)
                    System.Web.HttpContext.Current.Response.Write("<br />In NOT over data date DiffValAvviso_Data:" + dateDiffValAvviso_Data + " - dateDiffValSendAvvisoLog_Data:" + dateDiffValSendAvvisoLog_Data);



            }//fine Using
        }
        catch (Exception p)
        {
            if (DEBUG_MODE)
                System.Web.HttpContext.Current.Response.Write(p.ToString());
        }


        return OverData;

    }//fine IsOverData

    private bool SwitchOffAllowToSendBudgetSaldo(string ID1Campaign)
    {


        try
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                string sqlQuery = "UPDATE tbAdFarm_Campaigns";
                sqlQuery += " SET SendAvviso_AllowSendBudgetSaldo=0 , SendAvvisoLog_BudgetSaldo=@SendAvvisoLog_BudgetSaldo";
                sqlQuery += " WHERE ID1Campaign=" + ID1Campaign;


                command.Parameters.Add("@SendAvvisoLog_BudgetSaldo", SqlDbType.SmallDateTime);
                command.Parameters["@SendAvvisoLog_BudgetSaldo"].Value = DateTime.Now;

                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();

                return true;
            }//fine Using
        }
        catch (Exception p)
        {
            if (DEBUG_MODE)
                System.Web.HttpContext.Current.Response.Write(p.ToString());

            return false;
        }




    }//fine SwitchOffAllowToSendBudgetSaldo

    private bool SwitchOffCampaign(string ID1Campaign)
    {


        try
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                string sqlQuery = "UPDATE tbAdFarm_Campaigns";
                sqlQuery += " SET PW_Area1=0";
                sqlQuery += " WHERE Status <> 'SAT' AND ID1Campaign=" + ID1Campaign;

                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();

                //if (DEBUG_MODE)
                //    System.Web.HttpContext.Current.Response.Write("<br />Campagna "+ ID1Campaign +" disattivata (se non SAT)");

                return true;
            }//fine Using
        }
        catch (Exception p)
        {
            if (DEBUG_MODE)
                System.Web.HttpContext.Current.Response.Write(p.ToString());

            return false;
        }




    }//fine SwitchOffCampaign

    private bool SwitchOffCampaignByDate(string ID1Campaign)
    {


        try
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                string sqlQuery = "UPDATE tbAdFarm_Campaigns";
                sqlQuery += " SET PW_Area1=0";
                sqlQuery += " WHERE Status <> 'SAT' AND DateDiff(minute,getdate(),PWF_Area1)<=0 AND ID1Campaign=" + ID1Campaign;

                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();

                //if (DEBUG_MODE)
                //    System.Web.HttpContext.Current.Response.Write("<br />Campagna " + ID1Campaign + " disattivata per data");

                return true;
            }//fine Using
        }
        catch (Exception p)
        {
            if (DEBUG_MODE)
                System.Web.HttpContext.Current.Response.Write(p.ToString());

            return false;
        }




    }//fine SwitchOffCampaignByDate

    private bool SetSendAvvisoLog_Data(string ID1Campaign)
    {


        try
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                string sqlQuery = "UPDATE tbAdFarm_Campaigns";
                sqlQuery += " SET SendAvvisoLog_Data=GetDate()";
                sqlQuery += " ,SendAvviso_AllowSendData=0";
                sqlQuery += " ,SendAvviso_Data=0";
                sqlQuery += " WHERE ID1Campaign=" + ID1Campaign;

                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();

                return true;
            }//fine Using
        }
        catch (Exception p)
        {
            if (DEBUG_MODE)
                System.Web.HttpContext.Current.Response.Write(p.ToString());

            return false;
        }

    }//fine SetSendAvvisoLog_Data


    private bool SetSendAvvisoLog_BudgetSaldo(string ID1Campaign)
    {


        try
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                string sqlQuery = "UPDATE tbAdFarm_Campaigns";
                sqlQuery += " SET SendAvvisoLog_BudgetSaldo=GetDate()";
                sqlQuery += " WHERE SendAvviso_BudgetSaldo=1 AND ID1Campaign=" + ID1Campaign;

                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();

                return true;
            }//fine Using
        }
        catch (Exception p)
        {
            if (DEBUG_MODE)
                System.Web.HttpContext.Current.Response.Write(p.ToString());

            return false;
        }

    }//fine SetSendAvvisoLog_BudgetSaldo

    private ArrayList GetEmailAddess(string ID1Campaign)
    {


        ArrayList EmailAddess = new ArrayList();

        try
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                string sqlQuery = "SELECT Avviso_Email1,Avviso_Email2,Avviso_Email3,Avviso_Email4,Avviso_Email5";
                sqlQuery += " FROM tbAdFarm_Campaigns";
                sqlQuery += " WHERE ID1Campaign=" + ID1Campaign;

                command.CommandText = sqlQuery;
                SqlDataReader reader = command.ExecuteReader();


                while (reader.Read())
                {
                    if (reader["Avviso_Email1"] != DBNull.Value)
                        EmailAddess.Add(reader["Avviso_Email1"].ToString());

                    if (reader["Avviso_Email2"] != DBNull.Value)
                        EmailAddess.Add(reader["Avviso_Email2"].ToString());

                    if (reader["Avviso_Email3"] != DBNull.Value)
                        EmailAddess.Add(reader["Avviso_Email3"].ToString());

                    if (reader["Avviso_Email4"] != DBNull.Value)
                        EmailAddess.Add(reader["Avviso_Email4"].ToString());

                    if (reader["Avviso_Email5"] != DBNull.Value)
                        EmailAddess.Add(reader["Avviso_Email5"].ToString());

                }//fine while

                reader.Close();


            }//fine Using
        }
        catch (Exception p)
        {
            if (DEBUG_MODE)
                System.Web.HttpContext.Current.Response.Write(p.ToString());


        }

        return EmailAddess;

    }//fine GetEmailAddess

    private bool InsertDataIntoXML(string ID1Campaign, string Identificativo)
    {

        string sqlQuery = string.Empty;

        try
        {

            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                
                sqlQuery = "SELECT [ID1Campaign],[ID1AdTrack],[ID2Planner],[ID2Slot]";
                sqlQuery += " ,[DateEvent],[MoneyValue],[EventType],[ID2Banner],";
                sqlQuery += " [IPAddress]";
                sqlQuery += " FROM [vwAdFarm_Tracking]";
                sqlQuery += " WHERE ID1Campaign=" + ID1Campaign;
                sqlQuery += " AND DATEDIFF(second,DateEvent,'" + FreezeDate.ToString().Replace(".", ":") + "')>0 ";

                command.CommandText = sqlQuery;


                string FileName = Identificativo.Replace(" ", "_") + "__" + ID1Campaign + ".xml";

                if (!System.IO.File.Exists(SAVE_XML_PATH + FileName))
                {

                    if (DEBUG_MODE)
                        System.Web.HttpContext.Current.Response.Write("<br />Il file " + SAVE_XML_PATH + FileName + " NON esiste");

                    DataSet myDataSet = ConvertDataReaderToDataSet(command.ExecuteReader());

                    if (myDataSet == null)
                        return false;

                    System.IO.FileStream myFileStream = new System.IO.FileStream(SAVE_XML_PATH + FileName, System.IO.FileMode.Create);
                    myDataSet.WriteXml(myFileStream);
                    myFileStream.Close();
                    myDataSet.Dispose();



                }
                else
                {
                    if (DEBUG_MODE)
                        System.Web.HttpContext.Current.Response.Write("<br />Il file " + SAVE_XML_PATH + FileName + " esiste");


                    System.IO.FileStream myFileStream = System.IO.File.OpenRead(SAVE_XML_PATH + FileName);

                    if (myFileStream.Length > (10485760))
                    {
                        string DateCopy = DateTime.Now.Day.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Year.ToString()
                            + "_" + DateTime.Now.Hour.ToString() + "_" + DateTime.Now.Minute.ToString() + "_" + DateTime.Now.Second.ToString();

                        string NewFileName = Identificativo.Replace(" ", "_") + "_" + DateCopy + "__" + ID1Campaign + ".xml";

                        System.IO.File.Copy(SAVE_XML_PATH + FileName, SAVE_XML_PATH + NewFileName);
                        myFileStream.Close();
                        
                        DataSet myDataSet = ConvertDataReaderToDataSet(command.ExecuteReader());

                        if (myDataSet == null)
                            return false;

                        myFileStream = new System.IO.FileStream(SAVE_XML_PATH + FileName, System.IO.FileMode.Create);
                        myDataSet.WriteXml(myFileStream);
                        myFileStream.Close();
                        myDataSet.Dispose();

                    }
                    else
                    {

                        myFileStream.Close();

                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.Load(SAVE_XML_PATH + FileName);
                        if (DEBUG_MODE)
                            System.Web.HttpContext.Current.Response.Write("<br />File aperto");
                        XmlElement subRoot = null;
                        XmlElement appendedElement = null;
                        XmlText xmlText = null;

                        SqlDataReader reader = command.ExecuteReader();

                        if (DEBUG_MODE)
                            System.Web.HttpContext.Current.Response.Write("<br />Inserimento dati");

                        int index = 0;
                        //int CountOf = 0;

                        string NodeValue = string.Empty;

                        while (reader.Read())
                        {
                            ++index;

                           



                            subRoot = xmlDoc.CreateElement("Table1");


                            if (reader["EventType"] != DBNull.Value)
                                subRoot.SetAttribute("EventType", reader["EventType"].ToString());

                            if (reader["ID1Campaign"] != DBNull.Value)
                                subRoot.SetAttribute("ID1Campaign", reader["ID1Campaign"].ToString());


                            if (reader["ID2Banner"] != DBNull.Value)
                                subRoot.SetAttribute("ID2Banner", reader["ID2Banner"].ToString());


                            if (reader["ID1AdTrack"] != DBNull.Value)
                                subRoot.SetAttribute("ID1AdTrack", reader["ID1AdTrack"].ToString());

                            if (reader["ID2Planner"] != DBNull.Value)
                                subRoot.SetAttribute("ID2Planner", reader["ID2Planner"].ToString());

                            if (reader["ID2Slot"] != DBNull.Value)
                                subRoot.SetAttribute("ID2Slot", reader["ID2Slot"].ToString());




                            //appendedElement = xmlDoc.CreateElement("ID1Campaign");

                            //string NodeValue = string.Empty;

                            //if (reader["ID1Campaign"] != DBNull.Value)
                            //    NodeValue = Convert.ToInt32(reader["ID1Campaign"]).ToString();

                            //xmlText = xmlDoc.CreateTextNode(NodeValue);
                            //appendedElement.AppendChild(xmlText);
                            //subRoot.AppendChild(appendedElement);
                            //xmlDoc.DocumentElement.AppendChild(subRoot);


                            //NodeValue = string.Empty;

                            //if (reader["ID2Banner"] != DBNull.Value)
                            //    NodeValue = Convert.ToInt32(reader["ID2Banner"]).ToString();

                            //appendedElement = xmlDoc.CreateElement("ID2Banner");
                            //xmlText = xmlDoc.CreateTextNode(NodeValue);
                            //appendedElement.AppendChild(xmlText);
                            //subRoot.AppendChild(appendedElement);
                            //xmlDoc.DocumentElement.AppendChild(subRoot);

                            //NodeValue = string.Empty;

                            //if (reader["ID1AdTrack"] != DBNull.Value)
                            //    NodeValue = Convert.ToInt32(reader["ID1AdTrack"]).ToString();

                            //appendedElement = xmlDoc.CreateElement("ID1AdTrack");
                            //xmlText = xmlDoc.CreateTextNode(NodeValue);
                            //appendedElement.AppendChild(xmlText);
                            //subRoot.AppendChild(appendedElement);
                            //xmlDoc.DocumentElement.AppendChild(subRoot);


                            //NodeValue = string.Empty;

                            //if (reader["ID2Planner"] != DBNull.Value)
                            //    NodeValue = Convert.ToInt32(reader["ID2Planner"]).ToString();

                            //appendedElement = xmlDoc.CreateElement("ID2Planner");
                            //xmlText = xmlDoc.CreateTextNode(NodeValue);
                            //appendedElement.AppendChild(xmlText);
                            //subRoot.AppendChild(appendedElement);
                            //xmlDoc.DocumentElement.AppendChild(subRoot);


                            //NodeValue = string.Empty;

                            //if (reader["ID2Slot"] != DBNull.Value)
                            //    NodeValue = Convert.ToInt32(reader["ID2Slot"]).ToString();

                            //appendedElement = xmlDoc.CreateElement("ID2Slot");
                            //xmlText = xmlDoc.CreateTextNode(NodeValue);
                            //appendedElement.AppendChild(xmlText);
                            //subRoot.AppendChild(appendedElement);
                            //xmlDoc.DocumentElement.AppendChild(subRoot);


                            NodeValue = string.Empty;

                            if (reader["DateEvent"] != DBNull.Value)
                                NodeValue = Convert.ToDateTime(reader["DateEvent"]).ToString();

                            appendedElement = xmlDoc.CreateElement("DateEvent");
                            xmlText = xmlDoc.CreateTextNode(NodeValue);
                            appendedElement.AppendChild(xmlText);
                            subRoot.AppendChild(appendedElement);
                            xmlDoc.DocumentElement.AppendChild(subRoot);


                            NodeValue = string.Empty;

                            if (reader["MoneyValue"] != DBNull.Value)
                                NodeValue = Convert.ToDecimal(reader["MoneyValue"]).ToString().Replace(",", ".");

                            appendedElement = xmlDoc.CreateElement("MoneyValue");
                            xmlText = xmlDoc.CreateTextNode(NodeValue);
                            appendedElement.AppendChild(xmlText);
                            subRoot.AppendChild(appendedElement);
                            xmlDoc.DocumentElement.AppendChild(subRoot);



                            //if (reader["EventType"] != DBNull.Value)
                            //    NodeValue = reader["EventType"].ToString();

                            //appendedElement = xmlDoc.CreateElement("EventType");
                            //xmlText = xmlDoc.CreateTextNode(NodeValue);
                            //appendedElement.AppendChild(xmlText);
                            //subRoot.AppendChild(appendedElement);
                            //xmlDoc.DocumentElement.AppendChild(subRoot);


                            if (reader["IPAddress"] != DBNull.Value)
                                NodeValue = reader["IPAddress"].ToString();

                            appendedElement = xmlDoc.CreateElement("IPAddress");
                            xmlText = xmlDoc.CreateTextNode(NodeValue);
                            appendedElement.AppendChild(xmlText);
                            subRoot.AppendChild(appendedElement);
                            xmlDoc.DocumentElement.AppendChild(subRoot);



                        }//fine while

                        if (DEBUG_MODE)
                            System.Web.HttpContext.Current.Response.Write("<br />Fine inserimento dati");

                        reader.Close();
                        xmlDoc.Save(SAVE_XML_PATH + FileName);

                        if (DEBUG_MODE)
                            System.Web.HttpContext.Current.Response.Write("<br />File salvato con successo");

                    }//fine else

                }//fine else

                command.Dispose();
                return true;

            }//fine Using
        }
        catch (Exception p)
        {

            if (DEBUG_MODE)
                System.Web.HttpContext.Current.Response.Write(p.ToString());
            return false;
        }

    }//fine InsertDataIntoXML







    private bool SendMail(string From, string To, string Body,
   string Alias, string Subject, string fileName,
   string mySmtpClient, string UserName, string Password)
    {
        try
        {


            System.Text.RegularExpressions.Regex myRegex =
                new System.Text.RegularExpressions.Regex(@"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$");

            if (!myRegex.IsMatch(To))
                return false;

            Zeus.Mail myMail = new Zeus.Mail();

            myMail.MailToSend.To.Add(new MailAddress(To));
            myMail.MailToSend.From = new MailAddress(From, Alias);
            myMail.MailToSend.Subject = Subject;
            myMail.MailToSend.IsBodyHtml = true;
            myMail.MailToSend.Body = Body;

            if (fileName.Length > 0)
            {
                Attachment data = new Attachment(fileName);
                System.Net.Mime.ContentDisposition disposition = data.ContentDisposition;
                disposition.CreationDate = System.IO.File.GetCreationTime(fileName);
                disposition.ModificationDate = System.IO.File.GetLastWriteTime(fileName);
                disposition.ReadDate = System.IO.File.GetLastAccessTime(fileName);
                myMail.MailToSend.Attachments.Add(data);
            }


            //if (mySmtpClient.Length <= 0)
            //    return false;

            //SmtpClient client = new SmtpClient();

            //if ((UserName.Length > 0) && (Password.Length > 0))
            //    client.Credentials = new System.Net.NetworkCredential(UserName, Password);

            //client.Send(mail);

            myMail.SendMail(Zeus.Mail.MailSendMode.Sync);

            if (DEBUG_MODE)
                System.Web.HttpContext.Current.Response.Write("<br />DEB: E-Mail inviata");

            return true;
        }
        catch (Exception p)
        {

            if (DEBUG_MODE)
            {
                System.Web.HttpContext.Current.Response.Write(p.ToString());
            }

            return false;
        }

    }//fine SendMail




    private bool SelectFromLogAndUpdateCampaignTotals(string ID1Campaign)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        SqlTransaction transaction = null;
        SqlDataReader reader = null;

        try
        {

            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                transaction = connection.BeginTransaction();
                command.Transaction = transaction;


                decimal TotaleImpression_Value = 0;
                int TotaleImpression_Num = 0;
                decimal TotaleClick_Value = 0;
                int TotaleClick_Num = 0;
                DateTime LastAgentOperationData = DateTime.Now.AddYears(-1);
                decimal Budget_Totale = 0;
                decimal Budget_Saldo = 0;
                

                sqlQuery = "SELECT [TotaleImpression_Value],[TotaleImpression_Num],";
                sqlQuery += "[TotaleClick_Value],[TotaleClick_Num],[LastAgentOperationData],";
                sqlQuery += "[Budget_Totale],[Budget_Saldo]";
                sqlQuery += " FROM [tbAdFarm_Campaigns]";
                sqlQuery += " WHERE [ID1Campaign]=@ID1Campaign";
                
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID1Campaign", SqlDbType.Int);
                command.Parameters["@ID1Campaign"].Value = ID1Campaign;

                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (reader["TotaleImpression_Value"] != DBNull.Value)
                        TotaleImpression_Value = Convert.ToDecimal(reader["TotaleImpression_Value"]);

                    if (reader["TotaleImpression_Num"] != DBNull.Value)
                        TotaleImpression_Num = Convert.ToInt32(reader["TotaleImpression_Num"]);

                    if (reader["TotaleClick_Value"] != DBNull.Value)
                        TotaleClick_Value = Convert.ToDecimal(reader["TotaleClick_Value"]);

                    if (reader["TotaleClick_Num"] != DBNull.Value)
                        TotaleClick_Num = Convert.ToInt32(reader["TotaleClick_Num"]);

                    if (reader["LastAgentOperationData"] != DBNull.Value)
                        LastAgentOperationData =  Convert.ToDateTime(reader["LastAgentOperationData"]);

                    if (reader["Budget_Totale"] != DBNull.Value)
                        Budget_Totale = Convert.ToDecimal(reader["Budget_Totale"]);

                    if (reader["Budget_Saldo"] != DBNull.Value)
                        Budget_Saldo = Convert.ToDecimal(reader["Budget_Saldo"]);

                }//fine while

                reader.Close();

                sqlQuery = "SELECT SUM([MoneyValue])";
                sqlQuery += " FROM [Zeus].[dbo].[tbAdFarm_Tracking]";
                sqlQuery += " WHERE [EventType]='I' AND [ID2Campaign]=@ID1Campaign";
                //sqlQuery += " AND [MoneyValue]>0";
                sqlQuery += " AND DATEDIFF(second,DateEvent,'" + FreezeDate.ToString().Replace(".", ":") + "')>0 ";
                sqlQuery += " AND DATEDIFF(second,'" + LastAgentOperationData.ToString().Replace(".", ":") + "',DateEvent)>0";

                command.CommandText = sqlQuery;
                command.Parameters.Clear();
                command.Parameters.Add("@ID1Campaign", SqlDbType.Int);
                command.Parameters["@ID1Campaign"].Value = ID1Campaign;

                try
                {
                    TotaleImpression_Value += Convert.ToDecimal(command.ExecuteScalar());
                }
                catch (Exception)
                { }


                

                sqlQuery = "SELECT SUM([MoneyValue])";
                sqlQuery += " FROM [Zeus].[dbo].[tbAdFarm_Tracking]";
                sqlQuery += " WHERE [EventType]='C' AND [ID2Campaign]=@ID1Campaign";
                //sqlQuery += " AND [MoneyValue]>0";
                sqlQuery += " AND DATEDIFF(second,DateEvent,'" + FreezeDate.ToString().Replace(".", ":") + "')>0 ";
                sqlQuery += " AND DATEDIFF(second,'" + LastAgentOperationData.ToString().Replace(".", ":") + "',DateEvent)>0";
                command.CommandText = sqlQuery;
                command.Parameters.Clear();
                command.Parameters.Add("@ID1Campaign", SqlDbType.Int);
                command.Parameters["@ID1Campaign"].Value = ID1Campaign;


                try
                {
                    TotaleClick_Value += Convert.ToDecimal(command.ExecuteScalar());
                }
                catch (Exception)
                { }



                sqlQuery = "SELECT COUNT([ID1AdTrack])";
                sqlQuery += " FROM [Zeus].[dbo].[tbAdFarm_Tracking]";
                sqlQuery += " WHERE [EventType]='I' AND [ID2Campaign]=@ID1Campaign";
                //sqlQuery += " AND [MoneyValue]>0";
                sqlQuery += " AND DATEDIFF(second,DateEvent,'" + FreezeDate.ToString().Replace(".", ":") + "')>0 ";
                sqlQuery += " AND DATEDIFF(second,'" + LastAgentOperationData.ToString().Replace(".", ":") + "',DateEvent)>0";
                command.CommandText = sqlQuery;
                command.Parameters.Clear();
                command.Parameters.Add("@ID1Campaign", SqlDbType.Int);
                command.Parameters["@ID1Campaign"].Value = ID1Campaign;


                try
                {
                    TotaleImpression_Num += Convert.ToInt32(command.ExecuteScalar());
                }
                catch (Exception)
                { }


                sqlQuery = "SELECT COUNT([ID1AdTrack])";
                sqlQuery += " FROM [Zeus].[dbo].[tbAdFarm_Tracking]";
                sqlQuery += " WHERE [EventType]='C' AND [ID2Campaign]=@ID1Campaign";
                //sqlQuery += " AND [MoneyValue]>0";
                sqlQuery += " AND DATEDIFF(second,DateEvent,'" + FreezeDate.ToString().Replace(".", ":") + "')>0 ";
                sqlQuery += " AND DATEDIFF(second,'" + LastAgentOperationData.ToString().Replace(".", ":") + "',DateEvent)>0";
                command.CommandText = sqlQuery;
                command.Parameters.Clear();
                command.Parameters.Add("@ID1Campaign", SqlDbType.Int);
                command.Parameters["@ID1Campaign"].Value = ID1Campaign;

                try
                {
                    TotaleClick_Num += Convert.ToInt32(command.ExecuteScalar());
                }
                catch (Exception)
                { }


                Budget_Saldo = Budget_Totale - (TotaleImpression_Value + TotaleClick_Value);

                sqlQuery = "UPDATE tbAdFarm_Campaigns";
                sqlQuery += " SET [TotaleImpression_Value]=@TotaleImpression_Value,";
                sqlQuery += " TotaleImpression_Num=@TotaleImpression_Num,";
                sqlQuery += " TotaleClick_Value=@TotaleClick_Value,";
                sqlQuery += " TotaleClick_Num=@TotaleClick_Num,";
                sqlQuery += " LastAgentOperationData='" + FreezeDate.ToString().Replace(".", ":") + "',";
                sqlQuery += " Budget_Totale=@Budget_Totale,";
                sqlQuery += " Budget_Saldo=@Budget_Saldo";
                sqlQuery += " WHERE [ID1Campaign]=@ID1Campaign";
                
                command.CommandText = sqlQuery;
                command.Parameters.Clear();

                command.Parameters.Add("@TotaleImpression_Value", SqlDbType.Decimal);
                command.Parameters["@TotaleImpression_Value"].Value = TotaleImpression_Value;

                command.Parameters.Add("@TotaleImpression_Num", SqlDbType.Int);
                command.Parameters["@TotaleImpression_Num"].Value = TotaleImpression_Num;

                command.Parameters.Add("@TotaleClick_Value", SqlDbType.Decimal);
                command.Parameters["@TotaleClick_Value"].Value = TotaleClick_Value;

                command.Parameters.Add("@TotaleClick_Num", SqlDbType.Int);
                command.Parameters["@TotaleClick_Num"].Value = TotaleClick_Num;

                command.Parameters.Add("@Budget_Totale", SqlDbType.Decimal);
                command.Parameters["@Budget_Totale"].Value = Budget_Totale;

                command.Parameters.Add("@Budget_Saldo", SqlDbType.Decimal);
                command.Parameters["@Budget_Saldo"].Value = Budget_Saldo;
                
                command.Parameters.Add("@ID1Campaign", SqlDbType.Int);
                command.Parameters["@ID1Campaign"].Value = ID1Campaign;

                command.ExecuteNonQuery();

                transaction.Commit();
                transaction = null;

                return true;

            }//fine Using
        }
        catch (Exception p)
        {

            if (DEBUG_MODE)
                System.Web.HttpContext.Current.Response.Write(p.ToString());


            if (reader != null)
                reader.Close();


            try
            {

                if (transaction != null)
                    transaction.Rollback();
            }
            catch (Exception p2)
            {
                if (DEBUG_MODE)
                    System.Web.HttpContext.Current.Response.Write(p.ToString());
            }

            return false;
        }

    }//fine SelectFromLogAndUpdateCampaignTotals



    private string GetCampaignEndDate(string ID1Campaign)
    {
        string CampaignEndDate = string.Empty;

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        try
        {

            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();


                bool SendAvvisoScadenza_Data = false;

                sqlQuery = "SELECT PWF_Area1,SendAvvisoScadenza_Data";
                sqlQuery += " FROM tbAdFarm_Campaigns";
                sqlQuery += " WHERE ID1Campaign=@ID1Campaign";
                
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID1Campaign",SqlDbType.Int);
                command.Parameters["@ID1Campaign"].Value=ID1Campaign;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (reader["PWF_Area1"] != DBNull.Value)
                        CampaignEndDate = Convert.ToDateTime(reader["PWF_Area1"]).ToString("d");

                    if (reader["SendAvvisoScadenza_Data"] != DBNull.Value)
                        SendAvvisoScadenza_Data = Convert.ToBoolean(reader["SendAvvisoScadenza_Data"]);

                }

                reader.Close();


                if(DEBUG_MODE)
                    System.Web.HttpContext.Current.Response.Write("DEB SendAvvisoScadenza_Data" + SendAvvisoScadenza_Data);

                if (!SendAvvisoScadenza_Data)
                    CampaignEndDate = string.Empty;
              

            }//fine Using
        }
        catch (Exception p)
        {

            System.Web.HttpContext.Current.Response.Write(p.ToString());
          
        }



        return CampaignEndDate;

    }//fine GetCampaignEndDate



    private string GetStatus(string ID1Campaign)
    {
        string Status = string.Empty;

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        try
        {

            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();


                sqlQuery = "SELECT Status";
                sqlQuery += " FROM tbAdFarm_Campaigns";
                sqlQuery += " WHERE ID1Campaign=@ID1Campaign";

                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID1Campaign", SqlDbType.Int);
                command.Parameters["@ID1Campaign"].Value = ID1Campaign;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                    if (reader["Status"] != DBNull.Value)
                        switch (reader["Status"].ToString())
                        {
                            case "ACT":
                                Status = "Attiva";
                                break;

                            case "SAT":
                                Status = "Attiva anche dopo scadenza o sforamento budget";
                                break;
                        }

                reader.Close();



            }//fine Using
        }
        catch (Exception p)
        {

            System.Web.HttpContext.Current.Response.Write(p.ToString());

        }



        return Status;

    }//fine GetStatus


     private bool SetSendAvvisoScadenza_Data(string ID1Campaign)
    {
        string Status = string.Empty;

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        try
        {

            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();


                sqlQuery = " UPDATE tbAdFarm_Campaigns";
                sqlQuery += " SET SendAvvisoScadenza_Data=0";
                sqlQuery += " WHERE ID1Campaign=@ID1Campaign";

                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID1Campaign", SqlDbType.Int);
                command.Parameters["@ID1Campaign"].Value = ID1Campaign;

                command.ExecuteNonQuery();

                return true;

            }//fine Using
        }
        catch (Exception p)
        {

            System.Web.HttpContext.Current.Response.Write(p.ToString());
            return false;
        }



        

    }//fine SetSendAvvisoScadenza_Datastring

    public void DoWork()
    {

        string[][] ActiveCampaigns = (string[][])GetAdFarm_Campaign().ToArray(typeof(string[]));

        if (ActiveCampaigns == null)
            return;
        
        string[] EmailAddess = null;

        string PWF_Area1=string.Empty;

        string ServerName = string.Empty;

        if (System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"] != null)
            ServerName = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"];

        for (int i = 0; i < ActiveCampaigns.Length; ++i)
        {


            PWF_Area1 = GetCampaignEndDate(ActiveCampaigns[i][0]);

            if (PWF_Area1.Length > 0)
            {
                TimeSpan ts = new TimeSpan(DateTime.Now.Ticks - Convert.ToDateTime(PWF_Area1).Ticks);

                if (DEBUG_MODE)
                    System.Web.HttpContext.Current.Response.Write("<br />DEB PWF_Area1:" + PWF_Area1 + " - ts.TotalDays:" + ts.TotalDays);

                if ((ts.TotalDays > -1)
                    && (ts.TotalDays < 0))
                {

                    EmailAddess = (string[])GetEmailAddess(ActiveCampaigns[i][0]).ToArray(typeof(string));
                    string myStatus = GetStatus(ActiveCampaigns[i][0]);

                    for (int j = 0; j < EmailAddess.Length; ++j)
                        SendMail("AdFarm@Zeus.it"
                        , EmailAddess[j]
                        , "Notifica di scadenza.<br />La campagna pubblicitaria "
                        + ActiveCampaigns[i][1]
                        + " sul sito web Zeus scade domani.<br />"
                        + "La campagna è impostata sullo status: \"" + myStatus + "\"."
                        + "<br /><br/ >Link di moderazione:"
                        + " <a href=\"http://" + ServerName + "/Zeus/AdFarm/Campaign_Edt.aspx?XRI=" + ActiveCampaigns[i][0] + "\">"
                        + "http://" + ServerName + "/Zeus/AdFarm/Campaign_Edt.aspx?XRI=" + ActiveCampaigns[i][0] + "</a>"
                        , "Zeus.it"
                        , "Scadenza campagna " + ActiveCampaigns[i][1]
                        , string.Empty
                        , string.Empty
                        , string.Empty
                        , string.Empty);


                    SetSendAvvisoScadenza_Data(ActiveCampaigns[i][0]);
                    System.Web.HttpContext.Current.Response.Write("<br />Inviata E-Mail scadenza campagna.");

                }//fine if
            }//fine if


            if (IsOverData(ActiveCampaigns[i][0]))
            {

                EmailAddess = (string[])GetEmailAddess(ActiveCampaigns[i][0]).ToArray(typeof(string));
              

                for (int j = 0; j < EmailAddess.Length; ++j)
                    SendMail("AdFarm@Zeus.it"
                    , EmailAddess[j]
                    , "Promemoria di scadenza.<br />La campagna pubblicitaria " 
                    + ActiveCampaigns[i][1] 
                    + " sul sito web Zeus scadrà il " + PWF_Area1 + " ."
                    + "<br /><br/ >Link di moderazione:"
                        + " <a href=\"http://" + ServerName + "/Zeus/AdFarm/Campaign_Edt.aspx?XRI=" + ActiveCampaigns[i][0] + "\">"
                        + "http://" + ServerName + "/Zeus/AdFarm/Campaign_Edt.aspx?XRI=" + ActiveCampaigns[i][0] + "</a>"
                    , "Zeus.it"
                    , "Promemoria scadenza campagna " + ActiveCampaigns[i][1]
                    , string.Empty
                    , string.Empty
                    , string.Empty
                    , string.Empty);

                SetSendAvvisoLog_Data(ActiveCampaigns[i][0]);
                //SwitchOffCampaignByDate(ActiveCampaigns[i][0]);

                System.Web.HttpContext.Current.Response.Write("<br />Inviata E-Mail di promemoria di scadenza.");

            }//fine if

            SwitchOffCampaignByDate(ActiveCampaigns[i][0]);


            if (IsUnderBudgetSaldo(ActiveCampaigns[i][0]))
                //||(IsAllowToSendBudgetSaldoEmail(ActiveCampaigns[i][0])))
            {

                EmailAddess = (string[])GetEmailAddess(ActiveCampaigns[i][0]).ToArray(typeof(string));

                for (int j = 0; j < EmailAddess.Length; ++j)
                    SendMail("AdFarm@Zeus.it"
                    , EmailAddess[j]
                    , "Promemoria di sforamento budget saldo.<br /><br /> La campagna pubblicitaria " + ActiveCampaigns[i][1] + " sul sito web Zeus stà per sforare il budget saldo."
                     + "<br /><br/ >Link di moderazione:"
                        + " <a href=\"http://" + ServerName + "/Zeus/AdFarm/Campaign_Edt.aspx?XRI=" + ActiveCampaigns[i][0] + "\">"
                        + "http://" + ServerName + "/Zeus/AdFarm/Campaign_Edt.aspx?XRI=" + ActiveCampaigns[i][0] + "</a>"
                    , "Zeus.it"
                    , "Avviso sforamento budget saldo"
                    , string.Empty
                    , string.Empty
                    , string.Empty
                    , string.Empty);

                SetSendAvvisoLog_BudgetSaldo(ActiveCampaigns[i][0]);
                SwitchOffCampaign(ActiveCampaigns[i][0]);

                System.Web.HttpContext.Current.Response.Write("<br />Inviata E-Mail di promemoria di budget saldo.");
            
            }//fine if





            FreezeDate = DateTime.Now;

            if ((SelectFromLogAndUpdateCampaignTotals(ActiveCampaigns[i][0]))
                &&InsertDataIntoXML(ActiveCampaigns[i][0], ActiveCampaigns[i][1]))
                DeleteAll(ActiveCampaigns[i][0]);
            

        }//fine for

        return;


    }//fine DoWork


    public void AdFarmDoWork()
    {

        //AdFarm myAdFarm = new AdFarm("ADFRM", false);

        if (!GetAllConfigurationData())
            return;

        if (!CheckWorkTime())
            return;

        switch (AgentStatus.ToUpper())
        {
            case "WORK":

                if (!CheckOpSpreadTime())
                {
                    UpdateLastRunStart();
                    DoWork();
                    UpdateLastRunUpdate();
                    UpdateStatus("OKTOT");
                    UpdateLastRunEnd();
                }
                break;

            case "OKTOT":
                UpdateLastRunStart();
                UpdateStatus("WORK");
                DoWork();
                UpdateStatus("OKTOT");
                UpdateLastRunUpdate();
                UpdateLastRunEnd();
                break;


        }//fine switch

    }//fine AdFarmDoWork



}//fine classe
