﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Carrello
/// </summary>
/// 


public class Product
{

    public int Id { get; set; }
    public decimal Prezzo { get; set; }
    public string Articolo { get; set; }
    public string Codice { get; set; }
    public string Marca { get; set; }
    public string Immagine { get; set; }
    public int IvaPercent { get; set; }
    public decimal PrezzoNoIva { get; set; }

    public Product(int id)
    {
        this.Id = id;

        this.Prezzo = 0;
        this.Articolo = string.Empty;


        string strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        try
        {

            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();


                sqlQuery = @"SELECT 
      [Articolo]
      ,'/ZeusInc/Catalogo/Img2/' + Immagine2 as Immagine2
      ,Codice
      ,Brand
      ,[Listino1] + (Listino1 *  (IvaPercent * 0.01))  as Listino1
      ,[Listino1] as PrezzoNoIva
      ,IvaPercent
  FROM [Ares].[dbo].[vwCatalogo_All]
WHERE [ID1Articolo]=@ID1Articolo";

                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID1Articolo", System.Data.SqlDbType.Int);
                command.Parameters["@ID1Articolo"].Value = id;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (reader["Articolo"] != DBNull.Value)
                        this.Articolo = reader["Articolo"].ToString();

                    if (reader["Listino1"] != DBNull.Value)
                        this.Prezzo = Convert.ToDecimal(reader["Listino1"]);

                    if (reader["Immagine2"] != DBNull.Value)
                        this.Immagine = reader["Immagine2"].ToString();

                    if (reader["Codice"] != DBNull.Value)
                        this.Codice = reader["Codice"].ToString();

                    if (reader["Brand"] != DBNull.Value)
                        this.Marca = reader["Brand"].ToString();


                    if (reader["IvaPercent"] != DBNull.Value)
                        this.IvaPercent = Convert.ToInt32(reader["IvaPercent"]);

                    if (reader["PrezzoNoIva"] != DBNull.Value)
                        this.PrezzoNoIva = Convert.ToDecimal(reader["PrezzoNoIva"]);

                }//fine while

                reader.Close();



            }//fine Using
        }
        catch (Exception p)
        {

            System.Web.HttpContext.Current.Response.Write(p.ToString());

        }

    }//fine costruttore



}//fine classe


/*############################################################################################*/
/*############################################################################################*/
/*############################################################################################*/


public class CartItem : IEquatable<CartItem>
{
    #region Properties

    public int Quantity { get; set; }

    private int _productId;
    public int ProductId
    {
        get { return _productId; }
        set
        {
            _product = null;
            _productId = value;
        }
    }

    private Product _product = null;
    public Product Prod
    {
        get
        {
            if (_product == null)
            {
                _product = new Product(ProductId);
            }
            return _product;
        }
    }

    public string Articolo
    {
        get { return Prod.Articolo; }
    }

    public string Codice
    {
        get { return Prod.Codice; }
    }

    public string Marca
    {
        get { return Prod.Marca; }
    }

    public string Immagine
    {
        get { return Prod.Immagine; }
    }

    public decimal UnitPrezzo
    {
        get { return Prod.Prezzo; }
    }

    public decimal IvaPercent
    {
        get { return Prod.IvaPercent; }
    }

    public decimal PrezzoNoIva
    {
        get { return Prod.PrezzoNoIva; }
    }

    

    public decimal TotalPrezzo
    {
        get { return UnitPrezzo * Quantity; }
    }

    #endregion

    public CartItem(int productId)
    {
        this.ProductId = productId;
    }

      public bool Equals(CartItem item)
    {
        return item.ProductId == this.ProductId;
    }
}

/*############################################################################################*/
/*############################################################################################*/
/*############################################################################################*/

public class Carrello
{
    #region Properties

    public List<CartItem> Items { get; private set; }

    #endregion

    #region Singleton Implementation

    public static readonly Carrello Instance;
    static Carrello()
    {
        if (HttpContext.Current.Session["ASPNETCarrello"] == null)
        {
            Instance = new Carrello();
            Instance.Items = new List<CartItem>();
            HttpContext.Current.Session["ASPNETCarrello"] = Instance;
        }
        else
        {
            Instance = (Carrello)HttpContext.Current.Session["ASPNETCarrello"];
        }
    }//fine costruttore

    protected Carrello() { }

    public void Destroy() {

        if(HttpContext.Current.Session["ASPNETCarrello"] != null)
            HttpContext.Current.Session["ASPNETCarrello"] = null;
    }

    #endregion

    public bool SaveCarrello(string ID2Ordine)
    {
        if (Items != null && Items.Count > 0)
        {

            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
            string sqlQuery = string.Empty;

            try
            {

                using (SqlConnection connection = new SqlConnection(strConnessione))
                {

                    connection.Open();
                    SqlCommand command = connection.CreateCommand();

                    int index = 0;

                    foreach (CartItem item in Items)
                    {


                        sqlQuery = @"INSERT INTO [tbStore_OrdiniRighe]
           ([ZeusIdModulo]
           ,[ZeusLangCode]
           ,[ID2Ordine]
           ,[ID2Catalogo]
           ,[CodiceArt]
           ,[ID2Brand]
           ,[Descrizione]
           ,[Quantita]
           ,[QuantitaSpedita]
           ,[PrezzoCad]
           ,[PrezzoTot]
           ,[PrezzoTotNonImponibile]
           ,[ScontoCad]
           ,[ScontoCadPercent]
           ,[ScontoTot]
           ,[IvaCad]
           ,[IvaCadPercent]
           ,[IvaTot]
           ,[PesoCad]
           ,[PesoTot]
           ,[TotaleRiga]
           ,[Ordinamento]
           ,[ZeusId]
           ,[RecordNewUser]
           ,[RecordNewDate]
           ,[RecordEdtUser]
           ,[RecordEdtDate]
           ,[ZeusJolly1]
           ,[ZeusJolly2]
           ,[ZeusIsAlive])
     VALUES (''
           ,''
           ,@ID2Ordine
           ,@ID2Catalogo
           ,@CodiceArt
           ,0
           ,@Descrizione
           ,@Quantita
           ,0
           ,@PrezzoCad
           ,@PrezzoTot
           ,0
           ,0
           ,0
           ,0
           ,@IvaCad
           ,@IvaCadPercent
           ,@IvaTot
           ,0
           ,0
           ,@TotaleRiga
           ,@Ordinamento
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,1)";

                        command.CommandText = sqlQuery;

                        

                        command.Parameters.Add("@IvaCad", System.Data.SqlDbType.Decimal);
                        command.Parameters["@IvaCad"].Value = item.PrezzoNoIva * (item.IvaPercent * 0.01m);

                        command.Parameters.Add("@IvaCadPercent", System.Data.SqlDbType.Decimal);
                        command.Parameters["@IvaCadPercent"].Value = item.IvaPercent;

                        command.Parameters.Add("@IvaTot", System.Data.SqlDbType.Decimal);
                        command.Parameters["@IvaTot"].Value = item.TotalPrezzo * (item.IvaPercent * 0.01m);

                        command.Parameters.Add("@TotaleRiga", System.Data.SqlDbType.Decimal);
                        command.Parameters["@TotaleRiga"].Value = (item.Quantity * item.PrezzoNoIva) + ((item.Quantity * item.PrezzoNoIva) * (item.IvaPercent * 0.01m));

                        command.Parameters.Add("@PrezzoCad", System.Data.SqlDbType.Decimal);
                        command.Parameters["@PrezzoCad"].Value = item.PrezzoNoIva;

                        command.Parameters.Add("@PrezzoTot", System.Data.SqlDbType.Decimal);
                        command.Parameters["@PrezzoTot"].Value = item.Quantity * item.PrezzoNoIva;
                       
                        
                        
                        
                        
                        command.Parameters.Add("@ID2Ordine", System.Data.SqlDbType.Int);
                        command.Parameters["@ID2Ordine"].Value = ID2Ordine;

                        command.Parameters.Add("@ID2Catalogo", System.Data.SqlDbType.Int);
                        command.Parameters["@ID2Catalogo"].Value = item.ProductId;

                        command.Parameters.Add("@CodiceArt", System.Data.SqlDbType.NVarChar);
                        command.Parameters["@CodiceArt"].Value = item.Codice;

                        command.Parameters.Add("@Descrizione", System.Data.SqlDbType.NVarChar);
                        command.Parameters["@Descrizione"].Value = item.Articolo;

                        command.Parameters.Add("@Quantita", System.Data.SqlDbType.Decimal);
                        command.Parameters["@Quantita"].Value = item.Quantity;

                        

                        command.Parameters.Add("@Ordinamento", System.Data.SqlDbType.Int);
                        command.Parameters["@Ordinamento"].Value = index;

                        command.ExecuteNonQuery();
                        command.Parameters.Clear();

                        ++index;

                    }//fine foreach

                    return true;

                }//fine Using
            }
            catch (Exception p)
            {

                System.Web.HttpContext.Current.Response.Write(p.ToString());

            }



        }//fine if

        return false;

    }//fine SaveCarrello


    #region Item Modification Methods
     public void AddItem(int productId)
    {
        CartItem newItem = new CartItem(productId);

         if (Items.Contains(newItem))
        {
            foreach (CartItem item in Items)
            {
                if (item.Equals(newItem))
                {
                    item.Quantity++;
                    return;
                }
            }
        }
        else
        {
            newItem.Quantity = 1;
            Items.Add(newItem);
        }
    }//fine AddItem


    public void RemItem(int productId)
    {
        CartItem newItem = new CartItem(productId);

        if (Items.Contains(newItem))
            foreach (CartItem item in Items)
                if (item.Equals(newItem))
                {
                    if (item.Quantity > 1)
                        item.Quantity--;
                    return;
                }

    }//fine RemItem

    public bool HasItem(int productId)
    {
        CartItem newItem = new CartItem(productId);

        return Items.Contains(newItem);

    }//fine HasItem

     public void SetItemQuantity(int productId, int quantity)
     {
        if (quantity == 0)
        {
            RemoveItem(productId);
            return;
        }

        CartItem updatedItem = new CartItem(productId);

        foreach (CartItem item in Items)
        {
            if (item.Equals(updatedItem))
            {
                item.Quantity = quantity;
                return;
            }
        }
    }//fine SetItemQuantity




    public void RemoveItem(int productId)
    {
        CartItem removedItem = new CartItem(productId);
        Items.Remove(removedItem);
    }//fine RemoveItem
    #endregion

    #region Reporting Methods
    public decimal GetSubTotal()
    {
        decimal subTotal = 0;
        foreach (CartItem item in Items)
            subTotal += item.TotalPrezzo;

        return subTotal;
    }//fine GetSubTotal
    #endregion

}//fine classe