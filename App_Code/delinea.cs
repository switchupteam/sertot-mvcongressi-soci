﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;


public class delinea
{

    public string[] extension = { ".jpeg", ".gif", ".jpg" };
    public int MAXSIZE = 1000000000;
    public int MAXWITH = 800;
    public int MAXHEIGHT = 600;

    public delinea()
    {
    }

    ~delinea()
    {
    }

    public string CheckFileName(string filePath, string fileName)
    {

        int counter = 1;
        string finalPath = filePath;
        string strFile_name = System.IO.Path.GetFileNameWithoutExtension(fileName);
        string strFile_extension = System.IO.Path.GetExtension(fileName);

        if (System.IO.File.Exists(filePath + fileName))
        {

            // il file è già sul server
            filePath += fileName;
            while (System.IO.File.Exists(filePath))
            {
                counter++;
                filePath = System.IO.Path.Combine(finalPath, strFile_name + counter.ToString() + strFile_extension);

            }//fine while


            fileName = strFile_name + counter.ToString() + strFile_extension;


        }
        //else fileName =strFile_name;

        return fileName;

    }

    public Control FindControlRecursive(Control Root, string Id)
    {
        if (Root.ID == Id)
            return Root;

        foreach (Control Ctl in Root.Controls)
        {
            Control FoundCtl = FindControlRecursive(Ctl, Id);
            if (FoundCtl != null)
                return FoundCtl;
        }

        return null;
    }

    #region AntiSQLInjection

    public static bool IsQueryStringSecure(string TexttoValidate)
    {
        string TextVal;

        TextVal = TexttoValidate;

        bool IsSecure = true;

        //Build an array of characters that need to be filter.
        string[] strDirtyQueryString = { "xp_", ";", "--", "<", ">", "script", "iframe", "delete", "drop", "exec", "=" };

        //Loop through all items in the array
        foreach (string item in strDirtyQueryString)
        {
            if (TextVal.IndexOf(item) != -1)
            {
                IsSecure = false;
            }
        }


        //Loop through all items in the array with decode
        foreach (string item in strDirtyQueryString)
        {
            if (TextVal.IndexOf(HttpContext.Current.Server.HtmlDecode(item)) != -1)
            {
                IsSecure = false;
            }
        }

        return IsSecure;
    }

    public bool AntiSQLInjectionLeft(System.Collections.Specialized.NameValueCollection QueryString, string param)
    {
        if (QueryString.Count == 0)
            return false;

        //String[] myStrArr = new String[QueryString.Count];
        //QueryString.CopyTo(myStrArr, 0);
        //QueryString.

        foreach (string item in QueryString.AllKeys)
        {
            if (item != null)
            {
                if (item.ToLower().Equals(param.ToLower())) // && IsQueryStringSecure(QueryString[param])
                    return true;
            }
        }

        return false;
    }//fine AntiSQLInjectionLeft

    public bool AntiSQLInjectionRight(System.Collections.Specialized.NameValueCollection QueryString, string param)
    {

        if (QueryString.Count == 0)
            return false;

        //String[] myStrArr = new String[QueryString.Count];
        //QueryString.CopyTo(myStrArr, 0);
        //QueryString.

        foreach (string item in QueryString.AllKeys)
        {
            if (item != null)
            {
                if (item.ToLower().Equals(param.ToLower()) && IsQueryStringSecure(QueryString[param]))
                {
                    switch (param)
                    {

                        case "ReturnUrl":
                            return true;
                            break;

                        case "Lang":
                            if (QueryString["Lang"].Length <= 4)
                            {
                                switch (QueryString["Lang"].ToUpper())
                                {
                                    case "ITA":
                                        return true;
                                        break;

                                    case "ENG":
                                        return true;
                                        break;

                                    case "FRA":
                                        return true;
                                        break;

                                    default:
                                        return false;
                                        break;
                                }//fine switch

                            }//fine if

                            break;


                        case "XRI":
                            try
                            {
                                int var = Convert.ToInt32(QueryString["XRI"].ToString());
                                return true;
                            }
                            catch (Exception)
                            {
                            }

                            break;

                        case "XRE":
                            try
                            {
                                int var = Convert.ToInt32(QueryString["XRE"].ToString());
                                return true;
                            }
                            catch (Exception)
                            {
                            }
                            break;


                    }


                }
            }
        }

        return false;

    }//fine AntiSQLInjectionRight

    public bool AntiSQLInjection(System.Collections.Specialized.NameValueCollection QueryString)
    {

        if (QueryString.Count == 0)
            return false;

        bool myReturn = false;


        foreach (string item in QueryString.AllKeys)
        {
            if (item != null)
            {


                switch (item)
                {

                    case "ReturnUrl":
                        return true;
                        break;

                    case "Lang":
                        if (QueryString["Lang"].Length <= 3)
                            myReturn = true;
                        else return false;
                        break;

                    case "LangO":
                        if (QueryString["LangO"].Length <= 3)
                            myReturn = true;
                        else return false;
                        break;

                    case "LangD":
                        if (QueryString["LangD"].Length <= 3)
                            myReturn = true;
                        else return false;
                        break;


                    case "XRI":
                        try
                        {
                            int var = Convert.ToInt32(QueryString["XRI"].ToString());
                            myReturn = true;
                        }
                        catch (Exception)
                        {
                            return false;
                        }

                        break;

                    case "XRI1":
                        try
                        {
                            int var = Convert.ToInt32(QueryString["XRI1"].ToString());
                            myReturn = true;
                        }
                        catch (Exception)
                        {
                            return false;
                        }

                        break;

                    case "XRI2":
                        try
                        {
                            int var = Convert.ToInt32(QueryString["XRI2"].ToString());
                            myReturn = true;
                        }
                        catch (Exception)
                        {
                            return false;
                        }

                        break;


                    case "XRE":
                        myReturn = true;
                        break;

                    case "ZID":
                        try
                        {
                            Guid var = new Guid(QueryString["ZID"].ToString());
                            myReturn = true;
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                        break;

                    case "UID":
                        try
                        {
                            Guid var = new Guid(QueryString["UID"].ToString());
                            myReturn = true;
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                        break;

                    case "ZIM":
                        if (QueryString["ZIM"].Length <= 5)
                            myReturn = true;
                        else return false;
                        break;

                    default:
                        return false;
                        break;

                }//fine switch


            }//fine if
        }

        return myReturn;

    }//fine AntiSQLInjection

    public bool AntiSQLInjectionNew(System.Collections.Specialized.NameValueCollection QueryString, string param, string Type)
    {

        if (QueryString.Count == 0)
            return false;

        if (param.Length == 0)
            return false;

        if (Type.Length == 0)
            return false;

        bool myReturn = false;



        switch (Type.ToLower())
        {

            case "int":
                try
                {
                    if (QueryString[param].Length > 0)
                        Convert.ToInt32(QueryString[param]);

                    myReturn = true;
                }
                catch (Exception)
                { }
                break;

            case "uid":
                try
                {
                    if (QueryString[param].Length > 0)
                        new Guid(QueryString[param]);

                    myReturn = true;
                }
                catch (Exception)
                { }
                break;

            case "string":
                try
                {
                    if (QueryString[param].Length > 0)
                        QueryString[param].ToString();

                    myReturn = true;
                }
                catch (Exception)
                { }
                break;


        }//fine switch



        return myReturn;

    }//fine AntiSQLInjectionNew

    #endregion

    /* Used in /Zeus/Account/Account_Dtl.ascx */
    public bool CheckIsAdminManaged(string GUID)
    {

        bool IsAdminManaged = false;

        try
        {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(
               strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT IsAdminManaged FROM vwAdminUsersInRoles WHERE UserId=@UserId";// " AND RoleName ='GestioneUtenti' OR  RoleName ='GestioneUtentiAdmin'";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@UserId"].Value = new Guid(GUID);

                SqlDataReader reader = command.ExecuteReader();

                if (!reader.HasRows)
                {
                    reader.Close();
                    return IsAdminManaged;
                }

                while (reader.Read())
                    if (reader.GetBoolean(0))
                    {
                        IsAdminManaged = true;
                        break;
                    }

                reader.Close();

                if (IsAdminManaged)
                {
                    bool IsAdminRole = false;

                    sqlQuery = "SELECT IsAdminRole FROM vwAdminUsersInRoles WHERE UserId=@UserId";// " AND RoleName ='GestioneUtenti' OR  RoleName ='GestioneUtentiAdmin'";
                    command.CommandText = sqlQuery;
                    command.Parameters.Clear();
                    command.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
                    command.Parameters["@UserId"].Value = new Guid(Membership.GetUser(true).ProviderUserKey.ToString());

                    reader = command.ExecuteReader();

                    while (reader.Read())
                        if (reader.GetBoolean(0))
                        {
                            IsAdminRole = true;
                            break;
                        }

                    reader.Close();

                    IsAdminManaged = !IsAdminRole;

                }//fine if

            }//fine using
        }
        catch (Exception p)
        {
            System.Web.HttpContext.Current.Response.Write(p.ToString());

        }

        return IsAdminManaged;

    }//fine CheckIsAdminManaged

    public string myHtmlEncode(string HTML)
    {
        HTML = HTML.Replace("è", "e");
        HTML = HTML.Replace("é", "e");
        HTML = HTML.Replace("ê", "e");
        HTML = HTML.Replace("ë", "e");
        HTML = HTML.Replace("ò", "o");
        HTML = HTML.Replace("ô", "o");
        HTML = HTML.Replace("ù", "u");
        HTML = HTML.Replace("û", "u");
        HTML = HTML.Replace("ü", "u");
        HTML = HTML.Replace("à", "a");
        HTML = HTML.Replace("â", "a");
        HTML = HTML.Replace("ì", "i");
        HTML = HTML.Replace("î", "i");
        HTML = HTML.Replace("ï", "i");
        HTML = HTML.Replace("ç", "c");
        //HTML = HTML.Replace("_", string.Empty);
        HTML = HTML.Replace(" ", "-");
        HTML = HTML.Replace("/", "_");
        HTML = System.Text.RegularExpressions.Regex.Replace(HTML, @"[^\w-]+", string.Empty);
        return HTML;
    }

    public string CropSentance(string textToCrop, int desiredLength, string trail)
    {
        int iPos = -1;
        int iMax;
        string sOut = "";

        //Set the max length to be the desired length minus 
        //the length of the trailing characters.
        iMax = desiredLength - trail.Length;


        //Check if the max length is less than or equal to zero.
        if (iMax <= 0)
        {
            // Return an empty string
            return "";
        }
        else if (textToCrop.Length <= desiredLength)
        {
            // Return the original text as the text fits within the desired length
            return textToCrop;
        }
        else
        {
            // Remove the end of the text
            sOut = textToCrop.Substring(0, desiredLength);

            // Get the position of the last space in the text
            iPos = sOut.LastIndexOf(" ");

            //Return the cropped sentance
            return sOut.Substring(0, iPos + 1) + trail;
        }
    }//fine CropSentance

    public string UppercaseFirst(string s)
    {
        // Check for empty string.
        if (string.IsNullOrEmpty(s))
        {
            return string.Empty;
        }
        // Return char and concat substring.
        return char.ToUpper(s[0]) + s.Substring(1);
    }//fine UppercaseFirst

    public Boolean IsRenewalTime()
    {
        int GetLastYear = GetLastRenewYear();
        if (RenewalTime(1, 12, 30, 11))    //Controllo data
        {

            if ((GetLastYear <= DateTime.Now.Year) && (GetLastYear > 0))
                return true;

        }
        //if (RenewalTime(1, 1, 15, 5))      // Data limite di rinnovo quota    
        //{

        //    if ((GetLastYear < DateTime.Now.Year) && (GetLastYear > 0))
        //        return true;

        //}
        return false;
    }

    public Boolean IsRenewalTimeStart()
    {
        int GetLastYear = GetLastRenewYear();
        if (RenewalTime(1, 12))    //Controllo data
        {

            if ((GetLastYear <= DateTime.Now.Year) && (GetLastYear > 0))
                return true;

        }
        //if (RenewalTime(1, 1, 15, 5))      // Data limite di rinnovo quota    
        //{

        //    if ((GetLastYear < DateTime.Now.Year) && (GetLastYear > 0))
        //        return true;

        //}
        return false;
    }


    public int GetLastRenewYear()
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;
        int myReturn = 0;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {


            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();


                sqlQuery = "SELECT Anno FROM vwSoci_UltimoPagamento WHERE UserId=@UserId";
                command.CommandText = sqlQuery;

                MembershipUser user = Membership.GetUser();

                command.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@UserId"].Value = user.ProviderUserKey;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (reader["Anno"] != null)
                        myReturn = Convert.ToInt32(reader["Anno"]);
                }
                reader.Close();
            }
            catch (Exception p)
            {
                //Response.Write(p.ToString());
            }
        }//fine Using


        return myReturn;

    }//fine

    public int GetRenewYear(int anno)
    {
        if (DateTime.Now >= new DateTime(anno, 11, 1) && DateTime.Now <= new DateTime(anno, 12, 31))
            return anno + 1;
        else
            return anno;        
    }//fine

    public Boolean RenewalTime(int startDay, int StartMonth, int endDay, int endMonth)
    {
        DateTime dateToday = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
        DateTime dateStart = new DateTime(DateTime.Now.Year, StartMonth, startDay, 0, 0, 0);
        DateTime dateEnd = new DateTime(DateTime.Now.Year, endMonth, endDay, 0, 0, 0);
        int result = DateTime.Compare(dateToday, dateStart);

        if (result < 0)
            return false;
        else
        {
            result = DateTime.Compare(dateToday, dateEnd);
            if (result <= 0)
                return true;
        }
        return false;
    }

    public Boolean RenewalTime(int startDay, int StartMonth)
    {
        DateTime dateToday = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
        DateTime dateStart = new DateTime(DateTime.Now.Year, StartMonth, startDay, 0, 0, 0);
        int result = DateTime.Compare(dateToday, dateStart);

        if (result < 0)
            return false;
        else
            return true;
    }

    public string GetLang(System.Collections.Specialized.NameValueCollection QueryString)
    {
        string defaultLang = "ITA";

        if (QueryString.Count == 0)
            return defaultLang;

        //String[] myStrArr = new String[QueryString.Count];
        //QueryString.CopyTo(myStrArr, 0);
        //QueryString.

        foreach (string item in QueryString.AllKeys)
        {
            if (item != null)
            {
                if (item.ToUpper().Equals("LANG"))
                {
                    if (QueryString[item.ToString()].Length == 3)
                    {
                        if (QueryString[item.ToString()].ToUpper().Equals("ENG"))
                            return "ENG";
                        else
                            return "ITA";

                    }
                    else
                    {
                        return defaultLang;
                    }
                }
            }
        }

        return defaultLang;

    }//fine GetLang

}//fine classe
