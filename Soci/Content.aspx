﻿<%@ Page Language="C#" MasterPageFile="~/Master/LayoutMain.master" CodeFile="Content.aspx.cs" Inherits="Soci_Content" %>

<asp:Content ID="Content2" ContentPlaceHolderID="AreaTitle" runat="Server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="AreaContentMain" runat="Server">
    <section class="sociDettaglio">
        <div class="container">
            <h2 class="titolo"><%=Nome %> <%=Cognome %> <span class="onorario"><%= TestoSocioOnorario %></span></h2>
            <hr />

            <div class="boxDati">
                <div class="row align-items-center">
                    <div id="mdImage" runat="server" class="col-md-3">
                        <div class="immagine">
                            <img src="<%= Photo %>" alt="<%= PhotoAlt %>" class="img-fluid" />
                        </div>
                    </div>

                    <div id="mdData" runat="server" class="col">
                        <div class="elencoDati">
                            <div class="row">
                                <div class="col-sm-6 col-md-4 col-lg-3">
                                    <p class="titolo">Titolo</p>
                                    <p class="valore"><%=Qualifica %></p>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-3">
                                    <p class="titolo">Nome</p>
                                    <p class="valore"><%=Nome %></p>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-3">
                                    <p class="titolo">Cognome</p>
                                    <p class="valore"><%=Cognome %></p>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-3">
                                    <p class="titolo">Provincia</p>
                                    <p class="valore"><%=Provincia %></p>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-3">
                                    <p class="titolo">Indirizzo</p>
                                    <p class="valore"><%=Indirizzo %></p>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-3">
                                    <p class="titolo">Telefono</p>
                                    <p class="valore"><%=Telefono %></p>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-6">
                                    <p class="titolo">Email</p>
                                    <p class="valore"><%=Email %></p>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-6">
                                    <p class="titolo">Affiliazione</p>
                                    <p class="valore"><%=Affiliazione %></p>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-6">
                                    <p class="titolo">Sito Web</p>
                                    <p class="valore"><%= HttpUtility.HtmlDecode(SitoWeb) %></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <asp:PlaceHolder runat="server" ID="CurriculumPlaceholder" Visible="false">
                <div class="boxContenuti">
                    <h4 class="titolo">Curriculum Vitae</h4>
                    <hr />

                    <p>
                        <%= HttpUtility.HtmlDecode(Curriculum) %>
                    </p>
                </div>
            </asp:PlaceHolder>
        </div>
    </section>
</asp:Content>
