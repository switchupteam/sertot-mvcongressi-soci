﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Soci_Ordinari : System.Web.UI.Page
{
    protected string totPagineSoci = string.Empty;
    protected string paginaCorrenteSoci = string.Empty;

    string strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {        
        /*****************************************************/
        /**************** INIZIO SEO *************************/
        /*****************************************************/
        HtmlMeta description = new HtmlMeta();
        HtmlMeta keywords = new HtmlMeta();
        description.Name = "description";
        keywords.Name = "keywords";
        Page.Title = "Soci Ordinari | " + AresConfig.NomeSito;
        description.Content = "Soci Ordinari";
        keywords.Content = "Soci Ordinari";
        HtmlHead head = Page.Header;
        head.Controls.Add(description);
        head.Controls.Add(keywords);
        /*****************************************************/
        /**************** FINE SEO ***************************/
        /*****************************************************/

        if (Page.IsPostBack)
            return;


        int XRI = 0;
        int.TryParse(Request.QueryString["XRI"], out XRI);
        //SetRegionTitle(XRI);
        //BindCurrentRegion(XRI);


        //SET REGIONI        
        List<ListItem> regioni = new List<ListItem>();
        regioni.Add(new ListItem("Tutte le regioni", ""));

        string sqlQuery = string.Empty;
        using (SqlConnection connection = new SqlConnection(strConnessione))
        {
            connection.Open();
            SqlCommand command = connection.CreateCommand();

            sqlQuery = @"SELECT Regione 
                        FROM tbRegioni";
            command.CommandText = sqlQuery;

            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
                regioni.Add(new ListItem(reader["Regione"].ToString(), reader["Regione"].ToString()));

            reader.Close();
            connection.Close();
        }


        Dropdown_Regione.DataTextField = "Text";
        Dropdown_Regione.DataValueField = "Value";
        Dropdown_Regione.DataSource = regioni;
        Dropdown_Regione.DataBind();



        if (Request.QueryString["page"] != null)
        {
            if (Session["NomeSelezionato"] != null)
                TextBox_Nome.Text = Session["NomeSelezionato"].ToString();

            if (Session["RegioneSelezionata"] != null)
                Dropdown_Regione.Text = Session["RegioneSelezionata"].ToString();

            FiltraListaSoci();
        }
        else
        {
            Session["NomeSelezionato"] = null;
            Session["RegioneSelezionata"] = null;
        }

    }



    //private void SetRegionTitle(int XRI)
    //{
    //    string sqlQuery = string.Empty;
    //    using (SqlConnection connection = new SqlConnection(strConnessione))
    //    {
    //        connection.Open();
    //        SqlCommand command = connection.CreateCommand();

    //        sqlQuery = @"SELECT Regione FROM tbRegioni 
    //                        WHERE ID1Regione = @ID1Regione ";

    //        command.CommandText = sqlQuery;
    //        command.Parameters.Add("@ID1Regione", System.Data.SqlDbType.Int);
    //        command.Parameters["@ID1Regione"].Value = XRI;

    //        SqlDataReader reader = command.ExecuteReader();

    //        if (reader.Read())
    //        {
    //            litRegionName.Text = reader["Regione"].ToString();
    //        };
    //        reader.Close();
    //        connection.Close();
    //    }

    //}

    //private void BindCurrentRegion(int XRI)
    //{
    //    if (XRI == 0)
    //    {
    //        //Show placeholder Select a region;
    //        phNoRegionSelected.Visible = true;
    //        return;
    //    }
    //    //Load Provincie

    //    string sqlQuery = string.Empty;
    //    var provincie = new List<ProvinciaItem>();
    //    using (SqlConnection connection = new SqlConnection(strConnessione))
    //    {
    //        connection.Open();
    //        SqlCommand command = connection.CreateCommand();

    //        sqlQuery = @"SELECT Provincia, Sigla, Regione FROM tbProvince
    //                        INNER JOIN tbRegioni on tbRegioni.ID1Regione = tbProvince.ID2Regione
    //                        WHERE tbProvince.ID2Regione= @ID2Regione
    //                        ORDER BY Provincia ";

    //        command.CommandText = sqlQuery;
    //        command.Parameters.Add("@ID2Regione", System.Data.SqlDbType.Int);
    //        command.Parameters["@ID2Regione"].Value = XRI;

    //        SqlDataReader reader = command.ExecuteReader();

    //        while (reader.Read())
    //        {
    //            var newProvincia = new ProvinciaItem()
    //            {
    //                Provincia = reader["Provincia"].ToString(),
    //                Sigla = reader["Sigla"].ToString(),
    //                Regione = reader["Regione"].ToString()
    //            };

    //            newProvincia.SociAggregati = GetSociByType(SocioItem.SocioType.AGG, reader["Sigla"].ToString());
    //            newProvincia.SociOrdinari = GetSociByType(SocioItem.SocioType.ORD, reader["Sigla"].ToString());
    //            if (newProvincia.SociAggregati.Any() | newProvincia.SociOrdinari.Any())
    //            {
    //                provincie.Add(newProvincia);
    //            }
    //        }
    //        reader.Close();
    //        connection.Close();
    //    }

    //    //var rightProvincie = provincie.Where((c, i) => i % 2 != 0).ToList();
    //    //var leftProvincie = provincie.Where((c, i) => i % 2 == 0).ToList();

    //    //rptLeftProvincie.DataSource = leftProvincie;
    //    //rptLeftProvincie.DataBind();
    //    //rptRightProvincie.DataSource = rightProvincie;
    //    //rptRightProvincie.DataBind();

    //    if (!provincie.Any())
    //    {
    //        phNoMembers.Visible = true;
    //    }

    //}

    //private List<SocioItem> GetSociByType(SocioItem.SocioType tipoSocio, string Provincia)
    //{
    //    string sqlQuery = string.Empty;
    //    var soci = new List<SocioItem>();
    //    using (SqlConnection connection = new SqlConnection(strConnessione))
    //    {
    //        connection.Open();
    //        SqlCommand command = connection.CreateCommand();

    //        sqlQuery = @"SELECT Cognome, vwxSoci.Nome, LoweredEmail, Identificativo, Qualifica, Professione, ProvinciaSigla, vwxSoci.UserId, QUOTE.Nome
    //                    FROM vwxSoci
    //                     INNER JOIN tbProvince ON Sigla = ProvinciaSigla
    //                     OUTER APPLY(
    //                      SELECT TOP 1 tbQuote.Nome, tbQuoteAnnuali.DataVerifica
    //                      FROM tbQuoteAnnuali 
    //                       INNER JOIN tbQuote ON tbQuoteAnnuali.ID2QuotaCosto = tbQuote.IDQuota
    //                      WHERE tbQuoteAnnuali.UserId = vwxSoci.UserId 
    //                       AND tbQuote.ZeusIsAlive = 1
    //                      ORDER BY Anno DESC, AnnoProgressivo DESC
    //                     ) AS QUOTE
    //                    WHERE Sigla = @SiglaProvincia AND QUOTE.Nome = @TipoSocio  AND (QUOTE.DataVerifica IS NOT NULL AND QUOTE.DataVerifica < GETDATE())";
    //        command.CommandText = sqlQuery;
    //        command.Parameters.Add("@SiglaProvincia", System.Data.SqlDbType.VarChar);
    //        command.Parameters["@SiglaProvincia"].Value = Provincia;
    //        command.Parameters.Add("@TipoSocio", System.Data.SqlDbType.VarChar);
    //        command.Parameters["@TipoSocio"].Value = tipoSocio.ToString();

    //        SqlDataReader reader = command.ExecuteReader();

    //        while (reader.Read())
    //        {
    //            var socio = new SocioItem()
    //            {
    //                UserId = reader["UserId"].ToString(),
    //                FullName = string.Concat(reader["Qualifica"], " ", reader["Nome"], " ", reader["Cognome"])
    //            };
    //            soci.Add(socio);
    //        }
    //        reader.Close();
    //        connection.Close();


    //    }
    //    return soci;
    //}

    //public class ProvinciaItem
    //{
    //    private List<SocioItem> _sociItems = new List<SocioItem>();
    //    private List<SocioItem> _sociAggregati = new List<SocioItem>();

    //    public string Regione { get; set; }
    //    public string Provincia { get; set; }
    //    public string Sigla { get; set; }


    //    public List<SocioItem> SociOrdinari
    //    {
    //        get { return _sociItems; }
    //        set { _sociItems = value; }
    //    }
    //    public List<SocioItem> SociAggregati
    //    {
    //        get { return _sociAggregati; }
    //        set { _sociAggregati = value; }
    //    }


    //}

    //public class SocioItem
    //{
    //    public enum SocioType { ORD, AGG }
    //    public string UserId { get; set; }
    //    public string FullName { get; set; }
    //}

    public class Socio
    {
        public enum SocioType { ORD, AGG }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string Photo { get; set; }
        public string PhotoAlt { get; set; }
    }


    protected void Cerca_Button_Click(object sender, EventArgs e)
    {
        FiltraListaSoci();
    }

    private void FiltraListaSoci()
    {
        //if (!string.IsNullOrWhiteSpace(TextBox_Nome.Text) || !string.IsNullOrWhiteSpace(Dropdown_Regione.Text))
        //{
        List<Socio> listaSoci = new List<Socio>();


        string sqlQuery = string.Empty;
        using (SqlConnection connection = new SqlConnection(strConnessione))
        {
            connection.Open();
            SqlCommand command = connection.CreateCommand();

            //sqlQuery = string.Format(@"SELECT Cognome, vwxSoci.Nome, LoweredEmail, Identificativo, Qualifica, Professione, vwxSoci.UserId, QUOTE.Nome, RuoloUtente.TestoSocioOnorario {0}
            //                FROM vwxSoci
            //                    {1}
	           //                 OUTER APPLY (
		          //                  SELECT Top (1) CASE WHEN aspnet_Roles.RoleName = 'SociOnorari' THEN 'Socio Onorario' ELSE '' END AS TestoSocioOnorario
		          //                  FROM aspnet_Roles
			         //                   INNER JOIN aspnet_UsersInRoles ON aspnet_UsersInRoles.UserId = vwxSoci.UserId
		          //                  WHERE aspnet_Roles.RoleId = aspnet_UsersInRoles.RoleId 	
		          //                  ORDER BY TestoSocioOnorario DESC
	           //                 ) AS RuoloUtente
            //                    OUTER APPLY(
            //                        SELECT TOP 1 tbQuote.Nome, tbQuoteAnnuali.DataVerifica
            //                        FROM tbQuoteAnnuali
            //                            INNER JOIN tbQuote ON tbQuoteAnnuali.ID2QuotaCosto = tbQuote.IDQuota
            //                        WHERE tbQuoteAnnuali.UserId = vwxSoci.UserId
            //                            AND tbQuote.ZeusIsAlive = 1
            //                        ORDER BY Anno DESC, AnnoProgressivo DESC
	           //                 ) AS QUOTE
            //                WHERE(QUOTE.DataVerifica IS NOT NULL AND QUOTE.DataVerifica < GETDATE())
            //                    AND QUOTE.Nome = 'ORD'
            //                    {2} {3}

            //                EXCEPT

            //                SELECT Cognome, vwxSoci.Nome, LoweredEmail, Identificativo, Qualifica, Professione, vwxSoci.UserId, Nome = 'ORD', RuoloUtente.TestoSocioOnorario
            //                FROM vwxSoci
            //                    {1}
	           //                 OUTER APPLY (
		          //                  SELECT TOP (1) CASE WHEN aspnet_Roles.RoleName = 'SociOnorari' THEN 'Socio Onorario' ELSE '' END AS TestoSocioOnorario
		          //                  FROM aspnet_Roles
			         //                   INNER JOIN aspnet_UsersInRoles ON aspnet_UsersInRoles.UserId = vwxSoci.UserId
		          //                  WHERE aspnet_Roles.RoleId = aspnet_UsersInRoles.RoleId 	
		          //                  ORDER BY TestoSocioOnorario DESC
	           //                 ) AS RuoloUtente	
            //                WHERE RuoloUtente.TestoSocioOnorario = 'Socio Onorario'
            //                    {2} {3}

            //                UNION

            //                SELECT Cognome, vwxSoci.Nome, LoweredEmail, Identificativo, Qualifica, Professione, vwxSoci.UserId, Nome = '', RuoloUtente.TestoSocioOnorario
            //                FROM vwxSoci
            //                    {1}
	           //                 OUTER APPLY (
		          //                  SELECT TOP (1) CASE WHEN aspnet_Roles.RoleName = 'SociOnorari' THEN 'Socio Onorario' ELSE '' END AS TestoSocioOnorario
		          //                  FROM aspnet_Roles
			         //                   INNER JOIN aspnet_UsersInRoles ON aspnet_UsersInRoles.UserId = vwxSoci.UserId
		          //                  WHERE aspnet_Roles.RoleId = aspnet_UsersInRoles.RoleId 	
		          //                  ORDER BY TestoSocioOnorario DESC
	           //                 ) AS RuoloUtente	
            //                WHERE RuoloUtente.TestoSocioOnorario = 'Socio Onorario'
            //                    {2} {3}
            //                ORDER BY Cognome";

            sqlQuery = string.Format(@"SELECT vwxSoci.Cognome, vwxSoci.Nome, LoweredEmail, Identificativo, vwxSoci.Qualifica, vwxSoci.Professione, vwxSoci.UserId, QUOTE.Nome {0}, tbProfiliPersonali.ImmagineSocio, tbProfiliPersonali.ImmagineSocioAlt
                            FROM vwxSoci
                                {1}
                                OUTER APPLY(
                                    SELECT TOP 1 tbQuote.Nome, tbQuoteAnnuali.DataVerifica
                                    FROM tbQuoteAnnuali
                                        INNER JOIN tbQuote ON tbQuoteAnnuali.ID2QuotaCosto = tbQuote.IDQuota
                                    WHERE tbQuoteAnnuali.UserId = vwxSoci.UserId
                                        AND tbQuote.ZeusIsAlive = 1
                                    ORDER BY Anno DESC, AnnoProgressivo DESC
	                            ) AS QUOTE
                                INNER JOIN tbProfiliPersonali ON tbProfiliPersonali.UserId = vwxSoci.UserId
                            WHERE(QUOTE.DataVerifica IS NOT NULL AND QUOTE.DataVerifica < GETDATE())
                                AND QUOTE.Nome = 'ORD'
                                {2} {3}

                            EXCEPT

                            SELECT vwxSoci.Cognome, vwxSoci.Nome, LoweredEmail, Identificativo, vwxSoci.Qualifica, vwxSoci.Professione, vwxSoci.UserId, Nome = 'ORD' {0}, tbProfiliPersonali.ImmagineSocio, tbProfiliPersonali.ImmagineSocioAlt
                            FROM vwxSoci
                                {1}
	                            OUTER APPLY (
		                            SELECT TOP (1) CASE WHEN aspnet_Roles.RoleName = 'SociOnorari' THEN 'Socio Onorario' ELSE '' END AS TestoSocioOnorario
		                            FROM aspnet_Roles
			                            INNER JOIN aspnet_UsersInRoles ON aspnet_UsersInRoles.UserId = vwxSoci.UserId
		                            WHERE aspnet_Roles.RoleId = aspnet_UsersInRoles.RoleId 	
		                            ORDER BY TestoSocioOnorario DESC
	                            ) AS RuoloUtente	
                                INNER JOIN tbProfiliPersonali ON tbProfiliPersonali.UserId = vwxSoci.UserId
                            WHERE RuoloUtente.TestoSocioOnorario = 'Socio Onorario'
                                {2} {3}

                            UNION

                            SELECT vwxSoci.Cognome, vwxSoci.Nome, LoweredEmail, Identificativo, vwxSoci.Qualifica, vwxSoci.Professione, vwxSoci.UserId, Nome = '' {0}, tbProfiliPersonali.ImmagineSocio, tbProfiliPersonali.ImmagineSocioAlt
                            FROM vwxSoci
                                {1}
	                            OUTER APPLY (
		                            SELECT TOP (1) CASE WHEN aspnet_Roles.RoleName = 'SociOnorari' THEN 'Socio Onorario' ELSE '' END AS TestoSocioOnorario
		                            FROM aspnet_Roles
			                            INNER JOIN aspnet_UsersInRoles ON aspnet_UsersInRoles.UserId = vwxSoci.UserId
		                            WHERE aspnet_Roles.RoleId = aspnet_UsersInRoles.RoleId 	
		                            ORDER BY TestoSocioOnorario DESC
	                            ) AS RuoloUtente	
                                INNER JOIN tbProfiliPersonali ON tbProfiliPersonali.UserId = vwxSoci.UserId
                            WHERE RuoloUtente.TestoSocioOnorario = 'Socio Onorario'
                                {2} {3}
                            ORDER BY Cognome"
                , !string.IsNullOrWhiteSpace(Dropdown_Regione.Text) ? ", tbRegioni.Regione" : string.Empty
                , !string.IsNullOrWhiteSpace(Dropdown_Regione.Text) ? @"INNER JOIN tbProvince ON tbProvince.Sigla = vwxSoci.S1_ProvinciaSigla 
                                                                            INNER JOIN tbRegioni ON tbRegioni.ID1Regione = tbProvince.ID2Regione"
                    : string.Empty
                , !string.IsNullOrWhiteSpace(Dropdown_Regione.Text) ? string.Format("AND tbRegioni.Regione = '{0}'", Dropdown_Regione.Text) : string.Empty                
                , !string.IsNullOrWhiteSpace(TextBox_Nome.Text) ? string.Format("AND vwxSoci.Cognome LIKE '%{0}%'", TextBox_Nome.Text) : string.Empty                
                );
            command.CommandText = sqlQuery;

            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                var socio = new Socio()
                {
                    UserId = reader["UserId"].ToString(),
                    FullName = string.Concat(reader["Qualifica"], " ", reader["Nome"], " ", reader["Cognome"]),
                    Photo = !string.IsNullOrWhiteSpace(reader["ImmagineSocio"].ToString()) ? "/ZeusInc/Account/ImgSoci/Img1/" + reader["ImmagineSocio"].ToString() : "/asset/img/logoSocio.jpg",
                    PhotoAlt = !string.IsNullOrWhiteSpace(reader["ImmagineSocioAlt"].ToString()) ? reader["ImmagineSocioAlt"].ToString() : ""
                };

                listaSoci.Add(socio);
            }
            reader.Close();
            connection.Close();
        }


        if (listaSoci.Count() == 0)
        {
            phNoMembers.Visible = true;
            PaginazioneLista.Visible = false;

            ListView_ListaSoci.DataSource = null;
            ListView_ListaSoci.DataBind();

            DataPager_ListaSoci.PagedControlID = "ListView_ListaSoci";
        }
        else
        {
            phNoMembers.Visible = false;
            PaginazioneLista.Visible = true;

            ListView_ListaSoci.DataSource = listaSoci;
            ListView_ListaSoci.DataBind();

            DataPager_ListaSoci.PagedControlID = "ListView_ListaSoci";


            paginaCorrenteSoci = Request.QueryString["page"] ?? "1";
            totPagineSoci = ((listaSoci.Count() / DataPager_ListaSoci.PageSize) + 1).ToString();

            if (listaSoci.Count() < DataPager_ListaSoci.PageSize)
                DataPager_ListaSoci.Visible = false;
        }
        //}
        //else
        //{
        //    phNoMembers.Visible = true;
        //    PaginazioneLista.Visible = false;

        //    ListView_ListaSoci.DataSource = null;
        //    ListView_ListaSoci.DataBind();

        //    DataPager_ListaSoci.PagedControlID = "ListView_ListaSoci";
        //}
    }

    protected void SqlDataSource_ListaSoci_Selected(object sender, System.Web.UI.WebControls.SqlDataSourceStatusEventArgs e)
    {
        if (e.AffectedRows <= 0)
        {
            phNoMembers.Visible = true;
            PaginazioneLista.Visible = false;
        }
        else
        {
            paginaCorrenteSoci = Request.QueryString["page"] ?? "1";
            totPagineSoci = ((e.AffectedRows / DataPager_ListaSoci.PageSize) + 1).ToString();

            if (e.AffectedRows < DataPager_ListaSoci.PageSize)
                DataPager_ListaSoci.Visible = false;
        }
    }

    protected void TextBox_Nome_TextChanged(object sender, EventArgs e)
    {
        if (Session["NomeSelezionato"] != null && Session["NomeSelezionato"].ToString() != TextBox_Nome.Text)
        {
            DataPager_ListaSoci.SetPageProperties(0, 100, true);
            paginaCorrenteSoci = "1";
        }

        Session["NomeSelezionato"] = TextBox_Nome.Text;
    }

    protected void Dropdown_Regione_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Session["RegioneSelezionata"] != null && Session["RegioneSelezionata"].ToString() != Dropdown_Regione.Text)
        {
            DataPager_ListaSoci.SetPageProperties(0, 100, true);
            paginaCorrenteSoci = "1";
        }

        Session["RegioneSelezionata"] = Dropdown_Regione.Text;
    }
}