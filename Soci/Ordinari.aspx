﻿<%@ Page Language="C#" MasterPageFile="~/Master/LayoutMain.master" CodeFile="Ordinari.aspx.cs" Inherits="Soci_Ordinari" %>

<asp:Content ID="Content3" ContentPlaceHolderID="AreaTitle" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AreaContentMain" runat="Server">

    <section class="soci">
        <div class="container">
            <div class="boxTitolo">
                <h2 class="titolo">Soci ordinari</h2>
                <hr />
            </div>

            <div class="filtro">
                <div class="row align-items-end">
                    <div class="col-md-5">
                        <h5 class="titoletto">Cognome socio</h5>
                        <asp:TextBox runat="server" ID="TextBox_Nome" OnTextChanged="TextBox_Nome_TextChanged"></asp:TextBox>
                    </div>
                    <div class="col-md-5">
                        <h5 class="titoletto">Regione</h5>
                        <asp:DropDownList runat="server" ID="Dropdown_Regione" OnSelectedIndexChanged="Dropdown_Regione_SelectedIndexChanged"></asp:DropDownList>
                    </div>
                    <div class="col-md-2 text-center">
                        <div class="link">
                            <asp:LinkButton runat="server" ID="Cerca_Button" Text="Cerca" OnClick="Cerca_Button_Click" />
                        </div>
                    </div>
                </div>
            </div>


            <%--<div class="col-md-6">
                    <div class="mappaItalia">
                        <img src="/asset/img/map/Base_Clear.png" class="img-responsive baseClear" usemap="#location-map" />
                        <img src="/asset/img/map/SMI_BkgMap.png" class="img-responsive map" />
                        <img id="overlayABR" src="/asset/img/map/Hover_Abruzzo.png" class="img-responsive regione" />
                        <img id="overlayBAS" src="/asset/img/map/Hover_Basilicata.png" class="img-responsive regione" />
                        <img id="overlayCAL" src="/asset/img/map/Hover_Calabria.png" class="img-responsive regione" />
                        <img id="overlayCAM" src="/asset/img/map/Hover_Campania.png" class="img-responsive regione" />
                        <img id="overlayEMI" src="/asset/img/map/Hover_EmiliaRomagna.png" class="img-responsive regione" />
                        <img id="overlayFRI" src="/asset/img/map/Hover_FriuliVeneziaGiulia.png" class="img-responsive regione" />
                        <img id="overlayLAZ" src="/asset/img/map/Hover_Lazio.png" class="img-responsive regione" />
                        <img id="overlayLIG" src="/asset/img/map/Hover_Liguria.png" class="img-responsive regione" />
                        <img id="overlayLOM" src="/asset/img/map/Hover_Lombardia.png" class="img-responsive regione" />
                        <img id="overlayMAR" src="/asset/img/map/Hover_Marche.png" class="img-responsive regione" />
                        <img id="overlayMOL" src="/asset/img/map/Hover_Molise.png" class="img-responsive regione" />
                        <img id="overlayPIE" src="/asset/img/map/Hover_Piemonte.png" class="img-responsive regione" />
                        <img id="overlayPUG" src="/asset/img/map/Hover_Puglia.png" class="img-responsive regione" />
                        <img id="overlaySAR" src="/asset/img/map/Hover_Sardegna.png" class="img-responsive regione" />
                        <img id="overlaySIC" src="/asset/img/map/Hover_Sicilia.png" class="img-responsive regione" />
                        <img id="overlayTOS" src="/asset/img/map/Hover_Toscana.png" class="img-responsive regione" />
                        <img id="overlayTRE" src="/asset/img/map/Hover_TrentinoAltoAdige.png" class="img-responsive regione" />
                        <img id="overlayUMB" src="/asset/img/map/Hover_Umbria.png" class="img-responsive regione" />
                        <img id="overlayVAL" src="/asset/img/map/Hover_ValledAosta.png" class="img-responsive regione" />
                        <img id="overlayVEN" src="/asset/img/map/Hover_Veneto.png" class="img-responsive regione" />
                    </div>
                    <map name="location-map" id="location-map">
                        <area shape="poly" style="cursor: pointer;" id="ABR" alt="Abruzzo" href="/Soci/Default.aspx?XRI=1" coords="557,476,557,484,543,506,533,499,523,496,516,503,517,511,506,516,485,507,477,509,465,504,465,497,441,486,441,478,445,475,457,479,462,474,451,458,449,451,450,438,455,438,464,433,465,427,479,416,497,407,515,443,539,465,557,476" />
                        <area shape="poly" style="cursor: pointer;" id="BAS" alt="Basilicata" href="/Soci/Default.aspx?XRI=2" coords="636,680,651,673,668,681,680,676,683,658,700,658,711,636,705,626,699,605,689,602,680,607,663,588,650,585,647,577,642,572,617,574,623,586,612,594,608,593,619,627,640,649,630,669,637,680" />
                        <area shape="poly" style="cursor: pointer;" id="CAL" alt="Calabria" href="/Soci/Default.aspx?XRI=3" coords="637,681,662,758,674,776,648,801,633,841,635,868,670,870,697,829,702,816,701,791,741,772,740,731,713,707,690,692,699,674,696,663,680,666,679,683,661,685,651,676,635,681" />
                        <area shape="poly" style="cursor: pointer;" id="CAM" alt="Campania" href="/Soci/Default.aspx?XRI=4" coords="609,671,627,666,635,650,619,631,611,617,603,591,608,594,618,580,614,574,604,571,593,570,594,558,583,550,582,544,576,539,546,547,527,541,519,540,518,546,508,546,504,541,495,558,504,586,508,608,535,622,566,616,578,640,575,658,608,676,613,672" />
                        <area shape="poly" style="cursor: pointer;" id="EMI" alt="EmiliaRomagna" href="/Soci/Default.aspx?XRI=5" coords="387,220,369,208,351,212,335,212,320,210,307,210,294,210,285,209,281,207,266,211,251,201,238,198,227,191,218,194,197,194,190,204,187,212,187,230,180,234,182,241,196,247,194,257,209,261,221,253,234,266,264,279,270,289,283,287,296,296,316,293,324,280,346,291,348,313,374,322,378,309,392,304,409,314,416,306,394,279,387,247,387,220" />
                        <area shape="poly" style="cursor: pointer;" id="FRI" alt="Friuli Venezia Giulia" href="/Soci/Default.aspx?XRI=6" coords="422,54,413,70,393,83,403,101,401,111,412,125,431,126,439,135,461,134,468,134,489,150,496,149,475,131,471,117,465,109,473,101,463,96,461,78,477,64,423,55" />
                        <area shape="poly" style="cursor: pointer;" id="LAZ" alt="Lazio" href="/Soci/Default.aspx?XRI=7" coords="490,562,495,547,505,535,502,520,493,516,485,510,473,514,469,508,461,506,460,499,445,489,437,489,436,480,443,472,455,473,451,460,443,449,444,436,461,432,454,422,443,432,422,440,403,456,391,446,381,436,379,430,369,430,360,423,362,415,359,412,353,417,352,427,340,440,343,445,331,453,343,461,354,484,370,488,380,504,385,514,411,539,441,563,463,559,476,566,491,562" />
                        <area shape="poly" style="cursor: pointer;" id="LIG" alt="Liguria" href="/Soci/Default.aspx?XRI=8" coords="75,277,59,300,65,312,93,307,107,291,124,275,156,258,175,266,203,283,221,296,232,293,231,285,220,281,216,274,206,260,198,259,193,257,196,246,181,241,170,241,163,234,160,242,154,248,147,248,143,242,130,246,119,248,115,247,111,252,103,264,105,276,94,281,77,279" />
                        <area shape="poly" style="cursor: pointer;" id="LOM" alt="Lombardia" href="/Soci/Default.aspx?XRI=9" coords="174,123,172,102,189,79,190,60,202,58,210,80,226,74,239,79,244,88,249,74,241,60,252,53,262,60,273,65,275,69,271,99,264,109,273,132,286,128,276,145,278,163,290,186,318,206,311,207,292,210,281,204,269,209,234,195,207,191,190,194,183,210,186,219,183,228,175,215,166,201,159,195,147,194,139,173,142,170,154,166,152,145,145,124,148,110,157,99,162,117,175,124" />
                        <area shape="poly" style="cursor: pointer;" id="MAR" alt="Marche" href="/Soci/Default.aspx?XRI=10" coords="464,422,477,413,495,405,486,361,474,337,464,336,419,302,411,311,400,305,386,305,379,308,379,315,393,324,384,334,399,345,413,350,420,350,423,375,429,395,443,405,454,410,453,416,461,423" />
                        <area shape="poly" style="cursor: pointer;" id="MOL" alt="Molise" href="/Soci/Default.aspx?XRI=11" coords="548,547,575,535,572,524,584,517,585,496,563,482,543,512,538,509,526,503,520,507,523,512,507,521,509,529,508,538,509,544,513,535,526,534,547,545" />
                        <area shape="poly" style="cursor: pointer;" id="PIE" alt="Piemonte" href="/Soci/Default.aspx?XRI=12" coords="97,136,93,111,99,104,113,91,113,78,132,60,139,66,141,88,153,95,146,113,143,125,157,160,147,172,139,164,136,173,143,194,158,197,177,222,183,230,177,242,160,231,159,238,151,248,144,239,126,246,120,248,114,245,102,265,96,279,76,278,72,276,55,278,30,266,20,238,32,225,29,208,15,196,8,180,33,176,47,158,45,149,63,142,79,144,97,137" />
                        <area shape="poly" style="cursor: pointer;" id="PUG" alt="Puglia" href="/Soci/Default.aspx?XRI=13" coords="827,684,834,658,797,610,726,568,643,533,646,524,664,505,651,491,607,495,588,496,587,517,576,527,583,541,585,549,599,560,601,570,641,566,655,583,665,585,681,600,699,599,707,622,714,627,732,619,743,622,745,631,782,637,796,665,809,680,826,683" />
                        <area shape="poly" style="cursor: pointer;" id="SAR" alt="Sardegna" href="/Soci/Default.aspx?XRI=14" coords="94,585,122,588,175,553,209,626,191,653,201,669,187,758,150,748,143,770,116,776,97,765,95,742,102,666,99,621,88,614,91,583,99,563,96,587" />
                        <area shape="poly" style="cursor: pointer;" id="SIC" alt="Sicilia" href="/Soci/Default.aspx?XRI=15" coords="417,851,397,872,398,894,415,906,507,952,531,953,550,981,597,992,613,957,595,926,630,842,579,847,510,866,478,858,460,846,419,853" />
                        <area shape="poly" style="cursor: pointer;" id="TOS" alt="Toscana" href="/Soci/Default.aspx?XRI=16" coords="216,265,229,279,234,293,248,326,253,349,262,376,261,397,235,410,235,419,256,423,264,402,279,410,301,431,300,448,306,457,323,452,332,443,336,432,343,426,346,413,359,404,360,382,372,366,369,351,378,328,351,317,341,301,345,292,326,281,318,291,302,292,295,293,283,286,277,288,269,286,257,276,242,269,232,264,223,255,215,266" />
                        <area shape="poly" style="cursor: pointer;" id="TRE" alt="Trentino Alto Adige" href="/Soci/Default.aspx?XRI=17" coords="307,42,287,38,283,31,272,30,268,41,271,59,283,70,277,79,274,101,270,113,275,126,285,122,299,129,298,137,309,136,318,115,334,110,338,105,343,110,347,101,360,93,355,79,357,72,356,62,371,57,375,48,393,47,379,23,378,14,360,21,345,20,323,22,306,41" />
                        <area shape="poly" style="cursor: pointer;" id="UMB" alt="Umbria" href="/Soci/Default.aspx?XRI=18" coords="383,339,372,355,382,365,376,372,366,382,368,390,363,403,365,413,366,424,376,423,385,434,389,441,394,447,400,450,413,440,426,433,441,428,446,424,448,414,440,406,434,409,429,399,426,385,422,374,417,356,410,356,401,348,392,346,388,334,382,332,381,341" />
                        <area shape="poly" style="cursor: pointer;" id="VAL" alt="Valle dAosta" href="/Soci/Default.aspx?XRI=19" coords="44,150,71,143,82,143,94,137,94,123,94,109,79,102,68,104,64,108,59,108,51,111,40,105,26,114,26,125,36,144,44,151" />
                        <area shape="poly" style="cursor: pointer;" id="VEN" alt="Veneto" href="/Soci/Default.aspx?XRI=20" coords="290,130,276,148,288,180,319,198,341,213,365,207,382,208,392,220,402,213,390,193,395,170,429,155,440,140,432,131,416,130,400,123,397,110,394,105,399,98,390,87,405,68,420,57,403,49,389,55,381,54,376,52,367,65,361,66,360,79,368,97,351,104,348,116,338,110,328,118,321,122,312,141,304,141,295,139,291,131" />
                    </map>
                </div>--%>

            <asp:PlaceHolder runat="server" ID="phNoMembers" Visible="false">
                <p>Non sono presenti soci per i parametri selezionati</p>
            </asp:PlaceHolder>

            <div class="elencoSoci">
                <div class="row">
                    <asp:ListView runat="server" ID="ListView_ListaSoci">
                        <ItemTemplate>
                            <div class="col-sm-6">
                                <div class="socio">
                                    <div class="logo">
                                        <img src='<%# Eval("Photo") %>' alt='<%# Eval("PhotoAlt") %>' />
                                    </div>
                                    <div class="contenuto">
                                        <h4 class="nome"><%# Eval("FullName")%> <span class="onorario"></span></h4>

                                        <a class="link" href="/Soci/Content.aspx?XRI=<%#Eval("UserId") %>" target="_blank">Vedi la scheda ></a>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
            </div>

            <%--<asp:SqlDataSource runat="server" ID="SqlDataSource_ListaSoci" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>" OnSelected="SqlDataSource_ListaSoci_Selected" />--%>

            <div class="paginazione" runat="server" id="PaginazioneLista" visible="false">
                <div class="pagina">
                    <p>Pagina <%= paginaCorrenteSoci %> di <%= totPagineSoci %></p>
                </div>

                <div class="pager">
                    <asp:DataPager ID="DataPager_ListaSoci" runat="server" PagedControlID="ListView_ListaSoci" PageSize="100" QueryStringField="page">
                        <Fields>
                            <asp:NextPreviousPagerField ButtonType="Link" PreviousPageText="Indietro" ShowFirstPageButton="false" ShowPreviousPageButton="true" ShowNextPageButton="false" ButtonCssClass="elemento" />
                            <asp:NumericPagerField ButtonType="Link" NumericButtonCssClass="elemento" CurrentPageLabelCssClass="elementoSelezionato" NextPreviousButtonCssClass="elemento" />
                            <asp:NextPreviousPagerField ButtonType="Link" NextPageText="Avanti" ShowLastPageButton="false" ShowNextPageButton="true" ShowPreviousPageButton="false" ButtonCssClass="elemento" />
                        </Fields>
                    </asp:DataPager>
                </div>
            </div>
        </div>
    </section>

</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="Script" runat="Server">
    <%--<script src="/asset/js/jquery.rwdImageMaps.min.js"></script>


    <script type="text/javascript">

        $(document).ready(function () {

            //Hover display on map area
            if ($('#location-map')) {
                $('#location-map area').each(function () {
                    var id = $(this).attr('id');
                    $(this).mouseover(function () {
                        $('#overlay' + id).show();
                    });

                    $(this).mouseout(function () {
                        var id = $(this).attr('id');
                        $('#overlay' + id).hide();
                    });

                });
            }

            //Responsive map area
            $('img[usemap]').rwdImageMaps();

        });

    </script>--%>
</asp:Content>
