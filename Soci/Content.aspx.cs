﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Soci_Content : System.Web.UI.Page
{
    string strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

    protected string Qualifica = "";
    protected string Nome = "";
    protected string Cognome = "";
    protected string TestoSocioOnorario = "";
    protected string Provincia = "";
    protected string Indirizzo = "";
    protected string Telefono = "";
    protected string Email = "";
    protected string Affiliazione = "";
    protected string SitoWeb = "";
    protected string Photo = "";
    protected string PhotoAlt = "";
    protected string Curriculum = "";


    protected void Page_Load(object sender, EventArgs e)
    {
        var XRI = Request.QueryString["XRI"];
        BindSocio(XRI);


        /*****************************************************/
        /**************** INIZIO SEO *************************/
        /*****************************************************/
        HtmlMeta description = new HtmlMeta();
        HtmlMeta keywords = new HtmlMeta();
        description.Name = "description";
        keywords.Name = "keywords";
        Page.Title = Nome + " " + Cognome + " | " + AresConfig.NomeSito;
        description.Content = Nome + " " + Cognome;
        keywords.Content = Nome + " " + Cognome;
        HtmlHead head = Page.Header;
        head.Controls.Add(description);
        head.Controls.Add(keywords);
        /*****************************************************/
        /**************** FINE SEO ***************************/
        /*****************************************************/
    }

    private void BindSocio(string XRI)
    {
        string sqlQuery = string.Empty;
        using (SqlConnection connection = new SqlConnection(strConnessione))
        {
            connection.Open();
            SqlCommand command = connection.CreateCommand();

            sqlQuery = @"SELECT vwxSoci.Cognome, vwxSoci.Nome, RuoloUtente.TestoSocioOnorario, vwxSoci.Identificativo, vwxSoci.Qualifica, vwxSoci.Professione, vwxSoci.ProvinciaSigla
	                    , vwxSoci.UserId, tbProvince.Provincia, tbProfiliPersonali.CurriculumVitae, tbProfiliPersonali.Affiliazione, tbProfiliPersonali.SitoWeb
                        , tbProfiliPersonali.ImmagineSocio, tbProfiliPersonali.ImmagineSocioAlt
	                    , CASE WHEN tbProfiliMarketing.ConsensoDatiPersonali2 = 0 THEN '-' ELSE vwxSoci.Telefono1 END AS Telefono1
	                    , CASE WHEN tbProfiliMarketing.ConsensoDatiPersonali2 = 0 THEN '-' ELSE vwxSoci.Indirizzo + ' ' + vwxSoci.Numero + ' ' + vwxSoci.Citta END AS Indirizzo
	                    , CASE WHEN tbProfiliMarketing.ConsensoDatiPersonali2 = 0 THEN '-' ELSE vwxSoci.LoweredEmail END AS LoweredEmail	
                    FROM [dbo].[vwxSoci]
	                    INNER JOIN tbProvince ON tbProvince.Sigla = vwxSoci.ProvinciaSigla
	                    INNER JOIN tbProfiliMarketing ON tbProfiliMarketing.UserId = vwxSoci.UserId
                        INNER JOIN tbProfiliPersonali ON tbProfiliPersonali.UserId = vwxSoci.UserId
                        OUTER APPLY (
		                    SELECT TOP (1) CASE WHEN aspnet_Roles.RoleName = 'SociOnorari' THEN 'Socio Onorario' ELSE '' END AS TestoSocioOnorario
		                    FROM aspnet_Roles
			                    INNER JOIN aspnet_UsersInRoles ON aspnet_UsersInRoles.UserId = vwxSoci.UserId
		                    WHERE aspnet_Roles.RoleId = aspnet_UsersInRoles.RoleId 	
		                    ORDER BY TestoSocioOnorario DESC
	                    ) AS RuoloUtente
                    WHERE vwxSoci.UserId = @UserId";

            command.CommandText = sqlQuery;
            command.Parameters.Add("@UserId", System.Data.SqlDbType.VarChar);
            command.Parameters["@UserId"].Value = XRI;

            SqlDataReader reader = command.ExecuteReader();

            if (reader.Read())
            {
                Qualifica = !string.IsNullOrEmpty(reader["Qualifica"].ToString()) ? reader["Qualifica"].ToString() : "-";
                Nome = !string.IsNullOrEmpty(reader["Nome"].ToString()) ? reader["Nome"].ToString() : "";
                Cognome = !string.IsNullOrEmpty(reader["Cognome"].ToString()) ? reader["Cognome"].ToString() : "";
                TestoSocioOnorario = !string.IsNullOrEmpty(reader["TestoSocioOnorario"].ToString()) ? reader["TestoSocioOnorario"].ToString() : "";
                Provincia = !string.IsNullOrEmpty(reader["Provincia"].ToString()) ? reader["Provincia"].ToString() : "";

                Indirizzo = reader["Indirizzo"].ToString();

                Telefono = !string.IsNullOrEmpty(reader["Telefono1"].ToString()) ? reader["Telefono1"].ToString() : "-";
                Email = !string.IsNullOrEmpty(reader["LoweredEmail"].ToString()) ? reader["LoweredEmail"].ToString() : "-";
                Affiliazione = !string.IsNullOrEmpty(reader["Affiliazione"].ToString()) ? reader["Affiliazione"].ToString() : "-";
                SitoWeb = !string.IsNullOrEmpty(reader["SitoWeb"].ToString()) ? string.Format("<a href=\"{0}\" target=\"_blank\">{0}</a>", reader["SitoWeb"].ToString()) : "-";

                
                Photo = !string.IsNullOrWhiteSpace(reader["ImmagineSocio"].ToString()) ? "/ZeusInc/Account/ImgSoci/Img1/" + reader["ImmagineSocio"].ToString() : "/asset/img/logoSocio.jpg";
                if (!string.IsNullOrWhiteSpace(reader["ImmagineSocioAlt"].ToString()))
                    PhotoAlt = reader["ImmagineSocioAlt"].ToString();


                if (string.IsNullOrEmpty(Photo))
                {
                    //Hide panel image
                    mdImage.Visible = false;
                    //mdData.Attributes["class"] = "col-md-12";
                }

                Curriculum = string.Empty;
                if (!string.IsNullOrEmpty(reader["CurriculumVitae"].ToString()))
                {
                    Curriculum = reader["CurriculumVitae"].ToString();
                    CurriculumPlaceholder.Visible = true;
                }


                /*****************************************************/
                /**************** INIZIO SEO *************************/
                /*****************************************************/
                //HtmlMeta description = new HtmlMeta();
                //HtmlMeta keywords = new HtmlMeta();
                //description.Name = "description";
                //keywords.Name = "keywords";
                //Page.Title = reader["TitoloBrowser"].ToString() + " | " + AresConfig.NomeSito;
                ///////////////////TODO////////////
                //description.Content = reader["TagDescription"].ToString();
                //keywords.Content = reader["TagKeywords"].ToString();
                //HtmlHead head = Page.Header;
                //head.Controls.Add(description);
                //head.Controls.Add(keywords);

                /*****************************************************/
                /**************** FINE SEO ***************************/
                /*****************************************************/

                reader.Close();
                connection.Close();
            }
            else
            {
                reader.Close();
                connection.Close();
                Response.Redirect("~/System/Message.aspx?0");
            }
        }
    }
}