﻿using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Master_LayoutMain : System.Web.UI.MasterPage
{
    string lang = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        AresUtilities aresUtil = new AresUtilities();
        lang = aresUtil.GetLangRewritted();

        Header.Lang = lang;
        Footer.Lang = lang;
        form.Action = Request.RawUrl;
    }
}