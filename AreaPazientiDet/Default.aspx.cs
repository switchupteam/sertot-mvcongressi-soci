﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.HtmlControls;

public partial class Pubblicazione_Default : System.Web.UI.Page
{
    protected string titolo = string.Empty;
    protected string descBreve = string.Empty;
    protected string contenuto1 = string.Empty;
    protected string contenuto2 = string.Empty;
    protected string immagine1 = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Request.QueryString["XRI"]))
        {
            Response.Redirect("~/System/Message.aspx?0");
        }

        AresUtilities aresUtilities = new AresUtilities();
        string lang = aresUtilities.GetLangRewritted();

        string strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(strConnessione))
        {
            connection.Open();
            SqlCommand command = connection.CreateCommand();

            sqlQuery = @"SELECT ID1Publisher, Titolo, ZeusIdModulo, Immagine1, DescBreve1, Contenuto1, Contenuto2, ID2Categoria1
                            , Sottotitolo AS Link, UrlRewrite, TitoloBrowser, TagDescription, TagKeywords
                        FROM vwPublisher_Public_DtlArea1
                        WHERE ZeusIdModulo = 'PBPAZ' AND ID1Publisher = @ID1Publisher AND vwPublisher_Public_DtlArea1.ZeusLangCode = @ZeusLangCode ";
          
            command.CommandText = sqlQuery;
            command.Parameters.Add("@ID1Publisher", System.Data.SqlDbType.Int);
            command.Parameters["@ID1Publisher"].Value = Request.QueryString["XRI"];
            command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.NVarChar);
            command.Parameters["@ZeusLangCode"].Value = lang;

            SqlDataReader reader = command.ExecuteReader();

            if (reader.Read())
            {
                titolo = reader["Titolo"].ToString();
                descBreve = reader["DescBreve1"].ToString();
                contenuto1 = reader["Contenuto1"].ToString();
                contenuto2 = reader["Contenuto2"].ToString();

                immagine1 = reader["Immagine1"].ToString();
                if (string.IsNullOrWhiteSpace(immagine1))
                    immagine1 = "ImgNonDisponibile.jpg";


                /*****************************************************/
                /**************** INIZIO SEO *************************/
                /*****************************************************/
                HtmlMeta description = new HtmlMeta();
                HtmlMeta keywords = new HtmlMeta();
                description.Name = "description";
                keywords.Name = "keywords";
                Page.Title = reader["TitoloBrowser"].ToString() + " | " + AresConfig.NomeSito;
                description.Content = reader["TagDescription"].ToString();
                keywords.Content = reader["TagKeywords"].ToString();
                HtmlHead head = Page.Header;
                head.Controls.Add(description);
                head.Controls.Add(keywords);
                /*****************************************************/
                /**************** FINE SEO ***************************/
                /*****************************************************/

                reader.Close();
                connection.Close();
            }
            else
            {
                reader.Close();
                connection.Close();
                Response.Redirect("~/System/Message.aspx?0");
            }
        }
    }    
}