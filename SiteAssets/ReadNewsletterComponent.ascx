﻿<%@ Control Language="C#" ClassName="ReadNewsletterComponent" %>

<script runat="server">

    public string Lang
    {
        set { hfLang.Value = value; }
        get { return hfLang.Value; }
    }

    public string ComponentName
    {
        set { hfComponentName.Value = value; }
        get { return hfComponentName.Value; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {

    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

</script>

<asp:HiddenField ID="hfLang" runat="server" />
<asp:HiddenField ID="hfComponentName" runat="server" />

<asp:FormView runat="server" ID="NewsletterComponent" DataSourceID="NewsletterComponentDataSource" DefaultMode="ReadOnly" RenderOuterTable="false">
    <ItemTemplate>
        <asp:Label runat="server" ID="Contenuto1Label" CssClass="TFY_Testo1" Text='<%# Eval("Contenuto1") %>' />
    </ItemTemplate>
</asp:FormView>

<asp:SqlDataSource runat="server" ID="NewsletterComponentDataSource" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>" CancelSelectOnNullParameter="false" 
    SelectCommand="SELECT Contenuto1 
    FROM dbo.vwPageDesigner_Public_Dtl 
    WHERE ZeusIDModulo='PDNWS' 
    AND ZeusAssetType = @ComponentName 
    AND ZeusLangCode = @Lang">
    <SelectParameters>
        <asp:ControlParameter Name="Lang" ControlID="hfLang" PropertyName="Value" Size="3" DbType="String" ConvertEmptyStringToNull="true" />
        <asp:ControlParameter Name="ComponentName" ControlID="hfComponentName" PropertyName="Value" DbType="String" ConvertEmptyStringToNull="true" />
    </SelectParameters>
</asp:SqlDataSource>

