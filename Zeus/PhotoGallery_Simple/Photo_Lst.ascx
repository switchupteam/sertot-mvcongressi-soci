﻿<%@ Control Language="C#" ClassName="Photo_Lst" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">

    
    static int UP = 3;
    static int DOWN = 4;

    public string ZeusIdModulo
    {
        set { hfZeusIdModulo.Value = value; }
        get { return hfZeusIdModulo.Value; }
    }

    public string XRI
    {
        set { hfXRI.Value = value; }
        get { return hfXRI.Value; }
    }

    public string Lang
    {
        set { hfLang.Value = value; }
        get { return hfLang.Value; }
    }

    public string ZID
    {
        set { hfZID.Value = value; }
        get { return hfZID.Value; }
    }

    protected void Page_Init(Object sender, EventArgs e)
    {
        
    }
    
    protected void Page_Load(Object sender, EventArgs e)
    {
        if ((hfZeusIdModulo.Value.ToString().Length == 0) || (hfXRI.Value.ToString().Length == 0) || (hfZID.Value.ToString().Length == 0)  || (hfLang.Value.ToString().Length == 0))
            ErrorLabel.Text = "Attenzione: parametri mancanti o non corretti";

        
        //Link per aggiungere un'immagine
        AggiungiHyperLink.NavigateUrl = "Photo_New.aspx?ZID=" + hfZID.Value;
        AggiungiHyperLink.NavigateUrl += "&ZIM=" + hfZeusIdModulo.Value;
        AggiungiHyperLink.NavigateUrl += "&XRI=" + hfXRI.Value;
        AggiungiHyperLink.NavigateUrl += "&Lang=" + hfLang.Value;
        AggiungiHyperLink.NavigateUrl += "&RetUrl=" + Request.FilePath;
        
    }//fine Page_Load


    private String OttieniZIM(string ZeusIdModulo, String ZeusLangCode)
    {

        try
        {

            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            string myXPathEach = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_Locator.xml"));

            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPathEach);

            return mydoc.SelectSingleNode(myXPath + "ZMCD_ZIMGalleryAssociation").InnerText;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return "Errore";
        }

    }//fine OttieniZIM

    //FUNZIONI PER L'ORDINAMENTO  


    private int getLastOrder()
    {

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT MAX(Ordinamento)";
                sqlQuery += " FROM [tbPhotoGallery_Simple] ";
                sqlQuery += " WHERE [ID2ZeusLocator]=@ID2Locator";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID2Locator", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@ID2Locator"].Value = new System.Data.SqlTypes.SqlGuid(hfZID.Value);
                SqlDataReader reader = command.ExecuteReader();

                int myReturn = 0;

                while (reader.Read())
                    if (!reader.IsDBNull(0))
                        myReturn = reader.GetInt16(0);


                reader.Close();
                return myReturn;
            }
            catch (Exception p)
            {

                Response.Write(p.ToString());
                return -1;
            }
        }//fine Using

    }//fine getLastOrder

    
    
    static int LIVELLI = 1;

    
    private bool CheckIntegrityR(string ID2Locator)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {



            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();




                ArrayList myArray = new ArrayList();

                string[][] myMenuChange = null;

                int myOrder = 1;


                SqlDataReader reader = null;


                for (int index = 1; index <= LIVELLI; ++index)
                {


                    sqlQuery = "SELECT [ID1Photo],Ordinamento";
                    sqlQuery += " FROM [tbPhotoGallery_Simple]";
                    sqlQuery += " WHERE [ID2Locator]=@ID2Locator";
                    sqlQuery += " ORDER BY Ordinamento,ID1Photo";
                    command.CommandText = sqlQuery;
                    command.Parameters.Clear();


                    command.Parameters.Add("@ID2Locator", System.Data.SqlDbType.Int, 4);
                    command.Parameters["@ID2Locator"].Value = ID2Locator;

                    reader = command.ExecuteReader();

                    myOrder = 1;

                    while (reader.Read())
                    {

                        if (myOrder != reader.GetInt16(1))
                        {

                            string[] myData = new string[2];
                            myData[0] = reader.GetInt32(0).ToString();
                            myData[1] = myOrder.ToString();
                            myArray.Add(myData);
                        }

                        ++myOrder;
                    }//fine while

                    reader.Close();




                    myMenuChange = (string[][])myArray.ToArray(typeof(string[]));
                    myArray = new ArrayList();


                    for (int i = 0; i < myMenuChange.Length; ++i)
                    {

                        sqlQuery = "UPDATE tbPhotoGallery_Simple ";
                        sqlQuery += " SET Ordinamento=@Ordinamento";
                        sqlQuery += " WHERE ID1Photo=@ID1Photo";
                        command.CommandText = sqlQuery;
                        command.Parameters.Clear();

                        command.Parameters.Add("@Ordinamento", System.Data.SqlDbType.NVarChar, 4);
                        command.Parameters["@Ordinamento"].Value = myMenuChange[i][1];

                        command.Parameters.Add("@ID1Photo", System.Data.SqlDbType.Int, 4);
                        command.Parameters["@ID1Photo"].Value = myMenuChange[i][0];

                        command.ExecuteNonQuery();


                    }//fine for




                }//fine for LIVELLI


                return true;
            }
            catch (Exception p)
            {

                Response.Write(p.ToString());

                return false;
            }
        }//fine using
    }//fine CheckIntegrityR

    private bool SaveNewOrderSQL()
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {



            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();


                    char[] delimiters = new char[] { ';'};

                    string[] parts = NewOrder.Value.ToString().Split(delimiters);
                    string ids = "";
                    //sqlQuery = "INSERT INTO tbPhotoGallery_Simple (ID1Photo,Ordinamento) VALUES  ";
                    sqlQuery = "UPDATE tbPhotoGallery_Simple SET Ordinamento = CASE ID1Photo ";
                    
                    int ii=1;
	                foreach (string s in parts)
	                {
                        /*if(ii!=1)
                            sqlQuery += ",";
	                    sqlQuery +=" ("+s.Substring(3)+","+ii+") ";
                        ii++;*/
                        sqlQuery += "WHEN " + s.Substring(3) + " THEN " + ii + " ";
                        if (ii != 1)
                            ids += ",";
                        ids += s.Substring(3);
                        ii++;
	                }
                    sqlQuery += " END  WHERE ID1Photo IN (" + ids + ")";
                   // sqlQuery += " ON DUPLICATE KEY UPDATE Ordinamento=VALUES(Ordinamento) ";

                    //Response.Write(sqlQuery);
                
                    command.CommandText = sqlQuery;
                    command.Parameters.Clear();

                    /*command.Parameters.Add("@Ordinamento", System.Data.SqlDbType.NVarChar, 4);
                    command.Parameters["@Ordinamento"].Value = myMenuChange[i][1];

                    command.Parameters.Add("@ID1Photo", System.Data.SqlDbType.Int, 4);
                    command.Parameters["@ID1Photo"].Value = myMenuChange[i][0];*/

                    command.ExecuteNonQuery();




                return true;
            }
            catch (Exception p)
            {

                Response.Write(p.ToString());

                return false;
            }
        }//fine using
    }//fine CheckIntegrityR
    //FINE FUNZIONI PER ORDINAMENTO  



    protected void SaveOrderButton_Click(object sender, EventArgs e)
    {
        SaveNewOrderSQL();
        MovPhoto.DataBind();
    }

    
</script>
<script language="javascript" type="text/javascript">
    function showImageUP(imgId, typ) {
        var objImg = document.getElementById(imgId);
        if (objImg) {
            if (typ == "1")
                objImg.src = "../SiteImg/Bt3_ArrowUp_Off.gif";
            else if (typ == "2")
                objImg.src = "../SiteImg/Bt3_ArrowUp_On.gif";
        }
    }

    function showImageDOWN(imgId, typ) {
        var objImg = document.getElementById(imgId);
        if (objImg) {
            if (typ == "1")
                objImg.src = "../SiteImg/Bt3_ArrowDown_Off.gif";
            else if (typ == "2")
                objImg.src = "../SiteImg/Bt3_ArrowDown_On.gif";
        }
    }
</script>

<asp:HiddenField runat="server" ID="hfZID" />
<asp:HiddenField ID="hfZeusIdModulo" runat="server" />
<asp:HiddenField ID="hfLang" runat="server" />
    <asp:HiddenField ID="hfXRI" runat="server" />

<div style="padding: 10px 0 0 0; margin: 10px 0px;">
    <asp:HyperLink ID="AggiungiHyperLink" runat="server"
        Text="Aggiungi foto"
        SkinID="ZSSM_Button01">
    </asp:HyperLink>
</div>
<asp:Label runat="server" ID="ErrorLabel" CssClass="ErrorMex"></asp:Label>
<style>
  #sortable { list-style-type: none; margin: 0; padding: 0; width: 100%; overflow: hidden; }
  #sortable li { margin: 10px; padding: 1px; float: left; font-size:13px; text-align: center; width: 90px; min-height: 110px;overflow:hidden;}
  #sortable li img{ width: 90px; height: 90px;  }
  .ModButton{
      /*position:absolute;
      top:5px;
      left:5px;*/
  }
  #sortable li .MovIcon{
      position:absolute;
      top:5px;
      right:5px;
      width:20px;
      height:20px;

  }
  .ui-state-default{
      position:relative;
  }

  .MessageLabel{
      font-family: Arial,Helvetica,sans-serif;
        font-size: 14px;
        color: #333;
        line-height: 14px;
  }

  .NotShow{
      line-height:15px;
  }
    .ErrorMex
    {
    color:#F00;
    }

  </style>
  <script>
          
  </script>
<asp:Repeater ID="MovPhoto" runat="server" DataSourceID="PhotoGallerySqlDataSource">
    <HeaderTemplate>
        <ul id="sortable">
    </HeaderTemplate>
    <ItemTemplate>
        
        <li id="id_<%# Eval("ID1Photo") %>" class="ui-state-default" <%#  Eval("Pubblica").ToString().ToLower().Equals("true") ? " style=\"border-color: #0F0;\" " : " style=\"border-color: #F00;\" " %>>
            <img src="<%# Eval("ImgGamma_Path").ToString().Replace("~", "") %><%#  Eval("Immagine2") %>" />
            <br />
            <span>
                <asp:LinkButton runat="server"  PostBackUrl='<%# String.Format("Photo_Edt.aspx?XRI={0}&XRI1={1}&Lang={2}&ZIM={3}&RetURL={4}", hfXRI.Value,Eval("ID1Photo"),hfLang.Value.ToString(),hfZeusIdModulo.Value,Request.FilePath) %>' SkinID="ZSSM_Button01" CssClass="ModButton" Text="Modifica" />

            </span>
            <img class="MovIcon" src="/Zeus/SiteImg/Movable.png" />
        </li>
    </ItemTemplate>
    <FooterTemplate>
        </ul>
    </FooterTemplate>
</asp:Repeater>
<asp:SqlDataSource ID="PhotoGallerySqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
    SelectCommand="SELECT     DISTINCT  ROW_NUMBER() 
        OVER (ORDER BY Ordinamento) AS row_index, dbo.tbPhotoGallery_Simple.ID1Photo, dbo.tbPhotoGallery_Simple.Immagine1, dbo.tbPhotoGallery_Simple.Immagine2, dbo.tbPhotoGallery_Simple.Ordinamento, dbo.tbPhotoGallery_Simple.Pubblica, dbo.tbPhotoGallery_Simple.ID2ZeusLocator, 
                          dbo.tbPhotoGallery_Simple.DataCreazione, dbo.tbPZ_ImageRaider.ImgBeta_Path, dbo.tbPZ_ImageRaider.ImgGamma_Path
FROM            dbo.tbPhotoGallery_Simple INNER JOIN
                         
                         dbo.tbPZ_ImageRaider ON dbo.tbPZ_ImageRaider.ZeusIdModuloIndice LIKE tbPhotoGallery_Simple.ZeusIdModulo + 'P'
WHERE [ID2ZeusLocator]=@ZeusId ORDER BY Ordinamento"
    DeleteCommand="DELETE FROM tbPhotoGallery_Simple  WHERE [ID1Photo] =@ID1Photo " >
    <SelectParameters>
        <asp:QueryStringParameter DefaultValue="0" Name="ID2Locator" QueryStringField="XRI"
            Type="Int32" />
        <asp:QueryStringParameter DefaultValue="ITA" Name="Lang" QueryStringField="Lang"
            Type="String" Size="3" />
        <asp:ControlParameter ControlID="hfZID" PropertyName="Value" Name="ZeusId" DbType="Guid" />
    </SelectParameters>
</asp:SqlDataSource>
<br />
<div id="dvResult" class="MessageLabel"></div>
<asp:HiddenField runat="server" ID="NewOrder"  />


<script src="/Zeus/SiteJs/jquery-ui.min.js" type="text/javascript"></script>
<script type="text/javascript">
    //jQuery.noConflict();
    //(function ($) {
    $(document).ready(function () {
        if ($("#sortable") != null) {
            //$("#sortable").sortable();
            //$("#sortable").disableSelection();
            var LI_POSITION = 'li_position';
            $('ul#sortable').sortable({
                //observe the update event...
                update: function (event, ui) {
                    //create the array that hold the positions...
                    var order = [];
                    //loop trought each li...
                    $('#sortable li').each(function (e) {

                        //add each li position to the array...     
                        // the +1 is for make it start from 1 instead of 0
                        //order.push($(this).attr('id') + '=' + ($(this).index() + 1));
                        order.push($(this).attr('id'));
                    });
                    // join the array as single variable...
                    var positions = order.join(';')
                    //use the variable as you need!
                    document.getElementById("<%= NewOrder.ClientID %>").value = positions;
                    $("#SaveOrderButton").css("display", "inline");

                    //$('#ctl00$ZeusContent$FormView1$LocatorPhoto1$NewOrder').value = positions;
                    //alert(positions);
                    // $.cookie( LI_POSITION , positions , { expires: 10 });
                }
            });
        }
    });
    //})(jQuery);
    //jQuery.noConflict(false);
</script>
<script type="text/javascript">
   
    function SaveRecord() {
        //jQuery.noConflict();
        (function SaveRecord($) {
            //Get control's values
            var values = $.trim($('#<%=NewOrder.ClientID %>').val());
           

            var msg = "";
            //check for validation
           /* if (bookName == '') {
                msg += "<li>Please enter book name</li>";
            }
            if (author == '') {
                msg += "<li>Please enter author name</li>";
            }
            if (type == 0) {
                msg += "<li>Please select book type</li>";
            }
            if (price == '') {
                msg += "<li>Please enter book price</li>";
            }*/

            if (msg.length == 0) {
                //Jquery ajax call to server side method
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    //Url is the path of our web method (Page name/function name)
                    url: "/Zeus/PhotoGallery_Simple/Photo_WebCalls.aspx/SaveNewOrderSQL",
                    //Pass paramenters to the server side function
                    data: "{'values':'" + values + "'}",
                    success: function (response) {
                        //Success or failure message e.g. Record saved or not saved successfully
                        if (response.d == true) {
                            //Set message
                            $('#dvResult').text("Nuovo ordinamento salvato con successo");
                            //Reset controls                          
                            $('#<%=NewOrder.ClientID %>').val('');
                            $("#SaveOrderButton").css("display", "none");
                        }
                        else {
                            $('#dvResult').text("Errore durante il savataggio dell'ordinamento");
                            $('#<%=NewOrder.ClientID %>').val('');
                            $("#SaveOrderButton").css("display", "none");
                        }
                        //Fade Out to disappear message after 6 seconds
                        $('#dvResult').fadeOut(6000);
                    },
                    error: function (xhr, textStatus, error) {
                        //Show error message(if occured)
                        $('#dvResult').text("Error: " + error);
                    }
                });
            }
            else {
                //Validation failure message
                $('#dvResult').html('');
                $('#dvResult').html(msg);
            }
            $('#dvResult').fadeIn();
        })(jQuery);
    }
    //jQuery.noConflict(false);
    </script>

<button ID="SaveOrderButton" Class="ZSSM_Button01_LinkButton NotShow" onclick="SaveRecord();return false">Salva nuovo ordinamento</button>

<%--<asp:LinkButton ID="SaveOrderButton" runat="server" SkinID="ZSSM_Button01" Text="Salva nuovo ordinamento" CssClass="NotShow"
                    OnClick="SaveOrderButton_Click"></asp:LinkButton>--%>
<%--AND (tbPhotoGallery_Simple.ZeusLangCode = @Lang)--%>