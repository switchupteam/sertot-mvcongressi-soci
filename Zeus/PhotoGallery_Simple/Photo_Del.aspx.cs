﻿using System;
using System.Data.SqlClient;
using System.Web.Security;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per Content_Del
/// </summary>
public partial class Photo_Del : System.Web.UI.Page
{

    static string TitoloPagina = "Elimina immagine";

    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;

        delinea myDelinea = new delinea();

        if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM"))
             || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI"))
             || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI1"))
            || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "RetUrl"))
            || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang")))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        ZIM.Value = Server.HtmlEncode(Request.QueryString["ZIM"]);
        XRI.Value = Server.HtmlEncode(Request.QueryString["XRI"]);

        //Id Immagine
        ID1Photo.Value = Server.HtmlEncode(Request.QueryString["XRI1"]);
        RetUrl.Value = Server.HtmlEncode(Request.QueryString["RetUrl"]);
        Lang.Value = Server.HtmlEncode(Request.QueryString["Lang"]);

        TitoloLabel.Text = GetPageTitle(ID1Photo.Value);

    }//fine Page_Load

    private bool isOK()
    {
        try
        {
            if  (Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/Locator/Photo_Edt.aspx") > 0)
                return true;
            else return false;
        }
        catch (Exception)
        {
            return false;
        }

    }//fine isOK



    private string GetPageTitle(string ID1Pagina)
    {

        string PageTitle = string.Empty;
        try
        {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            string sqlQuery = "SELECT Immagine1";
            sqlQuery += " FROM tbPhotoGallery_Simple ";
            sqlQuery += " WHERE ID1Photo=" + Server.HtmlEncode(ID1Pagina);
            //Response.Write(sqlQuery);

            using (SqlConnection conn = new SqlConnection(strConnessione))
            {
                SqlCommand command = new SqlCommand(sqlQuery, conn);

                conn.Open();
                SqlDataReader reader = command.ExecuteReader();
                /*fino a quando ci sono record*/


                while (reader.Read())
                    if (reader["Immagine1"] != DBNull.Value)
                        PageTitle = reader["Immagine1"].ToString();

                reader.Close();

            }//fine Using

        }
        catch (Exception p)
        {
            ErrorLabel.Text = p.ToString();
        }

        return PageTitle;

    }//fine GetPageTitle


    private bool DeleteRecord(string ID1Pagina)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                sqlQuery = "DELETE FROM tbPhotoGallery_Simple WHERE ID1Photo=" + Server.HtmlEncode(ID1Pagina);
                //sqlQuery = "UPDATE tbLocator SET ZeusIsAlive=0 WHERE ID1REA=" + Server.HtmlEncode(ID1Pagina);

                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();
            }
            catch (Exception p)
            {

                ErrorLabel.Text = p.ToString();
                return false;
            }

        }//fine Using

        return true;

    }//fine DeleteRecord


    protected void EseguiButton_Click(object sender, EventArgs e)
    {
        if (ConfermaCheckBox.Checked)
        {
            if (DeleteRecord(ID1Photo.Value))
                //Response.Redirect("~/Zeus/System/Message.aspx?Msg=12343267136223555");
                Response.Redirect(RetUrl.Value + "?XRI=" + XRI.Value + "&ZIM=" + ZIM.Value + "&Lang=" + Lang.Value + "#PHOTO");
        }
        else ErrorLabel.Text = "Per rimuovere il contenuto è necessario confermare l'eliminazione.";
    }//fine EseguiButton_Click

}