﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">

    static string TitoloPagina = "Inserisci nuova fotografia";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;


        delinea myDeliena = new delinea();

        if (!myDeliena.AntiSQLInjectionNew(Request.QueryString, "ZID", "string")
            || !myDeliena.AntiSQLInjectionNew(Request.QueryString, "ZIM", "string")
            || !myDeliena.AntiSQLInjectionNew(Request.QueryString, "RetUrl", "string") 
            || !myDeliena.AntiSQLInjectionNew(Request.QueryString, "XRI", "int")
            || !myDeliena.AntiSQLInjectionNew(Request.QueryString, "Lang", "string"))
            Response.Redirect("~/Zeus/System/Message.aspx?0");


        ZeusIdModulo.Value = Request.QueryString["ZIM"];
        XRI.Value = Request.QueryString["XRI"];
        Lang.Value = Request.QueryString["Lang"];
        RetUrl.Value = Request.QueryString["RetUrl"];
        ZID.Value = Request.QueryString["ZID"];
        //ReadXML(ZeusIdModulo.Value, Lang.Value);
     
    }//fine Page_Load


    //##############################################################################################################
    //################################################ FILE UPLOAD #################################################
    //############################################################################################################## 

    protected void FileUploadCustomValidator_ServerValidate(object sender, ServerValidateEventArgs args)
    {
        try
        {
            ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");
            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");

            if (ImageRaider1.isValid())
                InsertButton.CommandName = "Insert";
            else
                InsertButton.CommandName = string.Empty;

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine FileUploadCustomValidator
    //##############################################################################################################
    //########################################### FINE FILE UPLOAD #################################################
    //##############################################################################################################


    private int getOrdinamento()
    {

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();



                sqlQuery = "SELECT COUNT(ID1Photo)";
                sqlQuery += " FROM [tbPhotoGallery_Simple] ";
                sqlQuery += "WHERE [ID2ZeusLocator]=@ID2ZeusLocator ";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID2ZeusLocator", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@ID2ZeusLocator"].Value = new System.Data.SqlTypes.SqlGuid(ZID.Value);
                

                int myCount = Convert.ToInt32(command.ExecuteScalar());
                ++myCount;

                if (myCount != null)
                    return myCount;
                else return 1;

            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
                return -1;
            }
        }//fine Using
    }//fine ClearAllTables

   
    protected void InsertButton_Click(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();

        LinkButton InsertButton = (LinkButton)sender;

        HiddenField FileNameBetaHiddenField = (HiddenField)FormView1.FindControl("FileNameBetaHiddenField");
        HiddenField FileNameGammaHiddenField = (HiddenField)FormView1.FindControl("FileNameGammaHiddenField");
        HiddenField Image12AltHiddenField = (HiddenField)FormView1.FindControl("Image12AltHiddenField");
        ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");

        HiddenField Ordinamento = (HiddenField)FormView1.FindControl("Ordinamento");

        
        if (InsertButton.CommandName != string.Empty)
        {

             if (ImageRaider1.GenerateBeta(string.Empty))
			FileNameBetaHiddenField.Value = ImageRaider1.ImgBeta_FileName;
		else FileNameBetaHiddenField.Value = ImageRaider1.DefaultBetaImage;

           if (ImageRaider1.GenerateGamma(string.Empty))
			FileNameGammaHiddenField.Value = ImageRaider1.ImgGamma_FileName;
		else FileNameGammaHiddenField.Value = ImageRaider1.DefaultGammaImage;


            Ordinamento.Value = getOrdinamento().ToString();
            
        }//fine if command
        
    }// fine InsertButton_Click 


    protected void tbPhotoGallery_SimpleSqlDataSource_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.AffectedRows == 0 || e.Exception != null)
        {
            Response.Redirect("~/Zeus/System/Message.aspx?InsertErr");
        }



        Response.Redirect(RetUrl.Value+"?XRI=" + XRI.Value + "&ZIM=" + ZeusIdModulo.Value + "&Lang=" + Lang.Value + "#PHOTO");

        
    }//fine tbPhotoGallery_SimpleSqlDataSource_Inserted


    protected void FormView1_DataBound(object sender, EventArgs e)
    {
        FormView myFV = (FormView)sender;
        HiddenField hfZeusLangCode = (HiddenField)myFV.FindControl("hfZeusLangCode");
        hfZeusLangCode.Value = Lang.Value.ToString();
    }

   

    protected void ImageRaider1_Init(object sender, EventArgs e)
    {
        ImageRaider myImageRaiderCrop = (ImageRaider)sender;
    }
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" />
    <asp:HiddenField ID="ZID" runat="server" />
    <asp:HiddenField ID="XRI" runat="server" />
    <asp:HiddenField ID="ZeusIdModulo" runat="server" />
    <asp:HiddenField ID="Lang" runat="server" />
    <asp:HiddenField ID="RetUrl" runat="server" />
    
    
    <asp:FormView ID="FormView1" runat="server" DataKeyNames="ID1Photo" DataSourceID="tbPhotoGallery_SimpleSqlDataSource"  OnDataBound="FormView1_DataBound"
        DefaultMode="Insert" Width="100%">
        <InsertItemTemplate>
            <dlc:ImageRaider ID="ImageRaider1" runat="server" ZeusIdModuloIndice="P" ZeusLangCode="ITA" OnInit="ImageRaider1_Init" 
                BindPzFromDB="true" Required="true"  />
            <asp:HiddenField ID="FileNameBetaHiddenField" runat="server" Value='<%# Bind("Immagine1") %>' />
            <asp:HiddenField ID="FileNameGammaHiddenField" runat="server" Value='<%# Bind("Immagine2") %>' />
            <asp:HiddenField ID="Image12AltHiddenField" runat="server" Value='<%# Bind("Immagine12Alt") %>' />
            <asp:CustomValidator ID="FileUploadCustomValidator" runat="server" OnServerValidate="FileUploadCustomValidator_ServerValidate"
                Display="Dynamic" SkinID="ZSSM_Validazione01" Visible="false"></asp:CustomValidator>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="IdentificativoLabel" runat="server">Pubblica</asp:Label></div>
                <table>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="ZML_Articolo" SkinID="FieldDescription" runat="server" Text="Pubblica"></asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:CheckBox ID="PubblicaCheckBox" runat="server" Checked='<%# Bind("Pubblica") %>' />
                        </td>
                    </tr>
                </table>
            </div>
            <asp:HiddenField ID="Ordinamento" runat="server" Value='<%# Bind("Ordinamento") %>' />
             <div align="center">
                <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert"
                    SkinID="ZSSM_Button01" Text="Salva dati" OnClick="InsertButton_Click"></asp:LinkButton></div>
            <asp:HiddenField ID="hfZeusLangCode" runat="server" Value='<%# Bind("ZeusLangCode") %>' />
        </InsertItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="tbPhotoGallery_SimpleSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        InsertCommand="INSERT INTO tbPhotoGallery_Simple 
        (ID2ZeusLocator,Immagine1, Immagine2 , Immagine12Alt, ZeusLangCode, Pubblica,Ordinamento,DataCreazione, ZeusIdModulo)
VaLUES (@ID2ZeusLocator,@Immagine1 , @Immagine2 , @Immagine12Alt, @ZeusLangCode, @Pubblica,@Ordinamento, GETDATE(), @ZeusIdModulo)" 
        oninserted="tbPhotoGallery_SimpleSqlDataSource_Inserted">
        <InsertParameters>
            <asp:Parameter Name="Immagine1" Type="String" />
            <asp:Parameter Name="Immagine2" Type="String" />
            <asp:Parameter Name="Immagine12Alt" Type="String" />
            <asp:Parameter Name="ZeusLangCode" Type="String" Size="3" />
            <asp:Parameter Name="Pubblica" Type="Boolean" />
            <asp:Parameter Name="Ordinamento" Type="Int32" />
            <asp:ControlParameter Name="ID2ZeusLocator" ControlID="ZID" DbType="Guid" PropertyName="Value" />
            <asp:ControlParameter Name="ZeusIdModulo" ControlID="ZeusIdModulo" DbType="String" Size="5" PropertyName="Value" />
        </InsertParameters>
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="0" Name="ID2Locator" QueryStringField="XRI"
                Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
