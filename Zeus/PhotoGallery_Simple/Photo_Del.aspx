﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="Photo_Del.aspx.cs"
    Inherits="Photo_Del"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server"  />
     <asp:HiddenField ID="XRI" runat="server" />
    <asp:HiddenField ID="ID1Photo" runat="server" />
    <asp:HiddenField ID="ZIM" runat="server" />
    <asp:HiddenField ID="RetUrl" runat="server" />
    <asp:HiddenField ID="Lang" runat="server" />
    <div class="BlockBox">
        <div class="BlockBoxHeader">
            <asp:Label ID="Label6" runat="server">Elminazione contenuto</asp:Label>
        </div>
        <table>
        <tr>
            <td>
                <asp:Label ID="Label1" SkinID="FieldDescription"  Font-Bold="true" runat="server">Attenzione! Stai per eliminare il contenuto:<br /> </asp:Label>
                    <asp:Label ID="TitoloLabel" SkinID="FieldDescription"   runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
            <td>
                    <asp:CheckBox ID="ConfermaCheckBox" runat="server" Checked="false" />
                    <asp:Label ID="Label11" SkinID="FieldDescription" runat="server">Conferma eliminazione</asp:Label>
                    <asp:Label ID="ErrorLabel" SkinID="Validazione01" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <div align="center">
        <asp:LinkButton ID="EseguiLinkButton" runat="server" Text="Elimina" SkinID="ZSSM_Button01"
            OnClick="EseguiButton_Click"></asp:LinkButton></div>
</asp:Content>
