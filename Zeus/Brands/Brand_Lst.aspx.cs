﻿using System;
using System.Web.UI.WebControls;
using System.Web.Security;
using ASP;

/// <summary>
/// Descrizione di riepilogo per Brand_Lst
/// </summary>
public partial class Brands_Brand_Lst : System.Web.UI.Page
{
    static string TitoloPagina = "Nuova brand";

    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();
        TitleField.Value = TitoloPagina;

        if (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM"))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI"))
            && (Request.QueryString["XRI"].Length > 0) && (isOK()))
            SingleRecordPanel.Visible = true;

        if (!ReadXML(Request.QueryString["ZIM"], GetLang()))
            Response.Redirect("~/Zeus/System/Message.aspx?1");

        ReadXML_Localization(Request.QueryString["ZIM"], GetLang());
    }

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Brands.xml"));

            TitleField.Value = mydoc.SelectSingleNode(myXPath + "ZML_TitoloPagina_Lst").InnerText;

            GridView2.Columns[6].HeaderText =
                GridView1.Columns[6].HeaderText = mydoc.SelectSingleNode(myXPath + "ZML_Immagine1_Lst").InnerText;

            GridView2.Columns[7].HeaderText =
                GridView1.Columns[7].HeaderText = mydoc.SelectSingleNode(myXPath + "ZML_Immagine2_Lst").InnerText;

            GridView2.Columns[8].HeaderText =
                GridView1.Columns[8].HeaderText = mydoc.SelectSingleNode(myXPath + "ZML_PWArea1Lst").InnerText;

            GridView2.Columns[9].HeaderText =
                GridView1.Columns[9].HeaderText = mydoc.SelectSingleNode(myXPath + "ZML_PWArea2Lst").InnerText;
        }
        catch (Exception p)
        {
        }
    }

    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_Brands.xml"));

            if (!CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_Lst").InnerText))
                Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223599");

            GridView1.Columns[10].Visible = GridView2.Columns[10].Visible =
                CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_Edt").InnerText);
            
            GridView2.Columns[7].Visible = GridView1.Columns[7].Visible =
                Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Immagine2_Lst").InnerText);

            GridView2.Columns[3].Visible = GridView1.Columns[3].Visible =
                Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Codice1").InnerText);

            GridView2.Columns[4].Visible = GridView1.Columns[4].Visible =
                Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Link1").InnerText);

            GridView2.Columns[9].Visible = GridView1.Columns[9].Visible =
                Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_PWArea2").InnerText);

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    private bool CheckPermit(string AllRoles)
    {
        bool IsPermit = false;

        try
        {
            if (AllRoles.Length == 0)
                return false;

            char[] delimiter = { ',' };

            foreach (string roles in Roles.GetRolesForUser(Membership.GetUser().UserName))
            {
                string[] PressRoles = AllRoles.Split(delimiter);

                for (int i = 0; i < PressRoles.Length; ++i)
                {
                    if (PressRoles[i].Trim().Equals(roles.Trim()))
                    {
                        IsPermit = true;
                        break;
                    }
                }
            }
        }
        catch (Exception)
        { }

        return IsPermit;
    }

    private bool isOK()
    {
        try
        {
            if ((Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/Brands/Brand_Edt.aspx") > 0)
                || (Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/Brands/Brand_New.aspx") > 0)
                || (Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/Brands/Brand_New_Lang.aspx") > 0))
                return true;
            else return false;
        }
        catch (Exception p)
        {
            return false;
        }
    }

    private string GetLang()
    {
        try
        {
            delinea myDelinea = new delinea();
            string Lang = "ITA";

            if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
            {
                if (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang"))
                    Lang = Request.QueryString["Lang"];
            }

            return Lang;
        }
        catch (Exception)
        {
            return "ITA";
        }
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.DataItemIndex > -1)
            {
                Image Image1 = (Image)e.Row.FindControl("Image1");
                Label AttivoArea1 = (Label)e.Row.FindControl("AttivoArea1");
                int intAttivoArea1 = Convert.ToInt32(AttivoArea1.Text.ToString());

                if (intAttivoArea1 == 1)
                    Image1.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";

                Image Image2 = (Image)e.Row.FindControl("Image2");
                Label AttivoArea2 = (Label)e.Row.FindControl("AttivoArea2");
                int intAttivoArea2 = Convert.ToInt32(AttivoArea2.Text.ToString());

                if (intAttivoArea2 == 1)
                    Image2.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";

                Label ZeusUserEditableLabel = (Label)e.Row.FindControl("ZeusUserEditableLabel");
                e.Row.Cells[7].Enabled = Convert.ToBoolean(ZeusUserEditableLabel.Text);
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    protected void Link1HyperLink_DataBinding(object sender, EventArgs e)
    {
        HyperLink Link1HyperLink = (HyperLink)sender;

        if (Link1HyperLink.NavigateUrl.Length == 0)
            Link1HyperLink.Visible = false;
    }
}