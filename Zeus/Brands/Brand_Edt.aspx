﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="Brand_Edt.aspx.cs"
    Inherits="Brands_Brand_Edt"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <script type="text/javascript">
        function OnClientModeChange(editor) {
            var mode = editor.get_mode();
            var doc = editor.get_document();
            var head = doc.getElementsByTagName("HEAD")[0];
            var link;

            switch (mode) {
                case 1: //remove the external stylesheet when displaying the content in Design mode    
                    //var external = doc.getElementById("external");
                    //head.removeChild(external);
                    break;
                case 2:
                    break;
                case 4: //apply your external css stylesheet to Preview mode    
                    link = doc.createElement("LINK");
                    link.setAttribute("href", "/SiteCss/Telerik.css");
                    link.setAttribute("rel", "stylesheet");
                    link.setAttribute("type", "text/css");
                    link.setAttribute("id", "external");
                    head.appendChild(link);
                    break;
            }
        }

        function editorCommandExecuted(editor, args) {
            if (!$telerik.isChrome)
                return;
            var dialogName = args.get_commandName();
            var dialogWin = editor.get_dialogOpener()._dialogContainers[dialogName];
            if (dialogWin) {
                var cellEl = dialogWin.get_contentElement() || dialogWin.ui.contentCell || dialogWin.ui.content,
                frame = dialogWin.get_contentFrame();
                frame.onload = function () {
                    cellEl.style.cssText = "";
                    dialogWin.autoSize();
                }
            }
        }
    </script>

    <asp:HiddenField ID="TitleField" runat="server" />
    <asp:HiddenField ID="ZIM" runat="server" />
    <asp:HiddenField ID="XRI" runat="server" />
    
    <asp:FormView ID="FormView1" runat="server" DataSourceID="tbBrandsSqlDataSource"
        DefaultMode="Edit" Width="100%">
        <EditItemTemplate>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="IdentificativoLabel" runat="server">Identificativo brand</asp:Label>
                </div>
                <table>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="BrandLabel" SkinID="FieldDescription" runat="server" Text="Brand *"></asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:TextBox ID="BrandTextBox" runat="server" Columns="50" MaxLength="50" Text='<%# Bind("Brand") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator1"
                                runat="server" ControlToValidate="BrandTextBox" SkinID="ZSSM_Validazione01" Display="Dynamic"
                                ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <asp:Panel ID="ZMCF_Link1" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="SitoWebLabel" SkinID="FieldDescription" runat="server" Text="Sito Web"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="Link1TextBox" runat="server" Columns="100" MaxLength="100" Text='<%# Bind("Link1") %>'></asp:TextBox>
                                <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="Url_Validator"
                                    runat="server" ControlToValidate="Link1TextBox" Display="Dynamic" ErrorMessage="Inserire un percorso completo e corretto"
                                    ValidationExpression="^(ht|f)tp(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&%\$#_]*)?$"
                                    SkinID="ZSSM_Validazione01">Inserire un percorso completo e corretto</asp:RegularExpressionValidator>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_Codice1" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="PZL_Codice1" SkinID="FieldDescription" runat="server" Text="Codice"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="Codice1TextBox" runat="server" Columns="20" MaxLength="20" Text='<%# Bind("Codice1") %>'></asp:TextBox>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_Categoria" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label11" runat="server" SkinID="FieldDescription" Text="Label">Categoria *</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:DropDownList ID="ID2CategoriaDropDownList" runat="server" OnSelectedIndexChanged="ID2CategoriaDropDownList_SelectIndexChange"
                                    AppendDataBoundItems="True">
                                </asp:DropDownList>
                                <asp:HiddenField ID="ID2CategoriaHiddenField" runat="server" Value='<%# Bind("ID2Categoria") %>' />
                                <asp:RequiredFieldValidator ID="ID2CategoriaRequiredFieldValidator" ErrorMessage="Obbligatorio"
                                    runat="server" ControlToValidate="ID2CategoriaDropDownList" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" InitialValue="0" ValidationGroup="myValidation">
                                </asp:RequiredFieldValidator>
                                <asp:SqlDataSource ID="dsCategoria" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"></asp:SqlDataSource>
                            </td>
                        </tr>
                    </asp:Panel>
                </table>
            </div>
            <dlc:ImageRaider ID="ImageRaider1" runat="server" ZeusIdModuloIndice="1" ZeusLangCode="ITA"
                BindPzFromDB="true" IsEditMode="true" DefaultEditBetaImage='<%# Eval("Immagine1") %>'
                ImageAlt='<%# Eval("Immagine1Alt") %>'
                OnDataBinding="ImageRaider_DataBinding" />
            <asp:HiddenField ID="FileNameBeta1HiddenField" runat="server" Value='<%# Bind("Immagine1") %>' />
            <asp:HiddenField ID="Image1AltHiddenField" runat="server" Value='<%# Bind("Immagine1Alt") %>' />
            <asp:CustomValidator ID="FileUpload1CustomValidator" runat="server" OnServerValidate="FileUploadCustomValidator_ServerValidate"
                Display="Dynamic" SkinID="ZSSM_Validazione01" ValidationGroup="myValidation"
                Visible="false"></asp:CustomValidator>
            <asp:Panel ID="ZMCF_Immagine2" runat="server">
                <dlc:ImageRaider ID="ImageRaider2" runat="server" ZeusIdModuloIndice="2" ZeusLangCode="ITA"
                    BindPzFromDB="true" IsEditMode="true" DefaultEditBetaImage='<%# Eval("Immagine2") %>'
                    ImageAlt='<%# Eval("Immagine2Alt") %>'
                    OnDataBinding="ImageRaider_DataBinding" />
                <asp:HiddenField ID="FileNameBeta2HiddenField" runat="server" Value='<%# Bind("Immagine2") %>' />
                <asp:HiddenField ID="Image2AltHiddenField" runat="server" Value='<%# Bind("Immagine2Alt") %>' />
                <asp:CustomValidator ID="FileUpload2CustomValidator" runat="server" OnServerValidate="FileUpload2CustomValidator_ServerValidate"
                    Display="Dynamic" SkinID="ZSSM_Validazione01" ValidationGroup="myValidation"
                    Visible="false"></asp:CustomValidator>
            </asp:Panel>
            <asp:Panel ID="ZMCF_BoxDescrizione" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="ZML_DescrizioneBox1" runat="server">Identificativo brand</asp:Label>
                    </div>
                    <table>
                        <asp:Panel ID="ZMCF_DescBreve1" runat="server">
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label ID="ZML_DescBreve1" SkinID="FieldDescription" runat="server" Text="Descrizione breve"></asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <CustomWebControls:TextArea runat="server" ID="DescBreveTextBox" Columns="100" Rows="4"
                                        TextMode="MultiLine" ValidationGroup="myValidation" Text='<%# Bind("DescBreve1") %>'
                                        EnableTheming="True" Height="100px" MaxLength="500"></CustomWebControls:TextArea>&nbsp;<br />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <img src="../SiteImg/Spc.gif" height="10" />
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Descrizione1" runat="server">
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label ID="ZML_Descrizione1" SkinID="FieldDescription" runat="server" Text="Contenuto"></asp:Label>
                                </td>
                                <td>
                                    <div id="BRNDS1_Contenuto1">
                                        <telerik:RadEditor
                                            Language="it-IT" ID="RadEditor1" runat="server"
                                            DocumentManager-DeletePaths="~/ZeusInc/Brands/Documents"
                                            DocumentManager-SearchPatterns="*.*"
                                            DocumentManager-ViewPaths="~/ZeusInc/Brands/Documents"
                                            DocumentManager-MaxUploadFileSize="52428800"
                                            DocumentManager-UploadPaths="~/ZeusInc/Brands/Documents"
                                            FlashManager-DeletePaths="~/ZeusInc/Brands/Media"
                                            FlashManager-MaxUploadFileSize="10240000"
                                            FlashManager-ViewPaths="~/ZeusInc/Brands/Media"
                                            FlashManager-UploadPaths="~/ZeusInc/Brands/Media"
                                            ImageManager-DeletePaths="~/ZeusInc/Brands/Images"
                                            ImageManager-ViewPaths="~/ZeusInc/Brands/Images"
                                            ImageManager-MaxUploadFileSize="10240000"
                                            ImageManager-SearchPatterns="*.gif, *.png, *.jpg, *.jpe, *.jpeg"
                                            ImageManager-UploadPaths="~/ZeusInc/Brands/Images"
                                            ImageManager-ViewMode="Grid"
                                            MediaManager-DeletePaths="~/ZeusInc/Brands/Media"
                                            MediaManager-MaxUploadFileSize="10240000"
                                            MediaManager-SearchPatterns="*.asf, *.asx, *.wma, *.wmv, *.avi, *.wav, *.mpeg, *.mpg, *.mpe, *.mov, *.mp3, *.m3u, *.mid, *.midi, *.rm, *.rma"
                                            MediaManager-ViewPaths="~/ZeusInc/Brands/Media"
                                            MediaManager-UploadPaths="~/ZeusInc/Brands/Media"
                                            TemplateManager-SearchPatterns="*.html,*.htm"
                                            ContentAreaMode="iframe"
                                            OnClientCommandExecuted="editorCommandExecuted"
                                            OnClientModeChange="OnClientModeChange"
                                            Content='<%# Bind("Descrizione1") %>'
                                            ToolsFile="~/Zeus/Brands/RadEditor1.xml"
                                            LocalizationPath="~/App_GlobalResources"
                                            AllowScripts="true" RenderMode="Classic" ToolbarMode="Default" EnableViewState="False"
                                            Width="700px" Height="500px">
                                            <CssFiles>
                                                <telerik:EditorCssFile Value="~/asset/css/ZeusTypeFoundry.css" />
                                            </CssFiles>
                                        </telerik:RadEditor>
                                        <%--<rade:radeditor stripabsoluteanchorpaths="true" language="it-IT" id="RadEditor1" runat="server" converttagstolower="True" converttoxhtml="False"
                                            copycsstoformatblocktool="False" deletedocumentspaths="~/ZeusInc/Brands/Documents"
                                            deleteflashpaths="~/ZeusInc/Brands/Media" deleteimagespaths="~/ZeusInc/Brands/Images"
                                            deletemediapaths="~/ZeusInc/Brands/Media" documentsfilters="*.*" documentspaths="~/ZeusInc/Brands/Documents"
                                            enableclientserialize="False" enablecontextmenus="True" enableenhancededit="True"
                                            enablehtmlindentation="True" enableserversiderendering="True" enabletab="True"
                                            maxdocumentsize="10240000" maxflashsize="10240000" maximagesize="10240000" maxmediasize="10240000"
                                            flashpaths="~/ZeusInc/Brands/Media" height="300px" html='<%# Bind("Descrizione1") %>'
                                            imagesfilters="*.gif, *.png, *.jpg, *.jpe, *.jpeg" imagespaths="~/ZeusInc/Brands/Images"
                                            mediafilters="*.asf, *.asx, *.wma, *.wmv, *.avi, *.wav, *.mpeg, *.mpg, *.mpe, *.mov, *.mp3, *.m3u, *.mid, *.midi, *.rm, *.rma"
                                            mediapaths="~/ZeusInc/Brands/Media" onclientcancel="" onclientcommandexecuted=""
                                            onclientcommandexecuting="" onclientinit="" onclientload="" onclientmodechange=""
                                            allowscripts="true" onclientsubmit="" passsessiondata="True" renderastextarea="False"
                                            templatefilters="*.html,*.htm" toolbarmode="Default" toolswidth="" uploaddocumentspaths="~/ZeusInc/Brands/Documents"
                                            uploadflashpaths="~/ZeusInc/Brands/Media" uploadimagespaths="~/ZeusInc/Brands/Images"
                                            uploadmediapaths="~/ZeusInc/Brands/Media" usefixedtoolbar="False" cssfiles="~/asset/css/ZeusTypeFoundry.css,~/SiteCss/ZeusSnippets.css"
                                            width="700px" toolsfile="~/Zeus/Brands/RadEditor1.xml" enableviewstate="False"
                                            enabledocking="False">
                                        </rade:radeditor>--%>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <img src="../SiteImg/Spc.gif" height="10" />
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Descrizione2" runat="server">
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label ID="ZML_Descrizione2" SkinID="FieldDescription" runat="server" Text="Contenuto"></asp:Label>
                                </td>
                                <td>
                                    <div id="BRNDS2_Contenuto2">
                                        <telerik:RadEditor
                                            Language="it-IT" ID="RadEditor2" runat="server"
                                            DocumentManager-DeletePaths="~/ZeusInc/Brands/Documents"
                                            DocumentManager-SearchPatterns="*.*"
                                            DocumentManager-ViewPaths="~/ZeusInc/Brands/Documents"
                                            DocumentManager-MaxUploadFileSize="52428800"
                                            DocumentManager-UploadPaths="~/ZeusInc/Brands/Documents"
                                            FlashManager-DeletePaths="~/ZeusInc/Brands/Media"
                                            FlashManager-MaxUploadFileSize="10240000"
                                            FlashManager-ViewPaths="~/ZeusInc/Brands/Media"
                                            FlashManager-UploadPaths="~/ZeusInc/Brands/Media"
                                            ImageManager-DeletePaths="~/ZeusInc/Brands/Images"
                                            ImageManager-ViewPaths="~/ZeusInc/Brands/Images"
                                            ImageManager-MaxUploadFileSize="10240000"
                                            ImageManager-SearchPatterns="*.gif, *.png, *.jpg, *.jpe, *.jpeg"
                                            ImageManager-UploadPaths="~/ZeusInc/Brands/Images"
                                            ImageManager-ViewMode="Grid"
                                            MediaManager-DeletePaths="~/ZeusInc/Brands/Media"
                                            MediaManager-MaxUploadFileSize="10240000"
                                            MediaManager-SearchPatterns="*.asf, *.asx, *.wma, *.wmv, *.avi, *.wav, *.mpeg, *.mpg, *.mpe, *.mov, *.mp3, *.m3u, *.mid, *.midi, *.rm, *.rma"
                                            MediaManager-ViewPaths="~/ZeusInc/Brands/Media"
                                            MediaManager-UploadPaths="~/ZeusInc/Brands/Media"
                                            TemplateManager-SearchPatterns="*.html,*.htm"
                                            ContentAreaMode="iframe"
                                            OnClientCommandExecuted="editorCommandExecuted"
                                            OnClientModeChange="OnClientModeChange"
                                            Content='<%# Bind("Descrizione2") %>'
                                            ToolsFile="~/Zeus/Brands/RadEditor2.xml"
                                            LocalizationPath="~/App_GlobalResources"
                                            AllowScripts="true" RenderMode="Classic" ToolbarMode="Default" EnableViewState="False"
                                            Width="700px" Height="500px">
                                            <CssFiles>
                                                <telerik:EditorCssFile Value="~/asset/css/ZeusTypeFoundry.css" />
                                            </CssFiles>
                                        </telerik:RadEditor>
                                        <%--<rade:radeditor stripabsoluteanchorpaths="true" language="it-IT" id="RadEditor2" runat="server" converttagstolower="True" converttoxhtml="False"
                                            copycsstoformatblocktool="False" deletedocumentspaths="~/ZeusInc/Brands/Documents"
                                            deleteflashpaths="~/ZeusInc/Brands/Media" deleteimagespaths="~/ZeusInc/Brands/Images"
                                            deletemediapaths="~/ZeusInc/Brands/Media" documentsfilters="*.*" documentspaths="~/ZeusInc/Brands/Documents"
                                            enableclientserialize="False" enablecontextmenus="True" enableenhancededit="True"
                                            enablehtmlindentation="True" enableserversiderendering="True" enabletab="True"
                                            maxdocumentsize="10240000" maxflashsize="10240000" maximagesize="10240000" maxmediasize="10240000"
                                            flashpaths="~/ZeusInc/Brands/Media" height="300px" html='<%# Bind("Descrizione2") %>'
                                            imagesfilters="*.gif, *.png, *.jpg, *.jpe, *.jpeg" imagespaths="~/ZeusInc/Brands/Images"
                                            mediafilters="*.asf, *.asx, *.wma, *.wmv, *.avi, *.wav, *.mpeg, *.mpg, *.mpe, *.mov, *.mp3, *.m3u, *.mid, *.midi, *.rm, *.rma"
                                            mediapaths="~/ZeusInc/Brands/Media" onclientcancel="" onclientcommandexecuted=""
                                            onclientcommandexecuting="" onclientinit="" onclientload="" onclientmodechange=""
                                            allowscripts="true" onclientsubmit="" passsessiondata="True" renderastextarea="False"
                                            templatefilters="*.html,*.htm" toolbarmode="Default" toolswidth="" uploaddocumentspaths="~/ZeusInc/Brands/Documents"
                                            uploadflashpaths="~/ZeusInc/Brands/Media" uploadimagespaths="~/ZeusInc/Brands/Images"
                                            uploadmediapaths="~/ZeusInc/Brands/Media" usefixedtoolbar="False" cssfiles="~/asset/css/ZeusTypeFoundry.css,~/SiteCss/ZeusSnippets.css"
                                            width="700px" toolsfile="~/Zeus/Brands/RadEditor2.xml" enableviewstate="False"
                                            enabledocking="False">
                                        </rade:radeditor>--%>
                                    </div>
                                </td>
                            </tr>
                        </asp:Panel>
                    </table>
                </div>
            </asp:Panel>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="Label6" runat="server">Planner</asp:Label>
                </div>
                <table>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="ZML_PWArea1" SkinID="FieldDescription" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:CheckBox ID="PW_Area1CheckBox" runat="server" Checked='<%# Bind("PW_Area1") %>' />
                            <asp:Label ID="Label20" runat="server" SkinID="FieldValue" Text="Label">Attiva pubblicazione dalla data</asp:Label>
                            <asp:TextBox ID="PWI_Area1TextBox" runat="server" Columns="10" MaxLength="10" Text='<%# Bind("PWI_Area1", "{0:d}") %>'
                                OnDataBinding="PWI_Area"></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator4"
                                runat="server" ControlToValidate="PWI_Area1TextBox" SkinID="ZSSM_Validazione01"
                                Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="RegularExpressionValidator3"
                                runat="server" ControlToValidate="PWI_Area1TextBox" SkinID="ZSSM_Validazione01"
                                Display="Dynamic" ErrorMessage="Formato data richiesto: GG/MM/AAAA" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>&nbsp;
                            <asp:Label ID="Label9" runat="server" SkinID="FieldValue">alla data </asp:Label>&nbsp;
                            <asp:TextBox ID="PWF_Area1TextBox" runat="server" Columns="10" MaxLength="10" Text='<%# Bind("PWF_Area1", "{0:d}") %>'
                                OnDataBinding="PWF_Area"></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator8"
                                runat="server" ControlToValidate="PWF_Area1TextBox" SkinID="ZSSM_Validazione01"
                                Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="RegularExpressionValidator5"
                                runat="server" ControlToValidate="PWF_Area1TextBox" SkinID="ZSSM_Validazione01"
                                Display="Dynamic" ErrorMessage="Formato data richiesto: GG/MM/AAAA" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>
                            <asp:CompareValidator ValidationGroup="myValidation" ID="CompareValidator1" runat="server"
                                ControlToCompare="PWF_Area1TextBox" ControlToValidate="PWI_Area1TextBox" SkinID="ZSSM_Validazione01"
                                Display="Dynamic" ErrorMessage="La data iniziale deve essere precedente o uguale alla data finale"
                                Operator="LessThanEqual" Type="Date"></asp:CompareValidator>
                        </td>
                    </tr>
                    <asp:Panel ID="ZMCF_PWArea2" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_PWArea2" SkinID="FieldDescription" runat="server" Text="Label"></asp:Label>
                                <td class="BlockBoxValue">
                                    <asp:CheckBox ID="PW_Area2CheckBox" runat="server" Checked='<%# Bind("PW_Area2") %>' />
                                    <asp:Label ID="Label7" runat="server" SkinID="FieldValue" Text="Label">Attiva pubblicazione dalla data</asp:Label>
                                    <asp:TextBox ID="PWI_Area2TextBox" runat="server" Columns="10" MaxLength="10" Text='<%# Bind("PWI_Area2", "{0:d}") %>'
                                        OnDataBinding="PWI_Area"></asp:TextBox>
                                    <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator9"
                                        runat="server" ControlToValidate="PWI_Area2TextBox" SkinID="ZSSM_Validazione01"
                                        Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="RegularExpressionValidator4"
                                        runat="server" ControlToValidate="PWI_Area2TextBox" SkinID="ZSSM_Validazione01"
                                        Display="Dynamic" ErrorMessage="Formato data richiesto: GG/MM/AAAA" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>&nbsp;
                                    <asp:Label ID="Label12" runat="server" SkinID="FieldValue">alla data </asp:Label>&nbsp;
                                    <asp:TextBox ID="PWF_Area2TextBox" runat="server" Columns="10" MaxLength="10" Text='<%# Bind("PWF_Area2", "{0:d}") %>'
                                        OnDataBinding="PWF_Area"></asp:TextBox>
                                    <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator10"
                                        runat="server" ControlToValidate="PWF_Area2TextBox" SkinID="ZSSM_Validazione01"
                                        Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="RegularExpressionValidator6"
                                        runat="server" ControlToValidate="PWF_Area2TextBox" SkinID="ZSSM_Validazione01"
                                        Display="Dynamic" ErrorMessage="Formato data richiesto: GG/MM/AAAA" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>
                                    <asp:CompareValidator ValidationGroup="myValidation" ID="CompareValidator2" runat="server"
                                        ControlToCompare="PWF_Area2TextBox" ControlToValidate="PWI_Area2TextBox" SkinID="ZSSM_Validazione01"
                                        Display="Dynamic" ErrorMessage="La data iniziale deve essere precedente o uguale alla data finale"
                                        Operator="LessThanEqual" Type="Date"></asp:CompareValidator>
                                </td>
                        </tr>
                    </asp:Panel>
                </table>
            </div>
            <asp:HiddenField ID="RecordEdtUserHiddenField" runat="server" OnDataBinding="RecordEdtUserHidden_DataBinding"
                Value='<%# Bind("RecordEdtUser", "{0}") %>' />
            <asp:HiddenField ID="RecordEdtDateHiddenField" runat="server" OnDataBinding="RecordEdtDateHidden_DataBinding"
                Value='<%# Bind("RecordEdtDate", "{0}") %>' />
            <asp:HiddenField ID="ZeusUserEditable" runat="server" Value='<%# Eval("ZeusUserEditable") %>'
                OnDataBinding="ZeusUserEditable_DataBinding" />
            <div align="center">
                <dlc:mySummaryValidation ID="mySummaryValidation1" runat="server" SkinID="ZSSM_Validazione01" />
                <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Update"
                    SkinID="ZSSM_Button01" Text="Salva dati" OnClick="InsertButton_Click" ValidationGroup="myValidation"></asp:LinkButton>
            </div>
            <asp:HiddenField ID="zdtRecordNewUser" runat="server" Value='<%# Eval("RecordNewUser") %>' />
            <asp:HiddenField ID="zdtRecordNewDate" runat="server" Value='<%# Eval("RecordNewDate") %>' />
            <asp:HiddenField ID="zdtRecordEdtUser" runat="server" Value='<%# Eval("RecordEdtUser") %>' />
            <asp:HiddenField ID="zdtRecordEdtDate" runat="server" Value='<%# Eval("RecordEdtDate") %>' />
            <div style="padding: 10px 0 0 0; margin: 0;">
                <dlc:zeusdatatracking ID="Zeusdatatracking1" runat="server" />
            </div>
        </EditItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="tbBrandsSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SELECT Brand, Immagine1,Immagine1Alt,Immagine2,Immagine2Alt, Link1,Codice1,ZeusUserEditable,
         PW_Area1, PWI_Area1, PWF_Area1, PW_Area2, PWI_Area2, PWF_Area2 
         ,RecordNewUser,RecordNewDate,RecordEdtUser, RecordEdtDate ,[DescBreve1],[Descrizione1],[Descrizione2], ID2Categoria
         FROM tbBrands 
         WHERE [ID1Brand] = @ID1Brand AND ZeusIsAlive=1"
        UpdateCommand=" SET DATEFORMAT dmy; 
         UPDATE [tbBrands] SET [Brand] = @Brand,  [Immagine1] = @Immagine1,Immagine1Alt = @Immagine1Alt, [Immagine2] = @Immagine2,
        Immagine2Alt = @Immagine2Alt
        , [Link1] = @Link1, [Codice1]=@Codice1, [PW_Area1] = @PW_Area1, [PWI_Area1] = @PWI_Area1
        , [PWF_Area1] = @PWF_Area1, [PW_Area2] = @PW_Area2, [PWI_Area2] = @PWI_Area2
        , [PWF_Area2] = @PWF_Area2, [RecordEdtUser] = @RecordEdtUser
        , [RecordEdtDate] = @RecordEdtDate 
        ,[DescBreve1]=@DescBreve1,[Descrizione1]=@Descrizione1,[Descrizione2]=@Descrizione2, ID2Categoria = @ID2Categoria 
        WHERE [ID1Brand] = @ID1Brand"
        OnUpdated="tbBrandsSqlDataSource_Updated" OnSelected="tbBrandsSqlDataSource_Selected">
        <SelectParameters>
            <asp:QueryStringParameter Name="ID1Brand" Type="Int32" QueryStringField="XRI" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Brand" Type="String" />
            <asp:Parameter Name="Immagine1" Type="String" />
            <asp:Parameter Name="Immagine1Alt" Type="String" />
            <asp:Parameter Name="Immagine2" Type="String" />
            <asp:Parameter Name="Immagine2Alt" Type="String" />
            <asp:Parameter Name="ID2Categoria" />
            <asp:Parameter Name="DescBreve1" Type="String" />
            <asp:Parameter Name="Descrizione1" Type="String" />
            <asp:Parameter Name="Descrizione2" Type="String" />
            <asp:Parameter Name="Link1" Type="String" />
            <asp:Parameter Name="Codice1" Type="String" />
            <asp:Parameter Name="PW_Area1" Type="Boolean" />
            <asp:Parameter DbType="DateTime" Name="PWI_Area1" />
            <asp:Parameter DbType="DateTime" Name="PWF_Area1" />
            <asp:Parameter Name="PW_Area2" Type="Boolean" />
            <asp:Parameter DbType="DateTime" Name="PWI_Area2" />
            <asp:Parameter DbType="DateTime" Name="PWF_Area2" />
            <asp:Parameter Name="RecordEdtUser" />
            <asp:Parameter DbType="DateTime" Name="RecordEdtDate" />
            <asp:QueryStringParameter Name="ID1Brand" Type="Int32" QueryStringField="XRI" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
