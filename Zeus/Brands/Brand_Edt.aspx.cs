﻿using ASP;
using System;
using System.Web.Security;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per Brand_Edt
/// </summary>
public partial class Brands_Brand_Edt : System.Web.UI.Page
{
    static string TitoloPagina = "Modifica brand";

    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();
        TitleField.Value = TitoloPagina;

        if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI"))
            || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM")))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        XRI.Value = Server.HtmlEncode(Request.QueryString["XRI"]);
        ZIM.Value = Server.HtmlEncode(Request.QueryString["ZIM"]);

        if (!ReadXML(ZIM.Value, GetLang()))
            Response.Redirect("~/Zeus/System/Message.aspx?1");

        ReadXML_Localization(ZIM.Value, GetLang());

        LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");
        InsertButton.Attributes.Add("onclick", "Validate()");
    }

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";
            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Brands.xml"));

            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            if (childNode.Name.Equals("ZML_TitoloPagina_Edt"))
                            {
                                TitleField.Value = childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                                else
                                {
                                    myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name + "Label");

                                    if (myLabel != null)
                                        myLabel.Text = childNode.InnerText;
                                }
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            string myXPathEach = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_Brands.xml"));

            if (!CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_Edt").InnerText))
                Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223599");

            Panel myPanel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPathEach);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            if (childNode.Name.IndexOf("ZMCF_") > -1)
                            {
                                myPanel = (Panel)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myPanel != null)
                                    myPanel.Visible = Convert.ToBoolean(childNode.InnerText);
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }

            if (!Page.IsPostBack)
                SetQueryOfID2CategoriaDropDownList(mydoc.SelectSingleNode(myXPath + "ZMCD_ZIMCategoria1").InnerText,
                    GetLang(), mydoc.SelectSingleNode(myXPath + "ZMCD_Cat1Liv").InnerText);

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    private bool CheckPermit(string AllRoles)
    {
        bool IsPermit = false;

        try
        {
            if (AllRoles.Length == 0)
                return false;

            char[] delimiter = { ',' };

            foreach (string roles in Roles.GetRolesForUser(Membership.GetUser().UserName))
            {
                string[] PressRoles = AllRoles.Split(delimiter);

                for (int i = 0; i < PressRoles.Length; ++i)
                {
                    if (PressRoles[i].Trim().Equals(roles.Trim()))
                    {
                        IsPermit = true;
                        break;
                    }
                }
            }
        }
        catch (Exception)
        { }

        return IsPermit;
    }

    private string GetLang()
    {
        try
        {
            delinea myDelinea = new delinea();
            string Lang = "ITA";

            if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
            {
                if (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang"))
                    Lang = Request.QueryString["Lang"];
            }

            return Lang;
        }
        catch (Exception)
        {
            return "ITA";
        }
    }

    protected void RecordEdtUserHidden_DataBinding(object sender, EventArgs e)
    {
        HiddenField UtenteCreazione = (HiddenField)sender;
        UtenteCreazione.Value = Membership.GetUser().ProviderUserKey.ToString();
    }

    protected void RecordEdtDateHidden_DataBinding(object sender, EventArgs e)
    {
        HiddenField DataCreazione = (HiddenField)sender;
        DataCreazione.Value = DateTime.Now.ToString();
    }

    protected void PWI_Area(object sender, EventArgs e)
    {
        TextBox DataCreazione = (TextBox)sender;

        if (DataCreazione.Text.Length == 0)
            DataCreazione.Text = DateTime.Now.ToString("d");
    }

    protected void PWF_Area(object sender, EventArgs e)
    {
        TextBox DataFinale = (TextBox)sender;
        if (DataFinale.Text.Length == 0)
            DataFinale.Text = "31/12/2040";
    }

    protected void ZeusUserEditable_DataBinding(object sender, EventArgs e)
    {
        HiddenField ZeusUserEditable = (HiddenField)sender;

        if (!Convert.ToBoolean(ZeusUserEditable.Value))
            Response.Redirect("~/Zeus/System/Message.aspx?ZeusUserEditable");
    }

    protected void ImageRaider_DataBinding(object sender, EventArgs e)
    {
        ImageRaider ImageRaider1 = (ImageRaider)sender;
        ImageRaider1.SetDefaultEditBetaImage();
    }

    protected void FileUploadCustomValidator_ServerValidate(object sender, ServerValidateEventArgs args)
    {
        try
        {
            ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");
            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");

            if (ImageRaider1.isValid())
                InsertButton.CommandName = "Update";
            else
                InsertButton.CommandName = string.Empty;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    protected void FileUpload2CustomValidator_ServerValidate(object sender, ServerValidateEventArgs args)
    {
        try
        {
            ImageRaider ImageRaider2 = (ImageRaider)FormView1.FindControl("ImageRaider2");
            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");

            if (ImageRaider2.isValid())
                InsertButton.CommandName = "Update";
            else
                InsertButton.CommandName = string.Empty;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    protected void InsertButton_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton InsertButton = (LinkButton)sender;
            ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");
            ImageRaider ImageRaider2 = (ImageRaider)FormView1.FindControl("ImageRaider2");
            HiddenField FileNameBeta1HiddenField = (HiddenField)FormView1.FindControl("FileNameBeta1HiddenField");
            HiddenField FileNameBeta2HiddenField = (HiddenField)FormView1.FindControl("FileNameBeta2HiddenField");
            HiddenField Image1AltHiddenField = (HiddenField)FormView1.FindControl("Image1AltHiddenField");
            HiddenField Image2AltHiddenField = (HiddenField)FormView1.FindControl("Image2AltHiddenField");

            if (InsertButton.CommandName != string.Empty)
            {

                if (ImageRaider1.GenerateBeta(string.Empty))
                    FileNameBeta1HiddenField.Value = ImageRaider1.ImgBeta_FileName;
                else FileNameBeta1HiddenField.Value = ImageRaider1.DefaultBetaImage;

                if (ImageRaider2.GenerateBeta(string.Empty))
                    FileNameBeta2HiddenField.Value = ImageRaider2.ImgBeta_FileName;
                else FileNameBeta2HiddenField.Value = ImageRaider2.DefaultBetaImage;

            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    protected void tbBrandsSqlDataSource_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.Exception == null)
            Response.Redirect("~/Zeus/Brands/Brand_Lst.aspx?XRI=" + XRI.Value
                + "&ZIM=" + ZIM.Value
                + "&Lang=" + GetLang());
        else
            Response.Redirect("~/Zeus/System/Message.aspx?UpdErr");
    }

    protected void tbBrandsSqlDataSource_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {
        if ((e.Exception != null)
            || (e.AffectedRows <= 0))
            Response.Redirect("~/Zeus/System/Message.aspx?SelErr");
    }

    protected void ID2CategoriaDropDownList_SelectIndexChange(object sender, EventArgs e)
    {
        HiddenField ID2CategoriaHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaHiddenField");
        DropDownList ID2CategoriaDropDownList = (DropDownList)sender;

        ID2CategoriaHiddenField.Value = ID2CategoriaDropDownList.SelectedValue;

    }//fine ID2CategoriaDropDownList_SelectIndexChange

    private bool SetQueryOfID2CategoriaDropDownList(string ZeusIdModulo,
        string ZeusLangCode, string PZV_Cat1Liv)
    {
        try
        {


            SqlDataSource dsCategoria = (SqlDataSource)FormView1.FindControl("dsCategoria");
            DropDownList ID2CategoriaDropDownList = (DropDownList)FormView1.FindControl("ID2CategoriaDropDownList");
            HiddenField ID2CategoriaHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaHiddenField");
            //ID2CategoriaHiddenField.Value = "0";

            switch (PZV_Cat1Liv)
            {

                case "123":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv1Liv2Liv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "Catliv1Liv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;


                case "23":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[MenuLabel2]";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "MenuLabel2";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;

                default:
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;
            }


            ID2CategoriaDropDownList.SelectedIndex = ID2CategoriaDropDownList.Items.IndexOf(ID2CategoriaDropDownList.Items.FindByValue(ID2CategoriaHiddenField.Value));

            return true;
        }
        catch (Exception p)
        {
            //Response.Write(p.ToString());
            return false;
        }
    }//fine SetQueryOfID2CategoriaDropDownList
}