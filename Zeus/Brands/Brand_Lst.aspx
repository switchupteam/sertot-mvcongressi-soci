﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="Brand_Lst.aspx.cs"
    Inherits="Brands_Brand_Lst"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <asp:Panel ID="SingleRecordPanel" runat="server" Visible="false">
        <div class="LayGridTitolo1">
            <asp:Label ID="CaptionallRecordLabel" SkinID="GridTitolo1" runat="server" Text="Ultimo contenuto creato / aggiornato"></asp:Label>
        </div>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
            AutoGenerateColumns="False" DataSourceID="SingleRecordSqlDatasource" OnRowDataBound="GridView1_RowDataBound"
            PageSize="30">
            <Columns>
                <asp:TemplateField HeaderText="AttivoArea1" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="AttivoArea1" runat="server" Text='<%# Eval("AttivoArea1") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="AttivoArea2" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="AttivoArea2" runat="server" Text='<%# Eval("AttivoArea2") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Brand" HeaderText="Brand" SortExpression="Brand" />
                <asp:BoundField DataField="Codice1" HeaderText="Codice" SortExpression="Codice1" />
                <asp:TemplateField HeaderText="Sito web" SortExpression="Link1">
                    <ControlStyle CssClass="GridView_Button1" />
                    <ItemStyle HorizontalAlign="Center" />
                    <ItemTemplate>
                        <asp:HyperLink ID="Link1HyperLink" runat="server" NavigateUrl='<%# Eval("Link1", "{0}") %>'
                            OnDataBinding="Link1HyperLink_DataBinding" Target="_blank" Text='Apri'></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Categoria" HeaderText="Categoria" SortExpression="Categoria" />
                <asp:TemplateField HeaderText="Logo">
                    <ItemStyle HorizontalAlign="Center" />
                    <ItemTemplate>
                        <asp:Image ID="Immagine1" runat="server" ImageUrl='<%# Eval("Immagine1", "~/ZeusInc/Brands/Img1/{0}") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Logo">
                    <ItemStyle HorizontalAlign="Center" />
                    <ItemTemplate>
                        <asp:Image ID="Immagine2" runat="server" ImageUrl='<%# Eval("Immagine2", "~/ZeusInc/Brands/Img2/{0}") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField SortExpression="AttivoArea1">
                    <ItemTemplate>
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>
                <asp:TemplateField SortExpression="AttivoArea2">
                    <ItemTemplate>
                        <asp:Image ID="Image2" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>
                <asp:HyperLinkField DataNavigateUrlFields="ID1Brand,ZeusLangCode,ZeusIdModulo" DataNavigateUrlFormatString="Brand_Edt.aspx?XRI={0}&Lang={1}&ZIM={2}"
                    Text="Modifica">
                    <ControlStyle CssClass="GridView_Button1" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:HyperLinkField>
                <asp:TemplateField HeaderText="ZeusUserEditable" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="ZeusUserEditableLabel" runat="server" Text='<%# Eval("ZeusUserEditable") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SingleRecordSqlDatasource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
            SelectCommand="SELECT [ID1Brand],[Brand],[Codice1], Categoria, [Immagine1],[Immagine2],[ZeusIdModulo],[ZeusLangCode],[Link1],[AttivoArea1],[AttivoArea2],[ZeusUserEditable] FROM vwBrands_All WHERE ID1Brand=@ID1Brand">
            <SelectParameters>
                <asp:QueryStringParameter Name="ID1Brand" QueryStringField="XRI" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <div class="VertSpacerMedium">
        </div>
    </asp:Panel>
    <div class="LayGridTitolo1">
        <asp:Label ID="Label2" runat="server" SkinID="GridTitolo1" Text="Elenco contenuti disponibili"></asp:Label>
    </div>
    <asp:GridView ID="GridView2" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" DataSourceID="vwBrands_AllSqlDataSource" OnRowDataBound="GridView1_RowDataBound"
        PageSize="50">
        <Columns>
            <asp:TemplateField HeaderText="AttivoArea1" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="AttivoArea1" runat="server" Text='<%# Eval("AttivoArea1") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="AttivoArea2" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="AttivoArea2" runat="server" Text='<%# Eval("AttivoArea2") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Brand" HeaderText="Brand" SortExpression="Brand" />
            <asp:BoundField DataField="Codice1" HeaderText="Codice" SortExpression="Codice1" />
            <asp:TemplateField HeaderText="Sito web" SortExpression="Link1">
                <ControlStyle CssClass="GridView_Button1" />
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:HyperLink ID="Link1HyperLink" runat="server" NavigateUrl='<%# Eval("Link1", "{0}") %>'
                        OnDataBinding="Link1HyperLink_DataBinding" Target="_blank" Text='Apri'></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Categoria" HeaderText="Categoria" SortExpression="Categoria" />
            <asp:TemplateField HeaderText="Logo">
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Image ID="Immagine1" runat="server" ImageUrl='<%# Eval("Immagine1", "~/ZeusInc/Brands/Img1/{0}") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Logo">
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Image ID="Immagine2" runat="server" ImageUrl='<%# Eval("Immagine2", "~/ZeusInc/Brands/Img2/{0}") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField SortExpression="AttivoArea1">
                <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:TemplateField SortExpression="AttivoArea2">
                <ItemTemplate>
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:HyperLinkField DataNavigateUrlFields="ID1Brand,ZeusLangCode,ZeusIdModulo" DataNavigateUrlFormatString="Brand_Edt.aspx?XRI={0}&Lang={1}&ZIM={2}"
                Text="Modifica">
                <ControlStyle CssClass="GridView_Button1" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:HyperLinkField>
            <asp:TemplateField HeaderText="ZeusUserEditable" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="ZeusUserEditableLabel" runat="server" Text='<%# Eval("ZeusUserEditable") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="vwBrands_AllSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SELECT [ID1Brand],[Brand],[Codice1], Categoria, [Immagine1],[Immagine2],[ZeusIdModulo],[ZeusLangCode],[Link1],[AttivoArea1],[AttivoArea2],[ZeusUserEditable] FROM vwBrands_All WHERE ZeusLangCode=@ZeusLangCode AND ZeusIdModulo=@ZeusIdModulo ORDER BY Brand">
        <SelectParameters>
            <asp:QueryStringParameter Name="ZeusLangCode" QueryStringField="Lang" />
            <asp:QueryStringParameter Name="ZeusIdModulo" QueryStringField="ZIM" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
