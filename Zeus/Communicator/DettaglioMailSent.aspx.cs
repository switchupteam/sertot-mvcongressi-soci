﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;
using System.Xml.Linq;


public partial class DettaglioMailSent : System.Web.UI.Page
{

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {

        Page.Title = "Report Invio Mail";
        TitleField.Value = "Report Invio Mail ";

        delinea myDelinea = new delinea();
        Info InfoComunicazione = new Info();

        if (!myDelinea.AntiSQLInjectionRight(Request.QueryString, "XRI"))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        string ID2LogMessage = Request.QueryString["XRI"];

        InfoComunicazione = GetComunicazioneInfo(Convert.ToInt32(ID2LogMessage));

        TitoloLabel.Text = InfoComunicazione.Titolo;
        StatoInvioLabel.Text = InfoComunicazione.StatoInvio;
        if (InfoComunicazione.Modalita.Trim() == "IMMED")
            ModalitaLabel.Text = "Invio Immediato";
        else if (InfoComunicazione.Modalita.Trim() == "SCHED")
            ModalitaLabel.Text = "Invio Schedulato";
        UtenteLabel.Text = InfoComunicazione.Utente;
        DestinatariLabel.Text = InfoComunicazione.NumeroDestinatari;
        InviatiLabel.Text = InfoComunicazione.NumeroInviate;
        LogLabel.Text = InfoComunicazione.Log;
        InvioLabel.Text = InfoComunicazione.DataSchedulazione;
        FineSpedizioneLabel.Text = InfoComunicazione.DataInvio;

        try
        {
            XDocument doc = XDocument.Load(MapPath("~/ZeusInc/TrackingMailSent/Report_") + Request.QueryString["XRI"] + ".xml");

            var UserList = (from a in doc.Descendants("Mail")
                            orderby ((string)a.Element("Esito")), ((string)a.Element("EMail")) ascending
                            select new Mail()
                            {
                                UserId = (string)a.Element("UserId"),
                                Nome = (string)a.Element("Nome"),
                                EMail = (string)a.Element("EMail"),
                                Cognome = (string)a.Element("Cognome"),
                                Oggetto = (string)a.Element("Oggetto"),
                                Esito = (string)a.Element("Esito"),
                                DataInvio = (string)a.Element("DataInvio")
                            } );

            GridView1.DataSource = UserList.ToList();
            GridView1.DataBind();
        }
        catch (Exception)
        {
            GridView1.DataSource = new List<string>();
            GridView1.DataBind();
        }

    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        var UserList = GridView1.DataSource;
        GridView1.PageIndex = e.NewPageIndex;

        GridView1.DataSource = UserList;
        GridView1.DataBind();
    }

    protected void DownloadReportButton_Click(object sender, EventArgs e)
    {
        try
        {
            FileInfo file = new FileInfo(MapPath("~/ZeusInc/TrackingMailSent/Report_") + Request.QueryString["XRI"] + ".xml");

            if (file.Exists)
            {
                Response.ClearContent();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                Response.AddHeader("Content-Length", file.Length.ToString());
                Response.ContentType = "text/xml";
                Response.TransmitFile(file.FullName);
                Response.End();
            }

        }
        catch (Exception)
        {
        }
    }

    #endregion

    #region Methods

    private Info GetComunicazioneInfo(int _ID1LogMessage)
    {
        
        Info MyInfo = new Info();
        string sqlQuery = string.Empty;

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        try
        {
            using (SqlConnection connection = new SqlConnection(strConnessione))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = @"SELECT tbCommunicator.Titolo, tbCommunicator_LogMessage.SendType, tbCommunicator_LogMessage.Status, 
                                    tbCommunicator_LogMessage.DestinatariTotale, tbCommunicator_LogMessage.DestinatariInviati, 
                                    tbCommunicator_LogMessage.SendDateStart, tbCommunicator_LogMessage.SendDateEnd, aspnet_Users.UserName
                            FROM tbCommunicator 
                                RIGHT OUTER JOIN tbCommunicator_LogMessage ON tbCommunicator.ID1Communicator = tbCommunicator_LogMessage.ID2Communicator
                                    LEFT OUTER JOIN aspnet_Users ON tbCommunicator_LogMessage.SendUser = aspnet_Users.UserId
                            WHERE tbCommunicator_LogMessage.ID1LogMessage = @ID1LogMessage
                            ";

                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID1LogMessage", System.Data.SqlDbType.Int);
                command.Parameters["@ID1LogMessage"].Value = Convert.ToInt32(Request.QueryString["XRI"]);

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {

                    if (reader["Titolo"] != DBNull.Value)
                        MyInfo.Titolo = reader["Titolo"].ToString();

                    if (reader["SendType"] != DBNull.Value)
                        MyInfo.Modalita = reader["SendType"].ToString();

                    if (reader["Status"] != DBNull.Value)
                        MyInfo.StatoInvio = reader["Status"].ToString();

                    if (reader["DestinatariTotale"] != DBNull.Value)
                        MyInfo.NumeroDestinatari = reader["DestinatariTotale"].ToString();

                    if (reader["DestinatariInviati"] != DBNull.Value)
                        MyInfo.NumeroInviate = reader["DestinatariInviati"].ToString();

                    if (reader["SendDateStart"] != DBNull.Value)
                        MyInfo.DataSchedulazione = reader["SendDateStart"].ToString();

                    if (reader["SendDateEnd"] != DBNull.Value)
                        MyInfo.DataInvio = reader["SendDateEnd"].ToString();

                    if (reader["UserName"] != DBNull.Value)
                        MyInfo.Utente = reader["UserName"].ToString();

                    MyInfo.Log = _ID1LogMessage.ToString();

                }

            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            Response.Redirect("~/Zeus/System/Message.aspx?0");
        }

        return MyInfo;

    }

    #endregion

}

public class Info
{

    #region Constructors

    public Info()
    {
        titolo = string.Empty;
        statoinvio = string.Empty;
        modalita = string.Empty;
        utente = string.Empty;
        numerodestinatari = string.Empty;
        numeroinviate = string.Empty;
        log = string.Empty;
        dataschedulazione = string.Empty;
        datainvio = string.Empty;
    }

    #endregion

    #region Fields

    private string titolo;
    private string statoinvio;
    private string modalita;
    private string utente;
    private string numerodestinatari;
    private string numeroinviate;
    private string log;
    private string dataschedulazione;
    private string datainvio;

    #endregion

    #region Properties

    public string Titolo
    {
        get { return titolo; }
        set { titolo = value; }
    }

    public string StatoInvio
    {
        get { return statoinvio; }
        set { statoinvio = value; }
    }

    public string Modalita
    {
        get { return modalita; }
        set { modalita = value; }
    }

    public string Utente
    {
        get { return utente; }
        set { utente = value; }
    }

    public string NumeroDestinatari
    {
        get { return numerodestinatari; }
        set { numerodestinatari = value; }
    }

    public string NumeroInviate
    {
        get { return numeroinviate; }
        set { numeroinviate = value; }
    }

    public string Log
    {
        get { return log; }
        set { log = value; }
    }

    public string DataSchedulazione
    {
        get { return dataschedulazione; }
        set { dataschedulazione = value; }
    }

    public string DataInvio
    {
        get { return datainvio; }
        set { datainvio = value; }
    }
    
    #endregion

}

public class Mail
{

    #region Constructors

    public Mail()
    {
        userid = string.Empty;
        nome = string.Empty;
        cognome = string.Empty;
        email = string.Empty;
        oggetto = string.Empty;
        esito = string.Empty;
        datainvio = string.Empty;
    }

    #endregion

    #region Fields

    private string userid;
    private string nome;
    private string cognome;
    private string email;
    private string oggetto;
    private string esito;
    private string datainvio;

    #endregion

    #region Properties

    public string UserId
    {
        get { return userid; }
        set { userid = value; }
    }

    public string Nome
    {
        get { return nome; }
        set { nome = value; }
    }

    public string Cognome
    {
        get { return cognome; }
        set { cognome = value; }
    }

    public string EMail
    {
        get { return email; }
        set { email = value; }
    }

    public string Oggetto
    {
        get { return oggetto; }
        set { oggetto = value; }
    }

    public string Esito
    {
        get { return esito; }
        set { esito = value; }
    }

    public string DataInvio
    {
        get { return datainvio; }
        set { datainvio = value; }
    }

    #endregion

}