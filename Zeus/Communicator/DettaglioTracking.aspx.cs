﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class DettaglioTracking : System.Web.UI.Page
{
	public DettaglioTracking()
	{
		
	}

    public class User
    {
      
        #region Properties

        private string userid;
        public string UserId
        {
            get { return userid; }
            set { userid = value; }
        }

        private string email;
        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        private string username;
        public string UserName
        {
            get { return username; }
            set { username = value; }
        }

        private string nome;
        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        private string cognome;
        public string Cognome
        {
            get { return cognome; }
            set { cognome = value; }
        }

        private string ragionesociale;
        public string RagioneSociale
        {
            get { return ragionesociale; }
            set { ragionesociale = value; }
        }

        private DateTime data;
        public DateTime Data
        {
            get { return data; }
            set { data = value; }
        }

        #endregion

        public User()
        {

        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = "Dettaglio comunicazione ";
        string sqlQuery = string.Empty;
        string ZeusId = string.Empty;

        delinea myDelinea = new delinea();

        if (!myDelinea.AntiSQLInjectionRight(Request.QueryString, "XRI"))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        try
        {
            using (SqlConnection connection = new SqlConnection(strConnessione))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                sqlQuery = @"SELECT ZeusId
                            FROM tbCommunicator
                            WHERE ID1Communicator = (SELECT ID2Communicator
                                                    FROM tbCommunicator_Allegati
                                                    WHERE ID1Allegato = @ID1Allegato)
                            ";

                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID1Allegato", System.Data.SqlDbType.Int);
                command.Parameters["@ID1Allegato"].Value = Convert.ToInt32(Request.QueryString["xri"]);

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (reader["ZeusId"] != DBNull.Value)
                        ZeusId = reader["ZeusId"].ToString();
                }
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

        try
        {
            XDocument doc = XDocument.Load(MapPath("~/ZeusInc/TrackingAttachment/") + Request.QueryString["XRI"] + "_Allegato_" + ZeusId + ".xml");

            var UserList = (from a in doc.Descendants("User")
                           select new User()
                           {
                               UserName = (string)a.Element("UserName"),
                               UserId = (string)a.Element("ID"),
                               Email = (string)a.Element("Email"),
                               Nome = (string)a.Element("Nome"),
                               Cognome = (string)a.Element("Cognome"),
                               Data = Convert.ToDateTime((string)a.Element("Data")),
                               RagioneSociale = (string)a.Element("RagioneSociale")
                           }).OrderBy(x => x.Email);

            GridView1.DataSource = UserList.ToList() ;
            GridView1.DataBind();
        }
        catch (Exception)
        {

            GridView1.DataSource = new List<string>();
            GridView1.DataBind();
        }

    }//fine Page_Load

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        var UserList = GridView1.DataSource;
        GridView1.PageIndex = e.NewPageIndex;

        GridView1.DataSource = UserList;
        GridView1.DataBind();
    }

    protected void DownloadAllegatoButton_Click(object sender, EventArgs e)
    {
        string sqlQuery = string.Empty;
        string ZeusId = string.Empty;

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        try
        {
            using (SqlConnection connection = new SqlConnection(strConnessione))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                sqlQuery = @"SELECT ZeusId
                            FROM tbCommunicator
                            WHERE ID1Communicator = (SELECT ID2Communicator
                                                    FROM tbCommunicator_Allegati
                                                    WHERE ID1Allegato = @ID1Allegato)
                            ";

                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID1Allegato", System.Data.SqlDbType.Int);
                command.Parameters["@ID1Allegato"].Value = Convert.ToInt32(Request.QueryString["xri"]);

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (reader["ZeusId"] != DBNull.Value)
                        ZeusId = reader["ZeusId"].ToString();
                }

                Response.Write(ZeusId);
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

        try
        {
            FileInfo file = new FileInfo(MapPath("~/ZeusInc/TrackingAttachment/") + Request.QueryString["XRI"] + "_Allegato_" + ZeusId + ".xml");

            if (file.Exists)
            {
                Response.ClearContent();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                Response.AddHeader("Content-Length", file.Length.ToString());
                Response.ContentType = "text/xml";
                Response.TransmitFile(file.FullName);
                Response.End();
            }

        }
        catch (Exception p)
        {
        }
    }
}