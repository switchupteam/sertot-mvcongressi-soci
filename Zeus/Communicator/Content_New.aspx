﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="Content_New.aspx.cs"
    Inherits="Communicator_Content_New"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Zeus/SiteAssets/ZeusColorPicker.ascx" TagName="ZeusColorPicker" TagPrefix="uc2" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <script type="text/javascript">
        function ChangeBackground(editor) {
            editor.GetContentArea().style.backgroundColor = document.getElementById("<%=BackgroundColorHiddenField.ClientID%>").value;
            //editor.GetContentArea().style.backgroundImage = "url(Logo.jpg)";
        }

        function editorCommandExecuted(editor, args) {
            if (!$telerik.isChrome)
                return;
            var dialogName = args.get_commandName();
            var dialogWin = editor.get_dialogOpener()._dialogContainers[dialogName];
            if (dialogWin) {
                var cellEl = dialogWin.get_contentElement() || dialogWin.ui.contentCell || dialogWin.ui.content,
                frame = dialogWin.get_contentFrame();
                frame.onload = function () {
                    cellEl.style.cssText = "";
                    dialogWin.autoSize();
                }
            }
        }

        function OnClientLoad(editor, args) {
            var style = editor.get_contentArea().style;
            style.backgroundImage = "none";
            style.backgroundColor = "#<%=((ZeusColorPicker)FormView1.FindControl("ZeusColorPicker1")).SelectedColor.ToString()%>";
        }
    </script>
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <asp:HiddenField ID="BackgroundColorHiddenField" runat="server" />
    <asp:FormView ID="FormView1" runat="server" DefaultMode="Insert" DataSourceID="NewsletterSqlDataSource"
        Width="100%">
        <InsertItemTemplate>
            <asp:Panel ID="ZMCF_CopiaDaModello" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="ZML_CopiaDaModelloTitle" runat="server"></asp:Label>
                    </div>
                    <table>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_Modello" SkinID="FieldDescription" runat="server"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:DropDownList ID="ModelloDropDownList" runat="server" DataSourceID="ModelloSqlDataSource"
                                    DataTextField="Titolo" DataValueField="ID1Communicator" AppendDataBoundItems="true">
                                    <asp:ListItem Text="&gt; Nessuno" Selected="True" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="ModelloSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                    SelectCommand="SELECT 
                                                        Titolo, 
                                                        ID1Communicator 
                                                    FROM tbCommunicator                                
                                                    WHERE (IsTemplate = 1) 
                                                        AND ZeusIdModulo = @ZeusIdModulo 
                                                        AND ZeusLangCode = @ZeusLangCode 
                                                    ORDER BY Titolo">
                                    <SelectParameters>
                                        <asp:QueryStringParameter Name="ZeusIdModulo" QueryStringField="ZIM" Type="String" />
                                        <asp:QueryStringParameter Name="ZeusLangCode" QueryStringField="Lang" Type="String" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                                <asp:LinkButton ID="CopiaLinkButton" runat="server" Text="Copia" SkinID="ZSSM_Button01"
                                    OnClick="CopiaLinkButton_Click"></asp:LinkButton>
                                <asp:CustomValidator ID="ErrorCustomValidator" ErrorMessage="Selezionare un modello."
                                    runat="server" ControlToValidate="TitoloTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ValidationGroup="myValidation" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <asp:Panel ID="ZMCF_Dettaglio" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="ZML_DettaglioTitle" runat="server"></asp:Label>
                    </div>
                    <table>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_Titolo" runat="server" SkinID="FieldDescription"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="TitoloTextBox" runat="server" Text='<%# Bind("Titolo") %>' Columns="100"
                                    MaxLength="200"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="TitoloRequiredFieldValidator" ErrorMessage="Obbligatorio"
                                    runat="server" ControlToValidate="TitoloTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ValidationGroup="myValidation">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_Categoria" runat="server" SkinID="FieldDescription"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:DropDownList ID="ID2CategoriaDropDownList" runat="server" OnSelectedIndexChanged="ID2CategoriaDropDownList_SelectIndexChange"
                                    AppendDataBoundItems="True">
                                    <asp:ListItem Text="> Seleziona" Value="0" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:HiddenField ID="ID2CategoriaHiddenField" runat="server" Value='<%# Bind("ID2Categoria") %>' />
                                <asp:RequiredFieldValidator ID="ID2CategoriaRequiredFieldValidator" ErrorMessage="Obbligatorio"
                                    runat="server" ControlToValidate="ID2CategoriaDropDownList" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" InitialValue="0" ValidationGroup="myValidation">
                                </asp:RequiredFieldValidator>
                                <asp:SqlDataSource ID="dsCategoria" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"></asp:SqlDataSource>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_IsModello" runat="server" SkinID="FieldDescription"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:CheckBox ID="ModelloCheckBox" runat="server" Checked='<%# Bind("isTemplate") %>'
                                    OnLoad="ModelloCheckBox_Load" />
                                <asp:Label ID="ZML_IsModelloInfo" runat="server" SkinID="Note"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_ConsensoAssociato" runat="server" SkinID="FieldDescription"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:DropDownList ID="NewsletterDropDownList" runat="server" DataSourceID="NewsletterSqlDataSource"
                                    DataTextField="Descrizione" DataValueField="ID1FlagConsenso" AppendDataBoundItems="true"
                                    SelectedValue='<%# Bind("ID2FlagConsensoAssociato") %>'>
                                    <asp:ListItem Text="&gt; Seleziona" Selected="True" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="ZML_ConsensoAssociatoInfo" runat="server" SkinID="Note"></asp:Label>
                                <asp:RequiredFieldValidator ID="NewsletterRequiredFieldValidator1" ErrorMessage="Obbligatorio"
                                    runat="server" ControlToValidate="NewsletterDropDownList" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" InitialValue="0" ValidationGroup="myValidation"></asp:RequiredFieldValidator>
                                <asp:SqlDataSource ID="NewsletterSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                    SelectCommand="SELECT  
                                                        ID1FlagConsenso,
                                                        Descrizione 
                                                    FROM tbCommunicator_FlagConsenso 
                                                    WHERE PW_Zeus1=1 
                                                    ORDER BY ID1FlagConsenso,Descrizione">
                                </asp:SqlDataSource>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="ZML_MittenteTitle" runat="server"></asp:Label>
                </div>
                <table>
                    <asp:Panel ID="EmailMittentePanel" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_Oggetto" runat="server" SkinID="FieldDescription"></asp:Label>
                            </td>
                            <td class="BlockBoxValue" valign="baseline">
                                <asp:TextBox ID="OggettoTextBox" runat="server" Text='<%# Bind("Sender_Oggetto") %>'
                                    Columns="100" MaxLength="300"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="OggettoRequiredFieldValidator" ErrorMessage="Obbligatorio"
                                    runat="server" ControlToValidate="OggettoTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ValidationGroup="myValidation">
                                </asp:RequiredFieldValidator><br />
                                <asp:Label ID="ZML_OggettoInfo1" runat="server" SkinID="Note"></asp:Label><br />
                                <asp:Label ID="ZML_OggettoInfo2" runat="server" SkinID="Note"></asp:Label><br />
                                <asp:Label ID="ZML_OggettoInfo3" runat="server" SkinID="Note"></asp:Label><br />
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_Mittente" runat="server" SkinID="FieldDescription"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="MittenteTextBox" runat="server" Text='<%# Bind("Sender_Nome") %>'
                                    Columns="100" MaxLength="100"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="MittenteRequiredFieldValidator" ErrorMessage="Obbligatorio"
                                    runat="server" ControlToValidate="MittenteTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ValidationGroup="myValidation">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_TitoloVersioneWeb" runat="server" SkinID="FieldDescription"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="TitoloWebTextBox" runat="server" Text='<%# Bind("TitoloWeb") %>' Columns="100"
                                    MaxLength="100"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ErrorMessage="Obbligatorio"
                                    runat="server" ControlToValidate="TitoloWebTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ValidationGroup="myValidation">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_SMSMittente" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_NumeroTelefonoSMS" runat="server" SkinID="FieldDescription"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="Sender_PhoneNumberTextBox" runat="server" Text='<%# Bind("Sender_PhoneNumber") %>'
                                    Columns="20" MaxLength="30"></asp:TextBox>
                            </td>
                        </tr>
                    </asp:Panel>
                </table>
            </div>
            <asp:Panel ID="PZV_BoxSmtpPanel" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="ZML_ServerSMTPTitle" runat="server"></asp:Label>
                    </div>
                    <table>
                                                <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_IndirizzoEMail" runat="server" SkinID="FieldDescription"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="IndirizzoEmailTextBox" runat="server" Text='<%# Bind("Sender_Email") %>'
                                    Columns="100" MaxLength="100" Enabled="false"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="IndirizzoEmailRequiredFieldValidator1" ErrorMessage="Obbligatorio"
                                    runat="server" ControlToValidate="IndirizzoEmailTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ValidationGroup="myValidation">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="IndirizzoEmailRegularExpressionValidator" runat="server"
                                    ControlToValidate="IndirizzoEmailTextBox" ErrorMessage="Indirizzo E-Mail non valido"
                                    SkinID="ZSSM_Validazione01" Display="Dynamic" ValidationGroup="myValidation"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_SMTPServer" runat="server" SkinID="FieldDescription"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="SMTP_ServerTextBox" runat="server" Text='<%# Bind("SMTP_Server") %>'
                                    Width="250" Enabled="false"/>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator" ErrorMessage="Obbligatorio"
                                    runat="server" ControlToValidate="SMTP_ServerTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ValidationGroup="MyValidation" />
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_SMTPUser" runat="server" SkinID="FieldDescription"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="SMTP_UserTextBox" runat="server" autocomplete="off" Text='<%# Bind("SMTP_User") %>'
                                    Width="250" Enabled="false"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_SMTPPassword" runat="server" SkinID="FieldDescription"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="SMTP_PasswordTextBox" runat="server" autocomplete="off" 
                                    TextMode="Password" Enabled="false" />
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_ConfermaPassword" runat="server" SkinID="FieldDescription"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="SMTP_PasswordConfermaTextBox" runat="server" TextMode="Password" Enabled="false"/>
                                <asp:CompareValidator SkinID="ZSSM_Validazione01" ID="PasswordCompare" runat="server"
                                    ControlToCompare="SMTP_PasswordConfermaTextBox" ControlToValidate="SMTP_PasswordTextBox"
                                    Display="Dynamic" ValidationGroup="MyValidation" ErrorMessage="Password e conferma password non coincidono"></asp:CompareValidator>
                            </td>
                        </tr>                        
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_DataOraInizioSpedizione" runat="server" SkinID="FieldDescription"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="SendMail_StartDateTextBox" runat="server" Text='<%# Bind("SendMail_StartDate","{0:d}") %>'
                                    Columns="10" MaxLength="10" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Obbligatorio"
                                    runat="server" ControlToValidate="SendMail_StartDateTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ValidationGroup="MyValidation" />
                                <asp:RegularExpressionValidator ID="SendMail_StartDateRegularExpressionValidator"
                                    SkinID="ZSSM_Validazione01" runat="server" Display="Dynamic" ErrorMessage="Formato data richiesto: GG/MM/AAAA"
                                    ControlToValidate="SendMail_StartDateTextBox" ValidationGroup="myValidation"
                                    ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>
                                <asp:Label ID="Label27" runat="server" Text="ore" SkinID="FieldValue"></asp:Label>
                                <asp:TextBox ID="SendMail_StartDateOreTextBox" runat="server" Width="15" MaxLength="2"
                                    Text="23"></asp:TextBox>
                                <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator25"
                                    runat="server" ControlToValidate="SendMail_StartDateOreTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="RegularExpressionValidator16"
                                    runat="server" ControlToValidate="SendMail_StartDateOreTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*" ValidationExpression="([0-1][0-9]|2[0-3])|[0123456789]">Inserire un valore numerico compreso tra 0 e 23</asp:RegularExpressionValidator>
                                <asp:Label ID="Label35" runat="server" Text="minuti" SkinID="FieldValue"></asp:Label>
                                <asp:TextBox ID="SendMail_StartDateMinutiTextBox" runat="server" Width="15" MaxLength="2"
                                    Text="59"></asp:TextBox>
                                <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="SendMail_StartDateRequiredFieldValidator"
                                    runat="server" ControlToValidate="SendMail_StartDateMinutiTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="RegularExpressionValidator17"
                                    runat="server" ControlToValidate="SendMail_StartDateMinutiTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*" ValidationExpression="([0-5][0-9])|[0123456789]">Inserire un valore numerico compreso tra 0 e 59</asp:RegularExpressionValidator>
                                <br />
                                <asp:Label ID="OrariLabel" runat="server" Text="Orari di spedizione consentiti: sempre"
                                    SkinID="Note"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="ZML_ContenutoComunicazioneTitle" runat="server"></asp:Label>
                </div>
                <table>
                    <asp:Panel ID="PZV_BoxContenuto1Panel" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_Contenuto" runat="server" SkinID="FieldDescription"></asp:Label>
                                <asp:RequiredFieldValidator ID="ContenutoRequiredFieldValidator" ErrorMessage="Obbligatorio"
                                    runat="server" ControlToValidate="RadEditor1" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ValidationGroup="myValidation"></asp:RequiredFieldValidator><br />
                                <br />
                                <asp:Label ID="ZML_ContenutoInfo1" runat="server" SkinID="Note"></asp:Label><br />
                                <br />
                                <asp:Label ID="ZML_ContenutoInfo2" runat="server" SkinID="Note"></asp:Label><br />
                                <br />
                                <asp:Label ID="ZML_ContenutoInfo3" runat="server" SkinID="Note"></asp:Label><br />
                                <br />
                                <asp:Label ID="ZML_ContenutoInfo4" runat="server" SkinID="Note"></asp:Label><br />
                                <br />
                                <asp:Label ID="ZML_ContenutoInfo5" runat="server" SkinID="Note"></asp:Label><br />
                                <br />
                                <asp:Label ID="ZML_ContenutoInfo6" runat="server" SkinID="Note"></asp:Label><br />
                                <br />
                                <asp:Label ID="ZML_ContenutoInfo7" runat="server" SkinID="Note" Font-Bold="true"></asp:Label><br />
                            </td>
                            <td class="BlockBoxValue">
                                <telerik:RadEditor
                                    Language="it-IT" ID="RadEditor1" runat="server"
                                    DocumentManager-DeletePaths="~/ZeusInc/Communicator/Documents"
                                    DocumentManager-SearchPatterns="*.*"
                                    DocumentManager-ViewPaths="~/ZeusInc/Communicator/Documents"
                                    DocumentManager-MaxUploadFileSize="52428800"
                                    DocumentManager-UploadPaths="~/ZeusInc/Communicator/Documents"
                                    FlashManager-DeletePaths="~/ZeusInc/Communicator/Media"
                                    FlashManager-MaxUploadFileSize="10240000"
                                    FlashManager-ViewPaths="~/ZeusInc/Communicator/Media"
                                    FlashManager-UploadPaths="~/ZeusInc/Communicator/Media"
                                    ImageManager-DeletePaths="~/ZeusInc/Communicator/Images"
                                    ImageManager-ViewPaths="~/ZeusInc/Communicator/Images"
                                    ImageManager-MaxUploadFileSize="10240000"
                                    ImageManager-SearchPatterns="*.gif, *.png, *.jpg, *.jpe, *.jpeg"
                                    ImageManager-UploadPaths="~/ZeusInc/Communicator/Images"
                                    ImageManager-ViewMode="Grid"
                                    MediaManager-DeletePaths="~/ZeusInc/Communicator/Media"
                                    MediaManager-MaxUploadFileSize="10240000"
                                    MediaManager-SearchPatterns="*.asf, *.asx, *.wma, *.wmv, *.avi, *.wav, *.mpeg, *.mpg, *.mpe, *.mov, *.mp3, *.m3u, *.mid, *.midi, *.rm, *.rma"
                                    MediaManager-ViewPaths="~/ZeusInc/Communicator/Media"
                                    MediaManager-UploadPaths="~/ZeusInc/Communicator/Media"
                                    TemplateManager-SearchPatterns="*.html,*.htm"
                                    ContentAreaMode="iframe"
                                    OnClientCommandExecuted="editorCommandExecuted"
                                    OnClientLoad="OnClientLoad"
                                    Content='<%# Bind("Contenuto1") %>'
                                    ToolsFile="~/Zeus/Communicator/RadEditor1.xml"
                                    LocalizationPath="~/App_GlobalResources"
                                    AllowScripts="true" RenderMode="Classic" ToolbarMode="Default" EnableViewState="False"
                                    Width="700px" Height="500px">
                                    <CssFiles>
                                        <telerik:EditorCssFile Value="~/asset/css/ZeusTypeFoundry.css" />
                                    </CssFiles>
                                </telerik:RadEditor>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_Contenuto2" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_ContenutoSecondario" SkinID="FieldDescription" runat="server"></asp:Label>
                                <asp:Label ID="ZML_ContenutoSecondarioInfo" runat="server" SkinID="Note"></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Obbligatorio"
                                    runat="server" ControlToValidate="Contenuto2TextArea" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ValidationGroup="myValidation"></asp:RequiredFieldValidator>
                            </td>
                            <td class="BlockBoxValue">
                                <CustomWebControls:TextArea ID="Contenuto2TextArea" runat="server" Columns="100"
                                    MaxLength="500" TextMode="MultiLine" Text='<%# Bind("Contenuto2") %>' Height="200"></CustomWebControls:TextArea>
                            </td>
                        </tr>
                    </asp:Panel>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="ZML_ColoreSfondo" SkinID="FieldDescription" runat="server"></asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <table>
                                <tr>
                                    <td>
                                        <uc2:ZeusColorPicker ID="ZeusColorPicker1" runat="server" SelectedColor='<%# Bind("BackgroundColor") %>'
                                            ValidationGroup="myValidation" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="SincronizzaLinkButton" Text="Aggiorna sfondo contenuto" runat="server"
                                            SkinID="ZSSM_Button01"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="ZML_PlannerTitle" runat="server"></asp:Label>
                </div>
                <table>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="ZML_Visibilita" runat="server" SkinID="FieldDescription"></asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:CheckBox ID="PW_Area1CheckBox" runat="server" Checked='<%# Bind("PW_Area1") %>' />
                            <asp:Label ID="ZML_PubblicazioneDallaData" runat="server" SkinID="FieldValue"></asp:Label>
                            <asp:TextBox ID="PWI_Area1TextBox" runat="server" Text='<%# Bind("PWI_Area1", "{0:d}") %>'
                                Columns="10" MaxLength="10" OnDataBinding="DataIniziale"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" SkinID="ZSSM_Validazione01"
                                runat="server" ControlToValidate="PWI_Area1TextBox" Display="Dynamic" ErrorMessage="*"
                                ValidationGroup="myValidation">Obbligatorio</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" SkinID="ZSSM_Validazione01"
                                runat="server" Display="Dynamic" ErrorMessage="Formato data richiesto: GG/MM/AAAA"
                                ControlToValidate="PWI_Area1TextBox" ValidationGroup="myValidation" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>
                            <asp:Label ID="ZML_PubblicazioneAllaData" runat="server" SkinID="FieldValue"></asp:Label>
                            <asp:TextBox ID="PWF_Area1TextBox" runat="server" Text='<%# Bind("PWF_Area1", "{0:d}") %>'
                                Columns="10" MaxLength="10" OnDataBinding="DataLimite"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" SkinID="ZSSM_Validazione01"
                                runat="server" ControlToValidate="PWF_Area1TextBox" Display="Dynamic" ErrorMessage="*"
                                ValidationGroup="myValidation">Obbligatorio</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" SkinID="ZSSM_Validazione01"
                                runat="server" Display="Dynamic" ErrorMessage="Formato data richiesto: GG/MM/AAAA"
                                ControlToValidate="PWF_Area1TextBox" ValidationGroup="myValidation" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>
                            <asp:CompareValidator ID="CompareValidator1" SkinID="ZSSM_Validazione01" runat="server"
                                ValidationGroup="myValidation" ControlToCompare="PWF_Area1TextBox" ControlToValidate="PWI_Area1TextBox"
                                ErrorMessage="La data iniziale deve essere precedente o uguale alla data finale"
                                Operator="LessThanEqual" Display="Dynamic" Type="Date"></asp:CompareValidator>
                        </td>
                    </tr>
                </table>
            </div>
            <asp:HiddenField ID="ZeusIdHiddenField" runat="server" Value='<%# Bind("ZeusId") %>'
                OnDataBinding="ZeusIdHiddenField_DataBinding" />
            <asp:HiddenField ID="RecordNewUserHiddenField" runat="server" Value='<%# Bind("RecordNewUser") %>'
                OnDataBinding="RecordNewUserHiddenField_DataBinding" />
            <asp:HiddenField ID="RecordNewDateHiddenField" runat="server" Value='<%# Bind("RecordNewDate") %>'
                OnDataBinding="RecordNewDateHiddenField_DataBinding" />
            <div align="center">
                <dlc:mySummaryValidation ID="mySummaryValidation1" runat="server" />
                <asp:LinkButton ID="InsertButton" runat="server" Text="Salva dati" SkinID="ZSSM_Button01"
                    CausesValidation="true" ValidationGroup="myValidation" CommandName="Insert" OnClick="InsertButton_Click"></asp:LinkButton>
            </div>
        </InsertItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="NewsletterSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        InsertCommand=" SET DATEFORMAT dmy; INSERT INTO tbCommunicator  (Titolo, ID2Categoria, Contenuto1, Contenuto2,IsTemplate, PermitEdt, 
        ZeusId, ZeusLangCode, ZeusIdModulo, ZeusIsAlive, Sender_Oggetto, Sender_Nome, Sender_Email,Sender_PhoneNumber,
         ID2FlagConsensoAssociato, PW_Area1, PWI_Area1, PWF_Area1, RecordNewUser, RecordNewDate,
         SMTP_Server,SMTP_User,SMTP_Password,SendMail_StartDate,BackgroundColor,TitoloWeb) 
         VALUES (@Titolo, @ID2Categoria, @Contenuto1,@Contenuto2, @IsTemplate, @PermitEdt, @ZeusId, @ZeusLangCode, 
         @ZeusIdModulo, @ZeusIsAlive, @Sender_Oggetto, @Sender_Nome, @Sender_Email, @Sender_PhoneNumber, @ID2FlagConsensoAssociato, 
         @PW_Area1, @PWI_Area1, @PWF_Area1, @RecordNewUser, @RecordNewDate,
         @SMTP_Server,@SMTP_User,@SMTP_Password,@SendMail_StartDate,@BackgroundColor,@TitoloWeb); SELECT @XRI = SCOPE_IDENTITY();"
        OnInserted="NewsletterSqlDataSource_Inserted"
        OnInserting="NewsletterSqlDataSource_Inserting">
        <InsertParameters>
            <asp:Parameter Name="Titolo" Type="String" />
            <asp:Parameter Name="ID2Categoria" Type="Int32" />
            <asp:Parameter Name="Contenuto1" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="Contenuto2" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="BackgroundColor" Type="String" DefaultValue="trasparente" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="TitoloWeb" Type="String" />
            <asp:Parameter Name="IsTemplate" Type="Boolean" />
            <asp:Parameter Name="Sender_Oggetto" Type="String" />
            <asp:Parameter Name="Sender_Nome" Type="String" />
            <asp:Parameter Name="Sender_Email" Type="String" />
            <asp:Parameter Name="Sender_PhoneNumber" Type="String" />
            <asp:Parameter Name="SMTP_Server" Type="String" />
            <asp:Parameter Name="SMTP_User" Type="String" />
            <asp:Parameter Name="SMTP_Password" Type="String" />
            <asp:Parameter Name="SendMail_StartDate" Type="DateTime" />
            <asp:Parameter Name="ID2FlagConsensoAssociato" Type="Int32" />
            <asp:Parameter Name="PermitEdt" Type="Boolean" DefaultValue="True" />
            <asp:Parameter Name="ZeusId" />
            <asp:Parameter Name="ZeusIsAlive" Type="Boolean" DefaultValue="True" />
            <asp:Parameter Name="PW_Area1" Type="Boolean" />
            <asp:Parameter Name="PWI_Area1" Type="DateTime" />
            <asp:Parameter Name="PWF_Area1" Type="DateTime" />
            <asp:Parameter Name="RecordNewUser" />
            <asp:Parameter Name="RecordNewDate" Type="DateTime" />
            <asp:QueryStringParameter Name="ZeusIdModulo" Type="String" QueryStringField="ZIM" />
            <asp:QueryStringParameter Name="ZeusLangCode" Type="String" QueryStringField="Lang"
                DefaultValue="ITA" />
            <asp:Parameter Direction="Output" Name="XRI" Type="Int32" />
        </InsertParameters>
    </asp:SqlDataSource>
</asp:Content>
