﻿using ASP;
using System;
using System.Data.SqlClient;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

/// <summary>
/// Descrizione di riepilogo per Content_New
/// </summary>
public partial class Communicator_Content_New : System.Web.UI.Page
{    
    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = "Nuova comunicazione / newsletter";

        delinea myDelinea = new delinea();

        if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
             || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM")))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        

        LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");
        InsertButton.Attributes.Add("onclick", "Validate()");

        if (!Page.IsPostBack)
        {
			if (!ReadXML(Request.QueryString["ZIM"], GetLang()))
				Response.Redirect("~/Zeus/System/Message.aspx?1");

			ReadXML_Localization(Request.QueryString["ZIM"], GetLang());
		
            TextBox SendMail_StartDateTextBox = (TextBox)FormView1.FindControl("SendMail_StartDateTextBox");
            if (SendMail_StartDateTextBox != null)
                SendMail_StartDateTextBox.Text = DateTime.Now.ToString("d");

            TextBox SendMail_StartDateOreTextBox = (TextBox)FormView1.FindControl("SendMail_StartDateOreTextBox");
            if (SendMail_StartDateOreTextBox != null)
                SendMail_StartDateOreTextBox.Text = DateTime.Now.Hour.ToString();

            TextBox SendMail_StartDateMinutiTextBox = (TextBox)FormView1.FindControl("SendMail_StartDateMinutiTextBox");
            if (SendMail_StartDateMinutiTextBox != null)
                SendMail_StartDateMinutiTextBox.Text = DateTime.Now.Minute.ToString();

            SetOrariConsentiti();
        }

        ZeusColorPicker ZeusColorPicker1 = (ZeusColorPicker)FormView1.FindControl("ZeusColorPicker1");

        if (ZeusColorPicker1 != null)
            BackgroundColorHiddenField.Value = ZeusColorPicker1.SelectedColor;
    }

    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            string myXPathEach = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_Communicator.xml"));

            if (!CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_New").InnerText))
                Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223599");

            Panel myPanel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPathEach);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            if (childNode.Name.IndexOf("ZMCF_") > -1)
                            {
                                myPanel = (Panel)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myPanel != null)
                                    myPanel.Visible = Convert.ToBoolean(childNode.InnerText);
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }

            TextBox IndirizzoEmailTextBox = (TextBox)FormView1.FindControl("IndirizzoEmailTextBox");
            TextBox SMTP_ServerTextBox = (TextBox)FormView1.FindControl("SMTP_ServerTextBox");
            TextBox SMTP_UserTextBox = (TextBox)FormView1.FindControl("SMTP_UserTextBox");
            TextBox SMTP_PasswordTextBox = (TextBox)FormView1.FindControl("SMTP_PasswordTextBox");
            TextBox SMTP_PasswordConfermaTextBox = (TextBox)FormView1.FindControl("SMTP_PasswordConfermaTextBox");

            IndirizzoEmailTextBox.Text = mydoc.SelectSingleNode(myXPath + "ZMCD_DefaultSmtpEmail").InnerText;
            SMTP_ServerTextBox.Text = mydoc.SelectSingleNode(myXPath + "ZMCD_DefaultSmtpServer").InnerText;
            SMTP_UserTextBox.Text = mydoc.SelectSingleNode(myXPath + "ZMCD_DefaultSmtpUser").InnerText;
            SMTP_PasswordTextBox.Attributes.Add("value", mydoc.SelectSingleNode(myXPath + "ZMCD_DefaultSmtpPassword").InnerText);
            SMTP_PasswordConfermaTextBox.Attributes.Add("value", mydoc.SelectSingleNode(myXPath + "ZMCD_DefaultSmtpPassword").InnerText);
            SetQueryOfID2CategoriaDropDownList(mydoc.SelectSingleNode(myXPath + "ZMCD_ZIMCategoria1").InnerText
                , GetLang(), mydoc.SelectSingleNode(myXPath + "ZMCD_Cat1Liv").InnerText);

            

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Communicator.xml"));

            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            if (childNode.Name.Equals("ZML_TitoloPagina_New"))
                            {
                                TitleField.Value = childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                                else
                                {
                                    myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name + "Label");

                                    if (myLabel != null)
                                        myLabel.Text = childNode.InnerText;
                                }
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    private bool  SetOrariConsentiti()
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        try
        {
            using (SqlConnection connection = new SqlConnection(strConnessione))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = @"SELECT OraInizio,
                                OraFine
                            FROM tbAgents_Dashboard
                            WHERE Agent = 'COMMUNICATOR' 
                                AND ZeusIdModulo = @ZeusIdModulo
                            ";
                
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ZeusIdModulo", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ZeusIdModulo"].Value = Request.QueryString["ZIM"].ToString();
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if ((reader["OraInizio"] != DBNull.Value)
                        && ((reader["OraFine"] != DBNull.Value)))
                    {
                        Label OrariLabel = (Label)FormView1.FindControl("OrariLabel");

                        if (OrariLabel != null)
                            if (!reader["OraFine"].ToString().Equals(reader["OraInizio"].ToString()))
                                OrariLabel.Text = "Orari di spedizione consentiti: dalle "
                                    + reader["OraInizio"].ToString() + " alle "
                                    + reader["OraFine"].ToString();
                    }
                }

                reader.Close();
                return true;
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    private string GetLang()
    {
        string Lang = "ITA";

        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;
    }

    private bool CheckPermit(string AllRoles)
    {
        char[] delimiter = { ',' };
        bool IsPermit = false;

        foreach (string roles in Roles.GetRolesForUser(Membership.GetUser().UserName))
            if (roles.Equals("ZeusAdmin"))
            {
                IsPermit = true;
                break;
            }
            else if (AllRoles.Length > 0)
            {
                string[] PressRoles = AllRoles.Split(delimiter);

                for (int i = 0; i < PressRoles.Length; ++i)
                {
                    if (PressRoles[i].Equals(roles))
                    {
                        IsPermit = true;
                        break;
                    }
                }
            }

        return IsPermit;
    }

    private bool SetQueryOfID2CategoriaDropDownList(string ZeusIdModulo,
       string ZeusLangCode, string PZV_Cat1Liv)
    {
        try
        {
            SqlDataSource dsCategoria = (SqlDataSource)FormView1.FindControl("dsCategoria");
            DropDownList ID2CategoriaDropDownList = (DropDownList)FormView1.FindControl("ID2CategoriaDropDownList");
            HiddenField ID2CategoriaHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaHiddenField");
            ID2CategoriaHiddenField.Value = "0";

            switch (PZV_Cat1Liv)
            {

                case "123":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv1Liv2Liv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "Catliv1Liv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;

                case "23":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv2Liv3]";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;

                default:
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;
            }

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    protected void ID2CategoriaDropDownList_SelectIndexChange(object sender, EventArgs e)
    {
        HiddenField ID2CategoriaHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaHiddenField");
        DropDownList ID2CategoriaDropDownList = (DropDownList)sender;

        ID2CategoriaHiddenField.Value = ID2CategoriaDropDownList.SelectedValue;
    }

    protected void ModelloCheckBox_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CheckBox ModelloCheckBox = (CheckBox)sender;
            ModelloCheckBox.Checked = false;
        }
    }

    protected void DataLimite(object sender, EventArgs e)
    {
        TextBox DataLimite = (TextBox)sender;
        if (DataLimite.Text.Length == 0)
        {
            DataLimite.Text = "31/12/2040";
        }
    }

    protected void DataIniziale(object sender, EventArgs e)
    {
        DateTime Data = new DateTime();
        Data = DateTime.Now;
        TextBox DataCreazione = (TextBox)sender;
        if (DataCreazione.Text.Length == 0)
        {
            DataCreazione.Text = Data.ToString("d");
        }
    }

    protected void ZeusIdHiddenField_DataBinding(object sender, EventArgs e)
    {
        HiddenField ZeusIdHiddenField = (HiddenField)sender;

        if (ZeusIdHiddenField.Value.Length == 0)
            ZeusIdHiddenField.Value = System.Guid.NewGuid().ToString();
    }

    protected void RecordNewUserHiddenField_DataBinding(object sender, EventArgs e)
    {
        HiddenField RecordNewUserHiddenField = (HiddenField)sender;
        RecordNewUserHiddenField.Value = Membership.GetUser().ProviderUserKey.ToString();
    }

    protected void RecordNewDateHiddenField_DataBinding(object sender, EventArgs e)
    {
        HiddenField RecordNewDateHiddenField = (HiddenField)sender;
        RecordNewDateHiddenField.Value = DateTime.Now.ToString();
    }

    private bool PopulateFormView(string ID1Communicator)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {
            try
            {
                TextBox TitoloTextBox = (TextBox)FormView1.FindControl("TitoloTextBox");
                TextBox TitoloWebTextBox = (TextBox)FormView1.FindControl("TitoloWebTextBox");
                DropDownList CategoriaDropDownList = (DropDownList)FormView1.FindControl("ID2CategoriaDropDownList");
                DropDownList NewsletterDropDownList = (DropDownList)FormView1.FindControl("NewsletterDropDownList");
                TextBox OggettoTextBox = (TextBox)FormView1.FindControl("OggettoTextBox");
                TextBox MittenteTextBox = (TextBox)FormView1.FindControl("MittenteTextBox");
                TextBox IndirizzoEmailTextBox = (TextBox)FormView1.FindControl("IndirizzoEmailTextBox");
                RadEditor RadEditor1 = (RadEditor)FormView1.FindControl("RadEditor1");

                TextBox SMTP_ServerTextBox = (TextBox)FormView1.FindControl("SMTP_ServerTextBox");
                TextBox SMTP_UserTextBox = (TextBox)FormView1.FindControl("SMTP_UserTextBox");
                TextBox SMTP_PasswordTextBox = (TextBox)FormView1.FindControl("SMTP_PasswordTextBox");
                TextBox SMTP_PasswordConfermaTextBox = (TextBox)FormView1.FindControl("SMTP_PasswordConfermaTextBox");

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT Titolo, ID2Categoria, ID2FlagConsensoAssociato";
                sqlQuery += " ,Sender_Oggetto, Sender_Nome, Sender_Email, Contenuto1";
                sqlQuery += " ,SMTP_Server,SMTP_User,SMTP_Password ,SendMail_StartDate, BackgroundColor, TitoloWeb";
                sqlQuery += " FROM tbCommunicator WHERE ID1Communicator=@ID1Communicator";
                command.CommandText = sqlQuery;

                command.Parameters.Add("@ID1Communicator", System.Data.SqlDbType.Int, 4);
                command.Parameters["@ID1Communicator"].Value = ID1Communicator;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (reader["TitoloWeb"] != DBNull.Value)
                        TitoloWebTextBox.Text = reader["TitoloWeb"].ToString();

                    if (reader["BackgroundColor"] != DBNull.Value)
                        BackgroundColorHiddenField.Value = reader["BackgroundColor"].ToString();

                    ZeusColorPicker ZeusColorPicker1 = (ZeusColorPicker)FormView1.FindControl("ZeusColorPicker1");
                    ZeusColorPicker1.SelectedColor = BackgroundColorHiddenField.Value;

                    if (reader["SendMail_StartDate"] != DBNull.Value)
                    {
                        TextBox SendMail_StartDateTextBox = (TextBox)FormView1.FindControl("SendMail_StartDateTextBox");
                        TextBox SendMail_StartDateOreTextBox = (TextBox)FormView1.FindControl("SendMail_StartDateOreTextBox");
                        TextBox SendMail_StartDateMinutiTextBox = (TextBox)FormView1.FindControl("SendMail_StartDateMinutiTextBox");

                        SendMail_StartDateTextBox.Text = Convert.ToDateTime(reader["SendMail_StartDate"]).ToString("d");
                        SendMail_StartDateOreTextBox.Text = Convert.ToDateTime(reader["SendMail_StartDate"]).Hour.ToString();
                        SendMail_StartDateMinutiTextBox.Text = Convert.ToDateTime(reader["SendMail_StartDate"]).Minute.ToString();
                    }

                    if (!reader.IsDBNull(0))
                        TitoloTextBox.Text = reader.GetString(0);
                    if ((!reader.IsDBNull(1)) && (CategoriaDropDownList != null))
                    {
                        CategoriaDropDownList.SelectedIndex = 
                            CategoriaDropDownList.Items.IndexOf(CategoriaDropDownList.Items.FindByValue(reader.GetInt32(1).ToString()));
                        HiddenField ID2CategoriaHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaHiddenField");
                        ID2CategoriaHiddenField.Value = CategoriaDropDownList.SelectedValue;
                    }
                    if (!reader.IsDBNull(2))
                        NewsletterDropDownList.SelectedIndex = 
                            NewsletterDropDownList.Items.IndexOf(NewsletterDropDownList.Items.FindByValue(reader.GetInt32(2).ToString()));
                    if (!reader.IsDBNull(3))
                        OggettoTextBox.Text = reader.GetString(3);
                    if (!reader.IsDBNull(4))
                        MittenteTextBox.Text = reader.GetString(4);
                    if (!reader.IsDBNull(5))
                        IndirizzoEmailTextBox.Text = reader.GetString(5);
                    if (!reader.IsDBNull(6))
                        RadEditor1.Content = reader.GetString(6);
                    if (!reader.IsDBNull(7))
                        SMTP_ServerTextBox.Text = reader.GetString(7);
                    if (!reader.IsDBNull(8))
                        SMTP_UserTextBox.Text = reader.GetString(8);
                    if (!reader.IsDBNull(9))
                    {
                        SMTP_PasswordTextBox.Attributes.Add("value", reader.GetString(9));
                        SMTP_PasswordConfermaTextBox.Attributes.Add("value", reader.GetString(9));
                    }
                }

                reader.Close();
                return true;
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
                return false;
            }
        }
    }

    protected void CopiaLinkButton_Click(object sender, EventArgs e)
    {
        try
        {
            CustomValidator ErrorCustomValidator = (CustomValidator)FormView1.FindControl("ErrorCustomValidator");
            DropDownList ModelloDropDownList = (DropDownList)FormView1.FindControl("ModelloDropDownList");

            if (!ModelloDropDownList.SelectedValue.Equals("0"))
            {
                ErrorCustomValidator.IsValid = true;
                PopulateFormView(ModelloDropDownList.SelectedValue);

            }
            else ErrorCustomValidator.IsValid = false;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    private bool SaveSendMail_StartDate()
    {
        try
        {
            TextBox SendMail_StartDateTextBox = (TextBox)FormView1.FindControl("SendMail_StartDateTextBox");
            TextBox SendMail_StartDateOreTextBox = (TextBox)FormView1.FindControl("SendMail_StartDateOreTextBox");
            TextBox SendMail_StartDateMinutiTextBox = (TextBox)FormView1.FindControl("SendMail_StartDateMinutiTextBox");
            SendMail_StartDateTextBox.Text += " " + SendMail_StartDateOreTextBox.Text + ":" + SendMail_StartDateMinutiTextBox.Text;

            RegularExpressionValidator SendMail_StartDateRegularExpressionValidator = 
                (RegularExpressionValidator)FormView1.FindControl("SendMail_StartDateRegularExpressionValidator");

            if (SendMail_StartDateRegularExpressionValidator != null)
                SendMail_StartDateRegularExpressionValidator.Visible = false;

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    protected void InsertButton_Click(object sender, EventArgs e)
    {
        SaveSendMail_StartDate();
    }

    protected void NewsletterSqlDataSource_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.Exception == null)
            Response.Redirect("~/Zeus/Communicator/Content_Lst.aspx?ZIM=" + Server.HtmlEncode(Request.QueryString["ZIM"]) 
                + "&Lang=" + GetLang() 
                + "&XRI=" + e.Command.Parameters["@XRI"].Value.ToString());
        else Response.Redirect("~/Zeus/System/Message.aspx");
    }
    protected void NewsletterSqlDataSource_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {
        System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_Communicator.xml"));
        string myXPath = "/MODULES/" + Request.QueryString["ZIM"] + "/" + GetLang() + "/";
        e.Command.Parameters["@SMTP_Password"].Value = mydoc.SelectSingleNode(myXPath + "ZMCD_DefaultSmtpPassword").InnerText;
    }
}