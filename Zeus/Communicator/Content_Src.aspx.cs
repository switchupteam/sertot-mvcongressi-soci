﻿using System;
using System.Data.SqlClient;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per Content_Src
/// </summary>
public partial class Communicator_Content_Src : System.Web.UI.Page
{   
    static string TitoloPagina = "Ricerca Communicator";

    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;

        delinea myDelinea = new delinea();

        if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
             || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM")))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        if (!ReadXML(Request.QueryString["ZIM"], GetLang()))
            Response.Redirect("~/Zeus/System/Message.aspx?1");

        ReadXML_Localization(Request.QueryString["ZIM"], GetLang());

        SetFocus(SearchButton.UniqueID);

        //if (!Page.IsPostBack)
        //    PWF_Area1TextBox.Text = DateTime.Now.ToString("d");
    }

    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            string myXPathEach = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_Communicator.xml"));

            if (!CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_Src").InnerText))
                Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223599");

            Panel myPanel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPathEach);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            if (childNode.Name.IndexOf("ZMCF_") > -1)
                            {
                                myPanel = (Panel)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myPanel != null)
                                    myPanel.Visible = Convert.ToBoolean(childNode.InnerText);
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }

            SetQueryOfID2CategoriaDropDownList(mydoc.SelectSingleNode(myXPath + "ZMCD_ZIMCategoria1").InnerText
                , GetLang(), mydoc.SelectSingleNode(myXPath + "ZMCD_Cat1Liv").InnerText);

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Communicator.xml"));

            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            if (childNode.Name.Equals("ZML_TitoloPagina_Src"))
                            {
                                TitleField.Value = childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                                else
                                {
                                    myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name + "Label");

                                    if (myLabel != null)
                                        myLabel.Text = childNode.InnerText;
                                }
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    private bool CheckPermit(string AllRoles)
    {

        char[] delimiter = { ',' };
        bool IsPermit = false;

        foreach (string roles in Roles.GetRolesForUser(Membership.GetUser().UserName))
            if (roles.Equals("ZeusAdmin"))
            {
                IsPermit = true;
                break;
            }
            else if (AllRoles.Length > 0)
            {
                string[] PressRoles = AllRoles.Split(delimiter);

                for (int i = 0; i < PressRoles.Length; ++i)
                {
                    if (PressRoles[i].Equals(roles))
                    {
                        IsPermit = true;
                        break;
                    }
                }
            }

        return IsPermit;
    }

    private void SetFocus(string ID)
    {
        Page.Form.DefaultButton = ID;
        Page.Form.DefaultFocus = ID;
    }

    private string GetLang()
    {
        try
        {
            delinea myDelinea = new delinea();
            string Lang = "ITA";

            if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
            {
                if (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang"))
                    Lang = Server.HtmlEncode(Request.QueryString["Lang"]);
            }

            return Lang;
        }
        catch (Exception)
        {
            return "ITA";
        }
    }

    private bool populateFormView(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            string sqlQuery =  "SELECT [PZD_ZIMCategoria1],[PZV_Cat1Liv]";
            sqlQuery += " ,[PZL_TitoloPagina_Src]";
            sqlQuery += " FROM [tbPZ_Communicator]";
            sqlQuery += " WHERE ZeusIdModulo = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
            sqlQuery += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";

            using (SqlConnection conn = new SqlConnection(strConnessione))
            {
                SqlCommand command = new SqlCommand(sqlQuery, conn);

                conn.Open();
                SqlDataReader reader = command.ExecuteReader();

                if (!reader.HasRows)
                {
                    reader.Close();
                    return false;
                }

                while (reader.Read())
                {
                    if (reader["PZL_TitoloPagina_Src"] != DBNull.Value)
                        TitleField.Value = reader["PZL_TitoloPagina_Src"].ToString();

                    if ((reader["PZD_ZIMCategoria1"] != DBNull.Value) &&
                        (reader["PZV_Cat1Liv"] != DBNull.Value))
                        SetQueryOfID2CategoriaDropDownList(reader["PZD_ZIMCategoria1"].ToString(), GetLang(), reader["PZV_Cat1Liv"].ToString());      
                }
                reader.Close();
            }

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    private bool SetQueryOfID2CategoriaDropDownList(string ZeusIdModulo,
        string ZeusLangCode, string PZV_Cat1Liv)
    {
        try
        {
            switch (PZV_Cat1Liv)
            {
                case "123":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv1Liv2Liv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "Catliv1Liv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;

                case "23":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv2Liv3]";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;

                default:
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;
            }

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    protected void SearchButton_Click(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();

        if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
            || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM")))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        string myRedirect = string.Empty;

        if (TitoloTextBox.Text.Length > 0)
            myRedirect += "&XRE1="+TitoloTextBox.Text;

        if (!ID2CategoriaDropDownList.SelectedValue.Equals("0"))
            myRedirect += "&XRI1="+ID2CategoriaDropDownList.SelectedValue;

        if (!FiltroModelloDropDownList.SelectedValue.Equals("0"))
            myRedirect += "&XRI2="+FiltroModelloDropDownList.SelectedValue;

        if (!AttivoDropDownList.SelectedValue.Equals("0"))
            myRedirect += "&XRI3=" + AttivoDropDownList.SelectedValue;

        if (!ConsensoDropDownList.SelectedValue.Equals("0"))
            myRedirect += "&XRI4=" + ConsensoDropDownList.SelectedValue;

        if (PWI_Area1TextBox.Text.Length > 0)
            myRedirect += "&PWI=" + PWI_Area1TextBox.Text;

        if (PWF_Area1TextBox.Text.Length > 0)
            myRedirect += "&PWF=" + PWF_Area1TextBox.Text;

        Response.Redirect("~/Zeus/Communicator/Content_Lst.aspx?ZIM="+Request.QueryString["ZIM"]+"&Lang="+GetLang()+myRedirect);
    }
}