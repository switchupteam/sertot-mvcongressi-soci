﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="SendMail_Excel.aspx.cs"
    Inherits="Communicator_SendMail_Excel"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="LogSendMessage.ascx" TagName="LogSendMessage" TagPrefix="uc2" %>
<%@ Register Src="ImportDestinatari_ExcelInfo.ascx" TagName="ImportDestinatari_ExcelInfo"
    TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="CheckedColumnsHiddenField" runat="server" />
    <asp:HiddenField ID="PathHiddenField" runat="server" />
    <asp:HiddenField ID="ImportCodeHiddenField" runat="server" />
     <asp:HiddenField ID="EXCEL_EMAIL_ADDRESS" runat="server" />
    <script language="javascript">
              
       var isLoadButton=false;
       
        window.onbeforeunload = function() {
        
         if((isLoadButton)&&(document.getElementById("<%=ConfermaCheckBox.ClientID%>").checked==true))
         {
            $.blockUI({
                message: '<h1><img src="/SiteImg/wait.gif" /> Avvio procedura di spedizione in corso...</h1>',
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: '.5',
                    color: '#fff'
                }
            });//fine blockUIz
          }//fine if
        }//fine function
        
        function SetIsLoadButtonToTrue()
        {
          isLoadButton=true;
        }
        
    </script>
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <dlc:PermitRoles ID="myPermitRoles" runat="server" PAGE_TYPE="SND" SQL_TABLE="tbPZ_Communicator" />
    <asp:HiddenField ID="TotalCountHiddenField" runat="server" />
    <asp:HiddenField ID="PZD_UnsubscribeLink" runat="server" />
    <asp:HiddenField ID="PZD_SendAgentWeb_MaxQtyHiddenField" runat="server" />
    <asp:HiddenField ID="ZMCD_UnsubscribeLink" runat="server" />
    <asp:HiddenField ID="ZMCD_WebLink" runat="server" />
    <asp:HiddenField ID="ZMCD_UrlDomain" runat="server" />
    <asp:FormView ID="FormView1" runat="server" DefaultMode="ReadOnly" DataSourceID="CommunicatorSqlDataSource"
        CellPadding="0" Width="100%" OnDataBound="FormView1_DataBound">
        <ItemTemplate>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="Label9" runat="server">Invio comunicazione</asp:Label>
                </div>
                <table>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label1" runat="server" SkinID="FieldDescription">Titolo</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:Label ID="TitoloLabel" runat="server" Text='<%# Eval("Titolo") %>' SkinID="FieldValue" />
                        </td>
                    </tr>
                </table>
            </div>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="Label2" runat="server">Dettaglio messaggio E-Mail</asp:Label>
                </div>
                <table>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label3" runat="server" SkinID="FieldDescription"> Oggetto</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:Label ID="Sender_OggettoLabel" runat="server" Text='<%# Eval("Sender_Oggetto") %>'
                                SkinID="FieldValue" />
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label4" runat="server" SkinID="FieldDescription">Mittente</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:Label ID="Sender_NomeLabel" runat="server" Text='<%# Eval("Sender_Nome") %>'
                                SkinID="FieldValue" />
                        </td>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label5" runat="server" SkinID="FieldDescription">E-Mail</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:Label ID="Sender_EmailLabel" runat="server" Text='<%# Eval("Sender_Email") %>'
                                SkinID="FieldValue" />
                        </td>
                    </tr>
                </table>
            </div>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="Label16" runat="server">Dettaglio Server SMTP</asp:Label>
                </div>
                <table>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label17" runat="server" SkinID="FieldDescription">SMTP Server *</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:TextBox ID="SMTP_ServerTextBox" runat="server" Text='<%# Eval("SMTP_Server") %>' />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator" ErrorMessage="Obbligatorio"
                                runat="server" ControlToValidate="SMTP_ServerTextBox" SkinID="ZSSM_Validazione01"
                                Display="Dynamic" ValidationGroup="SendValidationGroup" />
                        </td>
                    </tr>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label19" runat="server" SkinID="FieldDescription">SMTP User</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:TextBox ID="SMTP_UserTextBox" runat="server" Text='<%# Eval("SMTP_User") %>' />
                        </td>
                    </tr>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label21" runat="server" SkinID="FieldDescription">SMTP Password</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:TextBox ID="SMTP_PasswordTextBox" runat="server" Text='<%# Eval("SMTP_Password") %>'
                                TextMode="Password" />
                            <asp:HiddenField ID="SMTP_PasswordHiddenField" runat="server" Value='<%# Eval("SMTP_Password") %>' />
                        </td>
                    </tr>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label23" runat="server" SkinID="FieldDescription">Conferma Password</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:TextBox ID="SMTP_PasswordConfermaTextBox" runat="server" Text='<%# Eval("SMTP_Password") %>'
                                TextMode="Password" />
                            <asp:CompareValidator SkinID="ZSSM_Validazione01" ID="PasswordCompare" runat="server"
                                ControlToCompare="SMTP_PasswordConfermaTextBox" ControlToValidate="SMTP_PasswordTextBox"
                                Display="Dynamic" ValidationGroup="SendValidationGroup" ErrorMessage="Password e conferma password non coincidono"></asp:CompareValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label26" runat="server" SkinID="FieldDescription">Data e ora inizio spedizione </asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:HiddenField ID="SendMail_StartDateHiddenField" runat="server" Value='<%# Eval("SendMail_StartDate") %>'
                                OnDataBinding="SendMail_StartDateHiddenField_DataBinding" />
                            <asp:Label SkinID="FieldValue" ID="SendMail_StartDateTextBox" runat="server" Text='<%# Eval("SendMail_StartDate","{0:d}") %>' />
                            <asp:Label ID="Label27" runat="server" Text="ore" SkinID="FieldValue"></asp:Label>
                            <asp:Label SkinID="FieldValue" ID="SendMail_StartDateOreTextBox" runat="server"></asp:Label>
                            <asp:Label ID="Label35" runat="server" Text="minuti" SkinID="FieldValue"></asp:Label>
                            <asp:Label SkinID="FieldValue" ID="SendMail_StartDateMinutiTextBox" runat="server"></asp:Label>
                            <br />
                            <asp:Label ID="OrariLabel" runat="server" Text="Orari di spedizione consentiti: sempre"
                                SkinID="Note"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="Label12" runat="server">Note e commenti invio comunicazione</asp:Label>
                </div>
                <table>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label28" runat="server" SkinID="FieldDescription">Note</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <CustomWebControls:TextArea ID="NoteTextArea" runat="server" Columns="100" MaxLength="500"
                                TextMode="MultiLine" Height="100"></CustomWebControls:TextArea>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="Label6" runat="server">Registro invii comunicazioni</asp:Label>
                </div>
                <table width="100%">
                    <tr>
                        <td>
                            <uc2:LogSendMessage ID="LogSendMessage1" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
            <asp:HiddenField ID="AttivoArea1HiddenField" runat="server" Value='<%# Eval("AttivoArea1") %>'
                OnDataBinding="AttivoArea1HiddenField_DataBinding" />
            <asp:HiddenField ID="ID2FlagConsensoAssociatoHiddenField" runat="server" Value='<%# Eval("ID2FlagConsensoAssociato") %>' />
            <asp:HiddenField ID="ID1CommunicatorHiddenField" runat="server" Value='<%# Eval("ID1Communicator") %>' />
        </ItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="CommunicatorSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SELECT [ID1Communicator], [Titolo],  [Sender_Oggetto], [Sender_Nome],
         [Sender_Email],[AttivoArea1],[ID2FlagConsensoAssociato],SMTP_Server,SMTP_User,SMTP_Password,SendMail_StartDate
         FROM [vwCommunicator_Dtl_Send] 
         WHERE ([ID1Communicator] = @ID1Communicator)" OnSelected="CommunicatorSqlDataSource_Selected">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="0" Name="ID1Communicator" QueryStringField="XRI"
                Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <div class="ZITC006">
    </div>
    <asp:Panel ID="SearchPanel" runat="server">
        <div class="BlockBox">
            <div class="BlockBoxHeader">
                <asp:Label ID="Label9" runat="server">Ricerca destinatari</asp:Label>
            </div>
            <table>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Infolabel1" runat="server" SkinID="FieldDescription" Text="Il foglio di lavoro Excel (il TAB in basso) si deve chiamare IMPORT."></asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <table>
                            <tr>
                                <td>
                                    <asp:FileUpload ID="FileUpload1" runat="server" CssClass="FileUpload" />
                                    <asp:Button ID="LoadButton" runat="server" Text="Carica" OnClick="LoadButton_Click"
                                        SkinID="ZSSM_Button01" />
                                </td>
                                <td>
                                    <asp:Button ID="VerificaButton" runat="server" Text="Verifica file caricato" OnClick="VerificaButton_Click"
                                        Visible="false" SkinID="ZSSM_Button01" />
                                        <asp:Button ID="ImportaButton" runat="server" Text="Importa i dati trovati" OnClick="ImportaButton_Click"
                                        Visible="false" SkinID="ZSSM_Button01" />
                                         
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <uc1:ImportDestinatari_ExcelInfo ID="ImportDestinatari_ExcelInfo1" runat="server" />
                                    <br />
                                    <asp:Label ID="Label7" runat="server" SkinID="Validazione01"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
       
    </asp:Panel>
    <asp:Panel ID="SendingPanel" runat="server" Visible="false">
        <div class="BlockBox">
            <div class="BlockBoxHeader">
                <asp:Label ID="Label18" runat="server">Stato invio</asp:Label>
            </div>
            <table>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label20" runat="server" SkinID="FieldDescription">Stato</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:Label ID="Label22" runat="server" SkinID="FieldValue">Invio in corso</asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:Panel ID="ErrorPanel" runat="server" Visible="false">
        <div class="BlockBox">
            <div class="BlockBoxHeader">
                <asp:Label ID="Label24" runat="server">Stato invio</asp:Label>
            </div>
            <table>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label25" runat="server" SkinID="FieldDescription">Stato</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:Label ID="Label26" runat="server" SkinID="FieldValue">Invio non concluso correttamente</asp:Label>
                        <asp:Label ID="ErroreReinvioLabel" runat="server" SkinID="Validazione01"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label27" runat="server" SkinID="FieldDescription">Azioni possibili</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:LinkButton ID="RestartLinkButton" SkinID="ZSSM_Button01" runat="server" Text="Riprendi invio"
                            OnClick="RestartLinkButton_Click"></asp:LinkButton>
                        <asp:LinkButton ID="StopLinkButton" SkinID="ZSSM_Button01" runat="server" Text="Termina invio"
                            OnClick="StopLinkButton_Click"></asp:LinkButton>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:Panel ID="InfoPanel" runat="server" Visible="false">
        <div class="BlockBox">
            <div class="BlockBoxHeader">
                <asp:Label ID="Label14" runat="server">Destinatari</asp:Label>
            </div>
            <table>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:CustomValidator ID="ConfermaCustomValidator" ErrorMessage="Obbligatorio" InitialValue="0"
                            runat="server" SkinID="ZSSM_Validazione01" Display="Dynamic" ValidationGroup="SendValidationGroup"
                            OnServerValidate="ConfermaCustomValidator_ServerValidate" />
                    </td>
                    <td class="BlockBoxValue">
                        <asp:CheckBox ID="ConfermaCheckBox" runat="server" Checked="false" />
                        <asp:Label ID="CountLabel" runat="server" SkinID="CorpoTesto" ></asp:Label>  
                         <asp:Label ID="ErrorLabel" runat="server" SkinID="Validazione01"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div align="center">
            <asp:LinkButton ID="FindLinkButton" SkinID="ZSSM_Button01" runat="server" Text="<< Ritorna all'importazione dati"
                OnClick="FindLinkButton_Click"></asp:LinkButton>
            <asp:LinkButton ID="SendLinkButton" SkinID="ZSSM_Button01" runat="server" Text="Invia immediatamente"
                OnClick="SendLinkButton_Click" ValidationGroup="SendValidationGroup"></asp:LinkButton>
            <asp:LinkButton ID="InviaTaskLinkButton" SkinID="ZSSM_Button01" runat="server" Text="Schedula invio"
                OnClick="InviaTaskLinkButton_Click" ValidationGroup="SendValidationGroup"></asp:LinkButton>
            <asp:LinkButton ID="AbortLinkButton" SkinID="ZSSM_Button01" runat="server" Text="Annulla"
                OnClick="AbortLinkButton_Click"></asp:LinkButton>
        </div>
        <br />
        <asp:GridView ID="ElencoGridView" runat="server" Visible="false">
            <Columns>
                <asp:BoundField DataField="Cognome" HeaderText="Cognome" />
                <asp:BoundField DataField="Nome" HeaderText="Nome" />
                <asp:BoundField DataField="LoweredEmail" HeaderText="E-Mail" />
            </Columns>
        </asp:GridView>
    </asp:Panel>
    <asp:Panel ID="FinalPanel" runat="server" Visible="false">
        <div class="BlockBox">
            <div class="BlockBoxHeader">
                <asp:Label ID="Label15" runat="server">Invio della comunicazione completato</asp:Label>
            </div>
            <div align="center">
                <asp:Label ID="Info1FinishLabel" runat="server" SkinID="Validazione01"></asp:Label>
            </div>
        </div>
        <div align="center">
            <asp:LinkButton ID="Find2LinkButton" SkinID="ZSSM_Button01" runat="server" Text="<< Cerca ancora"
                OnClick="Find2LinkButton_Click"></asp:LinkButton>
        </div>
    </asp:Panel>
    <div align="center">
        <asp:CustomValidator ID="ErrorCustomValidator" ErrorMessage="Attenzione: la comunicazione non può essere inviata perchè non è attiva."
            runat="server" SkinID="ZSSM_Validazione01" Display="Dynamic" />
    </div>
</asp:Content>
