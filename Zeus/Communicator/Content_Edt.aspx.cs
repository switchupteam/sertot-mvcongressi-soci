﻿using ASP;
using CustomWebControls;
using System;
using System.Data.SqlClient;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per Content_Edt
/// </summary>
public partial class Communicator_Content_Edt : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = "Modifica comunicazione / newsletter";

        delinea myDelinea = new delinea();

        if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI"))
            || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM"))
            || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang")))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        if (!ReadXML(Request.QueryString["ZIM"], GetLang()))
            Response.Redirect("~/Zeus/System/Message.aspx?1");

        ReadXML_Localization(Request.QueryString["ZIM"], GetLang());

        LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");
        InsertButton.Attributes.Add("onclick", "Validate()");

        switch (GetStatusOfLog(Server.HtmlEncode(Request.QueryString["XRI"])))
        {
            case "OK":
                CheckBox ModelloCheckBox = (CheckBox)FormView1.FindControl("ModelloCheckBox");
                ModelloCheckBox.Enabled = false;
                break;
        }

        if (!Page.IsPostBack)
            SetOrariConsentiti();

        ZeusColorPicker ZeusColorPicker1 = (ZeusColorPicker)FormView1.FindControl("ZeusColorPicker1");

        if (ZeusColorPicker1 != null)
            BackgroundColorHiddenField.Value = ZeusColorPicker1.SelectedColor;
    }

    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            string myXPathEach = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_Communicator.xml"));

            if (!CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_Edt").InnerText))
                Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223599");

            Panel myPanel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPathEach);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            if (childNode.Name.IndexOf("ZMCF_") > -1)
                            {
                                myPanel = (Panel)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myPanel != null)
                                    myPanel.Visible = Convert.ToBoolean(childNode.InnerText);
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }

            SetQueryOfID2CategoriaDropDownList(mydoc.SelectSingleNode(myXPath + "ZMCD_ZIMCategoria1").InnerText
                , GetLang(), mydoc.SelectSingleNode(myXPath + "ZMCD_Cat1Liv").InnerText);
            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Communicator.xml"));

            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            if (childNode.Name.Equals("ZML_TitoloPagina_Edt"))
                            {
                                TitleField.Value = childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                                else
                                {
                                    myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name + "Label");

                                    if (myLabel != null)
                                        myLabel.Text = childNode.InnerText;
                                }
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    private bool SetOrariConsentiti()
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        try
        {
            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT [OraInizio],[OraFine]";
                sqlQuery += " FROM [tbAgents_Dashboard]";
                sqlQuery += " WHERE [Agent]='COMMUNICATOR'";
                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if ((reader["OraInizio"] != DBNull.Value)
                        && ((reader["OraFine"] != DBNull.Value)))
                    {
                        Label OrariLabel = (Label)FormView1.FindControl("OrariLabel");

                        if (OrariLabel != null)
                        {
                            if (!reader["OraFine"].ToString().Equals(reader["OraInizio"].ToString()))
                            {
                                OrariLabel.Text = "Orari di spedizione consentiti: dalle "
                                    + reader["OraInizio"].ToString() + " alle "
                                    + reader["OraFine"].ToString();
                            }
                        }
                    }
                }

                reader.Close();
                return true;
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    protected void FormView1_DataBound(object sender, EventArgs e)
    {
        //if (!PZFormView(Server.HtmlEncode(Request.QueryString["ZIM"]), GetLang()))
        //    Response.Redirect("~/Zeus/System/Message.aspx?1");

        HiddenField SMTP_PasswordHiddenField = (HiddenField)FormView1.FindControl("SMTP_PasswordHiddenField");
        TextBox SMTP_PasswordTextBox = (TextBox)FormView1.FindControl("SMTP_PasswordTextBox");
        TextBox SMTP_PasswordConfermaTextBox = (TextBox)FormView1.FindControl("SMTP_PasswordConfermaTextBox");

        SMTP_PasswordTextBox.Attributes.Add("value", SMTP_PasswordHiddenField.Value);
        SMTP_PasswordConfermaTextBox.Attributes.Add("value", SMTP_PasswordHiddenField.Value);

    }

    private string GetLang()
    {
        try
        {
            delinea myDelinea = new delinea();
            string Lang = "ITA";

            if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
            {
                if (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang"))
                    Lang = Server.HtmlEncode(Request.QueryString["Lang"]);
            }

            return Lang;
        }
        catch (Exception)
        {
            return "ITA";
        }
    }

    private bool CheckPermit(string AllRoles)
    {

        char[] delimiter = { ',' };
        bool IsPermit = false;

        foreach (string roles in Roles.GetRolesForUser(Membership.GetUser().UserName))
            if (roles.Equals("ZeusAdmin"))
            {
                IsPermit = true;
                break;
            }
            else if (AllRoles.Length > 0)
            {
                string[] PressRoles = AllRoles.Split(delimiter);

                for (int i = 0; i < PressRoles.Length; ++i)
                {
                    if (PressRoles[i].Equals(roles))
                    {
                        IsPermit = true;
                        break;
                    }
                }
            }

        return IsPermit;
    }

    private bool PZFormView(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            Panel PZV_BoxContenuto1Panel = (Panel)FormView1.FindControl("PZV_BoxContenuto1Panel");
            Panel PZV_BoxContenuto2Panel = (Panel)FormView1.FindControl("PZV_BoxContenuto2Panel");
            Label PZL_BoxContenuto1Label = (Label)FormView1.FindControl("PZL_BoxContenuto1Label");
            Label PZL_BoxContenuto2Label = (Label)FormView1.FindControl("PZL_BoxContenuto2Label");
            TextArea Contenuto2TextArea = (TextArea)FormView1.FindControl("Contenuto2TextArea");
            Label Contenuto2InfoLabel = (Label)FormView1.FindControl("Contenuto2InfoLabel");
            Panel EmailMittentePanel = (Panel)FormView1.FindControl("EmailMittentePanel");
            Panel SMSMittentePanel = (Panel)FormView1.FindControl("SMSMittentePanel");

            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            string sqlQuery = "SELECT [PZL_TitoloPagina_Edt],[PZV_BoxContenuto1],[PZV_BoxContenuto2]";
            sqlQuery += " ,[PZL_BoxContenuto1],[PZL_BoxContenuto2],[PZD_Contenuto2MaxChar],[PermitRoles_Edt],[SendMode]";
            sqlQuery += " ,PZD_ZIMCategoria1,PZV_Cat1Liv";
            sqlQuery += " FROM [tbPZ_Communicator]";
            sqlQuery += " WHERE  ZeusLangCode='" + ZeusLangCode + "' and ZeusIdModulo='" + ZeusIdModulo + "'";

            using (SqlConnection conn = new SqlConnection(strConnessione))
            {
                SqlCommand command = new SqlCommand(sqlQuery, conn);

                conn.Open();
                SqlDataReader reader = command.ExecuteReader();

                if (!reader.HasRows)
                {
                    reader.Close();
                    return false;
                }

                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                        TitleField.Value = reader.GetString(0);

                    if (!reader.IsDBNull(1))
                        PZV_BoxContenuto1Panel.Visible = reader.GetBoolean(1);

                    if (!reader.IsDBNull(2))
                        PZV_BoxContenuto2Panel.Visible = reader.GetBoolean(2);

                    if (!reader.IsDBNull(3))
                        PZL_BoxContenuto1Label.Text = reader.GetString(3);

                    if (!reader.IsDBNull(4))
                        PZL_BoxContenuto2Label.Text = reader.GetString(4);

                    if (!reader.IsDBNull(5))
                    {
                        Contenuto2TextArea.MaxLength = reader.GetInt32(5);
                        Contenuto2InfoLabel.Text += reader.GetInt32(5).ToString();
                    }

                    if (!reader.IsDBNull(6))
                        if (!CheckPermit(reader.GetString(6)))
                        {
                            reader.Close();
                            Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223598");
                        }

                    if (!reader.IsDBNull(7))
                    {
                        switch (reader.GetString(7))
                        {
                            case "SMS":
                                EmailMittentePanel.Visible = false;
                                SMSMittentePanel.Visible = true;
                                break;

                            case "EMAIL":
                                EmailMittentePanel.Visible = true;
                                SMSMittentePanel.Visible = false;
                                break;

                        }
                    }

                    if ((!reader.IsDBNull(8)) && (!reader.IsDBNull(9)))
                        if (!Page.IsPostBack)
                            SetQueryOfID2CategoriaDropDownList(reader.GetString(8),
                                GetLang(), reader.GetString(9));
                }

                reader.Close();
                return true;
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    private bool SetQueryOfID2CategoriaDropDownList(string ZeusIdModulo,
        string ZeusLangCode, string PZV_Cat1Liv)
    {
        try
        {
            SqlDataSource dsCategoria = (SqlDataSource)FormView1.FindControl("dsCategoria");
            DropDownList ID2CategoriaDropDownList = (DropDownList)FormView1.FindControl("ID2CategoriaDropDownList");
            HiddenField ID2CategoriaHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaHiddenField");

            switch (PZV_Cat1Liv)
            {
                case "123":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv1Liv2Liv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "Catliv1Liv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;

                case "23":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv2Liv3]";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;

                default:
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;
            }

            ID2CategoriaDropDownList.SelectedIndex = 
                ID2CategoriaDropDownList.Items.IndexOf(ID2CategoriaDropDownList.Items.FindByValue(ID2CategoriaHiddenField.Value));

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    protected void ID2CategoriaDropDownList_SelectIndexChange(object sender, EventArgs e)
    {
        HiddenField ID2CategoriaHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaHiddenField");
        DropDownList ID2CategoriaDropDownList = (DropDownList)sender;
        ID2CategoriaHiddenField.Value = ID2CategoriaDropDownList.SelectedValue;
    }

    private string GetStatusOfLog(string ID1Communicator)
    {
        string StatusOfLog = string.Empty;

        try
        {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(
               strConnessione))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT TOP(1) Status ";
                sqlQuery += " FROM  tbCommunicator_LogMessage";
                sqlQuery += " WHERE [ID2Communicator]=" + ID1Communicator;
                sqlQuery += " ORDER BY ID1LogMessage";

                command.CommandText = sqlQuery;
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                        StatusOfLog = reader.GetString(0);
                }

                reader.Close();
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

        return StatusOfLog;
    }

    protected void DataLimite(object sender, EventArgs e)
    {
        TextBox DataLimite = (TextBox)sender;
        if (DataLimite.Text.Length == 0)
        {
            DataLimite.Text = "31/12/2040";
        }
    }

    protected void DataIniziale(object sender, EventArgs e)
    {
        DateTime Data = new DateTime();
        Data = DateTime.Now;
        TextBox DataCreazione = (TextBox)sender;
        if (DataCreazione.Text.Length == 0)
        {
            DataCreazione.Text = Data.ToString("d");
        }
    }

    protected void RecordEdtUserHiddenField_DataBinding(object sender, EventArgs e)
    {
        HiddenField RecordEdtUserHiddenField = (HiddenField)sender;
        RecordEdtUserHiddenField.Value = Membership.GetUser().ProviderUserKey.ToString();
    }

    protected void RecordEdtDateHiddenField_DataBinding(object sender, EventArgs e)
    {
        HiddenField RecordEdtDateHiddenField = (HiddenField)sender;
        RecordEdtDateHiddenField.Value = DateTime.Now.ToString();
    }

    protected void SendMail_StartDateHiddenField_DataBinding(object sender, EventArgs e)
    {
        HiddenField SendMail_StartDateHiddenField = (HiddenField)sender;

        try
        {
            TextBox SendMail_StartDateOreTextBox = (TextBox)FormView1.FindControl("SendMail_StartDateOreTextBox");
            TextBox SendMail_StartDateMinutiTextBox = (TextBox)FormView1.FindControl("SendMail_StartDateMinutiTextBox");

            SendMail_StartDateOreTextBox.Text = Convert.ToDateTime(SendMail_StartDateHiddenField.Value).Hour.ToString();
            SendMail_StartDateMinutiTextBox.Text = Convert.ToDateTime(SendMail_StartDateHiddenField.Value).Minute.ToString();
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    private bool SaveSendMail_StartDate()
    {
        try
        {
            TextBox SendMail_StartDateTextBox = (TextBox)FormView1.FindControl("SendMail_StartDateTextBox");
            TextBox SendMail_StartDateOreTextBox = (TextBox)FormView1.FindControl("SendMail_StartDateOreTextBox");
            TextBox SendMail_StartDateMinutiTextBox = (TextBox)FormView1.FindControl("SendMail_StartDateMinutiTextBox");
            SendMail_StartDateTextBox.Text += " " + SendMail_StartDateOreTextBox.Text + ":" + SendMail_StartDateMinutiTextBox.Text;

            RegularExpressionValidator SendMail_StartDateRegularExpressionValidator = 
                (RegularExpressionValidator)FormView1.FindControl("SendMail_StartDateRegularExpressionValidator");

            if (SendMail_StartDateRegularExpressionValidator != null)
                SendMail_StartDateRegularExpressionValidator.Visible = false;

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    protected void InsertButton_Click(object sender, EventArgs e)
    {
        SaveSendMail_StartDate();
    }

    protected void NewsletterSqlDataSource_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.AffectedRows > 0)
            Response.Redirect("~/Zeus/Communicator/Content_Lst.aspx?ZIM=" + Server.HtmlEncode(Request.QueryString["ZIM"]) 
                + "&Lang=" + GetLang() 
                + "&XRI=" + Server.HtmlEncode(Request.QueryString["XRI"]));
        else Response.Redirect("~/Zeus/System/Message.aspx?UpdateErr");
    }
}