﻿using System;
using System.Data.SqlClient;
using System.Web.Security;
using System.Web.UI.WebControls;


/// <summary>
/// Descrizione di riepilogo per Content_Lst
/// </summary>
public partial class Communicator_Content_Lst : System.Web.UI.Page
{     
    static string STARTDATE = "01/01/2000";
    static string ENDDATE = "01/01/2040";

    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();

        if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
             || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM")))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        if (!ReadXML(Request.QueryString["ZIM"], GetLang()))
            Response.Redirect("~/Zeus/System/Message.aspx?1");

        ReadXML_Localization(Request.QueryString["ZIM"], GetLang());

        TitleField.Value = "Elenco comunicazioni / newsletter";

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI")) && (Request.QueryString["XRI"].Length > 0) && (isOK()))
            SingleRecordPanel.Visible = true;
        else
        {
            if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRE1")) && (Request.QueryString["XRE1"].Length > 0))
                NewsletterSqlDatasource.SelectCommand += " AND Titolo LIKE '%" + Server.HtmlEncode(Request.QueryString["XRE1"]) + "%'";

            if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI1")) && (!Request.QueryString["XRI1"].Equals("0")))
                NewsletterSqlDatasource.SelectCommand += " AND ID2Categoria=" + Server.HtmlEncode(Request.QueryString["XRI1"]);

            if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI2")) && (Request.QueryString["XRI2"].Length > 0))
                switch (Request.QueryString["XRI2"].ToLower())
                {
                    case "1":
                        NewsletterSqlDatasource.SelectCommand += " AND IsTemplate=1";
                        break;

                    case "2":
                        NewsletterSqlDatasource.SelectCommand += " AND IsTemplate=0";
                        break;
                }

            if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI3")) && (!Request.QueryString["XRI3"].Equals("0")))
                switch (Request.QueryString["XRI3"].ToLower())
                {
                    case "1":
                        NewsletterSqlDatasource.SelectCommand += " AND AttivoArea1=1";
                        break;

                    case "2":
                        NewsletterSqlDatasource.SelectCommand += " AND AttivoArea1 < 1";
                        break;
                }

            if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI4")) && (!Request.QueryString["XRI4"].Equals("0")))
                NewsletterSqlDatasource.SelectCommand += " AND ID2FlagConsensoAssociato=" + Server.HtmlEncode(Request.QueryString["XRI4"]);

            if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "PWI")) && (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "PWF"))
                && (Request.QueryString["PWI"].Length > 0) && (Request.QueryString["PWF"].Length > 0))
                NewsletterSqlDatasource.SelectCommand += " AND CONVERT(datetime, CONVERT(varchar, RecordNewDate, 103)) BETWEEN '" 
                    + Server.HtmlEncode(Request.QueryString["PWI"]) + "' AND '" + Server.HtmlEncode(Request.QueryString["PWF"]) + "'";

            else if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "PWI")) && (Request.QueryString["PWI"].Length > 0))
                NewsletterSqlDatasource.SelectCommand += " AND CONVERT(datetime, CONVERT(varchar, RecordNewDate, 103)) BETWEEN '" 
                    + Server.HtmlEncode(Request.QueryString["PWI"]) + "' AND '" + ENDDATE + "'";

            else if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "PWF")) && (Request.QueryString["PWF"].Length > 0))
                NewsletterSqlDatasource.SelectCommand += " AND CONVERT(datetime, CONVERT(varchar, RecordNewDate, 103)) BETWEEN '" 
                    + STARTDATE + "' AND '" + Server.HtmlEncode(Request.QueryString["PWF"]) + "'";
        }

        NewsletterSqlDatasource.SelectCommand += " ORDER BY ID1Communicator DESC";
    }

    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            string myXPathEach = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_Communicator.xml"));

            if (!CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_Lst").InnerText))
                Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223599");

            Panel myPanel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPathEach);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            if (childNode.Name.IndexOf("ZMCF_") > -1)
                            {
                                myPanel = (Panel)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myPanel != null)
                                    myPanel.Visible = Convert.ToBoolean(childNode.InnerText);
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Communicator.xml"));

            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            if (childNode.Name.Equals("ZML_TitoloPagina_Lst"))
                            {
                                TitleField.Value = childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                                else
                                {
                                    myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name + "Label");

                                    if (myLabel != null)
                                        myLabel.Text = childNode.InnerText;
                                }
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    private string GetLang()
    {
        try
        {
            delinea myDelinea = new delinea();
            string Lang = "ITA";

            if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
            {
                if (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang"))
                    Lang = Server.HtmlEncode(Request.QueryString["Lang"]);
            }

            return Lang;
        }
        catch (Exception)
        {
            return "ITA";
        }
    }

    private bool isOK()
    {
        try
        {
            if ((Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Communicator/Content_Edt.aspx") > 0) 
                || (Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Communicator/Content_New.aspx") > 0))
                return true;
            else return false;
        }
        catch (Exception p)
        {
            return false;
        }
    }

    private bool CheckPermit(string AllRoles)
    {
        char[] delimiter = { ',' };
        bool IsPermit = false;

        foreach (string roles in Roles.GetRolesForUser(Membership.GetUser().UserName))
            if (roles.Equals("ZeusAdmin"))
            {
                IsPermit = true;
                break;
            }
            else if (AllRoles.Length > 0)
            {
                string[] PressRoles = AllRoles.Split(delimiter);

                for (int i = 0; i < PressRoles.Length; ++i)
                {
                    if (PressRoles[i].Equals(roles))
                    {
                        IsPermit = true;
                        break;
                    }
                }
            }

        return IsPermit;
    }

    private bool PZFormView(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            string sqlQuery = "SELECT [PZL_TitoloPagina_Lst]";
            sqlQuery += " ,[PermitRoles_Lst],[SendMode]";
            sqlQuery += " FROM [tbPZ_Communicator]";
            sqlQuery += " WHERE  ZeusLangCode='" + ZeusLangCode + "' and ZeusIdModulo='" + ZeusIdModulo + "'";

            using (SqlConnection conn = new SqlConnection(strConnessione))
            {
                SqlCommand command = new SqlCommand(sqlQuery, conn);

                conn.Open();
                SqlDataReader reader = command.ExecuteReader();

                if (!reader.HasRows)
                {
                    reader.Close();
                    return false;
                }

                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                        TitleField.Value = reader.GetString(0);

                    if (!reader.IsDBNull(1))
                        if (!CheckPermit(reader.GetString(1)))
                        {
                            reader.Close();
                            Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223598");
                        }
                }

                reader.Close();
                return true;
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {           
            if (e.Row.DataItemIndex > -1)
            {
                Image AttivoImage = (Image)e.Row.FindControl("AttivoImage");
                HiddenField AttivoArea1HiddenField = (HiddenField)e.Row.FindControl("AttivoArea1HiddenField");

                int intAttivoArea1 = Convert.ToInt32(AttivoArea1HiddenField.Value.ToString());

                if (intAttivoArea1 == 1)
                    AttivoImage.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";

                Image ModelloImage = (Image)e.Row.FindControl("ModelloImage");
                HiddenField ModelloHiddenField = (HiddenField)e.Row.FindControl("ModelloHiddenField");

                if (Convert.ToBoolean(ModelloHiddenField.Value))
                {
                    ModelloImage.ImageUrl = "~/Zeus/SiteImg/Ico1_PaginaPadre.gif";
                    ModelloImage.Visible = true;
                }
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    protected void NewsletterSqlDatasource_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.Exception != null)
            Response.Redirect("~/Zeus/System/Message.aspx?SelectErr");
    }
}