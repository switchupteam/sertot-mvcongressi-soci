﻿using System;
using System.Data.SqlClient;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per LogSendMessage
/// </summary>
public partial class Communicator_LogSendMessage : System.Web.UI.UserControl
{
    private string myID2Communicator = string.Empty;

    public string ID2Communicator
    {
        set { myID2Communicator = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        BindTheData();
    }

    public void BindTheData()
    {
        LogSendMessageSqlDataSource.SelectCommand = "SELECT [ID2Communicator],[ID1LogMessage] ,[DestinatariTotale],";
        LogSendMessageSqlDataSource.SelectCommand += " [DestinatariInviati], [SendDateStart],[SendType],";
        LogSendMessageSqlDataSource.SelectCommand += " [SMTP_Server],[SendDateEnd], [Utente], [Descrizione],[Note],[Status],[Tracking]";
        LogSendMessageSqlDataSource.SelectCommand += "  FROM [vwLogSendMessage]";

        if (myID2Communicator.Length > 0)
            LogSendMessageSqlDataSource.SelectCommand += " WHERE ID2Communicator=" + myID2Communicator;

        LogSendMessageSqlDataSource.SelectCommand += " ORDER BY SendDateStart";
        GridView1.DataSourceID = "LogSendMessageSqlDataSource";
        GridView1.DataBind();
    }

    private bool AnnullaThread(string ID1LogMessage)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {
            SqlTransaction transaction = null;

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                transaction = connection.BeginTransaction();
                command.Transaction = transaction;
                sqlQuery = "DELETE FROM [tbCommunicator_TempRecipients]";
                sqlQuery += " WHERE ID2LogMessage=" + ID1LogMessage;
                sqlQuery += " AND SendType='SCHED'";
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();

                sqlQuery = "UPDATE [tbCommunicator_LogMessage]";
                sqlQuery += " SET Status='ANN'";
                sqlQuery += " WHERE ID1LogMessage=" + ID1LogMessage;
                sqlQuery += " AND SendType='SCHED'";
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();

                transaction.Commit();
                return true;
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());

                if (transaction != null)
                    transaction.Rollback();

                return false;
            }
        }
    }

    protected void AnnullaLinkButton_DataBinding(object sender, EventArgs e)
    {
        LinkButton AnnullaLinkButton = (LinkButton)sender;

        if ((!AnnullaLinkButton.Text.ToUpper().Equals("SCHED"))
            || ((!AnnullaLinkButton.CssClass.ToUpper().Equals("SND"))
            && (!AnnullaLinkButton.CssClass.ToUpper().Equals("ERR"))))
        {
            AnnullaLinkButton.Visible = false;
        }
        else
            AnnullaLinkButton.Visible = true;

        AnnullaLinkButton.Text = "Annulla Invio";
        AnnullaLinkButton.CssClass = "GridView_Button1";
    }

    protected void AnnullaLinkButton_Click(object sender, EventArgs e)
    {
        LinkButton AnnullaLinkButton = (LinkButton)sender;

        if (AnnullaThread(AnnullaLinkButton.ToolTip))
            Response.Redirect("~/Zeus/System/Message.aspx?Msg=1234602945698303");
        else Response.Redirect("~/Zeus/System/Message.aspx?AnnullaThreadErr");
    }

    protected string SwitchStateToHuman(string State)
    {
        string StateToHuman = string.Empty;

        switch (State.ToUpper())
        {
            case "OK":
                StateToHuman = "Completato";
                break;

            case "KO":
                StateToHuman = "Fallito";
                break;

            case "ERR":
                StateToHuman = "Errore";
                break;

            case "SND":
                StateToHuman = "In corso";
                break;

            case "ANN":
                StateToHuman = "Annullato";
                break;
        }

        return StateToHuman;
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.DataItemIndex > -1)
            {
                Image TrackingImage = (Image)e.Row.FindControl("TrackingImage");
                HiddenField TrackingHiddenField = (HiddenField)e.Row.FindControl("TrackingHiddenField");

                int intTracking = Convert.ToInt32(TrackingHiddenField.Value.ToString());

                if (intTracking == 1)
                    TrackingImage.ImageUrl = "~/Zeus/SiteImg/Ico1_EyeOn.png";
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }
}