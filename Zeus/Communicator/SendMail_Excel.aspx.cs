﻿using ASP;
using CustomWebControls;
using GemBox.Spreadsheet;
using System;
using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml.Linq;

/// <summary>
/// Descrizione di riepilogo per SendMail_Excel
/// </summary>
public partial class Communicator_SendMail_Excel : System.Web.UI.Page
{       
    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = "Invio comunicazione";

        delinea myDelinea = new delinea();

        if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI"))
            || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM"))
            || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang")))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        if (!ReadXML(Request.QueryString["ZIM"], GetLang()))
            Response.Redirect("~/Zeus/System/Message.aspx?1");

        ReadXML_Localization(Request.QueryString["ZIM"], GetLang());

        string firstScript = "/SiteJS/jquery-1.2.6.js";
        string secondScript = "/SiteJS/jQuery.BlockUI.js";

        HtmlGenericControl Include = new HtmlGenericControl("script");
        Include.Attributes.Add("type", "text/javascript");
        Include.Attributes.Add("src", firstScript);
        this.Page.Header.Controls.Add(Include);

        Include = new HtmlGenericControl("script");
        Include.Attributes.Add("type", "text/javascript");
        Include.Attributes.Add("src", secondScript);
        this.Page.Header.Controls.Add(Include);

        SendLinkButton.Attributes.Add("onfocus", "javascript:return SetIsLoadButtonToTrue();");
        InviaTaskLinkButton.Attributes.Add("onfocus", "javascript:return SetIsLoadButtonToTrue();");

        LogSendMessage LogSendMessage1 = (LogSendMessage)FormView1.FindControl("LogSendMessage1");
        LogSendMessage1.ID2Communicator = Server.HtmlEncode(Request.QueryString["XRI"]);

        switch (GetStatusOfLog(Server.HtmlEncode(Request.QueryString["XRI"])))
        {
            case "ERR":
                SearchPanel.Visible = false;
                FinalPanel.Visible = false;
                InfoPanel.Visible = false;
                ErrorPanel.Visible = true;
                SendingPanel.Visible = false;
                break;

            case "SND":
                SearchPanel.Visible = false;
                FinalPanel.Visible = false;
                InfoPanel.Visible = false;
                ErrorPanel.Visible = false;
                SendingPanel.Visible = true;
                break;

            default:
                ErrorPanel.Visible = false;
                SendingPanel.Visible = false;
                break;
        }

        if (!Page.IsPostBack)
        {
            SetOrariConsentiti();
        }
    }

    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            string myXPathEach = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_Communicator.xml"));

            if (!CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_SendMail").InnerText))
                Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223599");

            Panel myPanel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPathEach);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            if (childNode.Name.IndexOf("ZMCF_") > -1)
                            {
                                myPanel = (Panel)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myPanel != null)
                                    myPanel.Visible = Convert.ToBoolean(childNode.InnerText);
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }

            PZD_SendAgentWeb_MaxQtyHiddenField.Value = mydoc.SelectSingleNode(myXPath + "ZMCD_SendAgentWeb_MaxQty").InnerText;
            PZD_UnsubscribeLink.Value = mydoc.SelectSingleNode(myXPath + "ZMCD_UnsubscribeLink").InnerText;
            ZMCD_UnsubscribeLink.Value = mydoc.SelectSingleNode(myXPath + "ZMCD_UnsubscribeLink").InnerText;
            ZMCD_WebLink.Value = mydoc.SelectSingleNode(myXPath + "ZMCD_WebLink").InnerText;
            ZMCD_UrlDomain.Value = mydoc.SelectSingleNode(myXPath + "ZMCD_UrlDomain").InnerText;

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Communicator.xml"));

            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            if (childNode.Name.Equals("ZML_TitoloPagina_New"))
                            {
                                TitleField.Value = childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                                else
                                {
                                    myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name + "Label");

                                    if (myLabel != null)
                                        myLabel.Text = childNode.InnerText;
                                }
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    private bool SetOrariConsentiti()
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        try
        {
            using (SqlConnection connection = new SqlConnection(strConnessione))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT [OraInizio],[OraFine]";
                sqlQuery += "  FROM [tbAgents_Dashboard]";
                sqlQuery += " WHERE [Agent]='COMMUNICATOR'";
                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if ((reader["OraInizio"] != DBNull.Value)
                        && ((reader["OraFine"] != DBNull.Value)))
                    {
                        Label OrariLabel = (Label)FormView1.FindControl("OrariLabel");

                        if (OrariLabel != null)
                            if (!reader["OraFine"].ToString().Equals(reader["OraInizio"].ToString()))
                                OrariLabel.Text = "Orari di spedizione consentiti: dalle "
                                    + reader["OraInizio"].ToString() + " alle "
                                    + reader["OraFine"].ToString();
                    }
                }

                reader.Close();
                return true;
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    protected void FormView1_DataBound(object sender, EventArgs e)
    {
        HiddenField SMTP_PasswordHiddenField = (HiddenField)FormView1.FindControl("SMTP_PasswordHiddenField");
        TextBox SMTP_PasswordTextBox = (TextBox)FormView1.FindControl("SMTP_PasswordTextBox");
        TextBox SMTP_PasswordConfermaTextBox = (TextBox)FormView1.FindControl("SMTP_PasswordConfermaTextBox");

        SMTP_PasswordTextBox.Attributes.Add("value", SMTP_PasswordHiddenField.Value);
        SMTP_PasswordConfermaTextBox.Attributes.Add("value", SMTP_PasswordHiddenField.Value);
    }

    private string GetLang()
    {
        try
        {
            delinea myDelinea = new delinea();
            string Lang = "ITA";

            if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
            {
                if (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang"))
                    Lang = Server.HtmlEncode(Request.QueryString["Lang"]);
            }

            return Lang;
        }
        catch (Exception)
        {
            return "ITA";
        }
    }

    private bool CheckPermit(string AllRoles)
    {
        char[] delimiter = { ',' };
        bool IsPermit = false;

        foreach (string roles in Roles.GetRolesForUser(Membership.GetUser().UserName))
            if (roles.Equals("ZeusAdmin"))
            {
                IsPermit = true;
                break;
            }
            else if (AllRoles.Length > 0)
            {
                string[] PressRoles = AllRoles.Split(delimiter);

                for (int i = 0; i < PressRoles.Length; ++i)
                {
                    if (PressRoles[i].Equals(roles))
                    {
                        IsPermit = true;
                        break;
                    }
                }
            }

        return IsPermit;
    }
    
    private string GetFlagConsenso(string ID1Communicator)
    {
        string FlagConsenso = string.Empty;

        try
        {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            string sqlQuery = "SELECT DISTINCT tbCommunicator_FlagConsenso.Descrizione";
            sqlQuery += " FROM tbCommunicator INNER JOIN tbCommunicator_FlagConsenso";
            sqlQuery += " ON tbCommunicator.ID2FlagConsensoAssociato = tbCommunicator_FlagConsenso.ID1FlagConsenso ";
            sqlQuery += " WHERE tbCommunicator.ID1Communicator=" + ID1Communicator;

            using (SqlConnection conn = new SqlConnection(strConnessione))
            {
                SqlCommand command = new SqlCommand(sqlQuery, conn);

                conn.Open();
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                    if (!reader.IsDBNull(0))
                        FlagConsenso = reader.GetString(0);

                reader.Close();
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

        return FlagConsenso;
    }

    protected void AttivoArea1HiddenField_DataBinding(object sender, EventArgs e)
    {
        HiddenField AttivoArea1HiddenField = (HiddenField)sender;

        if (Convert.ToInt32(AttivoArea1HiddenField.Value.ToString()) <= 0)
        {
            SearchPanel.Visible = false;
            InfoPanel.Visible = false;
            ErrorCustomValidator.IsValid = false;
        }
    }

    protected void CommunicatorSqlDataSource_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.AffectedRows <= 0)
            Response.Redirect("~/Zeus/System/Message.aspx?SelectErr");
    }

    private string GetStatusOfLog(string ID1Communicator)
    {
        string StatusOfLog = string.Empty;

        try
        {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(
               strConnessione))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT TOP(1) Status ";
                sqlQuery += " FROM  tbCommunicator_LogMessage";
                sqlQuery += " WHERE [ID2Communicator]=" + ID1Communicator + " AND SendType='IMMED'";
                sqlQuery += " ORDER BY ID1LogMessage DESC";

                command.CommandText = sqlQuery;
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                        StatusOfLog = reader.GetString(0);
                }

                reader.Close();
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

        return StatusOfLog;
    }

    private string GetTableColumnName(string ID1FlagConsenso)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {
            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                if (!ID1FlagConsenso.Equals("0"))
                {
                    sqlQuery = "SELECT DISTINCT tbCommunicator_FlagConsenso.CampoDB";
                    sqlQuery += " FROM tbCommunicator INNER JOIN tbCommunicator_FlagConsenso";
                    sqlQuery += " ON tbCommunicator.ID2FlagConsensoAssociato = tbCommunicator_FlagConsenso.ID1FlagConsenso";
                    sqlQuery += " WHERE ID1FlagConsenso=" + ID1FlagConsenso;
                    command.CommandText = sqlQuery;
                }
                else return string.Empty;

                SqlDataReader reader = command.ExecuteReader();

                string NewsLetter = string.Empty;

                while (reader.Read())
                    NewsLetter = reader.GetString(0);

                reader.Close();

                return NewsLetter;
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
                return string.Empty;
            }
        }
    }

    private int GetNumberOf(
        string ID1FlagConsenso, string Nome, string Cognome,
        string RagioneSociale, string Email, bool Consenso)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {
            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT COUNT(LoweredEmail)  ";
                sqlQuery += " FROM tbProfiliPersonali INNER JOIN tbProfiliMarketing ";
                sqlQuery += " ON tbProfiliPersonali.UserId = tbProfiliMarketing.UserId ";
                sqlQuery += " INNER JOIN tbProfiliSocietari ON tbProfiliPersonali.UserId = tbProfiliSocietari.UserId ";
                sqlQuery += " INNER JOIN aspnet_Membership ON tbProfiliPersonali.UserId = aspnet_Membership.UserId";
                sqlQuery += " LEFT OUTER JOIN dbo.tbBusinessBook ON dbo.tbProfiliSocietari.ID2BusinessBook = dbo.tbBusinessBook.ID1BsnBook";
                sqlQuery += " WHERE IsLockedOut=0 AND IsApproved=1";

                if (Consenso)
                {
                    string NewsLetter = GetTableColumnName(ID1FlagConsenso);

                    if (NewsLetter.Length > 0)
                        sqlQuery += " AND " + NewsLetter + "=1";
                }

                if (Nome.Length > 0)
                    sqlQuery += " AND Nome LIKE '%" + Nome + "%'";

                if (Cognome.Length > 0)
                    sqlQuery += " AND Cognome LIKE '%" + Cognome + "%' ";

                if (RagioneSociale.Length > 0)
                    sqlQuery += " AND tbBusinessBook.RagioneSociale LIKE '%" + RagioneSociale + "%'";

                if (Email.Length > 0)
                    sqlQuery += " AND LoweredEmail LIKE '%" + Email + "%'";

                sqlQuery += " AND aspnet_Membership.UserId  NOT IN";
                sqlQuery += " (SELECT   tbCommunicator_LogUnsubscribe.UserId ";
                sqlQuery += " FROM  tbCommunicator_LogUnsubscribe INNER JOIN ";
                sqlQuery += " tbCommunicator_LogMessage ON tbCommunicator_LogUnsubscribe.ID2LogMessage = tbCommunicator_LogMessage.ID1LogMessage ";
                sqlQuery += " WHERE tbCommunicator_LogMessage.ID2Communicator=" + Server.HtmlEncode(Request.QueryString["XRI"]) + ")";

                if (ConfigurationManager.AppSettings["MissingMail"] != null)
                    sqlQuery += " AND aspnet_Membership.LoweredEmail <> '" + ConfigurationManager.AppSettings["MissingMail"] + "' ";

                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();

                int myReturn = 0;

                while (reader.Read())
                    myReturn = reader.GetInt32(0);

                return myReturn;
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
                return -1;
            }
        }
    }

    private string AddCSS()
    {
        string CSS = string.Empty;
        string filePath = "~/App_Themes/HtmlEditor/Communicator.css";

        try
        {
            System.IO.StreamReader streamReader = new System.IO.StreamReader(Server.MapPath(filePath));
            CSS = streamReader.ReadToEnd();
            streamReader.Close();
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

        return CSS;
    }

    private string AddHtml(string myBody, string BackgroundColor)
    {
        string HTMLHead = string.Empty;

        HTMLHead += "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
        HTMLHead += " <html xmlns=\"http://www.w3.org/1999/xhtml\"> ";
        HTMLHead += " <head> ";
        HTMLHead += " <title>Communicator</title> ";
        HTMLHead += " <style type=\"text/css\"> ";
        HTMLHead += AddCSS();
        HTMLHead += " </style> ";
        HTMLHead += " </head> ";

        if (BackgroundColor.Length <= 0)
            BackgroundColor = "FFFFFF";

        HTMLHead += "<body bgcolor=\"#" + BackgroundColor + "\"";
        HTMLHead += " <div style=\"background-color:#" + BackgroundColor + "; width:100%;padding:0;margin:0;border:0px;overflow:hidden;\"> ";

        if (Request.ServerVariables["SERVER_NAME"] != null)
            HTMLHead += " <link href=\"http://" + Request.ServerVariables["SERVER_NAME"] 
                + "/app_themes/htmleditor/communicator.css\" rel=\"stylesheet\" type=\"text/css\" /> ";

        HTMLHead += myBody;

        if (BackgroundColor.Length > 0)
            HTMLHead += "</div>";

        HTMLHead += " </body> ";
        HTMLHead += " </html> ";

        return HTMLHead;
    }

    private string[] GetCommunicatorInfo(string ID2Communicator)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;
        string[] CommunicatorInfo = null;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {
            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT Titolo,Sender_Oggetto,Sender_Nome,Sender_Email";
                sqlQuery += ",SMTP_Server,SMTP_User,SMTP_Password,SendMail_StartDate,Contenuto1,BackgroundColor";
                sqlQuery += " FROM tbCommunicator ";
                sqlQuery += " WHERE ID1Communicator=" + ID2Communicator;
                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();

                ArrayList myArray = new ArrayList();

                string BackgroundColor = string.Empty;

                while (reader.Read())
                {
                    if (reader["Titolo"] != DBNull.Value)
                        myArray.Add(reader["Titolo"].ToString());
                    else myArray.Add(string.Empty);

                    if (reader["Sender_Oggetto"] != DBNull.Value)
                        myArray.Add(reader["Sender_Oggetto"].ToString());
                    else myArray.Add(string.Empty);

                    if (reader["Sender_Nome"] != DBNull.Value)
                        myArray.Add(reader["Sender_Nome"].ToString());
                    else myArray.Add(string.Empty);

                    if (reader["Sender_Email"] != DBNull.Value)
                        myArray.Add(reader["Sender_Email"].ToString());
                    else myArray.Add(string.Empty);

                    if (reader["SMTP_Server"] != DBNull.Value)
                        myArray.Add(reader["SMTP_Server"].ToString());
                    else myArray.Add(string.Empty);

                    if (reader["SMTP_User"] != DBNull.Value)
                        myArray.Add(reader["SMTP_User"].ToString());
                    else myArray.Add(string.Empty);

                    if (reader["SMTP_Password"] != DBNull.Value)
                        myArray.Add(reader["SMTP_Password"].ToString());
                    else myArray.Add(string.Empty);

                    if (reader["SendMail_StartDate"] != DBNull.Value)
                        myArray.Add(Convert.ToDateTime(reader["SendMail_StartDate"]).ToString());
                    else myArray.Add(DateTime.Now.ToString("d"));

                    if (reader["BackgroundColor"] != DBNull.Value)
                        if (!reader["BackgroundColor"].ToString().ToLower().Equals("trasparente"))
                            BackgroundColor = reader["BackgroundColor"].ToString();

                    if (reader["Contenuto1"] != DBNull.Value)
                    {
                        string Contenuto = AddHtml(reader["Contenuto1"].ToString(), BackgroundColor);
                        myArray.Add(Contenuto);
                    }
                    else myArray.Add(string.Empty);
                }
                
                reader.Close();
                CommunicatorInfo = (string[])myArray.ToArray(typeof(string));
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
            }
        }

        return CommunicatorInfo;
    }

    private static string[] RemoveDuplicates(string[] s)
    {
        System.Collections.Generic.HashSet<string> set = new System.Collections.Generic.HashSet<string>(s);
        string[] result = new string[set.Count];
        set.CopyTo(result);
        return result;
    }

    private string ReplaceAttachment(string Contenuto, string ID2Communicator, string UserId)
    {
        try
        {
            string SubContenuto = Contenuto;
            string SubLink = string.Empty;
            int index = 0;
            string html = string.Empty;
            string pattern = string.Empty;
            Regex rx = null;
            Match m = null;
            ArrayList myArray = new ArrayList();

            while (SubContenuto.IndexOf("[[[#LOG#") > -1)
            {
                index = SubContenuto.IndexOf("[[[#LOG#");
                SubContenuto = SubContenuto.Substring(index + 8);
                index = SubContenuto.IndexOf("]]]");

                if (index > -1)
                {
                    html = SubContenuto.Substring(0, SubContenuto.Length - (SubContenuto.Substring(index).Length));
                    pattern = @"<A\shref=""(?<FilePath>[^""]*)"">(?<File>[^<]*)";
                    rx = new Regex(pattern, RegexOptions.IgnoreCase);
                    m = rx.Match(html);

                    while (m.Success)
                    {
                        myArray.Add(m.Groups["FilePath"].Value);
                        m = m.NextMatch();
                    }      
                }
            }

            string[] myData = RemoveDuplicates((string[])myArray.ToArray(typeof(string)));

            if (myData.Length > 0)
            {
                String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
                string sqlQuery = string.Empty;
                string ID1Allegato = string.Empty;

                using (SqlConnection connection = new SqlConnection(strConnessione))
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    SqlDataReader reader = null;

                    for (int i = 0; i < myData.Length; ++i)
                    {
                        sqlQuery = "if not exists (select ID1Allegato from [tbCommunicator_Allegati] where [Path] = @Path)";
                        sqlQuery += " begin ";
                        sqlQuery += "insert into [tbCommunicator_Allegati]";
                        sqlQuery += "([ID2Communicator],[Path],[RecordNewDate]) ";
                        sqlQuery += " VALUES (@ID2Communicator,@Path,GetDate()); ";
                        sqlQuery += " SELECT Scope_Identity() ";
                        sqlQuery += " end ";
                        sqlQuery += " else ";
                        sqlQuery += " select ID1Allegato from [tbCommunicator_Allegati] where [Path] = @Path ";

                        command.CommandText = sqlQuery;
                        command.Parameters.Add("@ID2Communicator", System.Data.SqlDbType.Int);
                        command.Parameters["@ID2Communicator"].Value = ID2Communicator;
                        command.Parameters.Add("@Path", System.Data.SqlDbType.NVarChar);
                        command.Parameters["@Path"].Value = myData[i].Trim();

                        try
                        {
                            ID1Allegato = Convert.ToInt32(command.ExecuteScalar()).ToString();
                        }
                        catch (Exception)
                        {
                            reader = command.ExecuteReader();

                            while (reader.Read())
                                ID1Allegato = Convert.ToInt32(reader["ID1Allegato"]).ToString();

                            reader.Close();
                        }

                        command.Parameters.Clear();
                        Contenuto = Contenuto.Replace(myData[i], "http://" 
                            + Request.ServerVariables["SERVER_NAME"] 
                            + "/Communicator/Click.aspx?XRI=" + ID1Allegato 
                            + "&UID=" + UserId);
                    }
                }

                Contenuto = Contenuto.Replace("[[[#LOG#", string.Empty);
                Contenuto = Contenuto.ToLower().Replace("]]]", string.Empty);
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

        return Contenuto;
    }

    private string ReplaceOurTag(string Contenuto, string ID1LogMessage, string EMail, string UserId, string ID2Communicator)
    {
        try
        {
            if (ZMCD_UnsubscribeLink.Value != string.Empty && ID1LogMessage != string.Empty
                && ID2Communicator != string.Empty && UserId != string.Empty)
                Contenuto = Contenuto.Replace("#COMMPRIVLW1", ZMCD_UnsubscribeLink.Value
                    + "?MSG=" + ID1LogMessage
                    + "&COM=" + ID2Communicator
                    + "&USR=" + UserId
                    + "&Lang=" + Request.QueryString["Lang"]);

            if (ZMCD_WebLink.Value != string.Empty && ID2Communicator != string.Empty)
                Contenuto = Contenuto.Replace("#COMMURLWEB", ZMCD_WebLink.Value
                    + "?Lang=" + Request.QueryString["Lang"]
                    + "&XRI=" + ID2Communicator
                    + "&ZIM=" + Request.QueryString["ZIM"]);

            if (ZMCD_UrlDomain.Value != string.Empty)
            {
                Contenuto = Contenuto.Replace("src=\"/ZeusInc", "src=\"" + ZMCD_UrlDomain.Value + "/ZeusInc");
                Contenuto = Contenuto.Replace("href=\"/ZeusInc", "href=\"" + ZMCD_UrlDomain.Value + "/ZeusInc");
                Contenuto = Contenuto.Replace("src=\"~/ZeusInc", "src=\"" + ZMCD_UrlDomain.Value + "ZeusInc");
                Contenuto = Contenuto.Replace("href=\"~/ZeusInc", "href=\"" + ZMCD_UrlDomain.Value + "ZeusInc");
                Contenuto = Contenuto.Replace("src=\"../ZeusInc", "src=\"" + ZMCD_UrlDomain.Value + "ZeusInc");
                Contenuto = Contenuto.Replace("href=\"../ZeusInc", "href=\"" + ZMCD_UrlDomain.Value + "ZeusInc");
                Contenuto = Contenuto.Replace("src=\"../../ZeusInc", "src=\"" + ZMCD_UrlDomain.Value + "ZeusInc");
                Contenuto = Contenuto.Replace("href=\"../../ZeusInc", "href=\"" + ZMCD_UrlDomain.Value + "ZeusInc");
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

        return Contenuto;
    }

    private bool WriteTempRecipients( string ID2Communicator,
        string ID2LogMessage, bool Consenso, string SendType)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;
        SqlTransaction transaction = null;
        SqlDataReader reader = null;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {
            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                transaction = connection.BeginTransaction();
                command.Transaction = transaction;

                string[] TempRecipients = Regex.Split(EXCEL_EMAIL_ADDRESS.Value, ",");
                string[] CommunicatorInfo = GetCommunicatorInfo(ID2Communicator);

                TextBox SMTP_ServerTextBox = (TextBox)FormView1.FindControl("SMTP_ServerTextBox");
                TextBox SMTP_UserTextBox = (TextBox)FormView1.FindControl("SMTP_UserTextBox");
                TextBox SMTP_PasswordTextBox = (TextBox)FormView1.FindControl("SMTP_PasswordTextBox");

                XElement MyXml = null;
                XElement Mail = null;

                try
                {
                    MyXml = XElement.Load(MapPath("~/ZeusInc/TrackingMailSent/Report_") + ID2LogMessage + ".xml");
                }
                catch (Exception)
                {
                    MyXml = new XElement("MailSent");
                    MyXml.Save(MapPath("~/ZeusInc/TrackingMailSent/Report_") + ID2LogMessage + ".xml");
                }                                              

                for (int i = 0; i < TempRecipients.Length; ++i)
                {
                    sqlQuery = " INSERT INTO [tbCommunicator_TempRecipients]";
                    sqlQuery += " ([ID2LogMessage],[Destinatario_UserId],Destinatario_Email";
                    sqlQuery += ",Destinatario_Cognome,Destinatario_Nome,Destinatario_Sesso";
                    sqlQuery += ",DataOraConsegna,Titolo,Sender_Oggetto,Sender_Nome,Sender_Email";
                    sqlQuery += ",SMTP_Server,SMTP_User,SMTP_Password,SendMail_StartDate,Contenuto1,SendType";
                    sqlQuery += ")";
                    sqlQuery += " VALUES (@ID2LogMessage,NULL,@Destinatario_Email";
                    sqlQuery += ",@Destinatario_Cognome,@Destinatario_Nome,@Destinatario_Sesso";
                    sqlQuery += ",@DataOraConsegna,@Titolo,@Sender_Oggetto,@Sender_Nome,@Sender_Email";
                    sqlQuery += ",@SMTP_Server,@SMTP_User,@SMTP_Password,@SendMail_StartDate,@Contenuto1,@SendType";
                    sqlQuery += ")";

                    command.Parameters.Add("@ID2LogMessage", System.Data.SqlDbType.Int);
                    command.Parameters["@ID2LogMessage"].Value = ID2LogMessage;
                   
                    command.Parameters.Add("@Destinatario_Email", System.Data.SqlDbType.NVarChar);
                    command.Parameters["@Destinatario_Email"].Value = TempRecipients[i];
                    command.Parameters.Add("@Destinatario_Cognome", System.Data.SqlDbType.NVarChar);
                    command.Parameters["@Destinatario_Cognome"].Value = string.Empty;
                    command.Parameters.Add("@Destinatario_Nome", System.Data.SqlDbType.NVarChar);
                    command.Parameters["@Destinatario_Nome"].Value = string.Empty;
                    command.Parameters.Add("@Destinatario_Sesso", System.Data.SqlDbType.NVarChar);
                    command.Parameters["@Destinatario_Sesso"].Value = string.Empty;
                    command.Parameters.Add("@DataOraConsegna", System.Data.SqlDbType.SmallDateTime);
                    command.Parameters["@DataOraConsegna"].Value = DateTime.Now;

                    command.Parameters.Add("@Titolo", System.Data.SqlDbType.NVarChar);
                    command.Parameters["@Titolo"].Value = CommunicatorInfo[0];
                    command.Parameters.Add("@Sender_Oggetto", System.Data.SqlDbType.NVarChar);
                    command.Parameters["@Sender_Oggetto"].Value = CommunicatorInfo[1];
                    command.Parameters.Add("@Sender_Nome", System.Data.SqlDbType.NVarChar);
                    command.Parameters["@Sender_Nome"].Value = CommunicatorInfo[2];
                    command.Parameters.Add("@Sender_Email", System.Data.SqlDbType.NVarChar);
                    command.Parameters["@Sender_Email"].Value = CommunicatorInfo[3];

                    command.Parameters.Add("@SMTP_Server", System.Data.SqlDbType.NVarChar);
                    if ((SMTP_ServerTextBox != null)
                        && (SMTP_ServerTextBox.Text.Length > 0))
                        command.Parameters["@SMTP_Server"].Value = SMTP_ServerTextBox.Text;
                    else
                        command.Parameters["@SMTP_Server"].Value = CommunicatorInfo[4];

                    command.Parameters.Add("@SMTP_User", System.Data.SqlDbType.NVarChar);
                    if ((SMTP_UserTextBox != null)
                        && (SMTP_UserTextBox.Text.Length > 0))
                        command.Parameters["@SMTP_User"].Value = SMTP_UserTextBox.Text;
                    else command.Parameters["@SMTP_User"].Value = CommunicatorInfo[5];

                    command.Parameters.Add("@SMTP_Password", System.Data.SqlDbType.NVarChar);
                    if ((SMTP_PasswordTextBox != null)
                        && (SMTP_PasswordTextBox.Text.Length > 0))
                        command.Parameters["@SMTP_Password"].Value = SMTP_PasswordTextBox.Text;
                    else command.Parameters["@SMTP_Password"].Value = CommunicatorInfo[6];

                    command.Parameters.Add("@SendMail_StartDate", System.Data.SqlDbType.SmallDateTime);
                    command.Parameters["@SendMail_StartDate"].Value = Convert.ToDateTime(CommunicatorInfo[7]);
                    command.Parameters.Add("@Contenuto1", System.Data.SqlDbType.NVarChar);
                    command.Parameters["@Contenuto1"].Value = ReplaceOurTag(CommunicatorInfo[8].ToString(),ID2LogMessage,
                        TempRecipients[i],string.Empty,ID2Communicator);
                    command.Parameters.Add("@SendType", System.Data.SqlDbType.NVarChar);
                    command.Parameters["@SendType"].Value = SendType;                   
                    command.CommandText = sqlQuery;
                    command.ExecuteNonQuery();
                    command.Parameters.Clear();

                    Mail = new XElement("Mail",
                                new XElement("UserId", ""),
                                new XElement("EMail", TempRecipients[i]),
                                new XElement("Nome", ""),
                                new XElement("Cognome", ""),
                                new XElement("Oggetto", ""),
                                new XElement("Esito", "Schedulata"),
                                new XElement("DataInvio", "Non Inviato"));

                    MyXml.Add(Mail);
                    MyXml.Save(MapPath("~/ZeusInc/TrackingMailSent/Report_") + ID2LogMessage + ".xml");
                }

                transaction.Commit();
                return true;
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());

                if (reader != null)
                    reader.Close();

                if (transaction != null)
                    transaction.Rollback();

                return false;
            }
        }
    }

    private string OpenTicketLog(string ID1Communicator, string DestinatariTotale,
        string SMTP_Server, string SMTP_User, string SMTP_Password, string Note, string SendType)
    {
        try
        {
            string ID1Log = string.Empty;
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(
               strConnessione))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "INSERT INTO [tbCommunicator_LogMessage] ";
                sqlQuery += " ([ID2Communicator],[SendDateStart],[DestinatariTotale]";
                sqlQuery += ",[ID2FlagConsenso],[SendUser],[SMTP_Server],[SMTP_User],[SMTP_Password],[Status],[Note],[SendType])";
                sqlQuery += " VALUES (" + ID1Communicator;
                sqlQuery += ",@RunStart";
                sqlQuery += "," + DestinatariTotale;
                sqlQuery += ",0";
                sqlQuery += ",@SendUser";
                sqlQuery += ",@SMTP_Server";
                sqlQuery += ",@SMTP_User";
                sqlQuery += ",@SMTP_Password";
                sqlQuery += ",'SND'";
                sqlQuery += " ,@Note";
                sqlQuery += " ,'" + SendType + "');";
                sqlQuery += "SELECT Scope_Identity()";

                command.Parameters.Add("@RunStart", System.Data.SqlDbType.SmallDateTime);
                command.Parameters["@RunStart"].Value = DateTime.Now;
                command.Parameters.Add("@SendUser", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@SendUser"].Value = Membership.GetUser(true).ProviderUserKey;
                command.Parameters.Add("@SMTP_Server", System.Data.SqlDbType.NVarChar);
                command.Parameters["@SMTP_Server"].Value = SMTP_Server;
                command.Parameters.Add("@SMTP_User", System.Data.SqlDbType.NVarChar);
                command.Parameters["@SMTP_User"].Value = SMTP_User;
                command.Parameters.Add("@SMTP_Password", System.Data.SqlDbType.NVarChar);
                command.Parameters["@SMTP_Password"].Value = SMTP_Password;
                command.Parameters.Add("@Note", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Note"].Value = Note;
                command.CommandText = sqlQuery;

                ID1Log = Convert.ToInt32(command.ExecuteScalar()).ToString();
            }

            return ID1Log;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return string.Empty;
        }
    }

    private bool AbortTicketLog(string ID1Communicator)
    {
        try
        {
            string sqlQuery = string.Empty;
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            using (SqlConnection connection = new SqlConnection(
               strConnessione))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "UPDATE [tbCommunicator_LogMessage] ";
                sqlQuery += " SET [SendDateEnd]=@SendDateEnd";
                sqlQuery += " ,Status='KO'";
                sqlQuery += " WHERE ID1LogMessage IN ( ";
                sqlQuery += " SELECT MAX([ID1LogMessage]) ";
                sqlQuery += " FROM  tbCommunicator_LogMessage";
                sqlQuery += " WHERE [ID2Communicator]=" + ID1Communicator;
                sqlQuery += " AND Status='ERR' AND SendType='IMMED')";

                command.Parameters.Add("@SendDateEnd", System.Data.SqlDbType.SmallDateTime);
                command.Parameters["@SendDateEnd"].Value = DateTime.Now;

                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();
            }

            return true;
        }
        catch (Exception p)
        {
            Console.Write(p.ToString());
            return false;
        }
    }

    private bool IsSendingLog(string ID1Communicator)
    {
        bool SendingLog = false;

        try
        {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(
               strConnessione))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT MAX([ID1LogMessage]) ";
                sqlQuery += " FROM  tbCommunicator_LogMessage";
                sqlQuery += " WHERE [Status] ='SND'";
                sqlQuery += " AND SendType='IMMED' ";
                sqlQuery += " AND [ID2Communicator]=" + ID1Communicator;

                command.CommandText = sqlQuery;
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                        SendingLog = true;
                }

                reader.Close();
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

        return SendingLog;
    }

    private bool IsPendantsLog(string ID1Communicator)
    {
        bool PendantsLog = false;

        try
        {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(strConnessione))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT MAX([ID1LogMessage]) ";
                sqlQuery += " FROM  tbCommunicator_LogMessage INNER JOIN";
                sqlQuery += " tbCommunicator_TempRecipients ON tbCommunicator_LogMessage.ID1LogMessage = tbCommunicator_TempRecipients.ID2LogMessage";
                sqlQuery += " WHERE [Status] ='ERR'";
                sqlQuery += " AND SendType='IMMED'";
                sqlQuery += " AND [ID2Communicator]=" + ID1Communicator;

                command.CommandText = sqlQuery;
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                        PendantsLog = true;
                }

                reader.Close();
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

        return PendantsLog;
    }

    private string GetID2LogMessageFailed(string ID1Communicator)
    {
        string ID2LogMessageFailed = string.Empty;

        try
        {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(
               strConnessione))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT MAX([ID1LogMessage]) ";
                sqlQuery += " FROM  tbCommunicator_LogMessage INNER JOIN";
                sqlQuery += " tbCommunicator_TempRecipients ON tbCommunicator_LogMessage.ID1LogMessage = tbCommunicator_TempRecipients.ID2LogMessage";
                sqlQuery += " WHERE [SendDateEnd] IS NULL";
                sqlQuery += " AND SendType='IMMED'";
                sqlQuery += " AND [ID2Communicator]=" + ID1Communicator;

                command.CommandText = sqlQuery;
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                        ID2LogMessageFailed = reader.GetInt32(0).ToString();
                }

                reader.Close();
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

        return ID2LogMessageFailed;
    }

    private string ReloadPendantsLog(string ID1Communicator, string ID2LogMessageFailed,
        string SMTP_Server, string SMTP_User, string SMTP_Password)
    {
        SqlTransaction transaction = null;

        try
        {
            string ID1Log = string.Empty;
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(
               strConnessione))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                transaction = connection.BeginTransaction();
                command.Transaction = transaction;

                sqlQuery = "INSERT INTO [tbCommunicator_LogMessage] ";
                sqlQuery += " ([ID2LogMessageFailed],[ID2Communicator],[SendDateStart],[DestinatariTotale]";
                sqlQuery += ",[ID2FlagConsenso],[SendUser]";
                sqlQuery += ",[DestinatariInviati],[SendDateLastUpdate],[Status])";
                sqlQuery += " SELECT TOP(1) [ID1LogMessage],[ID2Communicator],[SendDateStart],[DestinatariTotale] ";
                sqlQuery += " ,[ID2FlagConsenso],[SendUser]";
                sqlQuery += " ,[DestinatariInviati],[SendDateLastUpdate],'SND'";
                sqlQuery += " FROM   tbCommunicator_LogMessage INNER JOIN tbCommunicator_TempRecipients ON ";
                sqlQuery += "  tbCommunicator_LogMessage.ID1LogMessage =tbCommunicator_TempRecipients.ID2LogMessage";
                sqlQuery += " WHERE [SendDateEnd] IS NULL";
                sqlQuery += " AND [ID2Communicator]=" + ID1Communicator;
                sqlQuery += " ORDER BY tbCommunicator_LogMessage.[ID1LogMessage]";
                sqlQuery += ";SELECT Scope_Identity()";

                command.CommandText = sqlQuery;

                ID1Log = Convert.ToInt32(command.ExecuteScalar()).ToString();

                sqlQuery = "UPDATE [tbCommunicator_LogMessage] ";
                sqlQuery += " SET [SMTP_Server]=@SMTP_Server";
                sqlQuery += " ,[SMTP_User]=@SMTP_User";
                sqlQuery += " ,[SMTP_Password]=@SMTP_Password";
                sqlQuery += " WHERE ID1LogMessage=" + ID1Log;

                command.Parameters.Add("@SMTP_Server", System.Data.SqlDbType.NVarChar);
                command.Parameters["@SMTP_Server"].Value = SMTP_Server;

                command.Parameters.Add("@SMTP_User", System.Data.SqlDbType.NVarChar);
                command.Parameters["@SMTP_User"].Value = SMTP_User;

                command.Parameters.Add("@SMTP_Password", System.Data.SqlDbType.NVarChar);
                command.Parameters["@SMTP_Password"].Value = SMTP_Password;

                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();
                command.Parameters.Clear();

                sqlQuery = "UPDATE [tbCommunicator_TempRecipients] ";
                sqlQuery += " SET [ID2LogMessage]=" + ID1Log;
                sqlQuery += " WHERE ID2LogMessage=" + ID2LogMessageFailed;

                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();

                sqlQuery = "UPDATE [tbCommunicator_LogMessage] ";
                sqlQuery += " SET [Status]='KO'";
                sqlQuery += " AND SendDateEnd=@SendDateEnd";
                sqlQuery += " WHERE ID1LogMessage=" + ID2LogMessageFailed;

                command.Parameters.Add("@SendDateEnd", System.Data.SqlDbType.SmallDateTime);
                command.Parameters["@SendDateEnd"].Value = DateTime.Now;

                transaction.Commit();
            }

            return ID1Log;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());

            if (transaction != null)
                transaction.Rollback();
            return string.Empty;
        }
    }

    protected void FindLinkButton_Click(object sender, EventArgs e)
    {
        SearchPanel.Visible = true;
        InfoPanel.Visible = false;
        FinalPanel.Visible = false;
        ImportaButton.Visible = false;
        VerificaButton.Visible = false;
        Find2LinkButton.Visible = false;
        EXCEL_EMAIL_ADDRESS.Value = string.Empty;
    }

    protected void Find2LinkButton_Click(object sender, EventArgs e)
    {
        Response.Redirect(Request.ServerVariables["HTTP_REFERER"].ToString());
    }

    protected void SendLinkButton_Click(object sender, EventArgs e)
    {
        if (!ConfermaCheckBox.Checked)
            return;

        if (IsSendingLog(Server.HtmlEncode(Request.QueryString["XRI"])))
        {
            ErrorLabel.Text = "Attenzione: l'invio della newsletter è già in corso.";
            ErrorLabel.Visible = true;
            InfoPanel.Visible = true;
            return;
        }

        TextBox SMTP_ServerTextBox = (TextBox)FormView1.FindControl("SMTP_ServerTextBox");
        TextBox SMTP_UserTextBox = (TextBox)FormView1.FindControl("SMTP_UserTextBox");
        TextBox SMTP_PasswordTextBox = (TextBox)FormView1.FindControl("SMTP_PasswordTextBox");
        TextArea NoteTextArea = (TextArea)FormView1.FindControl("NoteTextArea");

        HiddenField ID2FlagConsensoAssociatoHiddenField = (HiddenField)FormView1.FindControl("ID2FlagConsensoAssociatoHiddenField");

        string ID2Log = OpenTicketLog(Server.HtmlEncode(Request.QueryString["XRI"]),
            TotalCountHiddenField.Value,  SMTP_ServerTextBox.Text,
            SMTP_UserTextBox.Text, SMTP_PasswordTextBox.Text, NoteTextArea.Text, "IMMED");

        if (ID2Log.Length == 0)
        {
            Info1FinishLabel.Text = "Errore in fase di apertura ticket dei log dell'invio E-Mail.";
            Info1FinishLabel.Visible = true;
            FinalPanel.Visible = true;
            return;
        }

        bool Consenso = false;

        if (!WriteTempRecipients(Server.HtmlEncode(Request.QueryString["XRI"]), ID2Log, Consenso, "IMMED"))
        {
            Info1FinishLabel.Text = "Errore nell' inizializzazione tabella temporanea utenti.";
            Info1FinishLabel.Visible = true;
            FinalPanel.Visible = true;
            return;
        }

        Response.Redirect("~/Zeus/Communicator/SendMail_Web.aspx?XRI=" + ID2Log
            + "&ZIM=" + Request.QueryString["ZIM"]
            + "&Lang=" + Request.QueryString["Lang"]);
    }

    protected void InviaTaskLinkButton_Click(object sender, EventArgs e)
    {
        if (!ConfermaCheckBox.Checked)
            return;

        TextBox SMTP_ServerTextBox = (TextBox)FormView1.FindControl("SMTP_ServerTextBox");
        TextBox SMTP_UserTextBox = (TextBox)FormView1.FindControl("SMTP_UserTextBox");
        TextBox SMTP_PasswordTextBox = (TextBox)FormView1.FindControl("SMTP_PasswordTextBox");
        TextArea NoteTextArea = (TextArea)FormView1.FindControl("NoteTextArea");

        HiddenField ID2FlagConsensoAssociatoHiddenField = (HiddenField)FormView1.FindControl("ID2FlagConsensoAssociatoHiddenField");

        string ID2Log = OpenTicketLog(Server.HtmlEncode(Request.QueryString["XRI"]),
            TotalCountHiddenField.Value, SMTP_ServerTextBox.Text,
            SMTP_UserTextBox.Text, SMTP_PasswordTextBox.Text, NoteTextArea.Text, "SCHED");

        if (ID2Log.Length == 0)
        {
            Info1FinishLabel.Text = "Errore in fase di apertura ticket dei log dell'invio E-Mail.";
            return;
        }

        bool Consenso = false;
        
        if (!WriteTempRecipients(Server.HtmlEncode(Request.QueryString["XRI"]), ID2Log, Consenso, "SCHED"))
        {
            Info1FinishLabel.Text = "Errore nell' inizializzazione tabella temporanea utenti.";
            return;
        }

        Response.Redirect("~/Zeus/System/Message.aspx?Msg=123486845698303");
    }

    protected void ConfermaCustomValidator_ServerValidate(object sender, ServerValidateEventArgs args)
    {
        args.IsValid = ConfermaCheckBox.Checked;
    }

    protected void AbortLinkButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Zeus/Home/");
    }

    protected void ElencoLinkButton_Click(object sender, EventArgs e)
    {

    }

    protected void RestartLinkButton_Click(object sender, EventArgs e)
    {
        string ID1Communicator = Server.HtmlEncode(Request.QueryString["XRI"]);

        if (IsPendantsLog(ID1Communicator))
        {
            TextBox SMTP_ServerTextBox = (TextBox)FormView1.FindControl("SMTP_ServerTextBox");
            TextBox SMTP_UserTextBox = (TextBox)FormView1.FindControl("SMTP_UserTextBox");
            TextBox SMTP_PasswordTextBox = (TextBox)FormView1.FindControl("SMTP_PasswordTextBox");

            string ID2LogFailed = GetID2LogMessageFailed(ID1Communicator);

            string ID1Log = ReloadPendantsLog(ID1Communicator, ID2LogFailed,
               SMTP_ServerTextBox.Text, SMTP_UserTextBox.Text, SMTP_PasswordTextBox.Text);

            Response.Redirect("~/Zeus/Communicator/SendMail_Web.aspx?XRI=" + ID1Log);
        }
    }

    protected void StopLinkButton_Click(object sender, EventArgs e)
    {
        if (AbortTicketLog(Server.HtmlEncode(Request.QueryString["XRI"])))
        {
            SearchPanel.Visible = true;
            ErrorPanel.Visible = false;
        }
    }

    protected void SendMail_StartDateHiddenField_DataBinding(object sender, EventArgs e)
    {
        HiddenField SendMail_StartDateHiddenField = (HiddenField)sender;

        try
        {
            Label SendMail_StartDateOreTextBox = (Label)FormView1.FindControl("SendMail_StartDateOreTextBox");
            Label SendMail_StartDateMinutiTextBox = (Label)FormView1.FindControl("SendMail_StartDateMinutiTextBox");

            SendMail_StartDateOreTextBox.Text = Convert.ToDateTime(SendMail_StartDateHiddenField.Value).Hour.ToString();
            SendMail_StartDateMinutiTextBox.Text = Convert.ToDateTime(SendMail_StartDateHiddenField.Value).Minute.ToString();
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    private string Generate(int ID, int SIZE)
    {
        string chars = "ABCDEFGHILMNOPQRSTUVZ1234567890abcdefghilmnopqrstuvzxykj";
        int i;
        string r = string.Empty;
        ID += Convert.ToInt32(DateTime.Now.Minute);

        Random rnd = new Random(ID);
        for (int j = 1; j <= SIZE; j++)
        {
            i = Convert.ToInt32(rnd.Next(0, chars.Length));
            r += chars.Substring(i, 1);
        }
        return r;
    }

    private bool ClearTempPath()
    {
        try
        {
            string[] Files = System.IO.Directory.GetFiles(Server.MapPath("~/ZeusInc/TempDataImport/"));

            for (int i = 0; i < Files.Length; ++i)
            {
                System.IO.FileInfo myFileInfo = new System.IO.FileInfo(Files[i]);

                myFileInfo.IsReadOnly = false;

                if (DateTime.Compare(myFileInfo.CreationTime, DateTime.Now.AddDays(-365)) <= 0)
                    System.IO.File.Delete(Files[i]);
            }

            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }

    private string GetExtension(string FileName)
    {
        string[] split = FileName.Split('.');
        string Extension = split[split.Length - 1];
        return Extension;
    }

    protected void LoadButton_Click(object sender, EventArgs e)
    {
        CountLabel.Text = string.Empty;
        EXCEL_EMAIL_ADDRESS.Value = string.Empty;

        ClearTempPath();

        if ((FileUpload1.HasFile) && (GetExtension(FileUpload1.FileName).Equals("xls") || (GetExtension(FileUpload1.FileName).Equals("xlsx"))))
        {
            string strFile_name = System.IO.Path.GetFileNameWithoutExtension(FileUpload1.FileName);
            string strFile_extension = System.IO.Path.GetExtension(FileUpload1.FileName);
            string myFileName = strFile_name + Generate(Convert.ToInt32(DateTime.Now.Millisecond), 4) + strFile_extension;

            FileUpload1.SaveAs(Server.MapPath("~/ZeusInc/TempDataImport/") + myFileName);
            PathHiddenField.Value = "~/ZeusInc/TempDataImport/" + myFileName;
            VerificaButton.Visible = false;
            ImportaButton.Visible = true;
        }
    }

    protected void VerificaButton_Click(object sender, EventArgs e)
    {
        VerificaButton.Visible = false;
        ImportaButton.Visible = true;
    }

    protected void ImportaButton_Click(object sender, EventArgs e)
    {
        string[] CheckedColum = null;
        char[] mySplit = { ',' };

        string SqlColums = CheckedColumnsHiddenField.Value;
        CheckedColum = CheckedColumnsHiddenField.Value.Split(mySplit);
        try
        {
            SpreadsheetInfo.SetLicense("E0YT-QK22-WFBB-4JRL");
            int index = 0;
            ExcelFile FileExcel = new ExcelFile();

            if (GetExtension(PathHiddenField.Value) == "xls")
                FileExcel.LoadXls(Server.MapPath(PathHiddenField.Value));
            else
                FileExcel.LoadXlsx(Server.MapPath(PathHiddenField.Value), XlsxOptions.PreserveMakeCopy);

            foreach (ExcelRow Row in FileExcel.Worksheets[0].Rows)
            {
                try
                {
                    string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" + @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                                        @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";


                    System.Text.RegularExpressions.Regex _Regex = new System.Text.RegularExpressions.Regex(strRegex);
                    if (Row.Cells[0].Value != null)
                    {
                        if (_Regex.IsMatch(Row.Cells[0].Value.ToString()))
                        {
                            EXCEL_EMAIL_ADDRESS.Value += Row.Cells[0].Value.ToString() + ",";
                            index++;
                        }
                    }
                }
                catch (Exception p)
                {
                    ErrorLabel.Text = p.ToString();
                    ErrorLabel.Visible = true;
                }
            }

            EXCEL_EMAIL_ADDRESS.Value = EXCEL_EMAIL_ADDRESS.Value.Substring(0, EXCEL_EMAIL_ADDRESS.Value.Length - 1);

            CountLabel.Visible = true;
            CountLabel.Text = "Confermare invio  comunicazione a " + index.ToString() + " contatti.";
            TotalCountHiddenField.Value = index.ToString();

            InfoPanel.Visible = true;
            SearchPanel.Visible = false;
        }
        catch (Exception p)
        {
            ErrorLabel.Text = p.ToString();
            ErrorLabel.Visible = true;
        }
    }
}