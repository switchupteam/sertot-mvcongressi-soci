﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="Content_Lst.aspx.cs"
    Inherits="Communicator_Content_Lst"
    Theme="Zeus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <dlc:PermitRoles ID="myPermitRoles" runat="server" PAGE_TYPE="LSTCOM" SQL_TABLE="tbPZ_Communicator" />
    <asp:Panel ID="SingleRecordPanel" runat="server" Visible="false">
        <div class="LayGridTitolo1">
            <asp:Label ID="CaptionallRecordLabel" runat="server" SkinID="GridTitolo1" Text="Ultimo contenuto creato / aggiornato"></asp:Label>
        </div>
        <asp:GridView ID="SingleRecordGridView" runat="server" AllowPaging="True" AutoGenerateColumns="False"
            AllowSorting="true" PageSize="30" DataSourceID="SingleRecordSqlDataSource" OnRowDataBound="GridView1_RowDataBound">
            <Columns>
                <asp:TemplateField Visible="false">
                    <ItemTemplate>
                        <asp:HiddenField ID="AttivoArea1HiddenField" runat="server" Value='<%# Eval("AttivoArea1") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false">
                    <ItemTemplate>
                        <asp:HiddenField ID="ModelloHiddenField" runat="server" Value='<%# Eval("IsTemplate") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Titolo" HeaderText="Comunicazione" SortExpression="Titolo" />
                <asp:BoundField DataField="CatLiv3" HeaderText="Categoria" SortExpression="CatLiv3" />
                <asp:TemplateField HeaderText="Modello" SortExpression="AttivoArea1">
                    <ItemTemplate>
                        <asp:Image ID="ModelloImage" runat="server" Visible="false" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Attivo" SortExpression="AttivoArea1">
                    <ItemTemplate>
                        <asp:Image ID="AttivoImage" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>
                <asp:BoundField DataField="RecordNewDate" HeaderText="Data cr." SortExpression="RecordNewDate"
                    DataFormatString="{0:d}" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center" />
                <asp:HyperLinkField DataNavigateUrlFields="ID1Communicator,ZeusIdModulo,ZeusLangCode" DataNavigateUrlFormatString="/Zeus/Communicator/Content_Dtl.aspx?XRI={0}&ZIM={1}&Lang={2}"
                HeaderText="Dettaglio" Text="Dettaglio">
                <ControlStyle CssClass="GridView_Button1" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:HyperLinkField>
            <asp:HyperLinkField DataNavigateUrlFields="ID1Communicator,ZeusIdModulo,ZeusLangCode" DataNavigateUrlFormatString="/Zeus/Communicator/Content_Edt.aspx?XRI={0}&ZIM={1}&Lang={2}"
                HeaderText="Modifica" Text="Modifica">
                <ControlStyle CssClass="GridView_Button1" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:HyperLinkField>
            <asp:HyperLinkField DataNavigateUrlFields="ID1Communicator,ZeusIdModulo,ZeusLangCode" DataNavigateUrlFormatString="/Zeus/Communicator/AnteprimaContenuto.aspx?XRI={0}&ZIM={1}&Lang={2}"
                HeaderText="Ant. Web" Target="_blank" Text="Dettaglio">
                <ControlStyle CssClass="GridView_Button1" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:HyperLinkField>
            <asp:TemplateField HeaderText="Invia E-Mail">
                <ItemTemplate>
                    <asp:HyperLink ID="SendHyperLink" runat="server" CssClass="GridView_Button1" Text="Invia"
                        NavigateUrl='<%# String.Format("/Zeus/Communicator/SendMail.aspx?XRI={0}&ZIM={1}&Lang={2}", DataBinder.Eval(Container.DataItem, "ID1Communicator"), DataBinder.Eval(Container.DataItem, "ZeusIdModulo"),DataBinder.Eval(Container.DataItem, "ZeusLangCode")) %>'
                         Visible='<%# !Convert.ToBoolean(Eval("IsTemplate")) %>'></asp:HyperLink>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Invia SMS" Visible="false">
                <ItemTemplate>
                    <asp:HyperLink ID="SendSmSHyperLink" runat="server" CssClass="GridView_Button1" Text="Invia"
                        NavigateUrl='<%# Eval("Send","/Zeus/Communicator/SendSms.aspx{0}") %>' Visible='<%# !Convert.ToBoolean(Eval("IsTemplate")) %>'></asp:HyperLink>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Invia E-Mail da Excel">
                <ItemTemplate>
                    <asp:HyperLink ID="SendExcelHyperLink" runat="server" CssClass="GridView_Button1" Text="Invia"
                        NavigateUrl='<%# String.Format("/Zeus/Communicator/SendMail_Excel.aspx?XRI={0}&ZIM={1}&Lang={2}", DataBinder.Eval(Container.DataItem, "ID1Communicator"), DataBinder.Eval(Container.DataItem, "ZeusIdModulo"),DataBinder.Eval(Container.DataItem, "ZeusLangCode")) %>'
                        Visible='<%# !Convert.ToBoolean(Eval("IsTemplate")) %>'></asp:HyperLink>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SingleRecordSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
            SelectCommand="SELECT ID1Communicator
                                ,Titolo
                                ,CatLiv3
                                ,IsTemplate
                                ,AttivoArea1
                                ,ZeusIdModulo
                                ,Send
                                ,Status
                                ,SendDateStart
                                ,ID1Log
                                ,RecordNewDate
                                ,ZeusLangCode
                            FROM vwCommunicator_Lst_Log 
                            WHERE ID1Communicator=@ID1Communicator">
            <SelectParameters>
                <asp:QueryStringParameter Name="ID1Communicator" QueryStringField="XRI" DefaultValue="0" />
            </SelectParameters>
        </asp:SqlDataSource>
        <div class="VertSpacerMedium">
        </div>
    </asp:Panel>
    <div class="LayGridTitolo1">
        <asp:Label ID="Elenco_Label" runat="server" Text="Elenco contenuti disponibili" SkinID="GridTitolo1"></asp:Label>
    </div>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False"
        AllowSorting="true" PageSize="50" DataSourceID="NewsletterSqlDatasource" OnRowDataBound="GridView1_RowDataBound">
        <Columns>
            <asp:TemplateField Visible="false">
                <ItemTemplate>
                    <asp:HiddenField ID="AttivoArea1HiddenField" runat="server" Value='<%# Eval("AttivoArea1") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField Visible="false">
                <ItemTemplate>
                    <asp:HiddenField ID="ModelloHiddenField" runat="server" Value='<%# Eval("IsTemplate") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Titolo" HeaderText="Comunicazione" SortExpression="Titolo" />
            <asp:BoundField DataField="CatLiv3" HeaderText="Categoria" SortExpression="CatLiv3" />
            <asp:TemplateField HeaderText="Modello" SortExpression="AttivoArea1">
                <ItemTemplate>
                    <asp:Image ID="ModelloImage" runat="server" Visible="false" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Attivo" SortExpression="AttivoArea1">
                <ItemTemplate>
                    <asp:Image ID="AttivoImage" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:BoundField DataField="RecordNewDate" HeaderText="Data cr." SortExpression="RecordNewDate"
                DataFormatString="{0:d}" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center" />
            <asp:HyperLinkField DataNavigateUrlFields="ID1Communicator,ZeusIdModulo,ZeusLangCode" DataNavigateUrlFormatString="/Zeus/Communicator/Content_Dtl.aspx?XRI={0}&ZIM={1}&Lang={2}"
                HeaderText="Dettaglio" Text="Dettaglio">
                <ControlStyle CssClass="GridView_Button1" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:HyperLinkField>
            <asp:HyperLinkField DataNavigateUrlFields="ID1Communicator,ZeusIdModulo,ZeusLangCode" DataNavigateUrlFormatString="/Zeus/Communicator/Content_Edt.aspx?XRI={0}&ZIM={1}&Lang={2}"
                HeaderText="Modifica" Text="Modifica">
                <ControlStyle CssClass="GridView_Button1" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:HyperLinkField>
            <asp:HyperLinkField DataNavigateUrlFields="ID1Communicator,ZeusIdModulo,ZeusLangCode" DataNavigateUrlFormatString="/Zeus/Communicator/AnteprimaContenuto.aspx?XRI={0}&ZIM={1}&Lang={2}"
                HeaderText="Ant. Web" Target="_blank" Text="Dettaglio">
                <ControlStyle CssClass="GridView_Button1" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:HyperLinkField>
            <asp:TemplateField HeaderText="Invia E-Mail">
                <ItemTemplate>
                    <asp:HyperLink ID="SendHyperLink" runat="server" CssClass="GridView_Button1" Text="Invia"
                        NavigateUrl='<%# String.Format("/Zeus/Communicator/SendMail.aspx?XRI={0}&ZIM={1}&Lang={2}", DataBinder.Eval(Container.DataItem, "ID1Communicator"), DataBinder.Eval(Container.DataItem, "ZeusIdModulo"),DataBinder.Eval(Container.DataItem, "ZeusLangCode")) %>'
                         Visible='<%# !Convert.ToBoolean(Eval("IsTemplate")) %>'></asp:HyperLink>
                </ItemTemplate>

                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Invia SMS" Visible="false">
                <ItemTemplate>
                    <asp:HyperLink ID="SendSmSHyperLink" runat="server" CssClass="GridView_Button1" Text="Invia"
                        NavigateUrl='<%# Eval("Send","/Zeus/Communicator/SendSms.aspx{0}") %>' Visible='<%# !Convert.ToBoolean(Eval("IsTemplate")) %>'></asp:HyperLink>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Invia E-Mail da Excel">
                <ItemTemplate>
                    <asp:HyperLink ID="SendExcelHyperLink" runat="server" CssClass="GridView_Button1" Text="Invia" 
                        NavigateUrl='<%# String.Format("/Zeus/Communicator/SendMail_Excel.aspx?XRI={0}&ZIM={1}&Lang={2}", DataBinder.Eval(Container.DataItem, "ID1Communicator"), DataBinder.Eval(Container.DataItem, "ZeusIdModulo"),DataBinder.Eval(Container.DataItem, "ZeusLangCode")) %>'
                        Visible='<%# !Convert.ToBoolean(Eval("IsTemplate")) %>'></asp:HyperLink>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            Dati non presenti
            <br />
            <br />
            <br />
            <br />
            <br />
        </EmptyDataTemplate>
        <EmptyDataRowStyle BackColor="White" Width="500px" BorderColor="Red" BorderWidth="0px"
            Height="200px" />
    </asp:GridView>
    <asp:SqlDataSource ID="NewsletterSqlDatasource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SET DATEFORMAT dmy; 
                        SELECT ID1Communicator
                            ,Titolo
                            ,CatLiv3
                            ,IsTemplate
                            ,AttivoArea1
                            ,ZeusIdModulo
                            ,Send
                            ,SendDateStart
                            ,Status
                            ,ID1Log
                            ,RecordNewDate
                            ,ZeusLangCode         
                        FROM vwCommunicator_Lst_Log          
                        WHERE ZeusIdModulo = @ZeusIdModulo
                            AND ZeusLangCode=@ZeusLangCode" 
        OnSelected="NewsletterSqlDatasource_Selected">
        <SelectParameters>
            <asp:QueryStringParameter Name="ZeusIdModulo" Type="String" QueryStringField="ZIM"
                DefaultValue="COMDS" />
            <asp:QueryStringParameter Name="ZeusLangCode" Type="String" DefaultValue="ITA" QueryStringField="Lang" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
