﻿using ASP;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per Content_Dtl
/// </summary>
public partial class Content_Dtl : System.Web.UI.Page
{
    public Content_Dtl()
    {
    }

    protected void Page_Init()
    {
        // Create an instance of HtmlLink.
        HtmlLink myHtmlLink = new HtmlLink();
        myHtmlLink.Href = "/App_Themes/HtmlEditor/Communicator.css";
        myHtmlLink.Attributes.Add("rel", "stylesheet");
        myHtmlLink.Attributes.Add("type", "text/css");
        
        // Add the instance of HtmlLink to the <HEAD> section of the page.
        Page.Header.Controls.Add(myHtmlLink);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = "Dettaglio comunicazione ";
        
        delinea myDelinea = new delinea();

        if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI"))
           || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM"))
           || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang")))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        if (!ReadXML(Request.QueryString["ZIM"], GetLang()))
            Response.Redirect("~/Zeus/System/Message.aspx?1");

        ReadXML_Localization(Request.QueryString["ZIM"], GetLang());
        
        GridView GridViewVisualizzazioni = (GridView)FormView1.FindControl("VisualizzazioniGridView");

        DataTable dt = new DataTable();
        DataRow dr = dt.NewRow();
        dt.Rows.Add(dr);
        ViewState["CurrentTable"] = dt;

        GridViewVisualizzazioni.DataSource = dt;
        GridViewVisualizzazioni.DataBind();

        LogSendMessage LogSendMessage1 = (LogSendMessage)FormView1.FindControl("LogSendMessage1");
        LogSendMessage1.ID2Communicator = Server.HtmlEncode(Request.QueryString["XRI"]);

        LogUnsubscribe LogUnsubscribe1 = (LogUnsubscribe)FormView1.FindControl("LogUnsubscribe1");
        LogUnsubscribe1.ID2Communicator = Server.HtmlEncode(Request.QueryString["XRI"]);

        LogAllegati LogAllegati1 = (LogAllegati)FormView1.FindControl("LogAllegati1");
        LogAllegati1.ID2Communicator = Server.HtmlEncode(Request.QueryString["XRI"]);

        if (!Page.IsPostBack)
            SetOrariConsentiti();

    }

    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            string myXPathEach = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_Communicator.xml"));

            if (!CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_Dtl").InnerText))
                Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223599");

            Panel myPanel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPathEach);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            if (childNode.Name.IndexOf("ZMCF_") > -1)
                            {
                                myPanel = (Panel)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myPanel != null)
                                    myPanel.Visible = Convert.ToBoolean(childNode.InnerText);
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }

            SetQueryOfID2CategoriaDropDownList(mydoc.SelectSingleNode(myXPath + "ZMCD_ZIMCategoria1").InnerText
                , GetLang(), mydoc.SelectSingleNode(myXPath + "ZMCD_Cat1Liv").InnerText);
            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Communicator.xml"));

            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            if (childNode.Name.Equals("ZML_TitoloPagina_Dtl"))
                            {
                                TitleField.Value = childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                                else
                                {
                                    myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name + "Label");

                                    if (myLabel != null)
                                        myLabel.Text = childNode.InnerText;
                                }
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    private bool SetOrariConsentiti()
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        try
        {
            using (SqlConnection connection = new SqlConnection(strConnessione))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT [OraInizio],[OraFine]";
                sqlQuery += "  FROM [tbAgents_Dashboard]";
                sqlQuery += " WHERE [Agent]='COMMUNICATOR'";
                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if ((reader["OraInizio"] != DBNull.Value)
                        && ((reader["OraFine"] != DBNull.Value)))
                    {
                        Label OrariLabel = (Label)FormView1.FindControl("OrariLabel");

                        if (OrariLabel != null)
                            if (!reader["OraFine"].ToString().Equals(reader["OraInizio"].ToString()))
                                OrariLabel.Text = "Orari di spedizione consentiti: dalle "
                                    + reader["OraInizio"].ToString() + " alle "
                                    + reader["OraFine"].ToString();
                    }
                }

                reader.Close();
                return true;
            }
        }
        catch (Exception p)
        {

            Response.Write(p.ToString());
            return false;
        }
    }

    private string GetLang()
    {
        try
        {
            delinea myDelinea = new delinea();
            string Lang = "ITA";

            if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
            {
                if (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang"))
                    Lang = Server.HtmlEncode(Request.QueryString["Lang"]);
            }

            return Lang;
        }
        catch (Exception)
        {
            return "ITA";
        }
    }

    private bool CheckPermit(string AllRoles)
    {

        char[] delimiter = { ',' };
        bool IsPermit = false;

        foreach (string roles in Roles.GetRolesForUser(Membership.GetUser().UserName))
            if (roles.Equals("ZeusAdmin"))
            {
                IsPermit = true;
                break;
            }
            else if (AllRoles.Length > 0)
            {
                string[] PressRoles = AllRoles.Split(delimiter);

                for (int i = 0; i < PressRoles.Length; ++i)
                {
                    if (PressRoles[i].Equals(roles))
                    {
                        IsPermit = true;
                        break;
                    }
                }
            }

        return IsPermit;
    }

    private bool SetQueryOfID2CategoriaDropDownList(string ZeusIdModulo,
       string ZeusLangCode, string PZV_Cat1Liv)
    {
        try
        {


            SqlDataSource dsCategoria = (SqlDataSource)FormView1.FindControl("dsCategoria");
            DropDownList ID2CategoriaDropDownList = (DropDownList)FormView1.FindControl("ID2CategoriaDropDownList");
            HiddenField ID2CategoriaHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaHiddenField");
            ID2CategoriaHiddenField.Value = "0";

            switch (PZV_Cat1Liv)
            {

                case "123":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv1Liv2Liv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "Catliv1Liv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;


                case "23":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv2Liv3]";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;

                default:
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;
            }

            ID2CategoriaDropDownList.SelectedIndex = 
                ID2CategoriaDropDownList.Items.IndexOf(ID2CategoriaDropDownList.Items.FindByValue(ID2CategoriaHiddenField.Value));

            Label CategoriaLabel = (Label)FormView1.FindControl("CategoriaLabel");
            CategoriaLabel.Text = ID2CategoriaDropDownList.SelectedItem.Text;

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }//fine SetQueryOfID2CategoriaDropDownList


    protected void AttivoArea1Image_Databinding(object sender, EventArgs e)
    {
        Image AttivoArea1Image = (Image)sender;

        if (!AttivoArea1Image.ImageUrl.Equals("0"))
            AttivoArea1Image.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";
        else AttivoArea1Image.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_Off.gif";

    }//fine AttivoArea1Image_Databinding

    protected void SendMail_StartDateHiddenField_DataBinding(object sender, EventArgs e)
    {
        HiddenField SendMail_StartDateHiddenField = (HiddenField)sender;

        try
        {
            Label SendMail_StartDateOreTextBox = (Label)FormView1.FindControl("SendMail_StartDateOreTextBox");
            Label SendMail_StartDateMinutiTextBox = (Label)FormView1.FindControl("SendMail_StartDateMinutiTextBox");

            SendMail_StartDateOreTextBox.Text = Convert.ToDateTime(SendMail_StartDateHiddenField.Value).Hour.ToString();
            SendMail_StartDateMinutiTextBox.Text = Convert.ToDateTime(SendMail_StartDateHiddenField.Value).Minute.ToString();
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }//fine SendMail_StartDateHiddenField_DataBinding

    protected void CommunicatorSqlDataSource_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.Exception != null)
            Response.Write("~/Zeus/System/Message.aspx?SelectErr");
    }//fine CommunicatorSqlDataSource_Selected


    protected void ModificaLinkButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Zeus/Communicator/Content_Edt.aspx?XRI=" + Server.HtmlEncode(Request.QueryString["XRI"]) +
                                "&ZIM=" + Server.HtmlEncode(Request.QueryString["ZIM"]) + "&Lang=" + Server.HtmlEncode(Request.QueryString["Lang"]));
    }//fine ModificaLinkButton_Click

    protected void VisualizzazioniButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Zeus/Communicator/DettaglioTrackingRead.aspx?XRI=" + Request.QueryString["XRI"].ToString());
    }

    protected void DownloadVisualizzazioneButton_Click(object sender, EventArgs e)
    {

        string sqlQuery = string.Empty;
        string ZeusId = string.Empty;

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        try
        {
            using (SqlConnection connection = new SqlConnection(strConnessione))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                sqlQuery = @"SELECT ZeusId
                            FROM tbCommunicator
                            WHERE ID1Communicator = @ID1Allegato
                            ";

                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID1Allegato", System.Data.SqlDbType.Int);
                command.Parameters["@ID1Allegato"].Value = Convert.ToInt32(Request.QueryString["xri"]);

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (reader["ZeusId"] != DBNull.Value)
                        ZeusId = reader["ZeusId"].ToString();
                }
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

        try
        {
            FileInfo file = new FileInfo(MapPath("~/ZeusInc/TrackingRead/") + Request.QueryString["XRI"] + "_Lettura_" + ZeusId + ".xml");

            if (file.Exists)
            {
                Response.ClearContent();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                Response.AddHeader("Content-Length", file.Length.ToString());
                Response.ContentType = "text/xml";
                Response.TransmitFile(file.FullName);
                Response.End();
            }
        }
        catch (Exception p)
        {
            //Response.Write(p.ToString());
            //Response.Redirect("~/System/Message.aspx?DwnErr");
        }
    }
    protected void VisualizzazioniGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Apri")
        {
            Response.Redirect("/Zeus/Communicator/DettaglioTrackingRead.aspx?XRI=" + Request.QueryString["XRI"].ToString());
        }

        if (e.CommandName == "Download")
        {
            string sqlQuery = string.Empty;
            string ZeusId = string.Empty;
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            try
            {
                using (SqlConnection connection = new SqlConnection(strConnessione))
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    sqlQuery = @"SELECT ZeusId
                            FROM tbCommunicator
                            WHERE ID1Communicator = @ID1Allegato
                            ";

                    command.CommandText = sqlQuery;
                    command.Parameters.Add("@ID1Allegato", System.Data.SqlDbType.Int);
                    command.Parameters["@ID1Allegato"].Value = Convert.ToInt32(Request.QueryString["xri"]);

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        if (reader["ZeusId"] != DBNull.Value)
                            ZeusId = reader["ZeusId"].ToString();
                    }
                }
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
            }

            try
            {
                FileInfo file = new FileInfo(MapPath("~/ZeusInc/TrackingRead/") + Request.QueryString["XRI"] + "_Lettura_" + ZeusId + ".xml");

                if (file.Exists)
                {
                    Response.ClearContent();
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                    Response.AddHeader("Content-Length", file.Length.ToString());
                    Response.ContentType = "text/xml";
                    Response.TransmitFile(file.FullName);
                    Response.End();
                }
            }
            catch (Exception p)
            {
                //Response.Write(p.ToString());
                //Response.Redirect("~/System/Message.aspx?DwnErr");
            }
        }
    }
}