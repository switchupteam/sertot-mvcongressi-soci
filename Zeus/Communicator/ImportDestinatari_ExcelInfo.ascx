﻿<%@ Control Language="C#" ClassName="ImportDestinatari_ExcelInfo" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.OleDb" %>

<script runat="server">

    static string[] OurColumn = { 
"EMAIL"
};

    private string myPath = string.Empty;


    public string Path
    {
        get { return myPath; }
        set { myPath = value; }
    }



    public string BindTheData()
    {

        string myColum = string.Empty;
        
        if (myPath.Length <= 0)
            return myColum;
        
        try
        {
            string strConn = "Provider=Microsoft.Jet.OLEDB.4.0;";
            strConn += "Data Source=" + Server.MapPath(myPath) + ";";
            strConn += "Extended Properties=\"Excel 8.0;\"";

            string sql = "SELECT TOP 1 * FROM [IMPORT$]";


            using (OleDbConnection conn = new OleDbConnection(strConn))
            {
                OleDbCommand cmd = new OleDbCommand(sql, conn);
                conn.Open();
                OleDbDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                DataTable schemaTable = rd.GetSchemaTable();



                if (schemaTable.Rows.Count <= 0)
                    return string.Empty;

                Label myLabel = null;
                
                this.Controls.Clear();
                this.Controls.Add(new LiteralControl("<table>"));
                this.Controls.Add(new LiteralControl("<tr>"));
                this.Controls.Add(new LiteralControl("<td colspan=\"2\">"));
                myLabel = new Label();
                myLabel.Text = "Dati trovati per l'invio comunicazione";
                myLabel.CssClass = "LabelSkin_GridTitolo1";
                this.Controls.Add(myLabel);
                this.Controls.Add(new LiteralControl("</td>"));
                this.Controls.Add(new LiteralControl("</tr>"));

                bool[] myControl = new bool[OurColumn.Length];

                for (int i = 0; i < myControl.Length; ++i)
                    myControl[i] = false;

                foreach (DataRow myField in schemaTable.Rows)
                    for (int i = 0; i < OurColumn.Length; ++i)
                        if (myField["ColumnName"].ToString().ToUpper().Equals(OurColumn[i].ToUpper()))
                            myControl[i] = true;



                
                
                
                for (int i = 0; i < OurColumn.Length; ++i)
                {
                    this.Controls.Add(new LiteralControl("<tr>"));
                    this.Controls.Add(new LiteralControl("<td>"));
                    myLabel = new Label();
                    myLabel.Text = OurColumn[i].ToUpper();
                    myLabel.CssClass = "LabelSkin_CorpoTesto";
                    this.Controls.Add(myLabel);
                    this.Controls.Add(new LiteralControl("</td>"));
                    this.Controls.Add(new LiteralControl("<td>"));

                    if (myControl[i])
                    {
                        this.Controls.Add(new LiteralControl("<img src=\"../SiteImg/Ico1_StatusOk.gif\" />"));
                        myColum +=OurColumn[i];

                        if (i + 2 < OurColumn.Length)
                            myColum += ",";
                    }
                    else
                        this.Controls.Add(new LiteralControl("<img src=\"../SiteImg/Ico1_StatusKo.gif\" />"));

                    this.Controls.Add(new LiteralControl("</td>"));
                    this.Controls.Add(new LiteralControl("</tr>"));
                }
                this.Controls.Add(new LiteralControl("</table>"));

                rd.Close();
                conn.Close();

            }//fine using

        }
        catch (Exception p)
        {
            InfoLabel.Text= p.ToString();
            
        }

        return myColum;

    }//fine BindTheData
    
    
</script>

<asp:Label ID="InfoLabel" runat="server" SkinID="Validazione01" ></asp:Label>