﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="Content_Dtl.aspx.cs" Inherits="Content_Dtl"
    Theme="Zeus" EnableEventValidation="true" %>

<%--<%@ Register Src="~/Communicator/Content.ascx" TagName="Content" TagPrefix="uc1" %>--%>
<%@ Register Src="LogSendMessage.ascx" TagName="LogSendMessage" TagPrefix="uc2" %>
<%@ Register Src="LogUnsubscribe.ascx" TagName="LogUnsubscribe" TagPrefix="uc3" %>
<%@ Register Src="LogAllegati.ascx" TagName="LogAllegati" TagPrefix="uc4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">

    <script type="text/javascript" language="JavaScript">
        function resizeIframeToFitContent(iframe) {
            // This function resizes an IFrame object
            // to fit its content.
            // The IFrame tag must have a unique ID attribute.


            //    iframe.height = document.frames[iframe.id]
            //                    .document.body.scrollHeight;

            document.getElementById(iframe.id).style.height =
            document.getElementById(iframe.id).contentWindow.document.body.scrollHeight + "px";

        }

    </script>

    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <dlc:PermitRoles ID="myPermitRoles" runat="server" PAGE_TYPE="DTLCOM" SQL_TABLE="tbPZ_Communicator"
        EDT_ID_BUTTON="ModificaButton" EMAIL_ID_BUTTON="SendHyperLink" />
    <asp:FormView ID="FormView1" runat="server" DefaultMode="ReadOnly" DataSourceID="CommunicatorSqlDataSource"
        Width="100%">
        <ItemTemplate>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="Label8" runat="server">Dettaglio</asp:Label>
                </div>
                <table>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label1" SkinID="FieldDescription" runat="server">Titolo</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:Label ID="TitoloLabel" runat="server" Text='<%# Eval("Titolo") %>' SkinID="FieldValue" />
                        </td>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label2" SkinID="FieldDescription" runat="server"> Modello</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:CheckBox ID="IsTemplateCheckBox" runat="server" Checked='<%# Eval("IsTemplate") %>'
                                Enabled="false" />
                        </td>
                    </tr>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label3" SkinID="FieldDescription" runat="server">Categoria</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:Label ID="CategoriaLabel" runat="server" SkinID="FieldValue"></asp:Label>
                            <asp:DropDownList ID="ID2CategoriaDropDownList" runat="server" Visible="false" AppendDataBoundItems="True">
                                <asp:ListItem Text="> Nessuno" Value="0" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:HiddenField ID="ID2CategoriaHiddenField" runat="server" Value='<%# Eval("ID2Categoria") %>' />
                            <asp:SqlDataSource ID="dsCategoria" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"></asp:SqlDataSource>
                        </td>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label5" SkinID="FieldDescription" runat="server">Consenso associato</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:Label ID="DescrizioneLabel" runat="server" Text='<%# Eval("Descrizione") %>'
                                SkinID="FieldValue" />
                        </td>
                    </tr>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label6" SkinID="FieldDescription" runat="server">Visibilità</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:CheckBox ID="PW_Area1CheckBox" runat="server" Checked='<%# Eval("PW_Area1") %>'
                                Enabled="false" />
                            <asp:Label ID="Label7" SkinID="FieldValue" runat="server">Attiva dalla data</asp:Label>
                            <asp:Label ID="PWI_Area1Label" runat="server" SkinID="FieldValue" Text='<%# Eval("PWI_Area1","{0:d}") %>' />
                            <asp:Label ID="Label9" SkinID="FieldValue" runat="server">alla data</asp:Label>
                            <asp:Label ID="PWF_Area1Label" runat="server" SkinID="FieldValue" Text='<%# Eval("PWF_Area1","{0:d}") %>' />
                        </td>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label4" SkinID="FieldDescription" runat="server">Attivo</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:Image ID="AttivoArea1Image" runat="server" ImageUrl='<%# Eval("AttivoArea1") %>'
                                OnDataBinding="AttivoArea1Image_Databinding" />
                        </td>
                    </tr>
                </table>
            </div>
            <div class="BlockBox">
                <asp:Panel ID="EmailMittentePanel" runat="server">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="Label10" runat="server">Informazioni messaggio</asp:Label>
                    </div>
                    <table>

                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label11" SkinID="FieldDescription" runat="server">Oggetto</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:Label ID="Sender_OggettoLabel" runat="server" Text='<%# Eval("Sender_Oggetto") %>'
                                    SkinID="FieldValue" />
                            </td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label12" SkinID="FieldDescription" runat="server">Mittente</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:Label ID="Sender_NomeLabel" runat="server" Text='<%# Eval("Sender_Nome") %>'
                                    SkinID="FieldValue" />
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label29" runat="server" SkinID="FieldDescription">Titolo versione web </asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:Label ID="TitoloWebTextBox" runat="server" Text='<%# Eval("TitoloWeb") %>' SkinID="FieldValue"></asp:Label>
                            </td>
                        </tr>
                </asp:Panel>
                <asp:Panel ID="ZMCF_SMSMittente" runat="server">
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label15" SkinID="FieldDescription" runat="server">Numero di telefono</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:Label ID="Sender_PhoneNumberLabel" runat="server" Text='<%# Eval("Sender_PhoneNumber") %>'
                                SkinID="FieldValue" />
                        </td>
                    </tr>
                </asp:Panel>
                </table>
            </div>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="Label19" runat="server">Server SMTP</asp:Label>
                </div>
                <table>
                     <tr>
                                          <td class="BlockBoxDescription">
                                <asp:Label ID="Label13" SkinID="FieldDescription" runat="server">E-Mail</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:Label ID="Sender_EmailLabel" runat="server" Text='<%# Eval("Sender_Email") %>'
                                    SkinID="FieldValue" />
                            </td>
                          </tr>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label20" runat="server" SkinID="FieldDescription">SMTP Server </asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:Label ID="SMTP_ServerTextBox" SkinID="FieldValue" runat="server" Text='<%# Eval("SMTP_Server") %>' />
                        </td>
                    </tr>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label21" runat="server" SkinID="FieldDescription">SMTP User </asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:Label SkinID="FieldValue" ID="SMTP_UserTextBox" runat="server" Text='<%# Eval("SMTP_User") %>' />
                        </td>
                    </tr>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label26" runat="server" SkinID="FieldDescription">Data e ora inizio spedizione </asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:HiddenField ID="SendMail_StartDateHiddenField" runat="server" Value='<%# Eval("SendMail_StartDate") %>'
                                OnDataBinding="SendMail_StartDateHiddenField_DataBinding" />
                            <asp:Label SkinID="FieldValue" ID="SendMail_StartDateTextBox" runat="server" Text='<%# Eval("SendMail_StartDate","{0:d}") %>' />
                            <asp:Label ID="Label27" runat="server" Text="ore" SkinID="FieldValue"></asp:Label>
                            <asp:Label SkinID="FieldValue" ID="SendMail_StartDateOreTextBox" runat="server"></asp:Label>
                            <asp:Label ID="Label35" runat="server" Text="minuti" SkinID="FieldValue"></asp:Label>
                            <asp:Label SkinID="FieldValue" ID="SendMail_StartDateMinutiTextBox" runat="server"></asp:Label>
                            <br />
                            <asp:Label ID="OrariLabel" runat="server" Text="Orari di spedizione consentiti: sempre"
                                SkinID="Note"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="Label14" runat="server">Anteprima contenuto</asp:Label>
                </div>
                <table width="100%">
                    <tr>
                        <td>
                            <div align="center">
                                <iframe id="AnteprimaContenuto" name="AnteprimaContenuto" src="AnteprimaContenuto.aspx?XRI=<%# Eval("ID1Communicator") %>&ZIM=<%# Eval("ZeusIdModulo") %>"
                                    scrolling="no" marginwidth="0" marginheight="0" frameborder="0" vspace="0" hspace="0"
                                    style="overflow: visible; width: 100%; display: block;" onload="resizeIframeToFitContent(this)"></iframe>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="Label18" runat="server">Tracking visualizzazioni comunicazioni e allegati</asp:Label>
                </div>
                <table width="100%">
                    <tr>
                        <td>
                            <uc4:LogAllegati ID="LogAllegati1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 80px;">
                            <asp:GridView ID="VisualizzazioniGridView" runat="server" OnRowCommand="VisualizzazioniGridView_RowCommand">
                                <Columns>
                                    <asp:TemplateField HeaderText="Visualizzazione comunicazioni">
                                        <ItemTemplate>
                                            <asp:Label Text="Registro apertura messaggi E-Mail" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:ButtonField HeaderText="Elenco utenti" Text="Visualizza" HeaderStyle-Width="200" CommandName="Apri">
                                        <ControlStyle CssClass="GridView_Button1" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:ButtonField>
                                    <asp:ButtonField HeaderText="File XML" Text="Download" HeaderStyle-Width="200"
                                        CommandName="Download">
                                        <ControlStyle CssClass="GridView_Button1" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:ButtonField>
                                </Columns>
                                <EmptyDataTemplate>
                                    Dati non presenti
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>            
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="Label16" runat="server">            Registro invii comunicazioni</asp:Label>
                </div>
                <table width="100%">
                    <tr>
                        <td>
                            <uc2:LogSendMessage ID="LogSendMessage1" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
            <asp:Panel ID="RimozioniPanel" runat="server" Visible="false">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="Label17" runat="server">Registro rimozioni utente associate a questa comunicazione</asp:Label>
                    </div>
                    <table width="100%">
                        <tr>
                            <td>
                                <uc3:LogUnsubscribe ID="LogUnsubscribe1" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <div align="center">
                <asp:LinkButton ID="ModificaLinkButton" runat="server" Text="Modifica" SkinID="ZSSM_Button01"
                    OnClick="ModificaLinkButton_Click"></asp:LinkButton>
                <asp:LinkButton ID="SendHyperLink" runat="server" SkinID="ZSSM_Button01" Text="Invia / schedula invio"
                    PostBackUrl='<%# String.Format("/Zeus/Communicator/SendMail.aspx?XRI={0}&ZIM={1}&Lang={2}", DataBinder.Eval(Container.DataItem, "ID1Communicator"), DataBinder.Eval(Container.DataItem, "ZeusIdModulo"),DataBinder.Eval(Container.DataItem, "ZeusLangCode"))%>'
                    Visible='<%# !Convert.ToBoolean(Eval("IsTemplate")) %>'></asp:LinkButton>
            </div>
            <div class="VertSpacerBig">
            </div>
            <asp:HiddenField ID="zdtRecordNewUser" runat="server" Value='<%# Eval("RecordNewUser") %>' />
            <asp:HiddenField ID="zdtRecordNewDate" runat="server" Value='<%# Eval("RecordNewDate") %>' />
            <asp:HiddenField ID="zdtRecordEdtUser" runat="server" Value='<%# Eval("RecordEdtUser") %>' />
            <asp:HiddenField ID="zdtRecordEdtDate" runat="server" Value='<%# Eval("RecordEdtDate") %>' />
        </ItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="CommunicatorSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SELECT [ID1Communicator], [ZeusIdModulo], ZeusLangCode, [Titolo], [IsTemplate], [CatLiv2Liv3], [AttivoArea1],[Send], [Descrizione],
         [PW_Area1], [PWI_Area1], [PWF_Area1], [Sender_Oggetto], [Sender_Nome],[Sender_PhoneNumber], 
         [Sender_Email], RecordNewUser,RecordNewDate,RecordEdtUser,RecordEdtDate 
         ,[ID2Categoria],SMTP_Server,SMTP_User , SendMail_StartDate,TitoloWeb
         FROM [vwCommunicator_Dtl] WHERE ([ID1Communicator] = @ID1Communicator)"
        OnSelected="CommunicatorSqlDataSource_Selected">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="0" Name="ID1Communicator" QueryStringField="XRI"
                Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <dlc:zeusdatatracking ID="Zeusdatatracking1" runat="server" />
</asp:Content>
