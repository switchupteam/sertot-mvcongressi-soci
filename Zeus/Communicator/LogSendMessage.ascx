﻿<%@ Control Language="C#" ClassName="LogSendMessage" CodeFile="LogSendMessage.ascx.cs"
    Inherits="Communicator_LogSendMessage"%>

<asp:GridView ID="GridView1" runat="server" AllowSorting="false" AutoGenerateColumns="False"
    Width="100%" CellPadding="0" OnRowDataBound="GridView1_RowDataBound">
    <Columns>
        <asp:BoundField DataField="SendDateStart" HeaderText="Invio / consegna" SortExpression="SendDateStart"
            ItemStyle-HorizontalAlign="Center">
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
        </asp:BoundField>
        <asp:BoundField DataField="SendDateEnd" HeaderText="Fine spediz." SortExpression="SendDateEnd"
            ItemStyle-HorizontalAlign="Center">
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
        </asp:BoundField>
        <asp:TemplateField HeaderText="Stato invio" SortExpression="Status">
            <ItemTemplate>
                <asp:Label ID="StatusLabel" runat="server" Text='<%# SwitchStateToHuman(Eval("Status").ToString()) %>'></asp:Label>
            </ItemTemplate>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
        </asp:TemplateField>
        <asp:BoundField DataField="SendType" HeaderText="Modalità" SortExpression="SendType"
            ItemStyle-HorizontalAlign="Center">
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
        </asp:BoundField>
        <asp:BoundField DataField="Utente" HeaderText="Utente" ReadOnly="True" SortExpression="Utente" />
        <asp:BoundField DataField="DestinatariTotale" HeaderText="Destinatari" SortExpression="DestinatariTotale"
            ItemStyle-HorizontalAlign="Right">
            <ItemStyle HorizontalAlign="Right"></ItemStyle>
        </asp:BoundField>
        <asp:BoundField DataField="DestinatariInviati" HeaderText="Inviati" SortExpression="DestinatariInviati"
            ItemStyle-HorizontalAlign="Right">
            <ItemStyle HorizontalAlign="Right"></ItemStyle>
        </asp:BoundField>
        <asp:BoundField DataField="Note" HeaderText="Note" />
        <asp:BoundField DataField="Descrizione" HeaderText="Consenso associato" SortExpression="Descrizione" />
        <asp:BoundField DataField="SMTP_Server" HeaderText="SMTP Server" SortExpression="SMTP_Server" />
        <asp:BoundField DataField="ID1LogMessage" HeaderText="Log" SortExpression="ID1LogMessage"
            ItemStyle-HorizontalAlign="Right">
            <ItemStyle HorizontalAlign="Right"></ItemStyle>
        </asp:BoundField>
        <asp:TemplateField HeaderText="Tracking">
            <ItemTemplate>
                <asp:Image ID="TrackingImage" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_EyeOff.png" />
            </ItemTemplate>
            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
        </asp:TemplateField>
        <asp:TemplateField Visible="false">
                    <ItemTemplate>
                        <asp:HiddenField ID="TrackingHiddenField" runat="server" Value='<%# Eval("Tracking") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:LinkButton ID="AnnullaLinkButton" runat="server" Text='<%# Eval("SendType") %>'
                    CssClass='<%# Eval("Status") %>' ToolTip='<%# Eval("ID1LogMessage") %>'
                    OnDataBinding="AnnullaLinkButton_DataBinding" OnClick="AnnullaLinkButton_Click"></asp:LinkButton>
            </ItemTemplate>
            <ItemStyle HorizontalAlign="Center"></ItemStyle>
        </asp:TemplateField>
        <asp:HyperLinkField DataNavigateUrlFields="ID1LogMessage" DataNavigateUrlFormatString="/Zeus/Communicator/DettaglioMailSent.aspx?XRI={0}"
            HeaderText="Report" Text="Visualizza">
            <ControlStyle CssClass="GridView_Button1" />
            <ItemStyle HorizontalAlign="Center" />
        </asp:HyperLinkField>
    </Columns>
    <EmptyDataTemplate>
        Dati non presenti
    </EmptyDataTemplate>
</asp:GridView>
<asp:SqlDataSource ID="LogSendMessageSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"></asp:SqlDataSource>
