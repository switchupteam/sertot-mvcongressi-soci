﻿using System;
using System.Collections;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Xml.Linq;

/// <summary>
/// Descrizione di riepilogo per SendMail_Web
/// </summary>
public partial class Communicator_SendMail_Web : System.Web.UI.Page
{	
    protected void Page_Load(object sender, EventArgs e)
    {
        if(SendMailAndWriteLog())
            Response.Redirect("~/Zeus/System/Message.aspx?Msg=1234868456784588");
        else Response.Redirect("~/Zeus/System/Message.aspx?Msg=1234868456781234");
    }    

    private string GetLang()
    {
        try
        {
            delinea myDelinea = new delinea();
            string Lang = "ITA";

            if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
            {
                if (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang"))
                    Lang = Request.QueryString["Lang"];
            }

            return Lang;
        }
        catch (Exception)
        {
            return "ITA";
        }
    }

    private bool CloseTicketLog(string ID1LogMessage)
    {
        try
        {
            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(
               System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "UPDATE [tbCommunicator_LogMessage] ";
                sqlQuery += " SET [SendDateEnd]=@SendDateEnd";
                sqlQuery += " ,Status='OK'";
                sqlQuery += " WHERE ID1LogMessage=" + ID1LogMessage;

                command.Parameters.Add("@SendDateEnd", System.Data.SqlDbType.SmallDateTime);
                command.Parameters["@SendDateEnd"].Value = DateTime.Now;
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();
            }

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    private bool ErrorTicketLog(string ID1LogMessage)
    {
        try
        {
            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(
               System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "UPDATE [tbCommunicator_LogMessage] ";
                sqlQuery += " SET [SendDateEnd]=@SendDateEnd";
                sqlQuery += " ,Status='ERR'";
                sqlQuery += " WHERE ID1LogMessage=" + ID1LogMessage;

                command.Parameters.Add("@SendDateEnd", System.Data.SqlDbType.SmallDateTime);
                command.Parameters["@SendDateEnd"].Value = DateTime.Now;
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();
            }

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    private bool ClearTempRecipientsAndUpdateLog(string UserId,
        string ID1LogMessage, string DestinatariInviati)
    {
        SqlTransaction transaction = null;
        SqlDataReader reader = null;

        try
        {
            string ID1Log = string.Empty;
            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(
               System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                transaction = connection.BeginTransaction();
                command.Transaction = transaction;

                sqlQuery = "DELETE FROM [tbCommunicator_TempRecipients] ";

                if (UserId.Length > 0)
                {
                    sqlQuery += " WHERE Destinatario_UserId=@Destinatario_UserId";
                    sqlQuery += " AND [SendType]='IMMED'";

                    command.Parameters.Add("@Destinatario_UserId", System.Data.SqlDbType.UniqueIdentifier);
                    command.Parameters["@Destinatario_UserId"].Value = new Guid(UserId);
                }
                else 
                {
                    sqlQuery += " WHERE Destinatario_UserId IS NULL";
                    sqlQuery += " AND [SendType]='IMMED'";
                }

                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();
                command.Parameters.Clear();

                sqlQuery = "UPDATE [tbCommunicator_LogMessage] ";
                sqlQuery += "SET [DestinatariInviati]=" + DestinatariInviati;
                sqlQuery += ",[SendDateLastUpdate]=@SendDateLastUpdate";
                sqlQuery += " WHERE ID1LogMessage=" + ID1LogMessage;

                command.Parameters.Add("@SendDateLastUpdate", System.Data.SqlDbType.SmallDateTime);
                command.Parameters["@SendDateLastUpdate"].Value = DateTime.Now;
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();
                transaction.Commit();
            }

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());

            if (transaction != null)
            {
                if (reader != null)
                    reader.Close();

                transaction.Rollback();
            }

            return false;
        }
    }

    string RemoveBetween(string s, string begin, string end)
    {
        System.Text.RegularExpressions.Regex regex =
            new System.Text.RegularExpressions.Regex(string.Format("\\{0}.*?\\{1}", begin, end));
        return regex.Replace(s, string.Empty);
    }

    private string ReplaceOurTagFromProfiliPersonali(string Contenuto, string Cognome, string Nome, string Sesso)
    {
        try
        {
            Contenuto = Contenuto.Replace("(((COGNOME)))", Cognome);
            Contenuto = Contenuto.Replace("(((NOME)))", Nome);

            switch (Sesso.ToUpper())
            {

                case "M":
                    Contenuto = RemoveBetween(Contenuto, "(((#W#", ")))");
                    Contenuto = RemoveBetween(Contenuto, "(((#F#", ")))");
                    Contenuto = RemoveBetween(Contenuto, "(((#X#", ")))");
                    Contenuto = Contenuto.Replace("(((#M#", string.Empty);
                    Contenuto = Contenuto.Replace(")))", string.Empty);
                    break;

                case "F":
                    Contenuto = RemoveBetween(Contenuto, "(((#W#", ")))");
                    Contenuto = RemoveBetween(Contenuto, "(((#M#", ")))");
                    Contenuto = RemoveBetween(Contenuto, "(((#X#", ")))");
                    Contenuto = Contenuto.Replace("(((#F#", string.Empty);
                    Contenuto = Contenuto.Replace(")))", string.Empty);
                    break;

                default:
                    Contenuto = RemoveBetween(Contenuto, "(((#W#", ")))");
                    Contenuto = RemoveBetween(Contenuto, "(((#F#", ")))");
                    Contenuto = RemoveBetween(Contenuto, "(((#M#", ")))");
                    Contenuto = Contenuto.Replace("(((#X#", string.Empty);
                    Contenuto = Contenuto.Replace(")))", string.Empty);
                    break;
            }

            Contenuto = Contenuto.Replace("(())", string.Empty);
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
        return Contenuto;
    }
  
    private int GetInviati(string ID1LogMessage)
    {
        int Inviati = 0;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString))
        {
            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT [DestinatariInviati]";
                sqlQuery += " FROM [tbCommunicator_LogMessage]";
                sqlQuery += " WHERE [ID1LogMessage] = " + ID1LogMessage;
                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();

                if (!reader.HasRows)
                {
                    reader.Close();
                    return 0;
                }

                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                        Inviati = reader.GetInt32(0);
                }

                reader.Close();
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
            }
        }

        return Inviati;
    }
        
    public class Mail
    {
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public string EMail { get; set; }
        public string EsitoInvio { get; set; }
        public DateTime DataInvio { get; set; }
    }

    public bool SendMailAndWriteLog()
    {
        delinea myDelinea = new delinea();

        if (!myDelinea.AntiSQLInjectionRight(Request.QueryString, "XRI"))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        string ID1LogMessage = Server.HtmlEncode(Request.QueryString["XRI"]);
        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString))
        {
            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT ";
                sqlQuery += " Destinatario_UserId,Destinatario_Email";
                sqlQuery += ",Destinatario_Cognome,Destinatario_Nome,Destinatario_Sesso";
                sqlQuery += ",Titolo,Sender_Oggetto,Sender_Nome,Sender_Email";
                sqlQuery += ",SMTP_Server,SMTP_User,SMTP_Password,Contenuto1";
                sqlQuery += " FROM tbCommunicator_TempRecipients";
                sqlQuery += " WHERE [ID2LogMessage]=" + ID1LogMessage;
                sqlQuery += " AND SendType='IMMED' ";

                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();

                ArrayList myArrayList = new ArrayList();
                while (reader.Read())
                {
                    string[] myData = new string[13];

                    if (reader["Destinatario_UserId"] != DBNull.Value)
                        myData[0] = reader["Destinatario_UserId"].ToString();
                    else myData[0] = string.Empty;

                    if (reader["Destinatario_Email"] != DBNull.Value)
                        myData[1] = reader["Destinatario_Email"].ToString();
                    else myData[1] = string.Empty;

                    if (reader["Destinatario_Cognome"] != DBNull.Value)
                        myData[2] = reader["Destinatario_Cognome"].ToString();
                    else myData[2] = string.Empty;

                    if (reader["Destinatario_Nome"] != DBNull.Value)
                        myData[3] = reader["Destinatario_Nome"].ToString();
                    else myData[3] = string.Empty;

                    if (reader["Destinatario_Sesso"] != DBNull.Value)
                        myData[4] = reader["Destinatario_Sesso"].ToString();
                    else myData[4] = string.Empty;

                    if (reader["Titolo"] != DBNull.Value)
                        myData[5] = reader["Titolo"].ToString();
                    else myData[5] = string.Empty;

                    if (reader["Sender_Oggetto"] != DBNull.Value)
                        myData[6] = reader["Sender_Oggetto"].ToString();
                    else myData[6] = string.Empty;

                    if (reader["Sender_Nome"] != DBNull.Value)
                        myData[7] = reader["Sender_Nome"].ToString();
                    else myData[7] = string.Empty;

                    if (reader["Sender_Email"] != DBNull.Value)
                        myData[8] = reader["Sender_Email"].ToString();
                    else myData[8] = string.Empty;

                    if (reader["SMTP_Server"] != DBNull.Value)
                        myData[9] = reader["SMTP_Server"].ToString();
                    else myData[9] = string.Empty;

                    if (reader["SMTP_User"] != DBNull.Value)
                        myData[10] = reader["SMTP_User"].ToString();
                    else myData[10] = string.Empty;

                    if (reader["SMTP_Password"] != DBNull.Value)
                        myData[11] = reader["SMTP_Password"].ToString();
                    else myData[11] = string.Empty;

                    if (reader["Contenuto1"] != DBNull.Value)
                        myData[12] = reader["Contenuto1"].ToString();
                    else myData[12] = string.Empty;

                    myArrayList.Add(myData);
                }

                reader.Close();

                string[][] TempRecipients = (string[][])myArrayList.ToArray(typeof(string[]));

                int Destinatario_UserId = 0;
                int Destinatario_Email = 1;
                int Destinatario_Cognome = 2;
                int Destinatario_Nome = 3;
                int Destinatario_Sesso = 4;
                int Titolo = 5;
                int Sender_Oggetto = 6;
                int Sender_Nome = 7;
                int Sender_Email = 8;
                int SMTP_Server = 9;
                int SMTP_User = 10;
                int SMTP_Password = 11;
                int Contenuto1 = 12;
                System.Xml.Linq.XElement MyXml = null;
                
                int Inviati = GetInviati(ID1LogMessage);

                try
                {
                    MyXml = XElement.Load(MapPath("~/ZeusInc/TrackingMailSent/Report_") + ID1LogMessage + ".xml");
                }
                catch (Exception)
                {
                    MyXml = new XElement("MailSent");
                    MyXml.Save(MapPath("~/ZeusInc/TrackingMailSent/Report_") + ID1LogMessage + ".xml");
                }
                                
                if (Inviati < TempRecipients.Length)
                {
                    for (int i = 0; i < TempRecipients.Length; ++i)
                    {
                        string Contenuto = ReplaceOurTagFromProfiliPersonali(TempRecipients[i][Contenuto1], 
                            TempRecipients[i][Destinatario_Cognome], TempRecipients[i][Destinatario_Nome]
                            , TempRecipients[i][Destinatario_Sesso]);

                        string Oggetto = ReplaceOurTagFromProfiliPersonali(
                            TempRecipients[i][Sender_Oggetto], TempRecipients[i][Destinatario_Cognome]
                            , TempRecipients[i][Destinatario_Nome], TempRecipients[i][Destinatario_Sesso]);

                        if (SendMail(TempRecipients[i][Sender_Email]
                            , TempRecipients[i][Destinatario_Email]
                            , Contenuto
                            , TempRecipients[i][Sender_Nome]
                            , Oggetto
                            , string.Empty
                            , TempRecipients[i][SMTP_Server]
                            , TempRecipients[i][SMTP_User]
                            , TempRecipients[i][SMTP_Password]
                            ))
                        {
                            ++Inviati;
                            ClearTempRecipientsAndUpdateLog(TempRecipients[i][Destinatario_UserId], ID1LogMessage, Inviati.ToString());

                            if (TempRecipients[i][Destinatario_UserId] != string.Empty)
                            {
                                var M = from a in MyXml.Elements("Mail")
                                        where a.Element("UserId").Value.Equals(TempRecipients[i][Destinatario_UserId])
                                        select a;

                                if (M.Count() > 0)
                                {
                                    M.First().Element("Esito").Value = "Inviato";
                                    M.First().Element("DataInvio").Value = DateTime.Now.ToString();
                                }
                            }
                            else
                            {
                                var M = from a in MyXml.Elements("Mail")
                                        where a.Element("EMail").Value.Equals(TempRecipients[i][Destinatario_Email])
                                        select a;

                                if (M.Count() > 0)
                                {
                                    M.First().Element("Esito").Value = "Inviato";
                                    M.First().Element("DataInvio").Value = DateTime.Now.ToString();
                                }
                            }
                            MyXml.Save(MapPath("~/ZeusInc/TrackingMailSent/Report_") + ID1LogMessage + ".xml");                                
                        }
                        else
                        {
                            ErrorTicketLog(ID1LogMessage);
                            return false;
                        }
                    }
                }

                CloseTicketLog(ID1LogMessage);
                return true;
            }
            catch (Exception p)
            {
                ErrorTicketLog(ID1LogMessage);
                Response.Write(p.ToString());
                return false;
            }
        }
    }

    private bool SendMail(string From, string To, string Body,
        string Alias, string Subject, string fileName,
        string mySmtpClient, string UserName, string Password)
    {
        try
        {
            Zeus.Mail myMail = new Zeus.Mail();

            myMail.MailToSend.To.Add(new MailAddress(To));
            myMail.MailToSend.From = new MailAddress(From, Alias);
            myMail.MailToSend.Subject = Subject;
            myMail.MailToSend.IsBodyHtml = true;
            myMail.MailToSend.Body = Body;
            
            HttpContext context = HttpContext.Current;
            string baseUrl = context.Request.Url.Scheme + "://" + context.Request.Url.Authority + context.Request.ApplicationPath.TrimEnd('/') + '/';
            myMail.MailToSend.Body = myMail.MailToSend.Body.Replace(@"src=""/zeusinc", @"src=""" + baseUrl + "ZeusInc");
            
            if (fileName.Length > 0)
            {
                Attachment data = new Attachment(fileName);
                System.Net.Mime.ContentDisposition disposition = data.ContentDisposition;
                disposition.CreationDate = System.IO.File.GetCreationTime(fileName);
                disposition.ModificationDate = System.IO.File.GetLastWriteTime(fileName);
                disposition.ReadDate = System.IO.File.GetLastAccessTime(fileName);
                myMail.MailToSend.Attachments.Add(data);
            }

            if (mySmtpClient.Length <= 0)
                return false;

            myMail.ExtSMTPSettings.Host = mySmtpClient;
            if ((UserName.Length > 0) && (Password.Length > 0))
            {
                myMail.ExtSMTPSettings.Credentials = new System.Net.NetworkCredential(UserName, Password);
            }

            myMail.SendMail(Zeus.Mail.MailSendMode.Sync);

                      
            return true;
        }
        catch (Exception p)
        {
            return false;
        }
    }

    private bool SendErrorMail(string From, string To, string Body, string Subject)
    {
        try
        {
            MailMessage mail = new MailMessage();

            mail.To.Add(new MailAddress(To));
            mail.From = new MailAddress(From);
            mail.Subject = Subject;
            mail.IsBodyHtml = false;
            mail.Body = Body;

            SmtpClient client = new SmtpClient("mail.delinea.it");

            client.Send(mail);
            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }
}