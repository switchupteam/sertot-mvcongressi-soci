﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="DettaglioTrackingRead.aspx.cs" Inherits="DettaglioTrackingRead"
    Theme="Zeus" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <div class="BlockBox">
        <div class="BlockBoxHeader">
            <asp:Label ID="Label1" runat="server">Note sul tracking delle visualizzazioni dei messaggi E-Mail</asp:Label>
        </div>
        <table width="100%">
            <tr>
                <td>
                    - Gli utenti vengono tracciati se accettano la visualizzazione delle immagini allegate.
                </td>
            </tr>
            <tr>
                <td>
                    - Se un utente apre il messaggio E-Mail ma non scarica le immagini, l'utente non può essere tracciato.
                </td>
            </tr>
            <tr>
                <td>
                    - Gli utenti vengono tracciati la prima volta che scaricano le immagini e non vengono più inseriti successivamente.
                </td>
            </tr>
            <tr>
                <td>
                    - L’utilizzo del tracking delle visualizzazioni può influire in parte sul punteggio negativo da parte di alcuni sistemi AntiSpam.
                </td>
            </tr>
        </table>
    </div>
    <asp:GridView ID="GridView1" runat="server" AllowSorting="true" AutoGenerateColumns="False"
        Width="100%" CellPadding="0" PageSize="50"
        AllowPaging="true">
        <Columns>
            <asp:BoundField DataField="Email" HeaderText="E-Mail"/>
            <asp:BoundField DataField="Data" HeaderText="Data" ItemStyle-HorizontalAlign="Center"/>            
            <asp:BoundField DataField="Cognome" HeaderText="Cognome"/>
            <asp:BoundField DataField="Nome" HeaderText="Nome"/>
            <asp:BoundField DataField="RagioneSociale" HeaderText="Azienda"/>
            <asp:BoundField DataField="UserName" HeaderText="UserName"/>
            <asp:BoundField DataField="UserId" HeaderText="UserId" ItemStyle-HorizontalAlign="Center"/>
                       
            <asp:HyperLinkField DataNavigateUrlFields="UserId" DataNavigateUrlFormatString="/Zeus/Account/Account_Dtl.aspx?UID={0}"
                HeaderText="Dettaglio" Text="Dettaglio">
                <ControlStyle CssClass="GridView_Button1" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:HyperLinkField>
        </Columns>
        <EmptyDataTemplate>
            Dati non presenti
        </EmptyDataTemplate>
    </asp:GridView>    
</asp:Content>
