﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per Elenco
/// </summary>
public partial class Communicator_Elenco : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();

        ElencoDestinatariSqlDataSource.SelectCommand = "SELECT LoweredEmail ,Nome ,Cognome";
        ElencoDestinatariSqlDataSource.SelectCommand += 
            " FROM tbProfiliPersonali INNER JOIN tbProfiliMarketing ON tbProfiliPersonali.UserId = tbProfiliMarketing.UserId";
        ElencoDestinatariSqlDataSource.SelectCommand += " INNER JOIN tbProfiliSocietari ON tbProfiliPersonali.UserId = tbProfiliSocietari.UserId";
        ElencoDestinatariSqlDataSource.SelectCommand += " INNER JOIN aspnet_Membership ON tbProfiliPersonali.UserId = aspnet_Membership.UserId";
        ElencoDestinatariSqlDataSource.SelectCommand += 
            " LEFT OUTER JOIN dbo.tbBusinessBook ON dbo.tbProfiliSocietari.ID2BusinessBook = dbo.tbBusinessBook.ID1BsnBook";
        
        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI8"))
                   && (Request.QueryString["XRI8"].Length > 0))
            ElencoDestinatariSqlDataSource.SelectCommand += 
                " INNER JOIN tbxBusinessBook_MultiCategorie ON tbxBusinessBook_MultiCategorie.ID2BusinessBook =tbBusinessBook.ID1BsnBook ";
        
        ElencoDestinatariSqlDataSource.SelectCommand += " WHERE IsLockedOut=0 AND IsApproved=1 ";

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRE1"))
             && (Request.QueryString["XRE1"].Length > 0))
            ElencoDestinatariSqlDataSource.SelectCommand += " AND Nome LIKE '%" 
                + Server.HtmlEncode(Request.QueryString["XRE1"]) + "%' ";

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRE2"))
                && (Request.QueryString["XRE2"].Length > 0))
            ElencoDestinatariSqlDataSource.SelectCommand += " AND Cognome LIKE '%" 
                + Server.HtmlEncode(Request.QueryString["XRE2"]) + "%' ";

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRE3"))
            && (Request.QueryString["XRE3"].Length > 0))
            ElencoDestinatariSqlDataSource.SelectCommand += " AND RagioneSociale LIKE '%" 
                + Server.HtmlEncode(Request.QueryString["XRE3"]) + "%' ";

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRE4"))
             && (Request.QueryString["XRE4"].Length > 0))
            ElencoDestinatariSqlDataSource.SelectCommand += " AND LoweredEmail LIKE '%"
                + Server.HtmlEncode(Request.QueryString["XRE4"]) + "%' ";

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRE9"))
             && (Request.QueryString["XRE9"].Length > 0))
        {
            string[] Separator = new string[] { "," };

            ElencoDestinatariSqlDataSource.SelectCommand += " AND ('" 
                + Request.QueryString["XRE9"].Split(Separator, StringSplitOptions.None)[0].Trim()
                + "' IN (SELECT * FROM dbo.SplitCSV(tbProfiliMarketing.Meta_ZeusSearch)) ";

            for (int i = 1; i < Request.QueryString["XRE9"].Split(Separator, StringSplitOptions.None).Length; i++)
            {
                ElencoDestinatariSqlDataSource.SelectCommand += " " + Request.QueryString["XRE10"] + " '" 
                    + Request.QueryString["XRE9"].Split(Separator, StringSplitOptions.None)[i].Trim() 
                    + "' IN (SELECT * FROM dbo.SplitCSV(tbProfiliMarketing.Meta_ZeusSearch)) ";
            }

            ElencoDestinatariSqlDataSource.SelectCommand += " )";
        }
        
        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI2"))
                             && (Request.QueryString["XRI2"].Length > 0)
                             &&(Request.QueryString["XRI2"].Equals("1")))
        {
            if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI1"))
                      && (Request.QueryString["XRI1"].Length > 0))
            {
                string NewsLetter = GetTableColumnName(Server.HtmlEncode(Request.QueryString["XRI1"]));

                if (NewsLetter.Length > 0)
                    ElencoDestinatariSqlDataSource.SelectCommand += " AND " + NewsLetter + "=1";
            }
        }

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI3"))
                    && (Request.QueryString["XRI3"].Length > 0))
            ElencoDestinatariSqlDataSource.SelectCommand += " AND ID2Categoria1=" 
                + Server.HtmlEncode(Request.QueryString["XRI3"]);

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI4"))
                    && (Request.QueryString["XRI4"].Length > 0))
            ElencoDestinatariSqlDataSource.SelectCommand += " AND ID2BusinessBook=" 
                + Server.HtmlEncode(Request.QueryString["XRI4"]);

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI5"))
                    && (Request.QueryString["XRI5"].Length > 0))
            ElencoDestinatariSqlDataSource.SelectCommand += " AND tbBusinessBook.ID2Categoria1="
                + Server.HtmlEncode(Request.QueryString["XRI5"]);

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI6"))
                    && (Request.QueryString["XRI6"].Length > 0))
            ElencoDestinatariSqlDataSource.SelectCommand += " AND tbBusinessBook.ID2Categoria2="
                + Server.HtmlEncode(Request.QueryString["XRI6"]);

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI7"))
                    && (Request.QueryString["XRI7"].Length > 0))
            ElencoDestinatariSqlDataSource.SelectCommand += " AND tbBusinessBook.ID2Categoria3="
                + Server.HtmlEncode(Request.QueryString["XRI7"]);

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI8"))
                    && (Request.QueryString["XRI8"].Length > 0))
            ElencoDestinatariSqlDataSource.SelectCommand += " AND tbxBusinessBook_MultiCategorie.ID2Categoria4="
                + Server.HtmlEncode(Request.QueryString["XRI8"]);
        
        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI"))
                     && (Request.QueryString["XRI"].Length > 0))
        {
            ElencoDestinatariSqlDataSource.SelectCommand += " AND aspnet_Membership.UserId  NOT IN";
            ElencoDestinatariSqlDataSource.SelectCommand += " (SELECT   tbCommunicator_LogUnsubscribe.UserId ";
            ElencoDestinatariSqlDataSource.SelectCommand += " FROM  tbCommunicator_LogUnsubscribe INNER JOIN ";
            ElencoDestinatariSqlDataSource.SelectCommand += @" tbCommunicator_LogMessage 
                ON tbCommunicator_LogUnsubscribe.ID2LogMessage = tbCommunicator_LogMessage.ID1LogMessage ";
            ElencoDestinatariSqlDataSource.SelectCommand += " WHERE tbCommunicator_LogMessage.ID2Communicator=" 
                + Server.HtmlEncode(Request.QueryString["XRI"]) + ")";
            
            if (ConfigurationManager.AppSettings["MissingMail"] != null)
                ElencoDestinatariSqlDataSource.SelectCommand += " AND aspnet_Membership.LoweredEmail <> '" 
                    + ConfigurationManager.AppSettings["MissingMail"] + "' ";            
        }

        ElencoDestinatariSqlDataSource.SelectCommand += " ORDER BY  cognome, nome, LoweredEmail ";
    }

    private string GetTableColumnName(string ID1FlagConsenso)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {
            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                if (!ID1FlagConsenso.Equals("0"))
                {
                    sqlQuery = "SELECT DISTINCT tbCommunicator_FlagConsenso.CampoDB";
                    sqlQuery += " FROM tbCommunicator INNER JOIN tbCommunicator_FlagConsenso";
                    sqlQuery += " ON tbCommunicator.ID2FlagConsensoAssociato = tbCommunicator_FlagConsenso.ID1FlagConsenso";
                    sqlQuery += " WHERE ID1FlagConsenso=" + ID1FlagConsenso;
                    command.CommandText = sqlQuery;
                }
                else 
                    return string.Empty;

                SqlDataReader reader = command.ExecuteReader();
                string NewsLetter = string.Empty;

                while (reader.Read())
                    NewsLetter = reader.GetString(0);

                reader.Close();
                return NewsLetter;
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
                return string.Empty;
            }
        }
    }

    private int GetDuplicate(string sqlQuery)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        sqlQuery = sqlQuery.Replace("SELECT LoweredEmail ,Nome ,Cognome", @"select sum(LoweredEmail)
from
(
 SELECT COUNT( distinct LoweredEmail) as LoweredEmail");
        sqlQuery = sqlQuery.Replace(" ORDER BY  cognome, nome, LoweredEmail", string.Empty);
        sqlQuery += @" GROUP BY LoweredEmail HAVING (COUNT(LoweredEmail) > 1)";
        sqlQuery += @" )as a ";

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {
            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = sqlQuery;

                int myReturn = 0;
                if (command.ExecuteScalar() != DBNull.Value)
                    myReturn = Convert.ToInt32(command.ExecuteScalar());
                else
                    myReturn = 0;
                
                return myReturn;
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
                return -1;
            }
        }
    }

    protected void ElencoDestinatariSqlDataSource_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.AffectedRows > 0)
        {
            int Duplicate = GetDuplicate(ElencoDestinatariSqlDataSource.SelectCommand);
            int Totale = 1;

            if (e.AffectedRows > Duplicate)
                Totale = (e.AffectedRows - Duplicate);
            
            Elenco_Label.Text = " Elenco destinatari comunicazione - Totale indirizzi: "
                + e.AffectedRows.ToString() 
                + " - Totale duplicati: " 
                + Duplicate.ToString() 
                + " - Totale invii: "
                + Totale.ToString();        
        }
    }
}