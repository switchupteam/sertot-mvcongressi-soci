﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="Content_Src.aspx.cs"
    Inherits="Communicator_Content_Src"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <dlc:PermitRoles ID="myPermitRoles" runat="server" PAGE_TYPE="SRC" SQL_TABLE="tbPZ_Communicator" />
    <div class="BlockBox">
        <div class="BlockBoxHeader">
            <asp:Label ID="ZML_DettaglioRicercaTitle" runat="server" ></asp:Label></div>
        <table>
            <tr>
                <td class="BlockBoxDescription">
                    <asp:Label ID="ZML_RicercaTitolo" SkinID="FieldDescription" runat="server"></asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:TextBox ID="TitoloTextBox" runat="server" MaxLength="100" Columns="100"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="BlockBoxDescription">
                    <asp:Label ID="ZML_RicercaCategoria" SkinID="FieldDescription" runat="server"></asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:DropDownList ID="ID2CategoriaDropDownList" runat="server" AppendDataBoundItems="True">
                        <asp:ListItem Text="> Tutte" Value="0" Selected="True"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="dsCategoria" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>">
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td class="BlockBoxDescription">
                    <asp:Label ID="ZML_RicercaIsModello" SkinID="FieldDescription" runat="server"></asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:DropDownList ID="FiltroModelloDropDownList" runat="server" AppendDataBoundItems="True">
                        <asp:ListItem Text="> Tutti" Value="0" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Solo modelli" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Solo comunicazioni" Value="2"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="BlockBoxDescription">
                    <asp:Label ID="ZML_RicercaIsAttivo" SkinID="FieldDescription" runat="server" ></asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:DropDownList ID="AttivoDropDownList" runat="server" AppendDataBoundItems="True">
                        <asp:ListItem Text="> Tutti" Value="0" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Solo attivi" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Solo non attivi" Value="2"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="BlockBoxDescription">
                    <asp:Label ID="ZML_RicercaConsenso" SkinID="FieldDescription" runat="server"></asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:DropDownList ID="ConsensoDropDownList" runat="server" AppendDataBoundItems="True"
                        DataValueField="ID1FlagConsenso" DataTextField="Descrizione" DataSourceID="ConsensoSqlDataSource">
                        <asp:ListItem Text="> Tutti" Value="0" Selected="True"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="ConsensoSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                        SelectCommand="SELECT [ID1FlagConsenso],[Descrizione] FROM [tbCommunicator_FlagConsenso] WHERE PW_Zeus1=1"></asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td class="BlockBoxDescription">
                    <asp:Label ID="ZML_RicercaCreatoTraIl" runat="server" SkinID="FieldDescription"></asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:TextBox ID="PWI_Area1TextBox" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="PWI_Area1TextBox"
                        SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="*" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$">Formato data richiesto: GG/MM/AAAA</asp:RegularExpressionValidator>
                    <asp:Label ID="ZML_RicercaCreatoTraIl2" runat="server" SkinID="FieldDescription"></asp:Label>
                    <asp:TextBox ID="PWF_Area1TextBox" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="PWF_Area1TextBox"
                        SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="*" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$">Formato data richiesto: GG/MM/AAAA</asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="PWI_Area1TextBox"
                        ControlToValidate="PWF_Area1TextBox" SkinID="ZSSM_Validazione01" Display="Dynamic"
                        ErrorMessage="La data iniziale deve essere precedente o uguale alla data finale"
                        Operator="GreaterThanEqual" Type="Date"></asp:CompareValidator>
                </td>
            </tr>
             <tr>
                <td class="BlockBoxDescription">
                </td>
                <td class="BlockBoxValue">
                    <asp:LinkButton SkinID="ZSSM_Button01" ID="SearchButton" runat="server" Text="Cerca"
                        OnClick="SearchButton_Click"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
