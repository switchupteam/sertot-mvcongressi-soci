﻿<%@ Control Language="C#" ClassName="LogUnsubscribe" %>

<script runat="server">

    
    private string myID2Communicator = string.Empty;


    public string ID2Communicator
    {
        set { myID2Communicator = value; }
    }



    protected void Page_Load(object sender, EventArgs e)
    {
        BindTheData();
    
    }//fine Page_Load
    
    

    public void BindTheData()
    {
        LogUnsubscribeSqlDataSource.SelectCommand = "SELECT [UnsubscribeDate], [SendDateStart], [TotaleRimossi], [DestinatariTotale]";
        LogUnsubscribeSqlDataSource.SelectCommand += " FROM [vwLogUnsubscribe]";

        if (myID2Communicator.Length > 0)
            LogUnsubscribeSqlDataSource.SelectCommand += " WHERE ID2Communicator=" +myID2Communicator;

        GridView1.DataSourceID = "LogUnsubscribeSqlDataSource";
        GridView1.DataBind();

    }//fine BindTheData
    
    
    
</script>

<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AllowSorting="false" Width="100%">
    <Columns>
        <asp:BoundField DataField="UnsubscribeDate" HeaderText="Data rimozione" SortExpression="UnsubscribeDate" />
        <asp:BoundField DataField="SendDateStart" HeaderText="Data invio" SortExpression="SendDateStart" />
        <asp:BoundField DataField="TotaleRimossi" HeaderText="Totale rimossi" SortExpression="TotaleRimossi" />
        <asp:BoundField DataField="DestinatariTotale" HeaderText="Totale inviati" SortExpression="DestinatariTotale" />
    </Columns>
    <EmptyDataTemplate>
        Dati non presenti
    </EmptyDataTemplate>
    <EmptyDataRowStyle BackColor="White" Width="100px" BorderColor="Red" BorderWidth="0px"
        Height="100px" />
</asp:GridView>
<asp:SqlDataSource ID="LogUnsubscribeSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>">
</asp:SqlDataSource>
