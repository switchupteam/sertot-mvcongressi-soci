﻿<%@ Page Language="C#" Theme="Zeus" CodeFile="Elenco.aspx.cs"
    Inherits="Communicator_Elenco"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Zeus - Elenco destinatari comunicazione</title>
</head>
<body>
    <form id="form1" runat="server">
    <div align="center">
    <div class="LayGridTitolo1">
        <asp:Label ID="Elenco_Label" runat="server" Text="Elenco destinatari comunicazione" SkinID="GridTitolo1"></asp:Label>
    </div>
        <asp:GridView ID="ElencoGridView" runat="server" DataSourceID="ElencoDestinatariSqlDataSource"
         AllowPaging="True" AutoGenerateColumns="False"
            AllowSorting="true" PageSize="30">
            <Columns>
                <asp:BoundField DataField="Cognome" HeaderText="Cognome"  SortExpression="Cognome"/>
                <asp:BoundField DataField="Nome" HeaderText="Nome"  SortExpression="Nome"/>
                <asp:BoundField DataField="LoweredEmail" HeaderText="E-Mail"  SortExpression="LoweredEmail"/>
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="ElencoDestinatariSqlDataSource" runat="server" 
            ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>" onselected="ElencoDestinatariSqlDataSource_Selected"
        ></asp:SqlDataSource>
         <br />
        <asp:ImageButton runat="server" ID="CloseWindowImageButton" OnClientClick="javascript:window.close(); return 0;"
            PostBackUrl="javascript:void(0);return 0;" ImageUrl="~/Zeus/SiteImg/Bt170_ChiudiFinestra.gif">
        </asp:ImageButton>
    </div>
    </form>
</body>
</html>
