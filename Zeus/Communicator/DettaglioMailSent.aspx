﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DettaglioMailSent.aspx.cs" Inherits="DettaglioMailSent"
    MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" Theme="Zeus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <div class="BlockBox">
        <div class="BlockBoxHeader">
            <asp:Label ID="Label2" runat="server">Info Comunicazione</asp:Label>
        </div>
        <table>    
            <tr>
                <td class="BlockBoxDescription">
                    <asp:Label ID="Label3" runat="server" SkinID="FieldDescription">Titolo:</asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:Label ID="TitoloLabel" runat="server" SkinID="FieldValue" />
                </td>
                <td class="BlockBoxDescription">
                    <asp:Label ID="Label1" runat="server" SkinID="FieldDescription">Stato Invio:</asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:Label ID="StatoInvioLabel" runat="server" SkinID="FieldValue" />
                </td>                
                <td class="BlockBoxDescription">
                    <asp:Label ID="Label6" runat="server" SkinID="FieldDescription">Modalità Invio:</asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:Label ID="ModalitaLabel" runat="server" SkinID="FieldValue" />
                </td>                
                <td class="BlockBoxDescription">
                    <asp:Label ID="Label8" runat="server" SkinID="FieldDescription">Destinatari:</asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:Label ID="DestinatariLabel" runat="server" SkinID="FieldValue" />
                </td>
                <td class="BlockBoxDescription">
                    <asp:Label ID="Label9" runat="server" SkinID="FieldDescription">Inviati:</asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:Label ID="InviatiLabel" runat="server" SkinID="FieldValue" />
                </td>                
            </tr>
            <tr>
                <td class="BlockBoxDescription">
                    <asp:Label ID="Label7" runat="server" SkinID="FieldDescription">Utente:</asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:Label ID="UtenteLabel" runat="server" SkinID="FieldValue" />
                </td>
                <td class="BlockBoxDescription">
                    <asp:Label ID="Label4" runat="server" SkinID="FieldDescription">Invio:</asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:Label ID="InvioLabel" runat="server" SkinID="FieldValue" />
                </td>
                <td class="BlockBoxDescription">
                    <asp:Label ID="Label11" runat="server" SkinID="FieldDescription">Fine Spedizione:</asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:Label ID="FineSpedizioneLabel" runat="server" SkinID="FieldValue" />
                </td>
                <td class="BlockBoxDescription">
                    <asp:Label ID="Label10" runat="server" SkinID="FieldDescription">Log:</asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:Label ID="LogLabel" runat="server" SkinID="FieldValue" />
                </td>
            </tr>
        </table>
    </div>
    <div class="BlockBox">
        <div class="BlockBoxHeader">
            <asp:Label ID="Label22" runat="server">Download Report</asp:Label>
        </div>
        <table width="100%">
            <tr>
                <td>
                    <asp:Button runat="server" SkinID="ZSSM_Button01" Width="200" Text="Download Report Xml" 
                        Height="19" ID="DownloadReportButton" OnClick="DownloadReportButton_Click" />
                </td>
            </tr>
        </table>
    </div>
    <asp:GridView ID="GridView1" runat="server" AllowSorting="true" AutoGenerateColumns="False" OnPageIndexChanging="GridView1_PageIndexChanging"
        Width="100%" CellPadding="0" PageSize="50" AllowPaging="true">
        <Columns>
            <asp:BoundField DataField="EMail" HeaderText="E-Mail" />
            <asp:BoundField DataField="DataInvio" HeaderText="Data Invio" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="Cognome" HeaderText="Cognome" />
            <asp:BoundField DataField="Nome" HeaderText="Nome" />
            <asp:BoundField DataField="Oggetto" HeaderText="Oggetto" />
            <asp:BoundField DataField="Esito" HeaderText="Esito" />
            <asp:HyperLinkField DataNavigateUrlFields="UserId" DataNavigateUrlFormatString="/Zeus/Account/Account_Dtl.aspx?UID={0}"
                HeaderText="Dettaglio" Text="Dettaglio">
                <ControlStyle CssClass="GridView_Button1" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:HyperLinkField>
        </Columns>
        <EmptyDataTemplate>
            Dati non presenti
        </EmptyDataTemplate>
    </asp:GridView>
</asp:Content>

