﻿<%@ Control Language="C#" ClassName="LogAllegati" %>

<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.IO" %>


<script runat="server">

    private string myID2Communicator = string.Empty;


    public string ID2Communicator
    {
        set { myID2Communicator = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        BindTheData();

    }//fine Page_Load

    public void BindTheData()
    {
        if (myID2Communicator.Length > 0)
        {

            LogAllegatiSqlDataSource.SelectCommand = "SELECT DISTINCT [ID1Allegato], Path";
            LogAllegatiSqlDataSource.SelectCommand += " FROM [tbCommunicator_Allegati]";
            LogAllegatiSqlDataSource.SelectCommand += " WHERE ID2Communicator=" + myID2Communicator;
            GridView1.DataSourceID = "LogAllegatiSqlDataSource";
            GridView1.DataBind();
        }

    }//fine BindTheData

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        
        if (e.CommandName == "Download")
        {
            
            string sqlQuery = string.Empty;
            string ZeusId = string.Empty;

            int currentRowIndex = Int32.Parse(e.CommandArgument.ToString());
            string Allegato = GridView1.DataKeys[currentRowIndex].Value.ToString();
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            try
            {
                using (SqlConnection connection = new SqlConnection(strConnessione))
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    sqlQuery = @"SELECT ZeusId
                            FROM tbCommunicator
                            WHERE ID1Communicator = (SELECT ID2Communicator
                                                    FROM tbCommunicator_Allegati
                                                    WHERE ID1Allegato = @ID1Allegato)
                            ";

                    command.CommandText = sqlQuery;
                    command.Parameters.Add("@ID1Allegato", System.Data.SqlDbType.Int);
                    command.Parameters["@ID1Allegato"].Value = Convert.ToInt32(Allegato);

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        if (reader["ZeusId"] != DBNull.Value)
                            ZeusId = reader["ZeusId"].ToString();
                    }

                }
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
            }

            try
            {
                FileInfo file = new FileInfo(MapPath("~/ZeusInc/TrackingAttachment/") + Allegato + "_Allegato_" + ZeusId + ".xml");

                if (file.Exists)
                {
                    Response.ClearContent();
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                    Response.AddHeader("Content-Length", file.Length.ToString());
                    Response.ContentType = "text/xml";
                    Response.TransmitFile(file.FullName);
                    Response.End();
                }

            }
            catch (Exception p)
            {
            }
        }
    }


</script>

<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" OnRowCommand="GridView1_RowCommand"
    Width="100%" CellPadding="0" AllowSorting="true" DataKeyNames="ID1Allegato">
    <Columns>
        <asp:BoundField DataField="Path" HeaderText="File allegato"/>
        <asp:HyperLinkField DataNavigateUrlFields="Path" DataNavigateUrlFormatString="{0}"
            HeaderText="File" Text="Apri">
            <ControlStyle CssClass="GridView_Button1" />
            <ItemStyle HorizontalAlign="Center" />
        </asp:HyperLinkField>
        <asp:HyperLinkField DataNavigateUrlFields="ID1Allegato" DataNavigateUrlFormatString="/Zeus/Communicator/DettaglioTracking.aspx?XRI={0}" HeaderStyle-Width="200"
            HeaderText="Elenco utenti" Text="Visualizza">
            <ControlStyle CssClass="GridView_Button1" />
            <ItemStyle HorizontalAlign="Center" />
        </asp:HyperLinkField>

        <asp:ButtonField HeaderText="File XML" Text="Download" HeaderStyle-Width="200"
            CommandName="Download" 
             >
            <ControlStyle CssClass="GridView_Button1" />
            <ItemStyle HorizontalAlign="Center" />
        </asp:ButtonField>        
    </Columns>
    <EmptyDataTemplate>
        Dati non presenti
    </EmptyDataTemplate>
</asp:GridView>
<asp:SqlDataSource ID="LogAllegatiSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"></asp:SqlDataSource>
