﻿// File JScript
// Keep user from entering more than maxLength characters
function doKeypress(control){
    if(control.value.length > control.attributes["maxLength"].value-1)
    {
        control.value = control.value.substring(0,control.attributes["maxLength"].value);
    }
}
// Cancel default behavior
function doBeforePaste(control){
   maxLength = control.attributes["maxLength"].value;
   if(maxLength)
   {
       event.returnValue = false;
   }
}
// Cancel default behavior and create a new paste routine
function doPaste(control) {

    setTimeout(function () {

        maxLength = control.attributes["MaxLength"].value;
        value = control.value;
        if (maxLength) {
            event.returnValue = false;
            maxLength = parseInt(maxLength);
            var o = control.document.selection.createRange();
            var iInsertLength = maxLength - value.length + o.text.length;
            var sData = window.clipboardData.getData("Text").substr(0, iInsertLength);
            o.text = sData;
        }

    }, 1000);
}

