//= require jquery
//= require jquery_ujs
//(function ($) {

var noTab = true;
//jQuery.noConflict();
(function ($) {
$(window).load(function () {
    if ($("#tabs").length !== 0) {
        noTab=false;
        $("#tabs").tabs({
            activate: function () { // or 'show' for older jquery versions
                var selectedTab = $('#tabs').tabs('option', 'active');
                $("#ctl00_ZeusContent_hdnSelectedTab").val(selectedTab);
            },
            active: $("#ctl00_ZeusContent_hdnSelectedTab").val()
        });

        if (window.location.href.indexOf("#PHOTO") > -1) {
            $("#tabs").tabs({ active: 3 });
        }

    $('#tabs > ul').each(function () {

    $(this).find('li').each(function () {
                var current = $(this);
                var debug = current.children().attr('href');
                var debug2 = $(current.children().attr('href')).html();
                if (!$(current.children().attr('href')).html().trim())
                    current.css('display', 'none');
                
            });
            // now that our current phrase is completely build we add it to our outer array
        });

        
    }
    if ($("#SaveOrderButton").length !== 0) {
        $("#SaveOrderButton").css("display", "none");
    }

});
})(jQuery);
//jQuery.noConflict(false);

function fnOnUpdateValidators() {

    if (noTab == false) {
        if ($("#ui-id-1").length !== 0)
            $("#ui-id-1").css("background-color", '');
        if ($("#ui-id-2").length !== 0)
            $("#ui-id-2").css("background-color", '');
        if ($("#ui-id-3").length !== 0)
            $("#ui-id-3").css("background-color", '');
        if ($("#ui-id-4").length !== 0)
            $("#ui-id-4").css("background-color", '');



        for (var i = 0; i < Page_Validators.length; i++) {
            var val = Page_Validators[i];
            //var ctrl = document.getElementById(val.controltovalidate);
            var ctrl = $("#" + val.controltovalidate);
            //alert($("#" + val.controltovalidate).parents('.Generali').length);
            //ctrl.css("background-color", '#FFAAAA');
            // if (ctrl != null && ctrl.style != null) {
            if (ctrl !== null) {
                if (!val.isvalid) {
                    // Impostazioni per Zeus/PageDesigner
                    if ((ctrl.parents('#Principale').length > 0) && ($("#ui-id-1").length !== 0))
                        $("#ui-id-1").css("background-color", '#FF1010');
                    if ((ctrl.parents('#Immagini').length > 0) && ($("#ui-id-2").length !== 0))
                        $("#ui-id-2").css("background-color", '#FF1010');
                    if ((ctrl.parents('#Contenuti').length > 0) && ($("#ui-id-3").length !== 0))
                        $("#ui-id-3").css("background-color", '#FF1010');
                    if ((ctrl.parents('#PhotoGallery').length > 0) && ($("#ui-id-4").length !== 0))
                        $("#ui-id-4").css("background-color", '#FF1010');

                    // Impostazioni per Zeus/Catalogo/Articoli
                    if ((ctrl.parents('#Listino').length > 0) && ($("#ui-id-4").length !== 0))
                        $("#ui-id-4").css("background-color", '#FF1010');
                    // ctrl.style.background = '#FFAAAA';
                    //ctrl.css("background-color", '#FFAAAA');
                }
                else
                    ctrl.css("background-color", '');
                //ctrl.style.backgroundColor = '';
            }
        }
    }
}

//})(jQuery);

