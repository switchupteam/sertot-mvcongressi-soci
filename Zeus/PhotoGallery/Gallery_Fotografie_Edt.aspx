<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
  
    static string TitoloPagina = "Photo gallery: modifica fotografia";


    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;
        delinea myDelinea = new delinea();

        if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI"))
            || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZID"))
            || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM")))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        ZIM.Value = Server.HtmlEncode(Request.QueryString["ZIM"]);

        if (!ReadXML(ZIM.Value, GetLang()))
            Response.Redirect("~/Zeus/System/Message.aspx?1");

        ReadXML_Localization(ZIM.Value, GetLang());

       

    }//fine Page_Load





    private string GetLang()
    {
        string Lang = "ITA";

        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;

    }//fine GetLang


    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_PhotoGallery.xml"));


            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {

                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {


                            myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                            if (myLabel != null)
                                myLabel.Text = childNode.InnerText;
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name + "Label");

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                            }


                        }
                        catch (Exception)
                        {

                        }
                    }//fine foreach
                }//fine foreach

            }//fine for




        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine ReadXML_Localization



    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            string myXPathEach = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_PhotoGallery.xml"));


            Panel myPanel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPathEach);

            for (int i = 0; i < nodelist.Count; ++i)
            {

                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {

                            if (childNode.Name.IndexOf("ZMCF_") > -1)
                            {
                                myPanel = (Panel)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myPanel != null)
                                    myPanel.Visible = Convert.ToBoolean(childNode.InnerText);
                            }

                        }
                        catch (Exception)
                        {

                        }
                    }//fine foreach
                }//fine foreach

            }//fine for


            Panel PW_Lang2Panel = (Panel)FormView1.FindControl("PW_Lang2Panel");
            PW_Lang2Panel.Visible =  Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Lang2").InnerText);

            Panel PW_Lang3Panel = (Panel)FormView1.FindControl("PW_Lang3Panel");
            PW_Lang3Panel.Visible = Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Lang3").InnerText);
            

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }

    }//fine ReadXML






    protected void UtenteCreazione(object sender, EventArgs e)
    {
        HiddenField UtenteCreazione = (HiddenField)sender;

        if (UtenteCreazione.Value.Length == 0)
            UtenteCreazione.Value = Membership.GetUser().ProviderUserKey.ToString();

    }

    protected void DataOggi(object sender, EventArgs e)
    {
        HiddenField DataCreazione = (HiddenField)sender;
        if (DataCreazione.Value.Length == 0)
        {
            DataCreazione.Value = DateTime.Now.ToString("d");
        }
    }


    //##############################################################################################################
    //################################################ FILE UPLOAD #################################################
    //############################################################################################################## 


    protected void ImageRaider_DataBinding(object sender, EventArgs e)
    {
        ImageRaider ImageRaider1 = (ImageRaider)sender;
        ImageRaider1.SetDefaultEditBetaImage();
        ImageRaider1.SetDefaultEditGammaImage();

    }

    protected void FileUploadCustomValidator_ServerValidate(object sender, ServerValidateEventArgs args)
    {
        try
        {
            ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");
            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");

            if (ImageRaider1.isValid())
                InsertButton.CommandName = "Update";
            else
                InsertButton.CommandName = string.Empty;

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine FileUploadCustomValidator
    //##############################################################################################################
    //########################################### FINE FILE UPLOAD #################################################
    //##############################################################################################################


    protected void InsertButton_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");
            HiddenField FileNameBetaHiddenField = (HiddenField)FormView1.FindControl("FileNameBetaHiddenField");
            HiddenField FileNameGammaHiddenField = (HiddenField)FormView1.FindControl("FileNameGammaHiddenField");
            HiddenField Image12AltHiddenField = (HiddenField)FormView1.FindControl("Image12AltHiddenField");
            ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");

            if (InsertButton.CommandName != string.Empty)
            {
                 if (ImageRaider1.GenerateBeta(string.Empty))
			FileNameBetaHiddenField.Value = ImageRaider1.ImgBeta_FileName;
		else FileNameBetaHiddenField.Value = ImageRaider1.DefaultBetaImage;

           if (ImageRaider1.GenerateGamma(string.Empty))
			FileNameGammaHiddenField.Value = ImageRaider1.ImgGamma_FileName;
		else FileNameGammaHiddenField.Value = ImageRaider1.DefaultGammaImage;

            }

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine InsertButton_Click


    protected void tbPhotoGallery_Photos_Insert_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.Exception == null)
            Response.Redirect("~/Zeus/PhotoGallery/Gallery_Fotografie.aspx?ZID=" + Server.HtmlEncode(Request.QueryString["ZID"]) + "&Lang=" + GetLang() + "&ZIM=" + Server.HtmlEncode(Request.QueryString["ZIM"]));
        else Response.Redirect("~/Zeus/System/Message.aspx?UpdErr");

    }//fine tbPhotoGallery_Photos_Insert_Updated
    
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" />
    <asp:HiddenField ID="ZIM" runat="server" />
    
    <asp:FormView ID="FormView1" runat="server" DataKeyNames="ID1Photo" DataSourceID="tbPhotoGallery_Photos_Insert"
        DefaultMode="Edit" Width="100%">
        <EditItemTemplate>
            <asp:Panel ID="AnteprimaPanel" runat="server">
                <dlc:ImageRaider ID="ImageRaider1" runat="server" ZeusIdModuloIndice="2" ZeusLangCode="ITA"
                    BindPzFromDB="true" IsEditMode="true" DefaultEditBetaImage='<%# Eval("Immagine1") %>'
                    ImageAlt='<%# Eval("Immagine12Alt") %>'
                    DefaultEditGammaImage='<%# Eval("Immagine2") %>' OnDataBinding="ImageRaider_DataBinding" />
                <asp:HiddenField ID="FileNameBetaHiddenField" runat="server" Value='<%# Bind("Immagine1") %>' />
                <asp:HiddenField ID="FileNameGammaHiddenField" runat="server" Value='<%# Bind("Immagine2") %>' />
                <asp:HiddenField ID="Image12AltHiddenField" runat="server" Value='<%# Bind("Immagine12Alt") %>' />
                <asp:CustomValidator ID="FileUploadCustomValidator" runat="server" OnServerValidate="FileUploadCustomValidator_ServerValidate"
                    Display="Dynamic" SkinID="ZSSM_Validazione01" ValidationGroup="myValidation"
                    Visible="false"></asp:CustomValidator>
            </asp:Panel>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="IdentificativoLabel" runat="server">
                    Dettaglio fotografia
                    </asp:Label>
                </div>
                <table>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="ZML_Lang1" runat="server" SkinID="FieldDescription">Didascalia ITA
                            </asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <CustomWebControls:TextArea ID="DidascaliaITATextArea" runat="server" Text='<%# Bind("Didascalia1") %>'
                                MaxLength="198" Columns="100">
                                &nbsp;&nbsp;
                            </CustomWebControls:TextArea>
                        </td>
                    </tr>
                    <asp:Panel ID="ZMCF_Lang2" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_Lang2" runat="server" SkinID="FieldDescription">Didascalia ENG
                                </asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <CustomWebControls:TextArea ID="DidascaliaENGTextArea" runat="server" Text='<%# Bind("Didascalia2") %>'
                                    MaxLength="198" Columns="100">
                            &nbsp;&nbsp;
                                </CustomWebControls:TextArea>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_Lang3" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_Lang3" runat="server" SkinID="FieldDescription">Didascalia FRA
                                </asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <CustomWebControls:TextArea ID="DidascaliaFRATextArea" runat="server" Text='<%# Bind("Didascalia3") %>'
                                    MaxLength="198" Columns="100">
                                &nbsp;&nbsp;
                                </CustomWebControls:TextArea>
                            </td>
                        </tr>
                    </asp:Panel>
                </table>
            </div>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="Planner" runat="server"> Planner
                    </asp:Label>
                </div>
                <table>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="PZL_PWArea1_NewLabel" SkinID="FieldDescription" runat="server">Pubblica</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="DidascaliaITACheckBox" runat="server" Checked='<%# Bind("PW_Lang1") %>' />
                                        <asp:Label ID="ZML_Lang1Extended" runat="server" Text="Italiano" SkinID="FieldValue"></asp:Label>
                                    </td>
                                    <td width="30px">
                                        <img src="../SiteImg/Spc.gif" width="30px" height="1px" />
                                    </td>
                                    <asp:Panel ID="PW_Lang2Panel" runat="server">
                                        <td>
                                            <asp:CheckBox ID="DidascaliaENGCheckBox" runat="server" Checked='<%# Bind("PW_Lang2") %>' />
                                            <asp:Label ID="ZML_Lang2Extended" runat="server" Text="Inglese" SkinID="FieldValue"></asp:Label>
                                        </td>
                                        <td width="30px">
                                            <img src="../SiteImg/Spc.gif" width="30px" height="1px" />
                                        </td>
                                    </asp:Panel>
                                    <asp:Panel ID="PW_Lang3Panel" runat="server">
                                        <td>
                                            <asp:CheckBox ID="DidascaliaFRACheckBox" runat="server" Checked='<%# Bind("PW_Lang3") %>' />
                                            <asp:Label ID="ZML_Lang3Extended" runat="server" Text="Francese" SkinID="FieldValue"></asp:Label>
                                        </td>
                                    </asp:Panel>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <asp:HiddenField ID="HiddenFieldRecordEdtUser" runat="server" Value='<%# Bind("RecordEdtUser") %>'
                OnDataBinding="UtenteCreazione" />
            <asp:HiddenField ID="HiddenFieldRecordEdtDate" runat="server" Value='<%# Bind("RecordEdtDate") %>'
                OnDataBinding="DataOggi" />
            <div align="center">
                <asp:LinkButton ID="InsertButton" runat="server" Text="Salva dati" CommandName="Update"
                    SkinID="ZSSM_Button01" OnClick="InsertButton_Click"></asp:LinkButton>
            </div>
        </EditItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="tbPhotoGallery_Photos_Insert" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SELECT ID1Photo,Immagine2,Immagine1,Immagine12Alt,Didascalia1,Didascalia2,Didascalia3,PW_Lang1,PW_Lang2,PW_Lang3,RecordEdtUser,RecordEdtDate FROM [tbPhotoGallery_Photo] WHERE [ID1Photo] = @ID1Photo  AND ZeusIsAlive=1 AND ZeusIdModulo=@ZeusIdModulo AND ZeusLangCode=@ZeusLangCode"
        UpdateCommand=" SET DATEFORMAT dmy; UPDATE [tbPhotoGallery_Photo] SET [Immagine1] = @Immagine1, [Immagine2] = @Immagine2, Immagine12Alt = @Immagine12Alt, [Didascalia1] = @Didascalia1, [Didascalia2] = @Didascalia2, [Didascalia3] = @Didascalia3, [PW_Lang1] = @PW_Lang1, [PW_Lang2] = @PW_Lang2, [PW_Lang3] = @PW_Lang3,  [RecordEdtUser] = @RecordEdtUser, [RecordEdtDate] = @RecordEdtDate WHERE [ID1Photo] = @ID1Photo"
        OnUpdated="tbPhotoGallery_Photos_Insert_Updated">
        <SelectParameters>
            <asp:QueryStringParameter Name="ID1Photo" QueryStringField="XRI" />
            <asp:QueryStringParameter Name="ZeusIdModulo" QueryStringField="ZIM" Type="String" />
            <asp:QueryStringParameter Name="ZeusLangCode" QueryStringField="Lang" Type="String"
                DefaultValue="ITA" />
        </SelectParameters>
        <UpdateParameters>
            <asp:QueryStringParameter Name="ID1Photo" QueryStringField="XRI" />
            <asp:Parameter Name="Immagine1" Type="String" />
            <asp:Parameter Name="Immagine2" Type="String" />
            <asp:Parameter Name="Immagine12Alt" Type="String" />
            <asp:Parameter Name="Didascalia1" Type="String" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="Didascalia2" Type="String" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="Didascalia3" Type="String" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="PW_Lang1" Type="Boolean" />
            <asp:Parameter Name="PW_Lang2" Type="Boolean" />
            <asp:Parameter Name="PW_Lang3" Type="Boolean" />
            <asp:Parameter Name="RecordEdtUser" />
            <asp:Parameter Name="RecordEdtDate" Type="DateTime" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
