<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">
  
    static string TitoloPagina = "Nuova galleria fotografica";


    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;
        delinea myDelinea = new delinea();

        if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM")))
            Response.Redirect("~/Zeus/System/Message.aspx?0");


        ZIM.Value = Server.HtmlEncode(Request.QueryString["ZIM"]);
        ZID.Value = Server.HtmlEncode(Request.QueryString["ZID"]);
        Lang.Value = GetLang();
        LangO.Value = Server.HtmlEncode(Request.QueryString["LangO"]);

        if (!ReadXML(ZIM.Value, GetLang()))
            Response.Redirect("~/Zeus/System/Message.aspx?1");

        ReadXML_Localization(ZIM.Value, GetLang());

        GetDataFromLangO();

        LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");
        InsertButton.Attributes.Add("onclick", "Validate()");

    }//fine Page_Load



    private string GetLang()
    {
        string Lang = "ITA";

        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;

    }//fine GetLang



    private bool SetQueryOfID2CategoriaDropDownList(string ZeusIdModulo
        , string ZeusLangCode, string PZV_Cat1Liv)
    {
        try
        {
            


            SqlDataSource dsCategoria = (SqlDataSource)FormView1.FindControl("dsCategoria");
            DropDownList ID2CategoriaDropDownList = (DropDownList)FormView1.FindControl("ID2CategoriaDropDownList");
            HiddenField ID2CategoriaHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaHiddenField");
            ID2CategoriaHiddenField.Value = "0";

            switch (PZV_Cat1Liv)
            {

                case "123":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv1Liv2Liv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "Catliv1Liv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;


                case "23":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv2Liv3]";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;

                default:
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;
            }

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }//fine SetQueryOfID2CategoriaDropDownList



    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_PhotoGallery.xml"));


            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {

                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {

                            if (childNode.Name.Equals("ZML_TitoloPagina_New"))
                            {
                                TitleField.Value = childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                                else
                                {
                                    myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name + "Label");

                                    if (myLabel != null)
                                        myLabel.Text = childNode.InnerText;
                                }
                            }

                        }
                        catch (Exception)
                        {

                        }
                    }//fine foreach
                }//fine foreach

            }//fine for
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine ReadXML_Localization



    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            string myXPathEach = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_PhotoGallery.xml"));


            Panel myPanel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPathEach);

            for (int i = 0; i < nodelist.Count; ++i)
            {

                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {

                            if (childNode.Name.IndexOf("ZMCF_") > -1)
                            {
                                myPanel = (Panel)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myPanel != null)
                                    myPanel.Visible = Convert.ToBoolean(childNode.InnerText);
                            }

                        }
                        catch (Exception)
                        {

                        }
                    }//fine foreach
                }//fine foreach

            }//fine for



            SetQueryOfID2CategoriaDropDownList(mydoc.SelectSingleNode(myXPath + "ZMCD_ZIMCategoria1").InnerText
                , GetLang()
                , mydoc.SelectSingleNode(myXPath + "ZMCF_Cat1Liv").InnerText);


          
          
            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }

    }//fine ReadXML

    


    protected void ID2CategoriaDropDownList_SelectIndexChange(object sender, EventArgs e)
    {
        HiddenField ID2CategoriaHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaHiddenField");
        DropDownList ID2CategoriaDropDownList = (DropDownList)sender;

        ID2CategoriaHiddenField.Value = ID2CategoriaDropDownList.SelectedValue;

    }//fine ID2CategoriaDropDownList_SelectIndexChange



    protected void UtenteCreazione(object sender, EventArgs e)
    {
        HiddenField UtenteCreazione = (HiddenField)sender;
        UtenteCreazione.Value = Membership.GetUser().ProviderUserKey.ToString();

    }

    protected void DataOggi(object sender, EventArgs e)
    {
        HiddenField DataCreazione = (HiddenField)sender;
        DataCreazione.Value = DateTime.Now.ToString();
    }

    protected void ZeusLangCode(object sender, EventArgs e)
    {
        HiddenField Lang = (HiddenField)sender;
        if (Lang.Value == string.Empty)
        {
            if (Request.QueryString["Lang"] != null)
                Lang.Value = Request.QueryString["Lang"];
            else Lang.Value = "ITA";
        }
    }

    protected void CreazioneGUID(object sender, EventArgs e)
    {
       /* HiddenField Guid = (HiddenField)sender;
        if (Guid.Value == string.Empty)
        {
            Guid.Value = System.Guid.NewGuid().ToString();
        }*/
    }


    protected void PWI_Area1TextBox_DataBinding(object sender, EventArgs e)
    {
        TextBox DataCreazione = (TextBox)sender;

        if (DataCreazione.Text.Length == 0)
            DataCreazione.Text = DateTime.Now.ToString("d");

    }//FINE PWI_Area1TextBox_DataBinding

    protected void PWF_Area1TextBox_DataBinding(object sender, EventArgs e)
    {
        TextBox DataFinale = (TextBox)sender;

        if (DataFinale.Text.Length == 0)
            DataFinale.Text = "31/12/2040";

    }//FINE PWF_Area1TextBox_DataBinding


    protected void HiddenImageFile_DataBinding(object sender, EventArgs e)
    {

        HiddenField HiddenImageFile = (HiddenField)sender;

        if (HiddenImageFile.Value.Length == 0)
            HiddenImageFile.Value = "ImgNonDisponibile.jpg";

    }//fine HiddenImageFile_DataBinding



    //##############################################################################################################
    //################################################ FILE UPLOAD #################################################
    //############################################################################################################## 

    protected void FileUploadCustomValidator_ServerValidate(object sender, ServerValidateEventArgs args)
    {
        try
        {
            ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");
            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");

            if (ImageRaider1.isValid())
                InsertButton.CommandName = "Insert";
            else
                InsertButton.CommandName = string.Empty;

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine FileUploadCustomValidator
    //##############################################################################################################
    //########################################### FINE FILE UPLOAD #################################################
    //##############################################################################################################
    


    protected void InsertButton_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");
            HiddenField FileNameBetaHiddenField = (HiddenField)FormView1.FindControl("FileNameBetaHiddenField");
            HiddenField Image1AltHiddenField = (HiddenField)FormView1.FindControl("Image1AltHiddenField");
            ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");
            
            if (InsertButton.CommandName != string.Empty)
            {
                 if (ImageRaider1.GenerateBeta(string.Empty))
			FileNameBetaHiddenField.Value = ImageRaider1.ImgBeta_FileName;
		else FileNameBetaHiddenField.Value = ImageRaider1.DefaultBetaImage;

            }

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine InsertButton_Click

    protected void dsPhotoGalleryNew_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.Exception == null)
        {
            Response.Redirect("~/Zeus/PhotoGallery/Gallery_Lst.aspx?XRI=" 
                + e.Command.Parameters["@XRI"].Value.ToString() 
                + "&ZIM=" + ZIM.Value
                + "&Lang=" + GetLang());
        }
        else Response.Redirect("~/Zeus/System/Message.aspx?InsErr");
    }//fine dsPhotoGalleryNew_Inserted

    private bool GetDataFromLangO()
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        try
        {
            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = @"SELECT dbo.tbPhotoGallery.ID1Gallery,
                                dbo.tbPhotoGallery.Titolo, 
                                dbo.tbCategorie.Categoria, 
                                dbo.tbPhotoGallery.DescBreve,
                                dbo.tbPhotoGallery.Descrizione, 
                                dbo.tbPhotoGallery.Immagine1
                            FROM dbo.tbPhotoGallery LEFT OUTER JOIN
                                dbo.tbCategorie ON dbo.tbPhotoGallery.ID2Categoria = dbo.tbCategorie.ID1Categoria                
                            WHERE  dbo.tbPhotoGallery.ZeusId=@ZeusId 
                                AND dbo.tbPhotoGallery.ZeusLangCode=@ZeusLangCode";

                command.CommandText = sqlQuery;
                command.Parameters.Add("@ZeusId", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@ZeusId"].Value = new Guid(ZID.Value);
                command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ZeusLangCode"].Value = LangO.Value;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (reader["Titolo"] != DBNull.Value)
                    {
                        Label TitoloLabel = (Label)FormView1.FindControl("TitoloLabel");
                        TitoloLabel.Text = reader["Titolo"].ToString();
                    }

                    if (reader["Categoria"] != DBNull.Value)
                    {
                        Label CategoriaLabel = (Label)FormView1.FindControl("CategoriaLabel");
                        CategoriaLabel.Text = reader["Categoria"].ToString();
                    }

                    if (reader["DescBreve"] != DBNull.Value)
                    {
                        Label DescBreveLabel = (Label)FormView1.FindControl("DescBreveLabel");
                        DescBreveLabel.Text = reader["DescBreve"].ToString();
                    }

                    if (reader["Descrizione"] != DBNull.Value)
                    {
                        Label DescrizioneLabel = (Label)FormView1.FindControl("DescrizioneLabel");
                        DescrizioneLabel.Text = reader["Descrizione"].ToString();
                    }

                    if (reader["Immagine1"] != DBNull.Value)
                    {
                        HiddenField FileNameBetaHiddenField = (HiddenField)FormView1.FindControl("FileNameBetaHiddenField");
                        FileNameBetaHiddenField.Value = reader["Immagine1"].ToString();
                        ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");
                        ImageRaider1.DefaultEditBetaImage = reader["Immagine1"].ToString();
                        ImageRaider1.SetDefaultEditBetaImage();
                        ImageRaider1.DefaultBetaImage = reader["Immagine1"].ToString();
                    }
                    /*
                    if (reader["Immagine2"] != DBNull.Value)
                    {
                        HiddenField FileNameGammaHiddenField = (HiddenField)FormView1.FindControl("FileNameGammaHiddenField");
                        FileNameGammaHiddenField.Value = reader["Immagine2"].ToString();
                        ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");
                        ImageRaider1.DefaultEditGammaImage = reader["Immagine2"].ToString();
                        ImageRaider1.SetDefaultEditGammaImage();
                        ImageRaider1.DefaultGammaImage = reader["Immagine2"].ToString();
                    }*/

                    HiddenField HiddenFieldZeusId = (HiddenField)FormView1.FindControl("HiddenFieldZeusId");
                    HiddenFieldZeusId.Value = ZID.Value;
                }

                reader.Close();
                return true;
            }
        }
        catch (Exception p)
        {

            Response.Write(p.ToString());
            return false;
        }

    }
    protected void ImageRaider_DataBinding(object sender, EventArgs e)
    {
        ImageRaider ImageRaider1 = (ImageRaider)sender;
        ImageRaider1.SetDefaultEditBetaImage();
        ImageRaider1.SetDefaultEditGammaImage();

    }
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <script type="text/javascript">
        function OnClientModeChange(editor) {
            var mode = editor.get_mode();
            var doc = editor.get_document();
            var head = doc.getElementsByTagName("HEAD")[0];
            var link;

            switch (mode) {
                case 1: //remove the external stylesheet when displaying the content in Design mode    
                    //var external = doc.getElementById("external");
                    //head.removeChild(external);
                    break;
                case 2:
                    break;
                case 4: //apply your external css stylesheet to Preview mode    
                    link = doc.createElement("LINK");
                    link.setAttribute("href", "/SiteCss/Telerik.css");
                    link.setAttribute("rel", "stylesheet");
                    link.setAttribute("type", "text/css");
                    link.setAttribute("id", "external");
                    head.appendChild(link);
                    break;
            }
        }

        function editorCommandExecuted(editor, args) {
            if (!$telerik.isChrome)
                return;
            var dialogName = args.get_commandName();
            var dialogWin = editor.get_dialogOpener()._dialogContainers[dialogName];
            if (dialogWin) {
                var cellEl = dialogWin.get_contentElement() || dialogWin.ui.contentCell || dialogWin.ui.content,
                frame = dialogWin.get_contentFrame();
                frame.onload = function () {
                    cellEl.style.cssText = "";
                    dialogWin.autoSize();
                }
            }
        }
    </script>
    <asp:HiddenField ID="TitleField" runat="server"  />
    <asp:HiddenField ID="ZIM" runat="server" />
    <asp:HiddenField ID="ZID" runat="server" />
    <asp:HiddenField ID="Lang" runat="server" />
    <asp:HiddenField ID="LangO" runat="server" />
       
    <asp:FormView ID="FormView1" runat="server" DataKeyNames="ID1Gallery" DefaultMode="Insert"
        DataSourceID="dsPhotoGalleryNew" Width="100%">
        <InsertItemTemplate>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="IdentificativoLabel" runat="server">Galleria fotografica</asp:Label>
                </div>
                <table>
                     <tr>
                            <td class="BlockBoxDescription">
                             </td>
                            <td class="BlockBoxValue">
                                <asp:Label ID="TitoloLabel" runat="server" SkinID="FieldDescription"></asp:Label>
                        </tr>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label1" SkinID="FieldDescription" runat="server">Titolo *</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:TextBox ID="TitoloTextBox" runat="server" Text='<%# Bind("Titolo") %>' Columns="100"
                                MaxLength="100"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="TitoloRequiredFieldValidator" ErrorMessage="Obbligatorio"
                                runat="server" ControlToValidate="TitoloTextBox" SkinID="ZSSM_Validazione01"
                                Display="Dynamic" ValidationGroup="myValidation">
                            </asp:RequiredFieldValidator>
                    </tr>
                    <tr>
                        <td class="BlockBoxDescription">
                            </td>
                        <td class="BlockBoxValue">
                            <asp:Label ID="CategoriaLabel" runat="server" SkinID="FieldDescription"></asp:Label>
                    </tr>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Categoria_Label" SkinID="FieldDescription" runat="server" Text="Categoria *"></asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:DropDownList ID="ID2CategoriaDropDownList" runat="server" OnSelectedIndexChanged="ID2CategoriaDropDownList_SelectIndexChange"
                                AppendDataBoundItems="True">
                                <asp:ListItem Text="> Seleziona" Value="0" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:HiddenField ID="ID2CategoriaHiddenField" runat="server" Value='<%# Bind("ID2Categoria") %>' />
                            <asp:RequiredFieldValidator ID="ID2CategoriaRequiredFieldValidator" ErrorMessage="Obbligatorio"
                                runat="server" ControlToValidate="ID2CategoriaDropDownList" SkinID="ZSSM_Validazione01"
                                Display="Dynamic" InitialValue="0" ValidationGroup="myValidation">
                            </asp:RequiredFieldValidator>
                            <asp:SqlDataSource ID="dsCategoria" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>">
                                <SelectParameters>
                                    <asp:QueryStringParameter DefaultValue="0" Name="ZeusIdModulo" QueryStringField="ZIM"
                                        Type="String" />
                                    <asp:QueryStringParameter DefaultValue="ITA" Name="ZeusLangCode" QueryStringField="Lang"
                                        Type="String" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
            </div>
            <asp:Panel ID="ZMCF_BoxDescrizioneAll" runat="server">
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="Label12" runat="server">
                    Descrizione</asp:Label>
                </div>
                <table>
                <asp:Panel ID="ZMCF_BoxDescrizione" runat="server">
                    <tr>
                        <td class="BlockBoxDescription">
                            </td>
                        <td class="BlockBoxValue">
                            <asp:Label ID="DescBreveLabel" runat="server" SkinID="FieldDescription"></asp:Label>
                    </tr>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label3" runat="server" SkinID="FieldDescription" Text="Label">Descrizione breve *</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <CustomWebControls:TextArea ID="DescBreveTextArea" runat="server" Text='<%# Bind("DescBreve") %>'
                                MaxLength="500" Columns="100" Height="100" TextMode="MultiLine">
                            </CustomWebControls:TextArea>
                            <asp:RequiredFieldValidator ID="DescBreveRequiredFieldValidator1" ErrorMessage="Obbligatorio"
                                runat="server" ControlToValidate="DescBreveTextArea" SkinID="ZSSM_Validazione01"
                                Display="Dynamic" ValidationGroup="myValidation">
                            </asp:RequiredFieldValidator>
                    </tr>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_Contenuto1" runat="server">
                        <tr>
                        <td class="BlockBoxDescription">
                            </td>
                        <td class="BlockBoxValue">
                            <asp:Label ID="DescrizioneLabel" runat="server" SkinID="FieldDescription"></asp:Label>
                    </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label8" runat="server" SkinID="FieldDescription" Text="Label">Descrizione completa</asp:Label>
                            </td>
                            <td>
                                   <telerik:RadEditor
                                    Language="it-IT" ID="RadEditor1" runat="server"
                                    DocumentManager-DeletePaths="~/ZeusInc/PhotoGallery/Documents"
                                    DocumentManager-SearchPatterns="*.*"
                                    DocumentManager-ViewPaths="~/ZeusInc/PhotoGallery/Documents"
                                    DocumentManager-MaxUploadFileSize="52428800"
                                    DocumentManager-UploadPaths="~/ZeusInc/PhotoGallery/Documents"
                                    FlashManager-DeletePaths="~/ZeusInc/PhotoGallery/Media"
                                    FlashManager-MaxUploadFileSize="10240000"
                                    FlashManager-ViewPaths="~/ZeusInc/PhotoGallery/Media"
                                    FlashManager-UploadPaths="~/ZeusInc/PhotoGallery/Media"
                                    ImageManager-DeletePaths="~/ZeusInc/PhotoGallery/Images"
                                    ImageManager-ViewPaths="~/ZeusInc/PhotoGallery/Images"
                                    ImageManager-MaxUploadFileSize="10240000"
                                    ImageManager-SearchPatterns="*.gif, *.png, *.jpg, *.jpe, *.jpeg"
                                    ImageManager-UploadPaths="~/ZeusInc/PhotoGallery/Images"
                                    ImageManager-ViewMode="Grid"
                                    MediaManager-DeletePaths="~/ZeusInc/PhotoGallery/Media"
                                    MediaManager-MaxUploadFileSize="10240000"
                                    MediaManager-SearchPatterns="*.asf, *.asx, *.wma, *.wmv, *.avi, *.wav, *.mpeg, *.mpg, *.mpe, *.mov, *.mp3, *.m3u, *.mid, *.midi, *.rm, *.rma"
                                    MediaManager-ViewPaths="~/ZeusInc/PhotoGallery/Media"
                                    MediaManager-UploadPaths="~/ZeusInc/PhotoGallery/Media"
                                    TemplateManager-SearchPatterns="*.html,*.htm"
                                    ContentAreaMode="iframe"
                                    OnClientCommandExecuted="editorCommandExecuted"
                                    OnClientModeChange="OnClientModeChange"
                                    Content='<%# Bind("Descrizione") %>'
                                    ToolsFile="~/Zeus/PhotoGallery/RadEditor1.xml"
                                    LocalizationPath="~/App_GlobalResources"
                                    AllowScripts="true" RenderMode="Classic" ToolbarMode="Default" EnableViewState="False"
                                    Width="700px" Height="500px">
                                    <CssFiles>
                                        <telerik:EditorCssFile Value="~/asset/css/ZeusTypeFoundry.css" />
                                    </CssFiles>
                                </telerik:RadEditor>
                            </td>
                        </tr>
                    </asp:Panel>
                </table>
            </div>
            </asp:Panel>
          <asp:Panel ID="ZMCF_BoxImmagine1" runat="server">
                <dlc:ImageRaider ID="ImageRaider1" runat="server" ZeusIdModuloIndice="1" ZeusLangCode="ITA" IsEditMode="true"                    
                    OnDataBinding="ImageRaider_DataBinding"
                    BindPzFromDB="true" />
                <asp:HiddenField ID="FileNameBetaHiddenField" runat="server" Value='<%# Bind("Immagine1") %>' />
                <asp:HiddenField ID="Image1AltHiddenField" runat="server" Value='<%# Bind("Immagine1Alt") %>' />
                <asp:CustomValidator ID="FileUploadCustomValidator" runat="server" OnServerValidate="FileUploadCustomValidator_ServerValidate"
                    Display="Dynamic" SkinID="ZSSM_Validazione01" ValidationGroup="myValidation"
                    Visible="false"></asp:CustomValidator>
            </asp:Panel>
             <asp:Panel ID="ZMCF_BoxPlanner" runat="server">
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="Planner" runat="server" Text="Planner"></asp:Label></div>
                <table>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="ZML_PlannerGallery" SkinID="FieldDescription" runat="server"></asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:CheckBox ID="PW_Area1CheckBox" runat="server" Checked='<%# Bind("PW_Area1") %>' />
                            <asp:Label ID="AttivaDa_Label" runat="server" Text="Attiva pubblicazione dalla data"
                                SkinID="FieldValue"></asp:Label>
                            <asp:TextBox ID="PWI_Area1TextBox" runat="server" Text='<%# Bind("PWI_Area1", "{0:d}") %>'
                                Columns="10" MaxLength="10" OnDataBinding="PWI_Area1TextBox_DataBinding"></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator3"
                                runat="server" ControlToValidate="PWI_Area1TextBox" SkinID="ZSSM_Validazione01"
                                Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="RegularExpressionValidator2"
                                runat="server" ControlToValidate="PWI_Area1TextBox" SkinID="ZSSM_Validazione01"
                                Display="Dynamic" ErrorMessage="*" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$">Formato data richiesto: GG/MM/AAAA</asp:RegularExpressionValidator>
                            <asp:Label ID="AttivaA_Label" runat="server" Text="alla data" SkinID="FieldValue"></asp:Label>
                            <asp:TextBox ID="PWF_Area1TextBox" runat="server" Text='<%# Bind("PWF_Area1", "{0:d}") %>'
                                Columns="10" MaxLength="10" OnDataBinding="PWF_Area1TextBox_DataBinding"></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator4"
                                runat="server" ControlToValidate="PWF_Area1TextBox" SkinID="ZSSM_Validazione01"
                                Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                    ID="RegularExpressionValidator3" runat="server" ControlToValidate="PWF_Area1TextBox"
                                    SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="*" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$">Formato data richiesto: GG/MM/AAAA</asp:RegularExpressionValidator>
                            <asp:CompareValidator ValidationGroup="myValidation" ID="CompareValidator1" runat="server"
                                ControlToCompare="PWI_Area1TextBox" ControlToValidate="PWF_Area1TextBox" SkinID="ZSSM_Validazione01"
                                Display="Dynamic" ErrorMessage="La data iniziale deve essere precedente o uguale alla data finale"
                                Operator="GreaterThanEqual" Type="Date"></asp:CompareValidator>
                        </td>
                    </tr>
                </table>
            </div>
            </asp:Panel>
            <asp:HiddenField ID="HiddenFieldZeusId" runat="server" Value='<%# Bind("ZeusId") %>'
                OnDataBinding="CreazioneGUID" />
            <asp:HiddenField ID="HiddenFieldRecordNewUser" runat="server" Value='<%# Bind("RecordNewUser") %>'
                OnDataBinding="UtenteCreazione" />
            <asp:HiddenField ID="HiddenFieldRecordNewDate" runat="server" Value='<%# Bind("RecordNewDate") %>'
                OnDataBinding="DataOggi" />
            <div align="center">
                <dlc:mySummaryValidation ID="mySummaryValidation1" runat="server" SkinID="ZSSM_Validazione01" />
                <asp:LinkButton ID="InsertButton" runat="server" Text="Salva dati" CommandName="Insert"
                    SkinID="ZSSM_Button01" CausesValidation="true" ValidationGroup="myValidation"
                    OnClick="InsertButton_Click"></asp:LinkButton>
            </div>
        </InsertItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="dsPhotoGalleryNew" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        InsertCommand=" SET DATEFORMAT dmy; INSERT INTO tbPhotoGallery ( Titolo, ID2Categoria, DescBreve, Descrizione, Immagine1, Immagine1Alt, PW_Area1, PWI_Area1, PWF_Area1, ZeusLangCode,ZeusIdModulo, ZeusId,RecordNewUser,RecordNewDate) VALUES (@Titolo, @ID2Categoria, @DescBreve, @Descrizione, @Immagine1, @Immagine1Alt, @PW_Area1, @PWI_Area1, @PWF_Area1, @ZeusLangCode,@ZeusIdModulo, @ZeusId,@RecordNewUser,@RecordNewDate); SELECT @XRI = SCOPE_IDENTITY();"
        OnInserted="dsPhotoGalleryNew_Inserted">
        <InsertParameters>
            <asp:Parameter Name="Titolo" Type="String" />
            <asp:Parameter Name="ID2Categoria" Type="Int32" />
            <asp:Parameter Name="DescBreve" Type="String" />
            <asp:Parameter Name="Descrizione" Type="String" />
            <asp:Parameter Name="Immagine1" Type="String" />
            <asp:Parameter Name="Immagine1Alt" Type="String" />
            <asp:Parameter Name="PW_Area1" Type="Boolean" />
            <asp:Parameter Name="PWI_Area1" Type="DateTime" />
            <asp:Parameter Name="PWF_Area1" Type="DateTime" />
            <asp:Parameter Name="ZeusId" />
            <asp:Parameter Name="RecordNewUser" />
            <asp:Parameter Name="RecordNewDate" Type="DateTime" />
            <asp:QueryStringParameter Name="ZeusLangCode" QueryStringField="Lang" Type="String"
                DefaultValue="ITA" />
            <asp:QueryStringParameter Name="ZeusIdModulo" QueryStringField="ZIM" Type="String"
                DefaultValue="PHGFF" />
            <asp:Parameter Direction="Output" Name="XRI" Type="Int32" />
        </InsertParameters>
    </asp:SqlDataSource>
</asp:Content>
 