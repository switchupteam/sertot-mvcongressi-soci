<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
  
    static string TitoloPagina = "Photo gallery: gestione fotografie";
    delinea myDelinea = new delinea();
    static int UP = 10;
    static int DOWN = 11;


    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;

        if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZID"))
            || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM")))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        ZIM.Value = Server.HtmlEncode(Request.QueryString["ZIM"]);

        if (!ReadXML(ZIM.Value, GetLang()))
            Response.Redirect("~/Zeus/System/Message.aspx?1");

        ReadXML_Localization(ZIM.Value, GetLang());

        if (!Page.IsPostBack)
        {
            TitoloLabel.Text += getPhotoGalleryTitle(Server.HtmlEncode(Request.QueryString["ZID"]), "ITA");
            ID2PhotoGallery.Value = Server.HtmlEncode(Request.QueryString["ZID"]);
        }

        LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");
        InsertButton.CommandName = "Insert";

        InsertButton.Attributes.Add("onclick", "Validate()");


    }//fine Page_Load


    private string GetLang()
    {
        string Lang = "ITA";

        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;

    }//fine GetLang


    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_PhotoGallery.xml"));


            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {

                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {

                            
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                                else
                                {
                                    myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name + "Label");

                                    if (myLabel != null)
                                        myLabel.Text = childNode.InnerText;
                                }
                            

                        }
                        catch (Exception)
                        {

                        }
                    }//fine foreach
                }//fine foreach

            }//fine for


            //GridView1.Columns[6].HeaderText =
            //    mydoc.SelectSingleNode(myXPath + "ZML_Lang1").InnerText;

            //GridView1.Columns[3].HeaderText = mydoc.SelectSingleNode(myXPath + "ZML_Lang1Extended").InnerText;

            //GridView1.Columns[5].HeaderText = mydoc.SelectSingleNode(myXPath + "ZML_Lang2Extended").InnerText;
            //GridView1.Columns[4].HeaderText = mydoc.SelectSingleNode(myXPath + "ZML_Lang2").InnerText;

            //GridView1.Columns[7].HeaderText = mydoc.SelectSingleNode(myXPath + "ZML_Lang3").InnerText;
            //GridView1.Columns[8].HeaderText = mydoc.SelectSingleNode(myXPath + "ZML_Lang3Extended").InnerText;



        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine ReadXML_Localization



    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            string myXPathEach = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_PhotoGallery.xml"));


            Panel myPanel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPathEach);

            for (int i = 0; i < nodelist.Count; ++i)
            {

                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {

                            if (childNode.Name.IndexOf("ZMCF_") > -1)
                            {
                                myPanel = (Panel)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myPanel != null)
                                    myPanel.Visible = Convert.ToBoolean(childNode.InnerText);
                            }

                        }
                        catch (Exception)
                        {

                        }
                    }//fine foreach
                }//fine foreach

            }//fine for

            Panel PW_Lang2Panel = (Panel)FormView1.FindControl("PW_Lang2Panel");

            PW_Lang2Panel.Visible =
            GridView1.Columns[5].Visible =
            GridView1.Columns[8].Visible =
            Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Lang2").InnerText);

            Panel PW_Lang3Panel = (Panel)FormView1.FindControl("PW_Lang3Panel");
            PW_Lang3Panel.Visible =
            GridView1.Columns[6].Visible =
            GridView1.Columns[9].Visible =
            Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Lang3").InnerText);

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }

    }//fine ReadXML





    protected void UtenteCreazione(object sender, EventArgs e)
    {
        HiddenField UtenteCreazione = (HiddenField)sender;

        if (UtenteCreazione.Value.Length == 0)
            UtenteCreazione.Value = Membership.GetUser().ProviderUserKey.ToString();

    }//fine UtenteCreazione

    protected void DataOggi(object sender, EventArgs e)
    {
        HiddenField DataCreazione = (HiddenField)sender;
        if (DataCreazione.Value.Length == 0)
        {
            DataCreazione.Value = DateTime.Now.ToString("d");
        }
    }

    protected void DataLimite(object sender, EventArgs e)
    {
        TextBox DataLimite = (TextBox)sender;
        if (DataLimite.Text == string.Empty)
        {
            DataLimite.Text = "31/12/2040";
        }
    }



    private string getPhotoGalleryTitle(string ZeudID, string ZeusLangCode)
    {
        try
        {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            string sqlQuery = "SELECT Titolo FROM tbPhotoGallery WHERE ZeusID='" + ZeudID + "' AND ZeusLangCode='" + ZeusLangCode + "'";

            string Titolo = string.Empty;
            using (SqlConnection conn = new SqlConnection(strConnessione))
            {
                SqlCommand command = new SqlCommand(sqlQuery, conn);


                conn.Open();
                SqlDataReader reader = command.ExecuteReader();
                /*fino a quando ci sono record*/
                while (reader.Read())
                {
                    Titolo = reader.GetString(0);
                }
                reader.Close();
                conn.Close();
            }
            return Titolo;
        }
        catch (Exception e)
        {
            Response.Write(e.ToString());
            return string.Empty;
        }

    }//fine getPhotoGalleryTitle




    //##############################################################################################################
    //################################################ ORDINAMENTO #################################################
    //##############################################################################################################

    //FUNZIONI PER L'ORDINAMENTO  

    private bool Move(string MoveCommand, string Ordinamento, string ID2PhotoGallery, string ID1PhotoNodeToMove)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;


        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            SqlTransaction transaction = null;


            try
            {

                int intNewOrder = Convert.ToInt32(Ordinamento);
                int intOldOrder = intNewOrder;

                switch (MoveCommand)
                {
                    case "Up":
                        --intNewOrder;
                        break;

                    case "Down":
                        ++intNewOrder;
                        break;

                    default:
                        return false;
                        break;

                }//fine switch


                connection.Open();

                SqlCommand command = connection.CreateCommand();

                transaction = connection.BeginTransaction();
                command.Transaction = transaction;

                sqlQuery = "SELECT ID1Photo FROM tbPhotoGallery_Photo WHERE ID2PhotoGallery=@ID2PhotoGallery AND ZeusIsAlive=1 AND Ordinamento=@Ordinamento";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID2PhotoGallery", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@ID2PhotoGallery"].Value = new Guid(ID2PhotoGallery);

                command.Parameters.Add("@Ordinamento", System.Data.SqlDbType.NVarChar, 4);
                command.Parameters["@Ordinamento"].Value = intNewOrder.ToString();

                SqlDataReader reader = command.ExecuteReader();

                string ID1PhotoNodeToSwitch = string.Empty;

                while (reader.Read())
                {
                    ID1PhotoNodeToSwitch = reader.GetInt32(0).ToString();

                }//fine while

                reader.Close();



                if (ID1PhotoNodeToSwitch.Equals(ID1PhotoNodeToMove))
                {
                    transaction.Rollback();
                    return false;
                }


                sqlQuery = "UPDATE tbPhotoGallery_Photo SET Ordinamento=@Ordinamento WHERE ID1Photo=@ID1Photo";
                command.CommandText = sqlQuery;
                command.Parameters.Clear();

                command.Parameters.Add("@Ordinamento", System.Data.SqlDbType.NVarChar, 4);
                command.Parameters["@Ordinamento"].Value = intNewOrder.ToString();

                command.Parameters.Add("@ID1Photo", System.Data.SqlDbType.Int, 4);
                command.Parameters["@ID1Photo"].Value = ID1PhotoNodeToMove;

                command.ExecuteNonQuery();


                sqlQuery = "UPDATE tbPhotoGallery_Photo SET Ordinamento=@Ordinamento WHERE ID1Photo=@ID1Photo";
                command.CommandText = sqlQuery;
                command.Parameters.Clear();

                command.Parameters.Add("@Ordinamento", System.Data.SqlDbType.NVarChar, 4);
                command.Parameters["@Ordinamento"].Value = intOldOrder.ToString();

                command.Parameters.Add("@ID1Photo", System.Data.SqlDbType.Int, 4);
                command.Parameters["@ID1Photo"].Value = ID1PhotoNodeToSwitch;

                command.ExecuteNonQuery();
                transaction.Commit();
                return true;

            }
            catch (Exception p)
            {

                Response.Write(p.ToString());
                transaction.Rollback();
                return false;
            }



        }//fine Using

    }//fine Move


    private int getLastOrder(string ID2PhotoGallery)
    {

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT MAX(Ordinamento) FROM tbPhotoGallery_Photo WHERE ID2PhotoGallery=@ID2PhotoGallery AND ZeusIsAlive=1 ";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID2PhotoGallery", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@ID2PhotoGallery"].Value = new Guid(ID2PhotoGallery);


                SqlDataReader reader = command.ExecuteReader();

                int myReturn = -1;

                while (reader.Read())
                {
                    myReturn = reader.GetInt32(0);

                }//fine while

                reader.Close();
                return myReturn;
            }
            catch (Exception p)
            {

                Response.Write(p.ToString());
                return -1;
            }
        }//fine Using

    }//fine getLastOrder


    private int getOrdinamento(string ID2PhotoGallery)
    {

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();



                sqlQuery = "SELECT COUNT(ID1Photo) FROM tbPhotoGallery_Photo WHERE ID2PhotoGallery=@ID2PhotoGallery";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID2PhotoGallery", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@ID2PhotoGallery"].Value = new Guid(ID2PhotoGallery);


                int myCount = Convert.ToInt32(command.ExecuteScalar());
                ++myCount;

                if (myCount != null)
                    return myCount;
                else return 1;

            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
                return -1;
            }
        }//fine Using
    }//fine getOrdinamento


    private bool CheckIntegrityR(string ID2PhotoGallery)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;
        SqlDataReader reader = null;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            SqlTransaction transaction = null;

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                transaction = connection.BeginTransaction();
                command.Transaction = transaction;


                string[][] myMenuChange = null;
                int myOrder = 1;
                ArrayList myArray = new ArrayList();


                sqlQuery = "SELECT ID1Photo,Ordinamento FROM tbPhotoGallery_Photo WHERE  ID2PhotoGallery=@ID2PhotoGallery  ORDER BY Ordinamento,ID1Photo";
                command.CommandText = sqlQuery;
                command.Parameters.Clear();

                command.Parameters.Add("@ID2PhotoGallery", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@ID2PhotoGallery"].Value = new Guid(ID2PhotoGallery);

                reader = command.ExecuteReader();

                myOrder = 1;

                while (reader.Read())
                {

                    if (myOrder != reader.GetInt32(1))
                    {

                        string[] myData = new string[2];
                        myData[0] = reader.GetInt32(0).ToString();
                        myData[1] = myOrder.ToString();
                        myArray.Add(myData);
                    }

                    ++myOrder;
                }//fine while

                reader.Close();




                myMenuChange = (string[][])myArray.ToArray(typeof(string[]));
                myArray = new ArrayList();

                for (int i = 0; i < myMenuChange.Length; ++i)
                {

                    sqlQuery = "UPDATE tbPhotoGallery_Photo SET Ordinamento=@Ordinamento WHERE ID1Photo=@ID1Photo";
                    command.CommandText = sqlQuery;
                    command.Parameters.Clear();

                    command.Parameters.Add("@Ordinamento", System.Data.SqlDbType.NVarChar, 4);
                    command.Parameters["@Ordinamento"].Value = myMenuChange[i][1];

                    command.Parameters.Add("@ID1Photo", System.Data.SqlDbType.Int, 4);
                    command.Parameters["@ID1Photo"].Value = myMenuChange[i][0];

                    command.ExecuteNonQuery();


                }//fine for





                transaction.Commit();

                return true;
            }
            catch (Exception p)
            {

                Response.Write(p.ToString());


                if (reader != null)
                    reader.Close();

                if (transaction != null)
                    transaction.Rollback();

                return false;
            }
        }//fine using
    }//fine CheckIntegrityR

    //FINE FUNZIONI PER ORDINAMENTO  

    //##############################################################################################################
    //############################################# FINE ORDINAMENTO ###############################################
    //############################################################################################################## 



    //FUNZIONI PER LA GRIDVIEW CHE ULIZZANO L'ORDINAMENTO
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {

            int index = Convert.ToInt32(e.CommandArgument);

            GridViewRow row = GridView1.Rows[index];
            Label ID1Photo = (Label)row.FindControl("ID1Photo");
            Label ID2PhotoGallery = (Label)row.FindControl("ID2PhotoGallery");
            Label Ordinamento = (Label)row.FindControl("Ordinamento");

            Move(e.CommandName.ToString(), Ordinamento.Text, ID2PhotoGallery.Text, ID1Photo.Text);


        }
        catch (Exception p)
        {
            //Response.Write(p.ToString());

        }

        GridView1.DataBind();



    }//fine GridView1_RowCommand


    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        try
        {
            if (e.Row.DataItemIndex > -1)
            {

                HiddenField Immagine2HiddenField = (HiddenField)e.Row.FindControl("Immagine2HiddenField");
                Image Immagine1Image = (Image)e.Row.FindControl("Immagine1Image");

                Immagine1Image.ImageUrl = "~/ZeusInc/PhotoGallery/Img2Photo/" + Immagine2HiddenField.Value.ToString();



                Image PW_Lang1Image = (Image)e.Row.FindControl("PW_Lang1Image");
                Image PW_Lang2Image = (Image)e.Row.FindControl("PW_Lang2Image");
                Image PW_Lang3Image = (Image)e.Row.FindControl("PW_Lang3Image");




                bool PW_Lang1 = Convert.ToBoolean(PW_Lang1Image.DescriptionUrl.ToString());
                bool PW_Lang2 = Convert.ToBoolean(PW_Lang2Image.DescriptionUrl.ToString());
                bool PW_Lang3 = Convert.ToBoolean(PW_Lang3Image.DescriptionUrl.ToString());

                if (PW_Lang1)
                    PW_Lang1Image.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";

                if (PW_Lang2)
                    PW_Lang2Image.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";

                if (PW_Lang3)
                    PW_Lang3Image.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";




                //ORDINAMENTO


                Label Ordinamento = (Label)e.Row.FindControl("Ordinamento");
                Label ID2PhotoGallery = (Label)e.Row.FindControl("ID2PhotoGallery");
                int ordinamentoRow = Convert.ToInt32(Ordinamento.Text);

                int maxordine = getLastOrder(ID2PhotoGallery.Text);

                if (ordinamentoRow == 1)
                    e.Row.Cells[UP].Text = string.Empty;

                if (ordinamentoRow == maxordine)
                    e.Row.Cells[DOWN].Text = string.Empty;


                if (e.Row.Cells[UP].Controls.Count > 0)
                {
                    e.Row.Cells[UP].Attributes.Add("onmouseover", "showImageUP('" + e.Row.Cells[UP].Controls[0].ClientID + "','2');");
                    e.Row.Cells[UP].Attributes.Add("onmouseout", "showImageUP('" + e.Row.Cells[UP].Controls[0].ClientID + "','1');");
                }

                if (e.Row.Cells[DOWN].Controls.Count > 0)
                {
                    e.Row.Cells[DOWN].Attributes.Add("onmouseover", "showImageDOWN('" + e.Row.Cells[DOWN].Controls[0].ClientID + "','2');");
                    e.Row.Cells[DOWN].Attributes.Add("onmouseout", "showImageDOWN('" + e.Row.Cells[DOWN].Controls[0].ClientID + "','1');");
                }



            }//fine if

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }//fine GridView1_RowDataBound


    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        if (GridView1.Rows.Count == 0)
            GridView1.Visible = false;


    }//FINE GridView1_DataBound


    protected void HiddenImageFile_DataBinding(object sender, EventArgs e)
    {

        HiddenField HiddenImageFile = (HiddenField)sender;

        if (HiddenImageFile.Value.Length == 0)
            HiddenImageFile.Value = "ImgNonDisponibile.jpg";

    }//fine HiddenImageFile_DataBinding



    //##############################################################################################################
    //################################################ FILE UPLOAD #################################################
    //############################################################################################################## 

    protected void FileUploadCustomValidator_ServerValidate(object sender, ServerValidateEventArgs args)
    {
        try
        {
            ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");
            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");

            if (ImageRaider1.isValid())
                InsertButton.CommandName = "Insert";
            else
                InsertButton.CommandName = string.Empty;




        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine FileUploadCustomValidator
    //##############################################################################################################
    //########################################### FINE FILE UPLOAD #################################################
    //##############################################################################################################


    protected void InsertButton_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");
            HiddenField FileNameBetaHiddenField = (HiddenField)FormView1.FindControl("FileNameBetaHiddenField");
            HiddenField FileNameGammaHiddenField = (HiddenField)FormView1.FindControl("FileNameGammaHiddenField");
            HiddenField Image12AltHiddenField = (HiddenField)FormView1.FindControl("Image12AltHiddenField");
            ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");

            if (InsertButton.CommandName != string.Empty)
            {
 if (ImageRaider1.GenerateBeta(string.Empty))
			FileNameBetaHiddenField.Value = ImageRaider1.ImgBeta_FileName;
		else FileNameBetaHiddenField.Value = ImageRaider1.DefaultBetaImage;
            
            if (ImageRaider1.GenerateGamma(string.Empty))
			FileNameGammaHiddenField.Value = ImageRaider1.ImgGamma_FileName;
		else FileNameGammaHiddenField.Value = ImageRaider1.DefaultGammaImage;



                HiddenField Ordinamento = (HiddenField)FormView1.FindControl("Ordinamento");

                if (Ordinamento != null)
                    Ordinamento.Value = getOrdinamento(ID2PhotoGallery.Value).ToString();

            }

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine InsertButton_Click






    protected void EliminaLinkButton_Click(object sender, EventArgs e)
    {
        LinkButton EliminaLinkButton = (LinkButton)sender;

        if ((DeletePhoto(EliminaLinkButton.ToolTip))
            && (CheckIntegrityR(ID2PhotoGallery.Value)))
            Response.Redirect("~/Zeus/PhotoGallery/Gallery_Fotografie.aspx?ZID=" + Server.HtmlEncode(Request.QueryString["ZID"]) + "&Lang=" + GetLang() + "&ZIM=" + Server.HtmlEncode(Request.QueryString["ZIM"]));
        else Response.Write("~/Zeus/System/Message.aspx?DeleteErr");

    }//fine EliminaLinkButton_Click

    private bool DeletePhoto(string ID1Photo)
    {
        try
        {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            string sqlQuery = "DELETE FROM tbPhotoGallery_Photo  WHERE ID1Photo=@ID1Photo";

            using (SqlConnection conn = new SqlConnection(strConnessione))
            {
                SqlCommand command = new SqlCommand(sqlQuery, conn);
                conn.Open();

                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID1Photo", System.Data.SqlDbType.Int);
                command.Parameters["@ID1Photo"].Value = ID1Photo;
                command.ExecuteNonQuery();

                return true;

            }//fine using

        }
        catch (Exception e)
        {
            Response.Write(e.ToString());
            return false;
        }


    }//fine DeletePhoto


    protected void tbPhotoGallery_Photos_Insert_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.Exception == null)
            Response.Redirect("~/Zeus/PhotoGallery/Gallery_Fotografie.aspx?ZID="
                + Server.HtmlEncode(Request.QueryString["ZID"])
                + "&Lang=" + GetLang() + "&ZIM="
                + ZIM.Value);
        else Response.Redirect("~/Zeus/System/Message.aspx?InsErr");
    }
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="ID2PhotoGallery" runat="server" />
    <asp:HiddenField ID="ZIM" runat="server" />
    
    <script language="javascript" type="text/javascript">
        function showImageUP(imgId, typ) {
            var objImg = document.getElementById(imgId);
            if (objImg) {
                if (typ == "1")
                    objImg.src = "../SiteImg/Bt3_ArrowUp_Off.gif";
                else if (typ == "2")
                    objImg.src = "../SiteImg/Bt3_ArrowUp_On.gif";
            }
        }

        function showImageDOWN(imgId, typ) {
            var objImg = document.getElementById(imgId);
            if (objImg) {
                if (typ == "1")
                    objImg.src = "../SiteImg/Bt3_ArrowDown_Off.gif";
                else if (typ == "2")
                    objImg.src = "../SiteImg/Bt3_ArrowDown_On.gif";
            }
        }
    </script>
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <div class="LayGridTitolo1">
        <asp:Label ID="TitoloLabel" SkinID="GridTitolo1" runat="server" Text="Galleria: "></asp:Label>
    </div>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AutoGenerateColumns="False"
        AllowSorting="false" DataSourceID="tbPhotoGallery_Photos" OnRowDataBound="GridView1_RowDataBound"
        OnDataBound="GridView1_DataBound" OnRowCommand="GridView1_RowCommand">
        <Columns>
            <asp:TemplateField Visible="false">
                <ItemTemplate>
                    <asp:HiddenField ID="Immagine1HiddenField" runat="server" Value='<%# Eval("Immagine1") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField Visible="false">
                <ItemTemplate>
                    <asp:HiddenField ID="Immagine2HiddenField" runat="server" Value='<%# Eval("Immagine2") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="#"  
             ItemStyle-Width="15" ItemStyle-HorizontalAlign="Center" >
                <ItemTemplate>
                    <asp:Label ID="Ordinamento" runat="server" Text='<%# Eval("Ordinamento") %>' Width="10"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-Width="90">
                <ItemTemplate>
                    <asp:Image ID="Immagine1Image" runat="server" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:BoundField DataField="Didascalia1" HeaderText="Didascalia ITA" SortExpression="Didascalia1" />
            <asp:BoundField DataField="Didascalia2" HeaderText="Didascalia ENG" SortExpression="Didascalia2" />
            <asp:BoundField DataField="Didascalia3" HeaderText="Didascalia FRA" SortExpression="Didascalia3" />
            <asp:TemplateField HeaderText="ITA" SortExpression="PW_Lang1" ItemStyle-Width="30">
                <ItemTemplate>
                    <asp:Image ID="PW_Lang1Image" runat="server" DescriptionUrl='<%# Eval("PW_Lang1") %>'
                        ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ENG" SortExpression="PW_Lang2" ItemStyle-Width="30">
                <ItemTemplate>
                    <asp:Image ID="PW_Lang2Image" runat="server" DescriptionUrl='<%# Eval("PW_Lang2") %>'
                        ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="FRA" SortExpression="PW_Lang3" ItemStyle-Width="30">
                <ItemTemplate>
                    <asp:Image ID="PW_Lang3Image" runat="server" DescriptionUrl='<%# Eval("PW_Lang3") %>'
                        ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:ButtonField CommandName="Up" HeaderText="Ord." ShowHeader="True" ImageUrl="~/Zeus/SiteImg/Bt3_ArrowUp_Off.gif"
                ButtonType="Image" ControlStyle-Width="16" ControlStyle-Height="16" ControlStyle-BorderWidth="0">
                <ItemStyle HorizontalAlign="Center" />
                <ControlStyle CssClass="GridView_Ord1" />
            </asp:ButtonField>
            <asp:ButtonField CommandName="Down" HeaderText="Ord." ShowHeader="True" ButtonType="Image"
                ImageUrl="~/Zeus/SiteImg/Bt3_ArrowDown_Off.gif" ControlStyle-Width="16" ControlStyle-Height="16"
                ControlStyle-BorderWidth="0">
                <ItemStyle HorizontalAlign="Center" />
                <ControlStyle CssClass="GridView_Ord1" />
            </asp:ButtonField>
            <asp:HyperLinkField DataNavigateUrlFields="ID1Photo,ID2PhotoGallery,ZeusLangCode,ZeusIdModulo"
                DataNavigateUrlFormatString="/Zeus/PhotoGallery/Gallery_Fotografie_Edt.aspx?XRI={0}&ZID={1}&Lang={2}&ZIM={3}"
                HeaderText="Fotografia" Text="Modifica" ItemStyle-Width="80">
                <ControlStyle CssClass="GridView_Button1" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:HyperLinkField>
            <asp:TemplateField HeaderText="Elimina">
                <ItemTemplate>
                    <asp:LinkButton ID="EliminaLinkButton" runat="server" Text="Elimina" ToolTip='<%# Eval("ID1Photo") %>'
                        OnClick="EliminaLinkButton_Click" CausesValidation="false"></asp:LinkButton>
                </ItemTemplate>
                <ControlStyle CssClass="GridView_Button1" />
                <ItemStyle HorizontalAlign="Center" Width="80" />
            </asp:TemplateField>
            <asp:TemplateField Visible="False">
                <ItemTemplate>
                    <asp:Label ID="ID1Photo" runat="server" Text='<%# Eval("ID1Photo") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField Visible="False">
                <ItemTemplate>
                    <asp:Label ID="ID2PhotoGallery" runat="server" Text='<%# Eval("ID2PhotoGallery") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="tbPhotoGallery_Photos" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SELECT [Titolo],[ID1Photo],[ID2PhotoGallery], Ordinamento,
        [Immagine1], [Immagine2], [Didascalia1], [Didascalia2],[Didascalia3], 
        [PW_Lang1], [PW_Lang2],[PW_Lang3],[ZeusLangCode],[ZeusIdModulo] 
        FROM [vwPhotoGallery_Photo_All] 
        WHERE ID2PhotoGallery=@ID2PhotoGallery AND ZeusLangCode=@ZeusLangCode
         ORDER BY Ordinamento">
        <SelectParameters>
            <asp:QueryStringParameter Name="ID2PhotoGallery" QueryStringField="ZID" />
            <asp:QueryStringParameter Name="ZeusLangCode" QueryStringField="Lang" DefaultValue="ITA" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
    <asp:FormView ID="FormView1" runat="server" DataKeyNames="ID1Photo" DataSourceID="tbPhotoGallery_Photos_Insert"
        DefaultMode="Insert" Width="100%">
        <InsertItemTemplate>
            <asp:HiddenField ID="Ordinamento" runat="server" Value='<%# Bind("Ordinamento") %>' />
            <asp:Panel ID="AnteprimaPanel" runat="server">
                <dlc:ImageRaider ID="ImageRaider1" runat="server" ZeusIdModuloIndice="2" ZeusLangCode="ITA"
                    BindPzFromDB="true" Required="true" RequiredErrorMessage="Obbligatorio" Vgroup="myValidation" />
                <asp:HiddenField ID="FileNameBetaHiddenField" runat="server" Value='<%# Bind("Immagine1") %>' />
                <asp:HiddenField ID="FileNameGammaHiddenField" runat="server" Value='<%# Bind("Immagine2") %>' />
                <asp:HiddenField ID="Image12AltHiddenField" runat="server" Value='<%# Bind("Immagine12Alt") %>' />
                <asp:CustomValidator ID="FileUploadCustomValidator" runat="server" OnServerValidate="FileUploadCustomValidator_ServerValidate"
                    Display="Dynamic" SkinID="ZSSM_Validazione01" ValidationGroup="myValidation"
                    Visible="false"></asp:CustomValidator>
            </asp:Panel>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="IdentificativoLabel" runat="server">
                    Dettaglio nuova fotografia
                    </asp:Label>
                </div>
                <table>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="ZML_Lang1" runat="server" SkinID="FieldDescription">Didascalia ITA
                            </asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <CustomWebControls:TextArea ID="DidascaliaITATextArea" runat="server" Text='<%# Bind("Didascalia1") %>'
                                MaxLength="198" Columns="100">
                            &nbsp;
                            </CustomWebControls:TextArea>
                        </td>
                    </tr>
                    <asp:Panel ID="ZMCF_Lang2" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_Lang2" runat="server" SkinID="FieldDescription">Didascalia ENG
                                </asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <CustomWebControls:TextArea ID="DidascaliaENGTextArea" runat="server" Text='<%# Bind("Didascalia2") %>'
                                    MaxLength="198" Columns="100">
                            &nbsp;
                                </CustomWebControls:TextArea>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_Lang3" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_Lang3" runat="server" SkinID="FieldDescription">Didascalia FRA
                                </asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <CustomWebControls:TextArea ID="DidascaliaFRATextArea" runat="server" Text='<%# Bind("Didascalia3") %>'
                                    MaxLength="198" Columns="100">
                                &nbsp;
                                </CustomWebControls:TextArea>
                            </td>
                        </tr>
                    </asp:Panel>
                </table>
            </div>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="Planner" runat="server"> Planner
                    </asp:Label>
                </div>
                <table>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="PZL_PWArea1_NewLabel" SkinID="FieldDescription" runat="server">Pubblica</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="DidascaliaITACheckBox" runat="server" Checked='<%# Bind("PW_Lang1") %>' />
                                        <asp:Label ID="ZML_Lang1Extended" runat="server" Text="Italiano" SkinID="FieldValue"></asp:Label>
                                    </td>
                                    <td width="30px">
                                        <img src="../SiteImg/Spc.gif" width="30px" height="1px" />
                                    </td>
                                    <asp:Panel ID="PW_Lang2Panel" runat="server">
                                        <td>
                                            <asp:CheckBox ID="DidascaliaENGCheckBox" runat="server" Checked='<%# Bind("PW_Lang2") %>' />
                                            <asp:Label ID="ZML_Lang2Extended" runat="server" Text="Inglese" SkinID="FieldValue"></asp:Label>
                                        </td>
                                        <td width="30px">
                                            <img src="../SiteImg/Spc.gif" width="30px" height="1px" />
                                        </td>
                                    </asp:Panel>
                                    <asp:Panel ID="PW_Lang3Panel" runat="server">
                                        <td>
                                            <asp:CheckBox ID="DidascaliaFRACheckBox" runat="server" Checked='<%# Bind("PW_Lang3") %>' />
                                            <asp:Label ID="ZML_Lang3Extended" runat="server" Text="Francese" SkinID="FieldValue"></asp:Label>
                                        </td>
                                    </asp:Panel>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <asp:HiddenField ID="HiddenFieldRecordNewUser" runat="server" Value='<%# Bind("RecordNewUser") %>'
                OnDataBinding="UtenteCreazione" />
            <asp:HiddenField ID="HiddenFieldRecordNewDate" runat="server" Value='<%# Bind("RecordNewDate") %>'
                OnDataBinding="DataOggi" />
            <div align="center">
                <dlc:mySummaryValidation ID="mySummaryValidation1" runat="server" SkinID="ZSSM_Validazione01" />
                <asp:LinkButton ID="InsertButton" runat="server" Text="Salva dati" CommandName="Insert"
                    SkinID="ZSSM_Button01" OnClick="InsertButton_Click" ValidationGroup="myValidation"></asp:LinkButton>
            </div>
        </InsertItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="tbPhotoGallery_Photos_Insert" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        InsertCommand=" SET DATEFORMAT dmy; 
        INSERT INTO [tbPhotoGallery_Photo] 
        ([ID2PhotoGallery], [Immagine1], [Immagine2], Immagine12Alt, [Didascalia1], [Didascalia2], [Didascalia3], 
        [PW_Lang1], [PW_Lang2], [PW_Lang3],[ZeusIsAlive], 
        [RecordNewUser], [RecordNewDate], [RecordEdtUser], [RecordEdtDate],[Ordinamento],[ZeusIdModulo],[ZeusLangCode]) 
        VALUES (@ID2PhotoGallery, @Immagine1, @Immagine2, @Immagine12Alt, @Didascalia1, @Didascalia2, @Didascalia3, 
        @PW_Lang1, @PW_Lang2, @PW_Lang3,@ZeusIsAlive, 
        @RecordNewUser, @RecordNewDate, @RecordEdtUser, @RecordEdtDate,@Ordinamento,@ZeusIdModulo,@ZeusLangCode)"
        OnInserted="tbPhotoGallery_Photos_Insert_Inserted">
        <InsertParameters>
            <asp:Parameter Name="Immagine1" Type="String" />
            <asp:Parameter Name="Immagine2" Type="String" />
            <asp:Parameter Name="Immagine12Alt" Type="String" />
            <asp:Parameter Name="Didascalia1" Type="String" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="Didascalia2" Type="String" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="Didascalia3" Type="String" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="PW_Lang1" Type="Boolean" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="PW_Lang2" Type="Boolean" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="PW_Lang3" Type="Boolean" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="ZeusIsAlive" Type="Boolean" DefaultValue="True" />
            <asp:Parameter Name="RecordNewUser" />
            <asp:Parameter Name="RecordNewDate" Type="DateTime" />
            <asp:Parameter Name="RecordEdtUser" />
            <asp:Parameter Name="RecordEdtDate" Type="DateTime" />
            <asp:Parameter Name="Ordinamento" Type="Int32" />
            <asp:QueryStringParameter Name="ID2PhotoGallery" QueryStringField="ZID" />
            <asp:QueryStringParameter Name="ZeusIdModulo" QueryStringField="ZIM" />
            <asp:QueryStringParameter Name="ZeusLangCode" QueryStringField="Lang" DefaultValue="ITA" />
        </InsertParameters>
    </asp:SqlDataSource>
</asp:Content>
