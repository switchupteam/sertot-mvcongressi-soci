﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
    
    static string TitoloPagina = "Associazione galleria fotografica esistente";



    protected void Page_Init()
    {
        if ((Request.ServerVariables["HTTP_REFERER"] != null)
                  && (Request.ServerVariables["HTTP_REFERER"].Length > 0))
            HTTPRefer.Value = Request.ServerVariables["HTTP_REFERER"];
    }

    protected void Page_Load(object sender, EventArgs e)
    {



        delinea myDelinea = new delinea();

        if (!Page.IsPostBack)
            TitleField.Value = TitoloPagina;

        if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
           || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI"))
           || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM")))
            Response.Redirect("~/Zeus/System/Message.aspx?0");


        SetFocus(SearchButton.UniqueID);

    }//fine Page_Load


    private void SetFocus(string ID)
    {
        Page.Form.DefaultButton = ID;
        Page.Form.DefaultFocus = ID;

    }//fine SetFocus


    protected void PWF(object sender, EventArgs e)
    {
        TextBox DataLimite = (TextBox)sender;
        if (DataLimite.Text.Length == 0)
            DataLimite.Text = DateTime.Now.ToString("d");

    }






    private string GetLang()
    {
        try
        {
            delinea myDelinea = new delinea();
            string Lang = "ITA";

            if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
            {
                if (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang"))
                    Lang = Request.QueryString["Lang"];
                else Response.Redirect("~/System/Message.aspx");
            }

            return Lang;
        }
        catch (Exception)
        {
            return "ITA";
        }
    }//fine GetLang






    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.DataItemIndex > -1)
            {
                Image AttivoImage = (Image)e.Row.FindControl("AttivoImage");
                HiddenField AttivoArea1HiddenField = (HiddenField)e.Row.FindControl("AttivoArea1HiddenField");

                int intAttivoArea1 = Convert.ToInt32(AttivoArea1HiddenField.Value.ToString());

                if (intAttivoArea1 == 1)
                    AttivoImage.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";

            }//fine if
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine GridView1_RowDataBound






    protected void SearchButton_Click(object sender, EventArgs e)
    {


        //PZD_ZIMAssociation
        bool find = false;


        if (TitoloTextBox.Text.Length > 0)
        {
            if (!find)
                vwPhotoGallery_AllSqlDataSource.SelectCommand += " WHERE";


            vwPhotoGallery_AllSqlDataSource.SelectCommand += " Titolo LIKE '%" + TitoloTextBox.Text + "%'";
            find = true;

        }





        if (NewDateFirstTextBox.Text.Length <= 0)
            NewDateFirstTextBox.Text = "01/01/2000";

        if (NewDateLastTextBox.Text.Length <= 0)
            NewDateLastTextBox.Text = "01/01/2050";

        if (!find)
            vwPhotoGallery_AllSqlDataSource.SelectCommand += " WHERE";
        else vwPhotoGallery_AllSqlDataSource.SelectCommand += " AND";

        vwPhotoGallery_AllSqlDataSource.SelectCommand += "  (CONVERT(datetime, CONVERT(varchar, RecordNewDate, 103)) BETWEEN '"
            + Server.HtmlEncode(NewDateFirstTextBox.Text)
            + "' AND '" + Server.HtmlEncode(NewDateLastTextBox.Text) + "')";



        if (EdtDateFirstTextBox.Text.Length <= 0)
            EdtDateFirstTextBox.Text = "01/01/2000";

        if (EdtDateLastTextBox.Text.Length <= 0)
            EdtDateLastTextBox.Text = "01/01/2050";


        vwPhotoGallery_AllSqlDataSource.SelectCommand += " AND";

        vwPhotoGallery_AllSqlDataSource.SelectCommand += "  (CONVERT(datetime, CONVERT(varchar, RecordNewDate, 103)) BETWEEN '"
            + Server.HtmlEncode(EdtDateFirstTextBox.Text)
            + "' AND '" + Server.HtmlEncode(EdtDateLastTextBox.Text) + "')";


        EdtDateFirstTextBox.Text = NewDateFirstTextBox.Text = string.Empty;
        EdtDateLastTextBox.Text = NewDateLastTextBox.Text = DateTime.Now.ToString("d");


        //vwPhotoGallery_AllSqlDataSource.SelectCommand += " AND PZD_ZIMAssociation LIKE '%" + Server.HtmlEncode(Request.QueryString["ZIM"]) + "%'";
        //Response.Write("DEB " + vwPhotoGallery_AllSqlDataSource.SelectCommand + "<br />");

        ElencoPanel.Visible = true;
        GridView2.DataSourceID = "vwPhotoGallery_AllSqlDataSource";
        GridView2.DataBind();


    }//fine SearchButton_Click



    private int getLastOrder()
    {

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT MAX(Ordinamento)";
                sqlQuery += " FROM vwPhotoGallery_Associazioni ";
                sqlQuery += " WHERE ID2Function=@ID2Function AND ZeusLangCode=@ZeusLangCode  AND ZIMFunction=@ZIMFunction";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID2Function", System.Data.SqlDbType.Int);
                command.Parameters["@ID2Function"].Value = Server.HtmlEncode(Request.QueryString["XRI"]);

                command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ZeusLangCode"].Value = Server.HtmlEncode(Request.QueryString["Lang"]);

                command.Parameters.Add("@ZIMFunction", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ZIMFunction"].Value = Server.HtmlEncode(Request.QueryString["ZIM"]);

                SqlDataReader reader = command.ExecuteReader();

                int myReturn = 1;

                while (reader.Read())
                    if (!reader.IsDBNull(0))
                        myReturn += reader.GetInt32(0);


                reader.Close();
                return myReturn;
            }
            catch (Exception p)
            {

                Response.Write(p.ToString());
                return -1;
            }
        }//fine Using

    }//fine getLastOrder

    private bool Associa(string ID2PhotoGallery)
    {
        try
        {

            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(
               strConnessione))
            {


                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SET DATEFORMAT dmy; INSERT INTO [tbxPhotoGallery_Associazioni]";
                sqlQuery += "  ([ID2PhotoGallery]";
                sqlQuery += "  ,[ZIMFunction]";
                sqlQuery += "  ,[ID2Function]";
                sqlQuery += "  ,[Ordinamento]";
                sqlQuery += "  ,[PW_Area1]";
                sqlQuery += "  ,[ZeusLangCode])";
                sqlQuery += "  VALUES ";
                sqlQuery += "  ( ";
                sqlQuery += Server.HtmlEncode(ID2PhotoGallery);
                sqlQuery += " ,'" + Server.HtmlEncode(Request.QueryString["ZIM"]) + "' ";
                sqlQuery += "," + Server.HtmlEncode(Request.QueryString["XRI"]);
                sqlQuery += "," + getLastOrder();
                sqlQuery += " ,1";
                sqlQuery += " ,'" + Server.HtmlEncode(Request.QueryString["Lang"]) + "' ) ";
                command.CommandText = sqlQuery;
                //Response.Write("DEB " + sqlQuery + "<br />");

                command.ExecuteNonQuery();

                GridView2.DataBind();
                ElencoPanel.Visible = true;


            }//fine Using
            return true;

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }


    }//fine Associa

    protected void AssociaLinkButton_Click(object sender, EventArgs e)
    {
        LinkButton AssociaLinkButton = (LinkButton)sender;

        if (Associa(AssociaLinkButton.ToolTip))
            if (HTTPRefer.Value.Length > 0)
                Response.Redirect(HTTPRefer.Value);
            else Response.Redirect("~/Zeus/System/Message.aspx?Msg=0000000000000015");

        AssociaLinkButton.ToolTip = string.Empty;
    }//fine AssociaLinkButton_Click
    

    
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" />
    <asp:HiddenField ID="HTTPRefer" runat="server" />
    <div class="LayGridTitolo1">
        <asp:Label ID="CaptionallRecordLabel" SkinID="GridTitolo1" runat="server" 
        Text="Associa a Titolo del contenuto"></asp:Label>
    </div>
    <div class="BlockBox">
        <div class="BlockBoxHeader">
            <asp:Label ID="IdentificativoLabel" runat="server">Ricerca galleria da associare
            </asp:Label>
        </div>
        <table>
            <tr>
                <td class="BlockBoxDescription">
                    <asp:Label ID="Label1" SkinID="FieldDescription" runat="server">Titolo
                    </asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:TextBox ID="TitoloTextBox" runat="server" Columns="100" MaxLength="100"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="BlockBoxDescription">
                    <asp:Label ID="Label3" SkinID="FieldDescription" runat="server" Text="Data creazione"></asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:Label ID="Label4" SkinID="FieldValue" runat="server" Text="Compresa tra il"></asp:Label>
                    <asp:TextBox ID="NewDateFirstTextBox" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="NewDateFirstTextBox"
                        Display="Dynamic" ErrorMessage="*" SkinID="ZSSM_Validazione01" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$">Formato data richiesto: gg/mm/aaaa</asp:RegularExpressionValidator>
                    <asp:Label ID="Label5" SkinID="FieldValue" runat="server" Text="e il"></asp:Label>
                    <asp:TextBox ID="NewDateLastTextBox" runat="server" Columns="10" MaxLength="10" OnLoad="PWF"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="NewDateLastTextBox"
                        Display="Dynamic" ErrorMessage="*" SkinID="ZSSM_Validazione01" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$">Formato data richiesto: gg/mm/aaaa</asp:RegularExpressionValidator>
                    <asp:CompareValidator ValidationGroup="myValidation" ID="CompareValidator2" runat="server"
                        ControlToCompare="NewDateFirstTextBox" ControlToValidate="NewDateLastTextBox"
                        SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="La data iniziale deve essere precedente o uguale alla data finale"
                        Operator="GreaterThanEqual" Type="Date"></asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td class="BlockBoxDescription">
                    <asp:Label ID="Label6" SkinID="FieldDescription" runat="server" Text="Data modifica"></asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:Label ID="Label43" SkinID="FieldValue" runat="server" Text="Compresa tra il"></asp:Label>
                    <asp:TextBox ID="EdtDateFirstTextBox" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="EdtDateFirstTextBox"
                        Display="Dynamic" ErrorMessage="*" SkinID="ZSSM_Validazione01" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$">Formato data richiesto: gg/mm/aaaa</asp:RegularExpressionValidator>
                    <asp:Label ID="Label44" SkinID="FieldValue" runat="server" Text="e il"></asp:Label>
                    <asp:TextBox ID="EdtDateLastTextBox" runat="server" Columns="10" MaxLength="10" OnLoad="PWF"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="EdtDateLastTextBox"
                        Display="Dynamic" ErrorMessage="*" SkinID="ZSSM_Validazione01" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$">Formato data richiesto: gg/mm/aaaa</asp:RegularExpressionValidator>
                    <asp:CompareValidator ValidationGroup="myValidation" ID="CompareValidator1" runat="server"
                        ControlToCompare="EdtDateFirstTextBox" ControlToValidate="EdtDateLastTextBox"
                        SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="La data iniziale deve essere precedente o uguale alla data finale"
                        Operator="GreaterThanEqual" Type="Date"></asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td class="BlockBoxDescription">
                </td>
                <td class="BlockBoxValue">
                    <asp:LinkButton SkinID="ZSSM_Button01" ID="SearchButton" runat="server" Text="Cerca"
                        OnClick="SearchButton_Click"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </div>
    <asp:Panel ID="ElencoPanel" runat="server" Visible="false">
        <asp:GridView ID="GridView2" runat="server" AllowPaging="True" AutoGenerateColumns="False"
            AllowSorting="true" PageSize="30" OnRowDataBound="GridView1_RowDataBound">
            <Columns>
                <asp:TemplateField Visible="false">
                    <ItemTemplate>
                        <asp:HiddenField ID="AttivoArea1HiddenField" runat="server" Value='<%# Eval("AttivoArea1") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Titolo" HeaderText="Galleria" SortExpression="Titolo" />
                <asp:BoundField DataField="CatLiv2Liv3" HeaderText="Categoria" SortExpression="CatLiv2Liv3" />
                <asp:TemplateField HeaderText="Att." SortExpression="AttivoArea1">
                    <ItemTemplate>
                        <asp:Image ID="AttivoImage" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>
                <asp:CheckBoxField DataField="PW_Area1" HeaderText="Pub." SortExpression="PW_Area1"
                    ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="PWI_Area1" HeaderText="Pub. inizio" SortExpression="PWI_Area1"
                    DataFormatString="{0:d}" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="PWF_Area1" HeaderText="Pub. fine" SortExpression="PWF_Area1"
                    DataFormatString="{0:d}" ItemStyle-HorizontalAlign="Center" />
                <asp:TemplateField HeaderText="Associa">
                    <ItemTemplate>
                        <asp:LinkButton ID="AssociaLinkButton" runat="server" CssClass="GridView_Button1"
                            Text="Associa" OnClick="AssociaLinkButton_Click" ToolTip='<%# Eval("ID1Gallery") %>'></asp:LinkButton>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                <div style="text-align: center;">
                    La ricerca non ha prodotto alcun risultato oppure tutte le gallerie sono state associate.</div>
            </EmptyDataTemplate>
            <EmptyDataRowStyle BackColor="#FFFFFF" Height="200px" />
        </asp:GridView>
        <asp:SqlDataSource ID="vwPhotoGallery_AllSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
            SelectCommand="SET DATEFORMAT dmy; SELECT [ID1Gallery],[AttivoArea1], [Titolo], 
        [CatLiv2Liv3],[ZeusLangCode],[ZeusIdModulo],
        [PW_Area1],[PWI_Area1],[PWF_Area1],[ZeusId] 
        FROM [vwPhotoGallery_NonAssociate] "></asp:SqlDataSource>
    </asp:Panel>
</asp:Content>
