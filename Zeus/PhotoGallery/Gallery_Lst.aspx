<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" %>

<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
  
    
   
    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();

        if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM")))
            Response.Redirect("~/Zeus/System/Message.aspx?0");


        ZIM.Value = Server.HtmlEncode(Request.QueryString["ZIM"]);

        if (!ReadXML(ZIM.Value, GetLang()))
            Response.Redirect("~/Zeus/System/Message.aspx?1");

        ReadXML_Localization(ZIM.Value, GetLang());



        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI"))
            && (Request.QueryString["XRI"].Length > 0)
            && (isOK()))
            SingleRecordPanel.Visible = true;





    }//fine Page_Load


    private string GetLang()
    {
        string Lang = "ITA";

        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;

    }//fine GetLang

    private bool isOK()
    {
        try
        {
            if ((Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/PhotoGallery/Gallery_Edt.aspx") > 0) || (Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/PhotoGallery/Gallery_New.aspx") > 0))
                return true;
            else return false;
        }
        catch (Exception p)
        {
            return false;
        }

    }//fine isOK


    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_PhotoGallery.xml"));


            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {

                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {

                            if (childNode.Name.Equals("ZML_TitoloPagina_Lst"))
                            {
                                TitleField.Value = childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                                else
                                {
                                    myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name + "Label");

                                    if (myLabel != null)
                                        myLabel.Text = childNode.InnerText;
                                }
                            }

                        }
                        catch (Exception)
                        {

                        }
                    }//fine foreach
                }//fine foreach

            }//fine for
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine ReadXML_Localization



    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            string myXPathEach = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_PhotoGallery.xml"));

            GridView1.Columns[3].Visible =
                                        GridView1.Columns[4].Visible =
                                            GridView1.Columns[5].Visible =
                                                GridView1.Columns[6].Visible =

                                                GridView2.Columns[3].Visible =
                                        GridView2.Columns[4].Visible =
                                            GridView2.Columns[5].Visible =
                                                GridView2.Columns[6].Visible =

                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxPlanner").InnerText);

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }

    }//fine ReadXML





    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.DataItemIndex > -1)
            {
                Image AttivoImage = (Image)e.Row.FindControl("AttivoImage");
                HiddenField AttivoArea1HiddenField = (HiddenField)e.Row.FindControl("AttivoArea1HiddenField");

                int intAttivoArea1 = Convert.ToInt32(AttivoArea1HiddenField.Value.ToString());

                if (intAttivoArea1 == 1)
                    AttivoImage.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";

            }//fine if
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine GridView1_RowDataBound
    
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" />
    <asp:HiddenField ID="ZIM" runat="server" />
    <asp:Panel ID="SingleRecordPanel" runat="server" Visible="false">
        <div class="LayGridTitolo1">
            <asp:Label ID="CaptionallRecordLabel" SkinID="GridTitolo1" runat="server" Text="Ultimo contenuto creato / aggiornato"></asp:Label>
        </div>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False"
            AllowSorting="true" PageSize="30" DataSourceID="SingleRecordSqlDataSource" OnRowDataBound="GridView1_RowDataBound">
            <Columns>
                <asp:TemplateField Visible="false">
                    <ItemTemplate>
                        <asp:HiddenField ID="AttivoArea1HiddenField" runat="server" Value='<%# Eval("AttivoArea1") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Titolo" HeaderText="Galleria" SortExpression="Titolo" />
                <asp:BoundField DataField="CatLiv2Liv3" HeaderText="Categoria" SortExpression="CatLiv2Liv3" />
                <asp:TemplateField HeaderText="Att." SortExpression="AttivoArea1">
                    <ItemTemplate>
                        <asp:Image ID="AttivoImage" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>
                <asp:CheckBoxField DataField="PW_Area1" HeaderText="Pub." SortExpression="PW_Area1"
                    ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="PWI_Area1" HeaderText="Pub. inizio" SortExpression="PWI_Area1"
                    DataFormatString="{0:d}" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="PWF_Area1" HeaderText="Pub. fine" SortExpression="PWF_Area1"
                    DataFormatString="{0:d}" ItemStyle-HorizontalAlign="Center" />
                <asp:HyperLinkField DataNavigateUrlFields="ID1Gallery,ZeusLangCode,ZeusIdModulo"
                    DataNavigateUrlFormatString="/Zeus/PhotoGallery/Gallery_Edt.aspx?XRI={0}&Lang={1}&ZIM={2}"
                    HeaderText="Galleria" Text="Modifica">
                    <ControlStyle CssClass="GridView_Button1" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:HyperLinkField>
                <asp:HyperLinkField DataNavigateUrlFields="ZeusId,ZeusLangCode,ZeusIdModulo" DataNavigateUrlFormatString="/Zeus/PhotoGallery/Gallery_Fotografie.aspx?ZID={0}&Lang={1}&ZIM={2}"
                    HeaderText="Fotografie" Text="Modifica">
                    <ControlStyle CssClass="GridView_Button1" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:HyperLinkField>
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SingleRecordSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
            SelectCommand="SELECT [ID1Gallery],[AttivoArea1], [Titolo], [CatLiv2Liv3],[ZeusLangCode],[ZeusIdModulo],[PW_Area1],[PWI_Area1],[PWF_Area1],[ZeusId] FROM [vwPhotoGallery_All] WHERE ID1Gallery=@ID1Gallery">
            <SelectParameters>
                <asp:QueryStringParameter Name="ID1Gallery" QueryStringField="XRI" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <div class="VertSpacerMedium">
        </div>
    </asp:Panel>
    <div class="LayGridTitolo1">
        <asp:Label ID="Label2" runat="server" SkinID="GridTitolo1" Text="Elenco contenuti disponibili"></asp:Label>
    </div>
    <asp:GridView ID="GridView2" runat="server" AllowPaging="True" AutoGenerateColumns="False"
        AllowSorting="true" PageSize="30" DataSourceID="vwPhotoGallery_All" OnRowDataBound="GridView1_RowDataBound">
        <Columns>
            <asp:TemplateField Visible="false">
                <ItemTemplate>
                    <asp:HiddenField ID="AttivoArea1HiddenField" runat="server" Value='<%# Eval("AttivoArea1") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Titolo" HeaderText="Galleria" SortExpression="Titolo" />
            <asp:BoundField DataField="CatLiv2Liv3" HeaderText="Categoria" SortExpression="CatLiv2Liv3" />
            <asp:TemplateField HeaderText="Att." SortExpression="AttivoArea1">
                <ItemTemplate>
                    <asp:Image ID="AttivoImage" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:CheckBoxField DataField="PW_Area1" HeaderText="Pub." SortExpression="PW_Area1"
                ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="PWI_Area1" HeaderText="Pub. inizio" SortExpression="PWI_Area1"
                DataFormatString="{0:d}" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="PWF_Area1" HeaderText="Pub. fine" SortExpression="PWF_Area1"
                DataFormatString="{0:d}" ItemStyle-HorizontalAlign="Center" />
            <asp:HyperLinkField DataNavigateUrlFields="ID1Gallery,ZeusLangCode,ZeusIdModulo"
                DataNavigateUrlFormatString="/Zeus/PhotoGallery/Gallery_Edt.aspx?XRI={0}&Lang={1}&ZIM={2}"
                HeaderText="Galleria" Text="Modifica">
                <ControlStyle CssClass="GridView_Button1" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:HyperLinkField>
            <asp:HyperLinkField DataNavigateUrlFields="ZeusId,ZeusLangCode,ZeusIdModulo" DataNavigateUrlFormatString="/Zeus/PhotoGallery/Gallery_Fotografie.aspx?ZID={0}&Lang={1}&ZIM={2}"
                HeaderText="Fotografie" Text="Modifica">
                <ControlStyle CssClass="GridView_Button1" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:HyperLinkField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="vwPhotoGallery_All" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SET DATEFORMAT dmy; SELECT [ID1Gallery],[AttivoArea1], [Titolo], [CatLiv2Liv3],[ZeusLangCode],[ZeusIdModulo],[PW_Area1],[PWI_Area1],[PWF_Area1],[ZeusId] FROM [vwPhotoGallery_All] WHERE ZeusLangCode=@ZeusLangCode AND ZeusIdModulo=@ZeusIdModulo">
        <SelectParameters>
            <asp:QueryStringParameter Name="ZeusLangCode" QueryStringField="Lang" />
            <asp:QueryStringParameter Name="ZeusIdModulo" QueryStringField="ZIM" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
