﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" %>

<script runat="server">
    
   
    static string TitoloPagina = "Ricerca associazioni articoli / accessori";


    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;
        SetFocus(SearchButton.UniqueID);

    }//fine Page_Load


    private void SetFocus(string ID)
    {
        Page.Form.DefaultButton = ID;
        Page.Form.DefaultFocus = ID;

    }//fine SetFocus


    protected void SearchButton_Click(object sender, EventArgs e)
    {

        string myRedirect = "~/Zeus/Catalogo/Accessori_Lst.aspx?1=1";

        if (IDArticoloTextBox.Text.Length > 0)
            myRedirect += "&IDArt=" + IDArticoloTextBox.Text;

        if (CodiceArticoloTextBox.Text.Length > 0)
            myRedirect += "&CodArt=" + CodiceArticoloTextBox.Text;

        if (ModelloArticoloTextBox.Text.Length > 0)
            myRedirect += "&ModArt=" + ModelloArticoloTextBox.Text;

        if (!CategoriaArticoloDropDownList.SelectedValue.Equals("0"))
            myRedirect += "&CatArt=" + CategoriaArticoloDropDownList.SelectedValue;


        if (IDAccessorioTextBox.Text.Length > 0)
            myRedirect += "&IDAcc=" + IDAccessorioTextBox.Text;

        if (CodiceAccessorioTextBox.Text.Length > 0)
            myRedirect += "&CodAcc=" + CodiceAccessorioTextBox.Text;

        if (ModelloAccessorioTextBox.Text.Length > 0)
            myRedirect += "&ModAcc=" + ModelloAccessorioTextBox.Text;

        if (!CategoriaAccessorioDropDownList.SelectedValue.Equals("0"))
            myRedirect += "&CatAcc=" + CategoriaAccessorioDropDownList.SelectedValue;



        Response.Redirect(myRedirect);

    }//fine SearchButton_Click

  
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" />
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td valign="top">
                <div class="BlockBoxDouble">
                    <div class="BlockBoxHeader">
                        Articolo</div>
                    <table width="100%">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label6" runat="server" SkinID="FieldDescription">ID</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="IDArticoloTextBox" runat="server" Columns="30" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Description_Label" runat="server" SkinID="FieldDescription">Codice</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="CodiceArticoloTextBox" runat="server" Columns="30" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label1" runat="server" SkinID="FieldDescription">Modello</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="ModelloArticoloTextBox" runat="server" Columns="30" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label2" runat="server" SkinID="FieldDescription">Categoria</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:DropDownList ID="CategoriaArticoloDropDownList" runat="server" AppendDataBoundItems="True"
                                    DataSourceID="dsCategoria" DataTextField="CatLiv2Liv3" DataValueField="ID1Categoria">
                                    <asp:ListItem Selected="True" Value="0">&gt; Tutte</asp:ListItem>
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="dsCategoria" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                    SelectCommand="SELECT [ID1Categoria], [CatLiv2Liv3], [ZeusIdModulo], [MenuLabel2] FROM [vwCategorie_Zeus1] WHERE ([Livello] = 3 AND [ZeusIdModulo] = @ZeusIdModulo AND [ZeusLangCode]=@ZeusLangCode) ORDER BY CatLiv2Liv3">
                                    <SelectParameters>
                                        <asp:Parameter Name="ZeusIdModulo" Type="String" DefaultValue="CATPR" />
                                        <asp:QueryStringParameter Name="ZeusLangCode" QueryStringField="Lang" Type="String"
                                            DefaultValue="ITA" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td valign="top" width="10">
                <img src="/SiteImg/Spc.gif" height="5" width="10" />
            </td>
            <td valign="top">
                <div class="BlockBoxDouble">
                    <div class="BlockBoxHeader">
                        Accessori</div>
                    <table width="100%">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label3" runat="server" SkinID="FieldDescription">ID</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="IDAccessorioTextBox" runat="server" Columns="30" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label4" runat="server" SkinID="FieldDescription">Codice</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="CodiceAccessorioTextBox" runat="server" Columns="30" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label5" runat="server" SkinID="FieldDescription">Modello</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="ModelloAccessorioTextBox" runat="server" Columns="30" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label7" runat="server" SkinID="FieldDescription">Categoria</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:DropDownList ID="CategoriaAccessorioDropDownList" runat="server" AppendDataBoundItems="True"
                                    DataSourceID="dsCategoria" DataTextField="CatLiv2Liv3" DataValueField="ID1Categoria">
                                    <asp:ListItem Selected="True" Value="0">&gt; Tutte</asp:ListItem>
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                    SelectCommand="SELECT [ID1Categoria], [CatLiv2Liv3], [ZeusIdModulo], [MenuLabel2] FROM [vwCategorie_Zeus1] WHERE ([Livello] = 3 AND [ZeusIdModulo] = @ZeusIdModulo AND [ZeusLangCode]=@ZeusLangCode) ORDER BY CatLiv2Liv3">
                                    <SelectParameters>
                                        <asp:Parameter Name="ZeusIdModulo" Type="String" DefaultValue="CATPR" />
                                        <asp:QueryStringParameter Name="ZeusLangCode" QueryStringField="Lang" Type="String"
                                            DefaultValue="ITA" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <br />
    <div align="center">
        <asp:LinkButton ID="SearchButton" runat="server" SkinID="ZSSM_Button01" Text="Cerca"
            OnClick="SearchButton_Click"></asp:LinkButton>
    </div>
</asp:Content>
