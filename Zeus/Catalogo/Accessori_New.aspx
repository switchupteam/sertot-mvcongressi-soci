﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
    
    static string TitoloPagina = "Nuova associazione articoli / accessori";

    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;
        SetFocus(SearchButton.UniqueID);

        delinea myDeliena = new delinea();


        if (!Page.IsPostBack)
        {
            if (myDeliena.AntiSQLInjectionNew(Request.QueryString, "parent", "int"))
                PrePopolateParent(Request.QueryString["parent"]);

            if (myDeliena.AntiSQLInjectionNew(Request.QueryString, "child", "int"))
                PrePopolateChild(Request.QueryString["child"]);
        }
        
        
        
        

    }//fine Page_Load

    private void SetFocus(string ID)
    {
        Page.Form.DefaultButton = ID;
        Page.Form.DefaultFocus = ID;

    }//fine SetFocus



    private bool PrePopolateParent(string ID1Articolo)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        

        try
        {

            if (ID1Articolo.Length == 0)
                return false;

            IDArticoloTextBox.Text = ID1Articolo;
            
            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();


                sqlQuery = @"SELECT [Codice]
      ,[Articolo]
      ,[ID2Categoria1]
      
  FROM [vwCatalogo_All]
WHERE [ID1Articolo]=@ID1Articolo";

                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID1Articolo", System.Data.SqlDbType.Int);
                command.Parameters["@ID1Articolo"].Value = ID1Articolo;
                
                SqlDataReader reader = command.ExecuteReader();
                

                while (reader.Read())
                {
                    if (reader["Codice"] != DBNull.Value)
                        CodiceArticoloTextBox.Text = reader["Codice"].ToString();

                    if (reader["Articolo"] != DBNull.Value)
                        ModelloArticoloTextBox.Text = reader["Articolo"].ToString();

                    if (reader["ID2Categoria1"] != DBNull.Value)
                    {
                        CategoriaArticoloDropDownList.Items.RemoveAt(0);
                        
                        CategoriaArticoloDropDownList.SelectedIndex =
                            CategoriaArticoloDropDownList.Items.IndexOf(
                            CategoriaArticoloDropDownList.Items.FindByValue(
                            Convert.ToInt32(reader["ID2Categoria1"]).ToString()));
                    }
                          
                }//fine while

                reader.Close();

                return true;

            }//fine Using
        }
        catch (Exception p)
        {

            Response.Write(p.ToString());
            return false;
        }


    }//fine PrePopolateParent


    private bool PrePopolateChild(string ID1Articolo)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;



        try
        {

            if (ID1Articolo.Length == 0)
                return false;

            IDAccessorioTextBox.Text = ID1Articolo;

            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();


                sqlQuery = @"SELECT [Codice]
      ,[Articolo]
      ,[ID2Categoria1]
      
  FROM [vwCatalogo_All]
WHERE [ID1Articolo]=@ID1Articolo";

                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID1Articolo", System.Data.SqlDbType.Int);
                command.Parameters["@ID1Articolo"].Value = ID1Articolo;

                SqlDataReader reader = command.ExecuteReader();


                while (reader.Read())
                {
                    if (reader["Codice"] != DBNull.Value)
                        CodiceAccessorioTextBox.Text = reader["Codice"].ToString();

                    if (reader["Articolo"] != DBNull.Value)
                        ModelloAccessorioTextBox.Text = reader["Articolo"].ToString();

                    if (reader["ID2Categoria1"] != DBNull.Value)
                    {
                        CategoriaAccessorioDropDownList.Items.RemoveAt(0);
                        
                        CategoriaAccessorioDropDownList.SelectedIndex =
                            CategoriaAccessorioDropDownList.Items.IndexOf(
                            CategoriaAccessorioDropDownList.Items.FindByValue(
                            Convert.ToInt32(reader["ID2Categoria1"]).ToString()));
                    }


                }//fine while

                reader.Close();

                return true;

            }//fine Using
        }
        catch (Exception p)
        {

            Response.Write(p.ToString());
            return false;
        }


    }//fine PrePopolateChild
    

    protected void SearchButton_Click(object sender, EventArgs e)
    {

        try
        {
            InfoLabel.Visible = false;

            if (IDArticoloTextBox.Text.Length > 0)
                ProdottiSqlDatasource.SelectCommand += " AND ID1Articolo = "
                    + IDArticoloTextBox.Text;

            if (CodiceArticoloTextBox.Text.Length > 0)
                ProdottiSqlDatasource.SelectCommand += " AND Codice LIKE '%"
                    + CodiceArticoloTextBox.Text + "%'";

            if (ModelloArticoloTextBox.Text.Length > 0)
                ProdottiSqlDatasource.SelectCommand += " AND Articolo LIKE '%"
                    + ModelloArticoloTextBox.Text + "%'";

            if (!CategoriaArticoloDropDownList.SelectedValue.Equals("0"))
                ProdottiSqlDatasource.SelectCommand += " AND ID2Categoria1 ="
                    + CategoriaArticoloDropDownList.SelectedValue;


            ProdottiSqlDatasource.SelectCommand += " ORDER BY Codice";

            //Response.Write("DEB ProdottiSqlDatasource.SelectCommand: " + ProdottiSqlDatasource.SelectCommand+"<br />");

            ProdottiGridView.DataSourceID = "ProdottiSqlDatasource";
            ProdottiGridView.DataBind();



            if (IDAccessorioTextBox.Text.Length > 0)
                AccessoriSqlDataSource.SelectCommand += " AND ID1Articolo = "
                    + IDAccessorioTextBox.Text;

            if (CodiceAccessorioTextBox.Text.Length > 0)
                AccessoriSqlDataSource.SelectCommand += " AND Codice LIKE '%"
                    + CodiceAccessorioTextBox.Text + "%'";

            if (ModelloAccessorioTextBox.Text.Length > 0)
                AccessoriSqlDataSource.SelectCommand += " AND Articolo LIKE '%"
                    + ModelloAccessorioTextBox.Text + "%'";

            if (!CategoriaAccessorioDropDownList.SelectedValue.Equals("0"))
                AccessoriSqlDataSource.SelectCommand += " AND ID2Categoria1 ="
                    + CategoriaAccessorioDropDownList.SelectedValue;

            AccessoriSqlDataSource.SelectCommand += " ORDER BY Codice";

            AccessoriGridView.DataSourceID = "AccessoriSqlDataSource";
            AccessoriSqlDataSource.DataBind();

            //Response.Write("DEB AccessoriSqlDataSource.SelectCommand: " + AccessoriSqlDataSource.SelectCommand + "<br />");

            AssociazionePanel.Visible = true;



            //if (!CategoriaDropDownList.SelectedValue.Equals("0"))
            //    AssociazioniSqlDatasource.SelectCommand += " AND ID2Categoria1 =" + CategoriaDropDownList.SelectedValue;

            //if (ID1AccessoriTextBox.Text.Length > 0)
            //    AssociazioniSqlDatasource.SelectCommand += " AND ID2Accessorio =" + ID1AccessoriTextBox.Text;

            //AssociazioniGridView.DataSourceID = "AssociazioniSqlDatasource";
            //AssociazioniGridView.DataBind();


            AssociazioniGridPanel.Visible = true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            AssociazionePanel.Visible = false;
        }
    }//fine SearchButton_Click


    private int InsertAssociazione(string ID2Articolo, string ID2Accessorio)
    {

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;
        int myReturn = -1;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {


            try
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = @"INSERT INTO tbCatalogo_Accessori 
(ID2Articolo, ID2Accessorio) 
VALUES (@ID2Articolo, @ID2Accessorio); 
SELECT @XRI = SCOPE_IDENTITY();";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID2Articolo", System.Data.SqlDbType.Int);
                command.Parameters["@ID2Articolo"].Value = ID2Articolo;
                command.Parameters.Add("@ID2Accessorio", System.Data.SqlDbType.Int);
                command.Parameters["@ID2Accessorio"].Value = ID2Accessorio;

                SqlParameter ID = command.Parameters.Add(new SqlParameter("@XRI", System.Data.SqlDbType.Int));
                ID.Direction = System.Data.ParameterDirection.Output;

                command.ExecuteNonQuery();

                if (ID.Value != null)
                    myReturn = Convert.ToInt32(ID.Value);

            }
            catch (Exception p)
            {
                AssociaCustomValidator.ErrorMessage = p.ToString();
                AssociaCustomValidator.IsValid = false;
            }

        }//fine Using

        return myReturn;

    }//fine InsertAssociazione


    private bool HasAssociazione(string ID2Articolo, string ID2Accessorio)
    {

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;
        bool myReturn = true;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {


            try
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = @"SELECT ID1CatAssoc 
FROM tbCatalogo_Accessori 
WHERE ID2Articolo=@ID2Articolo AND ID2Accessorio=@ID2Accessorio";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID2Articolo", System.Data.SqlDbType.Int);
                command.Parameters["@ID2Articolo"].Value = ID2Articolo;
                command.Parameters.Add("@ID2Accessorio", System.Data.SqlDbType.Int);
                command.Parameters["@ID2Accessorio"].Value = ID2Accessorio;
                SqlDataReader reader = command.ExecuteReader();

                myReturn = reader.HasRows;

                reader.Close();

                return myReturn;

            }
            catch (Exception p)
            {
                AssociaCustomValidator.ErrorMessage = p.ToString();
                AssociaCustomValidator.IsValid = false;

            }

        }//fine Using

        return myReturn;

    }//fine CheckAssociazione


    protected void AssociaLinkButton_Click(object sender, EventArgs e)
    {
        try
        {
            if ((Request.Form["ID1ArticoloRadioButton"] != null) && (Request.Form["ID1ArticoloRadioButton"].Length > 0) && (Request.Form["ID1AccessoriRadioButton"] != null) && (Request.Form["ID1AccessoriRadioButton"].Length > 0))
            {

                if (!HasAssociazione(Request.Form["ID1ArticoloRadioButton"],
                    Request.Form["ID1AccessoriRadioButton"]))
                {

                    int ID = 0;

                    if ((ID = InsertAssociazione(Request.Form["ID1ArticoloRadioButton"], Request.Form["ID1AccessoriRadioButton"])) > 0)
                    {
                        //Response.Redirect("~/Zeus/Associazioni/ProdottiSupport_Lst.aspx?XRI=" + ID);
                        InfoLabel.Visible = true;
                        XRI.Value = ID.ToString();
                        SingleRecordGridView.DataSourceID = "SingleRecordSqlDataSource";
                        SingleRecordGridView.DataBind();
                        SingleRecordPanel.Visible = true;


                        //if (!CategoriaDropDownList.SelectedValue.Equals("0"))
                        //    AssociazioniSqlDatasource.SelectCommand += " AND ID2Categoria1 =" + CategoriaDropDownList.SelectedValue;

                        //if (ID1AccessoriTextBox.Text.Length > 0)
                        //    AssociazioniSqlDatasource.SelectCommand += " AND ID2Accessorio =" + ID1AccessoriTextBox.Text;



                        AssociazioniGridView.DataSourceID = "AssociazioniSqlDatasource";
                        AssociazioniGridView.DataBind();


                        AssociazioniGridPanel.Visible = true;
                    }
                }
                else
                {
                    AssociaCustomValidator.IsValid = false;
                    AssociaCustomValidator.ErrorMessage = "Attenzione: associazione già presente.<br /><br />";
                }

            }
            else AssociaCustomValidator.IsValid = false;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine AssociaLinkButton

    protected void ProdottiGridView_Sorting(object sender, GridViewSortEventArgs e)
    {

        if (IDArticoloTextBox.Text.Length > 0)
            ProdottiSqlDatasource.SelectCommand += " AND ID1Articolo = "
                + IDArticoloTextBox.Text;

        if (CodiceArticoloTextBox.Text.Length > 0)
            ProdottiSqlDatasource.SelectCommand += " AND Codice LIKE '%"
                + CodiceArticoloTextBox.Text + "%'";

        if (ModelloArticoloTextBox.Text.Length > 0)
            ProdottiSqlDatasource.SelectCommand += " AND Articolo LIKE '%"
                + ModelloArticoloTextBox.Text + "%'";

        if (!CategoriaArticoloDropDownList.SelectedValue.Equals("0"))
            ProdottiSqlDatasource.SelectCommand += " AND ID2Categoria1 ="
                + CategoriaArticoloDropDownList.SelectedValue;

    }//fine ProdottiGridView_Sorting


    protected void AccessoriGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (IDAccessorioTextBox.Text.Length > 0)
            AccessoriSqlDataSource.SelectCommand += " AND ID1Articolo = "
                + IDAccessorioTextBox.Text;

        if (CodiceAccessorioTextBox.Text.Length > 0)
            AccessoriSqlDataSource.SelectCommand += " AND Codice LIKE '%"
                + CodiceAccessorioTextBox.Text + "%'";

        if (ModelloAccessorioTextBox.Text.Length > 0)
            AccessoriSqlDataSource.SelectCommand += " AND Articolo LIKE '%"
                + ModelloAccessorioTextBox.Text + "%'";

        if (!CategoriaAccessorioDropDownList.SelectedValue.Equals("0"))
            AccessoriSqlDataSource.SelectCommand += " AND ID2Categoria1 ="
                + CategoriaAccessorioDropDownList.SelectedValue;

    }//fine AccessoriGridView_Sorting
    
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td valign="top">
                <div class="BlockBoxDouble">
                    <div class="BlockBoxHeader">
                        Articolo</div>
                    <table width="100%">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label6" runat="server" SkinID="FieldDescription">ID</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="IDArticoloTextBox" runat="server" Columns="30" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Description_Label" runat="server" SkinID="FieldDescription">Codice</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="CodiceArticoloTextBox" runat="server" Columns="30" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label1" runat="server" SkinID="FieldDescription">Modello</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="ModelloArticoloTextBox" runat="server" Columns="30" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label2" runat="server" SkinID="FieldDescription">Categoria</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:DropDownList ID="CategoriaArticoloDropDownList" runat="server" AppendDataBoundItems="True"
                                    DataSourceID="dsCategoria" DataTextField="CatLiv2Liv3" DataValueField="ID1Categoria">
                                    <asp:ListItem Selected="True" Value="0">&gt; Tutte</asp:ListItem>
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="dsCategoria" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                    SelectCommand="SELECT [ID1Categoria], [CatLiv2Liv3], [ZeusIdModulo], [MenuLabel2] FROM [vwCategorie_Zeus1] WHERE ([Livello] = 3 AND [ZeusIdModulo] = @ZeusIdModulo AND [ZeusLangCode]=@ZeusLangCode) ORDER BY CatLiv2Liv3">
                                    <SelectParameters>
                                        <asp:Parameter Name="ZeusIdModulo" Type="String" DefaultValue="PBCT1" />
                                        <asp:QueryStringParameter Name="ZeusLangCode" QueryStringField="Lang" Type="String"
                                            DefaultValue="ITA" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td valign="top" width="10">
                <img src="/SiteImg/Spc.gif" height="5" width="10" />
            </td>
            <td valign="top">
                <div class="BlockBoxDouble">
                    <div class="BlockBoxHeader">
                        Accessori</div>
                    <table width="100%">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label3" runat="server" SkinID="FieldDescription">ID</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="IDAccessorioTextBox" runat="server" Columns="30" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label4" runat="server" SkinID="FieldDescription">Codice</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="CodiceAccessorioTextBox" runat="server" Columns="30" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label5" runat="server" SkinID="FieldDescription">Modello</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="ModelloAccessorioTextBox" runat="server" Columns="30" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label7" runat="server" SkinID="FieldDescription">Categoria</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:DropDownList ID="CategoriaAccessorioDropDownList" runat="server" AppendDataBoundItems="True"
                                    DataSourceID="dsCategoria" DataTextField="CatLiv2Liv3" DataValueField="ID1Categoria">
                                    <asp:ListItem Selected="True" Value="0">&gt; Tutte</asp:ListItem>
                                </asp:DropDownList>
                              
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <br />
    <div align="center">
        <asp:LinkButton ID="SearchButton" runat="server" SkinID="ZSSM_Button01" Text="Cerca"
            OnClick="SearchButton_Click"></asp:LinkButton>
    </div>
    <asp:Panel ID="AssociazionePanel" runat="server" Visible="false">
        <br />
        <table style="width: 100%">
            <tr>
                <td valign="top" style="width: 50%">
                    <%--PRIMA GRID--%>
                    <table class="Tabella" style="width: 100%">
                        <caption>
                            <div class="LabelSkin_GridTitolo1">
                                Articoli</div>
                        </caption>
                        <tr>
                            <td>
                                <asp:GridView ID="ProdottiGridView" runat="server" AllowPaging="True" AllowSorting="True"
                                    AutoGenerateColumns="False" PageSize="50" OnSorting="ProdottiGridView_Sorting">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <input name="ID1ArticoloRadioButton" type="radio" value='<%# Eval("ID1Articolo") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Codice" HeaderText="Codice" SortExpression="Codice" />
                                        <asp:BoundField DataField="Articolo" HeaderText="Modello" SortExpression="Articolo" />
                                    </Columns>
                                </asp:GridView>
                                <asp:SqlDataSource ID="ProdottiSqlDatasource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                    SelectCommand="SELECT [ID1Articolo], [Codice], [Articolo] FROM [tbCatalogo] WHERE ZeusLangCode=@ZeusLangCode">
                                    <SelectParameters>
                                        <asp:QueryStringParameter Name="ZeusLangCode" QueryStringField="Lang" DefaultValue="ITA" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top" style="width: 50%">
                    <%--SECONDA GRID--%>
                    <table class="Tabella" style="width: 100%">
                        <caption>
                            <div class="LabelSkin_GridTitolo1">
                                Accessori</div>
                        </caption>
                        <tr>
                            <td>
                                <asp:GridView ID="AccessoriGridView" runat="server" AllowPaging="True" AllowSorting="True"
                                    AutoGenerateColumns="False" PageSize="50" OnSorting="AccessoriGridView_Sorting">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <input name="ID1AccessoriRadioButton" type="radio" value='<%# Eval("ID1Articolo") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Codice" HeaderText="Codice" SortExpression="Codice" />
                                        <asp:BoundField DataField="Articolo" HeaderText="Modello" SortExpression="Articolo" />
                                    </Columns>
                                </asp:GridView>
                                <asp:SqlDataSource ID="AccessoriSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                    SelectCommand="SELECT [ID1Articolo], [Codice], [Articolo] FROM [tbCatalogo] WHERE ZeusLangCode=@ZeusLangCode">
                                    <SelectParameters>
                                        <asp:QueryStringParameter Name="ZeusLangCode" QueryStringField="Lang" DefaultValue="ITA" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <div align="center">
            <asp:Label ID="InfoLabel" runat="server" Visible="false" Text="<br />Associazione effettuata correttamente.<br /><br />"
                SkinID="CorpoTesto" Font-Bold="true"></asp:Label>
            <asp:CustomValidator ID="AssociaCustomValidator" runat="server" CssClass="Warning"
                Display="Dynamic" ErrorMessage="ATTENZIONE: Selezionare un prodotto e un Accessori.<br /><br />"></asp:CustomValidator>
            <asp:LinkButton ID="AssociaLinkButton" SkinID="ZSSM_Button01" runat="server" Text="Associa"
                OnClick="AssociaLinkButton_Click"></asp:LinkButton>
        </div>
    </asp:Panel>
    <asp:Panel ID="SingleRecordPanel" runat="server" Visible="false">
        <asp:HiddenField ID="XRI" runat="server" />
        <div class="LayGridTitolo1">
            <asp:Label ID="CaptionNewRecordLabel" SkinID="GridTitolo1" runat="server" Text="Ultima associazione creata"></asp:Label></div>
        <asp:GridView ID="SingleRecordGridView" runat="server" AllowPaging="True" AllowSorting="True"
            AutoGenerateColumns="False" PageSize="50">
            <Columns>
                <asp:BoundField DataField="CodiceArticolo" HeaderText="Cod. art." SortExpression="CodiceArticolo" />
                <asp:BoundField DataField="Articolo" HeaderText="Titolo art." SortExpression="Articolo" />
                <asp:BoundField DataField="ID1Articolo" HeaderText="ID art." SortExpression="ID1Articolo" />
                <asp:BoundField DataField="CategoriaArticolo" HeaderText="Categoria art." SortExpression="CategoriaArticolo" />
                <asp:BoundField DataField="CodiceAccessorio" HeaderText="Cod. acc." SortExpression="CodiceAccessorio" />
                <asp:BoundField DataField="Accessorio" HeaderText="Titolo acc." SortExpression="Accessorio" />
                <asp:BoundField DataField="ID1Accessorio" HeaderText="ID acc." SortExpression="ID1Accessorio" />
                <asp:BoundField DataField="CategoriaAccessorio" HeaderText="Categoria acc." SortExpression="CategoriaAccessorio" />
                <asp:HyperLinkField DataNavigateUrlFields="ID1CatAssoc" DataNavigateUrlFormatString="~/Zeus/Cataloghi/Delete.aspx?XRI={0}"
                    HeaderText="Elimina">
                    <ControlStyle CssClass="GridView_Button_Elimina" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:HyperLinkField>
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SingleRecordSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
            SelectCommand="
           SELECT     ID1CatAssoc, 
           ID1Accessorio,
           ID1Articolo,
            Articolo, 
           CategoriaArticolo, 
                      Accessorio, 
                      CategoriaAccessorio,
                       CodiceArticolo, 
                      CodiceAccessorio
FROM        vwCatalogo_Accessori
                      WHERE ID1CatAssoc=@ID1CatAssoc
                      ">
            <SelectParameters>
                <asp:ControlParameter Name="ID1CatAssoc" ControlID="XRI" PropertyName="Value" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <div class="VertSpacerMedium">
        </div>
    </asp:Panel>
    <asp:Panel ID="AssociazioniGridPanel" runat="server" Visible="false">
        <div class="LayGridTitolo1">
            <asp:Label ID="CaptionallRecordLabel" runat="server" Text="Elenco associazioni" SkinID="GridTitolo1"></asp:Label></div>
        <asp:GridView ID="AssociazioniGridView" runat="server" AllowPaging="True" AllowSorting="True"
            AutoGenerateColumns="False" PageSize="50">
            <Columns>
                <asp:BoundField DataField="CodiceArticolo" HeaderText="Cod. art." SortExpression="CodiceArticolo" />
                <asp:BoundField DataField="Articolo" HeaderText="Titolo art." SortExpression="Articolo" />
                <asp:BoundField DataField="ID1Articolo" HeaderText="ID art." SortExpression="ID1Articolo" />
                <asp:BoundField DataField="CategoriaArticolo" HeaderText="Categoria art." SortExpression="CategoriaArticolo" />
                <asp:BoundField DataField="CodiceAccessorio" HeaderText="Cod. acc." SortExpression="CodiceAccessorio" />
                <asp:BoundField DataField="Accessorio" HeaderText="Titolo acc." SortExpression="Accessorio" />
                <asp:BoundField DataField="ID1Accessorio" HeaderText="ID acc." SortExpression="ID1Accessorio" />
                <asp:BoundField DataField="CategoriaAccessorio" HeaderText="Categoria acc." SortExpression="CategoriaAccessorio" />
                <asp:HyperLinkField DataNavigateUrlFields="ID1CatAssoc" DataNavigateUrlFormatString="~/Zeus/Cataloghi/Delete.aspx?XRI={0}"
                    HeaderText="Elimina">
                    <ControlStyle CssClass="GridView_Button_Elimina" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:HyperLinkField>
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="AssociazioniSqlDatasource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
            SelectCommand="SELECT     ID1CatAssoc,
             ID1Accessorio,
           ID1Articolo, 
            Articolo, 
           CategoriaArticolo, 
                      Accessorio, 
                      CategoriaAccessorio,
                       CodiceArticolo, 
                      CodiceAccessorio
FROM        vwCatalogo_Accessori
        WHERE CodiceArticolo LIKE '%' + @CodiceArticolo + '%' 
        AND CodiceAccessorio LIKE '%' + @CodiceAccessorio + '%' 
        AND Articolo LIKE '%' + @Articolo + '%' 
        AND Accessorio LIKE '%' + @Accessorio + '%' ">
            <SelectParameters>
                <asp:ControlParameter Name="CodiceArticolo" ControlID="CodiceArticoloTextBox" PropertyName="Text"
                    Type="String" DefaultValue="%" />
                <asp:ControlParameter Name="CodiceAccessorio" ControlID="CodiceAccessorioTextBox"
                    PropertyName="Text" Type="String" DefaultValue="%" />
                <asp:ControlParameter Name="Articolo" ControlID="ModelloArticoloTextBox" PropertyName="Text"
                    Type="String" DefaultValue="%" />
                <asp:ControlParameter Name="Accessorio" ControlID="ModelloAccessorioTextBox" PropertyName="Text"
                    Type="String" DefaultValue="%" />
            </SelectParameters>
        </asp:SqlDataSource>
    </asp:Panel>
</asp:Content>
