﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();

        if (!myDelinea.AntiSQLInjection(Request.QueryString))
            Response.Redirect("~/Zeus/System/Message.aspx");
    }//fine Page_Load

    protected void tbCatalogo_SqlDataSource_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.Exception != null)
            Response.Redirect("~/Zeus/System/Message.aspx?1");
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Maui - Anteprima contenuto</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:FormView ID="FormView1" runat="server" DataSourceID="tbCatalogo_SqlDataSource"
                CellPadding="0">
                <ItemTemplate>
                    <asp:Label ID="Contenuto1Label" runat="server" Text='<%# Eval("Descrizione1") %>'></asp:Label>
                </ItemTemplate>
            </asp:FormView>
            <asp:SqlDataSource ID="tbCatalogo_SqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                SelectCommand="SELECT [Descrizione1] FROM [tbCatalogo] WHERE ID1Articolo=@ID1Articolo"
                OnSelected="tbCatalogo_SqlDataSource_Selected">
                <SelectParameters>
                    <asp:QueryStringParameter Name="ID1Articolo" QueryStringField="XRI" Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>
            <br />
            <div align="center">
                <input type="button" value="Chiudi" onclick="javascript: window.close()" name="button"></input>
            </div>
        </div>
    </form>
</body>
</html>
