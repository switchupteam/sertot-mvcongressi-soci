﻿using System;
using System.Data.SqlClient;
using System.Web.Security;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per Articoli_Lst
/// </summary>
public partial class Catalogo_Articoli_Lst : System.Web.UI.Page 
{
    static string TitoloPagina = "Elenco articoli catalogo";
    delinea myDelinea = new delinea();

    protected void Page_Load(object sender, EventArgs e)
    {
        if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM"))
            || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang")))
            Response.Redirect("/Zeus/System/Message.aspx?0");

        GridView1.Columns[13].Visible = GridView2.Columns[13].Visible = myPermitRoles.SHOW_DTL_BUTTON_IN_LST;
        GridView1.Columns[14].Visible = GridView2.Columns[14].Visible = myPermitRoles.SHOW_EDT_BUTTON_IN_LST;

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI")) && (Request.QueryString["XRI"].Length > 0) && (isOK()))
            SingleRecordPanel.Visible = true;

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRE"))
            && (Request.QueryString["XRE"].Length > 0))
            dsListaCataloghi.SelectCommand += " AND Articolo LIKE '%" + Server.HtmlDecode(Request.QueryString["XRE"]).Replace("'", "''") + "%'";

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRE2"))
            && (Request.QueryString["XRE2"].Length > 0))
            dsListaCataloghi.SelectCommand += " AND Codice LIKE '%" + Server.HtmlDecode(Request.QueryString["XRE2"]) + "%'";

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRE3"))
           && (Request.QueryString["XRE3"].Length > 0))
            dsListaCataloghi.SelectCommand += " AND ZeusTags LIKE '%" + Server.HtmlDecode(Request.QueryString["XRE3"]) + "%'";

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI1"))
           && (Request.QueryString["XRI1"].Length > 0))
            dsListaCataloghi.SelectCommand += " AND ID2Brand=" + Server.HtmlDecode(Request.QueryString["XRI1"]);

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI2"))
          && (Request.QueryString["XRI2"].Length > 0))
            dsListaCataloghi.SelectCommand += " AND ID2Categoria1=" + Server.HtmlDecode(Request.QueryString["XRI2"]);

        dsListaCataloghi.SelectCommand += " ORDER BY Articolo,PubblicaDataInizio DESC";
        ZIM.Value = Server.HtmlEncode(Request.QueryString["ZIM"]);

        if (!ReadXML(ZIM.Value, GetLang()))
            Response.Write("~/Zeus/System/Message.aspx?1");

        ReadXML_Localization(ZIM.Value, GetLang());
    }

    private string GetLang()
    {
        try
        {
            delinea myDelinea = new delinea();
            string Lang = "ITA";

            if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
            {
                if (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang"))
                    Lang = Request.QueryString["Lang"];
            }

            return Lang;
        }
        catch (Exception)
        {
            return "ITA";
        }
    }

    private bool isOK()
    {
        try
        {
            if ((Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/Catalogo/Articoli_Edt.aspx") > 0) 
                || (Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/Catalogo/Articoli_New.aspx") > 0) 
                || (Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/Catalogo/Articoli_New_Lang.aspx") > 0))
                return true;
            else 
                return false;
        }
        catch (Exception p)
        {
            return false;
        }
    }

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Catalogo.xml"));

            TitleField.Value = mydoc.SelectSingleNode(myXPath + "ZML_TitoloPagina_Lst").InnerText;

            GridView2.Columns[5].HeaderText 
                = GridView1.Columns[5].HeaderText
                =  mydoc.SelectSingleNode(myXPath + "ZML_Brand_Lst").InnerText;

            GridView2.Columns[6].HeaderText 
                = GridView1.Columns[6].HeaderText 
                = mydoc.SelectSingleNode(myXPath + "ZML_Categoria1_Lst").InnerText;

            GridView2.Columns[13].HeaderText
                = GridView1.Columns[13].HeaderText 
                = mydoc.SelectSingleNode(myXPath + "ZML_Categoria2_Lst").InnerText;

            GridView2.Columns[10].HeaderText 
                = GridView1.Columns[10].HeaderText 
                = mydoc.SelectSingleNode(myXPath + "ZML_PWAreaZeus_Lst").InnerText;

            GridView2.Columns[7].HeaderText 
                = GridView1.Columns[7].HeaderText 
                = mydoc.SelectSingleNode(myXPath + "ZML_PWArea1_Lst").InnerText;

            GridView2.Columns[8].HeaderText 
                = GridView1.Columns[8].HeaderText 
                = mydoc.SelectSingleNode(myXPath + "ZML_PWArea2_Lst").InnerText;

            GridView2.Columns[9].HeaderText 
                = GridView1.Columns[9].HeaderText 
                = mydoc.SelectSingleNode(myXPath + "ZML_PWArea3_Lst").InnerText;
        }
        catch (Exception p)
        {
        }
    }

    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_Catalogo.xml"));

            if (!CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_Lst").InnerText))
                Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223599");

            GridView1.Columns[15].Visible = GridView2.Columns[15].Visible =
                CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_Dtl").InnerText);
            GridView1.Columns[14].Visible = GridView2.Columns[14].Visible
                = CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_Edt").InnerText);

            GridView2.Columns[10].Visible =
                GridView1.Columns[10].Visible = Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_AreaZeus").InnerText);

            GridView2.Columns[3].Visible = GridView1.Columns[3].Visible =
                Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Codice").InnerText);

            GridView2.Columns[5].Visible = GridView1.Columns[5].Visible =
               Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Brand").InnerText);

            GridView2.Columns[6].Visible = GridView1.Columns[6].Visible =
                Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Categoria1_Lst").InnerText);

            GridView2.Columns[8].Visible = GridView1.Columns[8].Visible =
                Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Area2").InnerText);

            GridView2.Columns[9].Visible = GridView1.Columns[9].Visible =
                Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Area3").InnerText);

            GridView2.Columns[10].Visible = GridView1.Columns[10].Visible =
                Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_AreaZeus").InnerText);

            GridView2.Columns[12].Visible = GridView2.Columns[11].Visible
                = GridView1.Columns[12].Visible = GridView1.Columns[11].Visible
                = Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Quantita").InnerText);

            GridView2.Columns[13].Visible = GridView1.Columns[13].Visible =
                Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Categoria2_Lst").InnerText);

            Livello.Value = mydoc.SelectSingleNode(myXPath + "ZMCD_Cat2Liv").InnerText;

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    private bool CheckPermit(string AllRoles)
    {
        bool IsPermit = false;

        try
        {
            if (AllRoles.Length == 0)
                return false;

            char[] delimiter = { ',' };

            foreach (string roles in Roles.GetRolesForUser(Membership.GetUser().UserName))
            {
                string[] PressRoles = AllRoles.Split(delimiter);

                for (int i = 0; i < PressRoles.Length; ++i)
                {
                    if (PressRoles[i].Trim().Equals(roles.Trim()))
                    {
                        IsPermit = true;
                        break;
                    }
                }
            }
        }
        catch (Exception)
        { 
        }

        return IsPermit;
    }

    public string GetSezioni(string ID1Articolo)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;
        string Sezioni = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {
            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                switch (Livello.Value)
                {
                    case "23":
                        sqlQuery = "SELECT [CatLiv2Liv3]  ";
                        break;

                    case "3":
                        sqlQuery = "SELECT [CatLiv3]  ";
                        break;

                    default:
                        sqlQuery = "SELECT [CatLiv1Liv2Liv3] ";
                        break;
                }

                sqlQuery += " FROM [vwCatalogoCategorie] ";
                sqlQuery += " WHERE ID1Articolo=" + ID1Articolo;
                sqlQuery += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";

                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                    Sezioni += reader.GetString(0) + "<br />";

                reader.Close();
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
            }
        }

        return Sezioni;
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.DataItemIndex > -1)
        {
            Image Image1 = (Image)e.Row.FindControl("Image1");
            Label AttivoArea1 = (Label)e.Row.FindControl("AttivoArea1");

            try
            {
                int intAttivoArea1 = Convert.ToInt32(AttivoArea1.Text.ToString());

                if (intAttivoArea1 == 1)
                {
                    Image1.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";
                }
            }
            catch (Exception)
            {

            }

            Image Image2 = (Image)e.Row.FindControl("Image2");
            Label AttivoArea2 = (Label)e.Row.FindControl("AttivoArea2");
            try
            {
                int intAttivoArea2 = Convert.ToInt32(AttivoArea2.Text.ToString());

                if (intAttivoArea2 == 1)
                {
                    Image2.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";
                }
            }
            catch (Exception)
            {

            }

            Image Image3 = (Image)e.Row.FindControl("Image3");
            Label AttivoArea3 = (Label)e.Row.FindControl("AttivoArea3");
            try
            {
                int intAttivoArea3 = Convert.ToInt32(AttivoArea3.Text.ToString());

                if (intAttivoArea3 == 1)
                {
                    Image3.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";
                }
            }
            catch (Exception)
            {
            }
        }
    }

    protected void PW_Zeus1Image_DataBinding(object sender, EventArgs e)
    {
        Image PW_Zeus1Image = (Image)sender;

        if (Convert.ToBoolean(PW_Zeus1Image.ToolTip))
            PW_Zeus1Image.ImageUrl = "~/Zeus/SiteImg/Ico1_Flag_On.gif";

        PW_Zeus1Image.ToolTip = string.Empty;
    }
}