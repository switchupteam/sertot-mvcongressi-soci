<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="Articoli_Lst.aspx.cs"
    Inherits="Catalogo_Articoli_Lst"
    Theme="Zeus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" />
    <asp:HiddenField ID="ZIM" runat="server" />
     <asp:HiddenField ID="Livello" runat="server" />
    <dlc:PermitRoles ID="myPermitRoles" runat="server" PAGE_TYPE="LST" SQL_TABLE="tbPz_Catalogo" />
    <asp:Panel ID="SingleRecordPanel" runat="server" Visible="false">
        <div class="LayGridTitolo1">
            <asp:Label ID="CaptionallRecordLabel" SkinID="GridTitolo1" runat="server" Text="Ultimo contenuto creato / aggiornato"></asp:Label>
        </div>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
            AutoGenerateColumns="False" DataSourceID="dsListaCataloghiSingleRecord" OnRowDataBound="GridView1_RowDataBound"
            PageSize="50">
            <Columns>
                <asp:TemplateField HeaderText="AttivoArea1" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="AttivoArea1" runat="server" Text='<%# Eval("AttivoArea1") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="AttivoArea2" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="AttivoArea2" runat="server" Text='<%# Eval("AttivoArea2") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="AttivoArea3" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="AttivoArea3" runat="server" Text='<%# Eval("AttivoArea3") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Codice" HeaderText="Codice" SortExpression="Codice" />
                <asp:BoundField DataField="Articolo" HeaderText="Articolo" SortExpression="Articolo" />
                <asp:BoundField DataField="Brand" HeaderText="Brand" SortExpression="Brand" />
                <asp:BoundField DataField="Categoria" HeaderText="Categoria" SortExpression="Categoria" />
                <asp:TemplateField SortExpression="AttivoArea1">
                    <ItemTemplate>
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" /></ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>
                <asp:TemplateField SortExpression="AttivoArea2">
                    <ItemTemplate>
                        <asp:Image ID="Image2" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" /></ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>
                <asp:TemplateField SortExpression="AttivoArea3">
                    <ItemTemplate>
                        <asp:Image ID="Image3" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" /></ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>
                <asp:TemplateField SortExpression="PW_Zeus1">
                    <ItemTemplate>
                        <asp:Image ID="PW_Zeus1Image" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Flag_Off.gif"
                            ToolTip='<%# Eval("PW_Zeus1") %>' OnDataBinding="PW_Zeus1Image_DataBinding" /></ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>
                <asp:BoundField DataField="QtaDisponibile" HeaderText="Disp." SortExpression="QtaDisponibile"
                    ItemStyle-HorizontalAlign="Right" />
                <asp:BoundField DataField="QtaArrivo" HeaderText="Arr." SortExpression="QtaArrivo"
                    ItemStyle-HorizontalAlign="Right" />
                <asp:TemplateField HeaderText="Sezioni">
                    <ItemTemplate>
                        <asp:Label ID="SezioniLabel" runat="server" Text='<%# GetSezioni(Eval("ID1Articolo").ToString()) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:HyperLinkField DataNavigateUrlFields="ID1Articolo,ZeusLangCode,ZeusIdModulo"
                    DataNavigateUrlFormatString="Articoli_Edt.aspx?XRI={0}&Lang={1}&ZIM={2}" HeaderText="Modifica"
                    Text="Modifica">
                    <ControlStyle CssClass="GridView_ZeusButton1" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:HyperLinkField>
                <asp:HyperLinkField DataNavigateUrlFields="ID1Articolo,ZeusLangCode,ZeusIdModulo"
                    HeaderText="Dettaglio" Text="Dettaglio" DataNavigateUrlFormatString="Articoli_Dtl.aspx?XRI={0}&Lang={1}&ZIM={2}">
                    <ControlStyle CssClass="GridView_ZeusButton1" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:HyperLinkField>
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="dsListaCataloghiSingleRecord" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
            SelectCommand="SELECT [ID1Articolo], [Codice], [Articolo], [ID2Categoria1]
            , [Categoria], [AttivoArea1], [AttivoArea2],[AttivoArea3]
            ,[ZeusLangCode],[ZeusId],[ZeusIdModulo],[QtaDisponibile]
            ,[QtaArrivo] ,[PW_Zeus1],[Brand]
            FROM [vwCatalogo_All] 
            WHERE ID1Articolo=@ID1Articolo">
            <SelectParameters>
                <asp:QueryStringParameter Name="ID1Articolo" QueryStringField="XRI" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <div class="VertSpacerMedium">
        </div>
    </asp:Panel>
    <div class="LayGridTitolo1">
        <asp:Label ID="Label2" runat="server" SkinID="GridTitolo1" Text="Elenco contenuti disponibili"></asp:Label>
    </div>
    <asp:GridView ID="GridView2" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" DataSourceID="dsListaCataloghi" OnRowDataBound="GridView1_RowDataBound"
        PageSize="30">
        <Columns>
            <asp:TemplateField HeaderText="AttivoArea1" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="AttivoArea1" runat="server" Text='<%# Eval("AttivoArea1") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="AttivoArea2" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="AttivoArea2" runat="server" Text='<%# Eval("AttivoArea2") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="AttivoArea3" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="AttivoArea3" runat="server" Text='<%# Eval("AttivoArea3") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Codice" HeaderText="Codice" SortExpression="Codice" />
            <asp:BoundField DataField="Articolo" HeaderText="Articolo" SortExpression="Articolo" />
            <asp:BoundField DataField="Brand" HeaderText="Brand" SortExpression="Brand" />
            <asp:BoundField DataField="Categoria" HeaderText="Categoria" SortExpression="Categoria" />
            <asp:TemplateField SortExpression="AttivoArea1">
                <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" /></ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:TemplateField SortExpression="AttivoArea2">
                <ItemTemplate>
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" /></ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:TemplateField SortExpression="AttivoArea3">
                <ItemTemplate>
                    <asp:Image ID="Image3" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" /></ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:TemplateField SortExpression="PW_Zeus1">
                <ItemTemplate>
                    <asp:Image ID="PW_Zeus1Image" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Flag_Off.gif"
                        ToolTip='<%# Eval("PW_Zeus1") %>' OnDataBinding="PW_Zeus1Image_DataBinding" /></ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:BoundField DataField="QtaDisponibile" HeaderText="Disp." SortExpression="QtaDisponibile"
                ItemStyle-HorizontalAlign="Right" />
            <asp:BoundField DataField="QtaArrivo" HeaderText="Arr." SortExpression="QtaArrivo"
                ItemStyle-HorizontalAlign="Right" />
            <asp:TemplateField HeaderText="Sezioni">
                <ItemTemplate>
                    <asp:Label ID="SezioniLabel" runat="server" Text='<%# GetSezioni(Eval("ID1Articolo").ToString()) %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:HyperLinkField DataNavigateUrlFields="ID1Articolo,ZeusLangCode,ZeusIdModulo"
                Text="Modifica" DataNavigateUrlFormatString="Articoli_Edt.aspx?XRI={0}&Lang={1}&ZIM={2}"
                HeaderText="Modifica">
                <ControlStyle CssClass="GridView_ZeusButton1" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:HyperLinkField>
            <asp:HyperLinkField DataNavigateUrlFields="ID1Articolo,ZeusLangCode,ZeusIdModulo"
                Text="Dettaglio" DataNavigateUrlFormatString="Articoli_Dtl.aspx?XRI={0}&Lang={1}&ZIM={2}"
                HeaderText="Dettaglio">
                <ControlStyle CssClass="GridView_ZeusButton1" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:HyperLinkField>
            <%--<asp:HyperLinkField DataNavigateUrlFields="ID1Articolo,ZeusLangCode,ZeusIdModulo"
                DataNavigateUrlFormatString="SavePDF.aspx?XRI={0}&Lang={1}&ZIM={2}" Text="Anteprima"
                Target="_blank">
                <ControlStyle CssClass="GridView_Button1" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:HyperLinkField>--%>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="dsListaCataloghi" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SELECT [ID1Articolo], [Codice], [Articolo], [ID2Categoria1], [Categoria]
        , [AttivoArea1], [AttivoArea2],[AttivoArea3],[ZeusLangCode],[ZeusId],[ZeusIdModulo]
        ,[QtaDisponibile],[QtaArrivo] ,[PW_Zeus1],[Brand]
        FROM [vwCatalogo_All] 
        WHERE ZeusLangCode=@ZeusLangCode AND ZeusIdModulo=@ZeusIdModulo ">
        <SelectParameters>
            <asp:QueryStringParameter Name="ZeusLangCode" QueryStringField="Lang" />
            <asp:QueryStringParameter Name="ZeusIdModulo" QueryStringField="ZIM" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
