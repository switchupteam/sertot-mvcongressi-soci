﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
    
   
    static string TitoloPagina = "Ricerca Articoli";


    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;

        delinea myDelinea = new delinea();

        if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
            || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM")))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        ZIM.Value = Server.HtmlEncode(Request.QueryString["ZIM"]);


        if (!ReadXML(ZIM.Value, GetLang()))
            Response.Write("~/Zeus/System/Message.aspx?1");

        ReadXML_Localization(ZIM.Value, GetLang());


        SetFocus(SearchButton.UniqueID);

    }//fine Page_Load

    private void SetFocus(string ID)
    {
        Page.Form.DefaultButton = ID;
        Page.Form.DefaultFocus = ID;

    }//fine SetFocus


    private string GetLang()
    {
        try
        {
            delinea myDelinea = new delinea();
            string Lang = "ITA";

            if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
            {
                if (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang"))
                    Lang = Request.QueryString["Lang"];
                //else Response.Redirect("~/System/Message.aspx");
            }

            return Lang;
        }
        catch (Exception)
        {
            return "ITA";
        }
    }//fine GetLang


    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Catalogo.xml"));


            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {

                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {

                            if (childNode.Name.Equals("ZML_TitoloPagina_Src"))
                            {
                                TitleField.Value = childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                                else
                                {
                                    myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name + "Label");

                                    if (myLabel != null)
                                        myLabel.Text = childNode.InnerText;
                                }
                            }

                        }
                        catch (Exception)
                        {

                        }
                    }//fine foreach
                }//fine foreach

            }//fine for
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine ReadXML_Localization


    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            string myXPathEach = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_Catalogo.xml"));

            if (!CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_New").InnerText))
                Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223599");


            Panel myPanel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPathEach);

            for (int i = 0; i < nodelist.Count; ++i)
            {

                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {

                            if (childNode.Name.IndexOf("ZMCF_") > -1)
                            {
                                myPanel = (Panel)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myPanel != null)
                                    myPanel.Visible = Convert.ToBoolean(childNode.InnerText);
                            }

                        }
                        catch (Exception)
                        {

                        }
                    }//fine foreach
                }//fine foreach

            }//fine for




            SetQueryOfID2CategoriaDropDownList(mydoc.SelectSingleNode(myXPath + "ZMCD_ZIMCategoria1").InnerText
                , GetLang()
                , mydoc.SelectSingleNode(myXPath + "ZMCF_Cat1Liv").InnerText);









            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }

    }//fine ReadXML


    private bool CheckPermit(string AllRoles)
    {

        bool IsPermit = false;

        try
        {

            if (AllRoles.Length == 0)
                return false;

            char[] delimiter = { ',' };


            foreach (string roles in Roles.GetRolesForUser(Membership.GetUser().UserName))
            {
                string[] PressRoles = AllRoles.Split(delimiter);

                for (int i = 0; i < PressRoles.Length; ++i)
                {
                    if (PressRoles[i].Trim().Equals(roles.Trim()))
                    {
                        IsPermit = true;
                        break;
                    }
                }//fine for

            }//fine else
        }
        catch (Exception)
        { }

        return IsPermit;

    }//fine CheckPermit




    //##############################################################################################################
    //################################################ CATEGORIE ###################################################
    //############################################################################################################## 

    private bool SetQueryOfID2CategoriaDropDownList(string ZeusIdModulo,
        string ZeusLangCode, string PZV_Cat1Liv)
    {
        try
        {


            ID2CategoriaHiddenField.Value = "0";

            switch (PZV_Cat1Liv)
            {

                case "123":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv1Liv2Liv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "Catliv1Liv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;


                case "23":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv2Liv3]";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;

                default:
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;
            }

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }//fine SetQueryOfID2CategoriaDropDownList



    //##############################################################################################################
    //############################################ FINE CATEGORIE ##################################################
    //############################################################################################################## 





    protected void SearchButton_Click(object sender, EventArgs e)
    {
        string MyRedirect = "~/Zeus/Catalogo/Articoli_Lst.aspx";


        MyRedirect += "?ZIM=" + Server.HtmlEncode(Request.QueryString["ZIM"]);
        MyRedirect += "&Lang=" + GetLang();

        if (ArticoloTextBox.Text.Length > 0)
            MyRedirect += "&XRE=" + ArticoloTextBox.Text;


        if (!ID2BrandDropDownList.SelectedValue.Equals("0"))
            MyRedirect += "&XRI1=" + ID2BrandDropDownList.SelectedValue;


        if (CodiceTextBox.Text.Length > 0)
            MyRedirect += "&XRE2=" + CodiceTextBox.Text;


        if (!ID2CategoriaDropDownList.SelectedValue.Equals("0"))
            MyRedirect += "&XRI2=" + ID2CategoriaDropDownList.SelectedValue;

        if (TagTextBox.Text.Length > 0)
            MyRedirect += "&XRE3=" + TagTextBox.Text;


        Response.Redirect(MyRedirect);

    }//fine SearchButton_Click

    
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" />
    <asp:HiddenField ID="ZIM" runat="server" />
    <div class="BlockBox">
        <div class="BlockBoxHeader">
            <asp:Label ID="IdentificativoLabel" runat="server">Identificativo</asp:Label></div>
        <table>
            <tr>
                <td class="BlockBoxDescription">
                    <asp:Label ID="ZML_Articolo" SkinID="FieldDescription" runat="server" Text="Articolo"></asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:TextBox ID="ArticoloTextBox" runat="server" Columns="50" MaxLength="100"></asp:TextBox>
                </td>
            </tr>
            <asp:Panel ID="ZMCF_Brand" runat="server">
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="ZML_Brand" SkinID="FieldDescription" runat="server" Text="Brand"></asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:DropDownList ID="ID2BrandDropDownList" runat="server" AppendDataBoundItems="True"
                            DataSourceID="ID2BrandSqlDataSource" DataTextField="Brand" DataValueField="ID1Brand">
                            <asp:ListItem Text="> Tutte" Value="0" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="ID2BrandSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                            SelectCommand="SELECT ID1Brand,Brand FROM [vwBrands_All] WHERE [AttivoArea1] >0 ORDER BY Brand">
                        </asp:SqlDataSource>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="ZMCF_Codice" runat="server">
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label8" runat="server" SkinID="FieldDescription" Text="Label">Codice</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="CodiceTextBox" runat="server" Columns="25" MaxLength="25"></asp:TextBox>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="ZMCF_Categoria1" runat="server">
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="ZML_Categoria1" runat="server" SkinID="FieldDescription" Text="Label">Categoria *</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:DropDownList ID="ID2CategoriaDropDownList" runat="server" AppendDataBoundItems="True">
                            <asp:ListItem Text="> Tutte" Value="0" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:HiddenField ID="ID2CategoriaHiddenField" runat="server" />
                        <asp:SqlDataSource ID="dsCategoria" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>">
                            <SelectParameters>
                                <asp:QueryStringParameter DefaultValue="0" Name="ZeusIdModulo" QueryStringField="ZIM"
                                    Type="String" />
                                <asp:QueryStringParameter DefaultValue="ITA" Name="ZeusLangCode" QueryStringField="Lang"
                                    Type="String" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="ZMCF_TagRicerca" runat="server">
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label2" runat="server" SkinID="FieldDescription" Text="Label">Tag di ricerca</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TagTextBox" runat="server" Columns="50" MaxLength="100"></asp:TextBox>
                    </td>
                </tr>
            </asp:Panel>
            <tr>
                <td class="BlockBoxDescription">
                </td>
                <td class="BlockBoxValue">
                    <asp:LinkButton SkinID="ZSSM_Button01" ID="SearchButton" runat="server" Text="Cerca"
                        OnClick="SearchButton_Click"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
