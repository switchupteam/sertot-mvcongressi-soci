﻿using ASP;
using System;
using System.Data.SqlClient;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per Articoli_New
/// </summary>
public partial class Catalogo_Articoli_New_Lang : System.Web.UI.Page 
{
    static string TitoloPagina = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            delinea myDelinea = new delinea();

           
            if ((!myDelinea.AntiSQLInjectionNew(Request.QueryString, "ZIM", "string"))
            || (!myDelinea.AntiSQLInjectionNew(Request.QueryString, "ZID", "uid"))
            || (!myDelinea.AntiSQLInjectionNew(Request.QueryString, "LangO", "string")))
                Response.Redirect("~/Zeus/System/Message.aspx?0");

            ZIM.Value = Server.HtmlEncode(Request.QueryString["ZIM"]);
            ZID.Value = Server.HtmlEncode(Request.QueryString["ZID"]);
            Lang.Value = GetLang();
            LangO.Value = Server.HtmlEncode(Request.QueryString["LangO"]);

            if (!ReadXML(ZIM.Value, GetLang()))
                Response.Write("~/Zeus/System/Message.aspx?1");

            ReadXML_Localization(ZIM.Value, GetLang());

            string myJavascript = PrintMyJavaScript1();
            myJavascript += " " + PrintMyJavaScript2();
            myJavascript += " " + PrintMyJavaScript3();
            myJavascript += " " + PrintMyJavaScript4();
            myJavascript += " " + PrintMyJavaScript5();
            myJavascript += " " + PrintMyJavaScript6();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartUpScript", myJavascript, true);
            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");
            InsertButton.Attributes.Add("onclick", "Validate()");

            if (!Page.IsPostBack)
            {
                TextBox Listino1TextBox = (TextBox)FormView1.FindControl("Listino1TextBox");
                TextBox Listino2TextBox = (TextBox)FormView1.FindControl("Listino2TextBox");
                TextBox Listino3TextBox = (TextBox)FormView1.FindControl("Listino3TextBox");
                TextBox QtaDisponibileTextBox = (TextBox)FormView1.FindControl("QtaDisponibileTextBox");
                TextBox QtaArrivoTextBox = (TextBox)FormView1.FindControl("QtaArrivoTextBox");
                Listino1TextBox.Text = Listino2TextBox.Text = Listino3TextBox.Text = QtaDisponibileTextBox.Text = QtaArrivoTextBox.Text = "0";

                GetDataFromLangO();
            }

            Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "val", "fnOnUpdateValidators();");
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";
            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Catalogo.xml"));

            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            if (childNode.Name.Equals("ZML_TitoloPagina_New"))
                            {
                                TitleField.Value = childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                                else
                                {
                                    myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name + "Label");

                                    if (myLabel != null)
                                        myLabel.Text = childNode.InnerText;
                                }
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            string myXPathEach = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_Catalogo.xml"));

            if (!CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_New").InnerText))
                Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223599");

            Panel myPanel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPathEach);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            if (childNode.Name.IndexOf("ZMCF_") > -1)
                            {
                                myPanel = (Panel)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myPanel != null)
                                    myPanel.Visible = Convert.ToBoolean(childNode.InnerText);
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }

            SetQueryOfID2CategoriaDropDownList(mydoc.SelectSingleNode(myXPath + "ZMCD_ZIMCategoria1").InnerText
                , GetLang()
                , mydoc.SelectSingleNode(myXPath + "ZMCD_Cat1Liv").InnerText);

            CategorieMultiple CategorieMultiple1 = (CategorieMultiple)FormView1.FindControl("CategorieMultiple1");

            if (CategorieMultiple1 != null)
            {
                CategorieMultiple1.ZeusIdModulo = mydoc.SelectSingleNode(myXPath + "ZMCD_ZIMCategoria2").InnerText;
                CategorieMultiple1.ZeusLangCode = GetLang();
                CategorieMultiple1.RepeatColumns = mydoc.SelectSingleNode(myXPath + "ZMCD_ColCategoria2").InnerText;
                CategorieMultiple1.GetCategorie();
            }

            TextBox ZMCD_TagDescription = (TextBox)FormView1.FindControl("ZMCD_TagDescription");
            ZMCD_TagDescription.Text = mydoc.SelectSingleNode(myXPath + "ZMCD_TagDescription").InnerText;

            TextBox ZMCD_TagKeywords = (TextBox)FormView1.FindControl("ZMCD_TagKeywords");
            ZMCD_TagKeywords.Text = mydoc.SelectSingleNode(myXPath + "ZMCD_TagKeywords").InnerText;

            TextBox ZCMD_IvaPercentDefault = (TextBox)FormView1.FindControl("ZCMD_IvaPercentDefault");
            ZCMD_IvaPercentDefault.Text = mydoc.SelectSingleNode(myXPath + "ZCMD_IvaPercentDefault").InnerText;

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    private bool CheckPermit(string AllRoles)
    {
        bool IsPermit = false;

        try
        {
            if (AllRoles.Length == 0)
                return false;

            char[] delimiter = { ',' };

            foreach (string roles in Roles.GetRolesForUser(Membership.GetUser().UserName))
            {
                string[] PressRoles = AllRoles.Split(delimiter);

                for (int i = 0; i < PressRoles.Length; ++i)
                {
                    if (PressRoles[i].Trim().Equals(roles.Trim()))
                    {
                        IsPermit = true;
                        break;
                    }
                }
            }
        }
        catch (Exception)
        { }

        return IsPermit;
    }

    private string PrintMyJavaScript1()
    {
        string MyJavaScript = string.Empty;

        MyJavaScript += "function CheckDescriptionLength(sender, args)";
        MyJavaScript += "{";
        MyJavaScript += "if(document.getElementById('";
        MyJavaScript += FormView1.FindControl("RequiredFile").ClientID;
        MyJavaScript += "').checked || document.getElementById('";
        MyJavaScript += FormView1.FindControl("RequiredURL").ClientID;
        MyJavaScript += "').checked)";
        MyJavaScript += "if(document.getElementById('";
        MyJavaScript += FormView1.FindControl("Allegato1_DescTextBox").ClientID;
        MyJavaScript += "').value.length<=0)";
        MyJavaScript += "args.IsValid = false;";
        MyJavaScript += "return;";
        MyJavaScript += "}";

        return MyJavaScript;
    }

    private string PrintMyJavaScript2()
    {
        string MyJavaScript = string.Empty;
        UploadRaider Allegato_UploadRaider1 = (UploadRaider)FormView1.FindControl("Allegato_UploadRaider1");

        MyJavaScript += "function CheckUploadRaider(sender, args)";
        MyJavaScript += " { ";
        MyJavaScript += "if(document.getElementById('";
        MyJavaScript += FormView1.FindControl("RequiredFile").ClientID;
        MyJavaScript += "').checked) {";
        MyJavaScript += " if(document.getElementById('" + Allegato_UploadRaider1.INFOClientID + "').innerHTML.length>0) ";
        MyJavaScript += " args.IsValid = true; ";
        MyJavaScript += "  else   args.IsValid = false; ";
        MyJavaScript += " } ";
        MyJavaScript += " return; ";
        MyJavaScript += " } ";

        return MyJavaScript;
    }

    private string PrintMyJavaScript3()
    {
        string MyJavaScript = string.Empty;

        MyJavaScript += "function CheckUrlLength(sender, args)";
        MyJavaScript += "{";
        MyJavaScript += "if(document.getElementById('";
        MyJavaScript += FormView1.FindControl("RequiredURL").ClientID;
        MyJavaScript += "').checked";
        MyJavaScript += " && document.getElementById('";
        MyJavaScript += FormView1.FindControl("Allegato1_UrlEsternoTextBox").ClientID;
        MyJavaScript += "').value.length<=0)";
        MyJavaScript += "args.IsValid = false;";
        MyJavaScript += "return;";
        MyJavaScript += "}";

        return MyJavaScript;
    }

    private string PrintMyJavaScript4()
    {
        string MyJavaScript = string.Empty;

        MyJavaScript += "function CheckDescriptionLength2(sender, args)";
        MyJavaScript += "{";
        MyJavaScript += "if(document.getElementById('";
        MyJavaScript += FormView1.FindControl("RequiredFile2").ClientID;
        MyJavaScript += "').checked || document.getElementById('";
        MyJavaScript += FormView1.FindControl("RequiredURL2").ClientID;
        MyJavaScript += "').checked)";
        MyJavaScript += "if(document.getElementById('";
        MyJavaScript += FormView1.FindControl("Allegato1_DescTextBox2").ClientID;
        MyJavaScript += "').value.length<=0)";
        MyJavaScript += "args.IsValid = false;";
        MyJavaScript += "return;";
        MyJavaScript += "}";

        return MyJavaScript;
    }

    private string PrintMyJavaScript5()
    {
        string MyJavaScript = string.Empty;
        UploadRaider Allegato_UploadRaider2 = (UploadRaider)FormView1.FindControl("Allegato_UploadRaider2");

        MyJavaScript += "function CheckUploadRaider2(sender, args)";
        MyJavaScript += " { ";
        MyJavaScript += "if(document.getElementById('";
        MyJavaScript += FormView1.FindControl("RequiredFile2").ClientID;
        MyJavaScript += "').checked) {";
        MyJavaScript += " if(document.getElementById('" + Allegato_UploadRaider2.INFOClientID + "').innerHTML.length>0) ";
        MyJavaScript += " args.IsValid = true; ";
        MyJavaScript += "  else   args.IsValid = false; ";
        MyJavaScript += " } ";
        MyJavaScript += " return; ";
        MyJavaScript += " } ";

        return MyJavaScript;
    }

    private string PrintMyJavaScript6()
    {
        string MyJavaScript = string.Empty;

        MyJavaScript += "function CheckUrlLength2(sender, args)";
        MyJavaScript += "{";
        MyJavaScript += "if(document.getElementById('";
        MyJavaScript += FormView1.FindControl("RequiredURL2").ClientID;
        MyJavaScript += "').checked";
        MyJavaScript += " && document.getElementById('";
        MyJavaScript += FormView1.FindControl("Allegato1_UrlEsternoTextBox2").ClientID;
        MyJavaScript += "').value.length<=0)";
        MyJavaScript += "args.IsValid = false;";
        MyJavaScript += "return;";
        MyJavaScript += "}";

        return MyJavaScript;
    }

    private string GetLang()
    {
        try
        {
            delinea myDelinea = new delinea();
            string Lang = "ITA";

            if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
            {
                if (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang"))
                    Lang = Request.QueryString["Lang"];
            }

            return Lang;
        }
        catch (Exception)
        {
            return "ITA";
        }
    }

    private bool SetQueryOfID2CategoriaDropDownList(string ZeusIdModulo,
        string ZeusLangCode, string PZV_Cat1Liv)
    {
        try
        {
            SqlDataSource dsCategoria = (SqlDataSource)FormView1.FindControl("dsCategoria");
            DropDownList ID2CategoriaDropDownList = (DropDownList)FormView1.FindControl("ID2CategoriaDropDownList");
            HiddenField ID2CategoriaHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaHiddenField");
            ID2CategoriaHiddenField.Value = "0";

            switch (PZV_Cat1Liv)
            {

                case "123":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv1Liv2Liv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "Catliv1Liv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;


                case "23":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv2Liv3]";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;

                default:
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;
            }

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    protected void ID2CategoriaDropDownList_SelectIndexChange(object sender, EventArgs e)
    {
        HiddenField ID2CategoriaHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaHiddenField");
        DropDownList ID2CategoriaDropDownList = (DropDownList)sender;

        ID2CategoriaHiddenField.Value = ID2CategoriaDropDownList.SelectedValue;
    }


    protected void RecordNewUserHidden_DataBinding(object sender, EventArgs e)
    {
        HiddenField UtenteCreazione = (HiddenField)sender;
        UtenteCreazione.Value = Membership.GetUser().ProviderUserKey.ToString();
    }

    protected void RecordNewDateHidden_DataBinding(object sender, EventArgs e)
    {
        HiddenField DataCreazione = (HiddenField)sender;
        DataCreazione.Value = DateTime.Now.ToString();
    }

    protected void ZeusLangCodeHiddenField_DataBinding(object sender, EventArgs e)
    {
        HiddenField ZeusLangCodeHiddenField = (HiddenField)sender;
        ZeusLangCodeHiddenField.Value = GetLang();
    }

    protected void PWI_Area(object sender, EventArgs e)
    {
        DateTime Data = new DateTime();
        Data = DateTime.Now;
        TextBox DataCreazione = (TextBox)sender;
        if (DataCreazione.Text == string.Empty)
        {
            DataCreazione.Text = Data.ToString("dd/MM/yyyy");
        }
    }

    protected void PWF_Area(object sender, EventArgs e)
    {
        TextBox DataFinale = (TextBox)sender;
        if (DataFinale.Text == string.Empty)
        {
            DataFinale.Text = "31/12/2040";
        }
    }

    protected void HiddenImageFile_DataBinding(object sender, EventArgs e)
    {
        HiddenField HiddenImageFile = (HiddenField)sender;
        if (HiddenImageFile.Value.Length == 0)
            HiddenImageFile.Value = "ImgNonDisponibile.jpg";
    }

    protected void FileUploadCustomValidator_ServerValidate(object sender, ServerValidateEventArgs args)
    {
        try
        {
            ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");
            ImageRaider ImageRaider2 = (ImageRaider)FormView1.FindControl("ImageRaider2");
            ImageRaider ImageRaider3 = (ImageRaider)FormView1.FindControl("ImageRaider3");
            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");

            if ((ImageRaider1.isValid())&&(ImageRaider2.isValid())&&(ImageRaider3.isValid()))
                InsertButton.CommandName = "Insert";
            else
                InsertButton.CommandName = string.Empty;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }


    protected void ImageRaider_DataBinding(object sender, EventArgs e)
    {
        ImageRaider ImageRaider = (ImageRaider)sender;
        ImageRaider.SetDefaultEditBetaImage();
        ImageRaider.SetDefaultEditGammaImage();
    }

    protected void Check_RadioButton_Validate(object sender, ServerValidateEventArgs args)
    {
        HiddenField HiddenNomeFile = (HiddenField)FormView1.FindControl("Allegato1_File");
        HiddenField Allegato_Peso = (HiddenField)FormView1.FindControl("Allegato_Peso");
        RadioButton SelettoreNull = (RadioButton)FormView1.FindControl("SelettoreNull");
        RadioButton RequiredURL = (RadioButton)FormView1.FindControl("RequiredURL");
        RadioButton RequiredFile = (RadioButton)FormView1.FindControl("RequiredFile");
        TextBox Allegato1_DescTextBox = (TextBox)FormView1.FindControl("Allegato1_DescTextBox");
        TextBox Allegato1_UrlEsternoTextBox = (TextBox)FormView1.FindControl("Allegato1_UrlEsternoTextBox");
        CustomValidator CustomValidator1 = (CustomValidator)sender;

        if (!SelettoreNull.Checked)
        {
            if ((Allegato1_DescTextBox.Visible) && (Allegato1_DescTextBox.Text.Length == 0))
            {
                CustomValidator1.ErrorMessage = "Obbligatorio";
                args.IsValid = false;
                HiddenNomeFile.Value = null;
                Allegato_Peso.Value = null;

            }
            else if ((RequiredURL.Checked) && (Allegato1_UrlEsternoTextBox.Text.Length == 0))
            {
                CustomValidator1.ErrorMessage = "Url obbligatorio";
                args.IsValid = false;
            }

            else args.IsValid = true;
        }
    }

    protected void RadioButton1_CheckedChanged(object sender, EventArgs e)
    {
        HiddenField Allegato_Selettore = (HiddenField)FormView1.FindControl("Allegato_Selettore");
        Allegato_Selettore.Value = "FIL";
    }

    protected void RadioButton2_CheckedChanged(object sender, EventArgs e)
    {
        HiddenField Allegato_Selettore = (HiddenField)FormView1.FindControl("Allegato_Selettore");
        Allegato_Selettore.Value = "LNK";
    }

    protected void SelettoreNull_CheckedChanged(object sender, EventArgs e)
    {
        HiddenField Allegato_Selettore = (HiddenField)FormView1.FindControl("Allegato_Selettore");
        Allegato_Selettore.Value = string.Empty;
    }

    protected void Check_RadioButton_Validate2(object sender, ServerValidateEventArgs args)
    {
        HiddenField HiddenNomeFile = (HiddenField)FormView1.FindControl("Allegato1_File2");
        HiddenField Allegato_Peso = (HiddenField)FormView1.FindControl("Allegato_Peso2");
        RadioButton SelettoreNull = (RadioButton)FormView1.FindControl("SelettoreNull2");
        RadioButton RequiredURL = (RadioButton)FormView1.FindControl("RequiredURL2");
        RadioButton RequiredFile = (RadioButton)FormView1.FindControl("RequiredFile2");
        TextBox Allegato1_DescTextBox = (TextBox)FormView1.FindControl("Allegato1_DescTextBox2");
        TextBox Allegato1_UrlEsternoTextBox = (TextBox)FormView1.FindControl("Allegato1_UrlEsternoTextBox2");
        CustomValidator CustomValidator1 = (CustomValidator)sender;

        if (!SelettoreNull.Checked)
        {
            if ((Allegato1_DescTextBox.Visible) && (Allegato1_DescTextBox.Text.Length == 0))
            {
                CustomValidator1.ErrorMessage = "Obbligatorio";
                args.IsValid = false;
                HiddenNomeFile.Value = null;
                Allegato_Peso.Value = null;

            }
            else if ((RequiredURL.Checked) && (Allegato1_UrlEsternoTextBox.Text.Length == 0))
            {
                CustomValidator1.ErrorMessage = "Url obbligatorio";
                args.IsValid = false;
            }

            else args.IsValid = true;
        }
    }

    protected void RadioButton1_CheckedChanged2(object sender, EventArgs e)
    {
        HiddenField Allegato_Selettore = (HiddenField)FormView1.FindControl("Allegato_Selettore2");
        Allegato_Selettore.Value = "FIL";
    }

    protected void RadioButton2_CheckedChanged2(object sender, EventArgs e)
    {
        HiddenField Allegato_Selettore = (HiddenField)FormView1.FindControl("Allegato_Selettore2");
        Allegato_Selettore.Value = "LNK";
    }

    protected void SelettoreNull_CheckedChanged2(object sender, EventArgs e)
    {
        HiddenField Allegato_Selettore = (HiddenField)FormView1.FindControl("Allegato_Selettore2");
        Allegato_Selettore.Value = string.Empty;
    }

    private string AddZero(string MyString)
    {
        switch (MyString.Length)
        {
            case 1:
                MyString = "000" + MyString;
                break;

            case 2:
                MyString = "00" + MyString;
                break;

            case 3:
                MyString = "0" + MyString;
                break;
        }

        return MyString;
    }

    private bool MakeAndSaveUrlRewrite(string Title, string ID1Articolo)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;
        delinea myDelinea = new delinea();
        string UrlRewrite = myDelinea.myHtmlEncode(Title);

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {
            SqlTransaction transaction = null;

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                transaction = connection.BeginTransaction();
                command.Transaction = transaction;

                sqlQuery = "UPDATE tbCatalogo SET UrlRewrite='" + UrlRewrite + "' WHERE ID1Articolo=" + ID1Articolo;

                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();
                transaction.Commit();
                return true;
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
                transaction.Rollback();
                return false;
            }
        }
    }

    protected void InsertButton_Click(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();
        LinkButton InsertButton = (LinkButton)sender;
        RadioButton AutomaticaThumbnailRadioButton = (RadioButton)FormView1.FindControl("AutomaticaThumbnailRadioButton");
        RadioButton ManualeThumbnailRadioButton = (RadioButton)FormView1.FindControl("ManualeThumbnailRadioButton");
        HiddenField FileNameBetaHiddenField = (HiddenField)FormView1.FindControl("FileNameBetaHiddenField");
        HiddenField FileNameGammaHiddenField = (HiddenField)FormView1.FindControl("FileNameGammaHiddenField");
        HiddenField Immagine12AltHiddenField = (HiddenField)FormView1.FindControl("Immagine12AltHiddenField");
        ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");
        HiddenField FileNameBeta2HiddenField = (HiddenField)FormView1.FindControl("FileNameBeta2HiddenField");
        HiddenField FileNameGamma2HiddenField = (HiddenField)FormView1.FindControl("FileNameGamma2HiddenField");
        HiddenField Immagine34AltHiddenField = (HiddenField)FormView1.FindControl("Immagine34AltHiddenField");
        ImageRaider ImageRaider2 = (ImageRaider)FormView1.FindControl("ImageRaider2");
        HiddenField FileNameBeta3HiddenField = (HiddenField)FormView1.FindControl("FileNameBeta3HiddenField");
        HiddenField FileNameGamma3HiddenField = (HiddenField)FormView1.FindControl("FileNameGamma3HiddenField");
        HiddenField Immagine56AltHiddenField = (HiddenField)FormView1.FindControl("Immagine56AltHiddenField");
        ImageRaider ImageRaider3 = (ImageRaider)FormView1.FindControl("ImageRaider3");
        string filePath = string.Empty;
        string fileName = string.Empty;

        if (InsertButton.CommandName != string.Empty)
        {
            if (ImageRaider1.GenerateBeta(string.Empty))
                FileNameBetaHiddenField.Value = ImageRaider1.ImgBeta_FileName;
            else FileNameBetaHiddenField.Value = ImageRaider1.DefaultBetaImage;

            if (ImageRaider1.GenerateGamma(string.Empty))
                FileNameGammaHiddenField.Value = ImageRaider1.ImgGamma_FileName;
            else FileNameGammaHiddenField.Value = ImageRaider1.DefaultGammaImage;

            if (ImageRaider2.GenerateBeta(string.Empty))
                FileNameBeta2HiddenField.Value = ImageRaider2.ImgBeta_FileName;
            else FileNameBeta2HiddenField.Value = ImageRaider2.DefaultBetaImage;

            if (ImageRaider2.GenerateGamma(string.Empty))
                FileNameGamma2HiddenField.Value = ImageRaider2.ImgGamma_FileName;
            else FileNameGamma2HiddenField.Value = ImageRaider2.DefaultGammaImage;

            if (ImageRaider3.GenerateBeta(string.Empty))
                FileNameBeta3HiddenField.Value = ImageRaider3.ImgBeta_FileName;
            else FileNameBeta3HiddenField.Value = ImageRaider3.DefaultBetaImage;

            if (ImageRaider3.GenerateGamma(string.Empty))
                FileNameGamma3HiddenField.Value = ImageRaider3.ImgGamma_FileName;
            else FileNameGamma3HiddenField.Value = ImageRaider3.DefaultGammaImage;

            HiddenField Allegato1_File = (HiddenField)FormView1.FindControl("Allegato1_File");
            UploadRaider Allegato_UploadRaider1 = (UploadRaider)FormView1.FindControl("Allegato_UploadRaider1");

            if ((Allegato_UploadRaider1 != null)
                && (Allegato_UploadRaider1.GotFile()))
            {
                Allegato_UploadRaider1.SaveFile();
                Allegato1_File.Value = Allegato_UploadRaider1.FileName;
            }

            HiddenField Allegato2_File = (HiddenField)FormView1.FindControl("Allegato2_File");
            UploadRaider Allegato_UploadRaider2 = (UploadRaider)FormView1.FindControl("Allegato_UploadRaider2");

            if ((Allegato_UploadRaider2 != null)
                && (Allegato_UploadRaider2.GotFile()))
            {
                Allegato_UploadRaider2.SaveFile();
                Allegato2_File.Value = Allegato_UploadRaider2.FileName;
            }
        }
    }

    protected void dsArticoloNew_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        try
        {
            TextBox ArticoloTextBox = (TextBox)FormView1.FindControl("ArticoloTextBox");

            if ((e.Exception == null)
                && (MakeAndSaveUrlRewrite(ArticoloTextBox.Text, e.Command.Parameters["@XRI"].Value.ToString())))
            {
                CategorieMultiple CategorieMultiple1 = (CategorieMultiple)FormView1.FindControl("CategorieMultiple1");
                CategorieMultiple1.PAGE_ID = e.Command.Parameters["@XRI"].Value.ToString();

                CategorieMultiple1.SetCategorie();

                Response.Redirect("~/Zeus/Catalogo/Articoli_Lst.aspx?XRI="
                    + e.Command.Parameters["@XRI"].Value.ToString()
                    + "&ZIM=" + ZIM.Value
                    + "&Lang=" + GetLang());
            }
            else
                Response.Write("~/Zeus/System/Message.aspx?InsertErr");
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    private bool GetDataFromLangO()
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        try
        {
            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = @"SELECT        dbo.tbCatalogo.*,  dbo.tbBrands.Brand, dbo.tbCategorie.Categoria
                            FROM            dbo.tbCatalogo INNER JOIN
                            dbo.tbBrands ON dbo.tbCatalogo.ID2Brand = dbo.tbBrands.ID1Brand 
                            INNER JOIN
                            dbo.tbCategorie ON dbo.tbCatalogo.ID2Categoria1 = dbo.tbCategorie.ID1Categoria
                            WHERE (dbo.tbCatalogo.ZeusIsAlive = 1) 
                                AND dbo.tbCatalogo.ZeusId=@ZeusId 
                                AND dbo.tbCatalogo.ZeusLangCode=@ZeusLangCode";

                command.CommandText = sqlQuery;
                command.Parameters.Add("@ZeusId", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@ZeusId"].Value = new Guid(ZID.Value);
                command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ZeusLangCode"].Value = LangO.Value;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (reader["Articolo"] != DBNull.Value)
                    {
                        Label ArticoloLangLabel = (Label)FormView1.FindControl("ArticoloLangLabel");
                        ArticoloLangLabel.Text = reader["Articolo"].ToString();
                    }

                    if (reader["Brand"] != DBNull.Value)
                    {
                        Label BrandLangLabel = (Label)FormView1.FindControl("BrandLangLabel");
                        BrandLangLabel.Text = reader["Brand"].ToString();
                    }

                    if (reader["Codice"] != DBNull.Value)
                    {
                        Label CodiceLangLabel = (Label)FormView1.FindControl("CodiceLangLabel");
                        CodiceLangLabel.Text = reader["Codice"].ToString();
                    }

                    if (reader["Categoria"] != DBNull.Value)
                    {
                        Label CategoriaLangLabel = (Label)FormView1.FindControl("CategoriaLangLabel");
                        CategoriaLangLabel.Text = reader["Categoria"].ToString();
                    }

                    if (reader["ZeusTags"] != DBNull.Value)
                    {
                        Label TagLangLabel = (Label)FormView1.FindControl("TagLangLabel");
                        TagLangLabel.Text = reader["ZeusTags"].ToString();
                    }

                    if (reader["DescBreve1"] != DBNull.Value)
                    {
                        Label DescrizioneLangLabel = (Label)FormView1.FindControl("DescrizioneLangLabel");
                        DescrizioneLangLabel.Text = reader["DescBreve1"].ToString();
                    }

                    if (reader["Descrizione1"] != DBNull.Value)
                    {
                        Label Descrizione1LangLabel = (Label)FormView1.FindControl("Descrizione1LangLabel");
                        Descrizione1LangLabel.Text = reader["Descrizione1"].ToString();
                    }

                    if (reader["Descrizione2"] != DBNull.Value)
                    {
                        Label Descrizione2LangLabel = (Label)FormView1.FindControl("Descrizione2LangLabel");
                        Descrizione2LangLabel.Text = reader["Descrizione2"].ToString();
                    }

                    if (reader["TagDescription"] != DBNull.Value)
                    {
                        Label TagDescriptionLangLabel = (Label)FormView1.FindControl("TagDescriptionLangLabel");
                        TagDescriptionLangLabel.Text = reader["TagDescription"].ToString();
                    }

                    if (reader["TagKeywords"] != DBNull.Value)
                    {
                        Label TagKeywordsLangLabel = (Label)FormView1.FindControl("TagKeywordsLangLabel");
                        TagKeywordsLangLabel.Text = reader["TagKeywords"].ToString();
                    }

                    if (reader["Immagine1"] != DBNull.Value)
                    {
                        HiddenField FileNameBetaHiddenField = (HiddenField)FormView1.FindControl("FileNameBetaHiddenField");
                        FileNameBetaHiddenField.Value = reader["Immagine1"].ToString();
                        ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");
                        ImageRaider1.DefaultEditBetaImage = reader["Immagine1"].ToString();
                        ImageRaider1.SetDefaultEditBetaImage();
                        ImageRaider1.DefaultBetaImage = reader["Immagine1"].ToString();
                    }

                    if (reader["Immagine2"] != DBNull.Value)
                    {
                        HiddenField FileNameGammaHiddenField = (HiddenField)FormView1.FindControl("FileNameGammaHiddenField");
                        FileNameGammaHiddenField.Value = reader["Immagine2"].ToString();
                        ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");
                        ImageRaider1.DefaultEditGammaImage = reader["Immagine2"].ToString();
                        ImageRaider1.SetDefaultEditGammaImage();
                        ImageRaider1.DefaultGammaImage = reader["Immagine2"].ToString();
                    }

                    if (reader["Immagine3"] != DBNull.Value)
                    {
                        HiddenField FileNameBetaHiddenField = (HiddenField)FormView1.FindControl("FileNameBeta2HiddenField");
                        FileNameBetaHiddenField.Value = reader["Immagine3"].ToString();
                        ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider2");
                        ImageRaider1.DefaultEditBetaImage = reader["Immagine3"].ToString();
                        ImageRaider1.SetDefaultEditBetaImage();
                        ImageRaider1.DefaultBetaImage = reader["Immagine3"].ToString();
                    }

                    if (reader["Immagine4"] != DBNull.Value)
                    {
                        HiddenField FileNameGammaHiddenField = (HiddenField)FormView1.FindControl("FileNameGamma2HiddenField");
                        FileNameGammaHiddenField.Value = reader["Immagine4"].ToString();
                        ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider2");
                        ImageRaider1.DefaultEditGammaImage = reader["Immagine4"].ToString();
                        ImageRaider1.SetDefaultEditGammaImage();
                        ImageRaider1.DefaultGammaImage = reader["Immagine4"].ToString();
                    }

                    if (reader["Immagine5"] != DBNull.Value)
                    {
                        HiddenField FileNameBetaHiddenField = (HiddenField)FormView1.FindControl("FileNameBeta3HiddenField");
                        FileNameBetaHiddenField.Value = reader["Immagine5"].ToString();
                        ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider3");
                        ImageRaider1.DefaultEditBetaImage = reader["Immagine5"].ToString();
                        ImageRaider1.SetDefaultEditBetaImage();
                        ImageRaider1.DefaultBetaImage = reader["Immagine5"].ToString();
                    }

                    if (reader["Immagine6"] != DBNull.Value)
                    {
                        HiddenField FileNameGammaHiddenField = (HiddenField)FormView1.FindControl("FileNameGamma3HiddenField");
                        FileNameGammaHiddenField.Value = reader["Immagine6"].ToString();
                        ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider3");
                        ImageRaider1.DefaultEditGammaImage = reader["Immagine6"].ToString();
                        ImageRaider1.SetDefaultEditGammaImage();
                        ImageRaider1.DefaultGammaImage = reader["Immagine6"].ToString();
                    }

                    if (reader["SpecificaInt1"] != DBNull.Value)
                    {
                        Label SpecificaInt1LangLabel = (Label)FormView1.FindControl("SpecificaInt1LangLabel");
                        SpecificaInt1LangLabel.Text = reader["SpecificaInt1"].ToString();
                    }

                    if (reader["SpecificaText1"] != DBNull.Value)
                    {
                        Label SpecificaText1LangLabel = (Label)FormView1.FindControl("SpecificaText1LangLabel");
                        SpecificaText1LangLabel.Text = reader["SpecificaText1"].ToString();
                    }

                    if (reader["SpecificaText1"] != DBNull.Value)
                    {
                        Label SpecificaText1LangLabel = (Label)FormView1.FindControl("SpecificaText1LangLabel");
                        SpecificaText1LangLabel.Text = reader["SpecificaText1"].ToString();
                    }

                    if (reader["SpecificaText2"] != DBNull.Value)
                    {
                        Label SpecificaText2LangLabel = (Label)FormView1.FindControl("SpecificaText2LangLabel");
                        SpecificaText2LangLabel.Text = reader["SpecificaText2"].ToString();
                    }

                    if (reader["QtaDisponibile"] != DBNull.Value)
                    {
                        Label QtaDisponibileLangLabel = (Label)FormView1.FindControl("QtaDisponibileLangLabel");
                        QtaDisponibileLangLabel.Text = reader["QtaDisponibile"].ToString();
                    }

                    if (reader["QtaArrivo"] != DBNull.Value)
                    {
                        Label QtaArrivoLangLabel = (Label)FormView1.FindControl("QtaArrivoLangLabel");
                        QtaArrivoLangLabel.Text = reader["QtaArrivo"].ToString();
                    }

                    if (reader["Peso"] != DBNull.Value)
                    {
                        Label PesoLangLabel = (Label)FormView1.FindControl("PesoLangLabel");
                        PesoLangLabel.Text = reader["Peso"].ToString();
                    }

                    if (reader["UnitaMisura"] != DBNull.Value)
                    {
                        Label UnitaMisuraLangLabel = (Label)FormView1.FindControl("UnitaMisuraLangLabel");
                        UnitaMisuraLangLabel.Text = reader["UnitaMisura"].ToString();
                    }

                    if (reader["Listino1"] != DBNull.Value)
                    {
                        Label Listino1LangLabel = (Label)FormView1.FindControl("Listino1LangLabel");
                        Listino1LangLabel.Text = reader["Listino1"].ToString();
                    }

                    if (reader["Listino2"] != DBNull.Value)
                    {
                        Label Listino2LangLabel = (Label)FormView1.FindControl("Listino2LangLabel");
                        Listino2LangLabel.Text = reader["Listino2"].ToString();
                    }

                    if (reader["Listino3"] != DBNull.Value)
                    {
                        Label Listino3LangLabel = (Label)FormView1.FindControl("Listino3LangLabel");
                        Listino3LangLabel.Text = reader["Listino3"].ToString();
                    }

                    if (reader["Listino4"] != DBNull.Value)
                    {
                        Label Listino4LangLabel = (Label)FormView1.FindControl("Listino4LangLabel");
                        Listino4LangLabel.Text = reader["Listino4"].ToString();
                    }

                    if (reader["Listino5"] != DBNull.Value)
                    {
                        Label Listino5LangLabel = (Label)FormView1.FindControl("Listino5LangLabel");
                        Listino5LangLabel.Text = reader["Listino5"].ToString();
                    }

                    if (reader["IvaPercent"] != DBNull.Value)
                    {
                        Label IvaPercentLangLabel = (Label)FormView1.FindControl("IvaPercentLangLabel");
                        IvaPercentLangLabel.Text = reader["IvaPercent"].ToString();
                    }

                    if (reader["Allegato1_Desc"] != DBNull.Value)
                    {
                        Label Allegato1_DescLangLabel = (Label)FormView1.FindControl("Allegato1_DescLangLabel");
                        Allegato1_DescLangLabel.Text = reader["Allegato1_Desc"].ToString();
                    }

                    if (reader["Allegato2_Desc"] != DBNull.Value)
                    {
                        Label Allegato2_DescLangLabel = (Label)FormView1.FindControl("Allegato2_DescLangLabel");
                        Allegato2_DescLangLabel.Text = reader["Allegato2_Desc"].ToString();
                    }
                    
                    HiddenField ZeusId = (HiddenField)FormView1.FindControl("ZeusId");
                    ZeusId.Value = ZID.Value;
                }

                reader.Close();
                return true;
            }
        }
        catch (Exception p)
        {

            Response.Write(p.ToString());
            return false;
        }

    }
}