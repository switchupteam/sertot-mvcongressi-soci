﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">
    
   
    static string TitoloPagina = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {

        TitleField.Value = "Aggiornamento visualizzazioni di massa";

        delinea myDelinea = new delinea();

        if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
            || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM")))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        if (!populatePage(Request.QueryString["Lang"], Request.QueryString["ZIM"]))
            Response.Write("~/Zeus/System/Message.aspx?1");

    }//fine Page_Load



    private bool populatePage(string ZeusLangCode, string ZeusIdModulo)
    {
        try
        {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
            string sqlQuery = "SELECT PZL_PWArea1_Lst ";
            sqlQuery += " ,PZL_PWArea2_Lst,PZV_Area2_Lst,PZL_PWArea3_Lst,PZV_Area3_Lst";
            sqlQuery += " ,PZV_AreaZeus_Lst,PZL_PWAreaZeus_Lst";
            sqlQuery += " FROM tbPZ_Catalogo";
            sqlQuery += " WHERE  ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "' and ZeusIdModulo='" + Server.HtmlEncode(ZeusIdModulo) + "'";

            using (SqlConnection conn = new SqlConnection(strConnessione))
            {
                SqlCommand command = new SqlCommand(sqlQuery, conn);


                conn.Open();
                SqlDataReader reader = command.ExecuteReader();

                if (!reader.HasRows)
                {
                    reader.Close();
                    return false;
                }

                while (reader.Read())
                {
                    if (reader["PZV_AreaZeus_Lst"] != DBNull.Value)
                        GridView1.Columns[14].Visible = Convert.ToBoolean(reader["PZV_AreaZeus_Lst"]);

                    if (reader["PZL_PWAreaZeus_Lst"] != DBNull.Value)
                        GridView1.Columns[14].HeaderText = reader["PZL_PWAreaZeus_Lst"].ToString();

                    if (reader["PZL_PWArea1_Lst"] != DBNull.Value)
                        GridView1.Columns[2].HeaderText = reader["PZL_PWArea1_Lst"].ToString();

                    if (reader["PZL_PWArea2_Lst"] != DBNull.Value)
                        GridView1.Columns[6].HeaderText = reader["PZL_PWArea2_Lst"].ToString();

                    if (reader["PZV_Area2_Lst"] != DBNull.Value)
                        GridView1.Columns[6].Visible =
                            GridView1.Columns[7].Visible =
                            GridView1.Columns[8].Visible =
                            GridView1.Columns[9].Visible = Convert.ToBoolean(reader["PZV_Area2_Lst"]);

                    if (reader["PZL_PWArea3_Lst"] != DBNull.Value)
                        GridView1.Columns[10].HeaderText = reader["PZL_PWArea3_Lst"].ToString();

                    if (reader["PZV_Area3_Lst"] != DBNull.Value)
                        GridView1.Columns[10].Visible =
                            GridView1.Columns[11].Visible =
                            GridView1.Columns[12].Visible =
                            GridView1.Columns[13].Visible = Convert.ToBoolean(reader["PZV_Area3_Lst"]);


                }//fine while
                reader.Close();
                conn.Close();
            }//fine using

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }//fine populatePage


    protected void Image_DataBinding(object sender, EventArgs e)
    {

        Image Image1 = (Image)sender;
        try
        {
            if (Convert.ToBoolean(Image1.ToolTip))
                Image1.ImageUrl = "~/Zeus/SiteImg/Ico1_Flag_On.gif";
        }
        catch (Exception)
        { }

        Image1.ToolTip = string.Empty;

    }//fine Image_DataBinding



    protected void ImageAttivo_DataBinding(object sender, EventArgs e)
    {

        Image Image1 = (Image)sender;
        try
        {
            if (Convert.ToBoolean(Image1.ToolTip))
                Image1.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";
        }
        catch (Exception)
        { }

        Image1.ToolTip = string.Empty;

    }//fine ImageAttivo_DataBinding

    protected void ImageAttivoEval_DataBinding(object sender, EventArgs e)
    {

        Image Image1 = (Image)sender;

        try
        {

            if (Convert.ToInt32(Image1.ToolTip) > 0)
                Image1.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";
        }
        catch (Exception)
        { }

        Image1.ToolTip = string.Empty;

    }//fine ImageAttivo_DataBinding
    


</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" DataKeyNames="ID1Articolo" DataSourceID="PZ_Catalogo_SqlDataSource">
        <Columns>
            <asp:TemplateField HeaderText="Categoria" SortExpression="Categoria">
                <ItemTemplate>
                    <asp:Label ID="CategoriaLabel" runat="server" Text='<%# Eval("Categoria") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Articolo" SortExpression="Articolo">
                <ItemTemplate>
                    <asp:Label ID="ArticoloLabel" runat="server" Text='<%# Eval("Articolo") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Image ID="AttivoArea1NewImage" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif"
                        ToolTip='<%# Eval("AttivoArea1") %>' OnDataBinding="ImageAttivoEval_DataBinding" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:Image ID="AttivoArea1EdtImage" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif"
                        ToolTip='<%# Eval("AttivoArea1") %>' OnDataBinding="ImageAttivoEval_DataBinding" />
                </EditItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Pub.">
                <ItemTemplate>
                    <asp:Image ID="PW_Area1Image" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Flag_Off.gif"
                        ToolTip='<%# Eval("PW_Area1") %>' OnDataBinding="Image_DataBinding" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:CheckBox ID="PW_Area1CheckBox" runat="server" Checked='<%# Bind("PW_Area1") %>' />
                </EditItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Data inizio">
                <ItemTemplate>
                    <asp:Label ID="PWI_Area1Label" runat="server" Text='<%# Eval("PWI_Area1","{0:d}") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="PWI_Area1TextBox" runat="server" Columns="10" MaxLength="10" Text='<%# Bind("PWI_Area1", "{0:d}") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator445"
                        runat="server" ControlToValidate="PWI_Area1TextBox" SkinID="ZSSM_Validazione01"
                        Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="RegularExpressionValidator3345"
                        runat="server" ControlToValidate="PWI_Area1TextBox" SkinID="ZSSM_Validazione01"
                        Display="Dynamic" ErrorMessage="Formato data richiesto: GG/MM/AAAA" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>
                </EditItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Data fine">
                <ItemTemplate>
                    <asp:Label ID="PWF_Area1Label" runat="server" Text='<%# Eval("PWF_Area1","{0:d}") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="PWF_Area1TextBox" runat="server" Columns="10" MaxLength="10" Text='<%# Bind("PWF_Area1", "{0:d}") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator834"
                        runat="server" ControlToValidate="PWF_Area1TextBox" SkinID="ZSSM_Validazione01"
                        Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="RegularExpressionValidator545"
                        runat="server" ControlToValidate="PWF_Area1TextBox" SkinID="ZSSM_Validazione01"
                        Display="Dynamic" ErrorMessage="Formato data richiesto: GG/MM/AAAA" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>
                </EditItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Image ID="AttivoArea2NewImage" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif"
                        ToolTip='<%# Eval("AttivoArea2") %>' OnDataBinding="ImageAttivoEval_DataBinding" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:Image ID="AttivoArea2EdtImage" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif"
                        ToolTip='<%# Eval("AttivoArea2") %>' OnDataBinding="ImageAttivoEval_DataBinding" />
                </EditItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Pub.">
                <ItemTemplate>
                    <asp:Image ID="PW_Area2Image" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Flag_Off.gif"
                        ToolTip='<%# Eval("PW_Area2") %>' OnDataBinding="Image_DataBinding" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:CheckBox ID="PW_Area2CheckBox" runat="server" Checked='<%# Bind("PW_Area2") %>' />
                </EditItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Data inizio">
                <ItemTemplate>
                    <asp:Label ID="PWI_Area2Label" runat="server" Text='<%# Eval("PWI_Area2","{0:d}") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="PWI_Area2TextBox" runat="server" Columns="10" MaxLength="10" Text='<%# Bind("PWI_Area2", "{0:d}") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator434543"
                        runat="server" ControlToValidate="PWI_Area2TextBox" SkinID="ZSSM_Validazione01"
                        Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="RegularExpressionValidator3445"
                        runat="server" ControlToValidate="PWI_Area2TextBox" SkinID="ZSSM_Validazione01"
                        Display="Dynamic" ErrorMessage="Formato data richiesto: GG/MM/AAAA" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>
                </EditItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Data fine">
                <ItemTemplate>
                    <asp:Label ID="PWF_Area2Label" runat="server" Text='<%# Eval("PWF_Area2","{0:d}") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="PWF_Area2TextBox" runat="server" Columns="10" MaxLength="10" Text='<%# Bind("PWF_Area2", "{0:d}") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator8789"
                        runat="server" ControlToValidate="PWF_Area2TextBox" SkinID="ZSSM_Validazione01"
                        Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="RegularExpressionValidator50"
                        runat="server" ControlToValidate="PWF_Area2TextBox" SkinID="ZSSM_Validazione01"
                        Display="Dynamic" ErrorMessage="Formato data richiesto: GG/MM/AAAA" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>
                </EditItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Image ID="AttivoArea3NewImage" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif"
                        ToolTip='<%# Eval("AttivoArea3") %>' OnDataBinding="ImageAttivoEval_DataBinding" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:Image ID="AttivoArea3EdtImage" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif"
                        ToolTip='<%# Eval("AttivoArea3") %>' OnDataBinding="ImageAttivoEval_DataBinding" />
                </EditItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Pub.">
                <ItemTemplate>
                    <asp:Image ID="PW_Area3Image" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Flag_Off.gif"
                        ToolTip='<%# Eval("PW_Area3") %>' OnDataBinding="Image_DataBinding" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:CheckBox ID="PW_Area3CheckBox" runat="server" Checked='<%# Bind("PW_Area3") %>' />
                </EditItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Data inizio">
                <ItemTemplate>
                    <asp:Label ID="PWI_Area3Label" runat="server" Text='<%# Eval("PWI_Area3","{0:d}") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="PWI_Area3TextBox" runat="server" Columns="10" MaxLength="10" Text='<%# Bind("PWI_Area3", "{0:d}") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator434"
                        runat="server" ControlToValidate="PWI_Area3TextBox" SkinID="ZSSM_Validazione01"
                        Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="RegularExpressionValidator356"
                        runat="server" ControlToValidate="PWI_Area3TextBox" SkinID="ZSSM_Validazione01"
                        Display="Dynamic" ErrorMessage="Formato data richiesto: GG/MM/AAAA" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>
                </EditItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Data fine">
                <ItemTemplate>
                    <asp:Label ID="PWF_Area3Label" runat="server" Text='<%# Eval("PWF_Area3","{0:d}") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="PWF_Area3TextBox" runat="server" Columns="10" MaxLength="10" Text='<%# Bind("PWF_Area3", "{0:d}") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator824"
                        runat="server" ControlToValidate="PWF_Area3TextBox" SkinID="ZSSM_Validazione01"
                        Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="RegularExpressionValidator555"
                        runat="server" ControlToValidate="PWF_Area3TextBox" SkinID="ZSSM_Validazione01"
                        Display="Dynamic" ErrorMessage="Formato data richiesto: GG/MM/AAAA" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>
                </EditItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Area Zeus">
                <ItemTemplate>
                    <asp:Image ID="PW_Zeus1Image" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif"
                        ToolTip='<%# Eval("PW_Zeus1") %>' OnDataBinding="ImageAttivo_DataBinding" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:CheckBox ID="PW_Zeus1CheckBox" runat="server" Checked='<%# Bind("PW_Zeus1") %>' />
                </EditItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:CommandField ButtonType="Link" HeaderText="Aggiorna" ShowEditButton="true" UpdateText="Salva"
                EditText="Modifica" ControlStyle-CssClass="GridView_Button1" ItemStyle-HorizontalAlign="Center"
                CancelText="Annulla" ValidationGroup="myValidation" />
        </Columns>
        <EmptyDataTemplate>
            Nessun dato.
            <br />
            <br />
            <br />
            <br />
            <br />
        </EmptyDataTemplate>
    </asp:GridView>
    <asp:SqlDataSource ID="PZ_Catalogo_SqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SELECT [ID1Articolo],[Articolo],[Categoria]
      ,[PW_Area1],[PWI_Area1],[PWF_Area1]
      ,[PW_Area2],[PWI_Area2],[PWF_Area2]
      ,[PW_Area3],[PWI_Area3],[PWF_Area3]
      ,[PW_Zeus1] ,[AttivoArea1],[AttivoArea2],[AttivoArea3]
      FROM [vwCatalogo_Lst_All]
        WHERE ZeusLangCode=@ZeusLangCode AND ZeusIdModulo=@ZeusIdModulo " UpdateCommand="UPDATE [tbCatalogo] SET
       [PW_Area1]=@PW_Area1,[PWI_Area1]=@PWI_Area1,[PWF_Area1]=@PWF_Area1
      ,[PW_Area2]=@PW_Area2,[PWI_Area2]=@PWI_Area2,[PWF_Area2]=@PWF_Area2
      ,[PW_Area3]=@PW_Area3,[PWI_Area3]=@PWI_Area3,[PWF_Area3]=@PWF_Area3
      ,[PW_Zeus1]=@PW_Zeus1  
        WHERE ID1Articolo=@ID1Articolo">
        <SelectParameters>
            <asp:QueryStringParameter Name="ZeusLangCode" QueryStringField="Lang" />
            <asp:QueryStringParameter Name="ZeusIdModulo" QueryStringField="ZIM" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="PW_Area1" Type="Boolean" />
            <asp:Parameter Name="PWI_Area1" Type="DateTime" />
            <asp:Parameter Name="PWF_Area1" Type="DateTime" />
            <asp:Parameter Name="PW_Area2" Type="Boolean" />
            <asp:Parameter Name="PWI_Area2" Type="DateTime" />
            <asp:Parameter Name="PWF_Area2" Type="DateTime" />
            <asp:Parameter Name="PW_Area3" Type="Boolean" />
            <asp:Parameter Name="PWI_Area3" Type="DateTime" />
            <asp:Parameter Name="PWF_Area3" Type="DateTime" />
            <asp:Parameter Name="PW_Zeus1" Type="Boolean" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
