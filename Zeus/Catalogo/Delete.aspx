﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">
    
    static string TitoloPagina = "Elimina associazione articoli / accessori";

    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;

        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI")) 
            && (Request.QueryString["XRI"].Length > 0) )
            if (DeleteAssociazione(Request.QueryString["XRI"]))
                Response.Redirect("~/Zeus/System/Message.aspx?Msg=0000000000000011");

        Response.Redirect("~/Zeus/System/Message.aspx");

    }//fine Page_Load


   


    private bool DeleteAssociazione(string ID1CatAssoc) 
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {


            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = @"DELETE FROM tbCatalogo_Accessori 
WHERE ID1CatAssoc=@ID1CatAssoc";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID1CatAssoc", System.Data.SqlDbType.Int);
                command.Parameters["@ID1CatAssoc"].Value = ID1CatAssoc;
              
                command.ExecuteNonQuery();

                return true;

            }
            catch (Exception p)
            {
                Response.Write( p.ToString());
                return false;
            }
        }//fine Using


    }//fine DeleteAssociazione
    
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
</asp:Content>
