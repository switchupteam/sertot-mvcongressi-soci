﻿<%@ Control Language="C#" ClassName="CatalogoPhoto" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">

    
    static int UP = 3;
    static int DOWN = 4;



    protected void Page_Load(Object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["XRI"]))
            AggiungiHyperLink.NavigateUrl = "Photo_New.aspx?XRI=" + Request.QueryString["XRI"];

        if (!string.IsNullOrEmpty(Request.QueryString["ZIM"]))
            AggiungiHyperLink.NavigateUrl += "&ZIM=" + Request.QueryString["ZIM"];


        if (!string.IsNullOrEmpty(Request.QueryString["Lang"]))
            AggiungiHyperLink.NavigateUrl += "&Lang=" + Request.QueryString["Lang"];
    
    }//fine Page_Load

    //FUNZIONI PER L'ORDINAMENTO  


    private int getLastOrder()
    {

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT MAX(Ordinamento)";
                sqlQuery += " FROM [tbCatalogo_Photo] ";
                sqlQuery += " WHERE [ID2Articolo]=@ID2Articolo";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID2Articolo", System.Data.SqlDbType.Int);
                command.Parameters["@ID2Articolo"].Value = Server.HtmlEncode(Request.QueryString["XRI"]);

                SqlDataReader reader = command.ExecuteReader();

                int myReturn = 0;

                while (reader.Read())
                    if (!reader.IsDBNull(0))
                        myReturn = reader.GetInt16(0);


                reader.Close();
                return myReturn;
            }
            catch (Exception p)
            {

                Response.Write(p.ToString());
                return -1;
            }
        }//fine Using

    }//fine getLastOrder


    static int LIVELLI = 1;

    private bool CheckIntegrityR(string Id2Articolo)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {



            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();




                ArrayList myArray = new ArrayList();

                string[][] myMenuChange = null;

                int myOrder = 1;


                SqlDataReader reader = null;


                for (int index = 1; index <= LIVELLI; ++index)
                {


                    sqlQuery = "SELECT [ID1Photo],Ordinamento";
                    sqlQuery += " FROM [tbCatalogo_Photo]";
                    sqlQuery += " WHERE [Id2Articolo]=@Id2Articolo";
                    sqlQuery += " ORDER BY Ordinamento,ID1Photo";
                    command.CommandText = sqlQuery;
                    command.Parameters.Clear();


                    command.Parameters.Add("@Id2Articolo", System.Data.SqlDbType.Int, 4);
                    command.Parameters["@Id2Articolo"].Value = Id2Articolo;

                    reader = command.ExecuteReader();

                    myOrder = 1;

                    while (reader.Read())
                    {

                        if (myOrder != reader.GetInt16(1))
                        {

                            string[] myData = new string[2];
                            myData[0] = reader.GetInt32(0).ToString();
                            myData[1] = myOrder.ToString();
                            myArray.Add(myData);
                        }

                        ++myOrder;
                    }//fine while

                    reader.Close();




                    myMenuChange = (string[][])myArray.ToArray(typeof(string[]));
                    myArray = new ArrayList();


                    for (int i = 0; i < myMenuChange.Length; ++i)
                    {

                        sqlQuery = "UPDATE tbCatalogo_Photo ";
                        sqlQuery += " SET Ordinamento=@Ordinamento";
                        sqlQuery += " WHERE ID1Photo=@ID1Photo";
                        command.CommandText = sqlQuery;
                        command.Parameters.Clear();

                        command.Parameters.Add("@Ordinamento", System.Data.SqlDbType.NVarChar, 4);
                        command.Parameters["@Ordinamento"].Value = myMenuChange[i][1];

                        command.Parameters.Add("@ID1Photo", System.Data.SqlDbType.Int, 4);
                        command.Parameters["@ID1Photo"].Value = myMenuChange[i][0];

                        command.ExecuteNonQuery();


                    }//fine for




                }//fine for LIVELLI


                return true;
            }
            catch (Exception p)
            {

                Response.Write(p.ToString());

                return false;
            }
        }//fine using
    }//fine CheckIntegrityR


    //FINE FUNZIONI PER ORDINAMENTO  


    //FUNZIONI PER LA GRIDVIEW CHE ULIZZANO L'ORDINAMENTO
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        

        try
        {

            int index = Convert.ToInt32(e.CommandArgument);
            int indexNew = index;

            if (e.CommandName.ToString().Equals("Up"))
                --indexNew;
            else if (e.CommandName.ToString().Equals("Down"))
                ++indexNew;



            GridViewRow row = GridView1.Rows[index];
            Label ID1PhotoAssoc = (Label)row.FindControl("ID1Photo");
            Label Ordinamento = (Label)row.FindControl("Ordinamento");


            GridViewRow rowNew = GridView1.Rows[indexNew];
            Label ID1PhotoAssocNew = (Label)rowNew.FindControl("ID1Photo");
            Label OrdinamentoNew = (Label)rowNew.FindControl("Ordinamento");

            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            string sqlQuery = string.Empty;


            using (SqlConnection connection = new SqlConnection(
               strConnessione))
            {


                connection.Open();

                SqlCommand command = connection.CreateCommand();




                sqlQuery = "UPDATE [tbCatalogo_Photo]";
                sqlQuery += " SET Ordinamento=" + OrdinamentoNew.Text;
                sqlQuery += " WHERE [ID1Photo]=" + ID1PhotoAssoc.Text + ";";
                sqlQuery += "UPDATE [tbCatalogo_Photo]";
                sqlQuery += " SET Ordinamento=" + Ordinamento.Text;
                sqlQuery += " WHERE [ID1Photo]=" + ID1PhotoAssocNew.Text;
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();


              



            }//fine using

            GridView1.DataBind();


        }
        catch (Exception p)
        {
            //Response.Write(p.ToString());

        }


    }//fine GridView1_RowCommand

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {

            if (e.Row.DataItemIndex > -1)
            {

                Label Ordinamento = (Label)e.Row.FindControl("Ordinamento");
                int ordinamentoRow = Convert.ToInt32(Ordinamento.Text);

                int maxordine = getLastOrder();


                //Response.Write("DEB ordinamentoRow: " + ordinamentoRow+"<br />");
                //Response.Write("DEB maxordine: " + maxordine + "<br />");

                if (ordinamentoRow == 1)
                    e.Row.Cells[UP].Text = string.Empty;

                if (ordinamentoRow == maxordine)
                    e.Row.Cells[DOWN].Text = string.Empty;



                //immagini attivo
                Image Image1 = (Image)e.Row.FindControl("Image1");
                Label AttivoArea1 = (Label)e.Row.FindControl("AttivoArea1");

                if ((Image1 != null) && (AttivoArea1 != null))
                    if (Convert.ToBoolean(AttivoArea1.Text.ToString()))
                        Image1.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";


                if (e.Row.Cells[UP].Controls.Count > 0)
                {
                    e.Row.Cells[UP].Attributes.Add("onmouseover", "showImageUP('" + e.Row.Cells[UP].Controls[0].ClientID + "','2');");
                    e.Row.Cells[UP].Attributes.Add("onmouseout", "showImageUP('" + e.Row.Cells[UP].Controls[0].ClientID + "','1');");
                }

                if (e.Row.Cells[DOWN].Controls.Count > 0)
                {
                    e.Row.Cells[DOWN].Attributes.Add("onmouseover", "showImageDOWN('" + e.Row.Cells[DOWN].Controls[0].ClientID + "','2');");
                    e.Row.Cells[DOWN].Attributes.Add("onmouseout", "showImageDOWN('" + e.Row.Cells[DOWN].Controls[0].ClientID + "','1');");
                }



            }//fine if
        }
        catch (Exception p)
        {

            //Response.Write(p.ToString());
        }

    }//fine GridView1_RowDataBound

    protected void GridView1_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {
        CheckIntegrityR(Server.HtmlEncode(Request.QueryString["XRI"]));

    }//fine GridView1_RowDeleted
    
</script>
<script language="javascript" type="text/javascript">
    function showImageUP(imgId, typ) {
        var objImg = document.getElementById(imgId);
        if (objImg) {
            if (typ == "1")
                objImg.src = "../SiteImg/Bt3_ArrowUp_Off.gif";
            else if (typ == "2")
                objImg.src = "../SiteImg/Bt3_ArrowUp_On.gif";
        }
    }

    function showImageDOWN(imgId, typ) {
        var objImg = document.getElementById(imgId);
        if (objImg) {
            if (typ == "1")
                objImg.src = "../SiteImg/Bt3_ArrowDown_Off.gif";
            else if (typ == "2")
                objImg.src = "../SiteImg/Bt3_ArrowDown_On.gif";
        }
    }
</script>
<div style="padding: 10px 0 0 0; margin: 0;">
    <asp:HyperLink ID="AggiungiHyperLink" runat="server" 
    Text="Aggiungi foto" 
    SkinID="ZSSM_Button01">
    </asp:HyperLink>
</div>
<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="PhotoGallerySqlDataSource"
    OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand" CellPadding="0"
    DataKeyNames="ID1Photo" Width="100%" OnRowDeleted="GridView1_RowDeleted">
    <Columns>
    <asp:TemplateField HeaderText="ID1Photo" SortExpression="ID1Photo" Visible="False">
            <ItemTemplate>
                <asp:Label ID="ID1Photo" runat="server" Text='<%# Eval("ID1Photo") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Ordinamento" SortExpression="Ordinamento" Visible="False">
            <ItemTemplate>
                <asp:Label ID="Ordinamento" runat="server" Text='<%# Eval("Ordinamento") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Fotografia">
            <ItemTemplate>
                <asp:Image ID="Immagine2Image" runat="server" ImageUrl='<%# Eval("Immagine2","~/ZeusInc/PhotoGallery/Img2Photo/{0}") %>' />
            </ItemTemplate>
            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
        </asp:TemplateField>
        <asp:ButtonField CommandName="Up" HeaderText="Ord." ShowHeader="True" ImageUrl="~/Zeus/SiteImg/Bt3_ArrowUp_Off.gif"
            ButtonType="Image" ControlStyle-Width="16" ControlStyle-Height="16" ControlStyle-BorderWidth="0">
            <ItemStyle HorizontalAlign="Center" />
            <ControlStyle CssClass="GridView_Ord1" />
        </asp:ButtonField>
        <asp:ButtonField CommandName="Down" HeaderText="Ord." ShowHeader="True" ButtonType="Image"
            ImageUrl="~/Zeus/SiteImg/Bt3_ArrowDown_Off.gif" ControlStyle-Width="16" ControlStyle-Height="16"
            ControlStyle-BorderWidth="0">
            <ItemStyle HorizontalAlign="Center" />
            <ControlStyle CssClass="GridView_Ord1" />
        </asp:ButtonField>
        <asp:TemplateField HeaderText="Attivo">
            <ItemTemplate>
                <asp:Label ID="AttivoArea1" runat="server" Text='<%# Eval("Pubblica") %>' Visible="false"></asp:Label>
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" /></ItemTemplate>
            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            
        </asp:TemplateField>
        <asp:BoundField DataField="DataCreazione" HeaderText="Data creazione"
         ItemStyle-HorizontalAlign="Center" />

        <asp:CommandField ButtonType="Link" HeaderText="Rimuovi" ShowDeleteButton="true"
            DeleteText="Rimuovi" ControlStyle-CssClass="GridView_Button1" ItemStyle-HorizontalAlign="Center" />
        <asp:HyperLinkField DataNavigateUrlFields="ID2Articolo,ID1Photo,ZeusIdModulo,ZeusLangCode"
                Text="Modifica" DataNavigateUrlFormatString="Photo_Edt.aspx?XRI={0}&XRI1={1}&ZIM={2}&Lang={3}"
                HeaderText="Modifica">
                <ControlStyle CssClass="GridView_ZeusButton1" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:HyperLinkField>
            
    </Columns>
    <EmptyDataTemplate>
        Nessuna foto associata a questo contenuto
        <br />
        <br />
        <br />
        <br />
        <br />
    </EmptyDataTemplate>
    <EmptyDataRowStyle BackColor="White" Width="500px" BorderColor="Red" BorderWidth="0px"
        Height="200px" />
</asp:GridView>
<asp:SqlDataSource ID="PhotoGallerySqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
    SelectCommand="SELECT     dbo.tbCatalogo_Photo.ID1Photo, dbo.tbCatalogo_Photo.Immagine1, dbo.tbCatalogo_Photo.Immagine2, dbo.tbCatalogo_Photo.Ordinamento, 
                      dbo.tbCatalogo_Photo.Pubblica, dbo.tbCatalogo_Photo.ID2Articolo, dbo.tbCatalogo.ZeusIdModulo, dbo.tbCatalogo.ZeusLangCode,dbo.tbCatalogo_Photo.DataCreazione
FROM         dbo.tbCatalogo_Photo INNER JOIN
                      dbo.tbCatalogo ON dbo.tbCatalogo_Photo.ID2Articolo = dbo.tbCatalogo.ID1Articolo
WHERE [ID2Articolo]=@ID2Articolo ORDER BY Ordinamento" 
DeleteCommand="DELETE FROM tbCatalogo_Photo  WHERE [ID1Photo] =@ID1Photo">
    <SelectParameters>
        <asp:QueryStringParameter DefaultValue="0" Name="ID2Articolo" QueryStringField="XRI"
            Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>
