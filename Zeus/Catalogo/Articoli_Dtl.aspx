<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="Articoli_Dtl.aspx.cs"
    Inherits="Catalogo_Articoli_Dtl"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="ArticoliLangButton.ascx" TagName="ArticoliLangButton" TagPrefix="uc1" %>
<%@ Register Src="~/Zeus/SiteAssets/PhotoGallery_Associa.ascx" TagName="PhotoGallery_Associa" TagPrefix="uc2" %>
<%@ Register Src="~/Zeus/Catalogo/CatalogoPhoto.ascx" TagName="CatalogoPhoto" TagPrefix="uc3" %>
<%@ Register Src="~/Zeus/PhotoGallery_Simple/Photo_Lst.ascx" TagName="Photo_Lst"
    TagPrefix="uc3" %>

<asp:Content ID="ZeusAreaHead" ContentPlaceHolderID="ZeusAreaHead" runat="Server">
<link href="../../asset/css/ZeusTypeFoundry.css" rel="stylesheet" type="text/css" />
    <link href="../../SiteCss/ZeusSnippets.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    
    <asp:HiddenField ID="TitleField" runat="server" />
    <asp:HiddenField ID="ZIM" runat="server" />
    <asp:HiddenField ID="XRI" runat="server" />
    <asp:HiddenField ID="ZID" runat="server" />
    <dlc:PermitRoles ID="myPermitRoles" runat="server" PAGE_TYPE="DTL" SQL_TABLE="tbPz_Catalogo"
        EDT_ID_BUTTON="ModificaButton" />
    <asp:FormView ID="FormView1" runat="server" DataSourceID="dsArticoloNew" DefaultMode="ReadOnly"
        Width="100%">
        <ItemTemplate>
            <div id="tabs">

                <ul>
                    <li><a href="#Principale">Principale</a></li>
                    <li><a href="#Immagini">Immagini</a></li>
                    <li><a href="#Contenuti">Contenuti</a></li>
                    <li><a href="#Listino">Listino</a></li>
                    <li><a href="#MultiCategoria">Categorie multiple</a></li>
                    <li><a href="#Articoli">Articoli e accessori</a></li>
                    <li><a href="#PhotoGallery">PhotoGallery</a></li>
                </ul>
                <div id="Principale">
                    <div class="BlockBox">
                        <div class="BlockBoxHeader">
                            <asp:Label ID="IdentificativoLabel" runat="server">Identificativo articolo</asp:Label>
                        </div>
                        <table>
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label ID="ZML_Articolo" SkinID="FieldDescription" runat="server" Text="Articolo "></asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:Label ID="ArticoloDataLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("Articolo") %>'></asp:Label>
                                </td>
                            </tr>
                            <asp:Panel ID="ZMCF_Brand" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_Brand" SkinID="FieldDescription" runat="server" Text="Brand "></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:Label ID="ID2BrandLabel" runat="server" SkinID="FieldValue"></asp:Label>
                                        <asp:DropDownList ID="ID2BrandDropDownList" runat="server" AppendDataBoundItems="True"
                                            SelectedValue='<%# Eval("ID2Brand") %>' DataSourceID="ID2BrandSqlDataSource"
                                            DataTextField="Brand" DataValueField="ID1Brand" Visible="false">
                                            <asp:ListItem Text="Nessuno" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:SqlDataSource ID="ID2BrandSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                            SelectCommand="SELECT ID1Brand,Brand FROM  [vwBrands_All] WHERE [AttivoArea1] >0"></asp:SqlDataSource>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="ZMCF_Codice" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="Label8" runat="server" SkinID="FieldDescription" Text="Label">Codice </asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:Label ID="CodiceLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("Codice") %>'></asp:Label>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="ZMCF_Categoria1" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="Label11" runat="server" SkinID="FieldDescription" Text="Label">Categoria </asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:Label ID="CategoriaLabel" runat="server" SkinID="FieldValue"></asp:Label>
                                        <asp:DropDownList ID="ID2CategoriaDropDownList" runat="server" AppendDataBoundItems="True"
                                            Visible="false">
                                        </asp:DropDownList>
                                        <asp:HiddenField ID="ID2CategoriaHiddenField" runat="server" Value='<%# Eval("ID2Categoria1") %>' />
                                        <asp:RequiredFieldValidator ID="ID2CategoriaRequiredFieldValidator" ErrorMessage="Obbligatorio"
                                            runat="server" ControlToValidate="ID2CategoriaDropDownList" SkinID="ZSSM_Validazione01"
                                            Display="Dynamic" InitialValue="0" ValidationGroup="myValidation">
                                        </asp:RequiredFieldValidator>
                                        <asp:SqlDataSource ID="dsCategoria" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>">
                                            <SelectParameters>
                                                <asp:QueryStringParameter DefaultValue="0" Name="ZeusIdModulo" QueryStringField="ZIM"
                                                    Type="String" />
                                                <asp:QueryStringParameter DefaultValue="ITA" Name="ZeusLangCode" QueryStringField="Lang"
                                                    Type="String" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="ZMCF_TagRicerca" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="Label2" runat="server" SkinID="FieldDescription" Text="Label">Tag di ricerca</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:Label ID="TagLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("ZeusTags") %>'></asp:Label>
                                    </td>
                                </tr>
                            </asp:Panel>
                        </table>
                    </div>
                    <asp:Panel ID="ZMCF_BoxMetaTag" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="TagPagina" runat="server" Text="Tag di pagina" />
                            </div>
                            <table border="0" cellpadding="2" cellspacing="1">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="Description_Label" SkinID="FieldDescription" runat="server" Text="Description"></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:Label ID="ZMCD_TagDescription" runat="server" Text='<%# Eval("TagDescription") %>'
                                            SkinID="FieldValue"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="Keywords_Label" runat="server" SkinID="FieldDescription" Text="Keywords"></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:Label ID="ZMCD_TagKeywords" runat="server" Text='<%# Eval("TagKeywords") %>'
                                            SkinID="FieldValue"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_BoxSpecifiche" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="ZML_BoxSpecifiche" runat="server">Specifiche generiche</asp:Label>
                            </div>
                            <table>
                                <asp:Panel ID="ZMCF_SpecificaInt1" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="ZML_SpecificaInt1" SkinID="FieldDescription" runat="server" Text="SpecificaInt1"></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:Label ID="SpecificaInt1TextBox" runat="server" SkinID="FieldValue" Text='<%# Eval("SpecificaInt1") %>'></asp:Label>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="ZMCF_SpecificaText1" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="ZML_SpecificaText1" SkinID="FieldDescription" runat="server" Text="SpecificaText1"></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:Label ID="SpecificaText1TextBox" runat="server" SkinID="FieldValue" Text='<%# Eval("SpecificaText1") %>'></asp:Label>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="ZMCF_SpecificaText2" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="ZML_SpecificaText2" SkinID="FieldDescription" runat="server" Text="SpecificaText2"></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:Label ID="SpecificaText2TextBox" runat="server" SkinID="FieldValue" Text='<%# Eval("SpecificaText2") %>'></asp:Label>
                                        </td>
                                    </tr>
                                </asp:Panel>
                            </table>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_BoxUrlRewrite" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="Label1" runat="server" Text="Page Link / URL (indirizzo pagina web)"></asp:Label>
                            </div>
                            <table>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="Label3" runat="server" Text="Url Rewrite" SkinID="FieldDescription"></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:Label ID="InfoUrlLabel" runat="server" SkinID="FieldValue"></asp:Label>
                                        <asp:Label ID="ServerLabel" runat="server" SkinID="FieldValue" Text='<%# "http://" + Request.ServerVariables["SERVER_NAME"] %>'></asp:Label><asp:Label
                                            ID="ZMCD_UrlSection" runat="server" SkinID="FieldValue"></asp:Label><asp:Label ID="ID1Pagina"
                                                runat="server" SkinID="FieldValue" Text='<%# Eval("ID1Articolo","{0}/") %>'></asp:Label><asp:Label
                                                    ID="UrlPageDetailLabel" runat="server" Text='<%# Eval("UrlRewrite","{0}.aspx") %>'
                                                    SkinID="FieldValue"></asp:Label>
                                        <asp:HiddenField ID="UrlRewriteHiddenField" runat="server" Value='<%# Eval("UrlRewrite","{0}.aspx") %>' />
                                        <asp:HiddenField ID="CopyHiddenField" runat="server" />
                                    </td>
                                    <td>
                                        <input id="CopiaInput" value="Copia" type="button" onclick="ClipBoard(); return;"
                                            class="ZSSM_Button01_Button" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                    <div class="BlockBox">
                        <div class="BlockBoxHeader">
                            <asp:Label ID="Label6" runat="server">Planner</asp:Label>
                        </div>
                        <table>
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label ID="ZML_PWArea1" SkinID="FieldDescription" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:CheckBox ID="PW_Area1CheckBox" runat="server" Checked='<%# Eval("PW_Area1") %>'
                                        Enabled="false" />
                                    <asp:Label ID="Label20" runat="server" SkinID="FieldValue" Text="Label">Attiva pubblicazione dalla data</asp:Label>
                                    <asp:Label ID="PWI_Area1Label" runat="server" SkinID="FieldValue" Text='<%# Eval("PWI_Area1", "{0:d}") %>'
                                        OnDataBinding="PWI_Area"></asp:Label>
                                    <asp:Label ID="Label9" runat="server" SkinID="FieldValue">alla data </asp:Label>&nbsp;
                                <asp:Label ID="PWF_Area1Label" runat="server" SkinID="FieldValue" Text='<%# Eval("PWF_Area1", "{0:d}") %>'
                                    OnDataBinding="PWF_Area"></asp:Label>
                                </td>
                            </tr>
                            <asp:Panel ID="ZMCF_Area2" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_PWArea2" SkinID="FieldDescription" runat="server" Text="Label"></asp:Label>
                                        <td class="BlockBoxValue">
                                            <asp:CheckBox ID="PW_Area2CheckBox" runat="server" Checked='<%# Eval("PW_Area2") %>'
                                                Enabled="false" />
                                            <asp:Label ID="Label7" runat="server" SkinID="FieldValue" Text="Label">Attiva pubblicazione dalla data</asp:Label>
                                            <asp:Label ID="PWI_Area2Label" runat="server" SkinID="FieldValue" Text='<%# Eval("PWI_Area2", "{0:d}") %>'
                                                OnDataBinding="PWI_Area"></asp:Label>
                                            <asp:Label ID="Label12" runat="server" SkinID="FieldValue">alla data </asp:Label>&nbsp;
                                        <asp:Label ID="PWF_Area2Label" runat="server" SkinID="FieldValue" Text='<%# Eval("PWF_Area2", "{0:d}") %>'
                                            OnDataBinding="PWF_Area"></asp:Label>
                                        </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="ZMCF_Area3" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_PWArea3" SkinID="FieldDescription" runat="server" Text="Label"></asp:Label>
                                        <td class="BlockBoxValue">
                                            <asp:CheckBox ID="PW_Area3CheckBox" runat="server" Checked='<%# Eval("PW_Area3") %>'
                                                Enabled="false" />
                                            <asp:Label ID="InfoInizioLabel3" runat="server" SkinID="FieldValue" Text="Attiva pubblicazione dalla data"></asp:Label>
                                            <asp:Label ID="PWI_Area3Label" runat="server" SkinID="FieldValue" Text='<%# Eval("PWI_Area3", "{0:d}") %>'
                                                OnDataBinding="PWI_Area"></asp:Label>
                                            <asp:Label ID="InfoFineLabel3" runat="server" SkinID="FieldValue" Text="alla data "></asp:Label>&nbsp;
                                        <asp:Label ID="PWF_Area3Label" runat="server" SkinID="FieldValue" Text='<%# Eval("PWF_Area3", "{0:d}") %>'
                                            OnDataBinding="PWF_Area"></asp:Label>
                                        </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="ZMCF_AreaZeus" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_PWAreaZeus" SkinID="FieldDescription" runat="server" Text="Label"></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:CheckBox ID="PW_Zeus1CheckBox" Enabled="false" runat="server" Checked='<%# Eval("PW_Zeus1") %>' />
                                    </td>
                                </tr>
                            </asp:Panel>
                        </table>
                    </div>
                </div>

                <div id="Contenuti">
                    <asp:Panel ID="ZMCF_DescBreve1" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="TagPaginaLabel" runat="server">Descrizione breve</asp:Label>
                            </div>
                            <table>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="VoceLabel" runat="server" SkinID="FieldDescription" Text="Label">Descrizione<br />
                                    (Massimo 500 caratteri)</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:Label ID="DescBreveLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("DescBreve1", "{0}") %>'></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_Descrizione1" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="ZML_Descrizione1" runat="server" Text="Descrizione 1"></asp:Label>
                            </div>
                            <table>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="Label13" runat="server" SkinID="FieldDescription" Text="Label">Descrizione</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:Label ID="DescBreveLabel1" runat="server" SkinID="FieldValue" Text='<%# Eval("Descrizione1", "{0}") %>'></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_Descrizione2" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="ZML_Descrizione2" runat="server" Text="Descrizione 2"></asp:Label>
                            </div>
                            <table>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="Label15" runat="server" SkinID="FieldDescription" Text="Label">Descrizione</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:Label ID="DescBreveLabel12" runat="server" SkinID="FieldValue" Text='<%# Eval("Descrizione2", "{0}") %>'></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>

                </div>
                <div id="Immagini">
                    <asp:Panel ID="ZMCF_BoxImmagini" runat="server">
                        <asp:Panel ID="ZMCF_Immagine1" runat="server">
                            <dlc:ImageRaider ID="ImageRaider1" runat="server" ZeusIdModuloIndice="1" ZeusLangCode="ITA"
                                BindPzFromDB="true" IsEditMode="true" DefaultEditBetaImage='<%# Eval("Immagine1") %>'
                                Enabled="false" DefaultEditGammaImage='<%# Eval("Immagine2") %>'
                                ImageAlt='<%# Eval("Immagine12Alt") %>' OnDataBinding="ImageRaider_DataBinding" />
                            <asp:HiddenField ID="FileNameBetaHiddenField" runat="server" Value='<%# Eval("Immagine1") %>' />
                            <asp:HiddenField ID="FileNameGammaHiddenField" runat="server" Value='<%# Eval("Immagine2") %>' />
                            <asp:CustomValidator ID="FileUploadCustomValidator" runat="server" OnServerValidate="FileUploadCustomValidator_ServerValidate"
                                Display="Dynamic" SkinID="ZSSM_Validazione01" ValidationGroup="myValidation"
                                Visible="false"></asp:CustomValidator>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Immagine2" runat="server">
                            <dlc:ImageRaider ID="ImageRaider2" runat="server" ZeusIdModuloIndice="2" ZeusLangCode="ITA"
                                BindPzFromDB="true" IsEditMode="true" DefaultEditBetaImage='<%# Eval("Immagine3") %>'
                                Enabled="false" DefaultEditGammaImage='<%# Eval("Immagine4") %>' 
                                ImageAlt='<%# Eval("Immagine34Alt") %>' OnDataBinding="ImageRaider_DataBinding" />
                            <asp:HiddenField ID="FileNameBeta2HiddenField" runat="server" Value='<%# Eval("Immagine3") %>' />
                            <asp:HiddenField ID="FileNameGamma2HiddenField" runat="server" Value='<%# Eval("Immagine4") %>' />
                            <asp:CustomValidator ID="CustomValidator1" runat="server" OnServerValidate="FileUploadCustomValidator_ServerValidate"
                                Display="Dynamic" SkinID="ZSSM_Validazione01" ValidationGroup="myValidation"
                                Visible="false"></asp:CustomValidator>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Immagine3" runat="server">
                            <dlc:ImageRaider ID="ImageRaider3" runat="server" ZeusIdModuloIndice="3" ZeusLangCode="ITA"
                                BindPzFromDB="true" IsEditMode="true" DefaultEditBetaImage='<%# Eval("Immagine5") %>'
                                Enabled="false" DefaultEditGammaImage='<%# Eval("Immagine6") %>' 
                                ImageAlt='<%# Eval("Immagine56Alt") %>' OnDataBinding="ImageRaider_DataBinding" />
                            <asp:HiddenField ID="FileNameBeta3HiddenField" runat="server" Value='<%# Eval("Immagine5") %>' />
                            <asp:HiddenField ID="FileNameGamma3HiddenField" runat="server" Value='<%# Eval("Immagine6") %>' />
                            <asp:CustomValidator ID="CustomValidator2" runat="server" OnServerValidate="FileUploadCustomValidator_ServerValidate"
                                Display="Dynamic" SkinID="ZSSM_Validazione01" ValidationGroup="myValidation"
                                Visible="false"></asp:CustomValidator>
                        </asp:Panel>
                    </asp:Panel>
                </div>
                <div id="Listino">

                    <asp:Panel ID="AllDisponibilitaPanel" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="Label4" runat="server">Disponibilit�</asp:Label>
                            </div>
                            <table>
                                <asp:Panel ID="ZMCF_Quantita" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="QuantitaLabel" SkinID="FieldDescription" runat="server" Text="Quantit� "></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:Label ID="Label14" runat="server" SkinID="FieldValue">Disponibile </asp:Label><asp:Label
                                                ID="QtaDisponibileLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("QtaDisponibile") %>'></asp:Label><img
                                                    src="../SiteImg/Spc.gif" width="10" /><asp:Label ID="Label16" runat="server" SkinID="FieldValue">in arrivo </asp:Label><asp:Label
                                                        ID="QtaArrivoLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("QtaArrivo") %>'></asp:Label>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="ZMCF_Peso" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="Label5" SkinID="FieldDescription" runat="server" Text="Peso"></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:Label ID="PesoTextBox" runat="server" SkinID="FieldValue" Text='<%# Eval("Peso") %>'></asp:Label>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="ZMCF_UnitaMisura" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="Label10" SkinID="FieldDescription" runat="server" Text="Unit� di misura"></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:Label ID="UnitaMisuraTextBox" runat="server" Text='<%# Eval("UnitaMisura") %>'
                                                SkinID="FieldValue"></asp:Label>
                                        </td>
                                    </tr>
                                </asp:Panel>
                            </table>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_BoxListini" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="ZML_BoxListini" runat="server">Listino</asp:Label>
                            </div>
                            <table>
                                <asp:Panel ID="ZMCF_Listino1" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="ZML_Listino1" SkinID="FieldDescription" runat="server" Text="Listino1 EUR "></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:Label ID="Listino12Label" runat="server" SkinID="FieldValue" Text='<%# Eval("Listino1") %>'></asp:Label>
                                            <asp:Label ID="Prezzo1Label" runat="server" SkinID="FieldValue" Text='<%# Eval("Valuta") %>'></asp:Label>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="ZMCF_Listino2" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="ZML_Listino2" runat="server" SkinID="FieldDescription" Text="Listino2 EUR "></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:Label ID="ZML_Listino2123" SkinID="FieldValue" runat="server" Text='<%# Eval("Listino2") %>'></asp:Label>
                                            <asp:Label ID="Prezzo2Label" runat="server" SkinID="FieldValue" Text='<%# Eval("Valuta") %>'></asp:Label>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="ZMCF_Listino3" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="ZML_Listino3" runat="server" SkinID="FieldDescription" Text="Listino3 EUR "></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:Label ID="ZML_Listino3123123" SkinID="FieldValue" runat="server" Text='<%# Eval("Listino3") %>'></asp:Label>
                                            <asp:Label ID="Prezzo3Label" runat="server" SkinID="FieldValue" Text='<%# Eval("Valuta") %>'></asp:Label>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="ZMCF_Listino4" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="ZML_Listino421" runat="server" SkinID="FieldDescription" Text="Listino4 EUR"></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:Label ID="ZML_Listino4" runat="server" SkinID="FieldValue" Text='<%# Eval("Listino4") %>'></asp:Label>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="ZMCF_Listino5" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="ZML_Listino5123" runat="server" SkinID="FieldDescription" Text="Listino5 EUR"></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:Label ID="ZML_Listino5" runat="server" SkinID="FieldValue" Text='<%# Eval("Listino5") %>'></asp:Label>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="ZMCF_IvaPercent" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="Label17" SkinID="FieldDescription" runat="server" Text="Iva%"></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:Label ID="ZCMD_IvaPercentDefault" runat="server" Text='<%# Eval("IvaPercent") %>'
                                                SkinID="FieldValue"></asp:Label>
                                        </td>
                                    </tr>
                                </asp:Panel>
                            </table>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_Allegato1" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="ZML_TitoloAllegato1" runat="server" Text="Allegato"></asp:Label>
                            </div>
                            <table>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_DescrizioneAllegato1" runat="server" Text="Descrizione" SkinID="FieldDescription"></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:Label ID="Allegato1_DescLabel" runat="server" Text='<%# Eval("Allegato1_Desc", "{0}") %>'
                                            SkinID="FieldValue"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_FileAllegato1" runat="server" Text="Allegato" SkinID="FieldDescription"></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:HyperLink ID="FileAllegatoHyperLink" runat="server" NavigateUrl=' <%# Eval("Allegato1_File", "~/ZeusInc/Catalogo/Allegati/{0}") %>'
                                            Target="_blank" Text='<%# Eval("Allegato1_File","~/ZeusInc/Catalogo/Allegati/{0}") %>'
                                            SkinID="FieldValue" />
                                        <asp:LinkButton runat="server" ID="UrlLinkButton" PostBackUrl='<%# Eval("Allegato1_Url", "{0}") %>'
                                            Text='<%# Eval("Allegato1_Url", "{0}") %>' SkinID="ZSSM_Button01" OnDataBinding="UrlLinkButton_DataBinding"></asp:LinkButton>
                                        <asp:HiddenField ID="SelettoreAllegatoHiddenfield" runat="server" Value='<%# Eval("Allegato1_Selettore", "{0}") %>'
                                            OnDataBinding="SelettoreAllegatoHiddenfield_DataBinding" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_Allegato2" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="ZML_TitoloAllegato2" runat="server" Text="Allegato"></asp:Label>
                            </div>
                            <table>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_DescrizioneAllegato2" runat="server" Text="Descrizione" SkinID="FieldDescription"></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:Label ID="Allegato1_DescLabel2" runat="server" Text='<%# Eval("Allegato2_Desc", "{0}") %>'
                                            SkinID="FieldValue"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_FileAllegato2" runat="server" Text="Allegato" SkinID="FieldDescription"></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:HyperLink ID="FileAllegatoHyperLink2" runat="server" NavigateUrl=' <%# Eval("Allegato2_File", "~/ZeusInc/Catalogo/Allegati/{0}") %>'
                                            Target="_blank" Text='<%# Eval("Allegato2_File","~/ZeusInc/Catalogo/Allegati/{0}") %>'
                                            SkinID="FieldValue" />
                                        <asp:LinkButton runat="server" ID="UrlLinkButton2" PostBackUrl='<%# Eval("Allegato2_Url", "{0}") %>'
                                            Text='<%# Eval("Allegato2_Url", "{0}") %>' SkinID="ZSSM_Button01" OnDataBinding="UrlLinkButton2_DataBinding"></asp:LinkButton>
                                        <asp:HiddenField ID="SelettoreAllegatoHiddenfield2" runat="server" Value='<%# Eval("Allegato2_Selettore", "{0}") %>'
                                            OnDataBinding="SelettoreAllegatoHiddenfield2_DataBinding" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                </div>
                <div id="MultiCategoria">
                    <asp:Panel ID="ZMCF_Categoria2" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="ZML_Categoria2" runat="server" Text="Sezioni associate"></asp:Label>
                            </div>
                            <dlc:CategorieMultiple ID="CategorieMultiple1" runat="server" COLUMN_ID="ID2Articolo"
                                COLUMN_IDCAT="ID2Categoria2" TABLE="tbxCatalogo_MultiCategorie" MODE="EDT" />
                        </div>
                    </asp:Panel>
                </div>
                <div id="Articoli">
                    <asp:Panel ID="PZV_BoxAccessori" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                Elenco accessori associati a questo articolo
                            <img src="../SiteImg/Spc.gif" width="400" height="10" />
                                <asp:HyperLink ID="NuovaAssociazioneArticoloHyperLink" runat="server" Text="Nuova associazone"
                                    SkinID="FieldValue" NavigateUrl="~/Zeus/Catalogo/Accessori_New.aspx"></asp:HyperLink>
                            </div>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td>
                                        <asp:GridView ID="AssociazioniGridView" runat="server" AllowPaging="True" AllowSorting="True"
                                            DataSourceID="AssociazioniSqlDatasource" AutoGenerateColumns="False" PageSize="50"
                                            Width="100%">
                                            <Columns>
                                                <asp:BoundField DataField="CodiceAccessorio" HeaderText="Codice" SortExpression="CodiceAccessorio" />
                                                <asp:BoundField DataField="Brand" HeaderText="Brand" SortExpression="Brand" />
                                                <asp:BoundField DataField="Accessorio" HeaderText="Modello" SortExpression="Accessorio" />
                                                <asp:BoundField DataField="CategoriaAccessorio" HeaderText="Categoria" SortExpression="CategoriaAccessorio" />
                                                <asp:BoundField DataField="ID1Accessorio" HeaderText="ID acc." SortExpression="ID1Accessorio"
                                                    ItemStyle-HorizontalAlign="Right" />
                                                <asp:HyperLinkField DataNavigateUrlFields="ID1CatAssoc" DataNavigateUrlFormatString="~/Zeus/Catalogo/Delete.aspx?XRI={0}"
                                                    HeaderText="Elimina" Text="Elimina">
                                                    <ControlStyle CssClass="GridView_Button1" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:HyperLinkField>
                                            </Columns>
                                        </asp:GridView>
                                        <asp:SqlDataSource ID="AssociazioniSqlDatasource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                            SelectCommand="SELECT     dbo.tbCatalogo_Accessori.ID1CatAssoc, 
                                    tbCatalogo_1.Articolo AS Accessorio, dbo.vwCategorie_All.Categoria AS CategoriaAccessorio, 
                          tbCatalogo_1.Codice AS CodiceAccessorio, dbo.tbCatalogo.ID1Articolo, 
                          tbCatalogo_1.ID1Articolo AS ID1Accessorio, 
                          tbCatalogo_1.ID2Categoria1 AS ID2Categoria1Accessorio, 
                          tbCatalogo_1.DescBreve1 AS DescBreveAccessorio, 
                          dbo.vwBrands_All.Brand
    FROM         dbo.tbCatalogo INNER JOIN
                          dbo.tbCatalogo_Accessori ON dbo.tbCatalogo.ID1Articolo = dbo.tbCatalogo_Accessori.ID2Articolo INNER JOIN
                          dbo.tbCatalogo AS tbCatalogo_1 ON dbo.tbCatalogo_Accessori.ID2Accessorio = tbCatalogo_1.ID1Articolo INNER JOIN
                          dbo.vwCategorie_All ON tbCatalogo_1.ID2Categoria1 = dbo.vwCategorie_All.ID1Categoria INNER JOIN
                          dbo.vwBrands_All ON tbCatalogo_1.ID2Brand = dbo.vwBrands_All.ID1Brand
    WHERE tbCatalogo.ID1Articolo=@ID1Articolo">
                                            <SelectParameters>
                                                <asp:ControlParameter Name="ID1Articolo" ControlID="XRI" PropertyName="Value" Type="Int32" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="PZV_BoxArticoliPadre" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                Elenco articoli associati a questo accessorio
                            <img src="../SiteImg/Spc.gif" width="400" height="10" />
                                <asp:HyperLink ID="NuovaAssociazioneAccessorioHyperLink" runat="server" Text="Nuova associazone"
                                    SkinID="FieldValue" NavigateUrl="~/Zeus/Catalogo/Accessori_New.aspx"></asp:HyperLink>
                            </div>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td>
                                        <asp:GridView ID="ArticoloGridView" runat="server" AllowPaging="True" AllowSorting="True"
                                            DataSourceID="ArticoloSqlDataSource" AutoGenerateColumns="False" PageSize="50"
                                            Width="100%">
                                            <Columns>
                                                <asp:BoundField DataField="Codice" HeaderText="Codice" SortExpression="Codice" />
                                                <asp:BoundField DataField="Brand" HeaderText="Brand" SortExpression="Brand" />
                                                <asp:BoundField DataField="Articolo" HeaderText="Modello" SortExpression="Articolo" />
                                                <asp:BoundField DataField="Categoria" HeaderText="Categoria" SortExpression="Categoria" />
                                                <asp:BoundField DataField="ID1Articolo" HeaderText="ID acc." SortExpression="ID1Articolo"
                                                    ItemStyle-HorizontalAlign="Right" />
                                                <asp:HyperLinkField DataNavigateUrlFields="ID1CatAssoc" DataNavigateUrlFormatString="~/Zeus/Catalogo/Delete.aspx?XRI={0}"
                                                    HeaderText="Elimina" Text="Elimina">
                                                    <ControlStyle CssClass="GridView_Button1" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:HyperLinkField>
                                            </Columns>
                                        </asp:GridView>
                                        <asp:SqlDataSource ID="ArticoloSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                            SelectCommand="SELECT     dbo.tbCatalogo_Accessori.ID1CatAssoc,
                                     dbo.vwBrands_All.Brand, dbo.tbCatalogo.Articolo, 
                                     dbo.tbCatalogo.Codice, dbo.vwCategorie_All.Categoria,tbCatalogo.ID1Articolo 
                      
    FROM         dbo.tbCatalogo INNER JOIN
                          dbo.tbCatalogo_Accessori ON dbo.tbCatalogo.ID1Articolo = dbo.tbCatalogo_Accessori.ID2Articolo INNER JOIN
                          dbo.tbCatalogo AS tbCatalogo_1 ON dbo.tbCatalogo_Accessori.ID2Accessorio = tbCatalogo_1.ID1Articolo INNER JOIN
                          dbo.vwBrands_All ON dbo.tbCatalogo.ID2Brand = dbo.vwBrands_All.ID1Brand INNER JOIN
                          dbo.vwCategorie_All ON dbo.tbCatalogo.ID2Categoria1 = dbo.vwCategorie_All.ID1Categoria
                          WHERE tbCatalogo_1.ID1Articolo=@ID1Articolo">
                                            <SelectParameters>
                                                <asp:ControlParameter Name="ID1Articolo" ControlID="XRI" PropertyName="Value" Type="Int32" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                </div>
                <div id="PhotoGallery">
                    <asp:Panel ID="ZMCF_BoxPhotoGallery" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="Label38" runat="server" Text="Gallerie fotografiche associate -NON FUNZIONANTE-"></asp:Label>
                            </div>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td>
                                        <uc2:PhotoGallery_Associa ID="PhotoGallery_Associa1" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_BoxPhotoGallerySimple" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <a name="PHOTO">
                                    <asp:Label ID="Label56" runat="server" Text="Fotografie aggiuntive"></asp:Label></a>
                            </div>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td>
                                        <uc3:Photo_Lst ID="PhotoGallery_Simple1" runat="server" OnInit="PhotoGallery_Simple1_Init" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                </div>
            </div>

            <uc1:ArticoliLangButton ID="ArticoliLangButton1" runat="server" ZeusId='<%# Eval("ZeusId") %>'
                ZeusIdModulo='<%# Eval("ZeusIdModulo") %>' ZeusLangCode='<%# Eval("ZeusLangCode") %>' />
            <div align="center">
                <asp:LinkButton ID="ModificaButton" SkinID="ZSSM_Button01" runat="server" Text="Modifica"
                    OnClick="ModificaButton_Click"></asp:LinkButton>
                <img src="../SiteImg/Spc.gif" width="20" />
                <asp:LinkButton ID="DuplicaLinkButton" runat="server" Text="Duplica" SkinID="ZSSM_Button01"
                    OnClick="DuplicaLinkButton_Click"></asp:LinkButton>
                <img src="../SiteImg/Spc.gif" width="20" />
                <asp:LinkButton ID="DeleteLinkButton" runat="server" Text="Elimina" SkinID="ZSSM_Button01"
                    OnClick="DeleteLinkButton_Click"></asp:LinkButton>
            </div>
            <div style="padding: 10px 0 0 0; margin: 0;">
                <dlc:zeusdatatracking ID="Zeusdatatracking1" runat="server" />
            </div>
            <asp:HiddenField ID="zdtRecordNewUser" runat="server" Value='<%# Eval("RecordNewUser") %>' />
            <asp:HiddenField ID="zdtRecordNewDate" runat="server" Value='<%# Eval("RecordNewDate") %>' />
            <asp:HiddenField ID="zdtRecordEdtUser" runat="server" Value='<%# Eval("RecordEdtUser") %>' />
            <asp:HiddenField ID="zdtRecordEdtDate" runat="server" Value='<%# Eval("RecordEdtDate") %>' />
        </ItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="dsArticoloNew" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SELECT  ID1Articolo,Articolo,ID2Brand, Codice, ID2Categoria1, ZeusTags, 
        DescBreve1, Descrizione1, Descrizione2, [Immagine1], [Immagine2], Immagine12Alt, [Immagine3], [Immagine4], Immagine34Alt, [Immagine5], [Immagine6], Immagine56Alt
        , ImmagineThumb_Selettore,QtaDisponibile,QtaArrivo, Listino1
        ,Listino2, Listino3, Listino4, Listino5, Valuta, Allegato1_Desc
        , Allegato1_File, Allegato1_Url, Allegato1_Peso
        , Allegato1_Selettore, Allegato2_Desc,Allegato2_File
        , Allegato2_Url, Allegato2_Peso, Allegato2_Selettore
        ,PW_Area1, PWI_Area1, PWF_Area1
        , PW_Area2, PWI_Area2, PWF_Area2,PW_Area3, ZeusId, ZeusIdModulo, ZeusLangCode
        , PWI_Area3, PWF_Area3, RecordNewUser
        , RecordNewDate, RecordEdtUser, RecordEdtDate,UrlRewrite,[TagDescription],[TagKeywords] ,[PW_Zeus1]
        ,[IvaPercent],[Peso],[UnitaMisura],
        [SpecificaInt1],[SpecificaText1],[SpecificaText2] 
        FROM [tbCatalogo] 
        WHERE ID1Articolo=@ID1Articolo AND ZeusIsAlive=1  AND ZeusIdModulo=@ZeusIdModulo AND ZeusLangCode=@ZeusLangCode"
        OnSelected="dsArticoloNew_Selected">
        <SelectParameters>
            <asp:QueryStringParameter Name="ID1Articolo" Type="Int32" QueryStringField="XRI" />
            <asp:QueryStringParameter Name="ZeusIdModulo" QueryStringField="ZIM" Type="String" />
            <asp:QueryStringParameter Name="ZeusLangCode" QueryStringField="Lang" Type="String"
                DefaultValue="ITA" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
