﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">
    
   
    static string TitoloPagina = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = "Utilità forza planner";


    }//fine Page_Load

    protected void PWI_Area(object sender, EventArgs e)
    {
        TextBox DataCreazione = (TextBox)sender;

        if (DataCreazione.Text == string.Empty)
            DataCreazione.Text = DateTime.Now.AddDays(-365).ToString("d");

    }

    protected void PWF_Area(object sender, EventArgs e)
    {
        TextBox DataFinale = (TextBox)sender;

        if (DataFinale.Text == string.Empty)
            DataFinale.Text = "31/12/2040";

    }


    protected void CataloghiDropDonwList_SelectedIndexChanged(object sender, EventArgs e)
    {

        DropDownList CataloghiDropDonwList = (DropDownList)sender;

        if (CataloghiDropDonwList.SelectedValue.Equals("0"))
            return;

        PW_Area1Panel.Visible = true;

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        try
        {

            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();


                sqlQuery = "SELECT PZL_PWArea1_New,";
                sqlQuery += " PZV_Area2_New, PZL_PWArea2_New,";
                sqlQuery += " PZV_Area3_New, PZL_PWArea3_New";
                sqlQuery += " FROM tbPZ_Catalogo";
                sqlQuery += " WHERE  ZeusLangCode=@ZeusLangCode";
                sqlQuery += " AND ZeusIdModulo=@ZeusIdModulo";

                command.CommandText = sqlQuery;
                command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ZeusLangCode"].Value = "ITA";
                command.Parameters.Add("@ZeusIdModulo", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ZeusIdModulo"].Value = CataloghiDropDonwList.SelectedValue;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {


                    if (reader["PZL_PWArea1_New"] != DBNull.Value)
                        PW_Area1Label.Text = reader["PZL_PWArea1_New"].ToString();

                    if (reader["PZV_Area2_New"] != DBNull.Value)
                        PW_Area2Panel.Visible = Convert.ToBoolean(reader["PZV_Area2_New"]);

                    if (reader["PZL_PWArea2_New"] != DBNull.Value)
                        PW_Area2Label.Text = reader["PZL_PWArea2_New"].ToString();

                    if (reader["PZV_Area3_New"] != DBNull.Value)
                        PW_Area3Panel.Visible = Convert.ToBoolean(reader["PZV_Area3_New"]);

                    if (reader["PZL_PWArea3_New"] != DBNull.Value)
                        PW_Area3Label.Text = reader["PZL_PWArea3_New"].ToString();

                }//fine while

                reader.Close();

            }//fine Using
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine CataloghiDropDonwList_SelectedIndexChanged

    private bool ForcePlanning(string Attiva, string Creati, string PWI_Area, string PWF_Area, string myIndex)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        ErrorLabel.Text = string.Empty;

        try
        {

            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();


                sqlQuery = "UPDATE [tbCatalogo] SET ";
                sqlQuery += " [PW_Area" + myIndex + "] = "+Attiva;
                sqlQuery += " WHERE ";
                
                if(Creati.Equals("0"))
                    sqlQuery += " DATEDIFF(day,'" + PWI_Area + "', RecordNewDate)>0 AND DATEDIFF(day, RecordNewDate, '" + PWF_Area + "')>0";
                else sqlQuery += " DATEDIFF(day,'" + PWI_Area + "', RecordEdtDate)>0 AND DATEDIFF(day, RecordEdtDate, '" + PWF_Area + "')>0";
                    
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();

                //Response.Write("DEB "+sqlQuery+"<br />");

                

                return true;

            }//fine Using
        }
        catch (Exception p)
        {

            ErrorLabel.Text = p.ToString() + "<br />";
            return false;
        }

    }//fine ForcePlanning


    protected void InsertButton_Click(object sender, EventArgs e)
    {



        FinishLabel.Visible=ForcePlanning(PW_Area1AttivaDisattivaDropDownList.SelectedValue,
            PW_Area1CreatiModificatiDropDownList.SelectedValue,
            PWI_Area1TextBox.Text, PWF_Area1TextBox.Text, "1");

        FinishLabel.Visible = FinishLabel.Visible & ForcePlanning(PW_Area2AttivaDisattivaDropDownList.SelectedValue,
           PW_Area2CreatiModificatiDropDownList.SelectedValue,
           PWI_Area2TextBox.Text, PWF_Area2TextBox.Text, "2");

        FinishLabel.Visible = FinishLabel.Visible & ForcePlanning(PW_Area3AttivaDisattivaDropDownList.SelectedValue,
           PW_Area3CreatiModificatiDropDownList.SelectedValue,
           PWI_Area3TextBox.Text, PWF_Area3TextBox.Text, "3");
            
    }//fine InsertButton_Click
    
    
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <div class="BlockBox">
        <div class="BlockBoxHeader">
            <asp:Label ID="IdentificativoLabel" runat="server">Identificativo articolo</asp:Label></div>
        <table>
            <tr>
                <td class="BlockBoxDescription">
                    <asp:Label ID="Label1" SkinID="FieldDescription" runat="server" Text="Seleziona catalogo"></asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:DropDownList ID="CataloghiDropDonwList" runat="server" DataSourceID="tbModuliSqlDataSource"
                        DataTextField="Sezione" DataValueField="ZeusIdModulo" AppendDataBoundItems="true"
                        AutoPostBack="true" OnSelectedIndexChanged="CataloghiDropDonwList_SelectedIndexChanged">
                        <asp:ListItem Text="> Seleziona" Value="0" Selected="True">
                        
                        </asp:ListItem>
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="tbModuliSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                        SelectCommand="SELECT [ZeusIdModulo],[Sezione] FROM [tbModuli]
WHERE [Modulo]='Catalogo' AND [PW_Zeus1]=1"></asp:SqlDataSource>
                </td>
            </tr>
            <asp:Panel ID="PW_Area1Panel" runat="server" Visible="false">
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="PW_Area1Label" SkinID="FieldDescription" runat="server" Text="Articolo *"></asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:DropDownList ID="PW_Area1AttivaDisattivaDropDownList" runat="server">
                            <asp:ListItem Text="Attiva" Selected="True" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Disattiva" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:Label ID="Label3" runat="server" SkinID="FieldValue">
                pubblicazione sugli articoli
                        </asp:Label>
                        <asp:DropDownList ID="PW_Area1CreatiModificatiDropDownList" runat="server">
                            <asp:ListItem Text="Creati" Selected="True" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Modificati" Value="1"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:Label ID="Label4" runat="server" SkinID="FieldValue">
                tra il 
                        </asp:Label>
                        <asp:TextBox ID="PWI_Area1TextBox" runat="server" Columns="10" MaxLength="10" OnLoad="PWI_Area"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="PWI_Area1TextBox"
                            SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="PWI_Area1TextBox"
                            SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="Formato data richiesto: GG/MM/AAAA"
                            ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>
                        <asp:Label ID="Label5" runat="server" SkinID="FieldValue">
                e il 
                        </asp:Label>
                        <asp:TextBox ID="PWF_Area1TextBox" runat="server" Columns="10" MaxLength="10" OnLoad="PWF_Area"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="PWF_Area1TextBox"
                            SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="PWF_Area1TextBox"
                            SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="Formato data richiesto: GG/MM/AAAA"
                            ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>
                        <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToCompare="PWF_Area1TextBox"
                            ControlToValidate="PWI_Area1TextBox" SkinID="ZSSM_Validazione01" Display="Dynamic"
                            ErrorMessage="La data iniziale deve essere precedente o uguale alla data finale"
                            Operator="LessThanEqual" Type="Date"></asp:CompareValidator>
                        <asp:Label ID="Label6" runat="server" SkinID="FieldValue">
                (date incluse)
                        </asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="PW_Area2Panel" runat="server" Visible="false">
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="PW_Area2Label" SkinID="FieldDescription" runat="server" Text="Articolo *"></asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:DropDownList ID="PW_Area2AttivaDisattivaDropDownList" runat="server">
                            <asp:ListItem Text="Attiva" Selected="True" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Disattiva" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:Label ID="Label2" runat="server" SkinID="FieldValue">
                pubblicazione sugli articoli
                        </asp:Label>
                        <asp:DropDownList ID="PW_Area2CreatiModificatiDropDownList" runat="server">
                            <asp:ListItem Text="Creati" Selected="True" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Modificati" Value="1"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:Label ID="Label7" runat="server" SkinID="FieldValue">
                tra il 
                        </asp:Label>
                        <asp:TextBox ID="PWI_Area2TextBox" runat="server" Columns="10" MaxLength="10" OnLoad="PWI_Area"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="PWI_Area2TextBox"
                            SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="PWI_Area2TextBox"
                            SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="Formato data richiesto: GG/MM/AAAA"
                            ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>
                        <asp:Label ID="Label8" runat="server" SkinID="FieldValue">
                e il 
                        </asp:Label>
                        <asp:TextBox ID="PWF_Area2TextBox" runat="server" Columns="10" MaxLength="10" OnLoad="PWF_Area"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="PWF_Area2TextBox"
                            SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="PWF_Area2TextBox"
                            SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="Formato data richiesto: GG/MM/AAAA"
                            ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="PWF_Area2TextBox"
                            ControlToValidate="PWI_Area2TextBox" SkinID="ZSSM_Validazione01" Display="Dynamic"
                            ErrorMessage="La data iniziale deve essere precedente o uguale alla data finale"
                            Operator="LessThanEqual" Type="Date"></asp:CompareValidator>
                        <asp:Label ID="Label9" runat="server" SkinID="FieldValue">
                (date incluse)
                        </asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="PW_Area3Panel" runat="server" Visible="false">
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="PW_Area3Label" SkinID="FieldDescription" runat="server" Text="Articolo *"></asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:DropDownList ID="PW_Area3AttivaDisattivaDropDownList" runat="server">
                            <asp:ListItem Text="Attiva" Selected="True" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Disattiva" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:Label ID="Label10" runat="server" SkinID="FieldValue">
                pubblicazione sugli articoli
                        </asp:Label>
                        <asp:DropDownList ID="PW_Area3CreatiModificatiDropDownList" runat="server">
                            <asp:ListItem Text="Creati" Selected="True" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Modificati" Value="1"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:Label ID="Label11" runat="server" SkinID="FieldValue">
                tra il 
                        </asp:Label>
                        <asp:TextBox ID="PWI_Area3TextBox" runat="server" Columns="10" MaxLength="10" OnLoad="PWI_Area"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="PWI_Area3TextBox"
                            SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="PWI_Area3TextBox"
                            SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="Formato data richiesto: GG/MM/AAAA"
                            ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>
                        <asp:Label ID="Label12" runat="server" SkinID="FieldValue">
                e il 
                        </asp:Label>
                        <asp:TextBox ID="PWF_Area3TextBox" runat="server" Columns="10" MaxLength="10" OnLoad="PWF_Area"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="PWF_Area3TextBox"
                            SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="PWF_Area3TextBox"
                            SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="Formato data richiesto: GG/MM/AAAA"
                            ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>
                        <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToCompare="PWF_Area3TextBox"
                            ControlToValidate="PWI_Area3TextBox" SkinID="ZSSM_Validazione01" Display="Dynamic"
                            ErrorMessage="La data iniziale deve essere precedente o uguale alla data finale"
                            Operator="LessThanEqual" Type="Date"></asp:CompareValidator>
                        <asp:Label ID="Label13" runat="server" SkinID="FieldValue">
                (date incluse)
                        </asp:Label>
                    </td>
                </tr>
            </asp:Panel>
        </table>
    </div>
    <div align="center">
        <asp:Label ID="ErrorLabel" runat="server" SkinID="Validazione01" ></asp:Label>
        <asp:Label ID="FinishLabel" runat="server" SkinID="CorpoTesto"  Font-Bold="true"
         Text="Operazione effettuata con successo.<br />"></asp:Label>
        <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" SkinID="ZSSM_Button01"
            Text="Forza planner" OnClick="InsertButton_Click"></asp:LinkButton></div>
</asp:Content>
