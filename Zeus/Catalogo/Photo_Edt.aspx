﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">
    static string TitoloPagina = "Modifica fotografia";

    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;


        delinea myDeliena = new delinea();

        if (!myDeliena.AntiSQLInjectionNew(Request.QueryString, "XRI", "int")
            || !myDeliena.AntiSQLInjectionNew(Request.QueryString, "XRI1", "int")
            || !myDeliena.AntiSQLInjectionNew(Request.QueryString, "ZIM", "string")
            || !myDeliena.AntiSQLInjectionNew(Request.QueryString, "Lang", "string"))
            Response.Redirect("~/Zeus/System/Message.aspx?0");


        ID2Articolo.Value = Request.QueryString["XRI"];
        ID1Photo.Value = Request.QueryString["XRI1"];
        ZeusIdModulo.Value = Request.QueryString["ZIM"];
        Lang.Value = Request.QueryString["Lang"];


       

    }//fine Page_Load


    //##############################################################################################################
    //################################################ FILE UPLOAD #################################################
    //############################################################################################################## 


    protected void ImageRaider_DataBinding(object sender, EventArgs e)
    {
        ImageRaider ImageRaider1 = (ImageRaider)sender;
        ImageRaider1.SetDefaultEditBetaImage();
        ImageRaider1.SetDefaultEditGammaImage();

    }

    protected void FileUploadCustomValidator_ServerValidate(object sender, ServerValidateEventArgs args)
    {
        try
        {
            ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");
            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");

            if (ImageRaider1.isValid())
                InsertButton.CommandName = "Update";
            else
                InsertButton.CommandName = string.Empty;

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine FileUploadCustomValidator
    //##############################################################################################################
    //########################################### FINE FILE UPLOAD #################################################
    //##############################################################################################################


    
    

    protected void InsertButton_Click(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();

        LinkButton InsertButton = (LinkButton)sender;

        HiddenField FileNameBetaHiddenField = (HiddenField)FormView1.FindControl("FileNameBetaHiddenField");
        HiddenField FileNameGammaHiddenField = (HiddenField)FormView1.FindControl("FileNameGammaHiddenField");
        ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");

        HiddenField Ordinamento = (HiddenField)FormView1.FindControl("Ordinamento");

        
        if (InsertButton.CommandName != string.Empty)
        {

            if (ImageRaider1.GenerateBeta(string.Empty))
			FileNameBetaHiddenField.Value = ImageRaider1.ImgBeta_FileName;
		else FileNameBetaHiddenField.Value = ImageRaider1.DefaultBetaImage;
            
            if (ImageRaider1.GenerateGamma(string.Empty))
			FileNameGammaHiddenField.Value = ImageRaider1.ImgGamma_FileName;
		else FileNameGammaHiddenField.Value = ImageRaider1.DefaultGammaImage;
            
        }//fine if command
        
    }// fine InsertButton_Click 


    protected void tbCatalogo_PhotoSqlDataSource_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.AffectedRows == 0 || e.Exception != null)
            Response.Redirect("~/Zeus/System/Message.aspx?InsertErr");


        Response.Redirect("Articoli_Dtl.aspx?XRI=" + ID2Articolo.Value
            +"&ZIM="+ZeusIdModulo.Value
            + "&Lang=" + Lang.Value + "#PHOTO");

        
    }//fine tbCatalogo_PhotoSqlDataSource_Updated
    
    
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" />
    <asp:HiddenField ID="ID2Articolo" runat="server" />
    <asp:HiddenField ID="ZeusIdModulo" runat="server" />
    <asp:HiddenField ID="Lang" runat="server" />
    <asp:HiddenField ID="ID1Photo" runat="server" />
    
    <asp:FormView ID="FormView1" runat="server" DataKeyNames="ID1Photo" DataSourceID="tbCatalogo_PhotoSqlDataSource"
        DefaultMode="Edit" Width="100%">
        <EditItemTemplate>
             <dlc:ImageRaider ID="ImageRaider1" runat="server" ZeusIdModuloIndice="2" ZeusIdModulo="PHGFF" ZeusLangCode="ITA"
                    BindPzFromDB="true" IsEditMode="true" DefaultEditBetaImage='<%# Eval("Immagine1") %>'
                    DefaultEditGammaImage='<%# Eval("Immagine2") %>' OnDataBinding="ImageRaider_DataBinding" />
                <asp:HiddenField ID="FileNameBetaHiddenField" runat="server" Value='<%# Bind("Immagine1") %>' />
                <asp:HiddenField ID="FileNameGammaHiddenField" runat="server" Value='<%# Bind("Immagine2") %>' />
                <asp:CustomValidator ID="FileUploadCustomValidator" runat="server" OnServerValidate="FileUploadCustomValidator_ServerValidate"
                    Display="Dynamic" SkinID="ZSSM_Validazione01" 
                    Visible="false"></asp:CustomValidator>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="IdentificativoLabel" runat="server">Pubblica</asp:Label></div>
                <table>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="ZML_Articolo" SkinID="FieldDescription" runat="server" Text="Pubblica"></asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:CheckBox ID="PubblicaCheckBox" runat="server" Checked='<%# Bind("Pubblica") %>' />
                        </td>
                    </tr>
                </table>
            </div>
             <div align="center">
                <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" 
                CommandName="Update"
                    SkinID="ZSSM_Button01" Text="Salva dati" OnClick="InsertButton_Click"></asp:LinkButton></div>
        </EditItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="tbCatalogo_PhotoSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
         SelectCommand="SELECT [ID1Photo]
      ,[Immagine1]
      ,[Immagine2]
      ,[Ordinamento]
      ,[Pubblica]
  FROM [tbCatalogo_Photo]
WHERE [ID1Photo]=@ID1Photo" 
UpdateCommand="UPDATE tbCatalogo_Photo
SET Immagine1=@Immagine1 , Immagine2=@Immagine2 , Pubblica=@Pubblica
WHERE  ID1Photo=@ID1Photo" 
        OnUpdated="tbCatalogo_PhotoSqlDataSource_Updated">
        <UpdateParameters>
            <asp:Parameter Name="Immagine1" Type="String" />
            <asp:Parameter Name="Immagine2" Type="String" />
            <asp:Parameter Name="Pubblica" Type="Boolean" />
            <asp:ControlParameter DefaultValue="0" Name="ID1Photo" ControlID="ID1Photo"  PropertyName="Value"
                Type="Int32" />
        </UpdateParameters>
        <SelectParameters>
            <asp:ControlParameter DefaultValue="0" Name="ID1Photo" ControlID="ID1Photo"  PropertyName="Value"
                Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
