﻿using ASP;
using System;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per Articoli_Edt
/// </summary>
public partial class Catalogo_Articoli_Edt : System.Web.UI.Page 
{
    static string TitoloPagina = string.Empty;
    delinea myDelinea = new delinea();

    protected void Page_Load(object sender, EventArgs e)
    {
        if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
            || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI"))
            || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM")))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        ZIM.Value = Server.HtmlEncode(Request.QueryString["ZIM"]);
        XRI.Value = Server.HtmlEncode(Request.QueryString["XRI"]);

        if (!ReadXML(ZIM.Value, GetLang()))
            Response.Write("~/Zeus/System/Message.aspx?1");

        ReadXML_Localization(ZIM.Value, GetLang());

        try
        {
            string myJavascript = PrintMyJavaScript1();
            //myJavascript += " " + PrintMyJavaScript2();
            myJavascript += " " + PrintMyJavaScript3();
            myJavascript += " " + PrintMyJavaScript4();
            //myJavascript += " " + PrintMyJavaScript5();
            myJavascript += " " + PrintMyJavaScript6();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartUpScript", myJavascript, true);
            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");
            InsertButton.Attributes.Add("onclick", "Validate()");
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

        Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "val", "fnOnUpdateValidators();");
    }

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";
            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Catalogo.xml"));

            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            if (childNode.Name.Equals("ZML_TitoloPagina_Edt"))
                            {
                                TitleField.Value = childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                                else
                                {
                                    myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name + "Label");

                                    if (myLabel != null)
                                        myLabel.Text = childNode.InnerText;
                                }
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            string myXPathEach = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_Catalogo.xml"));

            if (!CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_Edt").InnerText))
                Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223599");

            Panel myPanel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPathEach);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            if (childNode.Name.IndexOf("ZMCF_") > -1)
                            {
                                myPanel = (Panel)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myPanel != null)
                                    myPanel.Visible = Convert.ToBoolean(childNode.InnerText);
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }

            if (!Page.IsPostBack)
                SetQueryOfID2CategoriaDropDownList(mydoc.SelectSingleNode(myXPath + "ZMCD_ZIMCategoria1").InnerText
                    , GetLang()
                    , mydoc.SelectSingleNode(myXPath + "ZMCD_Cat1Liv").InnerText);

            CategorieMultiple CategorieMultiple1 = (CategorieMultiple)FormView1.FindControl("CategorieMultiple1");

            if (CategorieMultiple1 != null)
            {
                CategorieMultiple1.ZeusIdModulo = mydoc.SelectSingleNode(myXPath + "ZMCD_ZIMCategoria2").InnerText;
                CategorieMultiple1.ZeusLangCode = GetLang();
                CategorieMultiple1.RepeatColumns = mydoc.SelectSingleNode(myXPath + "ZMCD_ColCategoria2").InnerText;
                CategorieMultiple1.PAGE_ID = XRI.Value;
                CategorieMultiple1.GetCategorie();
            }

            Label ZMCD_UrlSection = (Label)FormView1.FindControl("ZMCD_UrlSection");

            if (ZMCD_UrlSection != null)
                ZMCD_UrlSection.Text = mydoc.SelectSingleNode(myXPath + "ZMCD_UrlSection").InnerText;

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    private bool CheckPermit(string AllRoles)
    {
        bool IsPermit = false;

        try
        {
            if (AllRoles.Length == 0)
                return false;

            char[] delimiter = { ',' };

            foreach (string roles in Roles.GetRolesForUser(Membership.GetUser().UserName))
            {
                string[] PressRoles = AllRoles.Split(delimiter);

                for (int i = 0; i < PressRoles.Length; ++i)
                {
                    if (PressRoles[i].Trim().Equals(roles.Trim()))
                    {
                        IsPermit = true;
                        break;
                    }
                }
            }
        }
        catch (Exception)
        { 
        }

        return IsPermit;
    }

    private string PrintMyJavaScript1()
    {
        string MyJavaScript = string.Empty;

        MyJavaScript += "function CheckDescriptionLength(sender, args)";
        MyJavaScript += "{";
        MyJavaScript += "if(document.getElementById('";
        MyJavaScript += FormView1.FindControl("RequiredFile").ClientID;
        MyJavaScript += "').checked || document.getElementById('";
        MyJavaScript += FormView1.FindControl("RequiredURL").ClientID;
        MyJavaScript += "').checked)";
        MyJavaScript += "if(document.getElementById('";
        MyJavaScript += FormView1.FindControl("Allegato1_DescTextBox").ClientID;
        MyJavaScript += "').value.length<=0)";
        MyJavaScript += "args.IsValid = false;";
        MyJavaScript += "return;";
        MyJavaScript += "}";

        return MyJavaScript;
    }

    private string PrintMyJavaScript3()
    {
        string MyJavaScript = string.Empty;

        MyJavaScript += "function CheckUrlLength(sender, args)";
        MyJavaScript += "{";
        MyJavaScript += "if(document.getElementById('";
        MyJavaScript += FormView1.FindControl("RequiredURL").ClientID;
        MyJavaScript += "').checked";
        MyJavaScript += " && document.getElementById('";
        MyJavaScript += FormView1.FindControl("Allegato1_UrlEsternoTextBox").ClientID;
        MyJavaScript += "').value.length<=0";
        MyJavaScript += " && document.getElementById('";
        MyJavaScript += FormView1.FindControl("Allegato1_UrlHiddenField").ClientID;
        MyJavaScript += "').value.length<=0)";
        MyJavaScript += "args.IsValid = false;";
        MyJavaScript += "return;";
        MyJavaScript += "}";

        return MyJavaScript;
    }

    private string PrintMyJavaScript4()
    {
        string MyJavaScript = string.Empty;

        MyJavaScript += "function CheckDescriptionLength2(sender, args)";
        MyJavaScript += "{";
        MyJavaScript += "if(document.getElementById('";
        MyJavaScript += FormView1.FindControl("RequiredFile2").ClientID;
        MyJavaScript += "').checked || document.getElementById('";
        MyJavaScript += FormView1.FindControl("RequiredURL2").ClientID;
        MyJavaScript += "').checked)";
        MyJavaScript += "if(document.getElementById('";
        MyJavaScript += FormView1.FindControl("Allegato1_DescTextBox2").ClientID;
        MyJavaScript += "').value.length<=0)";
        MyJavaScript += "args.IsValid = false;";
        MyJavaScript += "return;";
        MyJavaScript += "}";

        return MyJavaScript;
    }

    private string PrintMyJavaScript6()
    {
        string MyJavaScript = string.Empty;

        MyJavaScript += "function CheckUrlLength2(sender, args)";
        MyJavaScript += "{";
        MyJavaScript += "if(document.getElementById('";
        MyJavaScript += FormView1.FindControl("RequiredURL2").ClientID;
        MyJavaScript += "').checked";
        MyJavaScript += " && document.getElementById('";
        MyJavaScript += FormView1.FindControl("Allegato1_UrlEsternoTextBox2").ClientID;
        MyJavaScript += "').value.length<=0";
        MyJavaScript += " && document.getElementById('";
        MyJavaScript += FormView1.FindControl("Allegato1_UrlHiddenField").ClientID;
        MyJavaScript += "').value.length<=0)";
        MyJavaScript += "args.IsValid = false;";
        MyJavaScript += "return;";
        MyJavaScript += "}";

        return MyJavaScript;
    }

    private string GetLang()
    {
        try
        {
            delinea myDelinea = new delinea();
            string Lang = "ITA";

            if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
            {
                if (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang"))
                    Lang = Request.QueryString["Lang"];
            }

            return Lang;
        }
        catch (Exception)
        {
            return "ITA";
        }
    }

    private bool SetQueryOfID2CategoriaDropDownList(string ZeusIdModulo, string ZeusLangCode, string PZV_Cat1Liv)
    {
        try
        {
            SqlDataSource dsCategoria = (SqlDataSource)FormView1.FindControl("dsCategoria");
            DropDownList ID2CategoriaDropDownList = (DropDownList)FormView1.FindControl("ID2CategoriaDropDownList");
            HiddenField ID2CategoriaHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaHiddenField");

            switch (PZV_Cat1Liv)
            {
                case "123":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv1Liv2Liv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "Catliv1Liv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;


                case "23":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv2Liv3]";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;

                default:
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;
            }

            ID2CategoriaDropDownList.SelectedIndex 
                = ID2CategoriaDropDownList.Items.IndexOf(ID2CategoriaDropDownList.Items.FindByValue(ID2CategoriaHiddenField.Value));

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    protected void ID2CategoriaDropDownList_SelectIndexChange(object sender, EventArgs e)
    {
        HiddenField ID2CategoriaHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaHiddenField");
        DropDownList ID2CategoriaDropDownList = (DropDownList)sender;

        ID2CategoriaHiddenField.Value = ID2CategoriaDropDownList.SelectedValue;
    }

    protected void RecordEdtUserHidden_DataBinding(object sender, EventArgs e)
    {
        HiddenField UtenteCreazione = (HiddenField)sender;
        UtenteCreazione.Value = Membership.GetUser().ProviderUserKey.ToString();
    }

    protected void RecordEdtDateHidden_DataBinding(object sender, EventArgs e)
    {
        HiddenField DataCreazione = (HiddenField)sender;
        DataCreazione.Value = DateTime.Now.ToString();
    }

    protected void PWI_Area(object sender, EventArgs e)
    {
        DateTime Data = new DateTime();
        Data = DateTime.Now;
        TextBox DataCreazione = (TextBox)sender;
        if (DataCreazione.Text == string.Empty)
        {
            DataCreazione.Text = Data.ToString("dd/MM/yyyy");
        }
    }

    protected void PWF_Area(object sender, EventArgs e)
    {
        TextBox DataFinale = (TextBox)sender;
        if (DataFinale.Text == string.Empty)
        {
            DataFinale.Text = "31/12/2040";
        }
    }

    protected void ImageRaider_DataBinding(object sender, EventArgs e)
    {
        ImageRaider ImageRaider = (ImageRaider)sender;
        ImageRaider.SetDefaultEditBetaImage();
        ImageRaider.SetDefaultEditGammaImage();
    }



    protected void FileUploadCustomValidator_ServerValidate(object sender, ServerValidateEventArgs args)
    {
        try
        {
            ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");
            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");

            if (ImageRaider1.isValid())
                InsertButton.CommandName = "Update";
            else
                InsertButton.CommandName = string.Empty;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    protected void Check_RadioButton_Validate(object sender, ServerValidateEventArgs args)
    {
        HiddenField HiddenNomeFile = (HiddenField)FormView1.FindControl("Allegato1_File");
        HiddenField Allegato_Peso = (HiddenField)FormView1.FindControl("Allegato_Peso");
        RadioButton SelettoreNull = (RadioButton)FormView1.FindControl("SelettoreNull");
        RadioButton RequiredURL = (RadioButton)FormView1.FindControl("RequiredURL");
        RadioButton RequiredFile = (RadioButton)FormView1.FindControl("RequiredFile");
        TextBox Allegato1_DescTextBox = (TextBox)FormView1.FindControl("Allegato1_DescTextBox");
        TextBox Allegato1_UrlEsternoTextBox = (TextBox)FormView1.FindControl("Allegato1_UrlEsternoTextBox");
        HyperLink FileUploadLink = (HyperLink)FormView1.FindControl("FileUploadLink");
        CustomValidator CustomValidator1 = (CustomValidator)sender;

        if (!SelettoreNull.Checked)
        {
            if ((Allegato1_DescTextBox.Visible) && (Allegato1_DescTextBox.Text.Length == 0))
            {
                CustomValidator1.ErrorMessage = "Obbligatorio";
                args.IsValid = false;
                HiddenNomeFile.Value = null;
                Allegato_Peso.Value = null;
            }
            else if ((RequiredURL.Checked) && (Allegato1_UrlEsternoTextBox.Text.Length == 0))
            {
                CustomValidator1.ErrorMessage = "Url obbligatorio";
                args.IsValid = false;
            }

            else args.IsValid = true;
        }
    }

    protected void RadioButton1_CheckedChanged(object sender, EventArgs e)
    {
        HiddenField Allegato_Selettore = (HiddenField)FormView1.FindControl("Allegato_Selettore");
        Allegato_Selettore.Value = "FIL";
    }

    protected void RadioButton2_CheckedChanged(object sender, EventArgs e)
    {
        HiddenField Allegato_Selettore = (HiddenField)FormView1.FindControl("Allegato_Selettore");
        Allegato_Selettore.Value = "LNK";
    }

    protected void SelettoreNull_CheckedChanged(object sender, EventArgs e)
    {
        HiddenField Allegato_Selettore = (HiddenField)FormView1.FindControl("Allegato_Selettore");
        Allegato_Selettore.Value = string.Empty;
    }

    protected void UrlLinkButton_DataBinding(object sender, EventArgs e)
    {
        LinkButton UrlLinkButton = (LinkButton)sender;
        if (UrlLinkButton.PostBackUrl.Length == 0)
            UrlLinkButton.Visible = false;
        else
        {
            UrlLinkButton.OnClientClick = "window.open('" + UrlLinkButton.PostBackUrl + "'); return false;";
        }
    }

    protected void FileUploadLinkButton_Databinding(object sender, EventArgs e)
    {
        LinkButton FileUploadLinkButton = (LinkButton)sender;
        if (FileUploadLinkButton.PostBackUrl.Length == 0)
            FileUploadLinkButton.Visible = false;
        else
        {
            FileUploadLinkButton.OnClientClick = "window.open('/ZeusInc/Catalogo/Allegati/" + FileUploadLinkButton.PostBackUrl + "'); return false;";
            FileUploadLinkButton.Text = FileUploadLinkButton.PostBackUrl.ToString();
        }
    }

    protected void FileUploadLink_DataBinding(object sender, EventArgs e)
    {
        HyperLink FileUploadLink = (HyperLink)sender;
        if (FileUploadLink.NavigateUrl.Length == 0)
            FileUploadLink.Visible = false;
        else
        {
            FileUploadLink.Text = FileUploadLink.NavigateUrl.ToString();
            FileUploadLink.NavigateUrl = "/ZeusInc/Catalogo/Allegati/" + FileUploadLink.NavigateUrl;
            FileUploadLink.Target = "_blank";
        }
    }

    protected void Allegato_Selettore_DataBinding(object sender, EventArgs e)
    {
        HiddenField Allegato_Selettore = (HiddenField)sender;

        switch (Allegato_Selettore.Value)
        {
            case "FIL":
                RadioButton RequiredFile = (RadioButton)FormView1.FindControl("RequiredFile");
                RequiredFile.Checked = true;
                break;

            case "LNK":
                RadioButton RequiredURL = (RadioButton)FormView1.FindControl("RequiredURL");
                RequiredURL.Checked = true;
                break;

            default:
                RadioButton SelettoreNull = (RadioButton)FormView1.FindControl("SelettoreNull");
                SelettoreNull.Checked = true;
                break;
        }
    }

    protected void Check_RadioButton_Validate2(object sender, ServerValidateEventArgs args)
    {
        HiddenField HiddenNomeFile = (HiddenField)FormView1.FindControl("Allegato1_File2");
        HiddenField Allegato_Peso = (HiddenField)FormView1.FindControl("Allegato_Peso2");
        RadioButton SelettoreNull = (RadioButton)FormView1.FindControl("SelettoreNull2");
        RadioButton RequiredURL = (RadioButton)FormView1.FindControl("RequiredURL2");
        RadioButton RequiredFile = (RadioButton)FormView1.FindControl("RequiredFile2");
        TextBox Allegato1_DescTextBox = (TextBox)FormView1.FindControl("Allegato1_DescTextBox2");
        TextBox Allegato1_UrlEsternoTextBox = (TextBox)FormView1.FindControl("Allegato1_UrlEsternoTextBox2");
        HyperLink FileUploadLink = (HyperLink)FormView1.FindControl("FileUploadLink2");
        CustomValidator CustomValidator1 = (CustomValidator)sender;

        if (!SelettoreNull.Checked)
        {
            if ((Allegato1_DescTextBox.Visible) && (Allegato1_DescTextBox.Text.Length == 0))
            {
                CustomValidator1.ErrorMessage = "Obbligatorio";
                args.IsValid = false;
                HiddenNomeFile.Value = null;
                Allegato_Peso.Value = null;
            }
            else if ((RequiredURL.Checked) && (Allegato1_UrlEsternoTextBox.Text.Length == 0))
            {
                CustomValidator1.ErrorMessage = "Url obbligatorio";
                args.IsValid = false;
            }

            else args.IsValid = true;
        }
    }

    protected void RadioButton1_CheckedChanged2(object sender, EventArgs e)
    {
        HiddenField Allegato_Selettore = (HiddenField)FormView1.FindControl("Allegato_Selettore2");
        Allegato_Selettore.Value = "FIL";
    }

    protected void RadioButton2_CheckedChanged2(object sender, EventArgs e)
    {
        HiddenField Allegato_Selettore = (HiddenField)FormView1.FindControl("Allegato_Selettore2");
        Allegato_Selettore.Value = "LNK";
    }

    protected void SelettoreNull_CheckedChanged2(object sender, EventArgs e)
    {
        HiddenField Allegato_Selettore = (HiddenField)FormView1.FindControl("Allegato_Selettore2");
        Allegato_Selettore.Value = string.Empty;
    }

    protected void UrlLinkButton_DataBinding2(object sender, EventArgs e)
    {
        LinkButton UrlLinkButton = (LinkButton)sender;
        if (UrlLinkButton.PostBackUrl.Length == 0)
            UrlLinkButton.Visible = false;
        else
        {
            UrlLinkButton.OnClientClick = "window.open('" + UrlLinkButton.PostBackUrl + "'); return false;";
        }
    }

    protected void FileUploadLinkButton_Databinding2(object sender, EventArgs e)
    {
        LinkButton FileUploadLinkButton = (LinkButton)sender;
        if (FileUploadLinkButton.PostBackUrl.Length == 0)
            FileUploadLinkButton.Visible = false;
        else
        {
            FileUploadLinkButton.OnClientClick = "window.open('/ZeusInc/Catalogo/Allegati/" + FileUploadLinkButton.PostBackUrl + "'); return false;";
            FileUploadLinkButton.Text = FileUploadLinkButton.PostBackUrl.ToString();
        }
    }

    protected void FileUploadLink_DataBinding2(object sender, EventArgs e)
    {
        HyperLink FileUploadLink = (HyperLink)sender;
        if (FileUploadLink.NavigateUrl.Length == 0)
            FileUploadLink.Visible = false;
        else
        {
            FileUploadLink.Text = FileUploadLink.NavigateUrl.ToString();
            FileUploadLink.NavigateUrl = "/ZeusInc/Catalogo/Allegati/" + FileUploadLink.NavigateUrl;
            FileUploadLink.Target = "_blank";
        }
    }

    protected void Allegato_Selettore_DataBinding2(object sender, EventArgs e)
    {
        HiddenField Allegato_Selettore = (HiddenField)sender;

        switch (Allegato_Selettore.Value)
        {
            case "FIL":
                RadioButton RequiredFile = (RadioButton)FormView1.FindControl("RequiredFile2");
                RequiredFile.Checked = true;
                break;

            case "LNK":
                RadioButton RequiredURL = (RadioButton)FormView1.FindControl("RequiredURL2");
                RequiredURL.Checked = true;
                break;

            default:
                RadioButton SelettoreNull = (RadioButton)FormView1.FindControl("SelettoreNull2");
                SelettoreNull.Checked = true;
                break;
        }
    }

    protected void InsertButton_Click(object sender, EventArgs e)
    {
        LinkButton InsertButton = (LinkButton)sender;
        RadioButton AutomaticaThumbnailRadioButton = (RadioButton)FormView1.FindControl("AutomaticaThumbnailRadioButton");
        RadioButton ManualeThumbnailRadioButton = (RadioButton)FormView1.FindControl("ManualeThumbnailRadioButton");
        HiddenField FileNameBetaHiddenField = (HiddenField)FormView1.FindControl("FileNameBetaHiddenField");
        HiddenField FileNameGammaHiddenField = (HiddenField)FormView1.FindControl("FileNameGammaHiddenField");
        HiddenField Immagine12AltHiddenField = (HiddenField)FormView1.FindControl("Immagine12AltHiddenField");
        ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");
        HiddenField FileNameBeta2HiddenField = (HiddenField)FormView1.FindControl("FileNameBeta2HiddenField");
        HiddenField FileNameGamma2HiddenField = (HiddenField)FormView1.FindControl("FileNameGamma2HiddenField");
        HiddenField Immagine34AltHiddenField = (HiddenField)FormView1.FindControl("Immagine34AltHiddenField");
        ImageRaider ImageRaider2 = (ImageRaider)FormView1.FindControl("ImageRaider2");
        HiddenField FileNameBeta3HiddenField = (HiddenField)FormView1.FindControl("FileNameBeta3HiddenField");
        HiddenField FileNameGamma3HiddenField = (HiddenField)FormView1.FindControl("FileNameGamma3HiddenField");
        HiddenField Immagine56AltHiddenField = (HiddenField)FormView1.FindControl("Immagine56AltHiddenField");
        ImageRaider ImageRaider3 = (ImageRaider)FormView1.FindControl("ImageRaider3");
        string filePath = string.Empty;
        string fileName = string.Empty;

        if (InsertButton.CommandName != string.Empty)
        {
            if (ImageRaider1.GenerateBeta(string.Empty))
                FileNameBetaHiddenField.Value = ImageRaider1.ImgBeta_FileName;
            else FileNameBetaHiddenField.Value = ImageRaider1.DefaultBetaImage;

            if (ImageRaider1.GenerateGamma(string.Empty))
                FileNameGammaHiddenField.Value = ImageRaider1.ImgGamma_FileName;
            else FileNameGammaHiddenField.Value = ImageRaider1.DefaultGammaImage;

            if (ImageRaider2.GenerateBeta(string.Empty))
                FileNameBeta2HiddenField.Value = ImageRaider2.ImgBeta_FileName;
            else FileNameBeta2HiddenField.Value = ImageRaider2.DefaultBetaImage;

            if (ImageRaider2.GenerateGamma(string.Empty))
                FileNameGamma2HiddenField.Value = ImageRaider2.ImgGamma_FileName;
            else FileNameGamma2HiddenField.Value = ImageRaider2.DefaultGammaImage;

            if (ImageRaider3.GenerateBeta(string.Empty))
                FileNameBeta3HiddenField.Value = ImageRaider3.ImgBeta_FileName;
            else FileNameBeta3HiddenField.Value = ImageRaider3.DefaultBetaImage;

            if (ImageRaider3.GenerateGamma(string.Empty))
                FileNameGamma3HiddenField.Value = ImageRaider3.ImgGamma_FileName;
            else FileNameGamma3HiddenField.Value = ImageRaider3.DefaultGammaImage;

            HiddenField Allegato1_File = (HiddenField)FormView1.FindControl("Allegato1_File");
            UploadRaider Allegato_UploadRaider1 = (UploadRaider)FormView1.FindControl("Allegato_UploadRaider1");

            if ((Allegato_UploadRaider1 != null)
                && (Allegato_UploadRaider1.GotFile()))
            {
                Allegato_UploadRaider1.SaveFile();
                Allegato1_File.Value = Allegato_UploadRaider1.FileName;
            }

            HiddenField Allegato2_File = (HiddenField)FormView1.FindControl("Allegato2_File");
            UploadRaider Allegato_UploadRaider2 = (UploadRaider)FormView1.FindControl("Allegato_UploadRaider2");

            if ((Allegato_UploadRaider2 != null)
                && (Allegato_UploadRaider2.GotFile()))
            {
                Allegato_UploadRaider2.SaveFile();
                Allegato2_File.Value = Allegato_UploadRaider2.FileName;
            }

            HiddenField UrlRewriteHiddenField = (HiddenField)FormView1.FindControl("UrlRewriteHiddenField");
            TextBox UrlPageDetailTextBox = (TextBox)FormView1.FindControl("UrlPageDetailTextBox");

            UrlRewriteHiddenField.Value = myDelinea.myHtmlEncode(UrlPageDetailTextBox.Text.TrimEnd());
        }
    }

    protected void dsArticoloNew_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        if ((e.Exception == null)
            && (e.AffectedRows > 0))
        {
            CategorieMultiple CategorieMultiple1 = (CategorieMultiple)FormView1.FindControl("CategorieMultiple1");
            CategorieMultiple1.SetCategorie();
            Response.Redirect("~/Zeus/Catalogo/Articoli_Lst.aspx?XRI=" + XRI.Value
                + "&ZIM=" + ZIM.Value 
                + "&Lang=" + GetLang());
        }
        else
            Response.Redirect("~/Zeus/System/Message.aspx?UpdErr");
    }
}