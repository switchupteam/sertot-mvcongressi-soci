﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" %>

<script runat="server">
    
   
    static string TitoloPagina = "Elenco associazioni articoli / accessori";


    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;


        if ((Request.QueryString["IDArt"] != null)
            && (Request.QueryString["IDArt"].Length > 0))
            try
            {
                Convert.ToInt32(Request.QueryString["IDArt"]);
                AssociazioniSqlDatasource.SelectCommand +=
                    " AND ID1Articolo=" + Request.QueryString["IDArt"];
            }
            catch (Exception)
            {

            }

        if ((Request.QueryString["CatArt"] != null)
           && (Request.QueryString["CatArt"].Length > 0))
            try
            {
                Convert.ToInt32(Request.QueryString["CatArt"]);
                AssociazioniSqlDatasource.SelectCommand +=
                    " AND ID2CategoriaArticolo=" + Request.QueryString["CatArt"];
            }
            catch (Exception)
            {

            }


        if ((Request.QueryString["IDAcc"] != null)
       && (Request.QueryString["IDAcc"].Length > 0))
            try
            {
                Convert.ToInt32(Request.QueryString["IDAcc"]);
                AssociazioniSqlDatasource.SelectCommand +=
                    " AND ID1Accessorio=" + Request.QueryString["IDAcc"];
            }
            catch (Exception)
            {

            }

        if ((Request.QueryString["CatAcc"] != null)
           && (Request.QueryString["CatAcc"].Length > 0))
            try
            {
                Convert.ToInt32(Request.QueryString["CatAcc"]);
                AssociazioniSqlDatasource.SelectCommand +=
                    " AND ID2CategoriaAccessorio=" + Request.QueryString["CatAcc"];
            }
            catch (Exception)
            {

            }



    }//fine Page_Load

    


  


    

</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <div class="LayGridTitolo1">
        <asp:Label ID="CaptionallRecordLabel" runat="server" Text="Elenco associazioni articoli / accessori"
            SkinID="GridTitolo1"></asp:Label></div>
    <asp:GridView ID="AssociazioniGridView" runat="server" AllowPaging="True" AllowSorting="True"
        DataSourceID="AssociazioniSqlDatasource" AutoGenerateColumns="False" PageSize="50">
        <Columns>
            <asp:BoundField DataField="CodiceArticolo" HeaderText="Cod. art." SortExpression="CodiceArticolo" />
            <asp:BoundField DataField="Articolo" HeaderText="Titolo art." SortExpression="Articolo" />
            <asp:BoundField DataField="ID1Articolo" HeaderText="ID art." SortExpression="ID1Articolo" />
            <asp:BoundField DataField="CategoriaArticolo" HeaderText="Categoria art." SortExpression="CategoriaArticolo" />
            <asp:BoundField DataField="CodiceAccessorio" HeaderText="Cod. acc." SortExpression="CodiceAccessorio" />
            <asp:BoundField DataField="Accessorio" HeaderText="Titolo acc." SortExpression="Accessorio" />
            <asp:BoundField DataField="ID1Accessorio" HeaderText="ID acc." SortExpression="ID1Accessorio" />
            <asp:BoundField DataField="CategoriaAccessorio" HeaderText="Categoria acc." SortExpression="CategoriaAccessorio" />
            <asp:HyperLinkField DataNavigateUrlFields="ID1CatAssoc" DataNavigateUrlFormatString="~/Zeus/Catalogo/Delete.aspx?XRI={0}"
                                        HeaderText="Elimina" Text="Elimina">
                                        <ControlStyle CssClass="GridView_Button1"  />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:HyperLinkField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="AssociazioniSqlDatasource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SELECT     ID1CatAssoc,
             ID1Accessorio,
           ID1Articolo, 
            Articolo, 
           CategoriaArticolo, 
                      Accessorio, 
                      CategoriaAccessorio,
                       CodiceArticolo, 
                      CodiceAccessorio
FROM        vwCatalogo_Accessori
WHERE Articolo LIKE '%' + @Articolo + '%'
 AND Accessorio LIKE '%' + @Accessorio + '%' 
 AND CodiceArticolo LIKE '%' + @CodiceArticolo + '%'
 AND CodiceAccessorio LIKE '%' + @CodiceAccessorio + '%'
 ">
        <SelectParameters>
            <asp:QueryStringParameter Name="Articolo" QueryStringField="ModArt" DefaultValue="%"
                Type="String" />
            <asp:QueryStringParameter Name="Accessorio" QueryStringField="ModAcc" DefaultValue="%"
                Type="String" />
            <asp:QueryStringParameter Name="CodiceArticolo" QueryStringField="CodArt" DefaultValue="%"
                Type="String" />
            <asp:QueryStringParameter Name="CodiceAccessorio" QueryStringField="CodArt" DefaultValue="%"
                Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
