<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="Articoli_Edt.aspx.cs"
    Inherits="Catalogo_Articoli_Edt"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <script type="text/javascript">
        function OnClientModeChange(editor) {
            var mode = editor.get_mode();
            var doc = editor.get_document();
            var head = doc.getElementsByTagName("HEAD")[0];
            var link;

            switch (mode) {
                case 1: //remove the external stylesheet when displaying the content in Design mode    
                    //var external = doc.getElementById("external");
                    //head.removeChild(external);
                    break;
                case 2:
                    break;
                case 4: //apply your external css stylesheet to Preview mode    
                    link = doc.createElement("LINK");
                    link.setAttribute("href", "/SiteCss/Telerik.css");
                    link.setAttribute("rel", "stylesheet");
                    link.setAttribute("type", "text/css");
                    link.setAttribute("id", "external");
                    head.appendChild(link);
                    break;
            }
        }

        function editorCommandExecuted(editor, args) {
            if (!$telerik.isChrome)
                return;
            var dialogName = args.get_commandName();
            var dialogWin = editor.get_dialogOpener()._dialogContainers[dialogName];
            if (dialogWin) {
                var cellEl = dialogWin.get_contentElement() || dialogWin.ui.contentCell || dialogWin.ui.content,
                frame = dialogWin.get_contentFrame();
                frame.onload = function () {
                    cellEl.style.cssText = "";
                    dialogWin.autoSize();
                }
            }
        }
    </script>

    <asp:HiddenField ID="TitleField" runat="server" />
    <asp:HiddenField ID="ZIM" runat="server" />
    <asp:HiddenField ID="XRI" runat="server" />
    <%--<dlc:RunUploadRaider ID="RunUploadRaider_All" runat="server" />--%>
    
    <dlc:PermitRoles ID="myPermitRoles" runat="server" PAGE_TYPE="EDT" SQL_TABLE="tbPz_Catalogo" />
    <asp:FormView ID="FormView1" runat="server" DataSourceID="dsArticoloNew" DefaultMode="Edit"
        Width="100%">
        <EditItemTemplate>
            <div id="tabs">

                <ul>
                    <li><a href="#Principale">Principale</a></li>
                    <li><a href="#Immagini">Immagini</a></li>
                    <li><a href="#Contenuti">Contenuti</a></li>
                    <li><a href="#Listino">Listino</a></li>
                    <li><a href="#MultiCategoria">Categorie multiple</a></li>

                </ul>
                <div id="Principale">
                    <div class="BlockBox">
                        <div class="BlockBoxHeader">
                            <asp:Label ID="IdentificativoLabel" runat="server">Identificativo articolo</asp:Label>
                        </div>
                        <table>
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label ID="ZML_Articolo" SkinID="FieldDescription" runat="server" Text="Articolo *"></asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:TextBox ID="ArticoloTextBox" runat="server" Columns="100"
                                        MaxLength="100" Text='<%# Bind("Articolo") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                        runat="server" ControlToValidate="ArticoloTextBox" SkinID="ZSSM_Validazione01"
                                        Display="Dynamic" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <asp:Panel ID="ZMCF_Brand" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_Brand" SkinID="FieldDescription" runat="server" Text="Brand *"></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:DropDownList ID="ID2BrandDropDownList" runat="server" AppendDataBoundItems="True" SelectedValue='<%# Bind("ID2Brand") %>'
                                            DataSourceID="ID2BrandSqlDataSource" DataTextField="Brand" DataValueField="ID1Brand">
                                            <asp:ListItem Text="> Seleziona" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:SqlDataSource ID="ID2BrandSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                            SelectCommand="SELECT ID1Brand,Brand FROM  [vwBrands_All] WHERE [AttivoArea1] >0 ORDER BY Brand ASC"></asp:SqlDataSource>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="ZMCF_Codice" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="Label8" runat="server" SkinID="FieldDescription" Text="Label">Codice *</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:TextBox ID="CodiceTextBox" runat="server" Columns="50" MaxLength="45" Text='<%# Bind("Codice") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                            runat="server" ControlToValidate="CodiceTextBox" SkinID="ZSSM_Validazione01"
                                            Display="Dynamic" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <%--<asp:Panel ID="ZMCF_BoxCatalogoParentPanel" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_BoxCatalogoParent" runat="server" SkinID="FieldDescription"
                                    Text="Label"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="CodiceParentTextBox" runat="server" Columns="50" MaxLength="45"
                                    Text='<%# Bind("CodiceParent") %>'></asp:TextBox>
                            </td>
                        </tr>
                    </asp:Panel>--%>
                            <asp:Panel ID="ZMCF_Categoria1" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_Categoria1" runat="server" SkinID="FieldDescription" Text="Label">Categoria *</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:DropDownList ID="ID2CategoriaDropDownList" runat="server" OnSelectedIndexChanged="ID2CategoriaDropDownList_SelectIndexChange"
                                            AppendDataBoundItems="True">
                                            <asp:ListItem Text="> Seleziona" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:HiddenField ID="ID2CategoriaHiddenField" runat="server" Value='<%# Bind("ID2Categoria1") %>' />
                                        <asp:RequiredFieldValidator ID="ID2CategoriaRequiredFieldValidator" ErrorMessage="Obbligatorio"
                                            runat="server" ControlToValidate="ID2CategoriaDropDownList" SkinID="ZSSM_Validazione01"
                                            Display="Dynamic" InitialValue="0">
                                        </asp:RequiredFieldValidator>
                                        <asp:SqlDataSource ID="dsCategoria" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>">
                                            <SelectParameters>
                                                <asp:QueryStringParameter DefaultValue="0" Name="ZeusIdModulo" QueryStringField="ZIM"
                                                    Type="String" />
                                                <asp:QueryStringParameter DefaultValue="ITA" Name="ZeusLangCode" QueryStringField="Lang"
                                                    Type="String" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="ZMCF_TagRicerca" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="Label2" runat="server" SkinID="FieldDescription" Text="Label">Tag di ricerca</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:TextBox ID="TagTextBox" runat="server" Columns="100" MaxLength="100" Text='<%# Bind("ZeusTags") %>'></asp:TextBox>
                                    </td>
                                </tr>
                            </asp:Panel>
                        </table>
                    </div>
                    <asp:Panel ID="ZMCF_BoxMetaTag" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="TagPagina" runat="server" Text="Tag di pagina" />
                            </div>
                            <table border="0" cellpadding="2" cellspacing="1">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="Description_Label" SkinID="FieldDescription" runat="server" Text="Description"></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:TextBox ID="ZMCD_TagDescription" runat="server" Text='<%# Bind("TagDescription") %>'
                                            Columns="100" MaxLength="280"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server"
                                            ValidationExpression="^[a-zA-Z0-9_.;:+, ]+$"
                                            Display="Dynamic" ErrorMessage="Caratteri consentiti: lettere numeri e .,:;_-+"
                                            SkinID="ZSSM_Validazione01"
                                            ControlToValidate="ZMCD_TagDescription"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="Keywords_Label" runat="server" SkinID="FieldDescription" Text="Keywords"></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:TextBox ID="ZMCD_TagKeywords" runat="server" Text='<%# Bind("TagKeywords") %>'
                                            Columns="100" MaxLength="280"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server"
                                            ValidationExpression="^[a-zA-Z0-9_.;:+, ]+$"
                                            Display="Dynamic" ErrorMessage="Caratteri consentiti: lettere numeri e .,:;_-+"
                                            SkinID="ZSSM_Validazione01"
                                            ControlToValidate="ZMCD_TagKeywords"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_BoxSpecifiche" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="ZML_BoxSpecifiche" runat="server">Specifiche generiche</asp:Label>
                            </div>
                            <table>
                                <asp:Panel ID="ZMCF_SpecificaInt1" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="ZML_SpecificaInt1" SkinID="FieldDescription" runat="server" Text="SpecificaInt1"></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:TextBox ID="SpecificaInt1TextBox" runat="server" Columns="10" MaxLength="9"
                                                Text='<%# Bind("SpecificaInt1") %>'></asp:TextBox>
                                            <asp:RangeValidator ID="RangeValidator6" runat="server" ControlToValidate="SpecificaInt1TextBox"
                                                SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="Richiesto numero intero"
                                                MaximumValue="9999999999999" MinimumValue="0" Type="Currency"></asp:RangeValidator>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="ZMCF_SpecificaText1" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="ZML_SpecificaText1" SkinID="FieldDescription" runat="server" Text="SpecificaText1"></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:TextBox ID="SpecificaText1TextBox" runat="server" Columns="50" MaxLength="200"
                                                Text='<%# Bind("SpecificaText1") %>'></asp:TextBox>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="ZMCF_SpecificaText2" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="ZML_SpecificaText2" SkinID="FieldDescription" runat="server" Text="SpecificaText2"></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:TextBox ID="SpecificaText2TextBox" runat="server" Columns="50" MaxLength="200"
                                                Text='<%# Bind("SpecificaText2") %>'></asp:TextBox>
                                        </td>
                                    </tr>
                                </asp:Panel>
                            </table>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_BoxUrlRewrite" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="Label1" runat="server" Text="Page Link / URL (indirizzo pagina web)"></asp:Label>
                            </div>
                            <table>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="Label3" runat="server" Text="Url Rewrite" SkinID="FieldDescription"></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:Label ID="ServerLabel" runat="server" SkinID="FieldValue" Text='<%# "http://" + Request.ServerVariables["SERVER_NAME"]  %>'></asp:Label><asp:Label
                                            ID="ZMCD_UrlSection" runat="server" SkinID="FieldValue"></asp:Label><asp:Label
                                                ID="ID1Publisher" runat="server" SkinID="FieldValue" Text='<%# Eval("ID1Articolo","{0}/") %>'></asp:Label><asp:TextBox
                                                    ID="UrlPageDetailTextBox" runat="server" Text='<%# Eval("UrlRewrite") %>' Width="400"></asp:TextBox>
                                        <asp:Label ID="IDUrlRewriting" runat="server" SkinID="CorpoTesto">.aspx</asp:Label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator27" ErrorMessage="Obbligatorio"
                                            runat="server" ControlToValidate="UrlPageDetailTextBox" Display="Dynamic" SkinID="ZSSM_Validazione01"
                                            ValidationGroup="myValidation">
                                        </asp:RequiredFieldValidator>
                                        <asp:Label ID="InfoUrlRewritingLabel" runat="server" Text="<br />Possono essere utilizzati solo i caratteri alfanumerici ed i caratteri - _  Tutti gli altri caratteri verranno sostituiti automaticamente."
                                            SkinID="Note"></asp:Label>
                                        <asp:HiddenField ID="UrlRewriteHiddenField" runat="server" Value='<%# Bind("UrlRewrite") %>' />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                    <div class="BlockBox">
                        <div class="BlockBoxHeader">
                            <asp:Label ID="Label6" runat="server">Planner</asp:Label>
                        </div>
                        <table>
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label ID="ZML_PWArea1" SkinID="FieldDescription" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:CheckBox ID="PW_Area1CheckBox" runat="server" Checked='<%# Bind("PW_Area1") %>' />
                                    <asp:Label ID="Label20" runat="server" SkinID="FieldValue" Text="Label">Attiva pubblicazione dalla data</asp:Label>
                                    <asp:TextBox ID="PWI_Area1TextBox" runat="server" Columns="10" MaxLength="10" Text='<%# Bind("PWI_Area1", "{0:d}") %>'
                                        OnDataBinding="PWI_Area"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                                        runat="server" ControlToValidate="PWI_Area1TextBox" SkinID="ZSSM_Validazione01"
                                        Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3"
                                        runat="server" ControlToValidate="PWI_Area1TextBox" SkinID="ZSSM_Validazione01"
                                        Display="Dynamic" ErrorMessage="Formato data richiesto: GG/MM/AAAA" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>&nbsp;
                            <asp:Label ID="Label9" runat="server" SkinID="FieldValue">alla data </asp:Label>&nbsp;
                            <asp:TextBox ID="PWF_Area1TextBox" runat="server" Columns="10" MaxLength="10" Text='<%# Bind("PWF_Area1", "{0:d}") %>'
                                OnDataBinding="PWF_Area"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8"
                                        runat="server" ControlToValidate="PWF_Area1TextBox" SkinID="ZSSM_Validazione01"
                                        Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5"
                                        runat="server" ControlToValidate="PWF_Area1TextBox" SkinID="ZSSM_Validazione01"
                                        Display="Dynamic" ErrorMessage="Formato data richiesto: GG/MM/AAAA" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>
                                    <asp:CompareValidator ID="CompareValidator1" runat="server"
                                        ControlToCompare="PWF_Area1TextBox" ControlToValidate="PWI_Area1TextBox" SkinID="ZSSM_Validazione01"
                                        Display="Dynamic" ErrorMessage="La data iniziale deve essere precedente o uguale alla data finale"
                                        Operator="LessThanEqual" Type="Date"></asp:CompareValidator>
                                </td>
                            </tr>
                            <asp:Panel ID="ZMCF_Area2" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_PWArea2" SkinID="FieldDescription" runat="server" Text="Label"></asp:Label>
                                        <td class="BlockBoxValue">
                                            <asp:CheckBox ID="PW_Area2CheckBox" runat="server" Checked='<%# Bind("PW_Area2") %>' />
                                            <asp:Label ID="Label7" runat="server" SkinID="FieldValue" Text="Label">Attiva pubblicazione dalla data</asp:Label>
                                            <asp:TextBox ID="PWI_Area2TextBox" runat="server" Columns="10" MaxLength="10" Text='<%# Bind("PWI_Area2", "{0:d}") %>'
                                                OnDataBinding="PWI_Area"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9"
                                                runat="server" ControlToValidate="PWI_Area2TextBox" SkinID="ZSSM_Validazione01"
                                                Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4"
                                                runat="server" ControlToValidate="PWI_Area2TextBox" SkinID="ZSSM_Validazione01"
                                                Display="Dynamic" ErrorMessage="Formato data richiesto: GG/MM/AAAA" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>&nbsp;
                                    <asp:Label ID="Label12" runat="server" SkinID="FieldValue">alla data </asp:Label>&nbsp;
                                    <asp:TextBox ID="PWF_Area2TextBox" runat="server" Columns="10" MaxLength="10" Text='<%# Bind("PWF_Area2", "{0:d}") %>'
                                        OnDataBinding="PWF_Area"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10"
                                                runat="server" ControlToValidate="PWF_Area2TextBox" SkinID="ZSSM_Validazione01"
                                                Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6"
                                                runat="server" ControlToValidate="PWF_Area2TextBox" SkinID="ZSSM_Validazione01"
                                                Display="Dynamic" ErrorMessage="Formato data richiesto: GG/MM/AAAA" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>
                                            <asp:CompareValidator ID="CompareValidator2" runat="server"
                                                ControlToCompare="PWF_Area2TextBox" ControlToValidate="PWI_Area2TextBox" SkinID="ZSSM_Validazione01"
                                                Display="Dynamic" ErrorMessage="La data iniziale deve essere precedente o uguale alla data finale"
                                                Operator="LessThanEqual" Type="Date"></asp:CompareValidator>
                                        </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="ZMCF_Area3" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_PWArea3" SkinID="FieldDescription" runat="server" Text="Label"></asp:Label>
                                        <td class="BlockBoxValue">
                                            <asp:CheckBox ID="PW_Area3CheckBox" runat="server" Checked='<%# Bind("PW_Area3") %>' />
                                            <asp:Label ID="InfoInizioLabel3" runat="server" SkinID="FieldValue" Text="Attiva pubblicazione dalla data"></asp:Label>
                                            <asp:TextBox ID="PWI_Area3TextBox" runat="server" Columns="10" MaxLength="10" Text='<%# Bind("PWI_Area3", "{0:d}") %>'
                                                OnDataBinding="PWI_Area"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11"
                                                runat="server" ControlToValidate="PWI_Area3TextBox" SkinID="ZSSM_Validazione01"
                                                Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                                                runat="server" ControlToValidate="PWI_Area3TextBox" SkinID="ZSSM_Validazione01"
                                                Display="Dynamic" ErrorMessage="Formato data richiesto: GG/MM/AAAA" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>&nbsp;
                                    <asp:Label ID="InfoFineLabel3" runat="server" SkinID="FieldValue" Text="alla data "></asp:Label>&nbsp;
                                    <asp:TextBox ID="PWF_Area3TextBox" runat="server" Columns="10" MaxLength="10" Text='<%# Bind("PWF_Area3", "{0:d}") %>'
                                        OnDataBinding="PWF_Area"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12"
                                                runat="server" ControlToValidate="PWF_Area3TextBox" SkinID="ZSSM_Validazione01"
                                                Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2"
                                                runat="server" ControlToValidate="PWF_Area3TextBox" SkinID="ZSSM_Validazione01"
                                                Display="Dynamic" ErrorMessage="Formato data richiesto: GG/MM/AAAA" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>
                                            <asp:CompareValidator ID="CompareValidator3" runat="server"
                                                ControlToCompare="PWF_Area3TextBox" ControlToValidate="PWI_Area3TextBox" SkinID="ZSSM_Validazione01"
                                                Display="Dynamic" ErrorMessage="La data iniziale deve essere precedente o uguale alla data finale"
                                                Operator="LessThanEqual" Type="Date"></asp:CompareValidator>
                                        </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="ZMCF_AreaZeus" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_PWAreaZeus" SkinID="FieldDescription" runat="server"
                                            Text="Label"></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:CheckBox ID="PW_Zeus1CheckBox" runat="server" Checked='<%# Bind("PW_Zeus1") %>' />
                                    </td>
                                </tr>
                            </asp:Panel>
                        </table>
                    </div>
                </div>

                <div id="Contenuti">
                    <asp:Panel ID="ZMCF_DescBreve1" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="TagPaginaLabel" runat="server">Descrizione breve</asp:Label>
                            </div>
                            <table>
                                <td class="BlockBoxDescription">
                                    <asp:Label ID="VoceLabel" runat="server" SkinID="FieldDescription" Text="Label">Descrizione<br />
                                        (Massimo 500 caratteri)</asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <CustomWebControls:TextArea ID="DescBreveTextBox" runat="server" Columns="80" EnableTheming="True"
                                        Height="100px" MaxLength="500" Rows="4" Text='<%# Bind("DescBreve1", "{0}") %>'
                                        TextMode="MultiLine"></CustomWebControls:TextArea>
                                </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_Descrizione1" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="ZML_Descrizione1" runat="server" Text="Descrizione 1"></asp:Label>
                            </div>
                            <table>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="Label13" runat="server" SkinID="FieldDescription" Text="Label">Descrizione</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <telerik:RadEditor
                                            Language="it-IT" ID="RadEditor2" runat="server"
                                            DocumentManager-DeletePaths="~/ZeusInc/Catalogo/Documents"
                                            DocumentManager-SearchPatterns="*.*"
                                            DocumentManager-ViewPaths="~/ZeusInc/Catalogo/Documents"
                                            DocumentManager-MaxUploadFileSize="52428800"
                                            DocumentManager-UploadPaths="~/ZeusInc/Catalogo/Documents"
                                            FlashManager-DeletePaths="~/ZeusInc/Catalogo/Media"
                                            FlashManager-MaxUploadFileSize="10240000"
                                            FlashManager-ViewPaths="~/ZeusInc/Catalogo/Media"
                                            FlashManager-UploadPaths="~/ZeusInc/Catalogo/Media"
                                            ImageManager-DeletePaths="~/ZeusInc/Catalogo/Images"
                                            ImageManager-ViewPaths="~/ZeusInc/Catalogo/Images"
                                            ImageManager-MaxUploadFileSize="10240000"
                                            ImageManager-SearchPatterns="*.gif, *.png, *.jpg, *.jpe, *.jpeg"
                                            ImageManager-UploadPaths="~/ZeusInc/Catalogo/Images"
                                            ImageManager-ViewMode="Grid"
                                            MediaManager-DeletePaths="~/ZeusInc/Catalogo/Media"
                                            MediaManager-MaxUploadFileSize="10240000"
                                            MediaManager-SearchPatterns="*.asf, *.asx, *.wma, *.wmv, *.avi, *.wav, *.mpeg, *.mpg, *.mpe, *.mov, *.mp3, *.m3u, *.mid, *.midi, *.rm, *.rma"
                                            MediaManager-ViewPaths="~/ZeusInc/Catalogo/Media"
                                            MediaManager-UploadPaths="~/ZeusInc/Catalogo/Media"
                                            TemplateManager-SearchPatterns="*.html,*.htm"
                                            ContentAreaMode="iframe"
                                            OnClientCommandExecuted="editorCommandExecuted"
                                            OnClientModeChange="OnClientModeChange"
                                            Content='<%# Bind("Descrizione1") %>'
                                            ToolsFile="~/Zeus/Catalogo/RadEditor1.xml"
                                            LocalizationPath="~/App_GlobalResources"
                                            AllowScripts="true" RenderMode="Classic" ToolbarMode="Default" EnableViewState="False"
                                            Width="700px" Height="500px">
                                            <CssFiles>
                                                <telerik:EditorCssFile Value="~/asset/css/ZeusTypeFoundry.css" />
                                            </CssFiles>
                                        </telerik:RadEditor>
                                        <%--<radE:RadEditor StripAbsoluteAnchorPaths="true"  Language="it-IT"   id="RadEditor2" runat="server" converttagstolower="True" converttoxhtml="False"
                                            copycsstoformatblocktool="False" deletedocumentspaths="~/ZeusInc/Catalogo/Documents"
                                            deleteflashpaths="~/ZeusInc/Catalogo/Media" deleteimagespaths="~/ZeusInc/Catalogo/Images"
                                            deletemediapaths="~/ZeusInc/Catalogo/Media" documentsfilters="*.*" documentspaths="~/ZeusInc/Catalogo/Documents"
                                            enableclientserialize="True" enablecontextmenus="True" enabledocking="False"
                                            enableenhancededit="True" enablehtmlindentation="True" enableserversiderendering="True"
                                            enabletab="True" flashpaths="~/ZeusInc/Catalogo/Media" height="300px" html='<%# Bind("Descrizione1", "{0}") %>'
                                            imagesfilters="*.gif, *.png, *.jpg, *.jpe, *.jpeg" imagespaths="~/ZeusInc/Catalogo/Images"
                                            mediafilters="*.asf, *.asx, *.wma, *.wmv, *.avi, *.wav, *.mpeg, *.mpg, *.mpe, *.mov, *.mp3, *.m3u, *.mid, *.midi, *.rm, *.rma"
                                            mediapaths="~/ZeusInc/Catalogo/Media" onclientcancel="" onclientcommandexecuted=""
                                            onclientcommandexecuting="" onclientinit="" onclientload="" onclientmodechange=""
                                            allowscripts="true" onclientsubmit="" passsessiondata="True" renderastextarea="False"
                                            templatefilters="*.html,*.htm" toolbarmode="Default" toolswidth="" uploaddocumentspaths="~/ZeusInc/Catalogo/Documents"
                                            uploadflashpaths="~/ZeusInc/Catalogo/Media" uploadimagespaths="~/ZeusInc/Catalogo/Images"
                                            uploadmediapaths="~/ZeusInc/Catalogo/Media" usefixedtoolbar="False" width="700px"
                                             CssFiles="~/asset/css/ZeusTypeFoundry.css,~/SiteCss/ZeusSnippets.css" toolsfile="~/Zeus/Catalogo/RadEditor1.xml"
                                            enableviewstate="false">
                                        </radE:RadEditor>--%>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_Descrizione2" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="ZML_Descrizione2" runat="server" Text="Descrizione 2"></asp:Label>
                            </div>
                            <table>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="Label15" runat="server" SkinID="FieldDescription" Text="Label">Descrizione</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <telerik:RadEditor
                                            Language="it-IT" ID="RadEditor1" runat="server"
                                            DocumentManager-DeletePaths="~/ZeusInc/Catalogo/Documents"
                                            DocumentManager-SearchPatterns="*.*"
                                            DocumentManager-ViewPaths="~/ZeusInc/Catalogo/Documents"
                                            DocumentManager-MaxUploadFileSize="52428800"
                                            DocumentManager-UploadPaths="~/ZeusInc/Catalogo/Documents"
                                            FlashManager-DeletePaths="~/ZeusInc/Catalogo/Media"
                                            FlashManager-MaxUploadFileSize="10240000"
                                            FlashManager-ViewPaths="~/ZeusInc/Catalogo/Media"
                                            FlashManager-UploadPaths="~/ZeusInc/Catalogo/Media"
                                            ImageManager-DeletePaths="~/ZeusInc/Catalogo/Images"
                                            ImageManager-ViewPaths="~/ZeusInc/Catalogo/Images"
                                            ImageManager-MaxUploadFileSize="10240000"
                                            ImageManager-SearchPatterns="*.gif, *.png, *.jpg, *.jpe, *.jpeg"
                                            ImageManager-UploadPaths="~/ZeusInc/Catalogo/Images"
                                            ImageManager-ViewMode="Grid"
                                            MediaManager-DeletePaths="~/ZeusInc/Catalogo/Media"
                                            MediaManager-MaxUploadFileSize="10240000"
                                            MediaManager-SearchPatterns="*.asf, *.asx, *.wma, *.wmv, *.avi, *.wav, *.mpeg, *.mpg, *.mpe, *.mov, *.mp3, *.m3u, *.mid, *.midi, *.rm, *.rma"
                                            MediaManager-ViewPaths="~/ZeusInc/Catalogo/Media"
                                            MediaManager-UploadPaths="~/ZeusInc/Catalogo/Media"
                                            TemplateManager-SearchPatterns="*.html,*.htm"
                                            ContentAreaMode="iframe"
                                            OnClientCommandExecuted="editorCommandExecuted"
                                            OnClientModeChange="OnClientModeChange"
                                            Content='<%# Bind("Descrizione2") %>'
                                            ToolsFile="~/Zeus/Catalogo/RadEditor2.xml"
                                            LocalizationPath="~/App_GlobalResources"
                                            AllowScripts="true" RenderMode="Classic" ToolbarMode="Default" EnableViewState="False"
                                            Width="700px" Height="500px">
                                            <CssFiles>
                                                <telerik:EditorCssFile Value="~/asset/css/ZeusTypeFoundry.css" />
                                            </CssFiles>
                                        </telerik:RadEditor>
                                        <%--<radE:RadEditor StripAbsoluteAnchorPaths="true"  Language="it-IT"   id="RadEditor1" runat="server" converttagstolower="True" converttoxhtml="False"
                                            copycsstoformatblocktool="False" deletedocumentspaths="~/ZeusInc/Catalogo/Documents"
                                            deleteflashpaths="~/ZeusInc/Catalogo/Media" deleteimagespaths="~/ZeusInc/Catalogo/Images"
                                            deletemediapaths="~/ZeusInc/Catalogo/Media" documentsfilters="*.*" documentspaths="~/ZeusInc/Catalogo/Documents"
                                            enableclientserialize="True" enablecontextmenus="True" enabledocking="False"
                                            enableenhancededit="True" enablehtmlindentation="True" enableserversiderendering="True"
                                            enabletab="True" flashpaths="~/ZeusInc/Catalogo/Media" height="300px" html='<%# Bind("Descrizione2", "{0}") %>'
                                            imagesfilters="*.gif, *.png, *.jpg, *.jpe, *.jpeg" imagespaths="~/ZeusInc/Catalogo/Images"
                                            mediafilters="*.asf, *.asx, *.wma, *.wmv, *.avi, *.wav, *.mpeg, *.mpg, *.mpe, *.mov, *.mp3, *.m3u, *.mid, *.midi, *.rm, *.rma"
                                            mediapaths="~/ZeusInc/Catalogo/Media" onclientcancel="" onclientcommandexecuted=""
                                            onclientcommandexecuting="" onclientinit="" onclientload="" onclientmodechange=""
                                            allowscripts="true" onclientsubmit="" passsessiondata="True" renderastextarea="False"
                                            templatefilters="*.html,*.htm" toolbarmode="Default" toolswidth="" uploaddocumentspaths="~/ZeusInc/Catalogo/Documents"
                                            uploadflashpaths="~/ZeusInc/Catalogo/Media" uploadimagespaths="~/ZeusInc/Catalogo/Images"
                                            uploadmediapaths="~/ZeusInc/Catalogo/Media" usefixedtoolbar="False" width="700px"
                                             CssFiles="~/asset/css/ZeusTypeFoundry.css,~/SiteCss/ZeusSnippets.css" toolsfile="~/Zeus/Catalogo/RadEditor2.xml"
                                            enableviewstate="false" backcolor="white">
                                        </radE:RadEditor>--%>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                </div>
                <div id="Immagini">
                    <asp:Panel ID="ZMCF_BoxImmagini" runat="server">
                        <asp:Panel ID="ZMCF_Immagine1" runat="server">
                            <dlc:ImageRaider ID="ImageRaider1" runat="server" ZeusIdModuloIndice="1" ZeusLangCode="ITA"
                                BindPzFromDB="true" IsEditMode="true" DefaultEditBetaImage='<%# Eval("Immagine1") %>'
                                DefaultEditGammaImage='<%# Eval("Immagine2") %>' 
                                ImageAlt='<%# Eval("Immagine12Alt") %>' OnDataBinding="ImageRaider_DataBinding" />
                            <asp:HiddenField ID="FileNameBetaHiddenField" runat="server" Value='<%# Bind("Immagine1") %>' />
                            <asp:HiddenField ID="FileNameGammaHiddenField" runat="server" Value='<%# Bind("Immagine2") %>' />
                            <asp:HiddenField ID="Immagine12AltHiddenField" runat="server" Value='<%# Bind("Immagine12Alt") %>' />
                            <asp:CustomValidator ID="FileUploadCustomValidator" runat="server" OnServerValidate="FileUploadCustomValidator_ServerValidate"
                                Display="Dynamic" SkinID="ZSSM_Validazione01"
                                Visible="false"></asp:CustomValidator>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Immagine2" runat="server">
                            <dlc:ImageRaider ID="ImageRaider2" runat="server" ZeusIdModuloIndice="2" ZeusLangCode="ITA"
                                BindPzFromDB="true" IsEditMode="true" DefaultEditBetaImage='<%# Eval("Immagine3") %>'
                                DefaultEditGammaImage='<%# Eval("Immagine4") %>' 
                                ImageAlt='<%# Eval("Immagine34Alt") %>' OnDataBinding="ImageRaider_DataBinding" />
                            <asp:HiddenField ID="FileNameBeta2HiddenField" runat="server" Value='<%# Bind("Immagine3") %>' />
                            <asp:HiddenField ID="FileNameGamma2HiddenField" runat="server" Value='<%# Bind("Immagine4") %>' />
                            <asp:HiddenField ID="Immagine34AltHiddenField" runat="server" Value='<%# Bind("Immagine34Alt") %>' />
                            <asp:CustomValidator ID="CustomValidator2" runat="server" OnServerValidate="FileUploadCustomValidator_ServerValidate"
                                Display="Dynamic" SkinID="ZSSM_Validazione01"
                                Visible="false"></asp:CustomValidator>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Immagine3" runat="server">
                            <dlc:ImageRaider ID="ImageRaider3" runat="server" ZeusIdModuloIndice="3" ZeusLangCode="ITA"
                                BindPzFromDB="true" IsEditMode="true" DefaultEditBetaImage='<%# Eval("Immagine5") %>'
                                DefaultEditGammaImage='<%# Eval("Immagine6") %>' 
                                ImageAlt='<%# Eval("Immagine56Alt") %>' OnDataBinding="ImageRaider_DataBinding" />
                            <asp:HiddenField ID="FileNameBeta3HiddenField" runat="server" Value='<%# Bind("Immagine5") %>' />
                            <asp:HiddenField ID="FileNameGamma3HiddenField" runat="server" Value='<%# Bind("Immagine6") %>' />
                            <asp:HiddenField ID="Immagine56AltHiddenField" runat="server" Value='<%# Bind("Immagine56Alt") %>' />
                            <asp:CustomValidator ID="CustomValidator3" runat="server" OnServerValidate="FileUploadCustomValidator_ServerValidate"
                                Display="Dynamic" SkinID="ZSSM_Validazione01"
                                Visible="false"></asp:CustomValidator>
                        </asp:Panel>
                    </asp:Panel>
                </div>
                <div id="Listino">
                    <asp:Panel ID="ZMCF_BoxDisponibilita" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="Label4" runat="server">Disponibilit� e caratteristiche</asp:Label>
                            </div>
                            <table>
                                <asp:Panel ID="ZMCF_Quantita" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="QuantitaLabel" SkinID="FieldDescription" runat="server" Text="Quantit� *"></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:Label ID="Label14" runat="server" SkinID="FieldValue">Disponibile</asp:Label>
                                            <asp:TextBox ID="QtaDisponibileTextBox" runat="server" Columns="10" MaxLength="9"
                                                Text='<%# Bind("QtaDisponibile") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                                runat="server" ControlToValidate="QtaDisponibileTextBox" SkinID="ZSSM_Validazione01"
                                                Display="Dynamic" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                                            <asp:RangeValidator ID="RangeValidator4" runat="server"
                                                ControlToValidate="QtaDisponibileTextBox" SkinID="ZSSM_Validazione01" Display="Dynamic"
                                                ErrorMessage="Richiesto numero intero" MaximumValue="9999999999999" MinimumValue="0"
                                                Type="Currency"></asp:RangeValidator>
                                            <img src="../SiteImg/Spc.gif" width="10" />
                                            <asp:Label ID="Label16" runat="server" SkinID="FieldValue">in arrivo</asp:Label>
                                            <asp:TextBox ID="QtaArrivoTextBox" runat="server" Columns="10" MaxLength="9" Text='<%# Bind("QtaArrivo") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13"
                                                runat="server" ControlToValidate="QtaArrivoTextBox" SkinID="ZSSM_Validazione01"
                                                Display="Dynamic" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                                            <asp:RangeValidator ID="RangeValidator5" runat="server"
                                                ControlToValidate="QtaArrivoTextBox" SkinID="ZSSM_Validazione01" Display="Dynamic"
                                                ErrorMessage="Richiesto numero intero" MaximumValue="9999999999999" MinimumValue="0"
                                                Type="Currency"></asp:RangeValidator>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="ZMCF_Peso" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="Label5" SkinID="FieldDescription" runat="server" Text="Peso *"></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:TextBox ID="PesoTextBox" runat="server" Columns="10" MaxLength="9" Text='<%# Bind("Peso") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14"
                                                runat="server" ControlToValidate="PesoTextBox" SkinID="ZSSM_Validazione01" Display="Dynamic"
                                                ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator SkinID="ZSSM_Validazione01" ID="RegularExpressionValidator7"
                                                runat="server" ErrorMessage="Inserire un valore decimale con massimo due cifre dopo la virgola"
                                                ValidationExpression="^\s*\d+(\,\d{1,2})?\s*$" ControlToValidate="PesoTextBox"
                                                Display="Dynamic"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="ZMCF_UnitaMisura" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="Label10" SkinID="FieldDescription" runat="server" Text="Unit� di misura"></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:TextBox ID="UnitaMisuraTextBox" runat="server" Text='<%# Bind("UnitaMisura") %>'></asp:TextBox>
                                        </td>
                                    </tr>
                                </asp:Panel>
                            </table>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_BoxListini" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="ZML_BoxListini" runat="server">Listino</asp:Label>
                            </div>
                            <table>
                                <asp:Panel ID="ZMCF_Listino1" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="ZML_Listino1" SkinID="FieldDescription" runat="server" Text="Listino1 EUR *"></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:TextBox ID="Listino1TextBox" runat="server" Columns="10" MaxLength="9" Text='<%# Bind("Listino1") %>'></asp:TextBox>
                                            <asp:Label ID="Prezzo1Label" runat="server" SkinID="FieldValue" Text='<%# Eval("Valuta") %>'></asp:Label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5"
                                                runat="server" ControlToValidate="Listino1TextBox" SkinID="ZSSM_Validazione01"
                                                Display="Dynamic" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                                            <asp:RangeValidator ID="RangeValidator1" runat="server"
                                                ControlToValidate="Listino1TextBox" SkinID="ZSSM_Validazione01" Display="Dynamic"
                                                ErrorMessage="Prezzo non valido" MaximumValue="9999999999999" MinimumValue="0"
                                                Type="Currency"></asp:RangeValidator>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="ZMCF_Listino2" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="ZML_Listino2" runat="server" SkinID="FieldDescription" Text="Listino2 EUR *"></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:TextBox ID="Listino2TextBox" runat="server" Columns="10" MaxLength="9" Text='<%# Bind("Listino2") %>'></asp:TextBox>
                                            <asp:Label ID="Prezzo2Label" runat="server" SkinID="FieldValue" Text='<%# Eval("Valuta") %>'></asp:Label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6"
                                                runat="server" ControlToValidate="Listino2TextBox" SkinID="ZSSM_Validazione01"
                                                Display="Dynamic" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                                            <asp:RangeValidator ID="RangeValidator2" runat="server"
                                                ControlToValidate="Listino2TextBox" SkinID="ZSSM_Validazione01" Display="Dynamic"
                                                ErrorMessage="Prezzo non valido" MaximumValue="99999999999" MinimumValue="0"
                                                Type="Currency"></asp:RangeValidator>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="ZMCF_Listino3" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="ZML_Listino3" runat="server" SkinID="FieldDescription" Text="Listino3 EUR *"></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:TextBox ID="Listino3TextBox" runat="server" Columns="10" MaxLength="9" Text='<%# Bind("Listino3") %>'></asp:TextBox>
                                            <asp:Label ID="Prezzo3Label" runat="server" SkinID="FieldValue" Text='<%# Eval("Valuta") %>'></asp:Label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7"
                                                runat="server" ControlToValidate="Listino3TextBox" SkinID="ZSSM_Validazione01"
                                                Display="Dynamic" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                                            <asp:RangeValidator ID="RangeValidator3" runat="server"
                                                ControlToValidate="Listino3TextBox" SkinID="ZSSM_Validazione01" Display="Dynamic"
                                                ErrorMessage="Prezzo non valido" MaximumValue="9999999999" MinimumValue="0" Type="Currency"></asp:RangeValidator>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="ZMCF_Listino4" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="ZML_Listino4" runat="server" SkinID="FieldDescription" Text="Listino4 EUR"></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:TextBox ID="Listino4TextBox" runat="server" Columns="100" MaxLength="200" Text='<%# Bind("Listino4") %>'></asp:TextBox>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="ZMCF_Listino5" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="ZML_Listino5" runat="server" SkinID="FieldDescription" Text="Listino5 EUR"></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:TextBox ID="Listino5TextBox" runat="server" Columns="100" MaxLength="200" Text='<%# Bind("Listino5") %>'></asp:TextBox>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="ZMCF_IvaPercent" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="Label11" SkinID="FieldDescription" runat="server" Text="Iva% *"></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:TextBox ID="ZCMD_IvaPercentDefault" runat="server" Text='<%# Bind("IvaPercent") %>'
                                                Columns="5"></asp:TextBox>
                                            <asp:RegularExpressionValidator SkinID="ZSSM_Validazione01" ID="RegularExpressionValidator45"
                                                runat="server" ErrorMessage="Inserire un valore decimale con massimo due cifre dopo la virgola"
                                                ValidationExpression="^\s*\d+(\,\d{1,2})?\s*$" ControlToValidate="ZCMD_IvaPercentDefault"
                                                Display="Dynamic"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator15"
                                                runat="server" ControlToValidate="ZCMD_IvaPercentDefault" SkinID="ZSSM_Validazione01"
                                                Display="Dynamic" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                </asp:Panel>
                            </table>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_Allegato1" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="ZML_TitoloAllegato1" runat="server" Text="Allegato"></asp:Label>
                            </div>
                            <table>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_DescrizioneAllegato1" runat="server" SkinID="FieldDescription"
                                            Text="Descrizione"></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:TextBox ID="Allegato1_DescTextBox" runat="server" Text='<%# Bind("Allegato1_Desc", "{0}") %>'
                                            Columns="100" MaxLength="40"></asp:TextBox>
                                        <asp:CustomValidator ID="CustomValidator1" runat="server"
                                            ErrorMessage="Obbligatorio" ClientValidationFunction="CheckDescriptionLength"
                                            Display="Dynamic" SkinID="ZSSM_Validazione01"></asp:CustomValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_FileAllegato1" runat="server" SkinID="FieldDescription" Text="File"></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:RadioButton ID="RequiredFile" runat="server" GroupName="AllegatoSelettore" OnCheckedChanged="RadioButton1_CheckedChanged" />
                                        <dlc:UploadRaider ID="Allegato_UploadRaider1" runat="server" SavePath="/ZeusInc/Catalogo/Allegati/"
                                            FileTypeRange="*.*;" MaxSizeKb="100 MB" />
                                        <asp:HiddenField ID="Allegato1_File"
                                            runat="server" Value='<%# Bind("Allegato1_File", "{0}") %>' />
                                        <asp:HyperLink runat="server" ID="FileUploadLink" NavigateUrl='<%# Eval("Allegato1_File", "{0}") %>'
                                            OnDataBinding="FileUploadLink_DataBinding" SkinID="FieldValue">[FileUploadLink]</asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_UrlAllegato1" runat="server" SkinID="FieldDescription" Text="Url"></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:RadioButton ID="RequiredURL" runat="server" GroupName="AllegatoSelettore" OnCheckedChanged="RadioButton2_CheckedChanged" />
                                        <asp:TextBox ID="Allegato1_UrlEsternoTextBox" runat="server" Text='<%# Bind("Allegato1_Url", "{0}") %>'
                                            Columns="100" MaxLength="80"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="Url_Validator"
                                            runat="server" ControlToValidate="Allegato1_UrlEsternoTextBox" Display="Dynamic"
                                            ErrorMessage="Inserire un percorso completo e corretto" ValidationExpression="^(ht|f)tp(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&%\$#_]*)?$"
                                            SkinID="ZSSM_Validazione01">Inserire un percorso completo e corretto</asp:RegularExpressionValidator>
                                        <asp:CustomValidator ID="CustomValidator4" runat="server"
                                            ErrorMessage="Obbligatorio" ClientValidationFunction="CheckUrlLength"
                                            Display="Dynamic" SkinID="ZSSM_Validazione01"></asp:CustomValidator>
                                        <asp:HiddenField ID="Allegato1_UrlHiddenField"
                                            runat="server" Value='<%# Bind("Allegato1_Url", "{0}") %>' />
                                        <asp:LinkButton runat="server" ID="UrlLinkButton" PostBackUrl='<%# Eval("Allegato1_Url", "{0}") %>'
                                            Text="Apri 1" OnDataBinding="UrlLinkButton_DataBinding" SkinID="ZSSM_Button01"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_NessunoAllegato1" runat="server" SkinID="FieldDescription" Text="Nessuno"></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:RadioButton ID="SelettoreNull" runat="server" GroupName="AllegatoSelettore"
                                            OnCheckedChanged="SelettoreNull_CheckedChanged" />
                                        <asp:CustomValidator ID="RadioButton_Validator" runat="server"
                                            ErrorMessage="Obbligatorio" OnServerValidate="Check_RadioButton_Validate" Display="Dynamic"
                                            SkinID="ZSSM_Validazione01"></asp:CustomValidator>
                                        <asp:HiddenField ID="Allegato_Peso" runat="server" Value='<%# Bind("Allegato1_Peso", "{0}") %>' />
                                        <asp:HiddenField ID="Allegato_Selettore" runat="server" Value='<%# Bind("Allegato1_Selettore", "{0}") %>'
                                            OnDataBinding="Allegato_Selettore_DataBinding" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_Allegato2" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="ZML_TitoloAllegato2" runat="server" Text="Allegato"></asp:Label>
                            </div>
                            <table>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_DescrizioneAllegato2" runat="server" SkinID="FieldDescription"
                                            Text="Descrizione"></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:TextBox ID="Allegato1_DescTextBox2" runat="server" Text='<%# Bind("Allegato2_Desc", "{0}") %>'
                                            Columns="100" MaxLength="40"></asp:TextBox>
                                        <asp:CustomValidator ID="CustomValidator5" runat="server"
                                            ErrorMessage="Obbligatorio" ClientValidationFunction="CheckDescriptionLength2"
                                            Display="Dynamic" SkinID="ZSSM_Validazione01"></asp:CustomValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_FileAllegato2" runat="server" SkinID="FieldDescription" Text="File"></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:RadioButton ID="RequiredFile2" runat="server" GroupName="AllegatoSelettore2"
                                            OnCheckedChanged="RadioButton1_CheckedChanged2" />

                                        <dlc:UploadRaider ID="Allegato_UploadRaider2" runat="server" SavePath="/ZeusInc/Catalogo/Allegati/"
                                            FileTypeRange="*.*;" MaxSizeKb="100 MB" />
                                        <asp:HiddenField ID="Allegato1_File2"
                                            runat="server" Value='<%# Bind("Allegato2_File", "{0}") %>' />

                                        <asp:HyperLink runat="server" ID="FileUploadLink2" NavigateUrl='<%# Eval("Allegato2_File", "{0}") %>'
                                            OnDataBinding="FileUploadLink_DataBinding2" SkinID="FieldValue">[FileUploadLink]</asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_UrlAllegato2" runat="server" SkinID="FieldDescription" Text="Url"></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:RadioButton ID="RequiredURL2" runat="server" GroupName="AllegatoSelettore2"
                                            OnCheckedChanged="RadioButton2_CheckedChanged2" />
                                        <asp:TextBox ID="Allegato1_UrlEsternoTextBox2" runat="server" Text='<%# Bind("Allegato2_Url", "{0}") %>'
                                            Columns="100" MaxLength="80"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="Url_Validator2"
                                            runat="server" ControlToValidate="Allegato1_UrlEsternoTextBox2" Display="Dynamic"
                                            ErrorMessage="Inserire un percorso completo e corretto" ValidationExpression="^(ht|f)tp(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&%\$#_]*)?$"
                                            SkinID="ZSSM_Validazione01">Inserire un percorso completo e corretto</asp:RegularExpressionValidator>
                                        <asp:CustomValidator ID="CustomValidator7" runat="server"
                                            ErrorMessage="Obbligatorio" ClientValidationFunction="CheckUrlLength2"
                                            Display="Dynamic" SkinID="ZSSM_Validazione01"></asp:CustomValidator>
                                        <asp:HiddenField ID="Allegato2_UrlHiddenField"
                                            runat="server" Value='<%# Bind("Allegato1_Url", "{0}") %>' />
                                        <asp:LinkButton runat="server" ID="UrlLinkButton2" PostBackUrl='<%# Eval("Allegato2_Url", "{0}") %>'
                                            Text="Apri 2" OnDataBinding="UrlLinkButton_DataBinding2" SkinID="ZSSM_Button01"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_NessunoAllegato2" runat="server" SkinID="FieldDescription" Text="Nessuno"></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:RadioButton ID="SelettoreNull2" runat="server" GroupName="AllegatoSelettore2"
                                            OnCheckedChanged="SelettoreNull_CheckedChanged2" />
                                        <asp:CustomValidator ID="RadioButton_Validator2" runat="server"
                                            ErrorMessage="Obbligatorio" OnServerValidate="Check_RadioButton_Validate2" Display="Dynamic"
                                            SkinID="ZSSM_Validazione01"></asp:CustomValidator>
                                        <asp:HiddenField ID="Allegato_Peso2" runat="server" Value='<%# Bind("Allegato2_Peso", "{0}") %>' />
                                        <asp:HiddenField ID="Allegato_Selettore2" runat="server" Value='<%# Bind("Allegato2_Selettore", "{0}") %>'
                                            OnDataBinding="Allegato_Selettore_DataBinding2" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                </div>
                <div id="MultiCategoria">
                    <asp:Panel ID="ZMCF_Categoria2" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="ZML_Categoria2" runat="server" Text="Sezioni associate"></asp:Label>
                            </div>
                            <dlc:CategorieMultiple ID="CategorieMultiple1" runat="server" COLUMN_ID="ID2Articolo"
                                COLUMN_IDCAT="ID2Categoria2" TABLE="tbxCatalogo_MultiCategorie" MODE="EDT" />
                        </div>
                    </asp:Panel>
                </div>
            </div>
            <asp:HiddenField ID="RecordEdtUserHiddenField" runat="server" OnDataBinding="RecordEdtUserHidden_DataBinding"
                Value='<%# Bind("RecordEdtUser", "{0}") %>' />
            <asp:HiddenField ID="RecordEdtDateHiddenField" runat="server" OnDataBinding="RecordEdtDateHidden_DataBinding"
                Value='<%# Bind("RecordEdtDate", "{0}") %>' />
            <%--
            <asp:Panel ID="ArticoliButtonPanel" runat="server">
                <uc1:ArticoliLangButton ID="ArticoliLangButton1" runat="server" />
            </asp:Panel>--%>
            <div align="center">
                <dlc:mySummaryValidation ID="mySummaryValidation1" runat="server" SkinID="ZSSM_Validazione01" />
                <br />
                <asp:LinkButton ID="InsertButton" runat="server" SkinID="ZSSM_Button01" CausesValidation="True"
                    CommandName="Update" Text="Salva dati" OnClick="InsertButton_Click"></asp:LinkButton>
            </div>
            <div style="padding: 10px 0 0 0; margin: 0;">
                <dlc:zeusdatatracking ID="Zeusdatatracking1" runat="server" />
            </div>
            <asp:HiddenField ID="zdtRecordNewUser" runat="server" Value='<%# Eval("RecordNewUser") %>' />
            <asp:HiddenField ID="zdtRecordNewDate" runat="server" Value='<%# Eval("RecordNewDate") %>' />
            <asp:HiddenField ID="zdtRecordEdtUser" runat="server" Value='<%# Eval("RecordEdtUser") %>' />
            <asp:HiddenField ID="zdtRecordEdtDate" runat="server" Value='<%# Eval("RecordEdtDate") %>' />
        </EditItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="dsArticoloNew" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SELECT  ID1Articolo,Articolo,ID2Brand, Codice, ID2Categoria1, ZeusTags, DescBreve1, Descrizione1, 
        Descrizione2, [Immagine1], [Immagine2], Immagine12Alt, [Immagine3], [Immagine4], Immagine34Alt, [Immagine5], [Immagine6], Immagine56Alt, ImmagineThumb_Selettore,QtaDisponibile,QtaArrivo, 
        Listino1,Listino2, Listino3, Listino4, Listino5, Valuta, Allegato1_Desc, Allegato1_File, 
        Allegato1_Url, Allegato1_Peso, Allegato1_Selettore, Allegato2_Desc,Allegato2_File, Allegato2_Url, 
        Allegato2_Peso, Allegato2_Selettore,PW_Area1, PWI_Area1, PWF_Area1, PW_Area2, PWI_Area2, 
        PWF_Area2,PW_Area3, PWI_Area3, PWF_Area3, RecordNewUser, RecordNewDate, RecordEdtUser, 
        RecordEdtDate,UrlRewrite ,[TagDescription],[TagKeywords],[PW_Zeus1],[CodiceParent],[IvaPercent],[Peso],[UnitaMisura],
        [SpecificaInt1],[SpecificaText1],[SpecificaText2] 
        FROM [tbCatalogo] 
        WHERE ID1Articolo=@ID1Articolo  AND ZeusIsAlive=1  AND ZeusIdModulo=@ZeusIdModulo AND ZeusLangCode=@ZeusLangCode"
        UpdateCommand=" SET DATEFORMAT dmy; UPDATE [tbCatalogo] SET [Articolo] = @Articolo, 
        [ID2Brand] = @ID2Brand, [Codice] = @Codice, [ID2Categoria1] = @ID2Categoria1, 
        [ZeusTags] = @ZeusTags, [DescBreve1] = @DescBreve1, [Descrizione1] = @Descrizione1, 
        [Descrizione2] = @Descrizione2, [Immagine1] = @Immagine1, [Immagine2] = @Immagine2, Immagine12Alt = @Immagine12Alt, [Immagine3] = @Immagine3, [Immagine4] = @Immagine4, Immagine34Alt = @Immagine34Alt, [Immagine5] = @Immagine5, [Immagine6] = @Immagine6, Immagine56Alt = @Immagine56Alt,
        [QtaDisponibile]=@QtaDisponibile,[QtaArrivo]=@QtaArrivo, [Listino1] = @Listino1, 
        [Listino2] = @Listino2, [Listino3] = @Listino3, [Listino4] = @Listino4, 
        [Listino5] = @Listino5, [Allegato1_Desc] = @Allegato1_Desc, [Allegato1_File] = @Allegato1_File, 
        [Allegato1_Url] = @Allegato1_Url,[Allegato1_Peso]=@Allegato1_Peso, 
        [Allegato1_Selettore] = @Allegato1_Selettore, [Allegato2_Desc] = @Allegato2_Desc, 
        [Allegato2_File] = @Allegato2_File, [Allegato2_Url] = @Allegato2_Url,
        [Allegato2_Peso]=@Allegato2_Peso, [Allegato2_Selettore] = @Allegato2_Selettore,  
        [PW_Area1] = @PW_Area1, [PWI_Area1] = @PWI_Area1, [PWF_Area1] = @PWF_Area1, 
        [PW_Area2] = @PW_Area2, [PWI_Area2] = @PWI_Area2, [PWF_Area2] = @PWF_Area2,
        [PW_Area3] = @PW_Area3, [PWI_Area3] = @PWI_Area3, [PWF_Area3] = @PWF_Area3,  
        [RecordEdtUser] = @RecordEdtUser, [RecordEdtDate] = @RecordEdtDate, 
        ImmagineThumb_Selettore=@ImmagineThumb_Selettore ,[TagDescription]=@TagDescription,[TagKeywords]=@TagKeywords
        ,PW_Zeus1=@PW_Zeus1,[CodiceParent]=@CodiceParent,[IvaPercent]=@IvaPercent,[Peso]=@Peso,[UnitaMisura]=@UnitaMisura ,
        UrlRewrite=@UrlRewrite,[SpecificaInt1]=@SpecificaInt1,
        [SpecificaText1]=@SpecificaText1,[SpecificaText2]=@SpecificaText2 
        WHERE ID1Articolo=@ID1Articolo"
        OnUpdated="dsArticoloNew_Updated">
        <SelectParameters>
            <asp:QueryStringParameter Name="ID1Articolo" Type="Int32" QueryStringField="XRI" />
            <asp:QueryStringParameter Name="ZeusIdModulo" QueryStringField="ZIM" Type="String" />
            <asp:QueryStringParameter Name="ZeusLangCode" QueryStringField="Lang" Type="String" DefaultValue="ITA" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Articolo" Type="String" />
            <asp:Parameter Name="ID2Brand" Type="Int32" />
            <asp:Parameter Name="Codice" Type="String" />
            <asp:Parameter Name="ID2Categoria1" Type="Int32" />
            <asp:Parameter Name="ZeusTags" Type="String" />
            <asp:Parameter Name="SpecificaInt1" Type="Int32" />
            <asp:Parameter Name="SpecificaText1" Type="String" />
            <asp:Parameter Name="SpecificaText2" Type="String" />
            <asp:Parameter Name="DescBreve1" Type="String" />
            <asp:Parameter Name="Descrizione1" Type="String" />
            <asp:Parameter Name="Descrizione2" Type="String" />
            <asp:Parameter Name="TagDescription" Type="String" />
            <asp:Parameter Name="TagKeywords" Type="String" />
            <asp:Parameter Name="Immagine1" Type="String" />
            <asp:Parameter Name="Immagine2" Type="String" />
            <asp:Parameter Name="Immagine12Alt" Type="String" />
            <asp:Parameter Name="Immagine3" Type="String" />
            <asp:Parameter Name="Immagine4" Type="String" />
            <asp:Parameter Name="Immagine34Alt" Type="String" />
            <asp:Parameter Name="Immagine5" Type="String" />
            <asp:Parameter Name="Immagine6" Type="String" />
            <asp:Parameter Name="Immagine56Alt" Type="String" />
            <asp:Parameter Name="QtaDisponibile" Type="Int32" DefaultValue="0" />
            <asp:Parameter Name="QtaArrivo" Type="Int32" DefaultValue="0" />
            <asp:Parameter Name="PW_Zeus1" Type="Boolean" />
            <asp:Parameter Name="Listino1" Type="Decimal" />
            <asp:Parameter Name="Listino2" Type="Decimal" />
            <asp:Parameter Name="Listino3" Type="Decimal" />
            <asp:Parameter Name="Listino4" Type="String" />
            <asp:Parameter Name="Listino5" Type="String" />
            <asp:Parameter Name="UrlRewrite" Type="String" />
            <asp:Parameter Name="Allegato1_Desc" Type="String" />
            <asp:Parameter Name="Allegato1_File" Type="String" />
            <asp:Parameter Name="Allegato1_Url" Type="String" />
            <asp:Parameter Name="Allegato1_Peso" Type="Decimal" />
            <asp:Parameter Name="Allegato1_Selettore" Type="String" />
            <asp:Parameter Name="Allegato2_Desc" Type="String" />
            <asp:Parameter Name="Allegato2_File" Type="String" />
            <asp:Parameter Name="Allegato2_Url" Type="String" />
            <asp:Parameter Name="Allegato2_Peso" Type="Decimal" />
            <asp:Parameter Name="Allegato2_Selettore" Type="String" />
            <asp:Parameter Name="PW_Area1" Type="Boolean" />
            <asp:Parameter Name="PWI_Area1" Type="DateTime" />
            <asp:Parameter Name="PWF_Area1" Type="DateTime" />
            <asp:Parameter Name="PW_Area2" Type="Boolean" />
            <asp:Parameter Name="PWI_Area2" Type="DateTime" />
            <asp:Parameter Name="PWF_Area2" Type="DateTime" />
            <asp:Parameter Name="PW_Area3" Type="Boolean" />
            <asp:Parameter Name="PWI_Area3" Type="DateTime" />
            <asp:Parameter Name="PWF_Area3" Type="DateTime" />
            <asp:Parameter Name="RecordEdtUser" />
            <asp:Parameter Name="RecordEdtDate" Type="DateTime" />
            <asp:Parameter Name="ImmagineThumb_Selettore" Type="String" />
            <asp:Parameter Name="CodiceParent" Type="String" />
            <asp:Parameter Name="IvaPercent" Type="Decimal" />
            <asp:Parameter Name="Peso" Type="Decimal" />
            <asp:Parameter Name="UnitaMisura" Type="String" />
            <asp:QueryStringParameter Name="ID1Articolo" Type="Int32" QueryStringField="XRI" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
