﻿using ASP;
using System;
using System.Data.SqlClient;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per Articoli_Dtl
/// </summary>
public partial class Catalogo_Articoli_Dtl : System.Web.UI.Page 
{      
    static string TitoloPagina = string.Empty;
    delinea myDelinea = new delinea();

    protected void Page_Load(object sender, EventArgs e)
    {
        if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
            || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI"))
            || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM")))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        ZIM.Value = Server.HtmlEncode(Request.QueryString["ZIM"]);
        XRI.Value = Server.HtmlEncode(Request.QueryString["XRI"]);

        ZID.Value = getZeusId();

        PhotoGallery_Associa PhotoGallery_Associa1 = (PhotoGallery_Associa)FormView1.FindControl("PhotoGallery_Associa1");
        PhotoGallery_Associa1.TitoloControlID = "ArticoloDataLabel";
        PhotoGallery_Associa1.ZeusIdModuloContenuto = ZIM.Value;
        PhotoGallery_Associa1.IDContenuto = XRI.Value;

        if (!ReadXML(ZIM.Value, GetLang()))
            Response.Write("~/Zeus/System/Message.aspx?1");

        ReadXML_Localization(ZIM.Value, GetLang());

        try
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartUpScript", CopyToClipBoard(), true);
            DropDownList ID2BrandDropDownList = (DropDownList)FormView1.FindControl("ID2BrandDropDownList");
            Label ID2BrandLabel = (Label)FormView1.FindControl("ID2BrandLabel");

            ID2BrandLabel.Text = ID2BrandDropDownList.SelectedItem.Text;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

        HyperLink NuovaAssociazioneAccessorioHyperLink = (HyperLink)FormView1.FindControl("NuovaAssociazioneAccessorioHyperLink");
        HyperLink NuovaAssociazioneArticoloHyperLink = (HyperLink)FormView1.FindControl("NuovaAssociazioneArticoloHyperLink");

        NuovaAssociazioneArticoloHyperLink.NavigateUrl = "Accessori_New.aspx?parent=" + XRI.Value;
        NuovaAssociazioneAccessorioHyperLink.NavigateUrl = "Accessori_New.aspx?child=" + XRI.Value;
    }

    private string getZeusId()
    {

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT [ZeusId]";
                sqlQuery += " FROM [tbCatalogo] ";
                sqlQuery += " WHERE [ID1Articolo]=@ID1Articolo";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID1Articolo", System.Data.SqlDbType.Int);
                command.Parameters["@ID1Articolo"].Value = Server.HtmlEncode(XRI.Value.ToString());

                SqlDataReader reader = command.ExecuteReader();

                string myReturn = "";

                while (reader.Read())
                {
                    myReturn = reader["ZeusId"].ToString();
                }



                reader.Close();
                return myReturn;
            }
            catch (Exception p)
            {

                Response.Write(p.ToString());
                return "";
            }
        }//fine Using

    }//fine getZeusId

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";
            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Catalogo.xml"));

            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {

                            if (childNode.Name.Equals("ZML_TitoloPagina_Dtl"))
                            {
                                TitleField.Value = childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                                else
                                {
                                    myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name + "Label");

                                    if (myLabel != null)
                                        myLabel.Text = childNode.InnerText;
                                }
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            string myXPathEach = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_Catalogo.xml"));

            if (!CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_Edt").InnerText))
                Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223599");

            Panel myPanel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPathEach);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            if (childNode.Name.IndexOf("ZMCF_") > -1)
                            {
                                myPanel = (Panel)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myPanel != null)
                                    myPanel.Visible = Convert.ToBoolean(childNode.InnerText);
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }

            SetQueryOfID2CategoriaDropDownList(mydoc.SelectSingleNode(myXPath + "ZMCD_ZIMCategoria1").InnerText
                , GetLang()
                , mydoc.SelectSingleNode(myXPath + "ZMCD_Cat1Liv").InnerText);

            CategorieMultiple CategorieMultiple1 = (CategorieMultiple)FormView1.FindControl("CategorieMultiple1");

            if (CategorieMultiple1 != null)
            {
                CategorieMultiple1.ZeusIdModulo = mydoc.SelectSingleNode(myXPath + "ZMCD_ZIMCategoria2").InnerText;
                CategorieMultiple1.ZeusLangCode = GetLang();
                CategorieMultiple1.RepeatColumns = mydoc.SelectSingleNode(myXPath + "ZMCD_ColCategoria2").InnerText;
                CategorieMultiple1.PAGE_ID = XRI.Value;
                CategorieMultiple1.PrintCategorie();
            }

            Label ZMCD_TagDescription = (Label)FormView1.FindControl("ZMCD_TagDescription");
            ZMCD_TagDescription.Text = mydoc.SelectSingleNode(myXPath + "ZMCD_TagDescription").InnerText;

            Label ZMCD_TagKeywords = (Label)FormView1.FindControl("ZMCD_TagKeywords");
            ZMCD_TagKeywords.Text = mydoc.SelectSingleNode(myXPath + "ZMCD_TagKeywords").InnerText;

            Panel PZV_BoxAccessori = (Panel)FormView1.FindControl("PZV_BoxAccessori");
            PZV_BoxAccessori.Visible = Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "PZV_BoxAccessori").InnerText);

            Panel PZV_BoxArticoliPadre = (Panel)FormView1.FindControl("PZV_BoxArticoliPadre");
            PZV_BoxArticoliPadre.Visible = Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "PZV_BoxArticoliPadre").InnerText);

            Label ZMCD_UrlSection = (Label)FormView1.FindControl("ZMCD_UrlSection");

            if (ZMCD_UrlSection != null)
                ZMCD_UrlSection.Text = mydoc.SelectSingleNode(myXPath + "ZMCD_UrlSection").InnerText;

            PhotoGallery_Associa PhotoGallery_Associa1 = (PhotoGallery_Associa)FormView1.FindControl("PhotoGallery_Associa1");

            if (PhotoGallery_Associa1 != null)
                PhotoGallery_Associa1.ZIMGalleryAssociation =
                    mydoc.SelectSingleNode(myXPath + "ZMCD_ZIMGalleryAssociation").InnerText;

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    private bool CheckPermit(string AllRoles)
    {
        bool IsPermit = false;

        try
        {
            if (AllRoles.Length == 0)
                return false;

            char[] delimiter = { ',' };

            foreach (string roles in Roles.GetRolesForUser(Membership.GetUser().UserName))
            {
                string[] PressRoles = AllRoles.Split(delimiter);

                for (int i = 0; i < PressRoles.Length; ++i)
                {
                    if (PressRoles[i].Trim().Equals(roles.Trim()))
                    {
                        IsPermit = true;
                        break;
                    }
                }
            }
        }
        catch (Exception)
        { }

        return IsPermit;
    }

    private string CopyToClipBoard()
    {
        string myJavascript = " function ClipBoard() ";
        myJavascript += " { ";
        myJavascript += " var holdtext =document.getElementById('";
        myJavascript += FormView1.FindControl("CopyHiddenField").ClientID;
        myJavascript += "'); ";
        myJavascript += " var copytext = document.getElementById('";
        myJavascript += FormView1.FindControl("ZMCD_UrlSection").ClientID;
        myJavascript += "'); ";
        myJavascript += " var copytext2 = document.getElementById('";
        myJavascript += FormView1.FindControl("ID1Pagina").ClientID;
        myJavascript += "'); ";
        myJavascript += " var copytext3 = document.getElementById('";
        myJavascript += FormView1.FindControl("UrlPageDetailLabel").ClientID;
        myJavascript += "'); ";
        myJavascript += " holdtext.value = copytext.innerHTML + copytext2.innerHTML + copytext3.innerHTML; ";
        myJavascript += " var Copied  = holdtext.createTextRange();";
        myJavascript += " Copied.select();";
        myJavascript += " Copied.execCommand('copy',false,null);";
        myJavascript += " return; ";
        myJavascript += " } ";

        return myJavascript;
    }

    private string GetLang()
    {
        try
        {
            delinea myDelinea = new delinea();
            string Lang = "ITA";

            if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
            {
                if (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang"))
                    Lang = Request.QueryString["Lang"];
            }

            return Lang;
        }
        catch (Exception)
        {
            return "ITA";
        }
    }

    private bool SetQueryOfID2CategoriaDropDownList(string ZeusIdModulo, string ZeusLangCode, string PZV_Cat1Liv)
    {
        try
        {
            SqlDataSource dsCategoria = (SqlDataSource)FormView1.FindControl("dsCategoria");
            DropDownList ID2CategoriaDropDownList = (DropDownList)FormView1.FindControl("ID2CategoriaDropDownList");
            HiddenField ID2CategoriaHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaHiddenField");

            switch (PZV_Cat1Liv)
            {

                case "123":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv1Liv2Liv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "Catliv1Liv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;


                case "23":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv2Liv3]";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;

                default:
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;
            }

            if ((ID2CategoriaHiddenField.Value.Length > 0)
                && (!ID2CategoriaHiddenField.Value.Equals("0")))
            {
                ID2CategoriaDropDownList.SelectedIndex = 
                    ID2CategoriaDropDownList.Items.IndexOf(ID2CategoriaDropDownList.Items.FindByValue(ID2CategoriaHiddenField.Value));
                Label CategoriaLabel = (Label)FormView1.FindControl("CategoriaLabel");
                CategoriaLabel.Text = ID2CategoriaDropDownList.SelectedItem.Text;
            }

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    protected void PWI_Area(object sender, EventArgs e)
    {
        DateTime Data = new DateTime();
        Data = DateTime.Now;
        Label DataCreazione = (Label)sender;
        if (DataCreazione.Text == string.Empty)
        {
            DataCreazione.Text = Data.ToString("dd/MM/yyyy");
        }
    }

    protected void PWF_Area(object sender, EventArgs e)
    {
        Label DataFinale = (Label)sender;
        if (DataFinale.Text == string.Empty)
        {
            DataFinale.Text = "31/12/2040";
        }
    }

    protected void ImageRaider_DataBinding(object sender, EventArgs e)
    {
        ImageRaider ImageRaider1 = (ImageRaider)sender;
        ImageRaider1.SetDefaultEditBetaImage();
        ImageRaider1.SetDefaultEditGammaImage();
    }

    protected void FileUploadCustomValidator_ServerValidate(object sender, ServerValidateEventArgs args)
    {
        try
        {
            ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");
            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");

            if (ImageRaider1.isValid())
                InsertButton.CommandName = "Update";
            else
                InsertButton.CommandName = string.Empty;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    protected void UrlLinkButton_DataBinding(object sender, EventArgs e)
    {
        LinkButton UrlLinkButton = (LinkButton)sender;
        if (UrlLinkButton.PostBackUrl.Length == 0)
            UrlLinkButton.Visible = false;
        else
        {
            UrlLinkButton.OnClientClick = "window.open('" + UrlLinkButton.PostBackUrl + "'); return false;";
        }
    }

    protected void SelettoreAllegatoHiddenfield_DataBinding(object sender, EventArgs e)
    {
        HiddenField SelettoreAllegatoHiddenfield = (HiddenField)sender;
        HyperLink FileAllegatoHyperLink = (HyperLink)FormView1.FindControl("FileAllegatoHyperLink");
        LinkButton UrlLinkButton = (LinkButton)FormView1.FindControl("UrlLinkButton");

        switch (SelettoreAllegatoHiddenfield.Value)
        {
            case "FIL":
                FileAllegatoHyperLink.Visible = true;
                UrlLinkButton.Visible = false;
                break;

            case "LNK":
                FileAllegatoHyperLink.Visible = false;
                UrlLinkButton.Visible = true;
                break;
        }
    }

    protected void UrlLinkButton2_DataBinding(object sender, EventArgs e)
    {
        LinkButton UrlLinkButton2 = (LinkButton)sender;
        if (UrlLinkButton2.PostBackUrl.Length == 0)
            UrlLinkButton2.Visible = false;
        else
        {
            UrlLinkButton2.OnClientClick = "window.open('" + UrlLinkButton2.PostBackUrl + "'); return false;";
        }
    }

    protected void SelettoreAllegatoHiddenfield2_DataBinding(object sender, EventArgs e)
    {
        HiddenField SelettoreAllegatoHiddenfield2 = (HiddenField)sender;
        HyperLink FileAllegatoHyperLink2 = (HyperLink)FormView1.FindControl("FileAllegatoHyperLink2");
        LinkButton UrlLinkButton2 = (LinkButton)FormView1.FindControl("UrlLinkButton2");

        switch (SelettoreAllegatoHiddenfield2.Value)
        {
            case "FIL":
                FileAllegatoHyperLink2.Visible = true;
                UrlLinkButton2.Visible = false;
                break;

            case "LNK":
                FileAllegatoHyperLink2.Visible = false;
                UrlLinkButton2.Visible = true;
                break;
        }
    }

    protected void ModificaButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Zeus/Catalogo/Articoli_Edt.aspx?XRI=" + XRI.Value
            + "&ZIM=" + Server.HtmlEncode(Request.QueryString["ZIM"]) + "&Lang=" + GetLang());
    }

    private void DuplicaArticolo(string ID1Articolo)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        try
        {
            using (SqlConnection connection = new SqlConnection(strConnessione))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = @"INSERT INTO [tbCatalogo]
                                ([Articolo]
                                ,[ID2Brand]
                                ,[Codice]
                                ,[ID2Categoria1]
                                ,[UrlRewrite]
                                ,[ZeusTags]
                                ,[DescBreve1]
                                ,[Descrizione1]
                                ,[Descrizione2]
                                ,[Immagine1]
                                ,[Immagine2]
                                ,[ImmagineThumb_Selettore]
                                ,[SpecificaInt1]
                                ,[SpecificaText1]
                                ,[SpecificaText2]
                                ,[QtaDisponibile]
                                ,[QtaArrivo]
                                ,[Listino1]
                                ,[Listino2]
                                ,[Listino3]
                                ,[Listino4]
                                ,[Listino5]
                                ,[IvaPercent]
                                ,[Valuta]
                                ,[CodiceParent]
                                ,[Peso]
                                ,[UnitaMisura]
                                ,[TagDescription]
                                ,[TagKeywords]
                                ,[Allegato1_Desc]
                                ,[Allegato1_File]
                                ,[Allegato1_Url]
                                ,[Allegato1_Peso]
                                ,[Allegato1_Selettore]
                                ,[Allegato2_Desc]
                                ,[Allegato2_File]
                                ,[Allegato2_Url]
                                ,[Allegato2_Peso]
                                ,[Allegato2_Selettore]
                                ,[PW_Area1]
                                ,[PWI_Area1]
                                ,[PWF_Area1]
                                ,[PW_Area2]
                                ,[PWI_Area2]
                                ,[PWF_Area2]
                                ,[PW_Area3]
                                ,[PWI_Area3]
                                ,[PWF_Area3]
                                ,[PW_Zeus1]
                                ,[RecordNewUser]
                                ,[RecordNewDate]
                                ,[RecordEdtUser]
                                ,[RecordEdtDate]
                                ,[ZeusJolly1]
                                ,[ZeusJolly2]
                                ,[ZeusIdModulo]
                                ,[ZeusId]
                                ,[ZeusLangCode]
                                ,[ZeusIsAlive])
                            SELECT 'COPIA DI ' + [Articolo] as [Articolo]
                                ,[ID2Brand]
                                ,'_'+[Codice]as[Codice]
                                ,[ID2Categoria1]
                                ,[UrlRewrite]
                                ,[ZeusTags]
                                ,[DescBreve1]
                                ,[Descrizione1]
                                ,[Descrizione2]
                                ,[Immagine1]
                                ,[Immagine2]
                                ,[ImmagineThumb_Selettore]
                                ,[SpecificaInt1]
                                ,[SpecificaText1]
                                ,[SpecificaText2]
                                ,[QtaDisponibile]
                                ,[QtaArrivo]
                                ,[Listino1]
                                ,[Listino2]
                                ,[Listino3]
                                ,[Listino4]
                                ,[Listino5]
                                ,[IvaPercent]
                                ,[Valuta]
                                ,[CodiceParent]
                                ,[Peso]
                                ,[UnitaMisura]
                                ,[TagDescription]
                                ,[TagKeywords]
                                ,[Allegato1_Desc]
                                ,[Allegato1_File]
                                ,[Allegato1_Url]
                                ,[Allegato1_Peso]
                                ,[Allegato1_Selettore]
                                ,[Allegato2_Desc]
                                ,[Allegato2_File]
                                ,[Allegato2_Url]
                                ,[Allegato2_Peso]
                                ,[Allegato2_Selettore]
                                ,[PW_Area1]
                                ,[PWI_Area1]
                                ,[PWF_Area1]
                                ,[PW_Area2]
                                ,[PWI_Area2]
                                ,[PWF_Area2]
                                ,[PW_Area3]
                                ,[PWI_Area3]
                                ,[PWF_Area3]
                                ,[PW_Zeus1]
                                ,[RecordNewUser]
                                ,[RecordNewDate]
                                ,[RecordEdtUser]
                                ,[RecordEdtDate]
                                ,[ZeusJolly1]
                                ,[ZeusJolly2]
                                ,[ZeusIdModulo]
                                ,[ZeusId]
                                ,[ZeusLangCode]
                                ,[ZeusIsAlive]
                            FROM[tbCatalogo]
                            WHERE [ID1Articolo]=@ID1Articolo; SELECT SCOPE_IDENTITY();";

                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID1Articolo", System.Data.SqlDbType.Int);
                command.Parameters["@ID1Articolo"].Value = ID1Articolo;

                string NewID1Articolo = Convert.ToInt32(command.ExecuteScalar()).ToString();

                sqlQuery = @"INSERT INTO [tbCatalogo_Accessori]
                                ([ID2Articolo]
                                ,[ID2Accessorio])
                            SELECT " + NewID1Articolo + @"
                                ,[ID2Accessorio]
                            FROM [tbCatalogo_Accessori]
                            WHERE ID2Articolo=@ID2Articolo";

                command.CommandText = sqlQuery;
                command.Parameters.Clear();
                command.Parameters.Add("@ID2Articolo", System.Data.SqlDbType.Int);
                command.Parameters["@ID2Articolo"].Value = ID1Articolo;
                command.ExecuteNonQuery();

                sqlQuery = @"INSERT INTO [tbCatalogo_Photo]
                                ([ID2Articolo]
                                ,[Immagine1]
                                ,[Immagine2]
                                ,[Ordinamento]
                                ,[Pubblica]
                                ,[DataCreazione])
                            SELECT " + NewID1Articolo + @"
                                ,[Immagine1]
                                ,[Immagine2]
                                ,[Ordinamento]
                                ,[Pubblica]
                                ,[DataCreazione]
                            FROM [tbCatalogo_Photo]
                            WHERE [ID2Articolo]=@ID2Articolo";
                command.CommandText = sqlQuery;
                command.Parameters.Clear();
                command.Parameters.Add("@ID2Articolo", System.Data.SqlDbType.Int);
                command.Parameters["@ID2Articolo"].Value = ID1Articolo;
                command.ExecuteNonQuery();

                Response.Redirect("~/Zeus/Catalogo/Articoli_Edt.aspx?XRI=" + NewID1Articolo
                    + "&ZIM=" + ZIM.Value + "&Lang=" + GetLang());
            }
        }
        catch (Exception p)
        {
            System.Web.HttpContext.Current.Response.Write(p.ToString());
        }
    }

    protected void DuplicaLinkButton_Click(object sender, EventArgs e)
    {
        DuplicaArticolo(XRI.Value);        
    }

    protected void dsArticoloNew_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {
        if ((e.Exception != null)
            || (e.AffectedRows <= 0))
            Response.Redirect("~/Zeus/System/Message.aspx?SelErr");
    }

    protected void DeleteLinkButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Zeus/Catalogo/Articoli_Del.aspx?XRI="
            + Server.HtmlEncode(Request.QueryString["XRI"]) + "&Lang=" + GetLang()
            + "&ZIM=" + ZIM.Value);

    }//fine DeleteLinkButton_Click

    protected void PhotoGallery_Simple1_Init(object sender, EventArgs e)
    {
        Photo_Lst myGallery = (Photo_Lst)sender;
        myGallery.Lang = GetLang();
        myGallery.ZeusIdModulo = ZIM.Value.ToString();
        myGallery.XRI = XRI.Value.ToString();
        myGallery.ZID = ZID.Value.ToString();
    }
}