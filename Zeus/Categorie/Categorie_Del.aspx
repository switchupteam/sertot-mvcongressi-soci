﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="Categorie_Del.aspx.cs"
    Inherits="Categorie_Del"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <asp:HiddenField ID="ID1Categoria" runat="server" />
    <div class="BlockBox">
        <div class="BlockBoxHeader">
            <asp:Label ID="Label6" runat="server">Elminazione categoria</asp:Label>
        </div>
        <table>
            <tr>
                <td>
                    <asp:Label ID="Label1" SkinID="FieldDescription" Font-Bold="true" runat="server">Stai per eliminare la categoria: </asp:Label>
                    <asp:Label ID="TitoloLabel" SkinID="FieldDescription" runat="server"></asp:Label>
                    <asp:Label ID="InfoLabel" SkinID="Validazione01" runat="server"></asp:Label>
                    <asp:Label ID="Info2Label" runat="server" SkinID="Validazione01">
                    <br />Si consiglia di rimuovere semplicemente la pubblicazione di questo record senza eliminarlo fisicamente, al fine di evitare problemi ai contenuti collegati.
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox ID="ConfermaCheckBox" runat="server" Checked="false" />
                    <asp:Label ID="Label11" SkinID="FieldDescription" runat="server">Conferma eliminazione</asp:Label>
                    <asp:Label ID="ErrorLabel" SkinID="Validazione01" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <div align="center">
        <asp:LinkButton ID="EseguiLinkButton" runat="server" Text="Elimina" SkinID="ZSSM_Button01"
            OnClick="EseguiButton_Click"></asp:LinkButton></div>
</asp:Content>
