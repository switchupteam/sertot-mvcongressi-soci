﻿using ASP;
using System;
using System.Data.SqlClient;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per Categorie_Dtl
/// </summary>
public partial class Categorie_Dtl : System.Web.UI.Page
{
    static string TitoloPagina = "Modifica categoria";

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            delinea myDelinea = new delinea();
            TitleField.Value = TitoloPagina;

            if ((!myDelinea.AntiSQLInjection(Request.QueryString))
            || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM"))
            || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
            || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI"))
            || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI1")))
                Response.Redirect("~/Zeus/System/Message.aspx?0");

            XRI.Value = Server.HtmlEncode(Request.QueryString["XRI"]);
            XRI1.Value = Server.HtmlEncode(Request.QueryString["XRI1"]);
            ZIM.Value = Server.HtmlEncode(Request.QueryString["ZIM"]);

            if (!ReadXML(ZIM.Value, GetLang()))
                Response.Write("~/Zeus/System/Message.aspx?1");

            ReadXML_Localization(ZIM.Value, GetLang());

            string myJavascript = CopyToClipBoard();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartUpScript", myJavascript, true);
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine Page_Load

    private string CopyToClipBoard()
    {
        string myJavascript = " function ClipBoard() ";
        myJavascript += " { ";
        myJavascript += " var holdtext =document.getElementById('";
        myJavascript += FormView1.FindControl("CopyHiddenField").ClientID;
        myJavascript += "'); ";
        myJavascript += " var copytext = document.getElementById('";
        myJavascript += FormView1.FindControl("ZMCD_UrlSection").ClientID;
        myJavascript += "'); ";
        myJavascript += " var copytext2 = document.getElementById('";
        myJavascript += FormView1.FindControl("ID1Pagina").ClientID;
        myJavascript += "'); ";
        myJavascript += " var copytext3 = document.getElementById('";
        myJavascript += FormView1.FindControl("UrlPageDetailLabel").ClientID;
        myJavascript += "'); ";
        myJavascript += " holdtext.value = copytext.innerHTML + copytext2.innerHTML + copytext3.innerHTML; ";
        myJavascript += " var Copied  = holdtext.createTextRange();";
        myJavascript += " Copied.select();";
        myJavascript += " Copied.execCommand('copy',false,null);";
        myJavascript += " return; ";
        myJavascript += " } ";

        return myJavascript;

    }//fine CopyToClipBoard

    private string GetLang()
    {
        string Lang = "ITA";

        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;

    }//fine GetLang

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Categorie.xml"));


            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {

                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            if (childNode.Name.Equals("ZML_TitoloPagina_Dtl"))
                            {
                                TitleField.Value = childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }

    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            string myXPathEach = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode;

            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_Categorie.xml"));

            if (!CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_Dtl").InnerText))
                Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223599");

            Panel myPanel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPathEach);

            for (int i = 0; i < nodelist.Count; ++i)
            {

                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {

                            if (childNode.Name.IndexOf("ZMCF_") > -1)
                            {
                                myPanel = (Panel)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myPanel != null)
                                    myPanel.Visible = Convert.ToBoolean(childNode.InnerText);
                            }

                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }

            Panel ZMCF_BoxUrlRewrite = (Panel)FormView1.FindControl("ZMCF_BoxUrlRewrite");

            if (ZMCF_BoxUrlRewrite != null)
                ZMCF_BoxUrlRewrite.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxUrlRewrite").InnerText);

            Panel ZMCF_TitoloBrowser = (Panel)FormView1.FindControl("ZMCF_TitoloBrowser");

            if (ZMCF_TitoloBrowser != null)
                ZMCF_TitoloBrowser.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_TitoloBrowser").InnerText);

            Panel ZMCF_DescBreve1 = (Panel)FormView1.FindControl("ZMCF_DescBreve1");

            if (ZMCF_DescBreve1 != null)
                ZMCF_DescBreve1.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_DescBreve1").InnerText);

            Panel ZMCF_Descrizione1 = (Panel)FormView1.FindControl("ZMCF_Descrizione1");

            if (ZMCF_Descrizione1 != null)
                ZMCF_Descrizione1.Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Descrizione1").InnerText);

            Panel ZMCF_Descrizione2 = (Panel)FormView1.FindControl("ZMCF_Descrizione2");

            if (ZMCF_Descrizione2 != null)
                ZMCF_Descrizione2.Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Descrizione2").InnerText);

            Panel ZMCF_BoxColore = (Panel)FormView1.FindControl("ZMCF_BoxColore");

            if (ZMCF_BoxColore != null)
                ZMCF_BoxColore.Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxColore").InnerText);

            Panel ZMCF_BoxImmagine1 = (Panel)FormView1.FindControl("ZMCF_BoxImmagine1");

            if (ZMCF_BoxImmagine1 != null)
                ZMCF_BoxImmagine1.Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxImmagine1").InnerText);

            Panel ZMCF_BoxImmagine2 = (Panel)FormView1.FindControl("ZMCF_BoxImmagine2");

            if (ZMCF_BoxImmagine2 != null)
                ZMCF_BoxImmagine2.Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxImmagine2").InnerText);

            Panel ZMCF_BoxPlanner = (Panel)FormView1.FindControl("ZMCF_BoxPlanner");

            if (ZMCF_BoxPlanner != null)
                ZMCF_BoxPlanner.Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxPlanner").InnerText);

            Panel ZMCF_PWArea1 = (Panel)FormView1.FindControl("ZMCF_PWArea1");

            if (ZMCF_PWArea1 != null)
                ZMCF_PWArea1.Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_PWArea1").InnerText);

            Panel ZMCF_PWArea2 = (Panel)FormView1.FindControl("ZMCF_PWArea2");

            if (ZMCF_PWArea2 != null)
                ZMCF_PWArea2.Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_PWArea2").InnerText);

            Panel ZMCF_BoxMetaTags = (Panel)FormView1.FindControl("ZMCF_BoxMetaTags");

            if (ZMCF_BoxMetaTags != null)
                ZMCF_BoxMetaTags.Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxMetaTags").InnerText);

            Panel ZMCF_TagMeta1 = (Panel)FormView1.FindControl("ZMCF_TagMeta1");

            if (ZMCF_TagMeta1 != null)
                ZMCF_TagMeta1.Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_TagMeta1").InnerText);

            Panel ZMCF_TagMeta2 = (Panel)FormView1.FindControl("ZMCF_TagMeta2");

            if (ZMCF_TagMeta2 != null)
                ZMCF_TagMeta2.Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_TagMeta2").InnerText);

            switch (XRI1.Value)
            {
                case "1":
                    if (!Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_PermitLevel1").InnerText))
                        Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223598");
                    else if (!Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_UrlRewriteLiv1").InnerText))
                        DisableUrlRewriting();
                    break;

                case "2":
                    if (!Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_PermitLevel2").InnerText))
                        Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223598");
                    else if (!Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_UrlRewriteLiv2").InnerText))
                        DisableUrlRewriting();
                    break;

                case "3":
                    if (!Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_PermitLevel3").InnerText))
                        Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223598");
                    else if (!Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_UrlRewriteLiv3").InnerText))
                        DisableUrlRewriting();
                    break;

            }

            Label ZMCD_UrlSection = (Label)FormView1.FindControl("ZMCD_UrlSection");

            if (ZMCD_UrlSection != null)
                ZMCD_UrlSection.Text = mydoc.SelectSingleNode(myXPath + "ZMCD_UrlSection").InnerText;

            Label ZMCD_UrlDomain = (Label)FormView1.FindControl("ZMCD_UrlDomain");

            if (ZMCD_UrlDomain != null)
                ZMCD_UrlDomain.Text = mydoc.SelectSingleNode(myXPath + "ZMCD_UrlDomain").InnerText;

            return true;
        }
        catch (Exception p)
        {

            Response.Write(p.ToString());
            return false;
        }

    }

    private bool CheckPermit(string AllRoles)
    {

        bool IsPermit = false;

        try
        {
            if (AllRoles.Length == 0)
                return false;

            char[] delimiter = { ',' };

            foreach (string roles in Roles.GetRolesForUser(Membership.GetUser().UserName))
            {
                string[] PressRoles = AllRoles.Split(delimiter);

                for (int i = 0; i < PressRoles.Length; ++i)
                {
                    if (PressRoles[i].Trim().Equals(roles.Trim()))
                    {
                        IsPermit = true;
                        break;
                    }
                }
            }
        }
        catch (Exception)
        { }

        return IsPermit;

    }

    private void DisableUrlRewriting()
    {
        Label ZMCD_UrlSection = (Label)FormView1.FindControl("ZMCD_UrlSection");

        try
        {
            HtmlInputButton CopiaInput = (HtmlInputButton)FormView1.FindControl("CopiaInput");
            Label ServerLabel = (Label)FormView1.FindControl("ServerLabel");

            if (CopiaInput != null)
                CopiaInput.Visible = false;

            if (ServerLabel != null)
                ServerLabel.Visible = false;
        }
        catch (Exception)
        { }

        ZMCD_UrlSection.Text = "Funzionalità non attiva su questo contenuto";
    }

    //##############################################################################################################
    //################################################ FILE UPLOAD #################################################
    //############################################################################################################## 

    protected void ImageRaider_DataBinding(object sender, EventArgs e)
    {
        ImageRaider ImageRaider1 = (ImageRaider)sender;
        ImageRaider1.SetDefaultEditBetaImage();
        ImageRaider1.SetDefaultEditGammaImage();

    }

    //##############################################################################################################
    //########################################### FINE FILE UPLOAD #################################################
    //##############################################################################################################

    protected void ColorLabel_DataBinding(object sender, EventArgs e)
    {

        Label ColorLabel = (Label)sender;
        HtmlControl ColoreDiv = (HtmlControl)FormView1.FindControl("ColoreDiv");
        ColoreDiv.Style["background-color"] = ColorLabel.Text;
        ColorLabel.Text = string.Empty;

    }

    protected void ID2CategoriaParentHiddenField_DataBinding(object sender, EventArgs e)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        HiddenField ID2CategoriaParentHiddenField = (HiddenField)sender;
        Label CategoriaInfoLabel = (Label)FormView1.FindControl("CategoriaInfoLabel");

        try
        {
            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();


                sqlQuery = "SELECT Categoria";
                sqlQuery += " FROM tbCategorie";
                sqlQuery += " WHERE ID1Categoria=@ID1Categoria";


                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID1Categoria", System.Data.SqlDbType.Int);
                command.Parameters["@ID1Categoria"].Value = ID2CategoriaParentHiddenField.Value;

                SqlDataReader reader = command.ExecuteReader();

                if (!reader.HasRows)
                {
                    reader.Close();
                    CategoriaInfoLabel.Text = "Radice";
                    return;
                }

                while (reader.Read())
                    if (reader["Categoria"] != DBNull.Value)
                        CategoriaInfoLabel.Text = reader["Categoria"].ToString();

                reader.Close();
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    protected void ModificaButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Zeus/Categorie/Categorie_Edt.aspx?XRI=" + Server.HtmlEncode(Request.QueryString["XRI"])
            + "&ZIM=" + Server.HtmlEncode(Request.QueryString["ZIM"])
            + "&Lang=" + GetLang()
            + "&XRI1=" + Server.HtmlEncode(Request.QueryString["XRI1"]));
    }

    protected void DeleteLinkButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Zeus/Categorie/Categorie_Del.aspx?XRI=" 
            + Server.HtmlEncode(Request.QueryString["XRI"]) + "&Lang=" + GetLang() 
            + "&ZIM=" + Server.HtmlEncode(Request.QueryString["ZIM"]));
    }

    protected void CategorieSqlDataSource_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {
        if ((e.Exception != null)
            || (e.AffectedRows <= 0))
            Response.Redirect("~/Zeus/System/Message.aspx?SelErr");
    }
}