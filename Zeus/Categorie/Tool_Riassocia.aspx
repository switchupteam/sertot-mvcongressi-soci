﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">
    
    static string TitoloPagina = "Riassociazione categorie";

    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;

        SetFocus(SostituisciLinkButton.UniqueID);

    }//fine Page_Load

    private void SetFocus(string ID)
    {
        Page.Form.DefaultButton = ID;
        Page.Form.DefaultFocus = ID;

    }//fine SetFocus

    protected void TipologiaDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList TipologiaDropDownList = (DropDownList)sender;
        OldCategoriaDropDownList.Items.Clear();
        OldCategoriaDropDownList.Items.Add(new ListItem("> Seleziona", "0"));
        NewCategoriaDropDownList.Items.Clear();
        NewCategoriaDropDownList.Items.Add(new ListItem("> Seleziona", "0"));

        if (!TipologiaDropDownList.SelectedValue.Equals("0"))
        {
            OldCategoriaSqlDataSource.SelectCommand = "SELECT [ID1Categoria],[CatLiv1Liv2Liv3] FROM [vwCategorie_All] WHERE [ZeusIdModulo]='" + TipologiaDropDownList.SelectedValue + "' AND ZeusLangCode='ITA' AND Livello=3";
            OldCategoriaDropDownList.DataSourceID = "OldCategoriaSqlDataSource";
            OldCategoriaDropDownList.DataBind();

            NewCategoriaSqlDataSource.SelectCommand = "SELECT [ID1Categoria],[CatLiv1Liv2Liv3] FROM [vwCategorie_All] WHERE [ZeusIdModulo]='" + TipologiaDropDownList.SelectedValue + "' AND ZeusLangCode='ITA' AND Livello=3";
            NewCategoriaDropDownList.DataSourceID = "NewCategoriaSqlDataSource";
            NewCategoriaDropDownList.DataBind();

        }

    }//fine TipologiaDropDownList_SelectedIndexChanged


    private bool SwitchCategory(string ID1Old, string ID1New)
    {

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        try
        {

            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();


                sqlQuery = "if exists (SELECT ID2Categoria1 FROM tbPublisher WHERE ID2Categoria1=" + ID1Old + ")";
                sqlQuery += " BEGIN ";
                sqlQuery += " UPDATE tbPublisher SET ID2Categoria1=" + ID1New + " WHERE ID2Categoria1=" + ID1Old;
                sqlQuery += " END ";
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();

                sqlQuery = "if exists (SELECT ID2Categoria1 FROM tbBusinessBook WHERE ID2Categoria1=" + ID1Old + ")";
                sqlQuery += " BEGIN ";
                sqlQuery += " UPDATE tbBusinessBook SET ID2Categoria1=" + ID1New + " WHERE ID2Categoria1=" + ID1Old;
                sqlQuery += " END ";
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();


                sqlQuery = "if exists (SELECT ID2Categoria2 FROM tbBusinessBook WHERE ID2Categoria2=" + ID1Old + ")";
                sqlQuery += " BEGIN ";
                sqlQuery += " UPDATE tbBusinessBook SET ID2Categoria2=" + ID1New + " WHERE ID2Categoria2=" + ID1Old;
                sqlQuery += " END ";
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();


                sqlQuery = "if exists (SELECT ID2Categoria3 FROM tbBusinessBook WHERE ID2Categoria3=" + ID1Old + ")";
                sqlQuery += " BEGIN ";
                sqlQuery += " UPDATE tbBusinessBook SET ID2Categoria3=" + ID1New + " WHERE ID2Categoria3=" + ID1Old;
                sqlQuery += " END ";
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();

                sqlQuery = "if exists (SELECT ID2Categoria FROM tbPressRoom WHERE ID2Categoria=" + ID1Old + ")";
                sqlQuery += " BEGIN ";
                sqlQuery += " UPDATE tbPressRoom SET ID2Categoria=" + ID1New + " WHERE ID2Categoria=" + ID1Old;
                sqlQuery += " END ";
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();

                sqlQuery = "if exists (SELECT ID2Categoria FROM tbPageDesigner WHERE ID2Categoria=" + ID1Old + ")";
                sqlQuery += " BEGIN ";
                sqlQuery += " UPDATE tbPageDesigner SET ID2Categoria=" + ID1New + " WHERE ID2Categoria=" + ID1Old;
                sqlQuery += " END ";
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();

                sqlQuery = "if exists (SELECT ID2Categoria1 FROM tbCatalogo WHERE ID2Categoria1=" + ID1Old + ")";
                sqlQuery += " BEGIN ";
                sqlQuery += " UPDATE tbCatalogo SET ID2Categoria1=" + ID1New + " WHERE ID2Categoria1=" + ID1Old;
                sqlQuery += " END ";
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();

                sqlQuery = "if exists (SELECT ID2Categoria FROM tbCommunicator WHERE ID2Categoria=" + ID1Old + ")";
                sqlQuery += " BEGIN ";
                sqlQuery += " UPDATE tbCommunicator SET ID2Categoria=" + ID1New + " WHERE ID2Categoria=" + ID1Old;
                sqlQuery += " END ";
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();

                sqlQuery = "if exists (SELECT ID2Categoria FROM tbPhotoGallery WHERE ID2Categoria=" + ID1Old + ")";
                sqlQuery += " BEGIN ";
                sqlQuery += " UPDATE tbPhotoGallery SET ID2Categoria=" + ID1New + " WHERE ID2Categoria=" + ID1Old;
                sqlQuery += " END ";
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();


                sqlQuery = "if exists (SELECT ID2Categoria1 FROM tbLocator WHERE ID2Categoria1=" + ID1Old + ")";
                sqlQuery += " BEGIN ";
                sqlQuery += " UPDATE tbLocator SET ID2Categoria1=" + ID1New + " WHERE ID2Categoria1=" + ID1Old;
                sqlQuery += " END ";
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();


                sqlQuery = "if exists (SELECT ID2Categoria2 FROM tbLocator WHERE ID2Categoria2=" + ID1Old + ")";
                sqlQuery += " BEGIN ";
                sqlQuery += " UPDATE tbLocator SET ID2Categoria2=" + ID1New + " WHERE ID2Categoria2=" + ID1Old;
                sqlQuery += " END ";
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();


                sqlQuery = "if exists (SELECT ID2Categoria4 FROM tbxBusinessBook_MultiCategorie WHERE ID2Categoria4=" + ID1Old + ")";
                sqlQuery += " BEGIN ";
                sqlQuery += " UPDATE tbxBusinessBook_MultiCategorie SET ID2Categoria4=" + ID1New + " WHERE ID2Categoria4=" + ID1Old;
                sqlQuery += " END ";
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();

                sqlQuery = "if exists (SELECT ID2Categoria2 FROM tbxPublisher_ContenutiCategorie WHERE ID2Categoria2=" + ID1Old + ")";
                sqlQuery += " BEGIN ";
                sqlQuery += " UPDATE tbxPublisher_ContenutiCategorie SET ID2Categoria2=" + ID1New + " WHERE ID2Categoria2=" + ID1Old;
                sqlQuery += " END ";
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();


                return true;

            }//fine Using
        }
        catch (Exception p)
        {

            //ErrorLabel.Visible = true;
            //ErrorLabel.Text = p.ToString();
            return false;
        }



    }//fine SwitchCategory



    protected void SostituisciLinkButton_Click(object sender, EventArgs e)
    {
        FinishLabel.Visible = false;
        
        if ((Page.IsValid)
            && (!OldCategoriaDropDownList.SelectedValue.Equals(NewCategoriaDropDownList.SelectedValue)))
        {
            ErrorLabel.Visible = false;
            SwitchCategory(OldCategoriaDropDownList.SelectedValue, NewCategoriaDropDownList.SelectedValue);
            FinishLabel.Visible = true;

        }//fine if
        else
        {
            ErrorLabel.Visible = true;
            ErrorLabel.Text = "ATTENZIONE: Impossibile sostituire due categorie identiche<br />";
        }

    }//fine SostituisciLinkButton
    
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <div class="BlockBox">
        <div class="BlockBoxHeader">
            <asp:Label ID="Dettaglio_contenuto" runat="server" Text="Riassociazione"></asp:Label></div>
        <table>
            <tr>
                <td class="BlockBoxDescription">
                    <asp:Label ID="Label3" SkinID="FieldDescription" runat="server" Text="Tipologia *"></asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:DropDownList ID="TipologiaDropDownList" runat="server" DataSourceID="TipologiaSqlDataSource"
                        AppendDataBoundItems="True" DataTextField="Sezione" AutoPostBack="true" DataValueField="ZeusIdModulo"
                        OnSelectedIndexChanged="TipologiaDropDownList_SelectedIndexChanged">
                        <asp:ListItem Text="> Seleziona" Value="0" Selected="True"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator" ErrorMessage="Obbligatorio"
                        runat="server" ControlToValidate="TipologiaDropDownList" SkinID="ZSSM_Validazione01"
                        Display="Dynamic" InitialValue="0">
                    </asp:RequiredFieldValidator>
                    <asp:SqlDataSource ID="TipologiaSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                        SelectCommand="SELECT [ZeusIdModulo],[Sezione] FROM [tbModuli] WHERE [Modulo]='Categorie' AND [PW_Zeus1]=1 AND ZeusLangCode='ITA'">
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td class="BlockBoxDescription">
                    <asp:Label ID="Label1" SkinID="FieldDescription" runat="server" Text="Vecchia categoria da sostituire *"></asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:DropDownList ID="OldCategoriaDropDownList" runat="server" DataSourceID="OldCategoriaSqlDataSource"
                        AppendDataBoundItems="True" DataTextField="CatLiv1Liv2Liv3" DataValueField="ID1Categoria">
                        <asp:ListItem Text="> Seleziona" Value="0" Selected="True"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Obbligatorio"
                        runat="server" ControlToValidate="OldCategoriaDropDownList" SkinID="ZSSM_Validazione01"
                        Display="Dynamic" InitialValue="0">
                    </asp:RequiredFieldValidator>
                    <asp:SqlDataSource ID="OldCategoriaSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>">
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td class="BlockBoxDescription">
                    <asp:Label ID="Label2" SkinID="FieldDescription" runat="server" Text="Nuova categoria sostitutiva *"></asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:DropDownList ID="NewCategoriaDropDownList" runat="server" DataSourceID="NewCategoriaSqlDataSource"
                        AppendDataBoundItems="True" DataTextField="CatLiv1Liv2Liv3" DataValueField="ID1Categoria">
                        <asp:ListItem Text="> Seleziona" Value="0" Selected="True"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Obbligatorio"
                        runat="server" ControlToValidate="NewCategoriaDropDownList" SkinID="ZSSM_Validazione01"
                        Display="Dynamic" InitialValue="0">
                    </asp:RequiredFieldValidator>
                    <asp:SqlDataSource ID="NewCategoriaSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>">
                    </asp:SqlDataSource>
                </td>
            </tr>
            <%--<tr>
                <td class="BlockBoxDescription">
                    <asp:Label ID="Label4" SkinID="FieldDescription" runat="server" Text="Moduli"></asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:CheckBoxList ID="ModuliCheckBoxList" runat="server" DataSourceID="ModuliSqlDataSource"
                        DataTextField="Sezione" DataValueField="ZeusIdModulo" RepeatColumns="3" RepeatDirection="Horizontal"
                        SkinID="FieldValue">
                    </asp:CheckBoxList>
                    <asp:SqlDataSource ID="ModuliSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                        SelectCommand="SELECT Sezione, ZeusIdModulo FROM tbModuli WHERE  (Modulo <> 'Categorie') AND (ZeusLangCode = 'ita')">
                    </asp:SqlDataSource>
                </td>
            </tr>--%>
        </table>
    </div>
    <div align="center">
        <asp:Label ID="ErrorLabel" runat="server" SkinID="Validazione01" Visible="false"></asp:Label>
        <asp:Label ID="FinishLabel" runat="server" SkinID="CorpoTesto" Text="Categorie riassociate con successo.<br />" Visible="false"></asp:Label>
        <asp:LinkButton ID="SostituisciLinkButton" runat="server" Text="Sostituisci" SkinID="ZSSM_Button01"
            OnClick="SostituisciLinkButton_Click"></asp:LinkButton>
    </div>
</asp:Content>
