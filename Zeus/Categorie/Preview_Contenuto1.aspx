﻿<%@ Page Language="C#"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();

        if (!myDelinea.AntiSQLInjection(Request.QueryString))
            Response.Redirect("~/Zeus/System/Message.aspx");
    }//fine Page_Load

    protected void tbCategorie_SqlDataSource_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.AffectedRows <= 0)
            Response.Redirect("~/Zeus/System/Message.aspx?1");
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MV Congressi Platform - Anteprima contenuto</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:FormView ID="FormView1" runat="server" DataSourceID="tbCategorie_SqlDataSource"
            CellPadding="0">
            <ItemTemplate>
                <asp:Label ID="Contenuto1Label" runat="server" Text='<%# Eval("Descrizione1") %>'></asp:Label>
            </ItemTemplate>
        </asp:FormView>
        <asp:SqlDataSource ID="tbCategorie_SqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
            SelectCommand="SELECT [Descrizione1] FROM [tbCategorie] WHERE ID1Categoria=@ID1Categoria"
            OnSelected="tbCategorie_SqlDataSource_Selected">
            <SelectParameters>
                <asp:QueryStringParameter Name="ID1Categoria" QueryStringField="XRI" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <br />
        <div align="center">
        <%--<input type=button value="Chiudi" onClick="javascript:window.close();return false" name="button">--%>
        <asp:LinkButton ID="CloseLinkButton" runat="server" OnClientClick="javascript:window.close();return false;" SkinID="ZSSM_Button01" Text="Chiudi"></asp:LinkButton>
        </div>
    </div>
    </form>
</body>
</html>
