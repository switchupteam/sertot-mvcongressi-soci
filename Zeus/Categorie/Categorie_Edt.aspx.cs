﻿using ASP;
using System;
using System.Collections;
using System.Data.SqlClient;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per Categorie_Edt
/// </summary>
public partial class Categorie_Edt : System.Web.UI.Page
{

    static string TitoloPagina = "Modifica categoria";

    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();
        TitleField.Value = TitoloPagina;

        if ((!myDelinea.AntiSQLInjection(Request.QueryString))
        || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM"))
        || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
        || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI"))
        || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI1")))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        XRI.Value = Server.HtmlEncode(Request.QueryString["XRI"]);
        XRI1.Value = Server.HtmlEncode(Request.QueryString["XRI1"]);
        ZIM.Value = Server.HtmlEncode(Request.QueryString["ZIM"]);

        if (!ReadXML(ZIM.Value, GetLang()))
            Response.Write("~/Zeus/System/Message.aspx?1");

        ReadXML_Localization(ZIM.Value, GetLang());

        try
        {
            string myJavascript = BothRequired1();
            myJavascript += " " + BothRequired2();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartUpScript", myJavascript, true);

            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");
            InsertButton.Attributes.Add("onclick", "Validate()");
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }


    private string BothRequired1()
    {

        string ClientID1 = FormView1.FindControl("TagMeta1ValueTextBox").ClientID;
        string ClientID2 = FormView1.FindControl("TagMeta1ContentTextBox").ClientID;

        string myJavascript = " function BothRequired1(sender ,args) ";
        myJavascript += " { ";
        myJavascript += " if(document.getElementById('" + ClientID1 + "').value.length>0) ";
        myJavascript += " if(document.getElementById('" + ClientID2 + "').value.length==0)";
        myJavascript += " { ";
        myJavascript += " args.IsValid=false; ";
        myJavascript += " return; ";
        myJavascript += " } ";

        myJavascript += " if(document.getElementById('" + ClientID2 + "').value.length>0) ";
        myJavascript += " if(document.getElementById('" + ClientID1 + "').value.length==0)";
        myJavascript += " { ";
        myJavascript += " args.IsValid=false; ";
        myJavascript += " return; ";
        myJavascript += " } ";
        myJavascript += " return; ";
        myJavascript += " } ";

        return myJavascript;

    }

    private string BothRequired2()
    {

        string ClientID1 = FormView1.FindControl("TagMeta2ValueTextBox").ClientID;
        string ClientID2 = FormView1.FindControl("TagMeta2ContentTextBox").ClientID;

        string myJavascript = " function BothRequired2(sender ,args) ";
        myJavascript += " { ";
        myJavascript += " if(document.getElementById('" + ClientID1 + "').value.length>0) ";
        myJavascript += " if(document.getElementById('" + ClientID2 + "').value.length==0)";
        myJavascript += " { ";
        myJavascript += " args.IsValid=false; ";
        myJavascript += " return; ";
        myJavascript += " } ";

        myJavascript += " if(document.getElementById('" + ClientID2 + "').value.length>0) ";
        myJavascript += " if(document.getElementById('" + ClientID1 + "').value.length==0)";
        myJavascript += " { ";
        myJavascript += " args.IsValid=false; ";
        myJavascript += " return; ";
        myJavascript += " } ";
        myJavascript += " return; ";
        myJavascript += " } ";

        return myJavascript;
    }

    private string GetLang()
    {
        string Lang = "ITA";

        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;

    }//fine GetLang



    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Categorie.xml"));


            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {

                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            if (childNode.Name.Equals("ZML_TitoloPagina_Edt"))
                            {
                                TitleField.Value = childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                            }

                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_Categorie.xml"));

            if (!CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_Edt").InnerText))
                Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223599");

            Panel ZMCF_TitoloBrowser = (Panel)FormView1.FindControl("ZMCF_TitoloBrowser");

            if (ZMCF_TitoloBrowser != null)
                ZMCF_TitoloBrowser.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_TitoloBrowser").InnerText);

            Panel ZMCF_BoxUrlRewrite = (Panel)FormView1.FindControl("ZMCF_BoxUrlRewrite");

            if (ZMCF_BoxUrlRewrite != null)
                ZMCF_BoxUrlRewrite.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxUrlRewrite").InnerText);

            Panel ZMCF_DescBreve1 = (Panel)FormView1.FindControl("ZMCF_DescBreve1");

            if (ZMCF_DescBreve1 != null)
                ZMCF_DescBreve1.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_DescBreve1").InnerText);

            Panel ZMCF_Descrizione1 = (Panel)FormView1.FindControl("ZMCF_Descrizione1");

            if (ZMCF_Descrizione1 != null)
                ZMCF_Descrizione1.Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Descrizione1").InnerText);

            Panel ZMCF_Descrizione2 = (Panel)FormView1.FindControl("ZMCF_Descrizione2");

            if (ZMCF_Descrizione2 != null)
                ZMCF_Descrizione2.Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Descrizione2").InnerText);

            Panel ZMCF_BoxColore = (Panel)FormView1.FindControl("ZMCF_BoxColore");

            if (ZMCF_BoxColore != null)
                ZMCF_BoxColore.Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxColore").InnerText);

            Panel ZMCF_BoxImmagine1 = (Panel)FormView1.FindControl("ZMCF_BoxImmagine1");

            if (ZMCF_BoxImmagine1 != null)
                ZMCF_BoxImmagine1.Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxImmagine1").InnerText);

            Panel ZMCF_BoxImmagine2 = (Panel)FormView1.FindControl("ZMCF_BoxImmagine2");

            if (ZMCF_BoxImmagine2 != null)
                ZMCF_BoxImmagine2.Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxImmagine2").InnerText);

            Panel ZMCF_BoxPlanner = (Panel)FormView1.FindControl("ZMCF_BoxPlanner");

            if (ZMCF_BoxPlanner != null)
                ZMCF_BoxPlanner.Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxPlanner").InnerText);

            Panel ZMCF_PWArea1 = (Panel)FormView1.FindControl("ZMCF_PWArea1");

            if (ZMCF_PWArea1 != null)
                ZMCF_PWArea1.Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_PWArea1").InnerText);

            Panel ZMCF_PWArea2 = (Panel)FormView1.FindControl("ZMCF_PWArea2");

            if (ZMCF_PWArea2 != null)
                ZMCF_PWArea2.Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_PWArea2").InnerText);

            Panel ZMCF_BoxMetaTags = (Panel)FormView1.FindControl("ZMCF_BoxMetaTags");

            if (ZMCF_BoxMetaTags != null)
                ZMCF_BoxMetaTags.Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxMetaTags").InnerText);

            Panel ZMCF_TagMeta1 = (Panel)FormView1.FindControl("ZMCF_TagMeta1");

            if (ZMCF_TagMeta1 != null)
                ZMCF_TagMeta1.Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_TagMeta1").InnerText);

            Panel ZMCF_TagMeta2 = (Panel)FormView1.FindControl("ZMCF_TagMeta2");

            if (ZMCF_TagMeta2 != null)
                ZMCF_TagMeta2.Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_TagMeta2").InnerText);

            switch (XRI1.Value)
            {
                case "1":
                    if (!Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_PermitLevel1").InnerText))
                        Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223598");
                    else if (!Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_UrlRewriteLiv1").InnerText))
                        DisableUrlRewriting();
                    break;

                case "2":
                    if (!Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_PermitLevel2").InnerText))
                        Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223598");
                    else if (!Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_UrlRewriteLiv2").InnerText))
                        DisableUrlRewriting();
                    break;

                case "3":
                    if (!Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_PermitLevel3").InnerText))
                        Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223598");
                    else if (!Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_UrlRewriteLiv3").InnerText))
                        DisableUrlRewriting();
                    break;
            }

            Label ZMCD_UrlSection = (Label)FormView1.FindControl("ZMCD_UrlSection");

            if (ZMCD_UrlSection != null)
                ZMCD_UrlSection.Text = mydoc.SelectSingleNode(myXPath + "ZMCD_UrlSection").InnerText;

            Label ZMCD_UrlDomain = (Label)FormView1.FindControl("ZMCD_UrlDomain");

            if (ZMCD_UrlDomain != null)
                ZMCD_UrlDomain.Text = mydoc.SelectSingleNode(myXPath + "ZMCD_UrlDomain").InnerText;

            if (!Page.IsPostBack)
            {

                bool[] Permit = new bool[3];

                Permit[0] = Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_PermitLevel1").InnerText);
                Permit[1] = Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_PermitLevel2").InnerText);
                Permit[2] = Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_PermitLevel3").InnerText);

                BindCategorieDropDownList(GetLang()
                    , ZIM.Value
                    , XRI1.Value, Permit);
            }

            return true;
        }
        catch (Exception p)
        {

            Response.Write(p.ToString());
            return false;
        }

    }//fine ReadXML


    private bool CheckPermit(string AllRoles)
    {

        bool IsPermit = false;

        try
        {

            if (AllRoles.Length == 0)
                return false;

            char[] delimiter = { ',' };


            foreach (string roles in Roles.GetRolesForUser(Membership.GetUser().UserName))
            {
                string[] PressRoles = AllRoles.Split(delimiter);

                for (int i = 0; i < PressRoles.Length; ++i)
                {
                    if (PressRoles[i].Trim().Equals(roles.Trim()))
                    {
                        IsPermit = true;
                        break;
                    }
                }//fine for

            }//fine else
        }
        catch (Exception)
        { }

        return IsPermit;

    }//fine CheckPermit



    private void DisableUrlRewriting()
    {
        //Label UrlPageDetailLabel = (Label)FormView1.FindControl("UrlPageDetailLabel");

        try
        {

            Label ServerLabel = (Label)FormView1.FindControl("ServerLabel");

            if (ServerLabel != null)
                ServerLabel.Visible = false;


            Label IDUrlRewriting = (Label)FormView1.FindControl("IDUrlRewriting");

            if (IDUrlRewriting != null)
                IDUrlRewriting.Visible = false;



            TextBox UrlPageDetailTextBox = (TextBox)FormView1.FindControl("UrlPageDetailTextBox");

            if (UrlPageDetailTextBox != null)
                UrlPageDetailTextBox.Visible = false;


            Label InfoUrlRewritingLabel = (Label)FormView1.FindControl("InfoUrlRewritingLabel");

            if (InfoUrlRewritingLabel != null)
                InfoUrlRewritingLabel.Visible = false;

        }
        catch (Exception)
        { }


        //UrlPageDetailLabel.Text = "Funzionalità non attiva su questo contenuto";

    }//fine DisableUrlRewriting


    private bool BindCategorieDropDownList(string ZeusLangCode, string ZeusIdModulo, string Livello, bool[] PermitLevels)
    {

        DropDownList CategoriaPadreDropDownList = (DropDownList)FormView1.FindControl("CategoriaPadreDropDownList");

        if (Livello.Equals("1"))
        {
            CategoriaPadreDropDownList.Enabled = false;
            CategoriaPadreDropDownList.Items.Add("Radice");
            return true;
        }



        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                if ((PermitLevels[2]) && (PermitLevels[1]))
                {
                    sqlQuery = "SELECT [ID1Categoria], [MenuLabel1] FROM [vwCategorie_Zeus1] WHERE ZeusIdModulo = @ZeusIdModulo AND ZeusLangCode = @ZeusLangCode AND Livello=@Livello";
                    sqlQuery += " AND (Livello=1 OR Livello=2)";
                    CategoriaPadreDropDownList.DataValueField = "ID1Categoria";
                    CategoriaPadreDropDownList.DataTextField = "MenuLabel1";
                }
                else if (PermitLevels[1])
                {
                    sqlQuery = "SELECT [ID1Categoria], [MenuLabel1] FROM [vwCategorie_Zeus1] WHERE ZeusIdModulo = @ZeusIdModulo AND ZeusLangCode = @ZeusLangCode AND Livello=@Livello";
                    sqlQuery += " AND Livello=1";
                    CategoriaPadreDropDownList.DataValueField = "ID1Categoria";
                    CategoriaPadreDropDownList.DataTextField = "MenuLabel1";
                }
                else if (PermitLevels[2])
                {
                    sqlQuery = "SELECT [ID1Categoria], [MenuLabel2] FROM [vwCategorie_Zeus1] WHERE ZeusIdModulo = @ZeusIdModulo AND ZeusLangCode = @ZeusLangCode AND Livello=@Livello";
                    sqlQuery += " AND Livello=2";
                    CategoriaPadreDropDownList.DataValueField = "ID1Categoria";
                    CategoriaPadreDropDownList.DataTextField = "MenuLabel2";

                }

                // sqlQuery = "SELECT [ID1Categoria], [MenuLabel1] FROM [vwCategorie_Zeus1] WHERE ZeusIdModulo = @ZeusIdModulo AND ZeusLangCode = @ZeusLangCode AND Livello=@Livello";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.NVarChar, 3);
                command.Parameters["@ZeusLangCode"].Value = ZeusLangCode;
                command.Parameters.Add("@ZeusIdModulo", System.Data.SqlDbType.NVarChar, 5);
                command.Parameters["@ZeusIdModulo"].Value = ZeusIdModulo;
                command.Parameters.Add("@Livello", System.Data.SqlDbType.Int);
                command.Parameters["@Livello"].Value = (Convert.ToInt32(Livello) - 1).ToString();

                CategoriaPadreDropDownList.DataSource = command.ExecuteReader();
                //CategoriaPadreDropDownList.DataValueField = "ID1Categoria";
                //CategoriaPadreDropDownList.DataTextField = "MenuLabel1";
                CategoriaPadreDropDownList.DataBind();



                HiddenField ID2CategoriaParentOldHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaParentOldHiddenField");
                CategoriaPadreDropDownList.SelectedIndex = CategoriaPadreDropDownList.Items.IndexOf(CategoriaPadreDropDownList.Items.FindByValue(ID2CategoriaParentOldHiddenField.Value));


                return true;
            }
            catch (Exception p)
            {

                Response.Write(p.ToString());
                return false;
            }
        }//fine Using


    }//fine BindCategorieDropDownList

    private int getLastOrder(string ID2CategoriaParent)
    {

        if (ID2CategoriaParent.Equals("0"))
            return 0;

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT MAX(Ordinamento) FROM tbCategorie WHERE ID2CategoriaParent=@ID2CategoriaParent AND ZeusIsAlive=1 AND ZeusLangCode=@ZeusLangCode  AND ZeusIdModulo=@ZeusIdModulo";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID2CategoriaParent", System.Data.SqlDbType.Int, 4);
                command.Parameters["@ID2CategoriaParent"].Value = ID2CategoriaParent;

                command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.NVarChar, 3);
                command.Parameters["@ZeusLangCode"].Value = Server.HtmlEncode(Request.QueryString["Lang"]);

                command.Parameters.Add("@ZeusIdModulo", System.Data.SqlDbType.NVarChar, 5);
                command.Parameters["@ZeusIdModulo"].Value = Server.HtmlEncode(Request.QueryString["ZIM"]);

                SqlDataReader reader = command.ExecuteReader();

                int myReturn = 0;

                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                        myReturn = reader.GetInt32(0);

                }//fine while

                reader.Close();
                return myReturn;
            }
            catch (Exception p)
            {

                Response.Write(p.ToString());
                return -1;
            }
        }//fine Using


    }//fine getLastOrder


    private bool ReOrder(string ID2CategoriaParent)
    {
        DropDownList CategoriaPadreDropDownList = (DropDownList)FormView1.FindControl("CategoriaPadreDropDownList");

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            SqlTransaction transaction = null;

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                transaction = connection.BeginTransaction();
                command.Transaction = transaction;

                sqlQuery = "SELECT ID1Categoria,Ordinamento FROM [vwCategorie_Zeus1] WHERE ID2CategoriaParent=@ID2CategoriaParent ORDER BY Ordinamento";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID2CategoriaParent", System.Data.SqlDbType.Int);
                command.Parameters["@ID2CategoriaParent"].Value = ID2CategoriaParent;

                SqlDataReader reader = command.ExecuteReader();

                int index = 1;
                ArrayList myArrayListCategorie = new ArrayList();

                while (reader.Read())
                {
                    if (reader.GetInt32(1) != index)
                    {
                        string[] myData = new string[3];


                        myData[0] = reader.GetInt32(0).ToString();
                        myData[1] = index.ToString();
                        myArrayListCategorie.Add(myData);

                    }
                    ++index;
                }//fine while

                reader.Close();


                string[][] myCategoryToChange = (string[][])myArrayListCategorie.ToArray(typeof(string[]));

                for (int i = 0; i < myCategoryToChange.Length; ++i)
                {

                    sqlQuery = "UPDATE tbCategorie SET Ordinamento=@Ordinamento WHERE ID1Categoria=@ID1Categoria";
                    command.CommandText = sqlQuery;
                    command.Parameters.Clear();

                    command.Parameters.Add("@Ordinamento", System.Data.SqlDbType.NVarChar, 4);
                    command.Parameters["@Ordinamento"].Value = myCategoryToChange[i][1];

                    command.Parameters.Add("@ID1Categoria", System.Data.SqlDbType.Int, 4);
                    command.Parameters["@ID1Categoria"].Value = myCategoryToChange[i][0];

                    command.ExecuteNonQuery();

                }//fine for


                transaction.Commit();

                return true;
            }
            catch (Exception p)
            {
                transaction.Rollback();
                Response.Write(p.ToString());
                return false;
            }
        }//fine Using


    }//fine BindCategorieDropDownList



    protected void ZeusUserEditableHiddenField_DataBinding(object sender, EventArgs e)
    {
        HiddenField ZeusUserEditableHiddenField = (HiddenField)sender;

        if (ZeusUserEditableHiddenField.Value.ToLower().Equals("false"))
            Response.Redirect("~/Zeus/System/Message.aspx?3");

    }//fine ZeusUserEditableHiddenField_DataBinding



    protected void DataLimite(object sender, EventArgs e)
    {
        TextBox DataLimite = (TextBox)sender;
        if (DataLimite.Text.Length == 0)
        {
            DataLimite.Text = "31/12/2040";
        }
    }

    protected void DataIniziale(object sender, EventArgs e)
    {
        DateTime Data = new DateTime();
        Data = DateTime.Now;
        TextBox DataCreazione = (TextBox)sender;
        if (DataCreazione.Text.Length == 0)
        {
            DataCreazione.Text = Data.ToString("d");
        }
    }

    protected void UtenteCreazione(object sender, EventArgs e)
    {
        HiddenField UtenteCreazione = (HiddenField)sender;
        UtenteCreazione.Value = Membership.GetUser().ProviderUserKey.ToString();

    }

    protected void DataOggi(object sender, EventArgs e)
    {
        HiddenField DataCreazione = (HiddenField)sender;
        DataCreazione.Value = DateTime.Now.ToString();

    }//fine DataOggi


    protected void CategoriaPadreDropDownList_SelectIndexChange(object sender, EventArgs e)
    {
        HiddenField ID2CategoriaParentHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaParentHiddenField");
        DropDownList CategoriaPadreDropDownList = (DropDownList)sender;

        ID2CategoriaParentHiddenField.Value = CategoriaPadreDropDownList.SelectedValue;

    }//fine CategoriaPadreDropDownList_SelectIndexChange


    //##############################################################################################################
    //################################################ FILE UPLOAD #################################################
    //############################################################################################################## 


    protected void ImageRaider_DataBinding(object sender, EventArgs e)
    {
        ImageRaider ImageRaider1 = (ImageRaider)sender;
        ImageRaider1.SetDefaultEditBetaImage();
        ImageRaider1.SetDefaultEditGammaImage();

    }

    protected void FileUploadCustomValidator_ServerValidate(object sender, ServerValidateEventArgs args)
    {
        try
        {
            ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");
            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");

            if (ImageRaider1.isValid())
                InsertButton.CommandName = "Update";
            else
                InsertButton.CommandName = string.Empty;

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine FileUploadCustomValidator

    protected void FileUpload2CustomValidator_ServerValidate(object sender, ServerValidateEventArgs args)
    {
        try
        {
            ImageRaider ImageRaider2 = (ImageRaider)FormView1.FindControl("ImageRaider2");
            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");

            if (ImageRaider2.isValid())
                InsertButton.CommandName = "Update";
            else
                InsertButton.CommandName = string.Empty;

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }
    //##############################################################################################################
    //########################################### FINE FILE UPLOAD #################################################
    //##############################################################################################################





    //##############################################################################################################
    //########################################### URLREWRITING #####################################################
    //##############################################################################################################


    private string GetLivello(string ID1Categoria)
    {
        string Livello = string.Empty;
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            try
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT [Livello]";
                sqlQuery += " FROM [vwCategorie_All]";
                sqlQuery += " WHERE [ID1Categoria]=" + Server.HtmlEncode(ID1Categoria);

                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                    if (!reader.IsDBNull(0))
                        Livello = reader.GetInt32(0).ToString();

                reader.Close();

            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
            }
        }//fine Using


        //Response.Write("DEB " + Livello + "<br />");

        switch (Livello)
        {
            case "1":
                Livello = "_A";
                break;

            case "2":
                Livello = "_B";
                break;

            case "3":
                Livello = "_C";
                break;
        }

        return Livello;

    }//fine GetLivello


    private string AddZero(string MyString)
    {
        switch (MyString.Length)
        {

            case 1:
                MyString = "000" + MyString;
                break;

            case 2:
                MyString = "00" + MyString;
                break;

            case 3:
                MyString = "0" + MyString;
                break;

        }//fine switch


        return MyString;

    }//AddZero





    //##############################################################################################################
    //###################################### FINE URLREWRITING #####################################################
    //##############################################################################################################



    protected void InsertButton_Click(object sender, EventArgs e)
    {

        LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");


        HiddenField FileNameBetaHiddenField = (HiddenField)FormView1.FindControl("FileNameBetaHiddenField");
        HiddenField FileNameGammaHiddenField = (HiddenField)FormView1.FindControl("FileNameGammaHiddenField");
        HiddenField Immagine12AltHiddenField = (HiddenField)FormView1.FindControl("Immagine12AltHiddenField");
        HiddenField FileNameBeta2HiddenField = (HiddenField)FormView1.FindControl("FileNameBeta2HiddenField");
        HiddenField FileNameGamma2HiddenField = (HiddenField)FormView1.FindControl("FileNameGamma2HiddenField");
        HiddenField Immagine34AltHiddenField = (HiddenField)FormView1.FindControl("Immagine34AltHiddenField");
        ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");
        ImageRaider ImageRaider2 = (ImageRaider)FormView1.FindControl("ImageRaider2");

        if (InsertButton.CommandName != string.Empty)
        {


            if (ImageRaider1.GenerateBeta(string.Empty))
                FileNameBetaHiddenField.Value = ImageRaider1.ImgBeta_FileName;
            else FileNameBetaHiddenField.Value = ImageRaider1.DefaultBetaImage;

            if (ImageRaider1.GenerateGamma(string.Empty))
                FileNameGammaHiddenField.Value = ImageRaider1.ImgGamma_FileName;
            else FileNameGammaHiddenField.Value = ImageRaider1.DefaultGammaImage;

            if (ImageRaider2.GenerateBeta(string.Empty))
                FileNameBeta2HiddenField.Value = ImageRaider2.ImgBeta_FileName;
            else FileNameBeta2HiddenField.Value = ImageRaider2.DefaultBetaImage;

            if (ImageRaider2.GenerateGamma(string.Empty))
                FileNameGamma2HiddenField.Value = ImageRaider2.ImgGamma_FileName;
            else FileNameGamma2HiddenField.Value = ImageRaider2.DefaultGammaImage;

            HiddenField ID2CategoriaParentHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaParentHiddenField");
            HiddenField ID2CategoriaParentOldHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaParentOldHiddenField");


            if (!ID2CategoriaParentHiddenField.Value.Equals(ID2CategoriaParentOldHiddenField.Value))
            {
                HiddenField OrdinamentoHiddenField = (HiddenField)FormView1.FindControl("OrdinamentoHiddenField");

                int Order = getLastOrder(ID2CategoriaParentHiddenField.Value);
                ++Order;
                OrdinamentoHiddenField.Value = Order.ToString();


            }

            delinea myDelinea = new delinea();
            HiddenField UrlRewriteHiddenField = (HiddenField)FormView1.FindControl("UrlRewriteHiddenField");
            TextBox UrlPageDetailTextBox = (TextBox)FormView1.FindControl("UrlPageDetailTextBox");

            UrlRewriteHiddenField.Value = myDelinea.myHtmlEncode(UrlPageDetailTextBox.Text.TrimEnd());// +"_" + AddZero(Server.HtmlEncode(Request.QueryString["XRI"])) + GetLivello(Server.HtmlEncode(Request.QueryString["XRI"])) + ".aspx";

        }//fine if commandname
    }//fine InsertButton_Click


    protected void CategorieSqlDataSource_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.Exception == null)
        {
            HiddenField ID2CategoriaParentHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaParentHiddenField");
            HiddenField ID2CategoriaParentOldHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaParentOldHiddenField");

            if (!ID2CategoriaParentHiddenField.Value.Equals(ID2CategoriaParentOldHiddenField.Value))
                ReOrder(ID2CategoriaParentOldHiddenField.Value);

            Response.Redirect("~/Zeus/Categorie/Categorie_Lst.aspx?ZIM="
                + ZIM.Value
                + "&Lang=" + GetLang()
                + "&XRI1=" + XRI.Value);
        }
        else Response.Redirect("~/Zeus/System/Message.aspx?UpdErr");

    }//fine CategorieSqlDataSource_Inserted


    protected void CategorieSqlDataSource_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {
        if ((e.Exception != null)
            || (e.AffectedRows <= 0))
            Response.Redirect("~/Zeus/System/Message.aspx?SelErr");
    }

}