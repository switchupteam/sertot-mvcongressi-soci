﻿using System;
using System.Data.SqlClient;
using System.Web.UI;

/// <summary>
/// Descrizione di riepilogo per Categorie_Del
/// </summary>
public partial class Categorie_Del : System.Web.UI.Page
{
    static string TitoloPagina = "Elimina categoria";

    protected void Page_Init(object sender, EventArgs e)
    {
        try
        {

            delinea myDelinea = new delinea();
            TitleField.Value = TitoloPagina;

            if (!Page.IsPostBack)
            {
                if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM"))
                    || (!isOK())
                    || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI")))
                    Response.Redirect("~/Zeus/System/Message.aspx?0");

                ID1Categoria.Value = Server.HtmlEncode(Request.QueryString["XRI"]);

                TitoloLabel.Text = GetCategoria(ID1Categoria.Value);
            }

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }


    }

    private bool isOK()
    {
        try
        {
            if ((Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/Categorie/Categorie_Dtl.aspx") > 0)
                || (Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/Categorie/Categorie_Edt.aspx") > 0))
                return true;
            else return false;
        }
        catch (Exception)
        {
            return false;
        }

    }

    private string GetLang()
    {
        try
        {
            delinea myDelinea = new delinea();
            string Lang = "ITA";

            if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
                && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
                Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

            return Lang;
        }
        catch (Exception)
        {
            return "ITA";
        }
    }


    private string GetCategoria(string ID1Categoria)
    {

        string Categoria = string.Empty;
        try
        {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            string sqlQuery = "SELECT Categoria";
            sqlQuery += " FROM tbCategorie ";
            sqlQuery += " WHERE ID1Categoria=" + Server.HtmlEncode(ID1Categoria);           

            using (SqlConnection conn = new SqlConnection(strConnessione))
            {
                SqlCommand command = new SqlCommand(sqlQuery, conn);

                conn.Open();
                SqlDataReader reader = command.ExecuteReader();
                /*fino a quando ci sono record*/


                while (reader.Read())
                    if (reader["Categoria"] != DBNull.Value)
                        Categoria = reader["Categoria"].ToString();


                reader.Close();


                bool IsAssociata = false;

                sqlQuery = " SELECT ID1Categoria ";
                sqlQuery += " FROM vwCategorie_All";
                sqlQuery += " WHERE ID1Categoria=" + ID1Categoria;
                sqlQuery += " AND Livello=1";

                command.CommandText = sqlQuery;
                reader = command.ExecuteReader();
                IsAssociata = reader.HasRows;
                reader.Close();

                sqlQuery = " SELECT ID1Categoria ";
                sqlQuery += " FROM vwCategorie_All";
                sqlQuery += " WHERE ID1Categoria=" + ID1Categoria;
                sqlQuery += " AND Livello=2";

                command.CommandText = sqlQuery;
                reader = command.ExecuteReader();
                IsAssociata = IsAssociata || reader.HasRows;
                reader.Close();

                if (IsAssociata)
                    InfoLabel.Text = "<br />Attenzione: esistono record associati al record che si desidera eliminare. Prima di procedere con l’eliminazione è consigliato spostare i record figli sotto altro padre.";



            }

        }
        catch (Exception p)
        {
            ErrorLabel.Text = p.ToString();
        }

        return Categoria;

    }

    private bool DeleteRecord(string ID1Categoria)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                sqlQuery = "UPDATE tbCategorie SET ZeusIsAlive=0 WHERE ID1Categoria=" + ID1Categoria;
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();
            }
            catch (Exception p)
            {

                ErrorLabel.Text = p.ToString();
                return false;
            }

        }

        return true;

    }


    protected void EseguiButton_Click(object sender, EventArgs e)
    {
        if (ConfermaCheckBox.Checked)
        {
            if (DeleteRecord(ID1Categoria.Value))
                Response.Redirect("~/Zeus/System/Message.aspx?Msg=54321957136256568");
        }
        else ErrorLabel.Text = "Per rimuovere definitivamente la categoria è necessario confermare l'eliminazione.";
    }

}