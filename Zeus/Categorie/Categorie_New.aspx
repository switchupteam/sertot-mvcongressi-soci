﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="Categorie_New.aspx.cs"
    Inherits="Categorie_New" 
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Zeus/SiteAssets/ZeusColorPicker.ascx" TagName="ZeusColorPicker" TagPrefix="uc2" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <script type="text/javascript">
        function OnClientModeChange(editor) {
            var mode = editor.get_mode();
            var doc = editor.get_document();
            var head = doc.getElementsByTagName("HEAD")[0];
            var link;

            switch (mode) {
                case 1: //remove the external stylesheet when displaying the content in Design mode    
                    //var external = doc.getElementById("external");
                    //head.removeChild(external);
                    break;
                case 2:
                    break;
                case 4: //apply your external css stylesheet to Preview mode    
                    link = doc.createElement("LINK");
                    link.setAttribute("href", "/SiteCss/Telerik.css");
                    link.setAttribute("rel", "stylesheet");
                    link.setAttribute("type", "text/css");
                    link.setAttribute("id", "external");
                    head.appendChild(link);
                    break;
            }
        }

        function editorCommandExecuted(editor, args) {
            if (!$telerik.isChrome)
                return;
            var dialogName = args.get_commandName();
            var dialogWin = editor.get_dialogOpener()._dialogContainers[dialogName];
            if (dialogWin) {
                var cellEl = dialogWin.get_contentElement() || dialogWin.ui.contentCell || dialogWin.ui.content,
                frame = dialogWin.get_contentFrame();
                frame.onload = function () {
                    cellEl.style.cssText = "";
                    dialogWin.autoSize();
                }
            }
        }
    </script>   
    <asp:HiddenField ID="TitleField" runat="server"  />
    <asp:HiddenField ID="ZIM" runat="server"  />
    
    <asp:FormView ID="FormView1" runat="server" DefaultMode="Insert" DataSourceID="CategorieSqlDataSource"
        Width="100%" >
        <InsertItemTemplate>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="ZML_BoxCategoria" runat="server" Text="Categoria"></asp:Label></div>
                <table>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="ZML_CategoriaParent" SkinID="FieldDescription" runat="server" Text="Categoria padre *"></asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:HiddenField ID="ID2CategoriaParentHiddenField" runat="server" Value='<%# Bind("ID2CategoriaParent") %>' />
                            <asp:DropDownList ID="CategoriaPadreDropDownList" runat="server" AppendDataBoundItems="True"
                                OnSelectedIndexChanged="CategoriaPadreDropDownList_SelectIndexChange">
                                <asp:ListItem Selected="True" Value="-1">&gt; Seleziona</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="ID2CategoriaRequiredFieldValidator" ErrorMessage="Obbligatorio"
                                runat="server" ControlToValidate="CategoriaPadreDropDownList" SkinID="ZSSM_Validazione01"
                                Display="Dynamic" InitialValue="-1"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="ZML_Categoria" SkinID="FieldDescription" runat="server" Text="Categoria *"></asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:TextBox ID="CategoriaTextBox" runat="server" MaxLength="100" Columns="50" Text='<%# Bind("Categoria") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ID="CategoriaRequiredFieldValidator" ErrorMessage="Obbligatorio"
                                runat="server" ControlToValidate="CategoriaTextBox" Display="Dynamic" SkinID="ZSSM_Validazione01">
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
        
                    
                </table>
            </div>
            <!---DESCRIZIONE BREVE --->
            <asp:Panel ID="ZMCF_DescBreve1" runat="server" Visible="false">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="ZML_DescBreveBox" runat="server" Text="Descrizione breve"></asp:Label></div>
                    <table>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_DescBreve" runat="server" SkinID="FieldDescription" Text="Descrizione"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <CustomWebControls:TextArea ID="DescBreveTextBox" runat="server" Columns="80" EnableTheming="True"
                                    Height="100px" MaxLength="500" Rows="4" Text='<%# Bind("DescBreve1", "{0}") %>'
                                    TextMode="MultiLine"></CustomWebControls:TextArea>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <!---DESCRIZIONE --->
            <asp:Panel ID="ZMCF_Descrizione1" runat="server" Visible="false">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="ZML_BoxDescrizione" runat="server" Text="Descrizione"></asp:Label></div>
                    <table>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_Descrizione1" runat="server" SkinID="FieldDescription" Text="Descrizione"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <telerik:RadEditor 
                                    Language="it-IT"  ID="RadEditor1" runat="server"
                                    DocumentManager-DeletePaths="~/ZeusInc/Categorie/Documents" 
                                    DocumentManager-SearchPatterns="*.*"
                                    DocumentManager-ViewPaths="~/ZeusInc/Categorie/Documents"
                                    DocumentManager-MaxUploadFileSize="52428800"
                                    DocumentManager-UploadPaths="~/ZeusInc/Categorie/Documents"

                                    FlashManager-DeletePaths="~/ZeusInc/Categorie/Media"
                                    FlashManager-MaxUploadFileSize="10240000"
                                    FlashManager-ViewPaths="~/ZeusInc/Categorie/Media"
                                    FlashManager-UploadPaths="~/ZeusInc/Categorie/Media"

                                    ImageManager-DeletePaths="~/ZeusInc/Categorie/Images"                                                                            
                                    ImageManager-ViewPaths="~/ZeusInc/Categorie/Images" 
                                    ImageManager-MaxUploadFileSize="10240000"
                                    ImageManager-SearchPatterns="*.gif, *.png, *.jpg, *.jpe, *.jpeg"
                                    ImageManager-UploadPaths="~/ZeusInc/Categorie/Images"
                                    ImageManager-ViewMode="Grid"
                                    MediaManager-DeletePaths="~/ZeusInc/Categorie/Media"
                                    MediaManager-MaxUploadFileSize="10240000"
                                    MediaManager-SearchPatterns="*.asf, *.asx, *.wma, *.wmv, *.avi, *.wav, *.mpeg, *.mpg, *.mpe, *.mov, *.mp3, *.m3u, *.mid, *.midi, *.rm, *.rma"
                                    MediaManager-ViewPaths="~/ZeusInc/Categorie/Media"
                                    MediaManager-UploadPaths="~/ZeusInc/Categorie/Media"
                                     
                                    TemplateManager-SearchPatterns="*.html,*.htm"
                                    ContentAreaMode="iframe"
                                    OnClientCommandExecuted="editorCommandExecuted"                                    
                                    OnClientModeChange="OnClientModeChange"
                                    Content='<%# Bind("Descrizione1") %>'
                                    ToolsFile="~/Zeus/Categorie/RadEditor1.xml"
                                    LocalizationPath="~/App_GlobalResources"
                                    AllowScripts="true" RenderMode="Classic" ToolbarMode="Default" EnableViewState="False"
                                    Width="750px" Height="500px"  
                                    >                                          
                                    <CssFiles>
                                        <telerik:EditorCssFile Value="~/asset/css/ZeusTypeFoundry.css" />
                                    </CssFiles>                                                                                                
                               </telerik:RadEditor>
                              
                            </td>
                        </tr>
                        <asp:Panel ID="ZMCF_Descrizione2" runat="server" Visible="false">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_Descrizione2" runat="server" SkinID="FieldDescription" Text="Descrizione"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <telerik:RadEditor 
                                    Language="it-IT"  ID="RadEditor2" runat="server"
                                    DocumentManager-DeletePaths="~/ZeusInc/Categorie/Documents" 
                                    DocumentManager-SearchPatterns="*.*"
                                    DocumentManager-ViewPaths="~/ZeusInc/Categorie/Documents"
                                    DocumentManager-MaxUploadFileSize="52428800"
                                    DocumentManager-UploadPaths="~/ZeusInc/Categorie/Documents"

                                    FlashManager-DeletePaths="~/ZeusInc/Categorie/Media"
                                    FlashManager-MaxUploadFileSize="10240000"
                                    FlashManager-ViewPaths="~/ZeusInc/Categorie/Media"
                                    FlashManager-UploadPaths="~/ZeusInc/Categorie/Media"

                                    ImageManager-DeletePaths="~/ZeusInc/Categorie/Images"                                                                            
                                    ImageManager-ViewPaths="~/ZeusInc/Categorie/Images" 
                                    ImageManager-MaxUploadFileSize="10240000"
                                    ImageManager-SearchPatterns="*.gif, *.png, *.jpg, *.jpe, *.jpeg"
                                    ImageManager-UploadPaths="~/ZeusInc/Categorie/Images"
                                    ImageManager-ViewMode="Grid"
                                    MediaManager-DeletePaths="~/ZeusInc/Categorie/Media"
                                    MediaManager-MaxUploadFileSize="10240000"
                                    MediaManager-SearchPatterns="*.asf, *.asx, *.wma, *.wmv, *.avi, *.wav, *.mpeg, *.mpg, *.mpe, *.mov, *.mp3, *.m3u, *.mid, *.midi, *.rm, *.rma"
                                    MediaManager-ViewPaths="~/ZeusInc/Categorie/Media"
                                    MediaManager-UploadPaths="~/ZeusInc/Categorie/Media"
                                     
                                    TemplateManager-SearchPatterns="*.html,*.htm"
                                    ContentAreaMode="iframe"
                                    OnClientCommandExecuted="editorCommandExecuted"                                    
                                    OnClientModeChange="OnClientModeChange"
                                    Content='<%# Bind("Descrizione2") %>'
                                    ToolsFile="~/Zeus/Categorie/RadEditor2.xml"
                                    LocalizationPath="~/App_GlobalResources"
                                    AllowScripts="true" RenderMode="Classic" ToolbarMode="Default" EnableViewState="False"
                                    Width="750px" Height="500px"  
                                    >                                          
                                    <CssFiles>
                                        <telerik:EditorCssFile Value="~/asset/css/ZeusTypeFoundry.css" />
                                    </CssFiles>                                                                                                
                               </telerik:RadEditor>
                                
                            </td>
                        </tr>
                        </asp:Panel>
                    </table>
                </div>
            </asp:Panel>
             
            <asp:Panel ID="ApettoGraficoPanel" runat="server" Visible="false">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="Label1" runat="server" Text="Aspetto grafico"></asp:Label></div>
                    <table>
                        <%--<asp:Panel ID="PZV_StylePanel" runat="server">
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label ID="Label2" runat="server" SkinID="FieldDescription" Text="Stile"></asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:TextBox ID="StyleTextBox" runat="server" Text='<%# Bind("Style") %>' MaxLength="20"></asp:TextBox>
                                </td>
                            </tr>
                        </asp:Panel>--%>
                        <asp:Panel ID="ZMCF_BoxColore" runat="server">
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label ID="Label3" runat="server" SkinID="FieldDescription" Text="Colore"></asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <uc2:ZeusColorPicker ID="ZeusColorPicker2" runat="server" SelectedColor='<%# Bind("Colore1") %>'
                                        />
                                </td>
                            </tr>
                        </asp:Panel>
                    </table>
                </div>
            </asp:Panel>
            <asp:Panel ID="ZMCF_BoxImmagine1" runat="server">
                <dlc:ImageRaider ID="ImageRaider1" runat="server" ZeusIdModuloIndice="1" ZeusLangCode="ITA"
                    BindPzFromDB="true" />
                <asp:HiddenField ID="FileNameBetaHiddenField" runat="server" Value='<%# Bind("Immagine1") %>' />
                <asp:HiddenField ID="FileNameGammaHiddenField" runat="server" Value='<%# Bind("Immagine2") %>' />
                <asp:HiddenField ID="Immagine12AltHiddenField" runat="server" Value='<%# Bind("Immagine12Alt") %>' />
               <asp:CustomValidator ID="FileUploadCustomValidator" runat="server" OnServerValidate="FileUploadCustomValidator_ServerValidate"
                    Display="Dynamic" SkinID="ZSSM_Validazione01"
                    Visible="false"></asp:CustomValidator>
                     </asp:Panel>
                <asp:Panel ID="ZMCF_BoxImmagine2" runat="server">
           <dlc:ImageRaider ID="ImageRaider2" runat="server" ZeusIdModuloIndice="1" ZeusLangCode="ITA"
                    BindPzFromDB="true" />
                 <asp:HiddenField ID="FileNameBeta2HiddenField" runat="server" Value='<%# Bind("Immagine3") %>' />
            <asp:HiddenField ID="FileNameGamma2HiddenField" runat="server" Value='<%# Bind("Immagine4") %>' />
                    <asp:HiddenField ID="Immagine34AltHiddenField" runat="server" Value='<%# Bind("Immagine34Alt") %>' />
            <asp:CustomValidator ID="FileUpload2CustomValidator" runat="server" OnServerValidate="FileUpload2CustomValidator_ServerValidate"
                Display="Dynamic" SkinID="ZSSM_Validazione01" ValidationGroup="myValidation"></asp:CustomValidator>
            </asp:Panel>
            <asp:Panel ID="ZMCF_BoxMetaTags" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="Label9" runat="server" Text="SEO Search Engine Optimization"></asp:Label></div>
                    <table>
                        <asp:Panel ID="ZMCF_TitoloBrowser" runat="server" >
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label2" SkinID="FieldDescription" runat="server" Text="TITLE / Titolo Browser"></asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:TextBox ID="TitoloBrowserTextBox" runat="server" MaxLength="100" Columns="50" Text='<%# Bind("TitoloBrowser") %>'></asp:TextBox>
                        </td>
                    </tr>
                    </asp:Panel>

                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label13" runat="server" SkinID="FieldDescription" Text="Label">Description</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="ZMCD_TagDescription" runat="server" Text='<%# Bind("TagDescription") %>'
                                    Columns="130" MaxLength="280"></asp:TextBox>
                               <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationExpression="^[a-zA-Z0-9_.;:+, ]+$"
                                    Display="Dynamic" ErrorMessage="Caratteri consentiti: lettere numeri e .,:;_-+"
                                    SkinID="ZSSM_Validazione01" ControlToValidate="ZMCD_TagDescription"></asp:RegularExpressionValidator>--%>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label14" runat="server" SkinID="FieldDescription" Text="Label">Keywords</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="ZMCD_TagKeywords" runat="server" Text='<%# Bind("TagKeywords") %>'
                                    Columns="130" MaxLength="280"></asp:TextBox>
                                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ValidationExpression="^[a-zA-Z0-9_.;:+, ]+$"
                                    Display="Dynamic" ErrorMessage="Caratteri consentiti: lettere numeri e .,:;_-+"
                                    SkinID="ZSSM_Validazione01" ControlToValidate="ZMCD_TagKeywords"></asp:RegularExpressionValidator>--%>
                            </td>
                        </tr>
                        <asp:Panel ID="ZMCF_TagMeta1" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_TagMeta1" runat="server" SkinID="FieldDescription" Text="Label">Description</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:DropDownList ID="TagMeta1AttributeDropDownList" runat="server" SelectedValue='<%# Bind("TagMeta1Attribute") %>'>
                                    <asp:ListItem Text="name" Value="name" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="http-equiv" Value="http-equiv"></asp:ListItem>
                                    <asp:ListItem Text="property" Value="property"></asp:ListItem>
                                    
                                </asp:DropDownList>
                                <asp:Label ID="Value1Label" runat="server" SkinID="FieldValue">Value</asp:Label>
                                <asp:TextBox ID="TagMeta1ValueTextBox" runat="server" Text='<%# Bind("TagMeta1Value") %>'
                                    Columns="20" MaxLength="30"></asp:TextBox>
                                    <%--<asp:RegularExpressionValidator ID="Regex1" runat="server" 
                                    ValidationExpression="^[a-zA-Z0-9_.;:+, ]+$" 
                                    Display="Dynamic" ErrorMessage="Caratteri consentiti: lettere numeri e .,:;_-+"
                                   SkinID="ZSSM_Validazione01"
                                    ControlToValidate="TagMeta1ValueTextBox"></asp:RegularExpressionValidator>--%>
                                <img src="../SiteImg/Spc.gif" width="10" />
                                <asp:Label ID="Label5" runat="server" SkinID="FieldValue">Content </asp:Label>
                                <asp:TextBox ID="TagMeta1ContentTextBox" runat="server" Text='<%# Bind("TagMeta1Content") %>'
                                    Columns="85" MaxLength="280"></asp:TextBox>
                                    <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator18" runat="server" 
                                    ValidationExpression="^[a-zA-Z0-9_.;:+, ]+$" 
                                    Display="Dynamic" ErrorMessage="Caratteri consentiti: lettere numeri e .,:;_-+"
                                   SkinID="ZSSM_Validazione01"
                                    ControlToValidate="TagMeta1ContentTextBox"></asp:RegularExpressionValidator>--%>
                                <asp:CustomValidator ID="CustomValidator4" runat="server"
                                    Display="Dynamic" ClientValidationFunction="BothRequired1" ErrorMessage="I campi Value e Content devono essere entrambi compilati oppure vuoti"
                                    SkinID="ZSSM_Validazione01"></asp:CustomValidator>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_TagMeta2" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_TagMeta2" runat="server" SkinID="FieldDescription" Text="Label">Description</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:DropDownList ID="TagMeta2AttributeDropDownList" runat="server" SelectedValue='<%# Bind("TagMeta2Attribute") %>'>
                                    <asp:ListItem Text="name" Value="name" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="http-equiv" Value="http-equiv"></asp:ListItem>
                                   <asp:ListItem Text="property" Value="property"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="Value2Label" runat="server" SkinID="FieldValue">Value</asp:Label>
                                <asp:TextBox ID="TagMeta2ValueTextBox" runat="server" Text='<%# Bind("TagMeta2Value") %>'
                                    Columns="20" MaxLength="30"></asp:TextBox>
                                     <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator19" runat="server" 
                                    ValidationExpression="^[a-zA-Z0-9_.;:+, ]+$" 
                                    Display="Dynamic" ErrorMessage="Caratteri consentiti: lettere numeri e .,:;_-+"
                                   SkinID="ZSSM_Validazione01"
                                    ControlToValidate="TagMeta2ValueTextBox"></asp:RegularExpressionValidator>--%>
                                <img src="../SiteImg/Spc.gif" width="10" />
                                <asp:Label ID="Label37" runat="server" SkinID="FieldValue">Content </asp:Label>
                                <asp:TextBox ID="TagMeta2ContentTextBox" runat="server" Text='<%# Bind("TagMeta2Content") %>'
                                    Columns="85" MaxLength="280"></asp:TextBox>
                                     <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator20" runat="server" 
                                    ValidationExpression="^[a-zA-Z0-9_.;:+, ]+$" 
                                    Display="Dynamic" ErrorMessage="Caratteri consentiti: lettere numeri e .,:;_-+"
                                   SkinID="ZSSM_Validazione01"
                                    ControlToValidate="TagMeta2ContentTextBox"></asp:RegularExpressionValidator>--%>
                                <asp:CustomValidator ID="CustomValidator5" runat="server"
                                    Display="Dynamic" ClientValidationFunction="BothRequired2" ErrorMessage="I campi Value e Content devono essere entrambi compilati oppure vuoti"
                                    SkinID="ZSSM_Validazione01"></asp:CustomValidator>
                            </td>
                        </tr>
                    </asp:Panel>
                    </table>
                </div>
            </asp:Panel>
            
            <asp:Panel ID="ZMCF_BoxPlanner" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="PlannerLabel" runat="server" Text="Planner"></asp:Label>
                    </div>
                    <table>
                        <asp:Panel ID="ZMCF_PWArea1" runat="server">
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label ID="ZML_PWArea1_New" runat="server" SkinID="FieldDescription" Text="Categoria principale"></asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:CheckBox ID="PW_Area1CheckBox" runat="server" Checked='<%# Bind("PW_Area1") %>' />
                                    <asp:Label ID="PW_Area1LstLabel" runat="server" SkinID="FieldValue" Text="Attiva pubblicazione dalla data"></asp:Label>
                                    <asp:TextBox ID="PWI_Area1TextBox" runat="server" Text='<%# Bind("PWI_Area1", "{0:d}") %>'
                                        Columns="10" MaxLength="10" OnDataBinding="DataIniziale"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" SkinID="ZSSM_Validazione01"
                                        runat="server" ControlToValidate="PWI_Area1TextBox" Display="Dynamic" ErrorMessage="*"
                                       >Obbligatorio</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" SkinID="ZSSM_Validazione01"
                                        runat="server" Display="Dynamic" ErrorMessage="Formato data richiesto: GG/MM/AAAA"
                                        ControlToValidate="PWI_Area1TextBox" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>
                                    <asp:Label ID="AttivaA_Label" runat="server" SkinID="FieldValue" Text="alla data"></asp:Label>
                                    <asp:TextBox ID="PWF_Area1TextBox" runat="server" Text='<%# Bind("PWF_Area1", "{0:d}") %>'
                                        Columns="10" MaxLength="10" OnDataBinding="DataLimite"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" SkinID="ZSSM_Validazione01"
                                        runat="server" ControlToValidate="PWF_Area1TextBox" Display="Dynamic" ErrorMessage="*"
                                       >Obbligatorio</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" SkinID="ZSSM_Validazione01"
                                        runat="server" Display="Dynamic" ErrorMessage="Formato data richiesto: GG/MM/AAAA"
                                        ControlToValidate="PWF_Area1TextBox" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>
                                    <asp:CompareValidator ID="CompareValidator1" SkinID="ZSSM_Validazione01" runat="server"
                                        ControlToCompare="PWF_Area1TextBox" ControlToValidate="PWI_Area1TextBox"
                                        ErrorMessage="La data iniziale deve essere precedente o uguale alla data finale"
                                        Operator="LessThanEqual" Display="Dynamic" Type="Date"></asp:CompareValidator>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_PWArea2" runat="server" Visible="false">
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label ID="ZML_PWArea2_New" runat="server" SkinID="FieldDescription" Text="Categoria secondaria"></asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:CheckBox ID="PW_Area2CheckBox" runat="server" Checked='<%# Bind("PW_Area2") %>' />
                                    <asp:Label ID="PW_Area2LstLabel" runat="server" SkinID="FieldValue" Text="Attiva pubblicazione dalla data"></asp:Label>
                                    <asp:TextBox ID="PWI_Area2TextBox" runat="server" Columns="10" MaxLength="10" OnDataBinding="DataIniziale"
                                        Text='<%# Bind("PWI_Area2", "{0:d}") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" SkinID="ZSSM_Validazione01"
                                        runat="server" ControlToValidate="PWI_Area2TextBox" Display="Dynamic" ErrorMessage="*"
                                       >Obbligatorio</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" SkinID="ZSSM_Validazione01"
                                        runat="server" ControlToValidate="PWI_Area2TextBox"
                                        Display="Dynamic" ErrorMessage="Formato data richiesto: GG/MM/AAAA" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>
                                    <asp:Label ID="AttivaA2_Label" runat="server" SkinID="FieldValue" Text="alla data"></asp:Label>
                                    <asp:TextBox ID="PWF_Area2TextBox" runat="server" Columns="10" MaxLength="10" OnDataBinding="DataLimite"
                                        Text='<%# Bind("PWF_Area2", "{0:d}") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" SkinID="ZSSM_Validazione01"
                                        ControlToValidate="PWF_Area2TextBox" Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4"
                                        SkinID="ZSSM_Validazione01" runat="server" ControlToValidate="PWF_Area2TextBox"
                                        Display="Dynamic" ErrorMessage="Formato data richiesto: GG/MM/AAAA" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>
                                    <asp:CompareValidator ID="CompareValidator2" runat="server"
                                        SkinID="ZSSM_Validazione01" ControlToCompare="PWF_Area2TextBox" ControlToValidate="PWI_Area2TextBox"
                                        Display="Dynamic" ErrorMessage="La data iniziale deve essere precedente o uguale alla data finale"
                                        Operator="LessThanEqual" Type="Date"></asp:CompareValidator>
                                </td>
                            </tr>
                        </asp:Panel>
                          <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label7" runat="server" SkinID="FieldDescription" Text="Area Zeus"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:CheckBox ID="PW_Zeus1CheckBox" runat="server" Checked='<%# Bind("PW_Zeus1") %>' />
                                <asp:Label ID="Label8" runat="server" SkinID="FieldValue" Text="Attiva pubblicazione nel gestionale"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <asp:HiddenField ID="HiddenFieldZeusId" runat="server" Value='<%# Bind("ZeusId", "{0}") %>'
                OnDataBinding="CreazioneGUID" />
            <asp:HiddenField ID="RecordNewUserHiddenField" runat="server" Value='<%# Bind("RecordNewUser", "{0}") %>'
                OnDataBinding="UtenteCreazione" />
            <asp:HiddenField ID="RecordNewDateHiddenField" Visible="false" runat="server" Value='<%# Bind("RecordNewDate", "{0}") %>'
                OnDataBinding="DataOggi" />
            <asp:HiddenField ID="OrdinamentoHiddenField" runat="server" Value='<%# Bind("Ordinamento", "{0}") %>' />
            <div align="center">
                <dlc:mySummaryValidation ID="mySummaryValidation1" runat="server" />
                <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert"
                    SkinID="ZSSM_Button01" Text="Salva dati" OnClick="InsertButton_Click"></asp:LinkButton>
            </div>
        </InsertItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="CategorieSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        InsertCommand=" SET DATEFORMAT dmy; INSERT INTO [tbCategorie] ([ID2CategoriaParent], [Categoria], [DescBreve1],
         [Descrizione1],[Descrizione2],[Colore1], [Immagine1], [Immagine2],[Immagine12Alt],[Immagine3], [Immagine4],
         [Immagine34Alt],[Ordinamento], 
         [PW_Zeus1], [PW_Area1], [PWI_Area1], [PWF_Area1], [PW_Area2], [PWI_Area2], [PWF_Area2],
          [ZeusId], [ZeusIdModulo], [ZeusLangCode], [ZeusIsAlive], [RecordNewUser], [RecordNewDate],[ZeusUserEditable],
          [TagDescription], [TagKeywords],TagMeta1Value,TagMeta1Content,
         TagMeta2Value,TagMeta2Content,TagMeta1Attribute,TagMeta2Attribute,TitoloBrowser) 
          VALUES (@ID2CategoriaParent, @Categoria, @DescBreve1, @Descrizione1,@Descrizione2,@Colore1, @Immagine1, @Immagine2,
        @Immagine12Alt, @Immagine3, @Immagine4, @Immagine34Alt,
           @Ordinamento, @PW_Zeus1, @PW_Area1, @PWI_Area1, @PWF_Area1, @PW_Area2, @PWI_Area2, @PWF_Area2, @ZeusId,
            @ZeusIdModulo, @ZeusLangCode, @ZeusIsAlive, @RecordNewUser, @RecordNewDate,@ZeusUserEditable,
            @TagDescription, @TagKeywords ,@TagMeta1Value,@TagMeta1Content,
            @TagMeta2Value,@TagMeta2Content,@TagMeta1Attribute,@TagMeta2Attribute,@TitoloBrowser); 
            SELECT @XRI = SCOPE_IDENTITY();" OnInserted="CategorieSqlDataSource_Inserted">
        <InsertParameters>
            <asp:Parameter Name="ID2CategoriaParent" Type="Int32" DefaultValue="0" />
            <asp:Parameter Name="Categoria" Type="String" />
            <asp:Parameter Name="TitoloBrowser" Type="String" />
            <asp:Parameter Name="DescBreve1" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="Descrizione1" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="Descrizione2" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="Colore1" Type="String" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="Immagine1" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="Immagine2" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="Immagine12Alt" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="Immagine3" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="Immagine4" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="Immagine34Alt" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="Ordinamento" Type="Int32" DefaultValue="1" />
            <asp:Parameter Name="TagDescription" Type="String" />
            <asp:Parameter Name="TagKeywords" Type="String" />
            <asp:Parameter Name="PW_Zeus1" Type="Boolean" />
            <asp:Parameter Name="PW_Area1" Type="Boolean" />
            <asp:Parameter Name="PWI_Area1" Type="DateTime" />
            <asp:Parameter Name="PWF_Area1" Type="DateTime" />
            <asp:Parameter Name="PW_Area2" Type="Boolean" />
            <asp:Parameter Name="PWI_Area2" Type="DateTime" />
            <asp:Parameter Name="PWF_Area2" Type="DateTime" />
            <asp:Parameter Name="ZeusId" />
            <asp:Parameter Name="ZeusIsAlive" Type="Boolean" DefaultValue="True" />
            <asp:Parameter Name="RecordNewUser" />
            <asp:Parameter Name="RecordNewDate" Type="DateTime" />
            <asp:Parameter Name="ZeusUserEditable" Type="Boolean" DefaultValue="True" />
             <asp:Parameter Name="TagMeta1Value" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="TagMeta1Content" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="TagMeta2Value" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="TagMeta2Content" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="TagMeta1Attribute" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="TagMeta2Attribute" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:QueryStringParameter Name="ZeusIdModulo" Type="String" QueryStringField="ZIM" />
            <asp:QueryStringParameter Name="ZeusLangCode" Type="String" QueryStringField="Lang"
                DefaultValue="ITA" />
            <asp:Parameter Direction="Output" Name="XRI" Type="Int32" />
        </InsertParameters>
    </asp:SqlDataSource>
</asp:Content>
