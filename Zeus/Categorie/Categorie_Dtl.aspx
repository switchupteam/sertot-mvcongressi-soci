﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="Categorie_Dtl.aspx.cs"
    Inherits="Categorie_Dtl"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Zeus/Categorie/CategoriePhoto.ascx" TagName="PublisherPhoto"
    TagPrefix="uc3" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <link href="../../asset/css/ZeusTypeFoundry.css" rel="stylesheet" type="text/css" />
    <link href="../../SiteCss/ZeusSnippets.css" rel="stylesheet" type="text/css" />
    <asp:HiddenField ID="TitleField" runat="server" />
    <asp:HiddenField ID="XRI" runat="server" />
    <asp:HiddenField ID="XRI1" runat="server" />
    <asp:HiddenField ID="ZIM" runat="server" />
    <asp:FormView ID="FormView1" runat="server" DefaultMode="ReadOnly" DataSourceID="CategorieSqlDataSource"
        Width="100%">
        <ItemTemplate>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="ZML_BoxCategoria" runat="server" Text="Categoria"></asp:Label></div>
                <table>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="ZML_CategoriaParent" SkinID="FieldDescription" runat="server" Text="Categoria padre"></asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:Label ID="CategoriaInfoLabel" runat="server" SkinID="FieldValue"></asp:Label>
                            <asp:HiddenField ID="ID2CategoriaParentHiddenField" runat="server" Value='<%# Eval("ID2CategoriaParent") %>'
                                OnDataBinding="ID2CategoriaParentHiddenField_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="ZML_Categoria" SkinID="FieldDescription" runat="server" Text="Categoria"></asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:Label ID="Categoria12Label" runat="server" SkinID="FieldValue" Text='<%# Eval("Categoria") %>'></asp:Label>
                        </td>
                    </tr>
                 </table>
            </div>
            <!---DESCRIZIONE BREVE --->
            <asp:Panel ID="ZMCF_DescBreve1" runat="server" Visible="false">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="ZML_DescBreveBox" runat="server" Text="Descrizione breve"></asp:Label></div>
                    <table>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_DescBreve" runat="server" SkinID="FieldDescription" Text="Descrizione"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:Label ID="Desc12BreveLabel" runat="server" Text='<%# Eval("DescBreve1", "{0}") %>'
                                    SkinID="FieldValue"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <!---DESCRIZIONE --->
            <asp:Panel ID="ZMCF_Descrizione1" runat="server" Visible="false">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="ZML_BoxDescrizione" runat="server" Text="Descrizione"></asp:Label></div>
                    <table>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_Descrizione1" runat="server" SkinID="FieldDescription" Text="Descrizione"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("Descrizione1", "{0}") %>' SkinID="FieldValue"></asp:Label>
                            </td>
                        </tr>
                        <asp:Panel ID="ZMCF_Descrizione2" runat="server" Visible="false">
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label ID="ZML_Descrizione2" runat="server" SkinID="FieldDescription" Text="Descrizione"></asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:Label ID="Label10" runat="server" Text='<%# Eval("Descrizione2", "{0}") %>'
                                        SkinID="FieldValue"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                    </table>
                </div>
            </asp:Panel>
            <asp:Panel ID="ApettoGraficoPanel" runat="server" Visible="false">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="Label6" runat="server" Text="Aspetto grafico"></asp:Label></div>
                    <table>
                        <%-- <asp:Panel ID="PZV_StylePanel" runat="server">
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label ID="Label7" runat="server" SkinID="FieldDescription" Text="Stile"></asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:Label ID="StyleLabel" runat="server" Text='<%# Eval("Style") %>' SkinID="FieldValue" ></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>--%>
                        <asp:Panel ID="ZMCF_BoxColore" runat="server">
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label ID="Label8" runat="server" SkinID="FieldDescription" Text="Colore"></asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <div id="ColoreDiv" runat="server" style="width: 40px; height: 20px; border: 1px solid #333333;">
                                    </div>
                                    <asp:Label ID="ColorLabel" runat="server" Text=' <%# Eval("Colore1", "#{0}") %>'
                                        OnDataBinding="ColorLabel_DataBinding"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                    </table>
                </div>
            </asp:Panel>
                <dlc:ImageRaider ID="ImageRaider1" runat="server" ZeusIdModuloIndice="1" ZeusLangCode="ITA"
                    BindPzFromDB="true" IsEditMode="true" DefaultEditBetaImage='<%# Eval("Immagine1") %>'
                    DefaultEditGammaImage='<%# Eval("Immagine2") %>' Enabled="false" 
                    ImageAlt='<%# Eval("Immagine12Alt") %>' OnDataBinding="ImageRaider_DataBinding" />
                <dlc:ImageRaider ID="ImageRaider2" runat="server" ZeusIdModuloIndice="2" ZeusLangCode="ITA"
                BindPzFromDB="true" IsEditMode="true" DefaultEditBetaImage='<%# Eval("Immagine3") %>'
                DefaultEditGammaImage='<%# Eval("Immagine4") %>' Enabled="false" 
                    ImageAlt='<%# Eval("Immagine34Alt") %>' OnDataBinding="ImageRaider_DataBinding" />
            <asp:Panel ID="ZMCF_Allegato" runat="server">
            </asp:Panel>
            <asp:Panel ID="ZMCF_BoxUrlRewrite" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="Label4" runat="server" Text="Page Link / URL (indirizzo pagina web)"></asp:Label></div>
                    <table>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label5" runat="server" Text="Url Rewrite" SkinID="FieldDescription"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:Label ID="InfoUrlLabel" runat="server" SkinID="FieldValue"></asp:Label>
                                <asp:Label ID="ZMCD_UrlDomain" runat="server" SkinID="FieldValue"></asp:Label><asp:Label
                                    ID="ZMCD_UrlSection" runat="server" SkinID="FieldValue"></asp:Label><asp:Label ID="ID1Pagina"
                                        runat="server" SkinID="FieldValue" Text='<%# Eval("ID1Categoria","{0}/") %>'></asp:Label><asp:Label
                                            ID="UrlPageDetailLabel" runat="server" Text='<%# Eval("UrlRewrite","{0}.aspx") %>'
                                            SkinID="FieldValue"></asp:Label>
                                <asp:HiddenField ID="UrlRewriteHiddenField" runat="server" Value='<%# Eval("UrlRewrite","{0}.aspx") %>' />
                                <asp:HiddenField ID="CopyHiddenField" runat="server" />
                                
                            </td>
                            <td>
                            <%--<input id="CopiaInput" value="Copia" type="button" onclick="ClipBoard(); return;"
                                    class="ZSSM_Button01_Button" />
                            </td>--%>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <asp:Panel ID="ZMCF_BoxMetaTags" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="Label9" runat="server" Text="SEO Search Engine Optimization"></asp:Label></div>
                    <table>
                                           <asp:Panel ID="ZMCF_TitoloBrowser" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label2" SkinID="FieldDescription" runat="server" Text="TITLE / Titolo Browser"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:Label ID="TitoloBrowserLabel" runat="server" Text='<%# Bind("TitoloBrowser") %>'
                                    SkinID="FieldValue"></asp:Label>
                            </td>
                        </tr>
                    </asp:Panel>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label13" runat="server" SkinID="FieldDescription" Text="Label">Description</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:Label ID="TagDescriptionLabel" runat="server" Text='<%# Eval("TagDescription") %>'
                                    SkinID="FieldValue"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label14" runat="server" SkinID="FieldDescription" Text="Label">Keywords</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:Label ID="TagKeywordsLabel" runat="server" Text='<%# Eval("TagKeywords") %>'
                                    SkinID="FieldValue"></asp:Label>
                            </td>
                        </tr>
                        <asp:Panel ID="ZMCF_TagMeta1" runat="server">
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label ID="ZML_TagMeta1" runat="server" SkinID="FieldDescription" Text="Label">Description</asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:Label ID="Label41" runat="server" Text='<%# Bind("TagMeta1Attribute","Attribute: {0} ") %>'
                                        SkinID="FieldValue"></asp:Label>
                                    <img src="../SiteImg/Spc.gif" width="10" />
                                    <asp:Label ID="TagMeta1ValueLabel" runat="server" Text='<%# Bind("TagMeta1Value","Value: {0} ") %>'
                                        SkinID="FieldValue"></asp:Label>
                                    <img src="../SiteImg/Spc.gif" width="10" />
                                    <asp:Label ID="TagMeta1ContentLabel" runat="server" Text='<%# Bind("TagMeta1Content","Content: {0}") %>'
                                        SkinID="FieldValue"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_TagMeta2" runat="server">
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label ID="ZML_TagMeta2" runat="server" SkinID="FieldDescription" Text="Label">Description</asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:Label ID="Label40" runat="server" Text='<%# Bind("TagMeta2Attribute","Attribute: {0} ") %>'
                                        SkinID="FieldValue"></asp:Label>
                                    <img src="../SiteImg/Spc.gif" width="10" />
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("TagMeta2Value","Value: {0} ") %>'
                                        SkinID="FieldValue"></asp:Label>
                                    <img src="../SiteImg/Spc.gif" width="10" />
                                    <asp:Label ID="Label39" runat="server" Text='<%# Bind("TagMeta2Content","Content: {0}") %>'
                                        SkinID="FieldValue"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                    </table>
                </div>
            </asp:Panel>
             <asp:Panel ID="ZMCF_BoxPhotoGallerySimple" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                       <A NAME="PHOTO"> <asp:Label ID="Label31" runat="server" Text="Fotografie aggiuntive"></asp:Label></A></div>
                   <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>
                                <uc3:PublisherPhoto ID="PublisherPhoto1" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <asp:Panel ID="ZMCF_BoxPlanner" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="PlannerLabel" runat="server" Text="Planner"></asp:Label>
                    </div>
                    <table>
                        <asp:Panel ID="ZMCF_PWArea1" runat="server">
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label ID="ZML_PWArea1_Dtl" runat="server" SkinID="FieldDescription" Text="Categoria principale"></asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:CheckBox ID="PW_Area1CheckBox" runat="server" Checked='<%# Eval("PW_Area1") %>'
                                        Enabled="false" />
                                    <asp:Label ID="PW_Area1LstLabel" runat="server" SkinID="FieldValue" Text="Attiva pubblicazione dalla data"></asp:Label>
                                    <asp:Label ID="PWI_Area1Label" runat="server" Text='<%# Eval("PWI_Area1", "{0:d}") %>'
                                        SkinID="FieldValue"></asp:Label>
                                    <asp:Label ID="AttivaA_Label" runat="server" SkinID="FieldValue" Text="alla data"></asp:Label>
                                    <asp:Label ID="PWF_Area1Label" runat="server" Text='<%# Eval("PWF_Area1", "{0:d}") %>'
                                        SkinID="FieldValue"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_PWArea2" runat="server" Visible="false">
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label ID="ZML_PWArea2_Dtl" runat="server" SkinID="FieldDescription" Text="Categoria secondaria"></asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:CheckBox ID="PW_Area2CheckBox" runat="server" Checked='<%# Eval("PW_Area2") %>'
                                        Enabled="false" />
                                    <asp:Label ID="PW_Area2LstLabel" runat="server" SkinID="FieldValue" Text="Attiva pubblicazione dalla data"></asp:Label>
                                    <asp:Label ID="PWI_Area2Label" runat="server" SkinID="FieldValue" Text='<%# Eval("PWI_Area2", "{0:d}") %>'></asp:Label>
                                    <asp:Label ID="AttivaA2_Label" runat="server" SkinID="FieldValue" Text="alla data"></asp:Label>
                                    <asp:Label ID="PWF_Area2Label" runat="server" SkinID="FieldValue" Text='<%# Eval("PWF_Area2", "{0:d}") %>'></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                    </table>
                </div>
            </asp:Panel>
            <div align="center">
                <asp:LinkButton ID="ModificaButton" SkinID="ZSSM_Button01" runat="server" Text="Modifica"
                    OnClick="ModificaButton_Click"></asp:LinkButton>
                <img src="../SiteImg/Spc.gif" width="20" />
                <asp:LinkButton ID="DeleteLinkButton" runat="server" Text="Elimina" SkinID="ZSSM_Button01"
                    OnClick="DeleteLinkButton_Click"></asp:LinkButton>
            </div>
            <div style="padding: 10px 0 0 0; margin: 0;">
                <dlc:zeusdatatracking ID="Zeusdatatracking1" runat="server" />
            </div>
            <asp:HiddenField ID="zdtRecordNewUser" runat="server" Value='<%# Eval("RecordNewUser") %>' />
            <asp:HiddenField ID="zdtRecordNewDate" runat="server" Value='<%# Eval("RecordNewDate") %>' />
            <asp:HiddenField ID="zdtRecordEdtUser" runat="server" Value='<%# Eval("RecordEdtUser") %>' />
            <asp:HiddenField ID="zdtRecordEdtDate" runat="server" Value='<%# Eval("RecordEdtDate") %>' />
        </ItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="CategorieSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SELECT [ID1Categoria], [ID2CategoriaParent], [Categoria], [Ordinamento], [DescBreve1], 
        [Descrizione1],[Colore1], [Immagine1], [Immagine2],[Immagine12Alt],[Immagine3], [Immagine4], [Immagine34Alt], 
        [PW_Area1], [PWI_Area1], [PWF_Area1], [PW_Area2], [PWI_Area2], [PWF_Area2],
         [RecordNewUser], [RecordNewDate],[RecordEdtUser], [RecordEdtDate] ,[ZeusUserEditable],[UrlRewrite],
         [TagDescription], [TagKeywords],TagMeta1Attribute,TagMeta2Attribute,TagMeta1Value,TagMeta1Content,
         TagMeta2Value,TagMeta2Content,TitoloBrowser,Descrizione2 
         FROM [tbCategorie] 
         WHERE ([ID1Categoria] = @ID1Categoria) AND ZeusIsAlive=1 AND ZeusIdModulo=@ZeusIdModulo AND ZeusLangCode=@ZeusLangCode"
        OnSelected="CategorieSqlDataSource_Selected">
        <SelectParameters>
            <asp:QueryStringParameter Name="ID1Categoria" QueryStringField="XRI" Type="Int32" />
            <asp:QueryStringParameter Name="ZeusIdModulo" QueryStringField="ZIM" Type="String" />
            <asp:QueryStringParameter Name="ZeusLangCode" QueryStringField="Lang" Type="String"
                DefaultValue="ITA" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
