﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.Security;
using System.Web.UI.WebControls;
/// <summary>
/// Descrizione di riepilogo per Categorie_Lst
/// </summary>
public partial class Categorie_Lst : System.Web.UI.Page
{
    static string TitoloPagina = "Elenco categorie";
    static int UP = 9;
    static int DOWN = 10;
    static int PWAREA1 = 12;
    static int PWAREA2 = 13;
    static int CATEGORIA = 7;

    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;

        delinea myDelinea = new delinea();

        if (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM"))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        ZIM.Value = Server.HtmlEncode(Request.QueryString["ZIM"]);

        if (!ReadXML(ZIM.Value, GetLang()))
            Response.Write("~/Zeus/System/Message.aspx?1");

        ReadXML_Localization(ZIM.Value, GetLang());

        Livello1HyperLink.NavigateUrl = "~/Zeus/Categorie/Categorie_Lst.aspx?ZIM=" + Request.QueryString["ZIM"] + "&Lang=" + Request.QueryString["Lang"] + "&XRI=1";
        Livello2HyperLink.NavigateUrl = "~/Zeus/Categorie/Categorie_Lst.aspx?ZIM=" + Request.QueryString["ZIM"] + "&Lang=" + Request.QueryString["Lang"] + "&XRI=2";
        Livello3HyperLink.NavigateUrl = "~/Zeus/Categorie/Categorie_Lst.aspx?ZIM=" + Request.QueryString["ZIM"] + "&Lang=" + Request.QueryString["Lang"] + "&XRI=3";
        TutteHyperLink.NavigateUrl = "~/Zeus/Categorie/Categorie_Lst.aspx?ZIM=" + Request.QueryString["ZIM"] + "&Lang=" + Request.QueryString["Lang"] + "&XRI=4";


        if ((myDelinea.AntiSQLInjectionRight(Request.QueryString, "XRI")) && (PopulateLinks(Request.QueryString["XRI"])))
            dsCategoria.SelectCommand += " AND Livello=" + Server.HtmlEncode(Request.QueryString["XRI"]);


        dsCategoria.SelectCommand += " ORDER BY OrdLiv1, OrdLiv2, OrdLiv3";

        //Response.Write("DEB " + dsCategoria.SelectCommand);


        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI1"))
            && (Request.QueryString["XRI1"].Length > 0)
            && (isOK()))
            SingleRecordPanel.Visible = true;


    }//fine Page__Load




    private bool isOK()
    {
        try
        {
            if ((Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/Categorie/Categorie_Edt.aspx") > 0)
                || (Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/Categorie/Categorie_New.aspx") > 0)
                || (Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/Categorie/Categorie_New_Lang.aspx") > 0))
                return true;
            else return false;
        }
        catch (Exception p)
        {
            return false;
        }

    }//fine isOK

    private bool PopulateLinks(string XRI)
    {
        try
        {
            switch (XRI)
            {
                case "1":
                    Livello1HyperLink.CssClass = "HyperlinkSkin_Filter_Selected";
                    break;

                case "2":
                    Livello2HyperLink.CssClass = "HyperlinkSkin_Filter_Selected";
                    break;

                case "3":
                    Livello3HyperLink.CssClass = "HyperlinkSkin_Filter_Selected";
                    break;

                default:
                    TutteHyperLink.CssClass = "HyperlinkSkin_Filter_Selected";
                    return false;
                    break;

            }//fine switch


            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }

    }//fine PopulateLinks


    private string GetLang()
    {
        string Lang = "ITA";

        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;

    }//fine GetLang



    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Categorie.xml"));


            TitleField.Value = mydoc.SelectSingleNode(myXPath + "ZML_TitoloPagina_Lst").InnerText;
            GridView1.Columns[CATEGORIA].HeaderText = GridView2.Columns[CATEGORIA].HeaderText = mydoc.SelectSingleNode(myXPath + "ZML_Categoria").InnerText;
            GridView1.Columns[PWAREA1].HeaderText = GridView2.Columns[PWAREA1].HeaderText = mydoc.SelectSingleNode(myXPath + "ZML_PWArea1_Lst").InnerText;
            GridView1.Columns[PWAREA2].HeaderText = GridView2.Columns[PWAREA2].HeaderText = mydoc.SelectSingleNode(myXPath + "ZML_PWArea2_Lst").InnerText;


        }
        catch (Exception)
        {
            //Response.Write(p.ToString());
        }

    }//fine ReadXML_Localization



    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_Categorie.xml"));


            if (!CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_Lst").InnerText))
                Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223599");



            GridView1.Columns[PWAREA1].Visible = GridView2.Columns[PWAREA1].Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_PWArea1").InnerText);

            GridView1.Columns[PWAREA2].Visible = GridView2.Columns[PWAREA2].Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_PWArea2").InnerText);


            ZMCF_PermitLevel1.Value = mydoc.SelectSingleNode(myXPath + "ZMCF_PermitLevel1").InnerText;
            ZMCF_PermitLevel2.Value = mydoc.SelectSingleNode(myXPath + "ZMCF_PermitLevel2").InnerText;
            ZMCF_PermitLevel3.Value = mydoc.SelectSingleNode(myXPath + "ZMCF_PermitLevel3").InnerText;


            return true;
        }
        catch (Exception p)
        {

            Response.Write(p.ToString());
            return false;
        }

    }//fine ReadXML


    private bool CheckPermit(string AllRoles)
    {

        bool IsPermit = false;

        try
        {

            if (AllRoles.Length == 0)
                return false;

            char[] delimiter = { ',' };


            foreach (string roles in Roles.GetRolesForUser(Membership.GetUser().UserName))
            {
                string[] PressRoles = AllRoles.Split(delimiter);

                for (int i = 0; i < PressRoles.Length; ++i)
                {
                    if (PressRoles[i].Trim().Equals(roles.Trim()))
                    {
                        IsPermit = true;
                        break;
                    }
                }//fine for

            }//fine else
        }
        catch (Exception)
        { }

        return IsPermit;

    }//fine CheckPermit

    //FUNZIONI PER L'ORDINAMENTO  

    private bool Move(string MoveCommand, string Ordinamento, string ID2CategoriaParent, string ID1CategoriaNodeToMove)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;
        List<int> RecordToOrder = new List<int>();

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            SqlTransaction transaction = null;

            try
            {

                int intNewOrder = Convert.ToInt32(Ordinamento);
                int intOldOrder = intNewOrder;

                connection.Open();

                SqlCommand command = connection.CreateCommand();

                transaction = connection.BeginTransaction();
                command.Transaction = transaction;
                sqlQuery = @"SELECT ID1Categoria 
                            FROM tbCategorie 
                            WHERE ID2CategoriaParent=@ID2CategoriaParent AND ZeusIsAlive=1 AND ZeusLangCode=@ZeusLangCode 
                                AND ZeusIdModulo=@ZeusIdModulo 
                            ORDER BY Ordinamento ASC";

                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID2CategoriaParent", System.Data.SqlDbType.Int, 4);
                command.Parameters["@ID2CategoriaParent"].Value = ID2CategoriaParent;

                command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.NVarChar, 3);
                command.Parameters["@ZeusLangCode"].Value = Server.HtmlEncode(Request.QueryString["Lang"]);

                command.Parameters.Add("@ZeusIdModulo", System.Data.SqlDbType.NVarChar, 5);
                command.Parameters["@ZeusIdModulo"].Value = Server.HtmlEncode(Request.QueryString["ZIM"]);

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    RecordToOrder.Add(Convert.ToInt32(reader["ID1Categoria"]));
                }

                reader.Close();

                int j = 1;

                foreach (int i in RecordToOrder)
                {
                    command.Parameters.Clear();

                    sqlQuery = @"UPDATE tbCategorie 
                                SET Ordinamento = @Ordinamento
                                WHERE ID1Categoria = @ID1Categoria
                                ";

                    command.Parameters.Add("@ID1Categoria", System.Data.SqlDbType.Int);
                    command.Parameters["@ID1Categoria"].Value = i;
                    command.Parameters.Add("@Ordinamento", System.Data.SqlDbType.Int);
                    command.Parameters["@Ordinamento"].Value = j;
                    command.CommandText = sqlQuery;

                    command.ExecuteNonQuery();

                    j++;
                }

                sqlQuery = @"SELECT Ordinamento 
                            FROM tbCategorie 
                            WHERE ID1Categoria = @ID1Categoria
                            ";
                command.CommandText = sqlQuery;
                command.Parameters.Clear();

                command.Parameters.Add("@ID1Categoria", System.Data.SqlDbType.Int);
                command.Parameters["@ID1Categoria"].Value = ID1CategoriaNodeToMove;

                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    intNewOrder = Convert.ToInt32(reader["Ordinamento"]);
                }

                reader.Close();

                intOldOrder = intNewOrder;

                switch (MoveCommand)
                {
                    case "Up":
                        --intNewOrder;
                        break;

                    case "Down":
                        ++intNewOrder;
                        break;

                    default:
                        return false;
                        break;

                }

                if (intNewOrder >= j || intNewOrder < 1)
                    return true;

                sqlQuery = @"SELECT ID1Categoria 
                            FROM tbCategorie 
                            WHERE ID2CategoriaParent=@ID2CategoriaParent AND ZeusIsAlive=1 AND ZeusLangCode=@ZeusLangCode 
                                AND ZeusIdModulo=@ZeusIdModulo AND Ordinamento=@Ordinamento";

                command.Parameters.Clear();

                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID2CategoriaParent", System.Data.SqlDbType.Int, 4);
                command.Parameters["@ID2CategoriaParent"].Value = ID2CategoriaParent;

                command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.NVarChar, 3);
                command.Parameters["@ZeusLangCode"].Value = Server.HtmlEncode(Request.QueryString["Lang"]);

                command.Parameters.Add("@ZeusIdModulo", System.Data.SqlDbType.NVarChar, 5);
                command.Parameters["@ZeusIdModulo"].Value = Server.HtmlEncode(Request.QueryString["ZIM"]);

                command.Parameters.Add("@Ordinamento", System.Data.SqlDbType.Int, 4);
                command.Parameters["@Ordinamento"].Value = intNewOrder;

                reader = command.ExecuteReader();

                string ID1CategoriaNodeToSwitch = string.Empty;

                while (reader.Read())
                {
                    ID1CategoriaNodeToSwitch = reader.GetInt32(0).ToString();
                }

                reader.Close();

                if (ID1CategoriaNodeToSwitch.Equals(ID1CategoriaNodeToMove))
                {
                    transaction.Rollback();
                    return false;
                }

                sqlQuery = "UPDATE tbCategorie SET Ordinamento=@Ordinamento WHERE ID1Categoria=@ID1Categoria";
                command.CommandText = sqlQuery;
                command.Parameters.Clear();

                command.Parameters.Add("@Ordinamento", System.Data.SqlDbType.NVarChar, 4);
                command.Parameters["@Ordinamento"].Value = intNewOrder.ToString();

                command.Parameters.Add("@ID1Categoria", System.Data.SqlDbType.Int, 4);
                command.Parameters["@ID1Categoria"].Value = ID1CategoriaNodeToMove;

                command.ExecuteNonQuery();


                sqlQuery = "UPDATE tbCategorie SET Ordinamento=@Ordinamento WHERE ID1Categoria=@ID1Categoria";
                command.CommandText = sqlQuery;
                command.Parameters.Clear();

                command.Parameters.Add("@Ordinamento", System.Data.SqlDbType.NVarChar, 4);
                command.Parameters["@Ordinamento"].Value = intOldOrder.ToString();

                command.Parameters.Add("@ID1Categoria", System.Data.SqlDbType.Int, 4);
                command.Parameters["@ID1Categoria"].Value = ID1CategoriaNodeToSwitch;

                command.ExecuteNonQuery();
                transaction.Commit();
                return true;

            }
            catch (Exception p)
            {

                Response.Write(p.ToString());
                transaction.Rollback();
                return false;
            }

        }//fine Using

    }//fine Move


    private int getLastOrder(string ID2CategoriaParent)
    {

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT MAX(Ordinamento) FROM tbCategorie WHERE ID2CategoriaParent=@ID2CategoriaParent AND ZeusIsAlive=1 AND ZeusLangCode=@ZeusLangCode  AND ZeusIdModulo=@ZeusIdModulo";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID2CategoriaParent", System.Data.SqlDbType.Int, 4);
                command.Parameters["@ID2CategoriaParent"].Value = ID2CategoriaParent;

                command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.NVarChar, 3);
                command.Parameters["@ZeusLangCode"].Value = Server.HtmlEncode(Request.QueryString["Lang"]);

                command.Parameters.Add("@ZeusIdModulo", System.Data.SqlDbType.NVarChar, 5);
                command.Parameters["@ZeusIdModulo"].Value = Server.HtmlEncode(Request.QueryString["ZIM"]);

                SqlDataReader reader = command.ExecuteReader();

                int myReturn = -1;

                while (reader.Read())
                {
                    myReturn = reader.GetInt32(0);

                }//fine while

                reader.Close();
                return myReturn;
            }
            catch (Exception p)
            {

                Response.Write(p.ToString());
                return -1;
            }
        }//fine Using

    }//fine getLastOrder

    //FINE FUNZIONI PER ORDINAMENTO  







    //FUNZIONI PER LA GRIDVIEW CHE ULIZZANO L'ORDINAMENTO
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int index = Convert.ToInt32(e.CommandArgument);
        GridViewRow row = GridView1.Rows[index];
        Label ID1Menu = (Label)row.FindControl("ID1Menu");
        Label ID1MenuParent = (Label)row.FindControl("ID1MenuParent");
        Label Ordinamento = (Label)row.FindControl("Ordinamento");

        Move(e.CommandName.ToString(), Ordinamento.Text, ID1MenuParent.Text, ID1Menu.Text);
        GridView1.DataBind();


    }//fine GridView1_RowCommand

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {

            if (e.Row.DataItemIndex > -1)
            {
                Label Ordinamento = (Label)e.Row.FindControl("Ordinamento");
                Label ID1MenuParent = (Label)e.Row.FindControl("ID1MenuParent");
                int ordinamentoRow = Convert.ToInt32(Ordinamento.Text);

                int maxordine = getLastOrder(ID1MenuParent.Text);

                if (ordinamentoRow == 1)
                    e.Row.Cells[UP].Text = string.Empty;

                if (ordinamentoRow == maxordine)
                    e.Row.Cells[DOWN].Text = string.Empty;



                //immagini attivo
                Image Image1 = (Image)e.Row.FindControl("Image1");
                Label AttivoArea1 = (Label)e.Row.FindControl("AttivoArea1");

                int intAttivoArea1 = Convert.ToInt32(AttivoArea1.Text.ToString());

                if (intAttivoArea1 == 1)
                    Image1.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";

                Image Image2 = (Image)e.Row.FindControl("Image2");
                Label AttivoArea2 = (Label)e.Row.FindControl("AttivoArea2");

                int intAttivoArea2 = Convert.ToInt32(AttivoArea2.Text.ToString());

                if (intAttivoArea2 == 1)
                    Image2.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";

                Image Image0 = (Image)e.Row.FindControl("Image0");
                Label AttivoZeus = (Label)e.Row.FindControl("AttivoZeus1");

                bool boolAttivoZeus = Convert.ToBoolean(AttivoZeus.Text.ToString());

                if (boolAttivoZeus)
                    Image0.ImageUrl = "~/Zeus/SiteImg/Ico1_Flag_On.gif";


                if (e.Row.Cells[UP].Controls.Count > 0)
                {
                    e.Row.Cells[UP].Attributes.Add("onmouseover", "showImageUP('" + e.Row.Cells[UP].Controls[0].ClientID + "','2');");
                    e.Row.Cells[UP].Attributes.Add("onmouseout", "showImageUP('" + e.Row.Cells[UP].Controls[0].ClientID + "','1');");
                }

                if (e.Row.Cells[DOWN].Controls.Count > 0)
                {
                    e.Row.Cells[DOWN].Attributes.Add("onmouseover", "showImageDOWN('" + e.Row.Cells[DOWN].Controls[0].ClientID + "','2');");
                    e.Row.Cells[DOWN].Attributes.Add("onmouseout", "showImageDOWN('" + e.Row.Cells[DOWN].Controls[0].ClientID + "','1');");
                }


                Label Livello = (Label)e.Row.FindControl("Livello");



                switch (Livello.Text)
                {
                    case "1":
                        e.Row.Cells[15].Enabled = Convert.ToBoolean(ZMCF_PermitLevel1.Value);
                        break;

                    case "2":
                        e.Row.Cells[15].Enabled = Convert.ToBoolean(ZMCF_PermitLevel2.Value);
                        break;

                    case "3":
                        e.Row.Cells[15].Enabled = Convert.ToBoolean(ZMCF_PermitLevel3.Value);
                        break;
                }




            }//fine if
        }
        catch (Exception p)
        {

            Response.Write(p.ToString());
        }

    }//fine GridView1_RowDataBound
}