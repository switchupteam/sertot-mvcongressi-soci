﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="Categorie_Lst.aspx.cs"
    Inherits="Categorie_Lst"
    Theme="Zeus" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">

    <script language="javascript" type="text/javascript">
            function showImageUP(imgId,typ) {
                var objImg = document.getElementById(imgId);
                if (objImg) {
                    if (typ == "1")
                        objImg.src = "../SiteImg/Bt3_ArrowUp_Off.gif";
                   else if (typ == "2")
                       objImg.src = "../SiteImg/Bt3_ArrowUp_On.gif";
               }
           }
           
           function showImageDOWN(imgId,typ) {
                var objImg = document.getElementById(imgId);
                if (objImg) {
                    if (typ == "1")
                        objImg.src = "../SiteImg/Bt3_ArrowDown_Off.gif";
                   else if (typ == "2")
                       objImg.src = "../SiteImg/Bt3_ArrowDown_On.gif";
               }
           }
    </script>

    <asp:HiddenField ID="TitleField" runat="server" />
    <asp:HiddenField ID="ZIM" runat="server" />
    <asp:HiddenField ID="ZMCF_PermitLevel1" runat="server" />
    <asp:HiddenField ID="ZMCF_PermitLevel2" runat="server" />
    <asp:HiddenField ID="ZMCF_PermitLevel3" runat="server" />
    
    <asp:Panel ID="SingleRecordPanel" runat="server" Visible="false">
        <div class="LayGridTitolo1">
            <asp:Label ID="CaptionNewRecordLabel" SkinID="GridTitolo1" runat="server" Text="Ultimo contenuto creato / aggiornato"></asp:Label></div>
        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" DataSourceID="SingleRecordSqlDataSource"
            OnRowDataBound="GridView1_RowDataBound"  CellPadding="0">
            <Columns>
                <asp:TemplateField HeaderText="AttivoZeus1" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="AttivoZeus1" runat="server" Text='<%# Eval("PW_Zeus1") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="AttivoArea1" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="AttivoArea1" runat="server" Text='<%# Eval("AttivoArea1") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="AttivoArea2" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="AttivoArea2" runat="server" Text='<%# Eval("AttivoArea2") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ID1Menu" SortExpression="ID1Menu" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="ID1Menu" runat="server" Text='<%# Eval("ID1Categoria") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ID2MenuParent" SortExpression="ID2MenuParent" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="ID1MenuParent" runat="server" Text='<%# Eval("ID2CategoriaParent") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Ordinamento" SortExpression="Ordinamento" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="Ordinamento" runat="server" Text='<%# Eval("Ordinamento") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="CatLiv1Liv2Liv3" HeaderText="Percorso" SortExpression="CatLiv1Liv2Liv3" />
                <asp:BoundField DataField="Categoria" HeaderText="Categoria" SortExpression="Categoria" />
                <asp:TemplateField HeaderText="Livello" SortExpression="Livello">
                    <ItemTemplate>
                        <asp:Label ID="Livello" runat="server" Text='<%# Eval("Livello") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>
                <%--  <asp:BoundField DataField="Livello" HeaderText="Livello" SortExpression="Livello"
                ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />--%>
                <asp:ButtonField CommandName="Up" Visible="false" HeaderText="Ord." ShowHeader="True" ImageUrl="~/Zeus/SiteImg/Bt3_ArrowUp_Off.gif"
                    ButtonType="Image">
                    <ItemStyle HorizontalAlign="Center" />
                    <ControlStyle CssClass="GridView_Ord1" />
                </asp:ButtonField>
                <asp:ButtonField CommandName="Down" Visible="false" HeaderText="Ord." ShowHeader="True" ButtonType="Image"
                    ImageUrl="~/Zeus/SiteImg/Bt3_ArrowDown_Off.gif">
                    <ItemStyle HorizontalAlign="Center" />
                    <ControlStyle CssClass="GridView_Ord1" />
                </asp:ButtonField>
                <asp:TemplateField HeaderText="Pub. Zeus" SortExpression="PW_Zeus1" >
                    <ItemTemplate>
                        <asp:Image ID="Image0" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Flag_Off.gif" /></ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Pub.CNS" SortExpression="AttivoArea1">
                    <ItemTemplate>
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" /></ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Pub.BSN  " SortExpression="AttivoArea2">
                    <ItemTemplate>
                        <asp:Image ID="Image2" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" /></ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>
                <asp:HyperLinkField DataNavigateUrlFields="ID1Categoria,Livello,ZeusIdModulo,ZeusLangCode"
                    DataNavigateUrlFormatString="Categorie_Dtl.aspx?XRI={0}&XRI1={1}&ZIM={2}&Lang={3}"
                    HeaderText="Dettaglio" Text="Dettaglio" >
                    <ControlStyle CssClass="GridView_ZeusButton1" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:HyperLinkField>
                <asp:HyperLinkField DataNavigateUrlFields="ID1Categoria,Livello,ZeusIdModulo,ZeusLangCode"
                    DataNavigateUrlFormatString="Categorie_Edt.aspx?XRI={0}&XRI1={1}&ZIM={2}&Lang={3}"
                    HeaderText="Modifica" Text="Modifica" >
                    <ControlStyle CssClass="GridView_ZeusButton1" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:HyperLinkField>
            </Columns>
            <EmptyDataTemplate>
                Dati non presenti
                <br />
                <br />
                <br />
                <br />
                <br />
            </EmptyDataTemplate>
            <EmptyDataRowStyle BackColor="White" Width="500px" BorderColor="Red" BorderWidth="0px"
                Height="200px" />
        </asp:GridView>
        <asp:SqlDataSource ID="SingleRecordSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
            SelectCommand="SELECT DISTINCT [ID1Categoria],[Ordinamento], [ID2CategoriaParent], 
        [Categoria], [ZeusIdModulo], [ZeusLangCode], [CatLiv1Liv2Liv3], [Ordinamento], 
        [AttivoArea1], [AttivoArea2], [PW_Zeus1],[Livello],[ZeusUserEditable],OrdLiv1, OrdLiv2, OrdLiv3 
        FROM [vwCategorie_All] 
        WHERE ID1Categoria=@ID1Categoria ">
            <SelectParameters>
                <asp:QueryStringParameter DefaultValue="0" Name="ID1Categoria" QueryStringField="XRI1"
                    Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <div class="VertSpacerMedium">
        </div>
        <br />
    </asp:Panel>
    <div class="LayGridTitolo1">
        <asp:Label ID="InfoLabel" runat="server" Text="Visualizza categorie " SkinID="GridFilter"></asp:Label>
        <asp:HyperLink ID="Livello1HyperLink" runat="server" Text="[Livello 1]" SkinID="GridFilter"></asp:HyperLink>
        <asp:HyperLink ID="Livello2HyperLink" runat="server" Text="[Livello 2]" SkinID="GridFilter"></asp:HyperLink>
        <asp:HyperLink ID="Livello3HyperLink" runat="server" Text="[Livello 3]" SkinID="GridFilter"></asp:HyperLink>
        <asp:HyperLink ID="TutteHyperLink" runat="server" Text="[Tutte]" SkinID="GridFilter"></asp:HyperLink>
    </div>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="dsCategoria"
        OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand" CellPadding="0">
        <Columns>
            <asp:TemplateField HeaderText="AttivoZeus1" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="AttivoZeus1" runat="server" Text='<%# Eval("PW_Zeus1") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="AttivoArea1" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="AttivoArea1" runat="server" Text='<%# Eval("AttivoArea1") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="AttivoArea2" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="AttivoArea2" runat="server" Text='<%# Eval("AttivoArea2") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ID1Menu" SortExpression="ID1Menu" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="ID1Menu" runat="server" Text='<%# Eval("ID1Categoria") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ID2MenuParent" SortExpression="ID2MenuParent" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="ID1MenuParent" runat="server" Text='<%# Eval("ID2CategoriaParent") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Ordinamento" SortExpression="Ordinamento" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="Ordinamento" runat="server" Text='<%# Eval("Ordinamento") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="CatLiv1Liv2Liv3" HeaderText="Percorso" SortExpression="CatLiv1Liv2Liv3" />
            <asp:BoundField DataField="Categoria" HeaderText="Categoria" SortExpression="Categoria" />
            <asp:TemplateField HeaderText="Livello" SortExpression="Livello">
                <ItemTemplate>
                    <asp:Label ID="Livello" runat="server" Text='<%# Eval("Livello") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <%--  <asp:BoundField DataField="Livello" HeaderText="Livello" SortExpression="Livello"
                ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />--%>
            <asp:ButtonField CommandName="Up" HeaderText="Ord." ShowHeader="True" ImageUrl="~/Zeus/SiteImg/Bt3_ArrowUp_Off.gif"
                ButtonType="Image">
                <ItemStyle HorizontalAlign="Center" />
                <ControlStyle CssClass="GridView_Ord1" />
            </asp:ButtonField>
            <asp:ButtonField CommandName="Down" HeaderText="Ord." ShowHeader="True" ButtonType="Image"
                ImageUrl="~/Zeus/SiteImg/Bt3_ArrowDown_Off.gif">
                <ItemStyle HorizontalAlign="Center" />
                <ControlStyle CssClass="GridView_Ord1" />
            </asp:ButtonField>
            <asp:TemplateField HeaderText="Pub. Zeus" SortExpression="PW_Zeus1">
                <ItemTemplate>
                    <asp:Image ID="Image0" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Flag_Off.gif" /></ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Pub.CNS" SortExpression="AttivoArea1">
                <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" /></ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Pub.BSN  " SortExpression="AttivoArea2">
                <ItemTemplate>
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" /></ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:HyperLinkField DataNavigateUrlFields="ID1Categoria,Livello,ZeusIdModulo,ZeusLangCode"
                DataNavigateUrlFormatString="Categorie_Dtl.aspx?XRI={0}&XRI1={1}&ZIM={2}&Lang={3}"
                HeaderText="Dettaglio" Text="Dettaglio" >
                <ControlStyle CssClass="GridView_ZeusButton1" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:HyperLinkField>
            <asp:HyperLinkField DataNavigateUrlFields="ID1Categoria,Livello,ZeusIdModulo,ZeusLangCode"
                DataNavigateUrlFormatString="Categorie_Edt.aspx?XRI={0}&XRI1={1}&ZIM={2}&Lang={3}"
                HeaderText="Modifica" Text="Modifica" >
                <ControlStyle CssClass="GridView_ZeusButton1" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:HyperLinkField>
        </Columns>
        <EmptyDataTemplate>
            Dati non presenti
            <br />
            <br />
            <br />
            <br />
            <br />
        </EmptyDataTemplate>
        <EmptyDataRowStyle BackColor="White" Width="500px" BorderColor="Red" BorderWidth="0px"
            Height="200px" />
    </asp:GridView>
    <asp:SqlDataSource ID="dsCategoria" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SELECT DISTINCT [ID1Categoria],[Ordinamento], [ID2CategoriaParent], 
        [Categoria], [ZeusIdModulo], [ZeusLangCode], [CatLiv1Liv2Liv3], [Ordinamento], 
        [AttivoArea1], [AttivoArea2], [PW_Zeus1],[Livello],[ZeusUserEditable],OrdLiv1, OrdLiv2, OrdLiv3 
        FROM [vwCategorie_All] 
        WHERE (([ZeusIdModulo] = @ZeusIdModulo) AND ([ZeusLangCode] = @ZeusLangCode)) ">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="0" Name="ZeusIdModulo" QueryStringField="ZIM"
                Type="String" />
            <asp:QueryStringParameter DefaultValue="ITA" Name="ZeusLangCode" QueryStringField="Lang"
                Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
