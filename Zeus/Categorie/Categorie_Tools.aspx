﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">
    
    static string TitoloPagina = "Controllo d'integrità voci Categoria";

    static int LIVELLI = 3;


    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;

    }//fine Page_Load


    private bool CheckIntegrityR(string ZeusIdModulo, string ZeusLangCode)
    {

        InfoLabel.Text+= "ZeusIdModulo:" + ZeusIdModulo + " ZeusLangCode:" + ZeusLangCode+"<br />";
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            SqlTransaction transaction = null;

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                transaction = connection.BeginTransaction();
                command.Transaction = transaction;



                ArrayList myArray = new ArrayList();
                ArrayList ID2CategoriaParentArrayList = new ArrayList();

                ID2CategoriaParentArrayList.Add("0");

                string[][] myCategoriaChange = null;
                string[] ID2CategoriaParent = null;

                int myOrder = 1;

                SqlDataReader reader = null;


                for (int index = 1; index <= LIVELLI; ++index)
                {


                    ID2CategoriaParent = (string[])ID2CategoriaParentArrayList.ToArray(typeof(string));
                    ID2CategoriaParentArrayList = new ArrayList();

                    for (int i = 0; i < ID2CategoriaParent.Length; ++i)
                    {

                        sqlQuery = "SELECT ID1Categoria,Categoria,Ordinamento FROM vwCategorie_All WHERE ZeusLangCode=@ZeusLangCode AND ZeusIdModulo=@ZeusIdModulo AND ID2CategoriaParent=@ID2CategoriaParent ORDER BY Ordinamento,ID1Categoria";
                        command.CommandText = sqlQuery;
                        command.Parameters.Clear();

                        command.Parameters.Add("@ZeusIdModulo", System.Data.SqlDbType.NVarChar, 5);
                        command.Parameters["@ZeusIdModulo"].Value = ZeusIdModulo;

                        command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.NVarChar, 3);
                        command.Parameters["@ZeusLangCode"].Value = ZeusLangCode;

                        command.Parameters.Add("@ID2CategoriaParent", System.Data.SqlDbType.Int, 4);
                        command.Parameters["@ID2CategoriaParent"].Value = ID2CategoriaParent[i];

                        reader = command.ExecuteReader();

                        myOrder = 1;

                        while (reader.Read())
                        {


                            ID2CategoriaParentArrayList.Add(reader.GetInt32(0).ToString());

                            if (myOrder != reader.GetInt32(2))
                            {

                                string[] myData = new string[3];
                                myData[0] = reader.GetInt32(0).ToString();
                                myData[1] = reader.GetString(1);
                                myData[2] = myOrder.ToString();
                                myArray.Add(myData);
                            }

                            ++myOrder;
                        }//fine while

                        reader.Close();

                    }//fine for


                    myCategoriaChange = (string[][])myArray.ToArray(typeof(string[]));
                    myArray = new ArrayList();

                    InfoLabel.Text += "----------------------- LIVELLO " + index + " ----------------------------------<br />";

                    for (int i = 0; i < myCategoriaChange.Length; ++i)
                    {

                        sqlQuery = "UPDATE tbCategorie SET Ordinamento=@Ordinamento WHERE ID1Categoria=@ID1Categoria";
                        command.CommandText = sqlQuery;
                        command.Parameters.Clear();

                        command.Parameters.Add("@Ordinamento", System.Data.SqlDbType.NVarChar, 4);
                        command.Parameters["@Ordinamento"].Value = myCategoriaChange[i][2];

                        command.Parameters.Add("@ID1Categoria", System.Data.SqlDbType.Int, 4);
                        command.Parameters["@ID1Categoria"].Value = myCategoriaChange[i][0];

                        command.ExecuteNonQuery();

                        InfoLabel.Text += "Modificata la voce menù : \"" + myCategoriaChange[i][1] + "\"<br/>";
                        InfoLabel.Text += "Nuova posizione in ordinamento: " + myCategoriaChange[i][2] + "<br /><br />";

                    }//fine for




                }//fine for LIVELLI
                transaction.Commit();

                InfoLabel.Text += "<br /><br />";
                return true;
            }
            catch (Exception p)
            {

                InfoLabel.Text = p.ToString();
                transaction.Rollback();
                return false;
            }
        }//fine using
    }//fine CheckIntegrityR



    private bool ForAllModulesAndLang()
    {
        try
        {

            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            string sqlQuery = string.Empty;

            ArrayList myArray = new ArrayList();
            
            using (SqlConnection connection = new SqlConnection(
               strConnessione))
            {

                SqlTransaction transaction = null;


                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT ZeusIDModulo,ZeusLangCode FROM tbModuli WHERE PW_Zeus1=1";
                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();

               
                while (reader.Read())
                {
                    string[] myData = new string[2];
                    myData[0] = reader.GetString(0);
                    myData[1] = reader.GetString(1);
                    myArray.Add(myData);
                    
                }//fine while

                reader.Close();
                
            }//fine using

            string[][] myModuli = (string[][])myArray.ToArray(typeof(string[]));

            InfoLabel.Text = string.Empty;
               
            for (int i = 0; i < myModuli.Length; ++i)
                CheckIntegrityR(myModuli[i][0], myModuli[i][1]);

            InfoLabel.Text += "Controllo integrità concluso con successo";
            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }

    }//fine ForAllModulesAndLang


    protected void CheckIntegrityButton_Click(object sender, EventArgs e)
    {
        InfoLabel.Text = "CONTROLLO IN CORSO.....";
        ForAllModulesAndLang();
        
        
        //CheckIntegrityR(Server.HtmlEncode(Request.QueryString["ZIM"]), Server.HtmlEncode(Request.QueryString["Lang"]));

    }//fine CheckIntegrityButton_Click
    
    
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <div class="BlockBox">
        <div class="BlockBoxHeader">
            <asp:Label ID="Dettaglio_contenuto" runat="server" Text="Controllo integrità categorie"></asp:Label></div>
        <table>
            <tr>
                <td class="BlockBoxDescription">
                </td>
                <td class="BlockBoxValue">
                    <asp:Label ID="InfoLabel" runat="server" SkinID="FieldValue"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="BlockBoxDescription">
                <asp:Button ID="CheckIntegrityButton" runat="server" OnClick="CheckIntegrityButton_Click"
                        SkinID="ZSSM_Button01" Text="Avvia controllo integrità" />
                </td>
                <td class="BlockBoxValue">
                    
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
