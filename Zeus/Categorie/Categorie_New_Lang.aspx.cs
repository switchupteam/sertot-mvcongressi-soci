﻿using ASP;
using System;
using System.Data.SqlClient;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per Categorie_New
/// </summary>
public partial class Categorie_New_Lang : System.Web.UI.Page
{      
    static string TitoloPagina = "Nuova categoria";

    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();
        TitleField.Value = TitoloPagina;

        if ((!myDelinea.AntiSQLInjectionNew(Request.QueryString, "ZIM", "string"))
            || (!myDelinea.AntiSQLInjectionNew(Request.QueryString, "ZID", "uid"))
            || (!myDelinea.AntiSQLInjectionNew(Request.QueryString, "LangO", "string")))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        ZIM.Value = Server.HtmlEncode(Request.QueryString["ZIM"]);
        ZID.Value = Server.HtmlEncode(Request.QueryString["ZID"]);
        Lang.Value = GetLang();
        LangO.Value = Server.HtmlEncode(Request.QueryString["LangO"]);

        GetDataFromLangO();

        if (!ReadXML(ZIM.Value, GetLang()))
            Response.Write("~/Zeus/System/Message.aspx?1");

        ReadXML_Localization(ZIM.Value, GetLang());

        try
        {
            string myJavascript = BothRequired1();
            myJavascript += " " + BothRequired2();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartUpScript", myJavascript, true);
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
        
        LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");
        InsertButton.Attributes.Add("onclick", "Validate()");

    }
 
    private string BothRequired1()
    {
        string ClientID1 = FormView1.FindControl("TagMeta1ValueTextBox").ClientID;
        string ClientID2 = FormView1.FindControl("TagMeta1ContentTextBox").ClientID;

        string myJavascript = " function BothRequired1(sender ,args) ";
        myJavascript += " { ";
        myJavascript += " if(document.getElementById('" + ClientID1 + "').value.length>0) ";
        myJavascript += " if(document.getElementById('" + ClientID2 + "').value.length==0)";
        myJavascript += " { ";
        myJavascript += " args.IsValid=false; ";
        myJavascript += " return; ";
        myJavascript += " } ";

        myJavascript += " if(document.getElementById('" + ClientID2 + "').value.length>0) ";
        myJavascript += " if(document.getElementById('" + ClientID1 + "').value.length==0)";
        myJavascript += " { ";
        myJavascript += " args.IsValid=false; ";
        myJavascript += " return; ";
        myJavascript += " } ";
        myJavascript += " return; ";
        myJavascript += " } ";

        return myJavascript;
    }

    private string BothRequired2()
    {
        string ClientID1 = FormView1.FindControl("TagMeta2ValueTextBox").ClientID;
        string ClientID2 = FormView1.FindControl("TagMeta2ContentTextBox").ClientID;

        string myJavascript = " function BothRequired2(sender ,args) ";
        myJavascript += " { ";
        myJavascript += " if(document.getElementById('" + ClientID1 + "').value.length>0) ";
        myJavascript += " if(document.getElementById('" + ClientID2 + "').value.length==0)";
        myJavascript += " { ";
        myJavascript += " args.IsValid=false; ";
        myJavascript += " return; ";
        myJavascript += " } ";

        myJavascript += " if(document.getElementById('" + ClientID2 + "').value.length>0) ";
        myJavascript += " if(document.getElementById('" + ClientID1 + "').value.length==0)";
        myJavascript += " { ";
        myJavascript += " args.IsValid=false; ";
        myJavascript += " return; ";
        myJavascript += " } ";
        myJavascript += " return; ";
        myJavascript += " } ";

        return myJavascript;
    }

    private string GetLang()
    {
        string Lang = "ITA";

        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;

    }

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Categorie.xml"));


            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {

                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {

                            if (childNode.Name.Equals("ZML_TitoloPagina_New"))
                            {
                                TitleField.Value = childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                            }

                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }

    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_Categorie.xml"));



            if (!CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_New").InnerText))
                Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223599");


            Panel ZMCF_TitoloBrowser = (Panel)FormView1.FindControl("ZMCF_TitoloBrowser");
           
            if (ZMCF_TitoloBrowser != null)
                ZMCF_TitoloBrowser.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_TitoloBrowser").InnerText);


            Panel ZMCF_DescBreve1 = (Panel)FormView1.FindControl("ZMCF_DescBreve1");

            if (ZMCF_DescBreve1 != null)
                ZMCF_DescBreve1.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_DescBreve1").InnerText);
            



            Panel ZMCF_Descrizione1 = (Panel)FormView1.FindControl("ZMCF_Descrizione1");
            
            if (ZMCF_Descrizione1 != null)
                ZMCF_Descrizione1.Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Descrizione1").InnerText);


            Panel ZMCF_Descrizione2 = (Panel)FormView1.FindControl("ZMCF_Descrizione2");

            if (ZMCF_Descrizione2 != null)
                ZMCF_Descrizione2.Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Descrizione2").InnerText);
                        
                        
            Panel ZMCF_BoxColore = (Panel)FormView1.FindControl("ZMCF_BoxColore");

            if (ZMCF_BoxColore != null)
                ZMCF_BoxColore.Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxColore").InnerText);
                        
                        

            Panel ZMCF_BoxImmagine1 = (Panel)FormView1.FindControl("ZMCF_BoxImmagine1");

            if (ZMCF_BoxImmagine1 != null)
                ZMCF_BoxImmagine1.Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxImmagine1").InnerText);

            Panel ZMCF_BoxImmagine2 = (Panel)FormView1.FindControl("ZMCF_BoxImmagine2");

            if (ZMCF_BoxImmagine2 != null)
                ZMCF_BoxImmagine2.Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxImmagine2").InnerText);           
            
                        
            Panel ZMCF_BoxPlanner = (Panel)FormView1.FindControl("ZMCF_BoxPlanner");

            if (ZMCF_BoxPlanner != null)
                ZMCF_BoxPlanner.Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxPlanner").InnerText);


            Panel ZMCF_PWArea1 = (Panel)FormView1.FindControl("ZMCF_PWArea1");

            if (ZMCF_PWArea1 != null)
                ZMCF_PWArea1.Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_PWArea1").InnerText);


            Panel ZMCF_PWArea2 = (Panel)FormView1.FindControl("ZMCF_PWArea2");

            if (ZMCF_PWArea2 != null)
                ZMCF_PWArea2.Visible =
                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_PWArea2").InnerText);

            if (!Page.IsPostBack)
                BindCategorieDropDownList(Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_PermitLevel1").InnerText),
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_PermitLevel2").InnerText),
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_PermitLevel3").InnerText));
                                             
           Panel ZMCF_BoxMetaTags = (Panel)FormView1.FindControl("ZMCF_BoxMetaTags");

           if (ZMCF_BoxMetaTags != null)
               ZMCF_BoxMetaTags.Visible =
                       Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxMetaTags").InnerText);
                       
                       
           Panel ZMCF_TagMeta1 = (Panel)FormView1.FindControl("ZMCF_TagMeta1");

           if (ZMCF_TagMeta1 != null)
               ZMCF_TagMeta1.Visible =
                       Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_TagMeta1").InnerText);
                       
                       
           Panel ZMCF_TagMeta2 = (Panel)FormView1.FindControl("ZMCF_TagMeta2");

           if (ZMCF_TagMeta2 != null)
               ZMCF_TagMeta2.Visible =
                       Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_TagMeta2").InnerText);

           TextBox UrlPageDetailTextBox = (TextBox)FormView1.FindControl("UrlPageDetailTextBox");

           if (UrlPageDetailTextBox != null)
               UrlPageDetailTextBox.Visible = false;

           Label InfoUrlRewritingLabel = (Label)FormView1.FindControl("InfoUrlRewritingLabel");

           if (InfoUrlRewritingLabel != null)
               InfoUrlRewritingLabel.Visible = false;
                        
            return true;
        }
        catch (Exception)
        {
            return false;
        }

    }

    private bool CheckPermit(string AllRoles)
    {

        bool IsPermit = false;

        try
        {
            if (AllRoles.Length == 0)
                return false;

            char[] delimiter = { ',' };


            foreach (string roles in Roles.GetRolesForUser(Membership.GetUser().UserName))
            {
                string[] PressRoles = AllRoles.Split(delimiter);

                for (int i = 0; i < PressRoles.Length; ++i)
                {
                    if (PressRoles[i].Trim().Equals(roles.Trim()))
                    {
                        IsPermit = true;
                        break;
                    }
                }
            }
        }
        catch (Exception)
        { }

        return IsPermit;

    }
    
    private bool BindCategorieDropDownList(bool PermitLevel1, bool PermitLevel2, bool PermitLevel3)
    {
        DropDownList CategoriaPadreDropDownList = (DropDownList)FormView1.FindControl("CategoriaPadreDropDownList");
        HiddenField ID2CategoriaParentHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaParentHiddenField");

        //ID2CategoriaParentHiddenField.Value = "0";

        //if ((!PermitLevel2) && (!PermitLevel3))
        //{
        //    CategoriaPadreDropDownList.Visible = false; 
        //    return false;
        //}

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {
            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                if (PermitLevel1)
                {
                    CategoriaPadreDropDownList.Items.Add(new ListItem("> Radice", "0"));
                    ID2CategoriaParentHiddenField.Value = "0";
                }

                if ((PermitLevel3) && (PermitLevel2))
                {
                    sqlQuery = "SELECT [ID1Categoria], [MenuLabel1] FROM [vwCategorie_Zeus1] WHERE ZeusIdModulo = @ZeusIdModulo AND ZeusLangCode = @ZeusLangCode";
                    sqlQuery += " AND (Livello=1 OR Livello=2)";
                    CategoriaPadreDropDownList.DataValueField = "ID1Categoria";
                    CategoriaPadreDropDownList.DataTextField = "MenuLabel1";
                }
                else if (PermitLevel2)
                {
                    sqlQuery = "SELECT [ID1Categoria], [MenuLabel1] FROM [vwCategorie_Zeus1] WHERE ZeusIdModulo = @ZeusIdModulo AND ZeusLangCode = @ZeusLangCode";
                    sqlQuery += " AND Livello=1";
                    CategoriaPadreDropDownList.DataValueField = "ID1Categoria";
                    CategoriaPadreDropDownList.DataTextField = "MenuLabel1";
                }
                else if (PermitLevel3)
                {
                    sqlQuery = "SELECT [ID1Categoria], [MenuLabel2] FROM [vwCategorie_Zeus1] WHERE ZeusIdModulo = @ZeusIdModulo AND ZeusLangCode = @ZeusLangCode";
                    sqlQuery += " AND Livello=2";
                    CategoriaPadreDropDownList.DataValueField = "ID1Categoria";
                    CategoriaPadreDropDownList.DataTextField = "MenuLabel2";

                }

                command.CommandText = sqlQuery;
                command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.NVarChar, 3);
                command.Parameters["@ZeusLangCode"].Value = Server.HtmlEncode(Request.QueryString["Lang"]);
                command.Parameters.Add("@ZeusIdModulo", System.Data.SqlDbType.NVarChar, 5);
                command.Parameters["@ZeusIdModulo"].Value = Server.HtmlEncode(Request.QueryString["ZIM"]);

                CategoriaPadreDropDownList.DataSource = command.ExecuteReader();
                CategoriaPadreDropDownList.DataBind();

                return true;
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
                return false;
            }
        }
    }

    protected void DataLimite(object sender, EventArgs e)
    {
        TextBox DataLimite = (TextBox)sender;
        if (DataLimite.Text.Length == 0)
        {
            DataLimite.Text = "31/12/2040";
        }
    }

    protected void DataIniziale(object sender, EventArgs e)
    {
        DateTime Data = new DateTime();
        Data = DateTime.Now;
        TextBox DataCreazione = (TextBox)sender;
        if (DataCreazione.Text.Length == 0)
        {
            DataCreazione.Text = Data.ToString("d");
        }
    }

    protected void UtenteCreazione(object sender, EventArgs e)
    {
        HiddenField UtenteCreazione = (HiddenField)sender;
        UtenteCreazione.Value = Membership.GetUser().ProviderUserKey.ToString();
    }

    protected void DataOggi(object sender, EventArgs e)
    {
        HiddenField DataCreazione = (HiddenField)sender;
        DataCreazione.Value = DateTime.Now.ToString();
    }

    protected void CreazioneGUID(object sender, EventArgs e)
    {
        HiddenField Guid = (HiddenField)sender;
        if (Guid.Value.Length == 0)
        {
            Guid.Value = System.Guid.NewGuid().ToString();
        }
    }

    private int getLastOrder(string ID2CategoriaParent)
    {

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT TOP 1 Ordinamento";
                sqlQuery += " FROM tbCategorie ";
                sqlQuery += "WHERE ID2CategoriaParent=@ID2CategoriaParent AND ZeusIdmodulo=@ZeusIdModulo AND ZeusIsAlive = 1 ORDER BY Ordinamento DESC";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID2CategoriaParent", System.Data.SqlDbType.Int);
                command.Parameters["@ID2CategoriaParent"].Value = ID2CategoriaParent;
                command.Parameters.Add("@ZeusIdModulo", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ZeusIdModulo"].Value = ZIM.Value;

                int myCount = Convert.ToInt32(command.ExecuteScalar());

                if (myCount != null)
                    return myCount;
                else return 1;
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
                return -1;
            }
        }                
    }


    protected void CategoriaPadreDropDownList_SelectIndexChange(object sender, EventArgs e)
    {
        HiddenField ID2CategoriaParentHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaParentHiddenField");
        DropDownList CategoriaPadreDropDownList = (DropDownList)sender;

        ID2CategoriaParentHiddenField.Value = CategoriaPadreDropDownList.SelectedValue;

    }


    protected void HiddenImageFile_DataBinding(object sender, EventArgs e)
    {
        HiddenField HiddenImageFile = (HiddenField)sender;
        if (HiddenImageFile.Value.Length == 0)
            HiddenImageFile.Value = "ImgNonDisponibile.jpg";

    }

    private string GetLivello(string ID1Categoria)
    {
        string Livello = string.Empty;
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            try
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT [ID2CategoriaParent]";
                sqlQuery += " FROM [vwCategorie_All]";
                sqlQuery += " WHERE [ID1Categoria]=" + Server.HtmlEncode(ID1Categoria);

                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                    if (!reader.IsDBNull(0))
                        Livello ="_" +reader.GetInt32(0).ToString();

                reader.Close();

            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
            }
        }//fine Using
        

        return Livello;

    }//fine GetLivello


    private string AddZero(string MyString)
    {
        switch (MyString.Length)
        {

            case 1:
                MyString = "000" + MyString;
                break;

            case 2:
                MyString = "00" + MyString;
                break;

            case 3:
                MyString = "0" + MyString;
                break;

        }//fine switch


        return MyString;

    }//AddZero


    private bool MakeAndSaveUrlRewrite(string Title, string ID1Categoria)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        delinea myDelinea = new delinea();
        string UrlRewrite = myDelinea.myHtmlEncode(Title);// +"_" + AddZero(ID1Categoria) + GetLivello(ID1Categoria) + ".aspx";


        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            SqlTransaction transaction = null;

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                transaction = connection.BeginTransaction();
                command.Transaction = transaction;

                sqlQuery = "UPDATE tbCategorie SET UrlRewrite='" + UrlRewrite + "' WHERE ID1Categoria=" + ID1Categoria;
                Response.Write("DEB " + sqlQuery + "<br />");
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();


                transaction.Commit();

                return true;
            }
            catch (Exception p)
            {

                Response.Write(p.ToString());
                transaction.Rollback();
                return false;
            }
        }//fine using

    }//fine MakeAndSaveUrlRewrite



    //##############################################################################################################
    //################################################ FILE UPLOAD #################################################
    //############################################################################################################## 

    protected void FileUploadCustomValidator_ServerValidate(object sender, ServerValidateEventArgs args)
    {
        try
        {
            ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");
            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");

            if (ImageRaider1.isValid())
                InsertButton.CommandName = "Insert";
            else
                InsertButton.CommandName = string.Empty;

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine FileUploadCustomValidator


    protected void FileUpload2CustomValidator_ServerValidate(object sender, ServerValidateEventArgs args)
    {
        try
        {
            ImageRaider ImageRaider2 = (ImageRaider)FormView1.FindControl("ImageRaider2");
            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");

            if (ImageRaider2.isValid())
                InsertButton.CommandName = "Insert";
            else
                InsertButton.CommandName = string.Empty;

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine FileUpload2CustomValidator

    //##############################################################################################################
    //########################################### FINE FILE UPLOAD #################################################
    //##############################################################################################################


    protected void InsertButton_Click(object sender, EventArgs e)
    {

        LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");

        HiddenField FileNameBetaHiddenField = (HiddenField)FormView1.FindControl("FileNameBetaHiddenField");
        HiddenField FileNameGammaHiddenField = (HiddenField)FormView1.FindControl("FileNameGammaHiddenField");
        HiddenField Immagine12AltHiddenField = (HiddenField)FormView1.FindControl("Immagine12AltHiddenField");
        HiddenField FileNameBeta2HiddenField = (HiddenField)FormView1.FindControl("FileNameBeta2HiddenField");
        HiddenField FileNameGamma2HiddenField = (HiddenField)FormView1.FindControl("FileNameGamma2HiddenField");
        HiddenField Immagine34AltHiddenField = (HiddenField)FormView1.FindControl("Immagine34AltHiddenField");
        ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");
        ImageRaider ImageRaider2 = (ImageRaider)FormView1.FindControl("ImageRaider2");


        if (InsertButton.CommandName != string.Empty)
        {
            HiddenField ID2CategoriaParentHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaParentHiddenField");
            HiddenField OrdinamentoHiddenField = (HiddenField)FormView1.FindControl("OrdinamentoHiddenField");

            int Order = getLastOrder(ID2CategoriaParentHiddenField.Value);
            ++Order;
            OrdinamentoHiddenField.Value = Order.ToString();


            if (ImageRaider1.GenerateBeta(string.Empty))
                FileNameBetaHiddenField.Value = ImageRaider1.ImgBeta_FileName;
            else FileNameBetaHiddenField.Value = ImageRaider1.DefaultBetaImage;

            if (ImageRaider1.GenerateGamma(string.Empty))
                FileNameGammaHiddenField.Value = ImageRaider1.ImgGamma_FileName;
            else FileNameGammaHiddenField.Value = ImageRaider1.DefaultGammaImage;

            if (ImageRaider2.GenerateBeta(string.Empty))
                FileNameBeta2HiddenField.Value = ImageRaider2.ImgBeta_FileName;
            else FileNameBeta2HiddenField.Value = ImageRaider2.DefaultBetaImage;

            if (ImageRaider2.GenerateGamma(string.Empty))
                FileNameGamma2HiddenField.Value = ImageRaider2.ImgGamma_FileName;
            else FileNameGamma2HiddenField.Value = ImageRaider2.DefaultGammaImage;

        }//fine if commandname
    }//fine OrdinamentoHiddenField_DataBinding


    protected void CategorieSqlDataSource_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {

        TextBox CategoriaTextBox = (TextBox)FormView1.FindControl("CategoriaTextBox");

        if ((e.Exception == null) && (MakeAndSaveUrlRewrite(CategoriaTextBox.Text, e.Command.Parameters["@XRI"].Value.ToString())))
            Response.Redirect("~/Zeus/Categorie/Categorie_Lst.aspx?ZIM=" + ZIM.Value
                + "&Lang=" + GetLang()
                + "&XRI1=" + e.Command.Parameters["@XRI"].Value.ToString());
        //else Response.Redirect("~/Zeus/System/Message.aspx?InsErr");
        else Response.Write(e.Exception.ToString());
    }//fine CategorieSqlDataSource_Inserted

    private bool GetDataFromLangO()
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        try
        {
            using (SqlConnection connection = new SqlConnection(strConnessione))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = @"SELECT tbCategorie.*,  vwCategorie_Zeus1.Categoria AS CatParent ";
                sqlQuery += "FROM tbCategorie LEFT OUTER JOIN vwCategorie_Zeus1 ON tbCategorie.ID2CategoriaParent = vwCategorie_Zeus1.ID1Categoria ";
                sqlQuery += "WHERE (tbCategorie.ZeusIsAlive = 1) ";
                sqlQuery += "AND tbCategorie.ZeusId=@ZeusId ";
                sqlQuery += "AND tbCategorie.ZeusLangCode=@ZeusLangCode";
                
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ZeusId", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@ZeusId"].Value = new Guid(ZID.Value);
                command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ZeusLangCode"].Value = LangO.Value;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (reader["Categoria"] != DBNull.Value)
                    {
                        Label TitoloLangLabel = (Label)FormView1.FindControl("TitoloLangLabel");
                        TitoloLangLabel.Text = reader["Categoria"].ToString();
                    }

                    if (reader["CatParent"] != DBNull.Value)
                    {
                        Label CategoriaLangLabel = (Label)FormView1.FindControl("CategoriaLangLabel");
                        CategoriaLangLabel.Text = reader["CatParent"].ToString();
                    }
                    else if (Convert.ToInt32(reader["ID2CategoriaParent"]) == 0)
                    {
                        Label CategoriaLangLabel = (Label)FormView1.FindControl("CategoriaLangLabel");
                        CategoriaLangLabel.Text = "Radice";
                    }
                    //if (reader["TitoloBrowser"] != DBNull.Value)
                    //{
                    //    Label DescrizioneBreveLangLabel = (Label)FormView1.FindControl("TitoloBrowserLangLabel");
                    //    DescrizioneBreveLangLabel.Text = reader["TitoloBrowser"].ToString();
                    //}

                    if (reader["Descrizione1"] != DBNull.Value)
                    {
                        Label Contenuto1LangLabel = (Label)FormView1.FindControl("ContenutoPrincipaleLangLabel");
                        Contenuto1LangLabel.Text = reader["Descrizione1"].ToString();
                    }

                    //if (reader["Contenuto2"] != DBNull.Value)
                    //{
                    //    Label Contenuto2LangLabel = (Label)FormView1.FindControl("Contenuto2LangLabel");
                    //    Contenuto2LangLabel.Text = reader["Contenuto2"].ToString();
                    //}

                    //if (reader["Contenuto3"] != DBNull.Value)
                    //{
                    //    Label Contenuto3LangLabel = (Label)FormView1.FindControl("Contenuto3LangLabel");
                    //    Contenuto3LangLabel.Text = reader["Contenuto3"].ToString();
                    //}

                    //if (reader["Autore"] != DBNull.Value)
                    //{
                    //    TextBox AutoreTextBox = (TextBox)FormView1.FindControl("AutoreTextBox");
                    //    AutoreTextBox.Text = reader["Autore"].ToString();
                    //}

                    //if (reader["Autore_Email"] != DBNull.Value)
                    //{
                    //    TextBox Autore_EmailTextBox = (TextBox)FormView1.FindControl("Autore_EmailTextBox");
                    //    Autore_EmailTextBox.Text = reader["Autore_Email"].ToString();
                    //}

                    //if (reader["Fonte"] != DBNull.Value)
                    //{
                    //    TextBox FonteTextBox = (TextBox)FormView1.FindControl("FonteTextBox");
                    //    FonteTextBox.Text = reader["Fonte"].ToString();
                    //}

                    //if (reader["Fonte_Url"] != DBNull.Value)
                    //{
                    //    TextBox Fonte_UrlTextBox = (TextBox)FormView1.FindControl("Fonte_UrlTextBox");
                    //    Fonte_UrlTextBox.Text = reader["Fonte_Url"].ToString();
                    //}

                    //if (reader["TitoloBrowser"] != DBNull.Value)
                    //{
                    //    Label TitoloBrowserLangLabel = (Label)FormView1.FindControl("TitoloBrowserLangLabel");
                    //    TitoloBrowserLangLabel.Text = reader["TitoloBrowser"].ToString();
                    //}

                    if (reader["Immagine1"] != DBNull.Value)
                    {
                        HiddenField FileNameBetaHiddenField = (HiddenField)FormView1.FindControl("FileNameBetaHiddenField");
                        FileNameBetaHiddenField.Value = reader["Immagine1"].ToString();
                        ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");
                        ImageRaider1.DefaultEditBetaImage = reader["Immagine1"].ToString();
                        ImageRaider1.SetDefaultEditBetaImage();
                        ImageRaider1.DefaultBetaImage = reader["Immagine1"].ToString();
                    }

                    //if (reader["Immagine2"] != DBNull.Value)
                    //{
                    //    HiddenField FileNameGammaHiddenField = (HiddenField)FormView1.FindControl("FileNameGammaHiddenField");
                    //    FileNameGammaHiddenField.Value = reader["Immagine2"].ToString();
                    //    ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");
                    //    ImageRaider1.DefaultEditGammaImage = reader["Immagine2"].ToString();
                    //    ImageRaider1.SetDefaultEditGammaImage();
                    //    ImageRaider1.DefaultGammaImage = reader["Immagine2"].ToString();
                    //}

                    //if (reader["Immagine3"] != DBNull.Value)
                    //{
                    //    HiddenField FileNameBeta2HiddenField = (HiddenField)FormView1.FindControl("FileNameBeta2HiddenField");
                    //    FileNameBeta2HiddenField.Value = reader["Immagine3"].ToString();
                    //    ImageRaider ImageRaider2 = (ImageRaider)FormView1.FindControl("ImageRaider2");
                    //    ImageRaider2.DefaultEditBetaImage = reader["Immagine3"].ToString();
                    //    ImageRaider2.SetDefaultEditBetaImage();
                    //    ImageRaider2.DefaultBetaImage = reader["Immagine3"].ToString();
                    //}

                    //if (reader["Immagine4"] != DBNull.Value)
                    //{
                    //    HiddenField FileNameGamma2HiddenField = (HiddenField)FormView1.FindControl("FileNameGamma2HiddenField");
                    //    FileNameGamma2HiddenField.Value = reader["Immagine4"].ToString();
                    //    ImageRaider ImageRaider2 = (ImageRaider)FormView1.FindControl("ImageRaider2");
                    //    ImageRaider2.DefaultEditGammaImage = reader["Immagine4"].ToString();
                    //    ImageRaider2.SetDefaultEditGammaImage();
                    //    ImageRaider2.DefaultGammaImage = reader["Immagine4"].ToString();
                    //}

                    HiddenField ZeusIdHidden = (HiddenField)FormView1.FindControl("ZeusIdHidden");
                    ZeusIdHidden.Value = ZID.Value;
                }

                reader.Close();
                return true;
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }
    protected void ImageRaider_DataBinding(object sender, EventArgs e)
    {
        ImageRaider ImageRaider1 = (ImageRaider)sender;
        ImageRaider1.SetDefaultEditBetaImage();
        ImageRaider1.SetDefaultEditGammaImage();

    }
}