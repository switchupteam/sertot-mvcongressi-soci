<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Theme="Zeus" %>

<%@ Register Src="~/Zeus/SiteAssets/HomePendingApprov.ascx" TagName="HomePendingApprov" TagPrefix="uc2" %>
<%@ Register Src="~/Zeus/SiteAssets/HomePendingPayment.ascx" TagName="HomePendingPayment" TagPrefix="uc2" %>


<script runat="server">   


    static string TitoloPagina = "Zeus Web Management System";


    protected void Page_Load(object sender, EventArgs e)
    {

        //CheckBrowser();

        TitleField.Value = TitoloPagina;
        //UserLabel.Text = Membership.GetNumberOfUsersOnline().ToString();

    }//fine Page_Load



    private void CheckBrowser()
    {
        if (Request.Browser.Browser.Equals("AppleMAC-Safari"))
            Response.Redirect("~/System/Message.aspx?Msg=00000012312321");
    }//fine CheckBrowser


</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
   <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <div class="MEX02B">
        <div class="MEX02B_text">Richieste di iscrizione in attesa di approvazione</div>
    </div>
    <uc2:HomePendingApprov ID="HomePendingApprov" runat="server" />
    <br />
</asp:Content>
