<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" %>

<%@ Import Namespace="System.Web.Security" %>
<%@ Import Namespace="System.Web.UI" %>
<%@ Import Namespace="System.Web.UI.WebControls" %>
<%@ Import Namespace="System.Web.UI.WebControls.WebParts" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">   
   
    static string TitoloPagina = "Reset password account utente";
    static int MAXLENPASSWORD = 8;


    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();
        TitleField.Value = TitoloPagina;

        if (!myDelinea.AntiSQLInjection(Request.QueryString))
            Response.Redirect("/System/Message.aspx"); if (!Page.IsPostBack)
            if (!isOK())
                Response.Redirect("~/Zeus/System/Message.aspx?Msg=7867868458426388&VfC=HKELP&Lang=ITA");

        try
        {
            Guid UID = new Guid(Request.QueryString["UID"]);
            UserNameLabel.Text = Membership.GetUser(UID).UserName.ToString();

            if (Roles.IsUserInRole(UserNameLabel.Text, "ZeusAdmin"))
            {
                Msg.Text = "Attenzione: non � possibile modificare la password di un account utente con il ruolo ZeusAdmin";
                PasswordPanel.Visible = false;
                Msg.Visible = true;
            }

        }
        catch (Exception)
        {
            CambiaPasswordCustomValidator.ErrorMessage = "Attenzione: l'utente non esiste!";
            CambiaPasswordCustomValidator.IsValid = false;
            TextBoxNewPass.Enabled = false;
            TextBoxConfNewPass.Enabled = false;

            CambiaPasswordLinkButton.Enabled = false;
        }

    }//fine Page_Load

    private bool isOK()
    {
        try
        {
            if (Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/Account/Account_Dtl.aspx") > 0
                || Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/Soci/Soci_Dsp.aspx") > 0)
                return true;
            else return false;
        }
        catch (Exception p)
        {
            return false;
        }

    }//fine isOK


    protected void CambiaPasswordLinkButton_Click(object sender, EventArgs e)
    {
        try
        {
            if (CambiaPasswordCustomValidator.IsValid)
            {
                Guid UID = new Guid(Request.QueryString["UID"]);
                MembershipUser user = Membership.GetUser(UID);
                user.ChangePassword(user.GetPassword().ToString(), TextBoxNewPass.Text.ToString());
                Membership.UpdateUser(user);
                Response.Redirect("Account_Lst.aspx?UID=" + Server.HtmlEncode(Request.QueryString["UID"]));
            }
        }
        catch (System.ArgumentException p)
        {

            CambiaPasswordCustomValidator.IsValid = false;

            if (Membership.MinRequiredNonAlphanumericCharacters > 0)
                CambiaPasswordCustomValidator.ErrorMessage = "Caratteri speciali richiesti: almeno " + Membership.MinRequiredNonAlphanumericCharacters;
        }

    }


    protected void CambiaPasswordCustomValidator_ServerValidate(object sender, ServerValidateEventArgs args)
    {
        CambiaPasswordCustomValidator.ErrorMessage = string.Empty;

        if (TextBoxNewPass.Text.Length < Membership.MinRequiredPasswordLength)
        {
            args.IsValid = false;
            CambiaPasswordCustomValidator.ErrorMessage = "Lunghezza minima password: " + Membership.MinRequiredPasswordLength + " caratteri alfanumerici";
        }
        if (!TextBoxNewPass.Text.Equals(TextBoxConfNewPass.Text))
        {
            args.IsValid = false;
            CambiaPasswordCustomValidator.ErrorMessage = "Password e Conferma password non coincidono";
        }

    }
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
       <asp:FormView ID="MemberShipFormView" runat="server" DefaultMode="ReadOnly" DataSourceID="MemberShipSqlDataSource"
        Width="100%" CellPadding="0">
        <ItemTemplate>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="AccountUtenteLabel" runat="server" Text="Account utente"></asp:Label></div>
                <table border="0" cellpadding="2" cellspacing="1">
                    <tr>
                        <td>
                            <asp:Label SkinID="FieldDescription" ID="UserName_Label" runat="server" Text="Label">UserName:</asp:Label>
                            <asp:Label SkinID="FieldValue" ID="UserNameLabel" runat="server" Text='<%# Eval("UserName", "{0}") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label SkinID="FieldDescription" ID="EMail_Label" runat="server" Text="Label">E-mail: </asp:Label>
                            <asp:HyperLink SkinID="FieldValue" ID="EmailHyperLink" CssClass="HyperLink1" runat="server"
                                NavigateUrl="mailto:" Text='<%# Eval("Email", "{0}") %>'></asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="DataCreazioneDesc_Label" SkinID="FieldDescription" runat="server"
                                Text="Label">Data creazione:</asp:Label>
                            <asp:Label ID="DataCreazioneLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("CreateDate", "{0}") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="UserIDDescr_Label" SkinID="FieldDescription" runat="server" Text="Label">User ID:</asp:Label>
                            <asp:Label ID="UserIDLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("Expr1", "{0}") %>'></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </ItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="MemberShipSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SELECT aspnet_Membership.Password, aspnet_Membership.Email
        , aspnet_Membership.IsApproved, aspnet_Membership.CreateDate, aspnet_Membership.LastLoginDate
        , aspnet_Membership.LastPasswordChangedDate
        , aspnet_Membership.LastLockoutDate, aspnet_Membership.FailedPasswordAttemptCount, aspnet_Users.UserName
        , aspnet_Users.UserId AS Expr1, aspnet_Membership.IsLockedOut FROM aspnet_Membership INNER JOIN aspnet_Users ON aspnet_Membership.UserId = aspnet_Users.UserId WHERE (aspnet_Users.UserId = @UserID)">
        <SelectParameters>
            <asp:QueryStringParameter Name="UserID" QueryStringField="UID" DefaultValue="0" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:Label ID="Msg" runat="server" Visible="false" skinID="Validazione01" ></asp:Label><br />
    <asp:Panel ID="PasswordPanel" runat="server">
    <div class="BlockBox">
        <div class="BlockBoxHeader">
            <asp:Label ID="Label1" runat="server" Text="Inserisci nuova password"></asp:Label></div>
        <table border="0" cellpadding="2" cellspacing="1">
            <tr>
                <td class="BlockBoxDescription">
                    <asp:Label SkinID="FieldDescription" ID="UserName_Label" runat="server" Text="UserName"></asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:Label SkinID="FieldValue" ID="UserNameLabel" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="BlockBoxDescription">
                    <asp:Label ID="NuovaPass_Label" SkinID="FieldDescription" runat="server" Text="Nuova password"></asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:TextBox ID="TextBoxNewPass" runat="server" Columns="50" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Obbligatorio"
                        ControlToValidate="TextBoxNewPass" SkinID="ZSSM_Validazione01" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="CambiaPasswordCustomValidator" runat="server" SkinID="ZSSM_Validazione01"
                        Display="Dynamic" OnServerValidate="CambiaPasswordCustomValidator_ServerValidate"></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="BlockBoxDescription">
                    <asp:Label ID="Conferma_Label" SkinID="FieldDescription" runat="server" Text="Conferma nuova password"></asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:TextBox ID="TextBoxConfNewPass" runat="server" Columns="50" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Obbligatorio"
                        ControlToValidate="TextBoxConfNewPass" SkinID="ZSSM_Validazione01" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
        </table>
    </div>
    <div align="center">
        <asp:LinkButton SkinID="ZSSM_Button01" ID="CambiaPasswordLinkButton" runat="server"
            OnClick="CambiaPasswordLinkButton_Click">Cambia password</asp:LinkButton></div>
            </asp:Panel>
</asp:Content>
