﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">


    static string TitoloPagina = "Nuovo account utente";
    delinea myDelinea = new delinea();


    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;



    }//fine Page_Load


    private string Generate(int ID, int SIZE)
    {
        string chars = "ABCDEFGHILMNOPQRSTUVZ1234567890abcdefghilmnopqrstuvzxykj";
        int i;
        string r = string.Empty;
        ID += Convert.ToInt32(DateTime.Now.Minute);

        Random rnd = new Random(ID);
        for (int j = 1; j <= SIZE; j++)
        {
            i = Convert.ToInt32(rnd.Next(0, chars.Length));
            r += chars.Substring(i, 1);
        }
        return r;
    }//fine GeneratePassword


    private string GenerateUserName(int ID, int SIZE)
    {
        string chars = "1234567890";
        int i;
        string r = string.Empty;
        ID += Convert.ToInt32(DateTime.Now.Minute);

        Random rnd = new Random(ID);
        for (int j = 1; j <= SIZE; j++)
        {
            i = Convert.ToInt32(rnd.Next(0, chars.Length));
            r += chars.Substring(i, 1);
        }
        return r;
    }//fine GenerateUserName

    private bool CreateUser()
    {


        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            SqlTransaction transaction = null;
            string UserName = NomeTextBox.Text.Replace(" ", "_") + "." + CognomeTextBox.Text.Replace(" ", "_");
            UserNameHiddenField.Value = UserName;

            try
            {


                string Password = Generate(Convert.ToInt32(DateTime.Now.Millisecond), 10) + "!";

                object UserGUID = Membership.CreateUser(UserName, Password, EmailTextBox.Text).ProviderUserKey;
                object CreatorGUID = Membership.GetUser().ProviderUserKey;

                UserGUIDHiddenField.Value = UserGUID.ToString();

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                transaction = connection.BeginTransaction();
                command.Transaction = transaction;

                sqlQuery = "SET DATEFORMAT dmy; INSERT INTO [tbProfiliPersonali]";
                sqlQuery += " ([UserId],[Cognome],[Nome],[CodiceFiscale],[Qualifica],[Professione]";
                sqlQuery += " ,[Sesso],[Indirizzo],[Numero],[Citta],[Comune],[Cap],[ProvinciaEstesa]";
                sqlQuery += " ,[ProvinciaSigla],[ID2Nazione],[NascitaCitta],[NascitaProvinciaSigla]";
                sqlQuery += " ,[NascitaID2Nazione],[Telefono1],[Telefono2],[Fax],[Note],[UserMaster],[RecordNewUser]";
                sqlQuery += " ,[RecordNewDate]";
                sqlQuery += " ,[ZeusJolly1],[ZeusJolly2],[ImportCode])";
                sqlQuery += " VALUES ";
                sqlQuery += " (@UserId,@Cognome,@Nome,@CodiceFiscale,@Qualifica,@Professione";
                sqlQuery += " ,@Sesso,@Indirizzo,@Numero,@Citta,@Comune,@Cap,@ProvinciaEstesa";
                sqlQuery += " ,@ProvinciaSigla,@ID2Nazione,@NascitaCitta,@NascitaProvinciaSigla";
                sqlQuery += " ,@NascitaID2Nazione,@Telefono1,@Telefono2,@Fax,@Note,@UserMaster,@RecordNewUser";
                sqlQuery += " ,@RecordNewDate";
                sqlQuery += " ,@ZeusJolly1,@ZeusJolly2,@ImportCode)";

                command.CommandText = sqlQuery;
                command.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@UserId"].Value = UserGUID;

                command.Parameters.Add("@Cognome", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Cognome"].Value = CognomeTextBox.Text;

                command.Parameters.Add("@Nome", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Nome"].Value = NomeTextBox.Text;

                command.Parameters.Add("@CodiceFiscale", System.Data.SqlDbType.NVarChar);
                command.Parameters["@CodiceFiscale"].Value = CodiceFiscaleTextBox.Text;

                command.Parameters.Add("@Qualifica", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Qualifica"].Value = QualificaTextBox.Text;

                command.Parameters.Add("@Professione", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Professione"].Value = ProfessioneTextBox.Text;

                command.Parameters.Add("@Sesso", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Sesso"].Value = SessoDropDownlist.SelectedValue;

                command.Parameters.Add("@Indirizzo", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Indirizzo"].Value = string.Empty;

                command.Parameters.Add("@Numero", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Numero"].Value = string.Empty;

                command.Parameters.Add("@Citta", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Citta"].Value = string.Empty;

                command.Parameters.Add("@Comune", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Comune"].Value = string.Empty;

                command.Parameters.Add("@Cap", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Cap"].Value = string.Empty;


                command.Parameters.Add("@ProvinciaEstesa", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ProvinciaEstesa"].Value = string.Empty;

                command.Parameters.Add("@ProvinciaSigla", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ProvinciaSigla"].Value = string.Empty;

                command.Parameters.Add("@ID2Nazione", System.Data.SqlDbType.Int);
                command.Parameters["@ID2Nazione"].Value = 1;

                //command.Parameters.Add("@NascitaData",System.Data.SqlDbType.SmallDateTime);
                //command.Parameters["@NascitaData"].Value = NascitaData;

                command.Parameters.Add("@NascitaCitta", System.Data.SqlDbType.NVarChar);
                command.Parameters["@NascitaCitta"].Value = string.Empty;

                command.Parameters.Add("@NascitaProvinciaSigla", System.Data.SqlDbType.NVarChar);
                command.Parameters["@NascitaProvinciaSigla"].Value = string.Empty;

                command.Parameters.Add("@NascitaID2Nazione", System.Data.SqlDbType.Int);
                command.Parameters["@NascitaID2Nazione"].Value = 1;

                command.Parameters.Add("@Telefono1", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Telefono1"].Value = TelefonoCasaTextBox.Text;

                command.Parameters.Add("@Telefono2", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Telefono2"].Value = TelefonoCellulareTextBox.Text;

                command.Parameters.Add("@Fax", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Fax"].Value = FaxTextBox.Text;

                command.Parameters.Add("@Note", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Note"].Value = string.Empty;

                command.Parameters.Add("@UserMaster", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@UserMaster"].Value = CreatorGUID;

                command.Parameters.Add("@RecordNewUser", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@RecordNewUser"].Value = CreatorGUID;

                command.Parameters.Add("@RecordNewDate", System.Data.SqlDbType.SmallDateTime);
                command.Parameters["@RecordNewDate"].Value = DateTime.Now;

                command.Parameters.Add("@ZeusJolly1", System.Data.SqlDbType.Int);
                command.Parameters["@ZeusJolly1"].Value = 0;

                command.Parameters.Add("@ZeusJolly2", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ZeusJolly2"].Value = string.Empty;

                command.Parameters.Add("@ImportCode", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ImportCode"].Value = "FAST_ACCOUNT";


                command.ExecuteNonQuery();

                command.Parameters.Clear();
                sqlQuery = "SET DATEFORMAT dmy; INSERT INTO [tbProfiliSocietari]";
                sqlQuery += " ([UserId],[ID2BusinessBook],[RagioneSociale],[PartitaIVA],[S1_Indirizzo]";
                sqlQuery += " ,[S1_Numero],[S1_Citta],[S1_Comune],[S1_Cap],[S1_ProvinciaEstesa],[S1_ProvinciaSigla]";
                sqlQuery += " ,[S1_ID2Nazione],[S2_Indirizzo],[S2_Numero],[S2_Citta],[S2_Comune],[S2_Cap],[S2_ProvinciaEstesa]";
                sqlQuery += " ,[S2_ProvinciaSigla],[S2_ID2Nazione],[Telefono1],[Telefono2],[Fax],[WebSite]";
                sqlQuery += " ,[RecordNewUser],[RecordNewDate],[ZeusJolly1],[ZeusJolly2],[ImportCode])";
                sqlQuery += " VALUES ";
                sqlQuery += " (@UserId,@ID2BusinessBook,@RagioneSociale,@PartitaIVA,@S1_Indirizzo";
                sqlQuery += " ,@S1_Numero,@S1_Citta,@S1_Comune,@S1_Cap,@S1_ProvinciaEstesa,@S1_ProvinciaSigla";
                sqlQuery += " ,@S1_ID2Nazione,@S2_Indirizzo,@S2_Numero,@S2_Citta,@S2_Comune,@S2_Cap,@S2_ProvinciaEstesa";
                sqlQuery += " ,@S2_ProvinciaSigla,@S2_ID2Nazione,@Telefono1,@Telefono2,@Fax,@WebSite";
                sqlQuery += " ,@RecordNewUser,@RecordNewDate,@ZeusJolly1,@ZeusJolly2,@ImportCode)";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@UserId"].Value = UserGUID;

                command.Parameters.Add("@ID2BusinessBook", System.Data.SqlDbType.Int);
                command.Parameters["@ID2BusinessBook"].Value = 0;

                command.Parameters.Add("@RagioneSociale", System.Data.SqlDbType.NVarChar);
                command.Parameters["@RagioneSociale"].Value = RagioneSocialeTextBox.Text;

                command.Parameters.Add("@PartitaIVA", System.Data.SqlDbType.NVarChar);
                command.Parameters["@PartitaIVA"].Value = string.Empty;

                command.Parameters.Add("@S1_Indirizzo", System.Data.SqlDbType.NVarChar);
                command.Parameters["@S1_Indirizzo"].Value = string.Empty;

                command.Parameters.Add("@S1_Numero", System.Data.SqlDbType.NVarChar);
                command.Parameters["@S1_Numero"].Value = string.Empty;

                command.Parameters.Add("@S1_Citta", System.Data.SqlDbType.NVarChar);
                command.Parameters["@S1_Citta"].Value = string.Empty;

                command.Parameters.Add("@S1_Comune", System.Data.SqlDbType.NVarChar);
                command.Parameters["@S1_Comune"].Value = string.Empty;

                command.Parameters.Add("@S1_Cap", System.Data.SqlDbType.NVarChar);
                command.Parameters["@S1_Cap"].Value = string.Empty;

                command.Parameters.Add("@S1_ProvinciaEstesa", System.Data.SqlDbType.NVarChar);
                command.Parameters["@S1_ProvinciaEstesa"].Value = string.Empty;

                command.Parameters.Add("@S1_ProvinciaSigla", System.Data.SqlDbType.NVarChar);
                command.Parameters["@S1_ProvinciaSigla"].Value = string.Empty;

                command.Parameters.Add("@S1_ID2Nazione", System.Data.SqlDbType.Int);
                command.Parameters["@S1_ID2Nazione"].Value = 1;


                command.Parameters.Add("@S2_Indirizzo", System.Data.SqlDbType.NVarChar);
                command.Parameters["@S2_Indirizzo"].Value = string.Empty;

                command.Parameters.Add("@S2_Numero", System.Data.SqlDbType.NVarChar);
                command.Parameters["@S2_Numero"].Value = string.Empty;

                command.Parameters.Add("@S2_Citta", System.Data.SqlDbType.NVarChar);
                command.Parameters["@S2_Citta"].Value = string.Empty;

                command.Parameters.Add("@S2_Comune", System.Data.SqlDbType.NVarChar);
                command.Parameters["@S2_Comune"].Value = string.Empty;

                command.Parameters.Add("@S2_Cap", System.Data.SqlDbType.NVarChar);
                command.Parameters["@S2_Cap"].Value = string.Empty;

                command.Parameters.Add("@S2_ProvinciaEstesa", System.Data.SqlDbType.NVarChar);
                command.Parameters["@S2_ProvinciaEstesa"].Value = string.Empty;

                command.Parameters.Add("@S2_ProvinciaSigla", System.Data.SqlDbType.NVarChar);
                command.Parameters["@S2_ProvinciaSigla"].Value = string.Empty;

                command.Parameters.Add("@S2_ID2Nazione", System.Data.SqlDbType.Int);
                command.Parameters["@S2_ID2Nazione"].Value = 1;

                command.Parameters.Add("@Telefono1", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Telefono1"].Value = string.Empty;

                command.Parameters.Add("@Telefono2", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Telefono2"].Value = string.Empty;

                command.Parameters.Add("@Fax", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Fax"].Value = string.Empty;

                command.Parameters.Add("@WebSite", System.Data.SqlDbType.NVarChar);
                command.Parameters["@WebSite"].Value = string.Empty;

                command.Parameters.Add("@RecordNewUser", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@RecordNewUser"].Value = CreatorGUID;

                command.Parameters.Add("@RecordNewDate", System.Data.SqlDbType.SmallDateTime);
                command.Parameters["@RecordNewDate"].Value = DateTime.Now;

                command.Parameters.Add("@ZeusJolly1", System.Data.SqlDbType.Int);
                command.Parameters["@ZeusJolly1"].Value = 0;

                command.Parameters.Add("@ZeusJolly2", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ZeusJolly2"].Value = string.Empty;

                command.Parameters.Add("@ImportCode", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ImportCode"].Value = "FAST_ACCOUNT";
                command.ExecuteNonQuery();


                command.Parameters.Clear();
                sqlQuery = "SET DATEFORMAT dmy; INSERT INTO [tbProfiliMarketing]";
                sqlQuery += " ([UserId],[ID2Categoria1],[ConsensoDatiPersonali],[ConsensoDatiPersonali2],[Comunicazioni1]";
                sqlQuery += ",[Comunicazioni2],[Comunicazioni3],[Comunicazioni4],[Comunicazioni5],[Comunicazioni6]";
                sqlQuery += ",[Comunicazioni7],[Comunicazioni8],[Comunicazioni9],[Comunicazioni10],[RecordNewUser]";
                sqlQuery += ",[RecordNewDate],[ZeusJolly1],[ZeusJolly2],[ImportCode],[RegCausal])";
                sqlQuery += " VALUES ";
                sqlQuery += " (@UserId,@ID2Categoria1,@ConsensoDatiPersonali,@ConsensoDatiPersonali2,@Comunicazioni1";
                sqlQuery += " ,@Comunicazioni2,@Comunicazioni3,@Comunicazioni4,@Comunicazioni5,@Comunicazioni6";
                sqlQuery += " ,@Comunicazioni7,@Comunicazioni8,@Comunicazioni9,@Comunicazioni10,@RecordNewUser";
                sqlQuery += " ,@RecordNewDate,@ZeusJolly1,@ZeusJolly2,@ImportCode,'IMA')";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@UserId"].Value = UserGUID;

                command.Parameters.Add("@ID2Categoria1", System.Data.SqlDbType.Int);
                command.Parameters["@ID2Categoria1"].Value = 0;

                command.Parameters.Add("@ConsensoDatiPersonali", System.Data.SqlDbType.Bit);
                command.Parameters["@ConsensoDatiPersonali"].Value = true;

                command.Parameters.Add("@ConsensoDatiPersonali2", System.Data.SqlDbType.Bit);
                command.Parameters["@ConsensoDatiPersonali2"].Value = true;

                command.Parameters.Add("@Comunicazioni1", System.Data.SqlDbType.Bit);
                command.Parameters["@Comunicazioni1"].Value = true;

                command.Parameters.Add("@Comunicazioni2", System.Data.SqlDbType.Bit);
                command.Parameters["@Comunicazioni2"].Value = true;

                command.Parameters.Add("@Comunicazioni3", System.Data.SqlDbType.Bit);
                command.Parameters["@Comunicazioni3"].Value = true;

                command.Parameters.Add("@Comunicazioni4", System.Data.SqlDbType.Bit);
                command.Parameters["@Comunicazioni4"].Value = true;

                command.Parameters.Add("@Comunicazioni5", System.Data.SqlDbType.Bit);
                command.Parameters["@Comunicazioni5"].Value = true;

                command.Parameters.Add("@Comunicazioni6", System.Data.SqlDbType.Bit);
                command.Parameters["@Comunicazioni6"].Value = true;

                command.Parameters.Add("@Comunicazioni7", System.Data.SqlDbType.Bit);
                command.Parameters["@Comunicazioni7"].Value = true;

                command.Parameters.Add("@Comunicazioni8", System.Data.SqlDbType.Bit);
                command.Parameters["@Comunicazioni8"].Value = true;

                command.Parameters.Add("@Comunicazioni9", System.Data.SqlDbType.Bit);
                command.Parameters["@Comunicazioni9"].Value = true;

                command.Parameters.Add("@Comunicazioni10", System.Data.SqlDbType.Bit);
                command.Parameters["@Comunicazioni10"].Value = true;

                command.Parameters.Add("@RecordNewUser", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@RecordNewUser"].Value = CreatorGUID;

                command.Parameters.Add("@RecordNewDate", System.Data.SqlDbType.SmallDateTime);
                command.Parameters["@RecordNewDate"].Value = DateTime.Now;

                command.Parameters.Add("@ZeusJolly1", System.Data.SqlDbType.Int);
                command.Parameters["@ZeusJolly1"].Value = 0;

                command.Parameters.Add("@ZeusJolly2", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ZeusJolly2"].Value = string.Empty;


                command.Parameters.Add("@ImportCode", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ImportCode"].Value = "FAST_ACCOUNT";
                command.ExecuteNonQuery();

                transaction.Commit();

                return true;
            }
            catch (Exception p)
            {



                if (transaction != null)
                {
                    transaction.Rollback();
                    Membership.DeleteUser(UserName);
                }
                return false;
            }
        }//fine using


        return true;


    }//fine CreateUser


    protected void CreateUserButton_Click(object sender, EventArgs e)
    {

        if (CreateUser())
        {
            MessaggioLabel.Text = "L'utente " + UserNameHiddenField.Value + " è stato creato correttamente.<br />";
            DettaglioHyperLink.NavigateUrl = "~/Zeus/Account/Account_Dtl.aspx?UID=" + UserGUIDHiddenField.Value;
            STEP1_Panel.Visible = false;
            STEP2_Panel.Visible = true;
            
        }


    }//fine CreateUserButton_Click
    
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" />
    <asp:HiddenField ID="UserNameHiddenField" runat="server" />
    <asp:HiddenField ID="UserGUIDHiddenField" runat="server" />
    
    <asp:Panel ID="STEP1_Panel" runat="server">
        <div class="BlockBox">
            <div class="BlockBoxHeader">
                <asp:Label ID="UtenteLabel" runat="server">Utente</asp:Label></div>
            <table>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="CognomeLabel" runat="server" SkinID="FieldDescription" Text="Label">Cognome *</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="CognomeTextBox" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator SkinID="ZSSM_Validazione01" Display="Dynamic" ID="RequiredFieldValidator1"
                            runat="server" ControlToValidate="CognomeTextBox" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label SkinID="FieldDescription" ID="Nome_Label" runat="server" Text="Nome *"></asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="NomeTextBox" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator SkinID="ZSSM_Validazione01" Display="Dynamic" ID="RequiredFieldValidator2"
                            runat="server" ControlToValidate="NomeTextBox" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label SkinID="FieldDescription" ID="Qualifica_Label" runat="server" Text="Qualifica"></asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="QualificaTextBox" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label SkinID="FieldDescription" ID="Professione_Label" runat="server" Text="Professione"></asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="ProfessioneTextBox" runat="server" Columns="20" MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label SkinID="FieldDescription" ID="SessoLabel" runat="server" Text="Sesso"></asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:DropDownList ID="SessoDropDownlist" runat="server">
                            <asp:ListItem Selected="True" Text="Non inserito" Value="X"></asp:ListItem>
                            <asp:ListItem Text="Maschio" Value="M"></asp:ListItem>
                            <asp:ListItem Text="Femmina" Value="F"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label SkinID="FieldDescription" ID="CodiceFiscale_Label" runat="server" Text="Codice fiscale personale"></asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="CodiceFiscaleTextBox" runat="server" Columns="16" MaxLength="16"></asp:TextBox>
                        <%--<asp:RegularExpressionValidator SkinID="ZSSM_Validazione01" Display="Dynamic" ID="CodiceFiscaleRegularExpressionValidator"
                            runat="server" ControlToValidate="CodiceFiscaleTextBox" ErrorMessage="Codice fiscale non corretto"
                            ValidationExpression="^[A-Za-z]{6}\d{2}[A-Za-z]\d{2}[A-Za-z]\d{3}[A-Za-z]$"></asp:RegularExpressionValidator>--%>
                    </td>
                </tr>
            </table>
        </div>
        <div class="BlockBox">
            <div class="BlockBoxHeader">
                <asp:Label ID="Contatti2_Label" runat="server" Text="Contatti"></asp:Label></div>
            <table>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label SkinID="FieldDescription" ID="Telefono2_Label" runat="server" Text="Telefono"></asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TelefonoCasaTextBox" runat="server" Columns="20" MaxLength="20"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label SkinID="FieldDescription" ID="Cell_Label" runat="server" Text="Telefono cellulare"></asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TelefonoCellulareTextBox" runat="server" Columns="20" MaxLength="20"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label SkinID="FieldDescription" ID="Fax_Label" runat="server" Text="Fax"></asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="FaxTextBox" runat="server" Columns="20" MaxLength="20"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>
        <div class="BlockBox">
            <div class="BlockBoxHeader">
                <asp:Label ID="Azienda_Label" runat="server" Text="Azienda / ente / società"></asp:Label></div>
            <table>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label SkinID="FieldDescription" ID="RagioneSoc_Label" runat="server" Text="Ragione sociale"></asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="RagioneSocialeTextBox" runat="server" Columns="80" MaxLength="100"></asp:TextBox>
                    </td>
                </tr>
                <asp:Panel ID="PZV_BusinessBookPanel" runat="server">
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label SkinID="FieldDescription" ID="AziendaCollegata_Label" runat="server" Text="Azienda collegata"></asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:DropDownList ID="AziendaCollegataDropDownList" runat="server" DataSourceID="AziendaCollegataSqlDataSource"
                                DataTextField="Identificativo" DataValueField="ID1BsnBook" AppendDataBoundItems="true">
                                <asp:ListItem Text="&gt; Non inserita" Value="0" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="AziendaCollegataSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                SelectCommand="SELECT [ID1BsnBook],[Identificativo]
                                FROM [tbBusinessBook]
                                WHERE [PW_Zeus1]=1 AND ZeusIsAlive=1 ORDER BY Identificativo"></asp:SqlDataSource>
                        </td>
                    </tr>
                </asp:Panel>
            </table>
        </div>
        <div class="BlockBox">
            <div class="BlockBoxHeader">
                <asp:Label ID="Step4_Label" runat="server" Text="E-Mail"></asp:Label>
            </div>
            <table>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label SkinID="FieldDescription" ID="EmailLabel" runat="server">Posta elettronica *</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="EmailTextBox" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator Display="Dynamic" SkinID="ZSSM_Validazione01" ID="EmailRequired"
                            runat="server" ControlToValidate="EmailTextBox" ToolTip="Indirizzo E-Mail non corretto"
                            ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="MailRegulaeExpressionValidation" runat="server"
                            Display="Dynamic" ValidationExpression="^((([a-z]|[0-9]|!|#|$|%|&|'|\*|\+|\-|/|=|\?|\^|_|`|\{|\||\}|~)+(\.([a-z]|[0-9]|!|#|$|%|&|'|\*|\+|\-|/|=|\?|\^|_|`|\{|\||\}|~)+)*)@((((([a-z]|[0-9])([a-z]|[0-9]|\-){0,61}([a-z]|[0-9])\.))*([a-z]|[0-9])([a-z]|[0-9]|\-){0,61}([a-z]|[0-9])\.(af|ax|al|dz|as|ad|ao|ai|aq|ag|ar|am|aw|au|at|az|bs|bh|bd|bb|by|be|bz|bj|bm|bt|bo|ba|bw|bv|br|io|bn|bg|bf|bi|kh|cm|ca|cv|ky|cf|td|cl|cn|cx|cc|co|km|cg|cd|ck|cr|ci|hr|cu|cy|cz|dk|dj|dm|do|ec|eg|sv|gq|er|ee|et|fk|fo|fj|fi|fr|gf|pf|tf|ga|gm|ge|de|gh|gi|gr|gl|gd|gp|gu|gt| gg|gn|gw|gy|ht|hm|va|hn|hk|hu|is|in|id|ir|iq|ie|im|il|it|jm|jp|je|jo|kz|ke|ki|kp|kr|kw|kg|la|lv|lb|ls|lr|ly|li|lt|lu|mo|mk|mg|mw|my|mv|ml|mt|mh|mq|mr|mu|yt|mx|fm|md|mc|mn|ms|ma|mz|mm|na|nr|np|nl|an|nc|nz|ni|ne|ng|nu|nf|mp|no|om|pk|pw|ps|pa|pg|py|pe|ph|pn|pl|pt|pr|qa|re|ro|ru|rw|sh|kn|lc|pm|vc|ws|sm|st|sa|sn|cs|sc|sl|sg|sk|si|sb|so|za|gs|es|lk|sd|sr|sj|sz|se|ch|sy|tw|tj|tz|th|tl|tg|tk|to|tt|tn|tr|tm|tc|tv|ug|ua|ae|gb|us|um|uy|uz|vu|ve|vn|vg|vi|wf|eh|ye|zm|zw|com|edu|gov|int|mil|net|org|biz|info|name|pro|aero|coop|museum|arpa))|(((([0-9]){1,3}\.){3}([0-9]){1,3}))|(\[((([0-9]){1,3}\.){3}([0-9]){1,3})\])))$"
                            ControlToValidate="EmailTextBox" ErrorMessage="Indirizzo E-Mail non corretto"
                            SkinID="ZSSM_Validazione01"></asp:RegularExpressionValidator>
                    </td>
                </tr>
            </table>
        </div>
        <div align="center">
            <asp:Button ID="CreateUserButton" runat="server" Text="Crea Utente" SkinID="ZSSM_Button01"
                OnClick="CreateUserButton_Click" />
        </div>
    </asp:Panel>
    <asp:Panel ID="STEP2_Panel" runat="server" Visible="false">
        <table width="100%" border="0" cellspacing="5" cellpadding="0">
            <tr>
                <td width="100%">
                    <asp:Label SkinID="SysMsgDesc" ID="TitoloLabel" runat="server" Text="Utente creato con successo"
                        Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label SkinID="SysMsgDesc" ID="MessaggioLabel" runat="server" Text="L'utente è stato creato con successo"></asp:Label>
                </td>
            </tr>
        </table>
        <table width="100%" border="0" cellspacing="3" cellpadding="0">
            <tr>
                <td width="50" valign="middle">
                    <asp:Image ID="DettaglioImage" runat="server" ImageUrl="~/SiteImg/Ico3_Forw1.gif" />
                </td>
            
                <td width="100%" valign="middle">
                    <asp:HyperLink ID="DettaglioHyperLink" SkinID="SysMsgOption" runat="server" Text="Visualizza il dettaglio utente"></asp:HyperLink>
                </td>
            </tr>
        </table>
        <table width="100%" border="0" cellspacing="3" cellpadding="0">
            <tr>
                <td width="50" valign="middle">
                    <asp:Image ID="NuovoImage" runat="server" ImageUrl="~/SiteImg/Ico3_Forw1.gif" />
                </td>
           
                <td width="100%" valign="middle">
                    <asp:HyperLink ID="NuovoHyperLink" SkinID="SysMsgOption" runat="server" Text="Crea un nuovo utente"
                        NavigateUrl="~/Zeus/Account/Account_New_Fast.aspx"></asp:HyperLink>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
