<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" Theme="Zeus"  %>

<script runat="server">   
   
    static string TitoloPagina = "Modifica accesso/blocco aree web";
    

    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();
        TitleField.Value = TitoloPagina;
        
        if (!myDelinea.AntiSQLInjection(Request.QueryString))
            Response.Redirect("/System/Message.aspx");if(!Page.IsPostBack) 
         if (!isOK())
            Response.Redirect("~/Zeus/System/Message.aspx?Msg=7867868458426388&VfC=HKELP&Lang=ITA");


        if (!Page.IsPostBack)
        {

            CheckBox AccessoAreeRiservateCheckBox = (CheckBox)WebAccessFormView.FindControl("AccessoAreeRiservateCheckBox");
            CheckBox BloccoAreeRiservateCheckBox = (CheckBox)WebAccessFormView.FindControl("BloccoAreeRiservateCheckBox");
            Panel BloccoPanel = (Panel)WebAccessFormView.FindControl("BloccoPanel");
            Guid UID = new Guid(Request.QueryString["UID"]);
            MembershipUser user = Membership.GetUser(UID);

            AccessoAreeRiservateCheckBox.Checked = user.IsApproved;
            if (user.IsLockedOut)
                BloccoPanel.Visible = true;
            
            BloccoAreeRiservateCheckBox.Checked = user.IsLockedOut;

        }
    }//fine Page_Load

    private bool isOK()
    {
        try
        {
            if (Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/Account/Account_Dtl.aspx") > 0
                || Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/Soci/Soci_Dsp.aspx") > 0)
                return true;
            else return false;
        }
        catch (Exception p)
        {
            return false;
        }

    }//fine isOK

    protected void WebAccessLinkButton_Click(object sender, EventArgs e)
    {

        CheckBox AccessoAreeRiservateCheckBox = (CheckBox)WebAccessFormView.FindControl("AccessoAreeRiservateCheckBox");
        CheckBox BloccoAreeRiservateCheckBox = (CheckBox)WebAccessFormView.FindControl("BloccoAreeRiservateCheckBox");

        try
        {
            Guid UID = new Guid(Request.QueryString["UID"]);
            MembershipUser user = Membership.GetUser(UID);

            user.IsApproved = Convert.ToBoolean(AccessoAreeRiservateCheckBox.Checked.ToString());

            if ((!BloccoAreeRiservateCheckBox.Checked) && (user.IsLockedOut))
                user.UnlockUser();


            Membership.UpdateUser(user);
            Response.Redirect("~/Zeus/Account/Account_Dtl.aspx?UID=" + Request.QueryString["UID"].ToString());
        }
        catch (Exception p)
        {
            //Response.Write(p.ToString());
        }

    }

    protected void DataUltimoBloccoLabel_Databinding(object sender, EventArgs e)
    {
        Label DataUltimoBloccoLabel = (Label)sender;
        if (DataUltimoBloccoLabel.Text.Equals("01/01/1754 0.00.00"))
            DataUltimoBloccoLabel.Text = "Mai bloccato";
    }
    
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
      <asp:FormView ID="MemberShipFormView" runat="server" DefaultMode="ReadOnly" DataSourceID="MemberShipSqlDataSource"
        Width="100%" CellPadding="0">
        <ItemTemplate>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="AccountUtenteLabel" runat="server" Text="Account utente"></asp:Label></div>
                <table border="0" cellpadding="2" cellspacing="1">
                    <tr>
                        <td>
                            <asp:Label SkinID="FieldDescription" ID="UserName_Label" runat="server" Text="Label">UserName:</asp:Label>
                            <asp:Label SkinID="FieldValue" ID="UserNameLabel" runat="server" Text='<%# Eval("UserName", "{0}") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label SkinID="FieldDescription" ID="EMail_Label" runat="server" Text="Label">E-mail: </asp:Label>
                            <asp:HyperLink SkinID="FieldValue" ID="EmailHyperLink" CssClass="HyperLink1" runat="server"
                                NavigateUrl="mailto:" Text='<%# Eval("Email", "{0}") %>'></asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="DataCreazioneDesc_Label" SkinID="FieldDescription" runat="server"
                                Text="Label">Data creazione:</asp:Label>
                            <asp:Label ID="DataCreazioneLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("CreateDate", "{0}") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="UserIDDescr_Label" SkinID="FieldDescription" runat="server" Text="Label">User ID:</asp:Label>
                            <asp:Label ID="UserIDLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("Expr1", "{0}") %>'></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </ItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="MemberShipSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SELECT aspnet_Membership.Password, aspnet_Membership.Email
        , aspnet_Membership.IsApproved, aspnet_Membership.CreateDate, aspnet_Membership.LastLoginDate
        , aspnet_Membership.LastPasswordChangedDate
        , aspnet_Membership.LastLockoutDate, aspnet_Membership.FailedPasswordAttemptCount, aspnet_Users.UserName
        , aspnet_Users.UserId AS Expr1, aspnet_Membership.IsLockedOut FROM aspnet_Membership INNER JOIN aspnet_Users ON aspnet_Membership.UserId = aspnet_Users.UserId WHERE (aspnet_Users.UserId = @UserID)">
        <SelectParameters>
            <asp:QueryStringParameter Name="UserID" QueryStringField="UID" DefaultValue="0" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:FormView ID="WebAccessFormView" runat="server" DefaultMode="ReadOnly" Width="100%" DataSourceID="WebAccessSqlDataSource">
        <ItemTemplate>
    <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="ProfiloPersonaleLabel1" runat="server" Text="Accesso aree riservate"></asp:Label></div>
                <table border="0" cellpadding="2" cellspacing="1">
                <tr>
                    <td>
                        <asp:CheckBox ID="AccessoAreeRiservateCheckBox" runat="server" />
                      
                        <asp:Label ID="ConsentiAccesso_Label" runat="server" SkinID="FieldValue" Text="Consenti accesso alle aree riservate (in base ai ruoli)"></asp:Label>
                        
                    </td>
                </tr>
            </table></div>
            
                <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="Label1" runat="server" Text="Blocco account per password errate"></asp:Label></div>
                <table border="0" cellpadding="2" cellspacing="1">
                <asp:Panel ID="BloccoPanel" runat="server" Visible="false">
                <tr>
                    <td>
                        <asp:CheckBox ID="BloccoAreeRiservateCheckBox" runat="server" />
                        <asp:Label ID="UtenteBloccato_Label" SkinID="FieldValue" runat="server" Text="Utente bloccato"></asp:Label>
                    </td>
                </tr>
                </asp:Panel>
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" SkinID="FieldDescription" Text="Tentativi consecutivi di inserimento password sbagliate:"></asp:Label>
                   
                        <asp:Label ID="TentativiPasswordErrataLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("FailedPasswordAttemptCount", "{0}") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="DataBlocco_Label" runat="server" SkinID="FieldDescription" Text="Ultima data di blocco password:"></asp:Label>
                    
                         <asp:Label ID="DataUltimoBloccoLabel" runat="server" SkinID="FieldValue" 
                            Text='<%# Eval("LastLockoutDate", "{0}") %>' OnDataBinding="DataUltimoBloccoLabel_Databinding"></asp:Label>
                    </td>
                </tr>
            </table></div>
            <div align="center">
                <asp:LinkButton ID="WebAccessLinkButton" SkinID="ZSSM_Button01" runat="server" OnClick="WebAccessLinkButton_Click">Salva dati</asp:LinkButton>
            </div>

        </ItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="WebAccessSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SELECT  aspnet_Membership.LastLockoutDate, aspnet_Membership.FailedPasswordAttemptCount, aspnet_Users.UserName FROM aspnet_Membership INNER JOIN aspnet_Users ON aspnet_Membership.UserId = aspnet_Users.UserId WHERE (aspnet_Users.UserId = @UserID)">
        <SelectParameters>
            <asp:QueryStringParameter Name="UserID" QueryStringField="UID" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
