<%@ Control Language="C#" ClassName="Account_DtlDelinea" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();

        if (!myDelinea.AntiSQLInjection(Request.QueryString))
            Response.Redirect("~/Zeus/System/Message.aspx");

        DisableButtonByRole();

        ReadXML("PROFILI", GetLang());
        ReadXML_Localization("PROFILI", GetLang());

        string myJavascript = RequiredCheckBox();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartUpScript", myJavascript, true);

        UID.Value = Request.QueryString["UID"];

    }//fine Page_Load


    private string GetLang()
    {
        try
        {
            delinea myDelinea = new delinea();
            string Lang = "ITA";

            if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
              && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
                Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

            return Lang;
        }
        catch (Exception)
        {
            return "ITA";
        }
    }//fine GetLang

    private string RequiredCheckBox()
    {


        CheckBox AllowCheckBox = (CheckBox)AllowUserFormView.FindControl("AllowCheckBox");

        if (AllowCheckBox == null)
            return string.Empty;


        string myJavascript = " function RequiredCheckBox(sender, args) ";
        myJavascript += " { ";
        myJavascript += " if(document.getElementById('";
        myJavascript += AllowCheckBox.ClientID;
        myJavascript += "').checked) ";
        myJavascript += " args.IsValid = true; ";
        myJavascript += " else   args.IsValid = false; ";
        myJavascript += " return; ";
        myJavascript += " }";


        return myJavascript;

    }//fine RequiredCheckBox



    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Profili.xml"));


            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {

                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                            if (myLabel != null)
                                myLabel.Text = childNode.InnerText;

                        }
                        catch (Exception)
                        {

                        }
                    }//fine foreach
                }//fine foreach

            }//fine for
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine ReadXML_Localization



    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_Profili.xml"));

            if (ZMCF_BoxUserMaster != null)
                ZMCF_BoxUserMaster.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxUserMaster").InnerText);

            if (ZMCF_Listings != null)
                ZMCF_Listings.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Listings").InnerText);


            return true;
        }
        catch (Exception)
        {
            return false;
        }

    }//fine ReadXML


    private bool DisableButtonByRole()
    {
        try
        {
            delinea myDelinea = new delinea();

            if (myDelinea.CheckIsAdminManaged(Server.HtmlEncode(Request.QueryString["UID"])))
            {

                LinkButton AccountUtenteLinkButton = (LinkButton)myDelinea.FindControlRecursive(this, "AccountUtenteLinkButton");
                LinkButton AccessoBloccoLinkButton = (LinkButton)myDelinea.FindControlRecursive(this, "AccessoBloccoLinkButton");
                LinkButton RuoliLinkButton = (LinkButton)myDelinea.FindControlRecursive(this, "RuoliLinkButton");
                LinkButton ResetPasswordLinkButton = (LinkButton)myDelinea.FindControlRecursive(this, "ResetPasswordLinkButton");
                LinkButton EliminaAccountUtenteLinkButton = (LinkButton)myDelinea.FindControlRecursive(this, "EliminaAccountUtenteLinkButton");

                AccountUtenteLinkButton.Enabled = false;
                AccessoBloccoLinkButton.Enabled = false;
                RuoliLinkButton.Enabled = false;
                ResetPasswordLinkButton.Enabled = false;
                EliminaAccountUtenteLinkButton.Enabled = false;


            }

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }

    }//fine DisableButtonByRole




    protected void AccountUtenteLinkButton_Load(object sender, EventArgs e)
    {
        LinkButton AccountUtenteLinkButton = (LinkButton)sender;
        AccountUtenteLinkButton.PostBackUrl += "?UID=" + Request.QueryString["UID"].ToString();
    }


    protected void AccessoBloccoLinkButton_Load(object sender, EventArgs e)
    {
        LinkButton AccessoBloccoLinkButton = (LinkButton)sender;
        AccessoBloccoLinkButton.PostBackUrl += "?UID=" + Request.QueryString["UID"].ToString();
    }




    protected void RuoliLinkButton_Load(object sender, EventArgs e)
    {
        LinkButton RuoliLinkButton = (LinkButton)sender;
        RuoliLinkButton.PostBackUrl += "?UID=" + Request.QueryString["UID"].ToString();

    }

    protected void ResetPasswordLinkButton_Load(object sender, EventArgs e)
    {
        LinkButton ResetPasswordLinkButton = (LinkButton)sender;
        ResetPasswordLinkButton.PostBackUrl += "?UID=" + Request.QueryString["UID"].ToString();

    }

    protected void EliminaAccountUtenteLinkButton_Load(object sender, EventArgs e)
    {
        LinkButton EliminaAccountUtenteLinkButton = (LinkButton)sender;
        EliminaAccountUtenteLinkButton.PostBackUrl += "?UID=" + Request.QueryString["UID"].ToString();
    }


    protected void DelUser_UserName_DataBinding(object sender, EventArgs e)
    {
        HiddenField DelUser_UserName = (HiddenField)UserDeletedFormView.FindControl("DelUser_UserName");
        Label DelUser_CognomeNome = (Label)UserDeletedFormView.FindControl("DelUser_CognomeNome");

        DelUser_CognomeNome.Text += " " + DelUser_UserName.Value.ToString();
    }

    protected void DataUltimoBloccoLabel_Databinding(object sender, EventArgs e)
    {
        Label DataUltimoBloccoLabel = (Label)sender;
        if (DataUltimoBloccoLabel.Text.Equals("01/01/1754 0.00.00"))
            DataUltimoBloccoLabel.Text = "Mai bloccato";
    }

    protected void EmailHyperLink_DataBinding(object sender, EventArgs e)
    {
        HyperLink EmailHyperLink = (HyperLink)sender;
        EmailHyperLink.NavigateUrl += EmailHyperLink.Target.ToString(); ;
        EmailHyperLink.Target = string.Empty;

    }

    protected void EmailHyperLink2_DataBinding(object sender, EventArgs e)
    {
        HyperLink EmailHyperLink2 = (HyperLink)sender;
        EmailHyperLink2.NavigateUrl += EmailHyperLink2.Target.ToString(); ;
        EmailHyperLink2.Target = string.Empty;

    }


    protected void RoleNameLabel_DataBinding(object sender, EventArgs e)
    {
        Label RoleNameLabel = (Label)sender;

        if (RoleNameLabel.Text.Length == 0)
            RoleNameLabel.Text = "L�utente non possiede ruoli assegnati";

    }//fine RoleNameLabel_DataBinding

    protected void ImportCodeLabel_DataBinding(object sender, EventArgs e)
    {
        Label ImportCodeLabel = (Label)sender;

        if (ImportCodeLabel.Text.Length > 0)
            ImportCodeLabel.Text = "Codice importazione: " + ImportCodeLabel.Text;
        else ImportCodeLabel.Text = "Codice importazione: account creato in modalit� manuale";

    }//fine ImportCodeLabel_DataBinding


    protected void MemberShipSqlDataSource_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.AffectedRows <= 0)
        {
            UserAlivePanel.Visible = false;
            UserNOTAlivePanel.Visible = true;
        }
    }//fine MemberShipSqlDataSource_Selected

    protected void AllowUserSqlDataSource_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {

        //Response.Write("DEB e.AffectedRows:" + e.AffectedRows+"<br />");

        try
        {

            if ((e.AffectedRows > 0)
                && (e.Exception == null))
            {



                Panel AccessoBloccoPanel = (Panel)MemberShipFormView.FindControl("AccessoBloccoPanel");

                if (AccessoBloccoPanel != null)
                    AccessoBloccoPanel.Visible = false;
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }


    }//fine AllowUserSqlDataSource_Selected

    private bool AllowUser(string UserID)
    {

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;
        SqlTransaction transaction = null;

        try
        {

            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                transaction = connection.BeginTransaction();
                command.Transaction = transaction;


                sqlQuery = "UPDATE [aspnet_Membership]";
                sqlQuery += " SET [IsApproved]=1";
                sqlQuery += " WHERE [UserId]=@UserId";

                command.CommandText = sqlQuery;
                command.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@UserId"].Value = new Guid(UserID);
                command.ExecuteNonQuery();



                sqlQuery = "UPDATE [tbProfiliMarketing]";
                sqlQuery += " SET [RecordNewUser]=@RecordNewUser";
                sqlQuery += " WHERE [UserId]=@UserId";

                command.CommandText = sqlQuery;
                command.Parameters.Clear();
                command.Parameters.Add("@RecordNewUser", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@RecordNewUser"].Value = Membership.GetUser(true).ProviderUserKey;
                command.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@UserId"].Value = new Guid(UserID);
                command.ExecuteNonQuery();


                sqlQuery = "UPDATE [tbProfiliPersonali]";
                sqlQuery += " SET [RecordNewUser]=@RecordNewUser";
                sqlQuery += " WHERE [UserId]=@UserId";

                command.CommandText = sqlQuery;
                command.Parameters.Clear();
                command.Parameters.Add("@RecordNewUser", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@RecordNewUser"].Value = Membership.GetUser(true).ProviderUserKey;
                command.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@UserId"].Value = new Guid(UserID);
                command.ExecuteNonQuery();


                sqlQuery = "UPDATE [tbProfiliSocietari]";
                sqlQuery += " SET [RecordNewUser]=@RecordNewUser";
                sqlQuery += " WHERE [UserId]=@UserId";

                command.CommandText = sqlQuery;
                command.Parameters.Clear();
                command.Parameters.Add("@RecordNewUser", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@RecordNewUser"].Value = Membership.GetUser(true).ProviderUserKey;
                command.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@UserId"].Value = new Guid(UserID);
                command.ExecuteNonQuery();


                transaction.Commit();
                return true;


            }//fine Using
        }
        catch (Exception p)
        {

            if (transaction != null)
                transaction.Rollback();

            Response.Write(p.ToString());
            return false;
        }


    }//fine AllowUser

    private string GetNumOfAnnunci(string UID, string numMax, string myZeusIdModulo)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;
        string NumOfAnnunci = string.Empty;

        try
        {

            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();


                sqlQuery = "SELECT count([ID1Listing])";
                sqlQuery += " FROM [tbListings] ";
                sqlQuery += " WHERE [Status] <> 'TMP' ";
                sqlQuery += " AND [UID_Inserzionista]=@UID_Inserzionista";
                sqlQuery += " AND ZeusIdModulo=@ZeusIdModulo";
                command.CommandText = sqlQuery;

                command.Parameters.Add("@UID_Inserzionista", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@UID_Inserzionista"].Value = new Guid(UID);
                command.Parameters.Add("@ZeusIdModulo", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ZeusIdModulo"].Value = myZeusIdModulo;

                NumOfAnnunci = ((int)command.ExecuteScalar()).ToString();

            }//fine Using
        }
        catch (Exception p)
        {

            Response.Write(p.ToString());

        }

        NumOfAnnunci = "Annunci: " + NumOfAnnunci + " di " + numMax;

        return NumOfAnnunci;

    }//fine GetNumOfAnnunci


    protected void ModificaAnnunciLinkButton_DataBinding(object sender, EventArgs e)
    {
        LinkButton ModificaAnnunciLinkButton = (LinkButton)sender;

        ModificaAnnunciLinkButton.PostBackUrl += "&ZIM=" + ModificaAnnunciLinkButton.ToolTip;
        ModificaAnnunciLinkButton.ToolTip = string.Empty;

    }//fine ModificaAnnunciLinkButton_DataBinding


    protected void AllowUserLinkButton_Click(object sender, EventArgs e)
    {

        if (AllowUser(UID.Value))
            Response.Redirect("~/Zeus/Account/Account_Dtl.aspx?UID=" + UID.Value);
        else Response.Redirect("~/Zeus/System/Message.aspx?AllowErr");


    }//fine AllowUserLinkButton_Click
    
    
</script>

<!--PARTE DELLE MEMBERSHIP-->
<asp:HiddenField ID="UID" runat="server" />
<asp:FormView ID="AllowUserFormView" runat="server" DefaultMode="ReadOnly" DataSourceID="AllowUserSqlDataSource"
    Width="100%" CellPadding="0" DataKey="UserID">
    <ItemTemplate>
        <div class="BlockBox">
            <div class="BlockBoxHeader">
                <asp:Label ID="AccountUtenteLabel" runat="server" Text="Abilitazione utente"></asp:Label></div>
            <table border="0" cellpadding="2" cellspacing="1">
                <tr>
                    <td>
                        <asp:Label ID="MessageLabel" runat="server" Text="ATTENZIONE: utente in attesa di abilitazione"
                            SkinID="Validazione01"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label SkinID="FieldDescription" ID="UserName_Label" runat="server" Text="Label">UserName:</asp:Label>
                        <asp:Label SkinID="FieldValue" ID="UserNameLabel" runat="server" Text='<%# Eval("UserName", "{0}") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label SkinID="FieldDescription" ID="EMail_Label" runat="server" Text="Label">E-mail: </asp:Label>
                        <asp:HyperLink SkinID="FieldValue" ID="EmailHyperLink" CssClass="HyperLink1" runat="server"
                            NavigateUrl="mailto:" Text='<%# Eval("Email", "{0}") %>' Target='<%# Eval("Email", "{0}") %>'
                            OnDataBinding="EmailHyperLink_DataBinding"></asp:HyperLink>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="DataCreazioneDesc_Label" SkinID="FieldDescription" runat="server"
                            Text="Label">Data creazione:</asp:Label>
                        <asp:Label ID="DataCreazioneLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("CreateDate", "{0}") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="UserIDDescr_Label" SkinID="FieldDescription" runat="server" Text="Label">User ID:</asp:Label>
                        <asp:Label ID="UserIDLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("UserID", "{0}") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox ID="AllowCheckBox" runat="server" Text="Conferma abilitazione utente"
                            CssClass="CheckBoxList_FieldValue" />
                        <asp:CustomValidator ID="AllowCustomValidator" runat="server" ClientValidationFunction="RequiredCheckBox"
                            ErrorMessage="Obbligatorio" SkinID="ZSSM_Validazione01" Display="Dynamic" ValidationGroup="Allow"></asp:CustomValidator>
                        <asp:LinkButton ID="AllowUserLinkButton" runat="server" SkinID="ZSSM_Button01" Text="Abilita"
                            OnClick="AllowUserLinkButton_Click" ValidationGroup="Allow"></asp:LinkButton>
                    </td>
                </tr>
            </table>
            <div class="VertSpacerMedium">
            </div>
        </div>
    </ItemTemplate>
</asp:FormView>
<asp:SqlDataSource ID="AllowUserSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
    SelectCommand="SELECT Cognome , Nome , RagioneSociale , UserID , Email , CreateDate ,RegCausal,UserName
        FROM vwAccount_Lst_ToApprove WHERE (UserId = @UserID)" OnSelected="AllowUserSqlDataSource_Selected">
    <SelectParameters>
        <asp:QueryStringParameter Name="UserID" QueryStringField="UID" DefaultValue="0" />
    </SelectParameters>
</asp:SqlDataSource>
<asp:Panel ID="UserAlivePanel" runat="server" Visible="true">
    <asp:FormView ID="MemberShipFormView" runat="server" DefaultMode="ReadOnly" DataSourceID="MemberShipSqlDataSource"
        Width="100%" CellPadding="0">
        <ItemTemplate>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="AccountUtenteLabel" runat="server" Text="Account utente"></asp:Label></div>
                <table border="0" cellpadding="2" cellspacing="1">
                    <tr>
                        <td>
                            <asp:Label SkinID="FieldDescription" ID="UserName_Label" runat="server" Text="Label">UserName:</asp:Label>
                            <asp:Label SkinID="FieldValue" ID="UserNameLabel" runat="server" Text='<%# Eval("UserName", "{0}") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label SkinID="FieldDescription" ID="EMail_Label" runat="server" Text="Label">E-mail: </asp:Label>
                            <asp:HyperLink SkinID="FieldValue" ID="EmailHyperLink" CssClass="HyperLink1" runat="server"
                                NavigateUrl="mailto:" Text='<%# Eval("Email", "{0}") %>' Target='<%# Eval("Email", "{0}") %>'
                                OnDataBinding="EmailHyperLink_DataBinding"></asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="DataCreazioneDesc_Label" SkinID="FieldDescription" runat="server"
                                Text="Label">Data creazione:</asp:Label>
                            <asp:Label ID="DataCreazioneLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("CreateDate", "{0}") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="UserIDDescr_Label" SkinID="FieldDescription" runat="server" Text="Label">User ID:</asp:Label>
                            <asp:Label ID="UserIDLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("Expr1", "{0}") %>'></asp:Label>
                        </td>
                    </tr>
                </table>
                <div style="text-align: center">
                    <asp:LinkButton ID="AccountUtenteLinkButton" runat="server" SkinID="ZSSM_Button01"
                        Text="Modifica E-Mail" PostBackUrl="~/Zeus/Account/Account_Email_Edt.aspx" OnLoad="AccountUtenteLinkButton_Load"></asp:LinkButton></div>
                <div class="VertSpacerMedium">
                </div>
            </div>
            <!--################################################-->
            <asp:Panel ID="AccessoBloccoPanel" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="AccessoBloccoLabel" runat="server" Text="Accesso / Blocco"></asp:Label></div>
                    <table border="0" cellpadding="2" cellspacing="1">
                        <tr>
                            <td>
                                <asp:CheckBox ID="AccessoAreeRiservateCheckBox" runat="server" Checked='<%# Bind("IsApproved") %>'
                                    Enabled="false" />
                                <asp:Label ID="Label2" SkinID="FieldValue" runat="server" Text="Label">Accesso aree riservate</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="BloccoAreeRiservateCheckBox" runat="server" Checked='<%# Bind("IsLockedOut") %>'
                                    Enabled="false" />
                                <asp:Label ID="Label3" SkinID="FieldValue" runat="server" Text="Label">Blocco aree riservate</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label4" SkinID="FieldDescription" runat="server" Text="Label">Data ultimo accesso:</asp:Label>
                                <asp:Label ID="DataUltimoLoginLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("LastLoginDate", "{0}") %>'></asp:Label>
                            </td>
                        </tr>
                        <asp:Panel ID="LockedOutPanel" runat="server" Visible='<%# Convert.ToBoolean(Eval("IsLockedOut").ToString()) %>'>
                            <tr>
                                <td>
                                    <asp:Label ID="Label5" runat="server" SkinID="FieldDescription" Text="Label"> Data ultimo blocco causa password errata:</asp:Label>
                                    <asp:Label ID="DataUltimoBloccoLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("LastLockoutDate", "{0}") %>'
                                        OnDataBinding="DataUltimoBloccoLabel_Databinding"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <tr>
                            <td>
                                <asp:Label ID="Label6" runat="server" SkinID="FieldDescription" Text="Label">Numero ultimi tentativi password errata:</asp:Label>
                                <asp:Label ID="TentativiPasswordErrataLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("FailedPasswordAttemptCount", "{0}") %>'></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                            </td>
                        </tr>
                    </table>
                    <div style="text-align: center">
                        <asp:LinkButton ID="AccessoBloccoLinkButton" runat="server" SkinID="ZSSM_Button01"
                            Text="Modifica" PostBackUrl="~/Zeus/Account/Account_WebAccess.aspx" OnLoad="AccessoBloccoLinkButton_Load"></asp:LinkButton></div>
                    <div class="VertSpacerMedium">
                    </div>
                </div>
            </asp:Panel>
            <!--###################################################-->
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="RuoliUtenteLabel" runat="server" Text="Ruoli utente"></asp:Label></div>
                <table border="0" cellpadding="2" cellspacing="1">
                    <tr>
                        <td>
                            <asp:DataList ID="DataList1" runat="server" DataSourceID="RolesSqlDataSource">
                                <ItemTemplate>
                                    <asp:Label ID="RoleNameLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("RoleName") %>'
                                        OnDataBinding="RoleNameLabel_DataBinding"></asp:Label><br />
                                </ItemTemplate>
                            </asp:DataList>
                            <asp:SqlDataSource ID="RolesSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                SelectCommand="SELECT [RoleName] FROM [vwAccount_UsersInRoles] WHERE ([UserId] = @UserId) ORDER BY RoleName">
                                <SelectParameters>
                                    <asp:QueryStringParameter Name="UserId" QueryStringField="UID" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
                <div style="text-align: center">
                    <asp:LinkButton ID="RuoliLinkButton" runat="server" SkinID="ZSSM_Button01" Text="Modifica"
                        PostBackUrl="~/Zeus/Account/Account_Roles_Edt.aspx" OnLoad="RuoliLinkButton_Load"></asp:LinkButton></div>
                <div class="VertSpacerMedium">
                </div>
            </div>
            <!--###########################################################################-->
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="PasswordLabel1" runat="server" Text="Password"></asp:Label></div>
                <table border="0" cellpadding="2" cellspacing="1">
                    <tr>
                        <td>
                            <asp:Label ID="Label9" SkinID="FieldDescription" runat="server" Text="Label">Data ultimo cambio password:</asp:Label>
                            <asp:Label ID="DataCambiamentoPasswordLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("LastPasswordChangedDate", "{0:g}") %>'></asp:Label>
                        </td>
                    </tr>
                </table>
                <div style="text-align: center">
                    <asp:LinkButton ID="ResetPasswordLinkButton" SkinID="ZSSM_Button01" runat="server"
                        Text="Reset password" PostBackUrl="~/Zeus/Account/Account_ResetPsw.aspx" OnLoad="ResetPasswordLinkButton_Load"></asp:LinkButton>
                </div>
                <div class="VertSpacerMedium">
                </div>
            </div>
        </ItemTemplate>
    </asp:FormView>
    <div class="BlockBox">
        <div class="BlockBoxHeader">
            <asp:Label ID="EliminaAccountLabel" runat="server" Text="Elimina account utente"></asp:Label></div>
        <table border="0" cellpadding="2" cellspacing="1">
        </table>
        <div style="text-align: center">
            <asp:LinkButton ID="EliminaAccountUtenteLinkButton" SkinID="ZSSM_Button01" runat="server"
                Text="Elimina account" PostBackUrl="~/Zeus/Account/Account_Del.aspx" OnLoad="EliminaAccountUtenteLinkButton_Load"></asp:LinkButton>
        </div>
        <div class="VertSpacerMedium">
        </div>
    </div>
    <asp:SqlDataSource ID="MemberShipSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SELECT aspnet_Membership.Password, aspnet_Membership.Email
        , aspnet_Membership.IsApproved, aspnet_Membership.CreateDate, aspnet_Membership.LastLoginDate
        , aspnet_Membership.LastPasswordChangedDate
        , aspnet_Membership.LastLockoutDate, aspnet_Membership.FailedPasswordAttemptCount, aspnet_Users.UserName
        , aspnet_Users.UserId AS Expr1, aspnet_Membership.IsLockedOut FROM aspnet_Membership INNER JOIN aspnet_Users ON aspnet_Membership.UserId = aspnet_Users.UserId WHERE (aspnet_Users.UserId = @UserID)"
        OnSelected="MemberShipSqlDataSource_Selected">
        <SelectParameters>
            <asp:QueryStringParameter Name="UserID" QueryStringField="UID" DefaultValue="0" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:Panel ID="ZMCF_BoxUserMaster" runat="server">
        <asp:FormView ID="UserMasterFormView" runat="server" DataSourceID="tbProfiliPersonaliSqlDataSource"
            DefaultMode="ReadOnly" Width="100%" CellPadding="0">
            <ItemTemplate>
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="ZML_BoxUserMaster" runat="server" Text="UserMaster"></asp:Label></div>
                    <table border="0" cellpadding="2" cellspacing="1">
                        <tr>
                            <td>
                                <asp:Label ID="Label2" SkinID="FieldDescription" runat="server">Utente:</asp:Label>
                                <asp:HyperLink CssClass="DataTrackingLabel" SkinID="FieldValue" ID="HyperLink1" runat="server"
                                    NavigateUrl='<%# Eval("UserMaster", "~/Zeus/Account/Account_Dtl.aspx?UID={0}") %>'
                                    Text='<%# Eval("Identificativo", "{0}") %>'></asp:HyperLink>
                                <br />
                                <asp:Label ID="ImportCodeLabel" SkinID="FieldDescription" runat="server" Text='<%# Eval("ImportCode") %>'
                                    OnDataBinding="ImportCodeLabel_DataBinding"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <div class="VertSpacerMedium">
                    </div>
                </div>
            </ItemTemplate>
            <EmptyDataTemplate>
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="UserMasterLabel" runat="server" Text="UserMaster"></asp:Label></div>
                    <table border="0" cellpadding="2" cellspacing="1">
                        <tr>
                            <td>
                                <asp:Label ID="Label2" SkinID="FieldDescription" runat="server">Utente: non associato</asp:Label>
                            </td>
                        </tr>
                    </table>
                    <div class="VertSpacerMedium">
                    </div>
                </div>
            </EmptyDataTemplate>
        </asp:FormView>
        <asp:SqlDataSource ID="tbProfiliPersonaliSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
            SelectCommand="SELECT UserMaster,Identificativo,ImportCode FROM vwUserMaster WHERE UserID=@UserID">
            <SelectParameters>
                <asp:QueryStringParameter Name="UserID" QueryStringField="UID" DefaultValue="0" />
            </SelectParameters>
        </asp:SqlDataSource>
    </asp:Panel>
    <asp:Panel ID="ZMCF_Listings" runat="server">
        <asp:Repeater ID="AnnunciRepeater" runat="server" DataSourceID="AnnunciSqlDataSource">
            <ItemTemplate>
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="UserMasterLabel" runat="server" Text='<%# Eval("Sezione","Dettaglio annunci tipologia {0}") %>'></asp:Label></div>
                    <table border="0" cellpadding="2" cellspacing="1">
                        <tr>
                            <td>
                                <asp:Label ID="Label2" SkinID="FieldDescription" runat="server">Limite massimo di annunci: </asp:Label>
                                <asp:Label ID="ZeusJolly1Label" SkinID="FieldDescription" runat="server" Text='<%# Eval("MaxListing") %>'></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label7" SkinID="FieldDescription" runat="server" Text='<%# GetNumOfAnnunci(Eval("Inserzionista").ToString(),Eval("MaxListing").ToString(),Eval("ZeusIdModulo").ToString()) %>'></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <div style="text-align: center">
                        <asp:LinkButton ID="ModificaAnnunciLinkButton" SkinID="ZSSM_Button01" runat="server"
                            ToolTip='<%# Eval("ZeusIdModulo") %>' OnDataBinding="ModificaAnnunciLinkButton_DataBinding"
                            Text="Modifica" PostBackUrl='<%# Eval("Inserzionista","~/Zeus/Account/Annunci_Edt.aspx?UID={0}") %>'></asp:LinkButton>
                    </div>
                    <div class="VertSpacerMedium">
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <asp:SqlDataSource ID="AnnunciSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
            SelectCommand="
        IF OBJECT_ID ('tbListings_MaxNumber','U') IS NOT NULL
        SELECT     dbo.tbModuli.Sezione, dbo.tbListings_MaxNumber.MaxListing,
        dbo.tbListings_MaxNumber.Inserzionista ,
        dbo.tbListings_MaxNumber.ZeusIdModulo

FROM         dbo.tbListings_MaxNumber INNER JOIN
                      dbo.tbModuli ON dbo.tbListings_MaxNumber.ZeusIdModulo = dbo.tbModuli.ZeusIdModulo WHERE  dbo.tbListings_MaxNumber.Inserzionista=@UserID">
            <SelectParameters>
                <asp:QueryStringParameter Name="UserID" QueryStringField="UID" DefaultValue="0" />
            </SelectParameters>
        </asp:SqlDataSource>
    </asp:Panel>
</asp:Panel>
<asp:Panel ID="UserNOTAlivePanel" runat="server" Visible="false">
    <asp:FormView ID="UserDeletedFormView" runat="server" DefaultMode="ReadOnly" DataSourceID="UserDeletedSqlDataSource">
        <ItemTemplate>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="AccountUtenteLabel1" runat="server" Text="Account utente (eliminato)"></asp:Label></div>
                <table border="0" cellpadding="2" cellspacing="1">
                    <tr>
                        <td>
                            <asp:Label ID="Label12" runat="server" SkinID="FieldDescription" Text="Label">UserName:</asp:Label>
                            <asp:Label ID="UserNameLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("UserName", "{0}") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label13" runat="server" SkinID="FieldDescription" Text="Label"> E-mail:</asp:Label>
                            <asp:HyperLink ID="EmailHyperLink2" runat="server" SkinID="FieldValue" NavigateUrl="mailto:"
                                Text='<%# Eval("Email", "{0}") %>' Target='<%# Eval("Email", "{0}") %>' OnDataBinding="EmailHyperLink2_DataBinding"></asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label14" SkinID="FieldDescription" runat="server" Text="Label">Data di creazione utente:</asp:Label>
                            <asp:Label ID="RecordNewDateLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("RecordNewDate", "{0}") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label15" runat="server" SkinID="FieldDescription" Text="Label">Data di eliminazione: </asp:Label>
                            <asp:Label ID="RecordDelDateLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("RecordDelDate", "{0}") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label16" runat="server" SkinID="FieldDescription" Text="Label">Utente che ha effettuato l'eliminazione:</asp:Label>
                            <asp:Label ID="DelUser_CognomeNome" runat="server" SkinID="FieldValue" Text='<%# Eval("DelUser_CognomeNome", "{0}") %>'></asp:Label>
                            <asp:HiddenField ID="DelUser_UserName" runat="server" Value='<%# Eval("DelUser_UserName","{0}") %>'
                                OnDataBinding="DelUser_UserName_DataBinding" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label17" runat="server" SkinID="FieldDescription" Text="Label">Data ultimo accesso:</asp:Label>
                            <asp:Label ID="LastLoginDateLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("LastLoginDate", "{0}") %>'></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </ItemTemplate>
    </asp:FormView>
</asp:Panel>
<asp:SqlDataSource ID="UserDeletedSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
    SelectCommand="SELECT UserName, Email, RecordNewDate, RecordDelDate, DelUser_UserName,DelUser_CognomeNome, LastLoginDate FROM vwUsers_Deleted WHERE (UserId = @UserID)">
    <SelectParameters>
        <asp:QueryStringParameter Name="UserID" QueryStringField="UID" DefaultValue="0" />
    </SelectParameters>
</asp:SqlDataSource>
