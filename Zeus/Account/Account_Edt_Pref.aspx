<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" %>

<script runat="server">
   
    static string TitoloPagina = "Modifica Preferenze";


    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();
        TitleField.Value = TitoloPagina;

        if (!myDelinea.AntiSQLInjection(Request.QueryString))
            Response.Redirect("~/Zeus/System/Message.aspx");

        if (!Page.IsPostBack)
            if (!isOK())
                Response.Redirect("~/Zeus/System/Message.aspx?Msg=7867868458426388&VfC=HKELP&Lang=ITA");


        ProfiloMarketing_Edt1.UID = Server.HtmlEncode(Request.QueryString["UID"]);
        ProfiloMarketing_Edt1.RedirectPath = "~/Zeus/Account/Account_Dtl.aspx?UID=" + Server.HtmlEncode(Request.QueryString["UID"]);




    }//fine Page_Load

    private bool isOK()
    {
        try
        {
            if (Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/Account/Account_Dtl.aspx") > 0
                || Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/Soci/Soci_Dsp.aspx") > 0)
                return true;
            else return false;
        }
        catch (Exception p)
        {
            return false;
        }

    }//fine isOK

    
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
      <asp:FormView ID="MemberShipFormView" runat="server" DefaultMode="ReadOnly" DataSourceID="MemberShipSqlDataSource"
        Width="100%" CellPadding="0">
        <ItemTemplate>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="AccountUtenteLabel" runat="server" Text="Account utente"></asp:Label></div>
                <table border="0" cellpadding="2" cellspacing="1">
                    <tr>
                        <td>
                            <asp:Label SkinID="FieldDescription" ID="UserName_Label" runat="server" Text="Label">UserName:</asp:Label>
                            <asp:Label SkinID="FieldValue" ID="UserNameLabel" runat="server" Text='<%# Eval("UserName", "{0}") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label SkinID="FieldDescription" ID="EMail_Label" runat="server" Text="Label">E-mail: </asp:Label>
                            <asp:HyperLink SkinID="FieldValue" ID="EmailHyperLink" CssClass="HyperLink1" runat="server"
                                NavigateUrl="mailto:" Text='<%# Eval("Email", "{0}") %>'></asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="DataCreazioneDesc_Label" SkinID="FieldDescription" runat="server"
                                Text="Label">Data creazione:</asp:Label>
                            <asp:Label ID="DataCreazioneLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("CreateDate", "{0}") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="UserIDDescr_Label" SkinID="FieldDescription" runat="server" Text="Label">User ID:</asp:Label>
                            <asp:Label ID="UserIDLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("Expr1", "{0}") %>'></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </ItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="MemberShipSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SELECT aspnet_Membership.Password, aspnet_Membership.Email
        , aspnet_Membership.IsApproved, aspnet_Membership.CreateDate, aspnet_Membership.LastLoginDate
        , aspnet_Membership.LastPasswordChangedDate
        , aspnet_Membership.LastLockoutDate, aspnet_Membership.FailedPasswordAttemptCount, aspnet_Users.UserName
        , aspnet_Users.UserId AS Expr1, aspnet_Membership.IsLockedOut FROM aspnet_Membership INNER JOIN aspnet_Users ON aspnet_Membership.UserId = aspnet_Users.UserId WHERE (aspnet_Users.UserId = @UserID)">
        <SelectParameters>
            <asp:QueryStringParameter Name="UserID" QueryStringField="UID" DefaultValue="0" />
        </SelectParameters>
    </asp:SqlDataSource>
    <dlc:ProfiloMarketing_Edt ID="ProfiloMarketing_Edt1" runat="server" />
    <dlc:zeusdatatracking ID="Zeusdatatracking1" runat="server" />
</asp:Content>
