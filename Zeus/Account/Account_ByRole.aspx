<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">

    static string TitoloPagina = "Gestione utenti per ruolo";


    public void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;
        Msg.Text = "";
        Label1.Visible = false;
        if (!IsPostBack)
        {

            if (!Roles.IsUserInRole(Membership.GetUser().UserName, "ZeusAdmin"))
                RolesGrid.DataSource = GetNotAdminManagedRoles();
            else RolesGrid.DataSource = Roles.GetAllRoles();

            RolesGrid.DataBind();


            if (!Roles.IsUserInRole(Membership.GetUser().UserName, "ZeusAdmin"))
                UsersListBox.DataSource = GetNotAdminManagedUser();
            else UsersListBox.DataSource = Membership.GetAllUsers();

            UsersListBox.DataBind();

        }
    }//fine Page_Load


    private bool IsAdminManagedUser(string UserName)
    {
        try
        {

            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(
               strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT RoleName FROM tbZeusRoles WHERE IsAdminManaged=1";
                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();

                bool myReturn = false;

                while (reader.Read())
                    if (Roles.IsUserInRole(UserName, reader.GetString(0)))
                        myReturn = true;

                reader.Close();
                return myReturn;
            }//fine using
            
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }

    }//fine RemoveAllUserFromList


    private bool IsAdminRole(string UserName) 
    {
        try
        {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(
               strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT RoleName FROM tbZeusRoles WHERE IsAdminRole=1";
                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();

                bool myReturn = false;

                while (reader.Read())
                    if (Roles.IsUserInRole(UserName, reader.GetString(0)))
                        myReturn = true;

                reader.Close();
                return myReturn;
            }//fine using
            
            
        }
        catch (Exception p) 
        {
            Response.Write(p.ToString());
            return false;
        }

    }//fine IsAdminRole

    private MembershipUserCollection GetNotAdminManagedUser()
    {
        MembershipUserCollection myMembershipUsers = new MembershipUserCollection();

        try
        {
            myMembershipUsers = Membership.GetAllUsers();
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(
               strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT RoleName FROM tbZeusRoles WHERE IsAdminManaged=1";
                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if ((!Roles.IsUserInRole(Membership.GetUser().UserName, reader.GetString(0)))|| (!IsAdminRole(Membership.GetUser().UserName)))
                    {
                        string[] myRemoveUsers = Roles.GetUsersInRole(reader.GetString(0));

                        for (int i = 0; i < myRemoveUsers.Length; ++i)
                            myMembershipUsers.Remove(myRemoveUsers[i]);
                    }
                }//fine while

                reader.Close();

            }//fine using

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

        return myMembershipUsers;

    }//fine GetNotAdminManagedUser

    private string[] GetNotAdminManagedRoles()
    {
        ArrayList myArray = new ArrayList();

        try
        {

            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(
               strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT RoleName FROM tbZeusRoles WHERE IsAdminManaged=0";
                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                    myArray.Add(reader.GetString(0));


                reader.Close();

            }//fine using

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

        return (string[])myArray.ToArray(typeof(string));
    }//fine GetNotAdminManagedRoles



    public void AddUsers_OnClick(object sender, EventArgs args)
    {
        if (LabelUser.Text == "")
        {
            Msg.Text = "Seleziona un ruolo.";
            return;
        }

        if (UsersListBox.SelectedItem == null)
        {
            Msg.Text = "Seleziona uno o pi� utenti.";
            return;
        }

        string[] newusers = new string[UsersListBox.GetSelectedIndices().Length];
        for (int i = 0; i < newusers.Length; i++)
        {
            newusers[i] = UsersListBox.Items[UsersListBox.GetSelectedIndices()[i]].Value;
        }

        try
        {
            Roles.AddUsersToRole(newusers, LabelUser.Text.ToString());

            string[] myUsersInRole = Roles.GetUsersInRole(LabelUser.Text.ToString());

            if (!Roles.IsUserInRole(Membership.GetUser().UserName, "ZeusAdmin"))
            {
                ArrayList myArray = new ArrayList();


                for (int i = 0; i < myUsersInRole.Length; ++i)
                    if (!IsAdminManagedUser(myUsersInRole[i]))
                        myArray.Add(myUsersInRole[i]);

                myUsersInRole = (string[])myArray.ToArray(typeof(string));
            }

            UsersInRoleGrid.DataSource = myUsersInRole;
            UsersInRoleGrid.DataBind();
            Label1.Visible = true;
        }
        catch (Exception e)
        {
            Msg.Text = e.Message;
        }
    }//fine AddUsers_OnClick

    public void UsersInRoleGrid_RemoveFromRole(object sender, GridViewCommandEventArgs args)
    {

        int index = Convert.ToInt32(args.CommandArgument);
        string username = ((DataBoundLiteralControl)UsersInRoleGrid.Rows[index].Cells[0].Controls[0]).Text;

        try
        {
            Roles.RemoveUserFromRole(username, LabelUser.Text.ToString());
        }
        catch (Exception e)
        {
            Msg.Text = "An exception of type " + e.GetType().ToString() +
                       " was encountered removing the user from the role.";
        }

        string[] myUsersInRole = Roles.GetUsersInRole(LabelUser.Text.ToString());

        if (!Roles.IsUserInRole(Membership.GetUser().UserName, "ZeusAdmin"))
        {
            ArrayList myArray = new ArrayList();


            for (int i = 0; i < myUsersInRole.Length; ++i)
                if (!IsAdminManagedUser(myUsersInRole[i]))
                    myArray.Add(myUsersInRole[i]);

            myUsersInRole = (string[])myArray.ToArray(typeof(string));
        }

        UsersInRoleGrid.DataSource = myUsersInRole;
        UsersInRoleGrid.DataBind();
        Label1.Visible = true;

    }//fine UsersInRoleGrid_RemoveFromRole

    protected void RolesList_Click(object sender, EventArgs e)
    {
        Label1.Visible = true;
        LinkButton role = (LinkButton)sender;
        LabelUser.Text = role.Text.ToString();

        string[] myUsersInRole = Roles.GetUsersInRole(role.Text.ToString());

        if (!Roles.IsUserInRole(Membership.GetUser().UserName, "ZeusAdmin"))
        {
            ArrayList myArray = new ArrayList();


            for (int i = 0; i<myUsersInRole.Length; ++i)
                if (!IsAdminManagedUser(myUsersInRole[i]))
                    myArray.Add(myUsersInRole[i]);

            myUsersInRole = (string[])myArray.ToArray(typeof(string));
        }

        UsersInRoleGrid.DataSource = myUsersInRole;
        UsersInRoleGrid.DataBind();

    }//fine RolesList_Click


</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <table cellpadding="10" cellspacing="0">
        <tr valign="top">
            <td style="width:40%">
           <div class="LayGridTitolo1"><asp:Label ID="CaptionNewRecordLabel" SkinID="GridTitolo1" runat="server" Text="Selezionare un ruolo:"></asp:Label></div>
                <asp:GridView runat="server" ID="RolesGrid" AutoGenerateColumns="False" 
                    CellPadding="0">
                    <Columns>
                        <asp:TemplateField HeaderText="Ruoli disponibili">
                            <ItemTemplate>
                                <asp:LinkButton ID="RolesList" runat="server" Text="<%# Container.DataItem.ToString() %>"
                                    EnableTheming="False" OnClick="RolesList_Click" ></asp:LinkButton>
                            </ItemTemplate>
                            <ControlStyle CssClass="HyperlinkSkin_FieldValue" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
            <td style="width:60%">
            <div class="LayGridTitolo1">
                <asp:Label ID="Label1" runat="server" SkinID="GridTitolo1" Text="Utenti associati al ruolo: " Visible="False"></asp:Label>
                <asp:Label ID="LabelUser" runat="server" SkinID="GridTitolo1"></asp:Label></div>
                <asp:GridView runat="server" ID="UsersInRoleGrid" AutoGenerateColumns="False" 
                    OnRowCommand="UsersInRoleGrid_RemoveFromRole" CellPadding="0">
                    <Columns>
                        <asp:TemplateField HeaderText="Username">
                            <ItemTemplate>
                                <%# Container.DataItem.ToString() %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:ButtonField Text="Rimuovi utente" >
                            <ControlStyle CssClass="HyperlinkSkin_FieldValue" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:ButtonField>
                    </Columns>
                </asp:GridView>
            </td>
            <td>
 <div class="LayGridTitolo1"><asp:Label ID="CaptionNewRecordLabel0" 
         SkinID="GridTitolo1" runat="server" Text="Utenti disponibili:"></asp:Label></div>
                <asp:ListBox ID="UsersListBox" SkinID="ListBox_Lst" DataTextField="Username" Rows="10" 
                    SelectionMode="Multiple" runat="server" Width="250px" />
 <div class="VertSpacerMedium"></div>
                <div><asp:Label ID="Msg" runat="server" SkinId="Validazione01"  /></div>
                <div class="VertSpacerMedium"></div>
     <div align="center">
                      <asp:LinkButton ID="AddUsersButton" SkinID="ZSSM_Button01" runat="server" OnClick="AddUsers_OnClick">Aggiungi</asp:LinkButton>
                    <div class="VertSpacerMedium"></div>
            </div>           
            </td>
        </tr>
    </table>
</asp:Content>
