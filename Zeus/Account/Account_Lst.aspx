<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" %>

<%@ Import Namespace="System.Web.Security" %>
<%@ Import Namespace="System.Web.UI" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">
    
    static string TitoloPagina = "Elenco account utente";

    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();

        //if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI")) && (Request.QueryString["XRI"].Length > 0) && (!Request.QueryString["XRI"].Equals("%")))
        //    dsElencoUtenti.SelectCommand += " AND ID2CategoriaUtente=" + Server.HtmlEncode(Request.QueryString["XRI"]);

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "UID")) && (isOK()))
        {
            SingleRecordSqlDataSource.SelectCommand += " WHERE UserId='" + Server.HtmlEncode(Request.QueryString["UID"]) + "'";
            GridView2.DataSourceID = "SingleRecordSqlDataSource";
            GridView2.DataBind();
            SingleRecordPanel.Visible = true;
        }


        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI2")) && (Request.QueryString["XRI2"].Length > 0))
        {
            if (Request.QueryString["XRI2"].Equals("0"))
                dsElencoUtenti.SelectCommand += " AND ZeusIsAlive=0";
            else if (Request.QueryString["XRI2"].Equals("1"))
                dsElencoUtenti.SelectCommand += " AND ZeusIsAlive=1 ";
        }

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI4")) && (Request.QueryString["XRI4"].Length > 0))
        {
            if (!Request.QueryString["XRI4"].Equals("0"))
                dsElencoUtenti.SelectCommand += " AND ID2BusinessBook=" + Server.HtmlEncode(Request.QueryString["XRI4"]);

        }

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI5")) && (Request.QueryString["XRI5"].Length > 0))
        {
            if (!Request.QueryString["XRI5"].Equals("0"))
                dsElencoUtenti.SelectCommand += " AND ID2Categoria1=" + Server.HtmlEncode(Request.QueryString["XRI5"]);

        }

        dsElencoUtenti.SelectCommand += " ORDER BY UserName";

        //Response.Write("DEB "+dsElencoUtenti.SelectCommand+"<br />");

        TitleField.Value = TitoloPagina;


        ReadXML("PROFILI", GetLang());
        ReadXML_Localization("PROFILI", GetLang());

        

    }//fine Page_Load


    private string GetLang()
    {
        string Lang = "ITA";

        delinea myDelinea = new delinea();
        
        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;
        
    }//fine GetLang

    private bool isOK()
    {
        try
        {
            if ((Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/Account/") > 0) || (Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/PageDesigner/Content_New.aspx") > 0))
                return true;
            else return false;
        }
        catch (Exception p)
        {
            return false;
        }

    }//fine isOK



    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode+"/";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Profili.xml"));

            UserList_GridView.Columns[2].HeaderText =
            GridView2.Columns[2].HeaderText =
            mydoc.SelectSingleNode(myXPath + "ZML_Categoria1_Lst").InnerText;


        }
        catch (Exception)
        {
        
        }

    }//fine ReadXML_Localization



    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_Profili.xml"));

            UserList_GridView.Columns[1].Visible =
            GridView2.Columns[1].Visible =
            Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BusinessBook").InnerText);

            UserList_GridView.Columns[2].Visible =
            GridView2.Columns[2].Visible =
            Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Listings").InnerText);


            return true;
        }
        catch (Exception)
        {
            return false;
        }

    }//fine ReadXML



    private string GetCategoria(string ID2Categoria)
    {
        if (ID2Categoria.Length <= 0)
            return string.Empty;

        string Categoria = string.Empty;

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        try
        {

            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();


                sqlQuery = "SELECT CatLiv3";
                sqlQuery += " FROM vwCategorie_All";
                sqlQuery += " WHERE ID1Categoria=" + ID2Categoria;


                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                    if (reader["CatLiv3"] != DBNull.Value)
                        Categoria = reader["CatLiv3"].ToString();
                reader.Close();



            }//fine Using
        }
        catch (Exception p)
        {

            Response.Write(p.ToString());

        }

        return Categoria;

    }//fine GetCategoria

    protected void ImgIco_DataBinding(object sender, EventArgs e)
    {
        Image Ico = (Image)sender;
        if (Ico.ImageUrl == "0")
        {
            Ico.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_Off.gif";
        }
        if (Ico.ImageUrl == "1")
        {
            Ico.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";
        }
    }//fine ImgIco_DataBinding

    protected void ImgBlocco_DataBinding(object sender, EventArgs e)
    {
        Image Ico = (Image)sender;
        if (Ico.ImageUrl == "1")
        {
            Ico.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_Off.gif";
        }
        if (Ico.ImageUrl == "0")
        {
            Ico.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";
        }
    }//fine ImgIco_DataBinding

    //protected void IdentificativoLabel_DataBinding(object sender, EventArgs e)
    //{
    //    Label IdentificativoLabel = (Label)sender;

    //    if (IdentificativoLabel.Text.Length == 0)
    //        IdentificativoLabel.Text = "Non inserita";

    //}//fine IdentificativoLabel_DataBinding
    
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <asp:Panel ID="SingleRecordPanel" runat="server" Visible="false">
        <div class="LayGridTitolo1">
            <asp:Label ID="CaptionNewRecordLabel" SkinID="GridTitolo1" runat="server" Text="Ultimo utente aggiornato"></asp:Label>
        </div>
        <asp:GridView ID="GridView2" runat="server" AllowPaging="True" AllowSorting="True"
            PageSize="100" AutoGenerateColumns="False" EmptyDataText="Nessun record di dati da visualizzare."
            DataKeyNames="UserId">
            <Columns>
                <asp:BoundField DataField="RagioneSociale" HeaderText="Rag. Soc." SortExpression="RagioneSociale">
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="Azienda" SortExpression="Identificativo">
                    <ItemTemplate>
                        <asp:Label ID="IdentificativoLabel" runat="server" Text='<%# Eval("Identificativo", "{0}") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Categoria" SortExpression="ID2Categoria1">
                    <ItemTemplate>
                        <asp:Label ID="ID2CategoriaLabel" runat="server" Text='<%# GetCategoria(Eval("ID2Categoria1").ToString()) %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:BoundField DataField="CognomeNome" HeaderText="Cognome Nome" SortExpression="CognomeNome">
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="UserName" HeaderText="UserName" SortExpression="UserName">
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="E-Mail">
                    <ItemTemplate>
                        <asp:HyperLink ID="HyperLink1" runat="server" SkinID="FieldValue" NavigateUrl='<%# Eval("Email", "mailto:{0}") %>'
                            Text='<%# Eval("Email", "{0}") %>'></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Accesso">
                    <ItemTemplate>
                        <asp:Image ID="ImgIcoAccesso" runat="server" OnDataBinding="ImgIco_DataBinding" ImageUrl='<%# Eval("IsApproved", "{0}") %>' />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Blocco">
                    <ItemTemplate>
                        <asp:Image ID="ImgIcoBlocco" runat="server" OnDataBinding="ImgBlocco_DataBinding"
                            ImageUrl='<%# Eval("IsLockedOut", "{0}") %>' />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Alive">
                    <ItemTemplate>
                        <asp:Image ID="ImgIcoAlive" runat="server" OnDataBinding="ImgIco_DataBinding" ImageUrl='<%# Eval("ZeusIsAlive", "{0}") %>' />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:BoundField DataField="LastLoginDate" HeaderText="Ultimo accesso" SortExpression="LastLoginDate">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:HyperLinkField DataNavigateUrlFields="UserID" DataNavigateUrlFormatString="Account_Dtl.aspx?UID={0}"
                    HeaderText="Account" Text="Dettaglio">
                    <ItemStyle HorizontalAlign="Center" />
                    <ControlStyle CssClass="GridView_ZeusButton1" />
                </asp:HyperLinkField>
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SingleRecordSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
            SelectCommand="SELECT UserID, Email, IsApproved, LastLoginDate, UserName, IsLockedOut, CognomeNome, ID2Categoria1,
            ZeusIsAlive,RagioneSociale,Identificativo FROM vwAccount_Lst_All "></asp:SqlDataSource>
        <div class="VertSpacerMedium">
        </div>
    </asp:Panel>
    <div class="LayGridTitolo1">
        <asp:Label ID="CaptionallRecordLabel" runat="server" SkinID="GridTitolo1" Text="Elenco utenti"></asp:Label>
    </div>
    <asp:GridView ID="UserList_GridView" runat="server" AllowPaging="True" AllowSorting="True"
        PageSize="100" AutoGenerateColumns="False" DataSourceID="dsElencoUtenti" EmptyDataText="Nessun record di dati da visualizzare."
        DataKeyNames="UserId">
        <Columns>
            <asp:BoundField DataField="RagioneSociale" HeaderText="Rag. Soc." SortExpression="RagioneSociale">
                <ItemStyle HorizontalAlign="Left" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="Azienda" SortExpression="Identificativo">
                <ItemTemplate>
                    <asp:Label ID="IdentificativoLabel" runat="server" Text='<%# Eval("Identificativo", "{0}") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Categoria" SortExpression="ID2Categoria1">
                <ItemTemplate>
                    <asp:Label ID="ID2CategoriaLabel" runat="server" Text='<%# GetCategoria(Eval("ID2Categoria1").ToString()) %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" />
            </asp:TemplateField>
            <asp:BoundField DataField="CognomeNome" HeaderText="Cognome Nome" SortExpression="CognomeNome">
                <ItemStyle HorizontalAlign="Left" />
            </asp:BoundField>
            <asp:BoundField DataField="UserName" HeaderText="UserName" SortExpression="UserName">
                <ItemStyle HorizontalAlign="Left" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="E-Mail">
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLink1" runat="server" SkinID="FieldValue" NavigateUrl='<%# Eval("Email", "mailto:{0}") %>'
                        Text='<%# Eval("Email", "{0}") %>'></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Accesso">
                <ItemTemplate>
                    <asp:Image ID="ImgIcoAccesso" runat="server" OnDataBinding="ImgIco_DataBinding" ImageUrl='<%# Eval("IsApproved", "{0}") %>' />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Blocco">
                <ItemTemplate>
                    <asp:Image ID="ImgIcoBlocco" runat="server" OnDataBinding="ImgBlocco_DataBinding"
                        ImageUrl='<%# Eval("IsLockedOut", "{0}") %>' />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Alive">
                <ItemTemplate>
                    <asp:Image ID="ImgIcoAlive" runat="server" OnDataBinding="ImgIco_DataBinding" ImageUrl='<%# Eval("ZeusIsAlive", "{0}") %>' />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:BoundField DataField="LastLoginDate" HeaderText="Ultimo accesso" SortExpression="LastLoginDate">
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:HyperLinkField DataNavigateUrlFields="UserID" DataNavigateUrlFormatString="Account_Dtl.aspx?UID={0}"
                HeaderText="Account"  Text="Dettaglio">
                <ItemStyle HorizontalAlign="Center" />
                <ControlStyle CssClass="GridView_ZeusButton1" />
            </asp:HyperLinkField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="dsElencoUtenti" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SELECT UserID, Email, IsApproved, LastLoginDate, UserName, IsLockedOut, CognomeNome, 
        ZeusIsAlive,RagioneSociale,Identificativo ,ID2Categoria1
        FROM vwAccount_Lst_All 
        WHERE (UserName LIKE '%' + @UserName + '%') AND (Email LIKE '%' + @Email + '%') 
        AND (IsApproved LIKE @IsApproved) AND (IsLockedOut LIKE @IsLockedOut) 
        AND (RicercaUtenti LIKE '%' + @RicercaUtenti + '%') AND (RagioneSociale LIKE '%' + @RagioneSociale + '%') 
        AND (Cognome LIKE '%' + @Cognome + '%') AND (Nome LIKE '%' + @Nome + '%') ">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="%" Name="UserName" QueryStringField="UserName"
                Type="String" />
            <asp:QueryStringParameter DefaultValue="%" Name="Email" QueryStringField="EMail"
                Type="String" />
            <asp:QueryStringParameter DefaultValue="%" Name="IsApproved" QueryStringField="Accesso" />
            <asp:QueryStringParameter DefaultValue="%" Name="IsLockedOut" QueryStringField="Blocco" />
            <asp:QueryStringParameter DefaultValue="%" Name="RicercaUtenti" QueryStringField="XRE"
                Type="String" />
            <asp:QueryStringParameter DefaultValue="%" QueryStringField="XRE1" Name="RagioneSociale"
                Type="String" />
            <asp:QueryStringParameter DefaultValue="%" QueryStringField="XRE2" Name="Cognome"
                Type="String" />
            <asp:QueryStringParameter DefaultValue="%" QueryStringField="XRE3" Name="Nome" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
