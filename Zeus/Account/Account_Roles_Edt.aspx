<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" Theme="Zeus" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">


    static string TitoloPagina = "Modifica ruoli utente";


    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();
        TitleField.Value = TitoloPagina;

        if (!myDelinea.AntiSQLInjection(Request.QueryString))
            Response.Redirect("/Zeus/System/Message.aspx");

        if (!Page.IsPostBack)
        {
            Label UserName_Label = (Label)MemberShipFormView.FindControl("UserName_Label");

            RuoliDaAssegnare.DataSource = ClearRoles(UserName_Label.Text);
            RuoliDaAssegnare.DataBind();

            RuoliAssegnati.DataSource = GetRuoliAssegnati(UserName_Label.Text);
            RuoliAssegnati.DataBind();


            if (!isOK())
                Response.Redirect("~/Zeus/System/Message.aspx?Msg=7867868458426388&VfC=HKELP&Lang=ITA");
        }

        Msg.Text = "";

    }//fine Page_Load

    private bool isOK()
    {
        try
        {
            if (Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/Account/Account_Dtl.aspx") > 0
                || Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/Soci/Soci_Dsp.aspx") > 0)
                return true;
            else return false;
        }
        catch (Exception p)
        {
            return false;
        }

    }//fine isOK


    private bool CheckIsAdminUser(string UserName)
    {
        bool IsAdminUser = false;

        try
        {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(
               strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT RoleName FROM tbZeusRoles WHERE IsAdminRole = 1 ORDER BY RoleName";
                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    // Response.Write("DEB roleName:" + reader["RoleName"].ToString() + " is in role:" + Roles.IsUserInRole(UserName, reader["RoleName"].ToString())+"<br />");

                    if ((reader["RoleName"] != DBNull.Value)
                      && (Roles.IsUserInRole(UserName, reader["RoleName"].ToString())))
                    {
                        IsAdminUser = true;
                        break;
                    }
                }


                reader.Close();

            }//fine using


        }
        catch (Exception p)
        {
            Response.Write(p.ToString());

        }

        return IsAdminUser;


    }//fine CheckIsAdminUser


    private string[] AddOtherRoles()
    {
        string[] OtherRoles = null;

        try
        {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(
               strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT RoleName FROM tbZeusRoles WHERE IsAdminRole= 0 ORDER BY RoleName";
                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();
                ArrayList myArray = new ArrayList();

                while (reader.Read())
                {
                    //Response.Write("DEB roleName:" + reader["RoleName"].ToString() + " <br />");

                    if (reader["RoleName"] != DBNull.Value)
                        myArray.Add(reader["RoleName"].ToString());
                }

                OtherRoles = (string[])myArray.ToArray(typeof(string));

                reader.Close();

            }//fine using


        }
        catch (Exception p)
        {
            Response.Write(p.ToString());

        }

        return OtherRoles;

    }//fine AddOtherRoles

    private string[] ClearRoles(string UserName)
    {
        string[] myAllRoles = null;
        string[] myUserRoles = Roles.GetRolesForUser(UserName);

        if (CheckIsAdminUser(Membership.GetUser(true).UserName))
        {
            myAllRoles = Roles.GetAllRoles();
            if (!Roles.IsUserInRole(Membership.GetUser(true).UserName, "ZeusAdmin"))
            {
                int index = Array.IndexOf(myAllRoles, "ZeusAdmin");

                if (index != -1)
                {

                    string[] copyStrArr = new string[myAllRoles.Length - 1];

                    // copy the elements before the found index
                    for (int i = 0; i < index; i++)
                    {
                        copyStrArr[i] = myAllRoles[i];
                    }

                    // copy the elements after the found index
                    for (int i = index; i < copyStrArr.Length; i++)
                    {
                        copyStrArr[i] = myAllRoles[i + 1];
                    }

                    myAllRoles = copyStrArr;

                }
            }

        }
        else myAllRoles = AddOtherRoles();

        ArrayList myArray = new ArrayList();

        bool IsInRole = false;

        for (int i = 0; i < myAllRoles.Length; ++i)
        {
            for (int j = 0; j < myUserRoles.Length; ++j)
            {
                if (myAllRoles[i].Equals(myUserRoles[j]))
                    IsInRole = true;
            }

            if (!IsInRole)
                myArray.Add(myAllRoles[i]);

            IsInRole = false;
        }

        return (string[])myArray.ToArray(typeof(string)) as string[];
    }//fine ClearRoles


    private string[] GetRuoliAssegnati(string UserName)
    {
        string[] myUserRoles = Roles.GetRolesForUser(UserName);

        if (!Roles.IsUserInRole(Membership.GetUser(true).UserName, "ZeusAdmin"))
        {
            int index = Array.IndexOf(myUserRoles, "ZeusAdmin");

            if (index != -1)
            {

                string[] copyStrArr = new string[myUserRoles.Length - 1];

                // copy the elements before the found index
                for (int i = 0; i < index; i++)
                {
                    copyStrArr[i] = myUserRoles[i];
                }

                // copy the elements after the found index
                for (int i = index; i < copyStrArr.Length; i++)
                {
                    copyStrArr[i] = myUserRoles[i + 1];
                }

                myUserRoles = copyStrArr;

            }
        }

        return myUserRoles;

    }//fine GetRuoliAssegnati



    protected void AddUsers_OnClick(object sender, EventArgs args)
    {
        Label UserName_Label = (Label)MemberShipFormView.FindControl("UserName_Label");



        if (RuoliDaAssegnare.SelectedItem == null)
        {
            Msg.Text = "Selezionare un ruolo da aggiungere";

        }
        try
        {

            bool isSocio = false;
            for (int i = 0; i < RuoliDaAssegnare.Items.Count; ++i)
                if (RuoliDaAssegnare.Items[i].Selected)
                {
                    Roles.AddUserToRole(UserName_Label.Text, RuoliDaAssegnare.Items[i].Value);
                    if (RuoliDaAssegnare.Items[i].Value == "soci" || RuoliDaAssegnare.Items[i].Value.ToLower() == "socionorari")
                        isSocio = true;
                }

            RuoliAssegnati.DataSource = GetRuoliAssegnati(UserName_Label.Text);
            RuoliAssegnati.DataBind();
            RuoliDaAssegnare.DataSource = ClearRoles(UserName_Label.Text);
            RuoliDaAssegnare.DataBind();


            //SALVO IL RUOLO SOC IN tbProfiliMarketing            
            try
            {
                String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

                string sqlQuery = string.Empty;

                using (SqlConnection connection = new SqlConnection(
                   strConnessione))
                {

                    connection.Open();
                    SqlCommand command = connection.CreateCommand();


                    var userId = Membership.GetUser(UserName_Label.Text).ProviderUserKey;
                    string statusSocio = ((string[])RuoliAssegnati.DataSource).FirstOrDefault(x => x.ToLower().Equals("soci") || x.ToLower().Equals("socionorari")) != null ? "SOC" : null;

                    sqlQuery = @"UPDATE tbProfiliMarketing
                                SET StatusSocio = @Status
                                WHERE UserId = @UserId";
                    command.Parameters.Clear();
                    command.CommandText = sqlQuery;
                    command.Parameters.Add("@Status", System.Data.SqlDbType.VarChar);
                    command.Parameters["@Status"].Value = statusSocio;
                    command.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
                    command.Parameters["@UserId"].Value = userId;

                    command.ExecuteNonQuery();



                    SertotUtility c = new SertotUtility();
                    string nuovoCodiceSocio = c.NuovoCodiceSocio();

                    sqlQuery = @"UPDATE tbProfiliMarketing
                                SET [CodiceSocioNum] = @CodiceSocioNum, [CodiceSocioStr] = @CodiceSocioStr
                                WHERE UserId = @UserId AND CodiceSocioStr IS NULL AND CodiceSocioNum IS NULL";
                    command.Parameters.Clear();
                    command.CommandText = sqlQuery;
                    command.Parameters.Add("@CodiceSocioNum", System.Data.SqlDbType.Int);
                    command.Parameters["@CodiceSocioNum"].Value = Convert.ToInt32(nuovoCodiceSocio);
                    command.Parameters.Add("@CodiceSocioStr", System.Data.SqlDbType.NVarChar);
                    command.Parameters["@CodiceSocioStr"].Value = nuovoCodiceSocio;
                    command.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
                    command.Parameters["@UserId"].Value = userId;
                    
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
            }
        }
        catch (Exception e)
        {
            Msg.IsValid = false;

        }

    }//AddUsers_OnClick

    protected void DeleteUsers_OnClick(object sender, EventArgs args)
    {
        Label UserName_Label = (Label)MemberShipFormView.FindControl("UserName_Label");
        if (RuoliAssegnati.SelectedItem == null)
        {
            Msg.Text = "Selezionare un ruolo da rimuovere";

        }
        try
        {

            for (int i = 0; i < RuoliAssegnati.Items.Count; ++i)
                if (RuoliAssegnati.Items[i].Selected)
                    Roles.RemoveUserFromRole(UserName_Label.Text, RuoliAssegnati.Items[i].Value);

            RuoliAssegnati.DataSource = GetRuoliAssegnati(UserName_Label.Text);
            RuoliAssegnati.DataBind();
            RuoliDaAssegnare.DataSource = ClearRoles(UserName_Label.Text);
            RuoliDaAssegnare.DataBind();
        }
        catch (Exception e)
        {
            Msg.IsValid = false;

        }
    }//DeleteUsers_OnClick

    protected void BackButton_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
            BackButton.PostBackUrl += "?UID=" + Request.QueryString["UID"].ToString();

    }


</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <asp:FormView ID="MemberShipFormView" runat="server" DefaultMode="ReadOnly" DataSourceID="MemberShipSqlDataSource"
        Width="100%" CellPadding="0">
        <ItemTemplate>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="AccountUtenteLabel" runat="server" Text="Account utente"></asp:Label>
                </div>
                <table border="0" cellpadding="2" cellspacing="1">
                    <tr>
                        <td>
                            <asp:Label SkinID="FieldDescription" ID="Label1" runat="server" Text="Label">UserName:</asp:Label>
                            <asp:Label SkinID="FieldValue" ID="UserName_Label" runat="server" Text='<%# Eval("UserName", "{0}") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label SkinID="FieldDescription" ID="EMail_Label" runat="server" Text="Label">E-mail: </asp:Label>
                            <asp:HyperLink SkinID="FieldValue" ID="EmailHyperLink" CssClass="HyperLink1" runat="server"
                                NavigateUrl="mailto:" Text='<%# Eval("Email", "{0}") %>'></asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="DataCreazioneDesc_Label" SkinID="FieldDescription" runat="server"
                                Text="Label">Data creazione:</asp:Label>
                            <asp:Label ID="DataCreazioneLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("CreateDate", "{0}") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="UserIDDescr_Label" SkinID="FieldDescription" runat="server" Text="Label">User ID:</asp:Label>
                            <asp:Label ID="UserIDLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("Expr1", "{0}") %>'></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </ItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="MemberShipSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SELECT aspnet_Membership.Password, aspnet_Membership.Email
        , aspnet_Membership.IsApproved, aspnet_Membership.CreateDate, aspnet_Membership.LastLoginDate
        , aspnet_Membership.LastPasswordChangedDate
        , aspnet_Membership.LastLockoutDate, aspnet_Membership.FailedPasswordAttemptCount, aspnet_Users.UserName
        , aspnet_Users.UserId AS Expr1, aspnet_Membership.IsLockedOut FROM aspnet_Membership INNER JOIN aspnet_Users ON aspnet_Membership.UserId = aspnet_Users.UserId WHERE (aspnet_Users.UserId = @UserID)">
        <SelectParameters>
            <asp:QueryStringParameter Name="UserID" QueryStringField="UID" DefaultValue="0" />
        </SelectParameters>
    </asp:SqlDataSource>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td valign="bottom">
                <asp:Label runat="server" ID="Ruoli_Label" Font-Bold="true" Text="Ruoli assegnati all'utente" SkinID="FieldDescription"></asp:Label>
            </td>
            <td width="50" valign="top"></td>
            <td valign="bottom">
                <asp:Label runat="server" ID="RuoliDisp_Label" SkinID="FieldDescription" Font-Bold="true" Text="Ruoli disponibili"></asp:Label>
            </td>
            <td></td>
        </tr>
        <tr>
            <td valign="top" width="180">
                <asp:ListBox ID="RuoliAssegnati" SelectionMode="Multiple" runat="server" SkinID="ListBox_Lst" Rows="8" Width="180"></asp:ListBox>

            </td>
            <td width="50" valign="top"></td>
            <td valign="top" width="180">
                <asp:ListBox ID="RuoliDaAssegnare" SelectionMode="Multiple" runat="server" SkinID="ListBox_Lst" Rows="8" Width="180"></asp:ListBox>
            </td>
            <td></td>
        </tr>
        <tr>
            <td valign="top">
                <div class="VertSpacerBig"></div>
                <asp:LinkButton SkinID="ZSSM_Button01" ID="DeleteUserButton" runat="server" OnClick="DeleteUsers_OnClick">Rimuovi ruoli selezionati > </asp:LinkButton><div class="VertSpacerMedium"></div>
            </td>
            <td width="50" valign="top"></td>
            <td valign="top" width="200">
                <div class="VertSpacerBig"></div>
                <asp:LinkButton SkinID="ZSSM_Button01" ID="AddUsersButton" runat="server" OnClick="AddUsers_OnClick">< Aggiungi ruoli selezionati</asp:LinkButton><div class="VertSpacerMedium"></div>
            </td>
            <td>
                <div class="VertSpacerBig"></div>
                <asp:LinkButton SkinID="ZSSM_Button01" ID="BackButton" runat="server" OnLoad="BackButton_Load" PostBackUrl="/Zeus/Account/Account_Dtl.aspx">< Ritorna al dettaglio utente</asp:LinkButton><div class="VertSpacerMedium"></div>
            </td>
        </tr>
        <tr>
            <td colspan="4" valign="top">
                <asp:CustomValidator ID="Msg" runat="server" SkinID="ZSSM_Validazione01" />
            </td>
        </tr>
    </table>
</asp:Content>
