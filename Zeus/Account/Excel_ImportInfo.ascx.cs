﻿using System;
using System.Xml.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using CustomWebControls;
using ASP;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Collections;
using System.Text.RegularExpressions;
using GemBox.Spreadsheet;

/// <summary>
/// Descrizione di riepilogo per ImportAccount_ExcellInfo
/// </summary>
public partial class ImportAccount_ExcelInfo : System.Web.UI.UserControl
{
    static string[] OurColumn = 
    { 
        "COGNOME"
        ,"NOME"
        ,"EMAIL"
        ,"UserName"
        ,"Password"
        ,"CodiceFiscale"
        ,"Qualifica"
        ,"Professione"
        ,"Sesso"
        ,"Indirizzo"
        ,"Citta"
        ,"Cap"
        ,"ProvinciaEstesa"
        ,"ProvinciaSigla"
        ,"NascitaData"
        ,"NascitaCitta"
        ,"NascitaProvinciaSigla"
        ,"Telefono1Personale"
        ,"Telefono2Personale"
        ,"FaxPersonale"
        ,"Note"
        ,"Jolly1Personale"
        ,"Jolly2Personale"
        ,"RagioneSociale"
        ,"PartitaIVA"
        ,"S1_Indirizzo"
        ,"S1_Citta"
        ,"S1_Comune"
        ,"S1_Cap"
        ,"S1_ProvinciaEstesa"
        ,"S1_ProvinciaSigla"
        ,"S2_Indirizzo"
        ,"S2_Citta"
        ,"S2_Comune"
        ,"S2_Cap"
        ,"S2_ProvinciaEstesa"
        ,"S2_ProvinciaSigla"
        ,"Telefono1"
        ,"Telefono2"
        ,"Fax"
        ,"Website"
        ,"Jolly1Societario"
        ,"Jolly2Societario"
        ,"ConsensoDatiPersonali"
        ,"ConsensoDatiPersonali2"
        ,"Comunicazioni1"
        ,"Comunicazioni2"
        ,"Comunicazioni3"
        ,"Comunicazioni4"
        ,"Comunicazioni5"
        ,"Comunicazioni6"
        ,"Comunicazioni7"
        ,"Comunicazioni8"
        ,"Comunicazioni9"
        ,"Comunicazioni10"
        ,"Jolly1Marketing"
        ,"Jolly2Marketing"
        ,"Meta_ZeusSearch"
    };

    private string myPath = string.Empty;
	private string UsedColumns = string.Empty;

    public string Path
    {
        get { return myPath; }
        set { myPath = value; }
    }

    private string GetExtension(string FileName)
    {
        string[] split = FileName.Split('.');
        string Extension = split[split.Length - 1];
        return Extension;
    }

    public string BindTheData()
    {
        string myColum = string.Empty;

        if (myPath.Length <= 0)
            return myColum;

        try
        {
            SpreadsheetInfo.SetLicense("E0YT-QK22-WFBB-4JRL");
            ExcelFile FileExcel = new ExcelFile();

            if (GetExtension(myPath) == "xls")
                FileExcel.LoadXls(Server.MapPath(myPath));
            else
                FileExcel.LoadXlsx(Server.MapPath(myPath), XlsxOptions.PreserveMakeCopy);

                if (FileExcel.Worksheets[0] == null)
                    return string.Empty;

                if (FileExcel.Worksheets[0].Rows[0] == null)
                    return string.Empty;

                Label myLabel = null;

                this.Controls.Clear();
                this.Controls.Add(new LiteralControl("<table>"));
                this.Controls.Add(new LiteralControl("<tr>"));
                this.Controls.Add(new LiteralControl("<td colspan=\"2\">"));
                myLabel = new Label();
                myLabel.Text = "Elenco colonne per l'importazione";
                myLabel.CssClass = "LabelSkin_GridTitolo1";
                this.Controls.Add(myLabel);
                this.Controls.Add(new LiteralControl("</td>"));
                this.Controls.Add(new LiteralControl("</tr>"));

                bool[] myControl = new bool[OurColumn.Length];

                for (int i = 0; i < myControl.Length; ++i)
                    myControl[i] = false;

                for (int j = 0; j < FileExcel.Worksheets[0].CalculateMaxUsedColumns(); j++)
                {
                    for (int i = 0; i < OurColumn.Length; ++i)
                    {
                        if (FileExcel.Worksheets[0].Cells[j].Value.ToString().ToUpper().Trim().Equals(OurColumn[i].ToUpper()))
                        {
                            myControl[i] = true;
							
                            if (UsedColumns.Length == 0)
                                UsedColumns = OurColumn[i];
                            else
                                UsedColumns += "," + OurColumn[i];
                        }
                    }
                }

                for (int i = 0; i < OurColumn.Length; ++i)
                {
                    this.Controls.Add(new LiteralControl("<tr>"));
                    this.Controls.Add(new LiteralControl("<td>"));
                    myLabel = new Label();
                    myLabel.Text = OurColumn[i].ToUpper();
                    myLabel.CssClass = "LabelSkin_CorpoTesto";
                    this.Controls.Add(myLabel);
                    this.Controls.Add(new LiteralControl("</td>"));
                    this.Controls.Add(new LiteralControl("<td>"));

                    if (myControl[i])
                    {
                        this.Controls.Add(new LiteralControl("<img src=\"../SiteImg/Ico1_StatusOk.gif\" />"));
                        myColum += OurColumn[i];

                        if (i + 2 < OurColumn.Length)
                            myColum += ",";
                    }
                    else
                        this.Controls.Add(new LiteralControl("<img src=\"../SiteImg/Ico1_StatusKo.gif\" />"));

                    this.Controls.Add(new LiteralControl("</td>"));
                    this.Controls.Add(new LiteralControl("</tr>"));
                }
                this.Controls.Add(new LiteralControl("</table>"));            
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

        return UsedColumns;
    }

    public string PrintFields()
    {
        string str="";
        for (int ii = 0; ii < OurColumn.Length; ii++)
        {
            str += OurColumn[ii] + "<br />";
        }
        return str;
    }
}