<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" %>
    
    <%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">

    static string TitoloPagina = "Ricerca account utente";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;
        mymySetFocus(SearchButton.UniqueID);

        ReadXML("PROFILI", GetLang());
        ReadXML_Localization("PROFILI", GetLang());
        
    }// Page_Load



    private string GetLang()
    {
        string Lang = "ITA";

        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;

    }//fine GetLang



    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Profili.xml"));


            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {

                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                            if (myLabel != null)
                                myLabel.Text = childNode.InnerText;

                        }
                        catch (Exception)
                        {

                        }
                    }//fine foreach
                }//fine foreach

            }//fine for
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine ReadXML_Localization



    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_Profili.xml"));


          
            if (ZMCF_BusinessBook != null)
                ZMCF_BusinessBook.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BusinessBook").InnerText);


            if (ZMCF_Categoria1 != null)
                ZMCF_Categoria1.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Categoria1").InnerText);
            

            SetQueryOfID2CategoriaDropDownList(mydoc.SelectSingleNode(myXPath + "ZMCD_ZIMCategoria1").InnerText
                          , GetLang(), mydoc.SelectSingleNode(myXPath + "ZMCD_Cat1Liv").InnerText);

            return true;
        }
        catch (Exception)
        {
            return false;
        }

    }//fine ReadXML


    //##############################################################################################################
    //################################################ CATEGORIE ###################################################
    //############################################################################################################## 

    private bool SetQueryOfID2CategoriaDropDownList(string ZeusIdModulo,
        string ZeusLangCode, string PZV_Cat1Liv)
    {
        try
        {



            switch (PZV_Cat1Liv)
            {

                case "123":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv1Liv2Liv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "Catliv1Liv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;


                case "23":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv2Liv3]";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;

                default:
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;
            }

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }//fine SetQueryOfID2CategoriaDropDownList






    //##############################################################################################################
    //############################################ FINE CATEGORIE ##################################################
    //############################################################################################################## 

    private void mymySetFocus(string ID)
    {
        Page.Form.DefaultButton = ID;
        Page.Form.DefaultFocus = ID;

    }//fine mymySetFocus

    protected void SearchButton_Click(object sender, EventArgs e)
    {

        string myRedirect = "Account_Lst.aspx?UserName=" + UsarNameTextBox.Text.ToString() +
            "&EMail=" + EMailTextBox.Text.ToString() +
          
            "&XRE1=" + AziendaTextBox.Text +
            "&XRE2=" + CognomeTextBox.Text +
            "&XRE3=" + NomeTextBox.Text +
            "&XRI4=" + AziendaCollegataDropDownList.SelectedValue +
            "&XRI5=" + ID2CategoriaDropDownList.SelectedValue;

        if (!AliveDropDownList.SelectedValue.Equals("%"))
            myRedirect += "&XRI2=" + AliveDropDownList.SelectedValue;

        if (!AccessoDropDownList.SelectedValue.Equals("%"))
            myRedirect += "&Accesso=" + AccessoDropDownList.SelectedValue;


        if (!BloccoDropDownList.SelectedValue.Equals("%"))
            myRedirect += "&Blocco=" + BloccoDropDownList.SelectedValue;
        
        Response.Redirect(myRedirect);
        
    }//fine SearchButton_Click
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
   <div class="BlockBox">
   <div class="BlockBoxHeader"><asp:Label ID="Ricerca_Label" runat="server" Text="Ricerca account utente"></asp:Label></div>
<table>
            <tr>
            <td class="BlockBoxDescription">
                <asp:Label ID="Azienda_Label" runat="server"  SkinID="FieldDescription" Text="Ragione sociale"></asp:Label>
            </td>
            <td class="BlockBoxValue">
                <asp:TextBox ID="AziendaTextBox" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
            </td>
        </tr>
        <asp:Panel ID="ZMCF_Categoria1" runat="server">
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="ZML_Categoria1" SkinID="FieldDescription" runat="server" Text="Categoria *"></asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:DropDownList ID="ID2CategoriaDropDownList" runat="server" AppendDataBoundItems="True">
                            <asp:ListItem Text="> Tutte" Value="0" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="dsCategoria" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>">
                        </asp:SqlDataSource>
                    </td>
                </tr>
            </asp:Panel>
        <asp:Panel ID="ZMCF_BusinessBook" runat="server">
          <tr>
            <td class="BlockBoxDescription">
                <asp:Label ID="Label1" runat="server"  SkinID="FieldDescription" Text="Azienda collegata "></asp:Label>
            </td>
            <td class="BlockBoxValue">
         <asp:DropDownList ID="AziendaCollegataDropDownList" runat="server" DataSourceID="AziendaCollegataSqlDataSource"
                            DataTextField="Identificativo" DataValueField="ID1BsnBook" AppendDataBoundItems="true">
                            <asp:ListItem Text="&gt; Tutte" Value="0" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="AziendaCollegataSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                            SelectCommand="SELECT [ID1BsnBook],[Identificativo]
                                FROM [tbBusinessBook]
                                WHERE [PW_Zeus1]=1"></asp:SqlDataSource>
                                </td>
                                </tr>
                                </asp:Panel>
        
        <tr>
            <td class="BlockBoxDescription">
                <asp:Label ID="Cognome_Label" runat="server"  SkinID="FieldDescription" Text="Cognome"></asp:Label>
            </td>
            <td class="BlockBoxValue">
                <asp:TextBox ID="CognomeTextBox" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="BlockBoxDescription">
                <asp:Label ID="Nome_Label" runat="server"  SkinID="FieldDescription" Text="Nome"></asp:Label>
            </td>
            <td class="BlockBoxValue">
                <asp:TextBox ID="NomeTextBox" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="BlockBoxDescription">
                <asp:Label ID="UserName_Label" runat="server" SkinID="FieldDescription" Text="UserName"></asp:Label>
            </td>
            <td class="BlockBoxValue">
                <asp:TextBox ID="UsarNameTextBox" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="BlockBoxDescription">
                <asp:Label ID="EMail_Label" runat="server"  SkinID="FieldDescription" Text="E-Mail"></asp:Label>
            </td>
            <td class="BlockBoxValue">
                <asp:TextBox ID="EMailTextBox" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="BlockBoxDescription">
                <asp:Label ID="Alive_Label" runat="server"  SkinID="FieldDescription" Text="Alive"></asp:Label>
            </td>
            <td class="BlockBoxValue">
                 <asp:DropDownList ID="AliveDropDownList" runat="server" AppendDataBoundItems="True">
                    <asp:ListItem Value="%">&gt; Tutti</asp:ListItem>
                    <asp:ListItem Value="1" Selected="True">Account presenti</asp:ListItem>
                    <asp:ListItem Value="0">Account eliminati</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="BlockBoxDescription">
                <asp:Label ID="Accesso_Label" runat="server"  SkinID="FieldDescription" Text="Accesso"></asp:Label>
            </td>
            <td class="BlockBoxValue">
                <asp:DropDownList ID="AccessoDropDownList" runat="server" AppendDataBoundItems="True">
                    <asp:ListItem Value="%">&gt; Tutti</asp:ListItem>
                    <asp:ListItem Value="1">Solo consentito</asp:ListItem>
                    <asp:ListItem Value="0">Solo negato</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="BlockBoxDescription">
                <asp:Label ID="Blocco_Label" runat="server"  SkinID="FieldDescription" Text="Blocco"></asp:Label>
            </td>
            <td class="BlockBoxValue">
                <asp:DropDownList ID="BloccoDropDownList" runat="server">
                    <asp:ListItem Value="%">&gt; Tutti</asp:ListItem>
                    <asp:ListItem Value="1">Solo bloccati</asp:ListItem>
                    <asp:ListItem Value="0">Solo sbloccati</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr><td colspan="2"></td></tr>
        <tr><td class="BlockBoxDescription"></td><td class="BlockBoxValue"><asp:LinkButton ID="SearchButton" SkinID="ZSSM_Button01" runat="server" Text="Cerca" OnClick="SearchButton_Click"></asp:LinkButton></td></tr>
    </table></div>
</asp:Content>
