﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" 
    Title="Untitled Page" CodeFile="Excel_Import.aspx.cs"
    Inherits="ImportAccount_Excel"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ OutputCache Duration="1" NoStore="true"  VaryByParam="none"%>
<%@ Register Src="Excel_ImportInfo.ascx" TagName="ImportAccount_ExcelInfo"
    TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">

    <script language="javascript" type="text/javascript">
              
       var isLoadButton=false;
       
        window.onbeforeunload = function() {
        
         if(isLoadButton)
         {
            $.blockUI({
                message: '<h1><img src="/SiteImg/wait.gif" /> Caricamento...</h1>',
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: '.5',
                    color: '#fff'
                }
            });
          }
        }
        
        function SetIsLoadButtonToTrue()
        {
          isLoadButton=true;
        }
        
    </script>

    <asp:HiddenField ID="TitleField" runat="server" />
    <asp:HiddenField ID="CheckedColumnsHiddenField" runat="server" />
    <asp:HiddenField ID="PathHiddenField" runat="server" />
    <asp:HiddenField ID="ImportCodeHiddenField" runat="server" />
    <br />
    <table>
        <tr>
            <td>
                <asp:FileUpload ID="FileUpload1" runat="server" />            </td>
            <td>
                <asp:Button ID="LoadButton" runat="server" Text="Carica" OnClick="LoadButton_Click" />            </td>
        </tr>
         <tr>
            <td colspan="2">
                <br /><a href="ImportTemplate.xls" target="_blank">Download template xml</a>       </td>
        </tr>
         <tr>
            <td colspan="2">
                <asp:Label ID="FieldsLabel" runat="server"></asp:Label>       </td>
        </tr>
    </table>
    <uc1:ImportAccount_ExcelInfo ID="ImportAccount_ExcelInfo1" runat="server" />
    <br />
    <asp:Label ID="InfoLabel" runat="server" SkinID="Validazione01"></asp:Label>
    <br />
    <asp:Button ID="VerificaButton" runat="server" Text="Verifica file caricato" OnClick="VerificaButton_Click"
        Visible="false" />
    <br />
    <asp:Label ID="ImportCodeLabel" runat="server" SkinID="GridTitolo1" Visible="false"></asp:Label>
    <br />
    <asp:Label ID="FinishLabel" runat="server" SkinID="CorpoTesto"  Font-Bold="true" Visible="false"></asp:Label>
    <asp:Button ID="CreateUserButton" runat="server" Text="Crea utenti" OnClick="CreateUserButton_Click"
        Visible="false" />
</asp:Content>
