﻿using System;
using System.Web;
using System.Data.SqlClient;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Collections;
using System.Text.RegularExpressions;
using GemBox.Spreadsheet;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.IO;

public partial class Zeus_Account_Excel_ExportSoci : System.Web.UI.Page
{
    static string TitoloPagina = "Esportazione soci su file Excel";

    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;

    }

    protected void EsportaButton_Click(object sender, EventArgs e)
    {
        DataTable Users = new DataTable();
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;
        SpreadsheetInfo.SetLicense("E0YT-QK22-WFBB-4JRL");
        ExcelFile ef = new ExcelFile();
        ExcelWorksheet ws = ef.Worksheets.Add("DataSheet");
        List<ExportUsers> UserToImportList = new List<ExportUsers>();
        ExportUsers UserToImport;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {
            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = @"SELECT ExportSociDaily.Cognome, ExportSociDaily.Nome, ExportSociDaily.VIA, ExportSociDaily.Cap, ExportSociDaily.Citta, ExportSociDaily.PROV, 
	                                ExportSociDaily.TEL, ExportSociDaily.Fax, ExportSociDaily.Email, ExportSociDaily.cellulare, ExportSociDaily.[secondo recapito], ExportSociDaily.tipologia, ExportSociDaily.[CODICE FISCALE],
	                                ExportSociDaily.QuotaAssociativa AS [Q Sertot], '' AS [Q ARRETR Sertot], '' AS [Q ANTIC Sertot],dbo.tbMetodiPagamento.MetodoPagamento, '' AS [MOD PAG Q Sertot arretrata], '' AS [MOD PAG Q Sertot anticipata], '' AS [data pagamento Sertot],
	                                '' AS [RIC ARR Sertot], '' AS [RIC ANT Sertot], dbo.tbQuoteAnnuali.Anno AS [ULT PAG Sertot], '' AS [NOTE Sertot], '' AS [RIC Sertot], '' AS [data Ric Antic Sertot], '' AS [Spedita ricevuta Sertot]
                            FROM dbo.tbQuote 
	                                INNER JOIN dbo.tbQuoteAnnuali ON dbo.tbQuote.IDQuota = dbo.tbQuoteAnnuali.ID2QuotaCosto
	                                INNER JOIN dbo.vwMaxAnno ON dbo.tbQuoteAnnuali.UserID = dbo.vwMaxAnno.UserID AND dbo.tbQuoteAnnuali.Anno = dbo.vwMaxAnno.MaxAnno 
	                                INNER JOIN dbo.tbMetodiPagamento ON dbo.tbQuoteAnnuali.ID2MetodoPagamento = dbo.tbMetodiPagamento.ID1MetodoPagamento 
	                                INNER JOIN dbo.vwSoci_Dtl ON dbo.tbQuoteAnnuali.UserID = dbo.vwSoci_Dtl.UserId 
	                                INNER JOIN dbo.vwQuote_Lst ON dbo.tbQuoteAnnuali.ID2QuotaCosto = dbo.vwQuote_Lst.IDQuota

	                        OUTER APPLY (
		                        SELECT TOP(1) * 
		                            FROM dbo.ExportSociDaily 
		                            WHERE dbo.vwSoci_Dtl.Cognome = dbo.ExportSociDaily.Cognome AND dbo.vwSoci_Dtl.Nome = dbo.ExportSociDaily.Nome) ExportSociDaily
                                ORDER BY ExportSociDaily.Cognome";

                DataSet ds = new DataSet();
                SqlDataAdapter daCust = new SqlDataAdapter(sqlQuery, connection);
                daCust.Fill(ds);

                DataTable myDataTable = ds.Tables[0].Copy();

                string ticks = DateTime.Now.ToShortDateString();
                string filename = "ExportSociDaily" + ticks;
                string filenameEstensione = filename + ".xls";

                try
                {                    
                    RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
                    objExport.ExportDetails(myDataTable, RKLib.ExportData.Export.ExportFormat.Excel, filenameEstensione);
                }
                catch (Exception ex)
                {
                    throw ex;
                }



                //command.CommandText = sqlQuery;
                //SqlDataReader reader = command.ExecuteReader();               

                //while (reader.Read())
                //{
                //    UserToImport = new ExportUsers();

                //    if (reader["COGNOME"] != DBNull.Value)
                //        UserToImport.Cognome = reader["COGNOME"].ToString();

                //    if (reader["NOME"] != DBNull.Value)
                //        UserToImport.Nome = reader["NOME"].ToString();

                //    if (reader["VIA"] != DBNull.Value)
                //        UserToImport.via = reader["VIA"].ToString();

                //    if (reader["CAP"] != DBNull.Value)
                //        UserToImport.cap = reader["CAP"].ToString();

                //    if (reader["CITTA"] != DBNull.Value)
                //        UserToImport.citta = reader["CITTA"].ToString();

                //    if (reader["PROV"] != DBNull.Value)
                //        UserToImport.prov = reader["PROV"].ToString();

                //    if (reader["TEL"] != DBNull.Value)
                //        UserToImport.tel = reader["TEL"].ToString();

                //    if (reader["FAX"] != DBNull.Value)
                //        UserToImport.fax = reader["FAX"].ToString();

                //    if (reader["Email"] != DBNull.Value)
                //        UserToImport.email = reader["Email"].ToString();

                //    if (reader["CODICE FISCALE"] != DBNull.Value)
                //        UserToImport.codiceFiscale = reader["CODICE FISCALE"].ToString();

                //    if (reader["Mod pag Q Sertot"] != DBNull.Value)
                //        UserToImport.modPagQSertot = reader["Mod pag Q Sertot"].ToString();

                //    if (reader["data pagamento Sertot"] != DBNull.Value)
                //        UserToImport.dataPagamentoSertot = reader["data pagamento Sertot"].ToString();

                //    if (reader["ULT PAG Sertot"] != DBNull.Value)
                //        UserToImport.ultPagSertot = reader["ULT PAG Sertot"].ToString();                    

                //    UserToImportList.Add(UserToImport);
                //}

                //reader.Close();


                //Users = LINQToDataTable(UserToImportList);

                //ws.InsertDataTable(Users, 0, 0, true);
                //ef.SaveXlsx(Server.MapPath("~/ZeusInc/Account/Export/Report.xlsx"));
                //FileInfo file = new FileInfo(Server.MapPath("~/ZeusInc/Account/Export/Report.xlsx"));

                //if (file.Exists)
                //{
                //    Response.ClearContent();
                //    Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                //    Response.AddHeader("Content-Length", file.Length.ToString());
                //    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //    Response.TransmitFile(file.FullName);
                //    Response.End();
                //}
            }
            catch (Exception p)
            {
                //Response.Write(p.ToString());
            }
        }
    }

    public static DataTable LINQToDataTable<T>(IEnumerable<T> linqList)
    {
        DataTable dtReturn = new DataTable();
        PropertyInfo[] columnNameList = null;

        if (linqList == null) return dtReturn;

        foreach (T t in linqList)
        {
            if (columnNameList == null)
            {
                columnNameList = ((Type)t.GetType()).GetProperties();

                foreach (PropertyInfo columnName in columnNameList)
                {
                    Type columnType = columnName.PropertyType;

                    if ((columnType.IsGenericType) && (columnType.GetGenericTypeDefinition() == typeof(Nullable<>)))
                    {
                        columnType = columnType.GetGenericArguments()[0];
                    }

                    dtReturn.Columns.Add(new DataColumn(columnName.Name, columnType));
                }
            }

            DataRow dataRow = dtReturn.NewRow();

            foreach (PropertyInfo columnName in columnNameList)
            {
                dataRow[columnName.Name] =
                    columnName.GetValue(t, null) == null ? DBNull.Value : columnName.GetValue(t, null);
            }

            dtReturn.Rows.Add(dataRow);
        }

        return dtReturn;
    }

    private class ExportUsers
    {
        public ExportUsers()
        {
        Cognome = string.Empty;
        Nome = string.Empty;
        via = string.Empty;
        cap = string.Empty;
        citta = string.Empty;
        prov = string.Empty;
        tel = string.Empty;
        fax = string.Empty;
        email = string.Empty;
        cellulare = string.Empty;
        secondoRecapito = string.Empty;
        tipologia = string.Empty;
        codiceFiscale = string.Empty;
        QSertot = string.Empty;
        Q_ARRETR_Sertot = string.Empty;
        Q_ANTIC_Sertot = string.Empty;
        modPagQSertot = string.Empty;
        Mod_pag_Q_Sertot_ARRETRATA = string.Empty;
        Mod_pag_Q_Sertot_ANTICIPATA = string.Empty;
        dataPagamentoSertot = string.Empty;
        RIC_ARR_Sertot = string.Empty;
        RIC_ANT_Sertot = string.Empty;
        ultPagSertot = string.Empty;
        NOTE_Sertot = string.Empty;
        RIC_Sertot = string.Empty;
        data_Ric_Antic_Sertot = string.Empty;
        Spedita_ricevuta_Sertot = string.Empty;
    }

        public string Cognome = string.Empty;
        public string Nome = string.Empty;
        public string via = string.Empty;
        public string cap = string.Empty;
        public string citta = string.Empty;
        public string prov = string.Empty;
        public string tel = string.Empty;
        public string fax = string.Empty;
        public string email = string.Empty;
        public string cellulare = string.Empty;
        public string secondoRecapito = string.Empty;
        public string tipologia = string.Empty;
        public string codiceFiscale = string.Empty;
        public string QSertot = string.Empty;
        public string Q_ARRETR_Sertot = string.Empty;
        public string Q_ANTIC_Sertot = string.Empty;
        public string modPagQSertot = string.Empty;
        public string Mod_pag_Q_Sertot_ARRETRATA = string.Empty;
        public string Mod_pag_Q_Sertot_ANTICIPATA = string.Empty;
        public string dataPagamentoSertot = string.Empty;
        public string RIC_ARR_Sertot = string.Empty;
        public string RIC_ANT_Sertot = string.Empty;
        public string ultPagSertot = string.Empty;
        public string NOTE_Sertot = string.Empty;
        public string RIC_Sertot = string.Empty;
        public string data_Ric_Antic_Sertot = string.Empty;
        public string Spedita_ricevuta_Sertot = string.Empty;
    }

    //private class ExportUserTable
    //{
    //    public string Cognome { get; set; }
    //    public string Nome { get; set; }
    //    public string via { get; set; }
    //    public string cap { get; set; }
    //    public string citta { get; set; }
    //    public string prov { get; set; }
    //    public string tel { get; set; }
    //    public string fax { get; set; }
    //    public string email { get; set; }
    //    public string codiceFiscale { get; set; }
    //    public string modPagQSertot { get; set; }
    //    public string dataPagamentoSertot { get; set; }
    //    public string ultPagSertot { get; set; }

    //}

}





//using System;
//using System.Web;
//using System.Data.SqlClient;
//using System.Web.Security;
//using System.Web.UI.HtmlControls;
//using System.Configuration;
//using System.Collections;
//using System.Text.RegularExpressions;
//using GemBox.Spreadsheet;
//using System.Data;
//using System.Collections.Generic;
//using System.Linq;
//using System.Reflection;
//using System.IO;

//public partial class Zeus_Account_Excel_ExportSoci : System.Web.UI.Page
//{
//    static string TitoloPagina = "Esportazione soci su file Excel";

//    protected void Page_Load(object sender, EventArgs e)
//    {
//        TitleField.Value = TitoloPagina;

//    }

//    protected void EsportaButton_Click(object sender, EventArgs e)
//    {
//        DataTable Users = new DataTable();
//        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
//        string sqlQuery = string.Empty;
//        SpreadsheetInfo.SetLicense("E0YT-QK22-WFBB-4JRL");
//        ExcelFile ef = new ExcelFile();
//        ExcelWorksheet ws = ef.Worksheets.Add("DataSheet");
//        List<ExportUsers> UserToImportList = new List<ExportUsers>();
//        ExportUsers UserToImport;
//        List<Nazione> Nazioni = new List<Nazione>();
//        Nazione Naz = new Nazione();

//        using (SqlConnection connection = new SqlConnection(
//           strConnessione))
//        {
//            try
//            {
//                connection.Open();
//                SqlCommand command = connection.CreateCommand();

//                /*sqlQuery = @"SELECT Cognome
//                                ,Nome
//                                ,RagioneSociale
//                                ,Email
//                                ,ConsensoDatiPersonali
//                                ,ConsensoDatiPersonali2
//                                ,Indirizzo
//                                ,Citta
//                                ,Cap
//                                ,ProvinciaSigla
//                                ,ID2Nazione
//                                ,S1_Indirizzo
//                                ,S1_Citta
//                                ,S1_Comune
//                                ,S1_Cap
//                                ,S1_ProvinciaSigla
//                                ,S1_ID2Nazione
//                                ,UserName
//                                ,Meta_ZeusSearch
//                            FROM aspnet_Users
//                                LEFT OUTER JOIN tbProfiliPersonali
//                                    ON aspnet_Users.UserId = tbProfiliPersonali.UserId
//                                LEFT OUTER JOIN tbProfiliMarketing 
//                                    ON tbProfiliPersonali.UserId = tbProfiliMarketing.UserId
//                                LEFT OUTER JOIN aspnet_Membership
//                                    ON tbProfiliPersonali.UserId = aspnet_Membership.UserId
//                                LEFT OUTER JOIN tbProfiliSocietari 
//                                    ON tbProfiliPersonali.UserId = tbProfiliSocietari.UserId
//                            ORDER BY Cognome ASC, Nome ASC        
//                ";
//                */

//                sqlQuery = @"SELECT        TOP (100) PERCENT dbo.tbProfiliPersonali.Cognome, dbo.tbProfiliPersonali.Nome, dbo.tbProfiliSocietari.RagioneSociale, dbo.aspnet_Membership.Email, dbo.tbProfiliMarketing.ConsensoDatiPersonali, 
//                                                     dbo.tbProfiliMarketing.ConsensoDatiPersonali2, dbo.tbProfiliPersonali.Indirizzo, dbo.tbProfiliPersonali.Citta, dbo.tbProfiliPersonali.Cap, dbo.tbProfiliPersonali.ProvinciaSigla, dbo.tbProfiliPersonali.ID2Nazione, 
//                                                     dbo.tbProfiliSocietari.S1_Indirizzo, dbo.tbProfiliSocietari.S1_Citta, dbo.tbProfiliSocietari.S1_Comune, dbo.tbProfiliSocietari.S1_Cap, dbo.tbProfiliSocietari.S1_ProvinciaSigla, dbo.tbProfiliSocietari.S1_ID2Nazione, 
//                                                     dbo.aspnet_Users.UserName, dbo.tbProfiliMarketing.Meta_ZeusSearch, dbo.aspnet_Roles.RoleName
//                            FROM            dbo.aspnet_Users INNER JOIN
//                                                     dbo.aspnet_UsersInRoles ON dbo.aspnet_Users.UserId = dbo.aspnet_UsersInRoles.UserId INNER JOIN
//                                                     dbo.aspnet_Roles ON dbo.aspnet_UsersInRoles.RoleId = dbo.aspnet_Roles.RoleId LEFT OUTER JOIN
//                                                     dbo.tbProfiliPersonali ON dbo.aspnet_Users.UserId = dbo.tbProfiliPersonali.UserId LEFT OUTER JOIN
//                                                     dbo.tbProfiliMarketing ON dbo.tbProfiliPersonali.UserId = dbo.tbProfiliMarketing.UserId LEFT OUTER JOIN
//                                                     dbo.aspnet_Membership ON dbo.tbProfiliPersonali.UserId = dbo.aspnet_Membership.UserId LEFT OUTER JOIN
//                                                     dbo.tbProfiliSocietari ON dbo.tbProfiliPersonali.UserId = dbo.tbProfiliSocietari.UserId
//                            WHERE        (dbo.aspnet_Roles.RoleName = N'SOCI')
//                            ORDER BY dbo.aspnet_Users.UserName, dbo.tbProfiliPersonali.Cognome, dbo.tbProfiliPersonali.Nome";

//                command.CommandText = sqlQuery;
//                SqlDataReader reader = command.ExecuteReader();

//                while (reader.Read())
//                {
//                    UserToImport = new ExportUsers();

//                    if (reader["Cognome"] != DBNull.Value)
//                        UserToImport.Cognome = reader["Cognome"].ToString();

//                    if (reader["Nome"] != DBNull.Value)
//                        UserToImport.Nome = reader["Nome"].ToString();

//                    if (reader["RagioneSociale"] != DBNull.Value)
//                        UserToImport.RagioneSociale = reader["RagioneSociale"].ToString();

//                    if (reader["Email"] != DBNull.Value)
//                        UserToImport.Email = reader["Email"].ToString();

//                    if (reader["ConsensoDatiPersonali"] != DBNull.Value)
//                        UserToImport.ConsensoDatiPersonali = Convert.ToBoolean(reader["ConsensoDatiPersonali"]);

//                    if (reader["ConsensoDatiPersonali2"] != DBNull.Value)
//                        UserToImport.ConsensoDatiPersonali2 = Convert.ToBoolean(reader["ConsensoDatiPersonali2"]);

//                    if (reader["Indirizzo"] != DBNull.Value)
//                        UserToImport.Indirizzo = reader["Indirizzo"].ToString();

//                    if (reader["Citta"] != DBNull.Value)
//                        UserToImport.Citta = reader["Citta"].ToString();

//                    if (reader["Cap"] != DBNull.Value)
//                        UserToImport.Cap = reader["Cap"].ToString();

//                    if (reader["ProvinciaSigla"] != DBNull.Value)
//                        UserToImport.ProvinciaSigla = reader["ProvinciaSigla"].ToString();

//                    if (reader["ID2Nazione"] != DBNull.Value)
//                        UserToImport.ID2Nazione = Convert.ToInt32(reader["ID2Nazione"].ToString());

//                    if (reader["S1_Indirizzo"] != DBNull.Value)
//                        UserToImport.S1_Indirizzo = reader["S1_Indirizzo"].ToString();

//                    if (reader["S1_Citta"] != DBNull.Value)
//                        UserToImport.S1_Citta = reader["S1_Citta"].ToString();

//                    if (reader["S1_Comune"] != DBNull.Value)
//                        UserToImport.S1_Comune = reader["S1_Comune"].ToString();

//                    if (reader["S1_Cap"] != DBNull.Value)
//                        UserToImport.S1_Cap = reader["S1_Cap"].ToString();

//                    if (reader["S1_ProvinciaSigla"] != DBNull.Value)
//                        UserToImport.S1_ProvinciaSigla = reader["S1_ProvinciaSigla"].ToString();

//                    if (reader["S1_ID2Nazione"] != DBNull.Value)
//                        UserToImport.S1_ID2Nazione = Convert.ToInt32(reader["S1_ID2Nazione"].ToString());

//                    if (reader["UserName"] != DBNull.Value)
//                        UserToImport.UserName = reader["UserName"].ToString();

//                    if (reader["Meta_ZeusSearch"] != DBNull.Value)
//                        UserToImport.Meta_ZeusSearch = reader["Meta_ZeusSearch"].ToString();

//                    UserToImportList.Add(UserToImport);
//                }

//                reader.Close();

//                sqlQuery = @"SELECT Nazione
//                            ,ID1Nazione
//                            FROM tbNazioni
//                            ";

//                command.CommandText = sqlQuery;
//                reader = command.ExecuteReader();

//                while (reader.Read())
//                {
//                    Naz = new Nazione();

//                    if (reader["ID1Nazione"] != DBNull.Value)
//                        Naz.ID1Nazione = Convert.ToInt32(reader["ID1Nazione"].ToString());

//                    if (reader["Nazione"] != DBNull.Value)
//                        Naz.NazioneNome = reader["Nazione"].ToString();

//                    Nazioni.Add(Naz);
//                }

//                var utenti = (from u in UserToImportList.AsEnumerable()
//                             join n1 in Nazioni.AsEnumerable() on u.ID2Nazione equals n1.ID1Nazione
//                             join n2 in Nazioni.AsEnumerable() on u.ID2Nazione equals n2.ID1Nazione
//                             select new
//                             {
//                                 CodiceSocio = u.UserName,
//                                 Cognome = u.Cognome,
//                                 Nome = u.Nome,
//                                 RagioneSociale = u.RagioneSociale,
//                                 Email = u.Email,
//                                 //ConsensoDatiPersonali = u.ConsensoDatiPersonali == true ? "Si" : "No",
//                                // ConsensoDatiPersonali2 = u.ConsensoDatiPersonali2 == true ? "Si" : "No",
//                                // Indirizzo = u.Indirizzo,
//                               //  Citta = u.Citta,
//                               //  Cap = u.Cap,
//                               //  ProvinciaSigla = u.ProvinciaSigla,
//                                // Nazione = n1.NazioneNome == "> Non inserita" ? "" : n1.NazioneNome,
//                                 UfficioIndirizzo = u.S1_Indirizzo,
//                                 UfficioCitta = u.S1_Citta,
//                                 //UfficioComune = u.S1_Comune,
//                                 UfficioCap = u.S1_Cap,
//                                 UfficioProvincia = u.S1_ProvinciaSigla
//                                // UfficioNazione = n2.NazioneNome == "> Non inserita" ? "" : n2.NazioneNome,

//                                // TagUtente = u.Meta_ZeusSearch
//                             }).AsEnumerable();


//                Users = LINQToDataTable(utenti);

//                ws.InsertDataTable(Users, 0, 0, true);
//                ef.SaveXlsx(Server.MapPath("~/ZeusInc/Account/Export/Report.xlsx"));
//                FileInfo file = new FileInfo(Server.MapPath("~/ZeusInc/Account/Export/Report.xlsx"));

//                if (file.Exists)
//                {
//                    Response.ClearContent();
//                    Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
//                    Response.AddHeader("Content-Length", file.Length.ToString());
//                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
//                    Response.TransmitFile(file.FullName);
//                    Response.End();
//                }
//            }
//            catch (Exception p)
//            {
//                Response.Write(p.ToString());
//            }
//        }
//    }

//    public static DataTable LINQToDataTable<T>(IEnumerable<T> linqList)
//    {
//        var dtReturn = new DataTable();
//        PropertyInfo[] columnNameList = null;

//        if (linqList == null) return dtReturn;

//        foreach (T t in linqList)
//        {
//            if (columnNameList == null)
//            {
//                columnNameList = ((Type)t.GetType()).GetProperties();

//                foreach (PropertyInfo columnName in columnNameList)
//                {
//                    Type columnType = columnName.PropertyType;

//                    if ((columnType.IsGenericType) && (columnType.GetGenericTypeDefinition() == typeof(Nullable<>)))
//                    {
//                        columnType = columnType.GetGenericArguments()[0];
//                    }

//                    dtReturn.Columns.Add(new DataColumn(columnName.Name, columnType));
//                }
//            }

//            DataRow dataRow = dtReturn.NewRow();

//            foreach (PropertyInfo columnName in columnNameList)
//            {
//                dataRow[columnName.Name] =
//                    columnName.GetValue(t, null) == null ? DBNull.Value : columnName.GetValue(t, null);
//            }

//            dtReturn.Rows.Add(dataRow);
//        }

//        return dtReturn;
//    }

//    private class ExportUsers
//    {
//        public ExportUsers()
//        {
//            ConsensoDatiPersonali = false;
//            ConsensoDatiPersonali2 = false;
//            Cognome = string.Empty;
//            Nome = string.Empty;
//            RagioneSociale = string.Empty;
//            Email = string.Empty;
//            Indirizzo = string.Empty;
//            Citta = string.Empty;
//            Cap = string.Empty;
//            ProvinciaSigla = string.Empty;
//            S1_Indirizzo = string.Empty;
//            S1_Citta = string.Empty;
//            S1_Comune = string.Empty;
//            S1_Cap = string.Empty;
//            S1_ProvinciaSigla = string.Empty;
//            UserName = string.Empty;
//            Meta_ZeusSearch = string.Empty;
//        }

//        public string Cognome { get; set; }
//        public string Nome { get; set; }
//        public string RagioneSociale { get; set; }
//        public string Email { get; set; }
//        public bool ConsensoDatiPersonali { get; set; }
//        public bool ConsensoDatiPersonali2 { get; set; }
//        public string Indirizzo { get; set; }
//        public string Citta { get; set; }
//        public string Cap { get; set; }
//        public string ProvinciaSigla { get; set; }
//        public int ID2Nazione { get; set; }
//        public string S1_Indirizzo { get; set; }
//        public string S1_Citta { get; set; }
//        public string S1_Comune { get; set; }
//        public string S1_Cap { get; set; }
//        public string S1_ProvinciaSigla { get; set; }
//        public int S1_ID2Nazione { get; set; }
//        public string UserName { get; set; }
//        public string Meta_ZeusSearch { get; set; }        
//    }

//    private class ExportUserTable
//    {
//        public string Cognome { get; set; }
//        public string Nome { get; set; }
//        public string RagioneSociale { get; set; }
//        public string Email { get; set; }
//        public bool ConsensoDatiPersonali { get; set; }
//        public bool ConsensoDatiPersonali2 { get; set; }
//        public string Indirizzo { get; set; }
//        public string Citta { get; set; }
//        public string Cap { get; set; }
//        public string ProvinciaSigla { get; set; }
//        public string Nazione { get; set; }
//        public string UfficioIndirizzo { get; set; }
//        public string UfficioCitta { get; set; }
//        public string UfficioComune { get; set; }
//        public string UfficioCap { get; set; }
//        public string UfficioProvincia { get; set; }
//        public string UfficioNazione { get; set; }
//        public string UserName { get; set; }
//        public string TagUtente { get; set; }        

//    }

//    public class Nazione
//    {
//        public Nazione()
//        {
//        }

//        public int ID1Nazione { get; set; }
//        public int ID2Continenti { get; set; }
//        public string NazioneNome { get; set; }
//        public string Nazione_ENG { get; set; }
//        public string ISO_A2 { get; set; }
//        public string ISO_A3 { get; set; }
//        public string ISO_Number { get; set; }
//        public bool PW_Ecommerce { get; set; }
//    }
//}