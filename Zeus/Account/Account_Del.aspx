<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" %>

<%@ Import Namespace="System.Web.Security" %>
<%@ Import Namespace="System.Web.UI" %>
<%@ Import Namespace="System.Web.UI.WebControls" %>
<%@ Import Namespace="System.Web.UI.WebControls.WebParts" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">

    static string TitoloPagina = "Conferma eliminazione account utente";
    delinea myDelinea = new delinea();

    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;
        
        if (!myDelinea.AntiSQLInjection(Request.QueryString))
            Response.Redirect("/Zeus/System/Message.aspx");

        if (!Page.IsPostBack)
            if (!isOK())
                Response.Redirect("~/Zeus/System/Message.aspx?Msg=7867868458426388&VfC=HKELP&Lang=ITA");


        UserId.Value = Server.HtmlEncode(Request.QueryString["UID"]);

    }//fine Page_Load

    private bool isOK()
    {
        try
        {
            if (Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/Account/Account_Dtl.aspx") > 0
                || Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/Soci/Soci_Dsp.aspx") > 0)
                return true;
            else return false;
        }
        catch (Exception p)
        {
            return false;
        }

    }//fine isOK

    protected void DeleteUserIDHidden_DataBinding(object sender, EventArgs e)
    {
        try
        {
            HiddenField DeleteUserIDHidden = (HiddenField)UserDetailFormView.FindControl("DeleteUserIDHidden");
            Label UserNameLabel = (Label)UserDetailFormView.FindControl("UserNameLabel");
            Label ConfermaElimLabel = (Label)UserDetailFormView.FindControl("ConfermaElimLabel");
            MembershipUser user = Membership.GetUser();
            LinkButton btDel = (LinkButton)myDelinea.FindControlRecursive(this, "btDel");
            CheckBox chkConfirm = (CheckBox)myDelinea.FindControlRecursive(this, "chkConfirm");
            object userKey = user.ProviderUserKey;
            string deleteUser = UserNameLabel.Text;

            DeleteUserIDHidden.Value = userKey.ToString();

            if (Roles.IsUserInRole(deleteUser, "ZeusAdmin"))
            {
                Msg.Text = "Attenzione: non � possibile eliminare un account utente con il ruolo ZeusAdmin";
                btDel.Visible = false;
                chkConfirm.Visible = false;
                ConfermaElimLabel.Visible = false;
            }
        }
        catch (Exception p)
        {
            //Response.Write(p.ToString());
        }
    }


    private bool DuplicateRecord(string UserId)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        try
        {

            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();


                sqlQuery = "SET DATEFORMAT dmy; INSERT INTO tbUsers_Deleted(UserID, UserName, Email, RecordNewDate, RecordDelDate, RecordDelUser, LastLoginDate)";
                sqlQuery += "SELECT  UserID,UserName,Email, RecordNewDate,GETDATE(),@RecordDelUser, LastLoginDate FROM vwAccount_Lst_All WHERE (UserID = @UserId)";

                command.CommandText = sqlQuery;

                command.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@UserId"].Value = new Guid(UserId);
                command.Parameters.Add("@RecordDelUser", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@RecordDelUser"].Value = Membership.GetUser().ProviderUserKey;

                command.ExecuteNonQuery();

                return true;

            }//fine Using
        }
        catch (Exception p)
        {

            Response.Write(p.ToString());
            return false;
        }

    }//fine DuplicateRecord


    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton btDel = (LinkButton)myDelinea.FindControlRecursive(this, "btDel");
            CheckBox chkConfirm = (CheckBox)myDelinea.FindControlRecursive(this, "chkConfirm");

            if (chkConfirm.Checked == true)
            {
                if (DuplicateRecord(UserId.Value))
                {
                    Label UserNameLabel = (Label)UserDetailFormView.FindControl("UserNameLabel");
                    string deleteUser = UserNameLabel.Text;
                    Membership.DeleteUser(deleteUser, true);

                    Response.Redirect("~/Zeus/System/Message.aspx?Msg=78675957136223598&VfC=HKELP&Lang=ITA");
                }
            }
            else
            {
                Msg.Text = "Confermare l'eliminazione";
            }
        }
        catch (Exception)
        {

        }
    }//fine Button1_Click

   


</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" />
    <asp:HiddenField ID="UserId" runat="server" />
    <asp:Label ID="Msg" runat="server" SkinID="Validazione01" Text="Vuoi eliminare definitivamente l'utente?"></asp:Label><br />
    <asp:FormView ID="UserDetailFormView" runat="server" DataKeyNames="UserId" DataSourceID="ds_UserDetail"
        Width="100%">
        <ItemTemplate>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="AccountUtenteLabel" runat="server" Text="Account utente"></asp:Label></div>
                <table border="0" cellpadding="2" cellspacing="1">
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label1" SkinID="FieldDescription" runat="server">UserName:</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:Label ID="UserNameLabel" runat="server" SkinID="FieldValue" Text='<%# Bind("UserName") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label2" SkinID="FieldDescription" runat="server">Proprietario account:</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:Label ID="CommentLabel" runat="server" SkinID="FieldValue" Text='<%# Bind("CognomeNome") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label3" SkinID="FieldDescription" runat="server">Indirizzo E-Mail:</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:Label ID="EmailLabel" runat="server" SkinID="FieldValue" Text='<%# Bind("Email") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label4" SkinID="FieldDescription" runat="server">Data di creazione dell'account:</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:Label ID="CreateDateLabel" runat="server" SkinID="FieldValue" Text='<%# Bind("RecordNewDate") %>'></asp:Label>
                        </td>
                    </tr>
                </table>
                <div style="text-align: center">
                    <asp:CheckBox ID="chkConfirm" runat="server" /><asp:Label ID="ConfermaElimLabel"
                        SkinID="CorpoTesto" runat="server" Text="Conferma eliminazione"></asp:Label>
                    <div class="VertSpacerMedium">
                    </div>
                    <asp:LinkButton ID="btDel" SkinID="ZSSM_Button01" runat="server" OnClick="Button1_Click"
                        Text="Elimina" /><br />
                </div>
                <div class="VertSpacerMedium">
                </div>
            </div>
            <asp:HiddenField ID="HiddenUserID" runat="server" Value='<%# Eval("UserId", "{0}") %>' />
            <asp:HiddenField ID="DeleteUserIDHidden" runat="server" OnDataBinding="DeleteUserIDHidden_DataBinding" />
            <asp:HiddenField ID="LastLoginDateHiddenField" runat="server" Value='<%# Eval("LastLoginDate", "{0:g}") %>' />
        </ItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="ds_UserDetail" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SELECT  UserID,Email, CognomeNome, UserName, RecordNewDate, LastLoginDate FROM vwAccount_Lst_All WHERE (UserID = @UserId)">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="0" Name="UserId" QueryStringField="UID" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
