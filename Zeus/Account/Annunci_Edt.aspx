﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" %>

<script runat="server">   
   
    static string TitoloPagina = "Modifica limite massimo di annunci";

    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();
        TitleField.Value = TitoloPagina;

        if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "UID"))
            || (Request.QueryString["UID"].Length == 0))
            Response.Redirect("/System/Message.aspx");

        if (!Page.IsPostBack)
            if (!isOK())
                Response.Redirect("~/Zeus/System/Message.aspx?Msg=7867868458426388&VfC=HKELP&Lang=ITA");

        UID.Value = Server.HtmlEncode(Request.QueryString["UID"]);

    }//fine Page_Load

    private bool isOK()
    {
        try
        {
            if (Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/Account/Account_Dtl.aspx") > 0)
                return true;
            else return false;
        }
        catch (Exception)
        {
            return false;
        }

    }//fine isOK


    protected void AnnunciSqlDataSource_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {

        if ((e.AffectedRows > 0)
            && (e.Exception == null))
            Response.Redirect("Account_Dtl.aspx?UID=" + UID.Value);
        else Response.Redirect("~/Zeus/System/Message.aspx?UpdErr");
    }//fine AnnunciSqlDataSource_Updated
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <asp:HiddenField ID="UID" runat="server" />
    <asp:FormView ID="AnnunciFormView" runat="server" DataSourceID="AnnunciSqlDataSource"
        DefaultMode="Edit" Width="100%" CellPadding="0">
        <EditItemTemplate>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="UserMasterLabel" runat="server" Text='<%# Eval("Sezione","Dettaglio annuncio{0}") %>' ></asp:Label></div>
                <table border="0" cellpadding="2" cellspacing="1">
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label SkinID="FieldDescription" ID="Cognome" runat="server" Text="Utente: "></asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:Label SkinID="FieldValue" ID="UserNameLabel" runat="server" Text='<%# Eval("Cognome") %>'></asp:Label>
                            <asp:Label SkinID="FieldValue" ID="Label2" runat="server" Text='<%# Eval("Nome") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label SkinID="FieldDescription" ID="Label1" runat="server" Text="Limite massimo di annunci:"></asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:TextBox ID="ZeusJolly1TextBox" runat="server" Text='<%# Bind("MaxListing") %>'
                                Columns="5" MaxLength="5"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Obbligatorio"
                                ControlToValidate="ZeusJolly1TextBox" SkinID="ZSSM_Validazione01" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table>
            </div>
            <div align="center">
                <asp:LinkButton SkinID="ZSSM_Button01" ID="InsertLinkButton" runat="server" CommandName="Update"
                    Text="Salva dati"></asp:LinkButton></div>
        </EditItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="AnnunciSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SELECT     dbo.tbModuli.Sezione, dbo.tbListings_MaxNumber.MaxListing, dbo.tbListings_MaxNumber.Inserzionista, dbo.tbListings_MaxNumber.ZeusIdModulo, 
                      dbo.vwAccount_Lst_All.Cognome, dbo.vwAccount_Lst_All.Nome
FROM         dbo.tbListings_MaxNumber INNER JOIN
                      dbo.tbModuli ON dbo.tbListings_MaxNumber.ZeusIdModulo = dbo.tbModuli.ZeusIdModulo INNER JOIN
                      dbo.vwAccount_Lst_All ON dbo.tbListings_MaxNumber.Inserzionista = dbo.vwAccount_Lst_All.UserId WHERE dbo.tbListings_MaxNumber.Inserzionista=@UserID
                       AND dbo.tbListings_MaxNumber.ZeusIdModulo=@ZeusIdModulo "
        
        
        UpdateCommand="UPDATE [tbListings_MaxNumber] SET MaxListing=@MaxListing WHERE Inserzionista=@UserID AND ZeusIdModulo=@ZeusIdModulo"
        OnUpdated="AnnunciSqlDataSource_Updated">
        <SelectParameters>
            <asp:QueryStringParameter Name="UserID" QueryStringField="UID" DefaultValue="0" />
            <asp:QueryStringParameter Name="ZeusIdModulo" QueryStringField="ZIM" DefaultValue="0" />
        </SelectParameters>
        <UpdateParameters>
            <asp:QueryStringParameter Name="UserID" QueryStringField="UID" DefaultValue="0" />
            <asp:QueryStringParameter Name="ZeusIdModulo" QueryStringField="ZIM" DefaultValue="0" />
            <asp:Parameter Name="MaxListing" DefaultValue="0" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
