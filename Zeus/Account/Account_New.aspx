<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" Async="true"
    Theme="Zeus" %>

<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Net.Mail" %>
<script runat="server">

    static string TitoloPagina = "Nuovo account utente";
    delinea myDelinea = new delinea();

    private bool SendMail(string Email, string myUserName, string myPassword)
    {
        try
        {
            Zeus.Mail myMail = new Zeus.Mail();

            myMail.MailToSend.To.Add(new MailAddress(Email));
            myMail.MailToSend.From = new MailAddress("support@switchup.it", "Sito Web MV Congressi Platform");

            myMail.MailToSend.Subject = "MV Congressi Platform WebSite - Creazione Nuovo Utente";


            myMail.MailToSend.IsBodyHtml = true;
            string myBody = string.Empty;



            myBody = Server.HtmlEncode("E� attivo l�account personale per accedere al sito http://sertot.switchup.it ");
            myBody += "<br />";
            myBody += "UserName:" + myUserName + "<br />";
            myBody += "Password: " + myPassword + "<br />";
            myBody += "Indirizzo E-Mail associato: " + Email + "<br />";
            myBody += "<br />";
            myBody += "Creazione Account Utente in data ";
            myBody += DateTime.Now.ToString("d");
            myBody += " alle ore ";
            myBody += DateTime.Now.Hour + "." + DateTime.Now.Minute + "." + DateTime.Now.Second;
            myBody += "<br />";
            myBody += "<br />";



            myMail.MailToSend.Body = myBody;

            myMail.SendMail(Zeus.Mail.MailSendMode.Async);

            return true;
        }
        catch (Exception p)
        {

            Response.Write(p.ToString());
            return false;
        }

    }//fine SendMail


    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;

        UserMasterHiddenField.Value = Membership.GetUser().ProviderUserKey.ToString();


        if (!Page.IsPostBack)
        {
            CreateUserWizard1.InvalidPasswordErrorMessage = "Lunghezza minima password: " + Membership.MinRequiredPasswordLength + " caratteri alfanumerici";

            if (Membership.MinRequiredNonAlphanumericCharacters > 0)
                CreateUserWizard1.InvalidPasswordErrorMessage += " Caratteri speciali richiesti: almeno " + Membership.MinRequiredNonAlphanumericCharacters;

            ReadXML("PROFILI", GetLang());
            ReadXML_Localization("PROFILI", GetLang());
        }



    }//fine Page_Load



    private string GetLang()
    {
        string Lang = "ITA";

        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;

    }//fine GetLang



    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Profili.xml"));


            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {

                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                            if (myLabel != null)
                                myLabel.Text = childNode.InnerText;

                        }
                        catch (Exception)
                        {

                        }
                    }//fine foreach
                }//fine foreach

            }//fine for
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine ReadXML_Localization



    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_Profili.xml"));


            ZMCF_BusinessBook.Visible =
             Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BusinessBook").InnerText);

            Panel ZMCF_Categoria1 = (Panel)CreateUserWizardStep3.FindControl("ZMCF_Categoria1");

            if (ZMCF_Categoria1 != null)
                ZMCF_Categoria1.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Categoria1").InnerText);

            if (ZMCF_ConsensoDatiPersonali != null)
                ZMCF_ConsensoDatiPersonali.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_ConsensoDatiPersonali").InnerText);

            if (ZMCF_ConsensoDatiPersonali2 != null)
                ZMCF_ConsensoDatiPersonali2.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_ConsensoDatiPersonali2").InnerText);

            if (ZMCF_Comunicazioni1 != null)
                ZMCF_Comunicazioni1.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni1").InnerText);

            if (ZMCF_Comunicazioni2 != null)
                ZMCF_Comunicazioni2.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni2").InnerText);

            if (ZMCF_Comunicazioni3 != null)
                ZMCF_Comunicazioni3.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni3").InnerText);

            if (ZMCF_Comunicazioni4 != null)
                ZMCF_Comunicazioni4.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni4").InnerText);

            if (ZMCF_Comunicazioni5 != null)
                ZMCF_Comunicazioni5.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni5").InnerText);

            if (ZMCF_Comunicazioni6 != null)
                ZMCF_Comunicazioni6.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni6").InnerText);


            if (ZMCF_Comunicazioni7 != null)
                ZMCF_Comunicazioni7.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni7").InnerText);


            if (ZMCF_Comunicazioni8 != null)
                ZMCF_Comunicazioni8.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni8").InnerText);


            if (ZMCF_Comunicazioni9 != null)
                ZMCF_Comunicazioni9.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni9").InnerText);


            if (ZMCF_Comunicazioni10 != null)
                ZMCF_Comunicazioni10.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni10").InnerText);


            SetQueryOfID2CategoriaDropDownList(mydoc.SelectSingleNode(myXPath + "ZMCD_ZIMCategoria1").InnerText
                          , GetLang(), mydoc.SelectSingleNode(myXPath + "ZMCD_Cat1Liv").InnerText);

            return true;
        }
        catch (Exception)
        {
            return false;
        }

    }//fine ReadXML





    //##############################################################################################################
    //################################################ CATEGORIE ###################################################
    //############################################################################################################## 

    private bool SetQueryOfID2CategoriaDropDownList(string ZeusIdModulo,
        string ZeusLangCode, string PZV_Cat1Liv)
    {
        try
        {


            SqlDataSource dsCategoria = (SqlDataSource)CreateUserWizardStep3.FindControl("dsCategoria");
            DropDownList ID2CategoriaDropDownList = (DropDownList)CreateUserWizardStep3.FindControl("ID2CategoriaDropDownList");
            HiddenField ID2CategoriaHiddenField = (HiddenField)CreateUserWizardStep3.FindControl("ID2CategoriaHiddenField");


            if (dsCategoria == null)
                return false;

            if (ID2CategoriaDropDownList == null)
                return false;

            if (ID2CategoriaHiddenField == null)
                return false;

            ID2CategoriaHiddenField.Value = "0";

            switch (PZV_Cat1Liv)
            {

                case "123":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv1Liv2Liv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "Catliv1Liv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;


                case "23":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv2Liv3]";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;

                default:
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;
            }

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }//fine SetQueryOfID2CategoriaDropDownList



    protected void ID2CategoriaDropDownList_SelectIndexChange(object sender, EventArgs e)
    {
        HiddenField ID2CategoriaHiddenField = (HiddenField)CreateUserWizardStep3.FindControl("ID2CategoriaHiddenField");
        DropDownList ID2CategoriaDropDownList = (DropDownList)sender;


        if (ID2CategoriaHiddenField != null)
            ID2CategoriaHiddenField.Value = ID2CategoriaDropDownList.SelectedValue;

    }//fine ID2CategoriaDropDownList_SelectIndexChange



    //##############################################################################################################
    //############################################ FINE CATEGORIE ##################################################
    //############################################################################################################## 


    protected void StepCreazione(object sender, EventArgs e)
    {
        try
        {
            TextBox UserNameTextBox = (TextBox)CreateUserWizardStep5.ContentTemplateContainer.FindControl("UserName");
            TextBox PasswordTextBox = (TextBox)CreateUserWizardStep5.ContentTemplateContainer.FindControl("Password");
            TextBox EmailTextBox = (TextBox)CreateUserWizardStep5.ContentTemplateContainer.FindControl("Email");
            CheckBox CheckBoxInvioMail = (CheckBox)CreateUserWizardStep5.ContentTemplateContainer.FindControl("CheckBoxInvioMail");


            MembershipUser User = Membership.GetUser(UserNameTextBox.Text);
            object UserGUID = User.ProviderUserKey;

            object CreatorGUID = Membership.GetUser().ProviderUserKey;


            //parte Personale
            SqlDataSource DataSource = (SqlDataSource)CreateUserWizardStep1.FindControl("dsStep1Personale");
            DataSource.InsertParameters.Add("UserId", UserGUID.ToString());
            DataSource.InsertParameters.Add("RecordNewUser", CreatorGUID.ToString());
            DataSource.InsertParameters.Add("RecordNewDate", System.TypeCode.DateTime, DateTime.Now.ToString());

            DataSource.Insert();

            //parte Societario
            SqlDataSource DataSource2 = (SqlDataSource)CreateUserWizardStep2.FindControl("dsStep2Societario");
            DataSource2.InsertParameters.Add("UserId", UserGUID.ToString());
            DataSource2.InsertParameters.Add("RecordNewUser", CreatorGUID.ToString());
            DataSource2.InsertParameters.Add("RecordNewDate", System.TypeCode.DateTime, DateTime.Now.ToString());

            DataSource2.Insert();

            //parte Marketing
            SqlDataSource DataSource3 = (SqlDataSource)CreateUserWizardStep3.FindControl("dsStep3Marketing");
            DataSource3.InsertParameters.Add("UserId", UserGUID.ToString());
            DataSource3.InsertParameters.Add("RecordNewUser", CreatorGUID.ToString());
            DataSource3.InsertParameters.Add("RecordNewDate", System.TypeCode.DateTime, DateTime.Now.ToString());
            DataSource3.InsertParameters.Add("RegCausal", "ZES");

            DataSource3.Insert();

            //parte per l'associazione dei ruoli all'utente
            CheckBoxList list = (CheckBoxList)myDelinea.FindControlRecursive(Page, "RolesCheckBoxList");
            // TextBox UserName=(TextBox)myDelinea.FindControlRecursive(Page,"");
            // Response.Write("DEB "+list.Items.Count+"<br />");
            for (int i = 0; i < list.Items.Count; ++i)
            {
                if (list.Items[i].Selected)
                    Roles.AddUserToRole(UserNameTextBox.Text.ToString(), list.Items[i].Text.ToString());
            }

            //SALVO IL RUOLO SOC IN tbProfiliMarketing            
            try
            {
                String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

                string sqlQuery = string.Empty;

                using (SqlConnection connection = new SqlConnection(
                   strConnessione))
                {

                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    
                    var userId = Membership.GetUser(UserNameTextBox.Text).ProviderUserKey;
                    string statusSocio = RolesCheckBoxList.Items.Cast<ListItem>().FirstOrDefault(x => x.Value.ToLower().Equals("soci") || x.Value.ToLower().Equals("socionorari")) != null ? "SOC" : null;

                    sqlQuery = @"UPDATE tbProfiliMarketing
                                SET StatusSocio = @Status
                                WHERE UserId = @UserId";
                    command.Parameters.Clear();
                    command.CommandText = sqlQuery;
                    command.Parameters.Add("@Status", System.Data.SqlDbType.VarChar);
                    command.Parameters["@Status"].Value = statusSocio;
                    command.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
                    command.Parameters["@UserId"].Value = userId;

                    command.ExecuteNonQuery();



                    SertotUtility c = new SertotUtility();
                    string nuovoCodiceSocio = c.NuovoCodiceSocio();

                    sqlQuery = @"UPDATE tbProfiliMarketing
                                SET [CodiceSocioNum] = @CodiceSocioNum, [CodiceSocioStr] = @CodiceSocioStr
                                WHERE UserId = @UserId AND CodiceSocioStr IS NULL AND CodiceSocioNum IS NULL";
                    command.Parameters.Clear();
                    command.CommandText = sqlQuery;
                    command.Parameters.Add("@CodiceSocioNum", System.Data.SqlDbType.Int);
                    command.Parameters["@CodiceSocioNum"].Value = Convert.ToInt32(nuovoCodiceSocio);
                    command.Parameters.Add("@CodiceSocioStr", System.Data.SqlDbType.NVarChar);
                    command.Parameters["@CodiceSocioStr"].Value = nuovoCodiceSocio;
                    command.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
                    command.Parameters["@UserId"].Value = userId;

                    command.ExecuteNonQuery();
                }
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
            }
            


            bool InvioMail = false;
            if (CheckBoxInvioMail.Checked == true)
                InvioMail = SendMail(EmailTextBox.Text, UserNameTextBox.Text, PasswordTextBox.Text);


            //Response.Write(InvioMail.ToString());
            Response.Redirect("~/Zeus/Account/Account_Lst.aspx?UID=" + UserGUID.ToString() + "&XRI2=1");

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }




    protected void ProfiloSocietario_OnActivate(object sender, EventArgs e)
    {
        Button StepPrevoiusButton = (Button)myDelinea.FindControlRecursive(Page, "StepPreviousButton");
        StepPrevoiusButton.Text = "< Profilo personale";
    }

    protected void ProfiloMarketing_OnActivate(object sender, EventArgs e)
    {
        Button StepPrevoiusButton2 = (Button)myDelinea.FindControlRecursive(Page, "StepPreviousButton");
        StepPrevoiusButton2.Text = "< Profilo azienda / ente";

    }

    protected void Ruoli_OnActivate(object sender, EventArgs e)
    {
        Button StepPrevoiusButton3 = (Button)myDelinea.FindControlRecursive(Page, "StepPreviousButton");
        StepPrevoiusButton3.Text = "< Preferenze";

    }

    protected void AccountUtente_OnActivate(object sender, EventArgs e)
    {
        try
        {

            Button StepPrevoiusButton4 = (Button)myDelinea.FindControlRecursive(Page, "StepPreviousButton");
            StepPrevoiusButton4.Text = "< Ruoli utente";
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    protected void DropDownListTipologia_DataBound(object sender, EventArgs e)
    {
        DropDownList drop = (DropDownList)sender;

        for (int i = 0; i < drop.Items.Count; ++i)
        {
            if (drop.Items[i].Text.Equals("Non inserito"))
            {
                drop.SelectedIndex = i;
                break;
            }
        }
    }

    protected void DropDownListSettore_DataBound(object sender, EventArgs e)
    {
        DropDownList drop = (DropDownList)sender;

        for (int i = 0; i < drop.Items.Count; ++i)
        {
            if (drop.Items[i].Text.Equals("Non inserito"))
            {
                drop.SelectedIndex = i;
                break;
            }
        }
    }


    private bool AddOtherRoles(CheckBoxList myList)
    {
        try
        {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(
               strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT RoleName FROM tbZeusRoles WHERE IsAdminManaged = 0";
                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {

                    myList.Items.Clear();

                    while (reader.Read())
                        myList.Items.Add(new ListItem(reader.GetString(0), reader.GetString(0)));
                }


                reader.Close();

            }//fine using

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }

    }//fine AddOtherRoles

    private bool CheckIsAdminUser(string UserName)
    {
        bool IsAdminUser = false;

        try
        {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            string sqlQuery = string.Empty;

            using (SqlConnection connection = new SqlConnection(
               strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT RoleName FROM tbZeusRoles WHERE IsAdminRole = 1";
                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                    if ((reader["RoleName"] != DBNull.Value)
                      && (Roles.IsUserInRole(UserName, reader["RoleName"].ToString())))
                    {
                        IsAdminUser = true;
                        break;
                    }


                reader.Close();

            }//fine using


        }
        catch (Exception p)
        {
            Response.Write(p.ToString());

        }

        return IsAdminUser;


    }//fine CheckIsAdminUser



    protected void RolesCheckboxList_OnLoad(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CheckBoxList check = (CheckBoxList)sender;
            if (CheckIsAdminUser(Membership.GetUser().UserName))
            {
                check.DataSource = Roles.GetAllRoles();
                check.DataBind();
            }
            else AddOtherRoles(check);
        }
    }//fine RolesCheckboxList_OnLoad





</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <asp:Label ID="NavigationLabel1" runat="server"></asp:Label>
    <asp:Label ID="NavigationLabel2" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="NavigationLabel3" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="NavigationLabel4" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="NavigationLabel5" runat="server" Visible="false"></asp:Label>
    <asp:CreateUserWizard ID="CreateUserWizard1" runat="server" OnCreatedUser="StepCreazione"
        Width="100%" FinishDestinationPageUrl="~/Zeus/Home/Default.aspx" LoginCreatedUser="False"
        ErrorMessageStyle-CssClass="ZSSM_Validazione01" CreateUserButtonText="Crea Utente"
        StepPreviousButtonText="< Ruoli utente" DuplicateUserNameErrorMessage="UserName gi� utilizzato">
        <ErrorMessageStyle CssClass="ZSSM_Validazione01" ForeColor="#ff0000" />
        <CreateUserButtonStyle CssClass="ZSSM_Button01_Button" />
        <StepPreviousButtonStyle CssClass="ZSSM_Button01_Button" />
        <StartNavigationTemplate>
            <div align="center">
                <asp:Button ID="StartNextButton" runat="server" CommandName="MoveNext" Text="Procedi al passo successivo >"
                    SkinID="ZSSM_Button01" />
            </div>
        </StartNavigationTemplate>
        <StepNavigationTemplate>
            <div align="center">
                <asp:Button ID="StepPreviousButton" runat="server" CommandName="MovePrevious" Text="Precedente"
                    SkinID="ZSSM_Button01" />
                <asp:Button ID="StepNextButton" runat="server" CommandName="MoveNext" Text="Procedi al passo successivo >"
                    SkinID="ZSSM_Button01" />
            </div>
        </StepNavigationTemplate>
        <WizardSteps>
            <asp:WizardStep runat="server" Title="Step 1 - Profilo personale" ID="CreateUserWizardStep1">
                <div class="LayWizardStep">
                    <asp:Label SkinID="WizardStepCurrent" ID="Label1" runat="server" Text="Profilo personale "></asp:Label>
                    <asp:Label SkinID="WizardStep" ID="Label2" runat="server" Text="&gt; Profilo azienda / ente "></asp:Label>
                    <asp:Label SkinID="WizardStep" ID="Label3" runat="server" Text="&gt; Preferenze "></asp:Label>
                    <asp:Label SkinID="WizardStep" ID="Label4" runat="server" Text="&gt; Ruoli utente "></asp:Label>
                    <asp:Label SkinID="WizardStep" ID="Label5" runat="server" Text="&gt; Dati di accesso account utente"></asp:Label>
                </div>
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="Utente_Label" runat="server" Text="Utente"></asp:Label>
                    </div>
                    <table>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="Cognome_Label" runat="server" Text="Cognome *"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="TextBoxCognome" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
                                <asp:RequiredFieldValidator SkinID="ZSSM_Validazione01" Display="Dynamic" ID="RequiredFieldValidator1"
                                    runat="server" ControlToValidate="TextBoxCognome" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="Nome_Label" runat="server" Text="Nome *"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="TextBoxNome" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
                                <asp:RequiredFieldValidator SkinID="ZSSM_Validazione01" Display="Dynamic" ID="RequiredFieldValidator2"
                                    runat="server" ControlToValidate="TextBoxNome" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="Qualifica_Label" runat="server" Text="Qualifica"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="TextBoxQualifica" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="Professione_Label" runat="server" Text="Professione"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="ProfessioneTextBox" runat="server" Columns="20" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="SessoLabel" runat="server" Text="Sesso"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:DropDownList ID="SessoDropDownlist" runat="server">
                                    <asp:ListItem Selected="True" Text="Non inserito" Value="X"></asp:ListItem>
                                    <asp:ListItem Text="Maschio" Value="M"></asp:ListItem>
                                    <asp:ListItem Text="Femmina" Value="F"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="CodiceFiscale_Label" runat="server" Text="Codice fiscale personale"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" Columns="16" MaxLength="16"></asp:TextBox>
                                <%--<asp:RegularExpressionValidator SkinID="ZSSM_Validazione01" Display="Dynamic" ID="CodiceFiscaleRegularExpressionValidator"
                                    runat="server" ControlToValidate="TextBoxCodiceFiscale" ErrorMessage="Codice fiscale non corretto"
                                    ValidationExpression="^[A-Za-z]{6}\d{2}[A-Za-z]\d{2}[A-Za-z]\d{3}[A-Za-z]$"></asp:RegularExpressionValidator>--%>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="Residenza_Label" runat="server" Text="Residenza"></asp:Label>
                    </div>
                    <table>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="Indirizzo_Label" runat="server" Text="Indirizzo"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="TextBoxIndirizzo" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
                                <%--<asp:Label SkinID="FieldDescription" ID="Numero_Label" runat="server" Text=" Numero"></asp:Label>
                                <asp:TextBox ID="TextBoxNumero" runat="server" Columns="10" MaxLength="10"></asp:TextBox>--%>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="Citt�_Label" runat="server" Text="Citt�"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="TextBoxCitta" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="Comune_Label" runat="server" Text="Comune"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="TextBoxComune" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
                                <asp:Label SkinID="FieldDescription" ID="CAP_Label" runat="server" Text="CAP"></asp:Label>
                                <asp:TextBox ID="TextBoxCAP" runat="server" Columns="5" MaxLength="5"></asp:TextBox>
                                <asp:RangeValidator Display="Dynamic" SkinID="ZSSM_Validazione01" ID="RangeValidator1"
                                    runat="server" ControlToValidate="TextBoxCAP" ErrorMessage="CAP non corretto"
                                    MaximumValue="99999" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="Provincia_Label" runat="server" Text="Provincia"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="TextBoxProvincia" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
                                <asp:Label SkinID="FieldDescription" ID="Sigla_Label" runat="server" Text="Sigla provincia"></asp:Label>
                                <asp:TextBox ID="TextBoxSiglaProvincia" runat="server" Columns="2" MaxLength="2"></asp:TextBox>
                                <asp:RegularExpressionValidator Display="Dynamic" SkinID="ZSSM_Validazione01" ID="RegularExpressionValidator3"
                                    runat="server" ControlToValidate="TextBoxSiglaProvincia" ErrorMessage="Sigla provincia non corretta"
                                    ValidationExpression="^[A-Za-z]{2}$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="Nazione_Label" runat="server" Text="Nazione"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:DropDownList ID="DropDownListNazione" runat="server" DataSourceID="dsNazioni2"
                                    DataTextField="Nazione" DataValueField="ID1Nazione">
                                    <asp:ListItem Text="&gt;  Non inserita" Selected="True" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="dsNazioni2" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                    SelectCommand="SELECT [ID1Nazione], [Nazione] FROM [tbNazioni] ORDER BY [Nazione]"></asp:SqlDataSource>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="Nascita_Label" runat="server" Text="Nascita"></asp:Label>
                    </div>
                    <table>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="Data_Label" runat="server" Text="Data"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="TextBoxData" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                <asp:RegularExpressionValidator Display="Dynamic" SkinID="ZSSM_Validazione01" ID="RegularExpressionValidator2"
                                    runat="server" ControlToValidate="TextBoxData" ErrorMessage="Fomato data richiesto: GG/MM/AAAA"
                                    ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="Citta2_Label" runat="server" Text="Citt�"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="TextBoxCittaNascita" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
                                <asp:Label SkinID="FieldDescription" ID="Provincia2_Label" runat="server" Text="Sigla provincia"></asp:Label>
                                <asp:TextBox ID="TextBoxProvinciaNascita" runat="server" Columns="2" MaxLength="2"></asp:TextBox>
                                <asp:RegularExpressionValidator Display="Dynamic" SkinID="ZSSM_Validazione01" ID="RegularExpressionValidator5"
                                    runat="server" ControlToValidate="TextBoxProvinciaNascita" ErrorMessage="Sigla provincia non corretta"
                                    ValidationExpression="^[A-Za-z]{2}$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="Nazione2_Label" runat="server" Text="Nazione"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:DropDownList ID="DropDownListNazioneNascita" runat="server" DataSourceID="dsNazioni2"
                                    DataTextField="Nazione" DataValueField="ID1Nazione">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="Contatti2_Label" runat="server" Text="Contatti"></asp:Label>
                    </div>
                    <table>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="Telefono2_Label" runat="server" Text="Telefono casa"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="TextBoxTelefonoCasa" runat="server" Columns="20" MaxLength="20"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="Cell_Label" runat="server" Text="Telefono cellulare"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="TextBoxTelefonoCellulare" runat="server" Columns="20" MaxLength="20"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="Fax_Label" runat="server" Text="Fax"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="TextBoxFax" runat="server" Columns="20" MaxLength="20"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </div>
                <asp:HiddenField ID="UserMasterHiddenField" runat="server" />
                <asp:SqlDataSource ID="dsStep1Personale" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                    InsertCommand=" SET DATEFORMAT dmy; 
                    INSERT INTO tbProfiliPersonali
                    (UserId, Cognome, Nome, CodiceFiscale, Qualifica, Professione, Sesso, Indirizzo, Numero, Citta, Comune, Cap, ProvinciaEstesa, ProvinciaSigla, ID2Nazione, NascitaData, NascitaCitta, NascitaProvinciaSigla, NascitaID2Nazione, Telefono1, Telefono2, Fax,UserMaster,RecordNewUser,RecordNewDate) 
                    VALUES (@UserId, @Cognome,@Nome, @CodiceFiscale, @Qualifica, @Professione, @Sesso, @Indirizzo, @Numero, @Citta, @Comune, @Cap, @ProvinciaEstesa, @ProvinciaSigla, @ID2Nazione, @NascitaData, @NascitaCitta, @NascitaProvinciaSigla, @NascitaID2Nazione, @Telefono1, @Telefono2, @Fax,@UserMaster,@RecordNewUser,@RecordNewDate)">
                    <InsertParameters>
                        <asp:ControlParameter Name="Cognome" Type="String" ControlID="TextBoxCognome" PropertyName="Text" />
                        <asp:ControlParameter Name="Nome" Type="String" ControlID="TextBoxNome" PropertyName="Text" />
                        <asp:ControlParameter Name="CodiceFiscale" Type="String" ControlID="TextBoxCodiceFiscale"
                            PropertyName="Text" />
                        <asp:ControlParameter Name="Qualifica" Type="String" ControlID="TextBoxQualifica"
                            PropertyName="Text" />
                        <asp:ControlParameter Name="Professione" Type="String" ControlID="ProfessioneTextBox"
                            PropertyName="Text" />
                        <asp:ControlParameter Name="Sesso" Type="String" ControlID="SessoDropDownlist" PropertyName="SelectedValue" />
                        <asp:ControlParameter Name="Indirizzo" Type="String" ControlID="TextBoxIndirizzo"
                            PropertyName="Text" />
                        <asp:Parameter Name="Numero" Type="String" />
                        <asp:ControlParameter Name="Citta" Type="String" ControlID="TextBoxCitta" PropertyName="Text" />
                        <asp:ControlParameter Name="Comune" Type="String" ControlID="TextBoxComune" PropertyName="Text" />
                        <asp:ControlParameter Name="Cap" Type="String" ControlID="TextBoxCap" PropertyName="Text" />
                        <asp:ControlParameter Name="ProvinciaEstesa" Type="String" ControlID="TextBoxProvincia"
                            PropertyName="Text" />
                        <asp:ControlParameter Name="ProvinciaSigla" Type="String" ControlID="TextBoxSiglaProvincia"
                            PropertyName="Text" />
                        <asp:ControlParameter Name="ID2Nazione" Type="Int16" ControlID="DropDownListNazione"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter Name="NascitaData" Type="String" ControlID="TextBoxData" PropertyName="Text" />
                        <asp:ControlParameter Name="NascitaCitta" Type="String" ControlID="TextBoxCittaNascita"
                            PropertyName="Text" />
                        <asp:ControlParameter Name="NascitaProvinciaSigla" Type="String" ControlID="TextBoxProvinciaNascita"
                            PropertyName="Text" />
                        <asp:ControlParameter Name="NascitaID2Nazione" Type="Int16" ControlID="DropDownListNazioneNascita"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter Name="Telefono1" Type="String" ControlID="TextBoxTelefonoCasa"
                            PropertyName="Text" />
                        <asp:ControlParameter Name="Telefono2" Type="String" ControlID="TextBoxTelefonoCellulare"
                            PropertyName="Text" />
                        <asp:ControlParameter Name="Fax" Type="String" ControlID="TextBoxFax" PropertyName="Text" />
                        <asp:ControlParameter Name="UserMaster" ControlID="UserMasterHiddenField" />
                    </InsertParameters>
                </asp:SqlDataSource>
            </asp:WizardStep>
            <asp:WizardStep runat="server" Title="Step 2 - Affiliazione" ID="CreateUserWizardStep2"
                OnActivate="ProfiloSocietario_OnActivate">
                <div class="LayWizardStep">
                    <asp:Label SkinID="WizardStep" ID="NavigationLabel1b" runat="server" Text="Profilo personale "></asp:Label>
                    <asp:Label SkinID="WizardStepCurrent" ID="NavigationLabel2b" runat="server" Text="&gt; Profilo azienda / ente "></asp:Label>
                    <asp:Label SkinID="WizardStep" ID="NavigationLabel3b" runat="server" Text="&gt; Preferenze "></asp:Label>
                    <asp:Label SkinID="WizardStep" ID="NavigationLabel4b" runat="server" Text="&gt; Ruoli utente "></asp:Label>
                    <asp:Label SkinID="WizardStep" ID="NavigationLabel5b" runat="server" Text="&gt; Dati di accesso account utente"></asp:Label>
                </div>
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="Azienda_Label" runat="server" Text="Azienda / ente / societ�"></asp:Label>
                    </div>
                    <table>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="RagioneSoc_Label" runat="server" Text="Ragione sociale"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="TextBoxRagioneSociale" runat="server" Columns="80" MaxLength="100"></asp:TextBox>
                            </td>
                        </tr>
                        <asp:Panel ID="ZMCF_BusinessBook" runat="server">
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label SkinID="FieldDescription" ID="AziendaCollegata_Label" runat="server" Text="Azienda collegata"></asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:DropDownList ID="AziendaCollegataDropDownList" runat="server" DataSourceID="AziendaCollegataSqlDataSource"
                                        DataTextField="Identificativo" DataValueField="ID1BsnBook" AppendDataBoundItems="true">
                                        <asp:ListItem Text="&gt; Non inserita" Value="0" Selected="True"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="AziendaCollegataSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                        SelectCommand="SELECT [ID1BsnBook],[Identificativo]
                                FROM [tbBusinessBook]
                                WHERE [PW_Zeus1]=1 AND ZeusIsAlive=1"></asp:SqlDataSource>
                                </td>
                            </tr>
                        </asp:Panel>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="PIVA_Label" runat="server" Text="Partita IVA o codice fiscale"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="TextBoxPartitaIVA" runat="server" Columns="20" MaxLength="20"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="SedeLegale_Label" runat="server" Text="Sede legale"></asp:Label>
                    </div>
                    <table>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="IndirizzoAzienda_Label" runat="server" Text="Indirizzo"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="TextBoxIndirizzoLegale" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
                                <%--<asp:Label SkinID="FieldDescription" ID="NumeroAzienda_Label" runat="server" Text=" Numero"></asp:Label>
                                <asp:TextBox ID="TextBoxNumeroLegale" runat="server" Columns="10" MaxLength="10"></asp:TextBox>--%>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="CittaAzienda_Label" runat="server" Text="Citt�"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="TextBoxCittaLegale" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="ComuneAzienda_Label" runat="server" Text="Comune"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="TextBoxComuneLegale" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
                                <asp:Label SkinID="FieldDescription" ID="CAPAzienda_Label" runat="server" Text=" CAP"></asp:Label>
                                <asp:TextBox ID="TextBoxCAPLegale" runat="server" Columns="5" MaxLength="5"></asp:TextBox>
                                <asp:RangeValidator SkinID="ZSSM_Validazione01" Display="Dynamic" ID="RangeValidator2"
                                    runat="server" ControlToValidate="TextBoxCAPLegale" ErrorMessage="CAP non corretto"
                                    MaximumValue="99999" MinimumValue="0"></asp:RangeValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="ProvinciaAzienda_Label" runat="server" Text="Provincia"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="TextBoxProvinciaLegale" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
                                <asp:Label SkinID="FieldDescription" ID="SiglaProvAzienda_Label" runat="server" Text="Sigla provincia"></asp:Label>
                                <asp:TextBox ID="TextBoxSiglaLegale" runat="server" Columns="2" MaxLength="2"></asp:TextBox>
                                <asp:RegularExpressionValidator Display="Dynamic" SkinID="ZSSM_Validazione01" ID="RegularExpressionValidator6"
                                    runat="server" ControlToValidate="TextBoxSiglaLegale" ErrorMessage="Sigla provincia non corretta"
                                    ValidationExpression="^[A-Za-z]{2}$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="NazioneAzienda_Label" runat="server" Text="Nazione"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:DropDownList ID="DropDownListNazioneLegale" runat="server" DataSourceID="dsNazioni"
                                    DataTextField="Nazione" DataValueField="ID1Nazione">
                                    <asp:ListItem Text="&gt; Non inserita" Selected="True" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="dsNazioni" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                    SelectCommand="SELECT [ID1Nazione], [Nazione] FROM [tbNazioni] ORDER BY [Nazione]"></asp:SqlDataSource>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="SedeIstituto_Label" runat="server" Text="Sede Istituto"></asp:Label>
                    </div>
                    <table>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="IndirizzoAzienda2_Label" runat="server"
                                    Text="Indirizzo"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="TextBoxIndirizzoIstituto" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
                                <%--<asp:Label SkinID="FieldDescription" ID="NumeroAzienda2_Label" runat="server" Text=" Numero"></asp:Label>
                                <asp:TextBox ID="TextBoxNumeroIstituto" runat="server" Columns="10" MaxLength="10"></asp:TextBox>--%>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="CittaAzienda2_Label" runat="server" Text="Citt�"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="TextBoxCittaIstituto" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="ComuneAzienda2_Label" runat="server" Text="Comune"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="TextBoxComuneIstituto" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
                                <asp:Label SkinID="FieldDescription" ID="CAPAzienda2_Label" runat="server" Text=" CAP"></asp:Label>
                                <asp:TextBox ID="TextBoxCapIstituto" runat="server" Columns="5" MaxLength="5"></asp:TextBox>
                                <asp:RangeValidator Display="Dynamic" SkinID="ZSSM_Validazione01" ID="RangeValidator3"
                                    runat="server" ControlToValidate="TextBoxCapIstituto" ErrorMessage="CAP non corretto"
                                    MaximumValue="99999" MinimumValue="0"></asp:RangeValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="ProvinciaAzienda2_Label" runat="server"
                                    Text="Provincia"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="TextBoxProvinciaIstituto" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
                                <asp:Label SkinID="FieldDescription" ID="SiglaProvinciaAzienda2_Label" runat="server"
                                    Text=" Sigla provincia"></asp:Label>
                                <asp:TextBox ID="TextBoxSiglaIstituto" runat="server" Columns="2" MaxLength="2"></asp:TextBox>
                                <asp:RegularExpressionValidator Display="Dynamic" SkinID="ZSSM_Validazione01" ID="RegularExpressionValidator7"
                                    runat="server" ControlToValidate="TextBoxSiglaIstituto" ErrorMessage="Sigla provincia non corretta"
                                    ValidationExpression="^[A-Za-z]{2}$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="NazioneAzeinda2_Label" runat="server" Text="Nazione"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:DropDownList ID="DropDownListNazioneIstituto" runat="server" DataSourceID="dsNazioni"
                                    DataTextField="Nazione" DataValueField="ID1Nazione">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="ContattiAzienda_Label" runat="server" Text="Contatti"></asp:Label>
                    </div>
                    <table>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="TelefonoAzienda_Label" runat="server" Text="Telefono Istituto"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="TextBoxTelefonoIstituto" runat="server" Columns="20" MaxLength="20"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="TelefonoCellulare_Label" runat="server"
                                    Text="Telefono cellulare"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="TextBoxCellulareSocietario" runat="server" Columns="20" MaxLength="20"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="FaxAzienda_Label" runat="server" Text="Fax"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="TextBoxFaxSocietario" runat="server" Columns="20" MaxLength="20"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="Label29" runat="server" Text="Sito web"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="TextBoxSitoWeb" runat="server" Columns="80" MaxLength="100"></asp:TextBox>
                                <asp:RegularExpressionValidator Display="Dynamic" SkinID="ZSSM_Validazione01" ID="RegularExpressionValidator1"
                                    runat="server" ControlToValidate="TextBoxSitoWeb" ErrorMessage="Indirizzo non corretto"
                                    ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                    </table>
                </div>
                <asp:SqlDataSource ID="dsStep2Societario" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                    InsertCommand=" SET DATEFORMAT dmy; INSERT INTO [tbProfiliSocietari] 
                    ([UserId], [RagioneSociale], [PartitaIVA], [S1_Indirizzo], [S1_Numero], [S1_Citta], [S1_Comune], [S1_Cap], 
                    [S1_ProvinciaEstesa], [S1_ProvinciaSigla], [S1_ID2Nazione], [S2_Indirizzo], [S2_Numero], [S2_Citta], [S2_Comune],
                     [S2_Cap], [S2_ProvinciaEstesa], [S2_ProvinciaSigla], [S2_ID2Nazione], [Telefono1], [Telefono2], [Fax], 
                     [WebSite],[RecordNewUser],[RecordNewDate],[ID2BusinessBook]) 
                     VALUES (@UserId, @RagioneSociale, @PartitaIVA, @S1_Indirizzo, @S1_Numero, @S1_Citta, @S1_Comune, 
                     @S1_Cap, @S1_ProvinciaEstesa, @S1_ProvinciaSigla, @S1_ID2Nazione, @S2_Indirizzo, @S2_Numero, 
                     @S2_Citta, @S2_Comune, @S2_Cap, @S2_ProvinciaEstesa, @S2_ProvinciaSigla, @S2_ID2Nazione, @Telefono1,
                     @Telefono2, @Fax, @WebSite,@RecordNewUser,@RecordNewDate,@ID2BusinessBook)">
                    <InsertParameters>
                        <asp:ControlParameter Name="RagioneSociale" Type="String" ControlID="TextBoxRagioneSociale"
                            PropertyName="Text" ConvertEmptyStringToNull="false" DefaultValue="" />
                        <asp:ControlParameter Name="PartitaIVA" Type="String" ControlID="TextBoxPartitaIVA"
                            PropertyName="Text" />
                        <asp:ControlParameter Name="S1_Indirizzo" Type="String" ControlID="TextBoxIndirizzoLegale"
                            PropertyName="Text" />
                        <asp:Parameter Name="S1_Numero" Type="String" />
                        <asp:ControlParameter Name="S1_Citta" Type="String" ControlID="TextBoxCittaLegale"
                            PropertyName="Text" />
                        <asp:ControlParameter Name="S1_Comune" Type="String" ControlID="TextBoxComuneLegale"
                            PropertyName="Text" />
                        <asp:ControlParameter Name="S1_Cap" Type="String" ControlID="TextBoxCAPLegale" PropertyName="Text" />
                        <asp:ControlParameter Name="S1_ProvinciaEstesa" Type="String" ControlID="TextBoxProvinciaLegale"
                            PropertyName="Text" />
                        <asp:ControlParameter Name="S1_ProvinciaSigla" Type="String" ControlID="TextBoxSiglaLegale"
                            PropertyName="Text" />
                        <asp:ControlParameter Name="S1_ID2Nazione" Type="Int32" ControlID="DropDownListNazioneLegale"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter Name="S2_Indirizzo" Type="String" ControlID="TextBoxIndirizzoIstituto"
                            PropertyName="Text" />
                        <asp:Parameter Name="S2_Numero" Type="String" />
                        <asp:ControlParameter Name="S2_Citta" Type="String" ControlID="TextBoxCittaIstituto"
                            PropertyName="Text" />
                        <asp:ControlParameter Name="S2_Comune" Type="String" ControlID="TextBoxComuneIstituto"
                            PropertyName="Text" />
                        <asp:ControlParameter Name="S2_Cap" Type="String" ControlID="TextBoxCapIstituto" PropertyName="Text" />
                        <asp:ControlParameter Name="S2_ProvinciaEstesa" Type="String" ControlID="TextBoxProvinciaIstituto"
                            PropertyName="Text" />
                        <asp:ControlParameter Name="S2_ProvinciaSigla" Type="String" ControlID="TextBoxSiglaIstituto"
                            PropertyName="Text" />
                        <asp:ControlParameter Name="S2_ID2Nazione" Type="Int32" ControlID="DropDownListNazioneIstituto"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter Name="Telefono1" Type="String" ControlID="TextBoxTelefonoIstituto"
                            PropertyName="Text" />
                        <asp:ControlParameter Name="Telefono2" Type="String" ControlID="TextBoxCellulareSocietario"
                            PropertyName="Text" />
                        <asp:ControlParameter Name="Fax" Type="String" ControlID="TextBoxFaxSocietario" PropertyName="Text" />
                        <asp:ControlParameter Name="WebSite" Type="String" ControlID="TextBoxSitoWeb" PropertyName="Text" />
                        <asp:ControlParameter Name="ID2BusinessBook" Type="String" ControlID="AziendaCollegataDropDownList"
                            PropertyName="SelectedValue" />
                    </InsertParameters>
                </asp:SqlDataSource>
            </asp:WizardStep>
            <asp:WizardStep runat="server" Title="Step 3 - Preferenze" ID="CreateUserWizardStep3"
                OnActivate="ProfiloMarketing_OnActivate">
                <div class="LayWizardStep">
                    <asp:Label SkinID="WizardStep" ID="NavigationLabel1c" runat="server" Text="Profilo personale "></asp:Label>
                    <asp:Label SkinID="WizardStep" ID="NavigationLabel2c" runat="server" Text="&gt; Profilo azienda / ente "></asp:Label>
                    <asp:Label SkinID="WizardStepCurrent" ID="NavigationLabel3c" runat="server" Text="&gt; Preferenze "></asp:Label>
                    <asp:Label SkinID="WizardStep" ID="NavigationLabel4c" runat="server" Text="&gt; Ruoli utente "></asp:Label>
                    <asp:Label SkinID="WizardStep" ID="NavigationLabel5c" runat="server" Text="&gt; Dati di accesso account utente"></asp:Label>
                </div>
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="Preferenze_Label" runat="server" Text="Preferenze"></asp:Label>
                    </div>
                    <table>
                        <asp:Panel ID="ZMCF_Categoria1" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:Label ID="ZML_Categoria1" SkinID="FieldDescription" runat="server" Text="Categoria"></asp:Label>
                                    <asp:DropDownList ID="ID2CategoriaDropDownList" runat="server" OnSelectedIndexChanged="ID2CategoriaDropDownList_SelectIndexChange"
                                        AppendDataBoundItems="True">
                                        <asp:ListItem Text="> Non inserito" Value="0" Selected="True"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="ID2CategoriaHiddenField" runat="server" />
                                    <asp:SqlDataSource ID="dsCategoria" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>">
                                        <SelectParameters>
                                            <asp:QueryStringParameter DefaultValue="0" Name="ZeusIdModulo" QueryStringField="ZIM"
                                                Type="String" />
                                            <asp:QueryStringParameter DefaultValue="ITA" Name="ZeusLangCode" QueryStringField="Lang"
                                                Type="String" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_ConsensoDatiPersonali" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="CheckBoxDatiPersonali" runat="server" Checked="True" />
                                    <asp:Label SkinID="FieldValue" ID="ZML_ConsensoDatiPersonali" runat="server" Text="Consenso trattamento dati personali"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_ConsensoDatiPersonali2" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="CheckBoxDatiPersonali2" runat="server" Checked="True" />
                                    <asp:Label SkinID="FieldValue" ID="ZML_ConsensoDatiPersonali2" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Comunicazioni1" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="Comunicazioni1CheckBox" runat="server" Checked="True" />
                                    <asp:Label SkinID="FieldValue" ID="ZML_Comunicazioni1" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Comunicazioni2" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="Comunicazioni2CheckBox" runat="server" Checked="True" />
                                    <asp:Label SkinID="FieldValue" ID="ZML_Comunicazioni2" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Comunicazioni3" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="Comunicazioni3CheckBox" runat="server" Checked="True" />
                                    <asp:Label SkinID="FieldValue" ID="ZML_Comunicazioni3" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Comunicazioni4" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="Comunicazioni4CheckBox" runat="server" Checked="True" />
                                    <asp:Label SkinID="FieldValue" ID="ZML_Comunicazioni4" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Comunicazioni5" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="Comunicazioni5CheckBox" runat="server" Checked="True" />
                                    <asp:Label SkinID="FieldValue" ID="ZML_Comunicazioni5" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Comunicazioni6" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="Comunicazioni6CheckBox" runat="server" Checked="True" />
                                    <asp:Label SkinID="FieldValue" ID="ZML_Comunicazioni6" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Comunicazioni7" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="Comunicazioni7CheckBox" runat="server" Checked="True" />
                                    <asp:Label SkinID="FieldValue" ID="ZML_Comunicazioni7" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Comunicazioni8" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="Comunicazioni8CheckBox" runat="server" Checked="True" />
                                    <asp:Label SkinID="FieldValue" ID="ZML_Comunicazioni8" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Comunicazioni9" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="Comunicazioni9CheckBox" runat="server" Checked="True" />
                                    <asp:Label SkinID="FieldValue" ID="ZML_Comunicazioni9" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Comunicazioni10" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="Comunicazioni10CheckBox" runat="server" Checked="True" />
                                    <asp:Label SkinID="FieldValue" ID="ZML_Comunicazioni10" runat="server" Text="Iscrizione Comunicazioni"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                    </table>
                </div>
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="Label8" runat="server" Text="Ricerca interna utenti"></asp:Label>
                    </div>
                    <table>
                        <asp:Panel ID="Panel1" runat="server">
                            <tr>
                                <td class="BlockBoxValue">
                                    <asp:Label SkinID="FieldValue" ID="Label9" runat="server" Text="Tag ricerca utenti Zeus"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="RicercaUtentiTextBox" Width="600"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <asp:Label SkinID="FieldValue" ID="Label10" runat="server" Text="Separare le keywords con virgola"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                    </table>
                </div>
                <asp:SqlDataSource ID="dsStep3Marketing" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                    InsertCommand=" SET DATEFORMAT dmy; INSERT INTO tbProfiliMarketing (UserId, ConsensoDatiPersonali,ConsensoDatiPersonali2,
                    Comunicazioni1,Comunicazioni2,Comunicazioni3,Comunicazioni4,Comunicazioni5,
                    Comunicazioni6,Comunicazioni7,Comunicazioni8,Comunicazioni9,Comunicazioni10,
                    RecordNewUser,RecordNewDate,ID2Categoria1,RegCausal,Meta_ZeusSearch) 
                    VALUES  (@UserId, @ConsensoDatiPersonali,@ConsensoDatiPersonali2,
                    @Comunicazioni1,@Comunicazioni2,@Comunicazioni3,@Comunicazioni4,@Comunicazioni5,
                    @Comunicazioni6,@Comunicazioni7,@Comunicazioni8,@Comunicazioni9,@Comunicazioni10,
                    @RecordNewUser,@RecordNewDate,@ID2Categoria1,@RegCausal,@Meta_ZeusSearch)">
                    <InsertParameters>
                        <asp:ControlParameter Name="ID2Categoria1" Type="Int32" ControlID="ID2CategoriaHiddenField"
                            PropertyName="Value" />
                        <asp:ControlParameter Name="ConsensoDatiPersonali" Type="Boolean" ControlID="CheckBoxDatiPersonali"
                            PropertyName="Checked" DefaultValue="false" />
                        <asp:ControlParameter Name="ConsensoDatiPersonali2" Type="Boolean" ControlID="CheckBoxDatiPersonali2"
                            PropertyName="Checked" DefaultValue="false" />
                        <asp:ControlParameter Name="Comunicazioni1" Type="Boolean" ControlID="Comunicazioni1CheckBox"
                            PropertyName="Checked" DefaultValue="false" />
                        <asp:ControlParameter Name="Comunicazioni2" Type="Boolean" ControlID="Comunicazioni2CheckBox"
                            PropertyName="Checked" DefaultValue="false" />
                        <asp:ControlParameter Name="Comunicazioni3" Type="Boolean" ControlID="Comunicazioni3CheckBox"
                            PropertyName="Checked" DefaultValue="false" />
                        <asp:ControlParameter Name="Comunicazioni4" Type="Boolean" ControlID="Comunicazioni4CheckBox"
                            PropertyName="Checked" DefaultValue="false" />
                        <asp:ControlParameter Name="Comunicazioni5" Type="Boolean" ControlID="Comunicazioni5CheckBox"
                            PropertyName="Checked" DefaultValue="false" />
                        <asp:ControlParameter Name="Comunicazioni6" Type="Boolean" ControlID="Comunicazioni6CheckBox"
                            PropertyName="Checked" DefaultValue="false" />
                        <asp:ControlParameter Name="Comunicazioni7" Type="Boolean" ControlID="Comunicazioni7CheckBox"
                            PropertyName="Checked" DefaultValue="false" />
                        <asp:ControlParameter Name="Comunicazioni8" Type="Boolean" ControlID="Comunicazioni8CheckBox"
                            PropertyName="Checked" DefaultValue="false" />
                        <asp:ControlParameter Name="Comunicazioni9" Type="Boolean" ControlID="Comunicazioni9CheckBox"
                            PropertyName="Checked" DefaultValue="false" />
                        <asp:ControlParameter Name="Comunicazioni10" Type="Boolean" ControlID="Comunicazioni10CheckBox"
                            PropertyName="Checked" DefaultValue="false" />
                        <asp:ControlParameter Name="Meta_ZeusSearch" Type="String" ControlID="RicercaUtentiTextBox"
                            PropertyName="Text" DefaultValue="" />
                    </InsertParameters>
                </asp:SqlDataSource>
            </asp:WizardStep>
            <asp:WizardStep runat="server" Title="Step 4 - Ruoli utente" ID="WizardStep4" OnActivate="Ruoli_OnActivate">
                <div class="LayWizardStep">
                    <asp:Label SkinID="WizardStep" ID="NavigationLabel1d" runat="server" Text="Profilo personale "></asp:Label>
                    <asp:Label SkinID="WizardStep" ID="NavigationLabel2d" runat="server" Text="&gt; Profilo azienda / ente "></asp:Label>
                    <asp:Label SkinID="WizardStep" ID="NavigationLabel3d" runat="server" Text="&gt; Preferenze "></asp:Label>
                    <asp:Label SkinID="WizardStepCurrent" ID="NavigationLabel4d" runat="server" Text="&gt; Ruoli utente "></asp:Label>
                    <asp:Label SkinID="WizardStep" ID="NavigationLabel5d" runat="server" Text="&gt; Dati di accesso account utente"></asp:Label>
                </div>
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="RuoliUtente_Label" runat="server" Text="Ruoli utente"></asp:Label>
                    </div>
                    <table>
                        <tr>
                            <td class="BlockBoxValue" colspan="2">
                                <asp:CheckBoxList SkinID="FieldValue" CellPadding="0" CellSpacing="0" ID="RolesCheckBoxList"
                                    runat="server" OnLoad="RolesCheckboxList_OnLoad" RepeatDirection="Vertical">
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:WizardStep>
            <asp:CreateUserWizardStep runat="server" Title="Step 5 - Dati di accesso account utente"
                ID="CreateUserWizardStep5" OnActivate="AccountUtente_OnActivate">
                <ContentTemplate>
                    <div class="LayWizardStep">
                        <asp:Label SkinID="WizardStep" ID="NavigationLabel1e" runat="server" Text="Profilo personale "></asp:Label>
                        <asp:Label SkinID="WizardStep" ID="NavigationLabel2e" runat="server" Text="> Profilo azienda / ente "></asp:Label>
                        <asp:Label SkinID="WizardStep" ID="NavigationLabel3e" runat="server" Text="> Preferenze "></asp:Label>
                        <asp:Label SkinID="WizardStep" ID="NavigationLabel4e" runat="server" Text="> Ruoli utente "></asp:Label>
                        <asp:Label SkinID="WizardStepCurrent" ID="NavigationLabel5e" runat="server" Text="> Dati di accesso account utente"></asp:Label>
                    </div>
                    <div class="BlockBox">
                        <div class="BlockBoxHeader">
                            <asp:Label ID="Step4_Label" runat="server" Text="Dati di accesso account utente"></asp:Label>
                        </div>
                        <table>
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label SkinID="FieldDescription" ID="UserNameLabel" runat="server" Text="UserName"></asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:TextBox ID="UserName" runat="server" Columns="50" MaxLength="80"></asp:TextBox>
                                    <asp:RequiredFieldValidator Display="Dynamic" SkinID="ZSSM_Validazione01" ID="UserNameRequired"
                                        runat="server" ControlToValidate="UserName" ToolTip="Il nome utente � obbligatorio."
                                        ValidationGroup="CreateUserWizard1" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                                    <span class="ZSSM_Validazione01" style="color: #FF0000">
                                        <asp:Literal ID="ErrorMessage" runat="server" EnableViewState="False"></asp:Literal></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label SkinID="FieldDescription" ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password</asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:TextBox ID="Password" runat="server" TextMode="Password" Columns="50" MaxLength="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator Display="Dynamic" SkinID="ZSSM_Validazione01" ID="PasswordRequired"
                                        runat="server" ControlToValidate="Password" ToolTip="La password � obbligatoria."
                                        ValidationGroup="CreateUserWizard1" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                                    <asp:CompareValidator SkinID="ZSSM_Validazione01" ID="PasswordCompare" runat="server"
                                        ControlToCompare="Password" ControlToValidate="ConfirmPassword" Display="Dynamic"
                                        ErrorMessage="Password e conferma password non coincidono" ValidationGroup="CreateUserWizard1"></asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label SkinID="FieldDescription" ID="ConfirmPasswordLabel" runat="server" AssociatedControlID="ConfirmPassword">Conferma password</asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password" Columns="50"
                                        MaxLength="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator Display="Dynamic" ID="ConfirmPasswordRequired" SkinID="ZSSM_Validazione01"
                                        runat="server" ControlToValidate="ConfirmPassword" ToolTip="La password di conferma � obbligatoria."
                                        ValidationGroup="CreateUserWizard1" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label SkinID="FieldDescription" ID="EmailLabel" runat="server" AssociatedControlID="Email">Posta elettronica</asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:TextBox ID="Email" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator Display="Dynamic" SkinID="ZSSM_Validazione01" ID="EmailRequired"
                                        runat="server" ControlToValidate="Email" ToolTip="Indirizzo E-Mail non corretto"
                                        ValidationGroup="CreateUserWizard1" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="MailRegulaeExpressionValidation" runat="server"
                                        Display="Dynamic" ValidationExpression="^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
                                        ControlToValidate="Email" ErrorMessage="Indirizzo E-Mail non corretto" SkinID="ZSSM_Validazione01"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="BlockBox">
                        <div class="BlockBoxHeader">
                            <asp:Label ID="Label6" runat="server" Text="Comunicazione di avvenuta creazione utente"></asp:Label>
                        </div>
                        <table>
                            <tr>
                                <td class="BlockBoxDescription"></td>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="CheckBoxInvioMail" runat="server" Checked="False" />
                                    <asp:Label SkinID="FieldValue" ID="ZML_ConsensoDatiPersonali" runat="server" Text="Invia conferma via E-Mail all�indirizzo inserito sopra"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                    </div>
                </ContentTemplate>
            </asp:CreateUserWizardStep>
            <asp:CompleteWizardStep runat="server">
                <ContentTemplate>
                    <table border="0">
                        <tr>
                            <td align="left" colspan="2">
                                <asp:Label SkinID="ZSSM_Validazione01" ID="WarningLabel" runat="server" Text="ATTENZIONE: l'utente non � stato creato correttamente." />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label SkinID="ZSSM_Validazione01" ID="WarningLabel2" runat="server" Text="Contattare l'amministratore di sistema per segnalare il problema." />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" colspan="2">
                                <asp:Button ID="ContinueButton" runat="server" CausesValidation="False" CommandName="Continue"
                                    Text="Continue" ValidationGroup="CreateUserWizard1" />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:CompleteWizardStep>
        </WizardSteps>
        <FinishCompleteButtonStyle CssClass="Button1" />
    </asp:CreateUserWizard>
</asp:Content>
