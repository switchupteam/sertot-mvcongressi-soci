﻿<%@ Control Language="C#" ClassName="Dashboard_UsersToEnable" %>

<script runat="server">


    private string GetCausalHumanReadble(string RegCasual)
    {

        if (RegCasual.Length == 0)
            return string.Empty;

        string CausalHumanReadble = string.Empty;

        try
        {

            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("~/App_Data/RegistrationCausal.xml"));
            CausalHumanReadble = mydoc.SelectSingleNode("RootCasual/RegDescription[@RegCausal='" + RegCasual + "']").InnerText;

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

        return CausalHumanReadble;

    }//fine GetCausalHumanReadble


</script>

<div class="LayGridTitolo1">
    <asp:Label ID="CaptionallRecordLabel" runat="server" SkinID="GridTitolo1" Text="Utenti in attesa di approvazione"></asp:Label>
</div>
<asp:GridView ID="UserList_GridView" runat="server" AllowPaging="True" AllowSorting="True"
    PageSize="50" AutoGenerateColumns="False" DataSourceID="UtentiDaApprovareSqlDataSource">
    <Columns>
        <asp:BoundField DataField="Cognome" HeaderText="Cognome" SortExpression="Cognome">
            <ItemStyle HorizontalAlign="Left" />
        </asp:BoundField>
        <asp:BoundField DataField="Nome" HeaderText="Nome" SortExpression="Nome">
            <ItemStyle HorizontalAlign="Left" />
        </asp:BoundField>
        <asp:BoundField DataField="RagioneSociale" HeaderText="Ragione sociale" SortExpression="RagioneSociale">
            <ItemStyle HorizontalAlign="Left" />
        </asp:BoundField>
        <asp:BoundField DataField="UserName" HeaderText="UserName" SortExpression="UserName">
            <ItemStyle HorizontalAlign="Center" />
        </asp:BoundField>
        <asp:BoundField DataField="Email" HeaderText="E-Mail" SortExpression="Email">
            <ItemStyle HorizontalAlign="Left" />
        </asp:BoundField>
        <asp:BoundField DataField="CreateDate" HeaderText="Data registrazione" SortExpression="CreateDate">
            <ItemStyle HorizontalAlign="Center" />
        </asp:BoundField>
        <asp:TemplateField HeaderText="Causale" SortExpression="Causale">
            <ItemTemplate>
                <asp:Label ID="CasualLabel" runat="server" Text='<%# GetCausalHumanReadble(Eval("RegCausal").ToString()) %>'></asp:Label>
            </ItemTemplate>
            <ItemStyle HorizontalAlign="Center" />
        </asp:TemplateField>
        <asp:HyperLinkField DataNavigateUrlFields="UserID" DataNavigateUrlFormatString="Account_Dtl.aspx?UID={0}"
            HeaderText="Abilita"  Text="Dettaglio">
            <ItemStyle HorizontalAlign="Center" />
            <ControlStyle CssClass="GridView_ZeusButton1" />
        </asp:HyperLinkField>
    </Columns>
    <EmptyDataTemplate>
        <div style="text-align: center;">
            Non sono presenti utenti da approvare</div>
    </EmptyDataTemplate>
    <EmptyDataRowStyle BackColor="#FFFFFF" Height="200px" />
</asp:GridView>
<asp:SqlDataSource ID="UtentiDaApprovareSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
    SelectCommand=" SELECT Cognome , Nome , RagioneSociale , UserName , Email , CreateDate ,RegCausal,UserID 
        FROM vwAccount_Lst_ToApprove"></asp:SqlDataSource>
