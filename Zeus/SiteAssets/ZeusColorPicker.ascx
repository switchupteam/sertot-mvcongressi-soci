﻿<%@ Control Language="C#" ClassName="ZeusColorPicker" %>

<script runat="server">

    
    private bool isEditMode = false;
    private bool isVisibleMode = true;
    private string validationGroup;

    public string ValidationGroup
    {
        set { validationGroup = value; }
    }

    public bool IsVisibleMode
    {
        set { isVisibleMode = value; }
        get { return isVisibleMode; }
    }
    
    public bool IsEditMode
    {
        set { isEditMode = value; }
        get { return isEditMode; }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
            ColorTextBox.Attributes.Add("onchange", "setColorFromText()");
            DiscountAmountCustomValidator.ValidationGroup = validationGroup;
        
    }

    public string SelectedColor
    {
        get { return ColorTextBox.Text; }
        set { ColorTextBox.Text = value; }
    }

    protected override void OnDataBinding(EventArgs e)
    {
        base.OnDataBinding(e);

        if ((isEditMode) 
            && (isVisibleMode))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartUpScript", "setColorFromText();", true);

        
    }

    public void AddScript()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartUpScript", "setColorFromText();", true);
    }
    
</script>

<script language="javascript">
   function colorChanged(sender) {

  document.getElementById('ColorDiv').style.backgroundColor  = 
       "#" + sender.get_selectedColor();      
}


function setColorFromText()
{
    if(document.getElementById("<%=ColorTextBox.ClientID%>") != null)
        if(document.getElementById("<%=ColorTextBox.ClientID%>").value.match(/[a-fA-F0-9]{6}/))
                    document.getElementById('ColorDiv').style.backgroundColor  = document.getElementById("<%=ColorTextBox.ClientID%>").value;
               
}


function CheckColorTexBox(sender, args)
{

    if(document.getElementById("<%=ColorTextBox.ClientID%>").value.match(/[a-fA-F0-9]{6}/) || document.getElementById("<%=ColorTextBox.ClientID%>").value.length<=0)
      args.IsValid = true;
    else   args.IsValid = false;
    
    return;
}


function Clear()
{
  document.getElementById("<%=ColorTextBox.ClientID%>").value="";
  document.getElementById('ColorDiv').style.backgroundColor="";
  
}

</script>


<%--<asp:ScriptManager ID="myScriptManager" runat="server">
</asp:ScriptManager>
--%><div>
    <table border="0" cellspacing="0" cellpadding="2">
        <tr>
            <td>
                <div id="ColorDiv" style="width: 40px; height: 20px; border: 1px solid #333333;">
                </div>
            </td>
            <td>
                <asp:Label ID="InfoLabel" runat="server" Text="#" Font-Bold="true" SkinID="FieldValue"></asp:Label>
                <asp:TextBox ID="ColorTextBox" runat="server" MaxLength="6" Columns="10"></asp:TextBox>
                <asp:Image ID="ColorImage" runat="server" ImageUrl="~/Zeus/SiteImg/ColoPickerButton.png" />
                <input type="button" class="ZSSM_Button01_Button" onclick="Clear()" value="Trasparente" />
                <asp:CustomValidator ID="DiscountAmountCustomValidator" runat="server" ControlToValidate="ColorTextBox"
                    ClientValidationFunction="CheckColorTexBox" ErrorMessage="Codice colore non valido"
                    SkinID="ZSSM_Validazione01" ToolTip="Codice colore non valido"></asp:CustomValidator>
                 <ajaxToolkit:ColorPickerExtender runat="server" ID="ColorPickerExtender1" TargetControlID="ColorTextBox"
                    PopupButtonID="ColorImage" OnClientColorSelectionChanged="colorChanged" />
            </td>
        </tr>
    </table>
</div>
