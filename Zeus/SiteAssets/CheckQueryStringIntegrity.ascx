﻿<%@ Control Language="C#" ClassName="CheckQueryStringIntegrity" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">

    private string mySQL_TABLE = string.Empty;
    private string myZeusIdModulo = string.Empty;
    private string myZeusLangCode = string.Empty;
    private string myPRIMARY_KEY = string.Empty;
    private string myPRIMARY_KEY_ID = string.Empty;



    public string SQL_TABLE
    {
        get { return mySQL_TABLE; }
        set { mySQL_TABLE = value; }
    }


    public string ZeusIdModulo
    {
        get { return myZeusIdModulo; }
        set { myZeusIdModulo = value; }
    }


    public string ZeusLangCode
    {
        get { return myZeusLangCode; }
        set { myZeusLangCode = value; }
    }

    public string PRIMARY_KEY
    {
        get { return myPRIMARY_KEY; }
        set { myPRIMARY_KEY = value; }
    }

    public string PRIMARY_KEY_ID
    {
        get { return myPRIMARY_KEY_ID; }
        set { myPRIMARY_KEY_ID = value; }
    }




    private bool CheckIntegrity()
    {
        bool isIntegry = false;
        
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        try
        {

            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();


                sqlQuery = "SELECT "+ myPRIMARY_KEY;
                sqlQuery += " FROM " + mySQL_TABLE;
                sqlQuery += " WHERE ZeusIdModulo = '" + Server.HtmlEncode(myZeusIdModulo) + "' ";
                sqlQuery += " AND ZeusLangCode='" + Server.HtmlEncode(myZeusLangCode) + "'";
                sqlQuery += " AND " + myPRIMARY_KEY + "='" + Server.HtmlEncode(myPRIMARY_KEY_ID) + "'";

                command.CommandText = sqlQuery;

                //Response.Write("DEB "+sqlQuery +"<br />");
                SqlDataReader reader = command.ExecuteReader();

                isIntegry = reader.HasRows;
                reader.Close();

               
            }//fine Using
        }
        catch (Exception p)
        {

            Response.Write(p.ToString());
            
        }

        return isIntegry;
        
    }//fine CheckIntegrity
    
    
    
    
    protected void Page_Load(object sender, EventArgs e)
    {
        BindTheData();
    }


    public void BindTheData()
    {

        delinea myDelinea = new delinea();

        if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
           || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI"))
           || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM")))
            Response.Redirect("~/Zeus/System/Message.aspx?QueryStringIntegrity");


          myZeusIdModulo = Server.HtmlEncode(Request.QueryString["ZIM"]);
          myZeusLangCode = Server.HtmlEncode(Request.QueryString["Lang"]);
          myPRIMARY_KEY_ID = Server.HtmlEncode(Request.QueryString["XRI"]);
        
            if (!CheckIntegrity())
                Response.Redirect("~/Zeus/System/Message.aspx?QueryStringIntegrity");
    
    }//fineBindTheData
    
</script>
