<%@ Control Language="C#" ClassName="dlFileUpload" %>
<%@ Import Namespace="System.IO" %>
<%@ Register Assembly="Flasher" Namespace="ControlFreak" TagPrefix="cc1" %>

<script runat="server">


    private bool pGotFile;
    private string pFileExt;
    private string pFileName;
    private HttpPostedFile pFilePost;
    private bool pRequired;
    private string pVgroup;

    private string[] pFileTypeRange;
    private bool Ind = false;

    private int minWidth = 0;
    private int minHeight = 0;
    private int maxWidth = 0;
    private int maxHeight = 0;
    private int minSize = 0;
    private int maxSize = 0;


    private string sizeError = string.Empty;
    private string phDimensionError = string.Empty;
    private string formatError = string.Empty;
    private string requiredError = string.Empty;
    private string savePath = string.Empty;
    private bool showInfo = true;

    private string defaultImage = string.Empty;
    private string defaultFlash = string.Empty;
    private string infoMessage1 = string.Empty;
    private string infoMessage2 = string.Empty;
    private string infoMessage3 = string.Empty;

    private bool enabled = true;
    private bool notShowImageOnly = true;

    static string TempPath = "/ZeusInc/Temp/";


    public bool NotShowImageOnly
    {
        set { notShowImageOnly = value; }
        get { return notShowImageOnly; }
    }


    public bool Enabled
    {
        set { enabled = value; }
    }

    public string InfoMessage1
    {
        set { infoMessage1 = value; }
        get { return infoMessage1; }
    }

    public string InfoMessage2
    {
        set { infoMessage2 = value; }
        get { return infoMessage2; }
    }

    public string InfoMessage3
    {
        set { infoMessage3 = value; }
        get { return infoMessage3; }
    }


    public string DefaultImage
    {
        set { defaultImage = value; }
        get { return defaultImage; }
    }


    public string DefaultFlash
    {
        set { defaultFlash = value; }
        get { return defaultFlash; }
    }


    //Get function
    public bool GotFile
    {
        get { return pGotFile; }
    }
    public string FileExt
    {
        get { return pFileExt; }
    }
    public string FileName
    {
        get { return pFileName; }
        set { pFileName = value; }
    }
    public HttpPostedFile FilePost
    {
        get { return pFilePost; }
    }

    public string SavePath
    {
        set { savePath = value; }
        get { return savePath; }
    }


    public bool ShowInfo
    {
        set { showInfo = value; }
        get { return showInfo; }
    }


    //Set function

    public bool Required
    {
        set { pRequired = value; }
    }
    public string Vgroup
    {
        set { pVgroup = value; }
    }
    public string FileTypeRange
    {
        set { pFileTypeRange = value.ToString().Split(','); }
    }
    public int MinWidthPx
    {
        set { minWidth = value; }
    }

    public int MinHeightPx
    {
        set { minHeight = value; }
    }

    public int MaxWidthPx
    {
        set { maxWidth = value; }
    }

    public int MaxHeightPx
    {
        set { maxHeight = value; }
    }

    public int MaxSizeKb
    {
        set { maxSize = value; }
    }

    public int MinSizeKb
    {
        set { minSize = value; }
    }

    public string SizeErrorMessage
    {
        set { sizeError = value; }
    }

    public string PhDimensionErrorMessage
    {
        set { phDimensionError = value; }
    }

    public string FormatErrorMessage
    {
        set { formatError = value; }
    }

    public string RequiredErrorMessage
    {
        set { requiredError = value; }
    }







    //              Page_Load
    protected void Page_Init()
    {
        //if ((!Page.IsPostBack)&&(!Page.IsCallback))
        //{
        try
        {
            //LoadButton.ValidationGroup = pVgroup;
            ErrorMsg.ValidationGroup = pVgroup;
            FileNameTriggedLabel.Text = string.Empty;

            if (defaultImage.Length > 0)
                UploadImage.ImageUrl = defaultImage;


            MessagePanel.Visible = showInfo;
            Message1Label.Text = "Formati consentiti: ";
            if (pFileTypeRange != null)
                foreach (string str in pFileTypeRange)
                    Message1Label.Text += str + ", ";

            Message1Label.Text = Message1Label.Text.Substring(0, Message1Label.Text.Length - 2);
            Message2Label.Text = "Dimensione: " + maxWidth + "x" + maxHeight + " pixel - 72dpi";
            Message3Label.Text = "Peso massimo: " + maxSize + "Kb";
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
        //}

    }//fine Page_Init


    protected void Page_Load(object sender, EventArgs e)
    {
        CambiaImmagineLabel.Visible = FilUpl.Visible = LoadButton.Visible = ClearButton.Visible = Message1Label.Visible = Message2Label.Visible = Message3Label.Visible = notShowImageOnly;
    }


    public bool isValid()
    {
        return Page.IsValid;

    }//fine isValid


    public void SetDefaultImage()
    {

        try
        {
            if (defaultImage.Length > 0)
            {
                if (defaultImage.Substring(defaultImage.Length - 3).ToLower().Equals("swf"))
                {
                    UploadFlasher.Visible = true;
                    UploadImage.Visible = false;
                    UploadFlasher.FlashFile = defaultFlash;
                }
                else
                {

                    UploadFlasher.Visible = false;
                    UploadImage.Visible = true;
                    UploadImage.ImageUrl = defaultImage;
                }
            }//fine if
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine SetDefaultImage


    public void SetInfoMessage()
    {
        Message1Label.Text = infoMessage1;
        Message2Label.Text = infoMessage2;
        Message3Label.Text = infoMessage3;

    }//fine SetInfoMessage


    public void SetEnabled()
    {
        FilUpl.Enabled = enabled;
        LoadButton.Enabled = enabled;
        ClearButton.Enabled = enabled;

    }//fine SetOnKeyDown


    public void ClearAll()
    {
        try
        {
            pFileTypeRange = null;
            minWidth = 0;
            minHeight = 0;
            maxWidth = 0;
            maxHeight = 0;
            maxSize = 0;
            minSize = 0;
            pGotFile = false;
            pFileName = string.Empty;
            UploadFlasher.Visible = false;
            UploadImage.Visible = true;

            if (FileNameTriggedLabel.Text.Length > 0)
                File.Delete(Server.MapPath(TempPath + FileNameTriggedLabel.Text));

            FileNameTriggedLabel.Text = "ImgNonDisponibile.jpg";
            UploadImage.ImageUrl = savePath + FileNameTriggedLabel.Text;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine ClearAll

    public void SetInfoMessageAuto()
    {
        Message1Label.Text = "Formati consentiti: ";
        if (pFileTypeRange != null)
            foreach (string str in pFileTypeRange)
                Message1Label.Text += str + ", ";

        Message1Label.Text.Remove(Message1Label.Text.Length - 1, 1);

        if (maxWidth != minWidth)
        {
            Message2Label.Text = " Larghezza: min " + minWidth.ToString();
            Message2Label.Text += " max " + maxWidth.ToString();
        }
        else Message2Label.Text = "Larghezza: " + maxWidth.ToString();
        Message2Label.Text += " pixel - 72dpi ";

        if (maxHeight != minHeight)
        {
            Message2Label.Text += " <br /> Altezza: min " + minHeight.ToString();
            Message2Label.Text += " max " + maxHeight.ToString();
        }
        else Message2Label.Text += " <br /> Altezza: " + maxHeight.ToString();

        Message2Label.Text += " pixel - 72dpi";
        Message3Label.Text = "Peso massimo: " + maxSize.ToString() + " Kb";

    }//fine SetInfoMessageAuto



    public void myDataBind()
    {
        try
        {
            //LoadButton.ValidationGroup = pVgroup;
            ErrorMsg.ValidationGroup = pVgroup;

            MessagePanel.Visible = showInfo;
            Message1Label.Text = "Formati consentiti: ";

            if (pFileTypeRange != null)
                foreach (string str in pFileTypeRange)
                    Message1Label.Text += str + ", ";

            Message1Label.Text = Message1Label.Text.Substring(0, Message1Label.Text.Length - 2);
            Message2Label.Text = "Dimensione: " + maxWidth + "x" + maxHeight + " pixel - 72dpi";
            Message3Label.Text = "Peso massimo: " + maxSize + "Kb";
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine DataBind


    private string CheckFileName(string filePath, string fileName)
    {

        int counter = 1;
        string finalPath = filePath;
        string strFile_name = System.IO.Path.GetFileNameWithoutExtension(fileName);
        string strFile_extension = System.IO.Path.GetExtension(fileName);

        if (System.IO.File.Exists(filePath + fileName))
        {

            // il file � gi� sul server
            filePath += fileName;
            while (System.IO.File.Exists(filePath))
            {
                counter++;
                filePath = System.IO.Path.Combine(finalPath, strFile_name + counter.ToString() + strFile_extension);

            }//fine while


            fileName = strFile_name + counter.ToString() + strFile_extension;


        }
        //else fileName =strFile_name;

        return fileName;

    }//fine CheckFileName


    public bool SaveFile()
    {
        try
        {

            string filePath = System.Web.HttpContext.Current.Server.MapPath(this.savePath);


            if (FilUpl.HasFile)
            {
                pFileName = CheckFileName(filePath, FilUpl.FileName);
                FilUpl.SaveAs(filePath + pFileName);
                return true;
            }
            else if ((FileNameTriggedLabel.Text.Length > 0) && (FileNameTriggedLabel.Text.IndexOf("ImgNonDisponibile") < 0))
            {
                pFileName = CheckFileName(filePath, FileNameTriggedLabel.Text);
                File.Copy(Server.MapPath(TempPath + FileNameTriggedLabel.Text), filePath + pFileName);
                File.Delete(Server.MapPath(TempPath + FileNameTriggedLabel.Text));
                return true;
            }



            return false;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }//fine SaveFile


    public bool SaveFileThumb(string ThumbPath)
    {
        try
        {
            string filePath = System.Web.HttpContext.Current.Server.MapPath(this.savePath);

            if (FilUpl.HasFile)
            {
                pFileName = CheckFileName(filePath, FilUpl.FileName);
                FilUpl.SaveAs(filePath + pFileName);

                if (saveThumbImage(pFileName, this.savePath, ThumbPath))
                    return true;

            }
            else if ((FileNameTriggedLabel.Text.Length > 0) && (FileNameTriggedLabel.Text.IndexOf("ImgNonDisponibile") < 0))
            {
                pFileName = CheckFileName(filePath, FileNameTriggedLabel.Text);
                File.Copy(Server.MapPath(TempPath + FileNameTriggedLabel.Text), filePath + pFileName);
                File.Delete(Server.MapPath(TempPath + FileNameTriggedLabel.Text));

                if (saveThumbImage(pFileName, this.savePath, ThumbPath))
                    return true;
            }
            //salviamo il file sul server

            return false;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }//fine SaveFile


    private bool ThumbnailCallback()
    {
        return false;
    }

    private static System.Drawing.Imaging.ImageFormat GetImageFormat(string Format)
    {

        switch (Format.ToLower())
        {
            case ".jpg":
                return System.Drawing.Imaging.ImageFormat.Jpeg;
                break;

            case ".jpeg":
                return System.Drawing.Imaging.ImageFormat.Jpeg;
                break;

            case ".gif":
                return System.Drawing.Imaging.ImageFormat.Gif;
                break;

            default:
                return System.Drawing.Imaging.ImageFormat.Jpeg;
                break;

        }//fine switch



    }// fine GetImageFormat

    private bool saveThumbImage(string name, string SrcPath, string FinalPath)
    {
        if (name.Length > 0)
        {
            int Height = 0;
            int Width = 0;

            try
            {
                System.Drawing.Image normalImage = System.Drawing.Image.FromFile(System.Web.HttpContext.Current.Server.MapPath(SrcPath + name));

                if (normalImage.Height > normalImage.Width)
                {
                    Height = 80;
                    Width = (80 * normalImage.Width) / normalImage.Height;
                }
                else
                {
                    Width = 80;
                    Height = (80 * normalImage.Height) / normalImage.Width;
                }



                System.Drawing.Image thumbImage = normalImage.GetThumbnailImage(Width, Height, ThumbnailCallback, IntPtr.Zero);
                thumbImage.Save(System.Web.HttpContext.Current.Server.MapPath(FinalPath + name), GetImageFormat(System.IO.Path.GetExtension(name)));

                normalImage.Dispose();
                thumbImage.Dispose();
                return true;
            }
            catch (Exception p)
            {
                System.Web.HttpContext.Current.Response.Write(p.ToString());
                return false;
            }
        }
        else return false;
    }//fine saveThumbImage




    protected void ErrorMsg_ServerValidate(object source, ServerValidateEventArgs args)
    {
        try
        {

            if (FilUpl.HasFile)
            {
                pGotFile = true;
                pFileExt = GetExtension(FilUpl.PostedFile.FileName);
                pFilePost = FilUpl.PostedFile;
                pFileName = FilUpl.FileName;

                if (pFileTypeRange != null)
                {
                    foreach (string str in pFileTypeRange)
                    {
                        if (str.ToLower().Equals(pFileExt.ToLower()))
                        {
                            Ind = true;
                            break;
                        }
                    }
                }
                else Ind = true;

                if (!Ind)
                {
                    args.IsValid = false;
                    if (formatError.Length == 0)
                        ErrorMsg.Text = "Formato del file non consentito";
                    else ErrorMsg.Text = formatError;
                    return;
                }

                System.Drawing.Image CheckSize = null;

                try
                {
                    CheckSize = System.Drawing.Image.FromStream(FilUpl.PostedFile.InputStream);
                }
                catch (Exception) { }

                if (CheckSize != null)
                {
                    if (minWidth > 0)
                    {

                        if (CheckSize.PhysicalDimension.Width < minWidth)
                        {
                            args.IsValid = false;
                            if (phDimensionError.Length == 0)
                                ErrorMsg.Text = "Dimensione immagine non corretta";
                            else ErrorMsg.Text = phDimensionError;
                            return;
                        }

                    }
                    if (minHeight > 0)
                    {

                        if (CheckSize.PhysicalDimension.Height < minHeight)
                        {
                            args.IsValid = false;
                            if (phDimensionError.Length == 0)
                                ErrorMsg.Text = "Dimensione immagine non corretta";
                            else ErrorMsg.Text = phDimensionError;
                            return;
                        }
                    }
                    if (maxWidth > 0)
                    {

                        if (CheckSize.PhysicalDimension.Width > maxWidth)
                        {
                            args.IsValid = false;
                            if (phDimensionError.Length == 0)
                                ErrorMsg.Text = "Dimensione immagine non corretta";
                            else ErrorMsg.Text = phDimensionError;
                            return;
                        }
                    }
                    if (maxHeight > 0)
                    {

                        if (CheckSize.PhysicalDimension.Height > maxHeight)
                        {
                            args.IsValid = false;
                            if (phDimensionError.Length == 0)
                                ErrorMsg.Text = "Dimensione immagine non corretta";
                            else ErrorMsg.Text = phDimensionError;
                            return;
                        }
                    }
                }//fine if CheckSize!=null
                if (minSize > 0)
                {

                    if (FilUpl.PostedFile.ContentLength < (minSize * 1024))
                    {
                        args.IsValid = false;
                        if (sizeError.Length == 0)
                            ErrorMsg.Text = "Il file non supera il peso minimo";
                        else ErrorMsg.Text = sizeError;
                        return;
                    }
                }
                if (maxSize > 0)
                {

                    if (FilUpl.PostedFile.ContentLength > (maxSize * 1024))
                    {
                        args.IsValid = false;
                        if (sizeError.Length == 0)
                            ErrorMsg.Text = "Il file supera il peso massimo";
                        else ErrorMsg.Text = sizeError;
                        return;
                    }
                }
            }
            else
            {

                pGotFile = false;
                if ((pRequired) && (FileNameTriggedLabel.Text.Length <= 0))
                {
                    args.IsValid = false;
                    if (requiredError.Length == 0)
                        ErrorMsg.Text = "Attenzione il file upload e' vuoto";
                    else ErrorMsg.Text = requiredError;
                }

            }//fine else
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }//fine ErrorMsg_ServerValidate



    protected void LoadButton_ServerValidate(object sender, ServerValidateEventArgs args)
    {
        try
        {

            if (FilUpl.HasFile)
            {
                pGotFile = true;
                pFileExt = GetExtension(FilUpl.PostedFile.FileName);
                pFilePost = FilUpl.PostedFile;
                pFileName = FilUpl.FileName;

                if (pFileTypeRange != null)
                {
                    foreach (string str in pFileTypeRange)
                    {
                        if (str.ToLower().Equals(pFileExt.ToLower()))
                        {
                            Ind = true;
                            break;
                        }
                    }
                }
                else Ind = true;

                if (!Ind)
                {
                    args.IsValid = false;
                    if (formatError.Length == 0)
                        LoadButtonCustomValidator.Text = "Formato del file non consentito";
                    else LoadButtonCustomValidator.Text = formatError;
                    return;
                }

                System.Drawing.Image CheckSize = null;

                try
                {
                    CheckSize = System.Drawing.Image.FromStream(FilUpl.PostedFile.InputStream);
                }
                catch (Exception) { }

                if (CheckSize != null)
                {
                    if (minWidth > 0)
                    {

                        if (CheckSize.PhysicalDimension.Width < minWidth)
                        {
                            args.IsValid = false;
                            if (phDimensionError.Length == 0)
                                LoadButtonCustomValidator.Text = "Dimensione immagine non corretta";
                            else LoadButtonCustomValidator.Text = phDimensionError;
                            return;
                        }

                    }
                    if (minHeight > 0)
                    {

                        if (CheckSize.PhysicalDimension.Height < minHeight)
                        {
                            args.IsValid = false;
                            if (phDimensionError.Length == 0)
                                LoadButtonCustomValidator.Text = "Dimensione immagine non corretta";
                            else LoadButtonCustomValidator.Text = phDimensionError;
                            return;
                        }
                    }
                    if (maxWidth > 0)
                    {

                        if (CheckSize.PhysicalDimension.Width > maxWidth)
                        {
                            args.IsValid = false;
                            if (phDimensionError.Length == 0)
                                LoadButtonCustomValidator.Text = "Dimensione immagine non corretta";
                            else LoadButtonCustomValidator.Text = phDimensionError;
                            return;
                        }
                    }
                    if (maxHeight > 0)
                    {

                        if (CheckSize.PhysicalDimension.Height > maxHeight)
                        {
                            args.IsValid = false;
                            if (phDimensionError.Length == 0)
                                LoadButtonCustomValidator.Text = "Dimensione immagine non corretta";
                            else LoadButtonCustomValidator.Text = phDimensionError;
                            return;
                        }
                    }
                }//fine if CheckSize!=null
                if (minSize > 0)
                {

                    if (FilUpl.PostedFile.ContentLength < (minSize * 1024))
                    {
                        args.IsValid = false;
                        if (sizeError.Length == 0)
                            LoadButtonCustomValidator.Text = "Il file non supera il peso minimo";
                        else LoadButtonCustomValidator.Text = sizeError;
                        return;
                    }
                }
                if (maxSize > 0)
                {

                    if (FilUpl.PostedFile.ContentLength > (maxSize * 1024))
                    {
                        args.IsValid = false;
                        if (sizeError.Length == 0)
                            LoadButtonCustomValidator.Text = "Il file supera il peso massimo";
                        else LoadButtonCustomValidator.Text = sizeError;
                        return;
                    }
                }
            }
            else
            {

                pGotFile = false;
                if ((pRequired) && (FileNameTriggedLabel.Text.Length <= 0))
                {
                    args.IsValid = false;
                    if (requiredError.Length == 0)
                        LoadButtonCustomValidator.Text = "Attenzione il file upload e' vuoto";
                    else LoadButtonCustomValidator.Text = requiredError;
                }

            }//fine else
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }//fine LoadButton_ServerValidate


    private string GetExtension(string FileName)
    {
        string[] split = FileName.Split('.');
        string Extension = split[split.Length - 1];
        return Extension;

    }//fine GetExtension



    protected void LoadButton_Click(object sender, EventArgs e)
    {
        try
        {

            if ((Page.IsValid) && (FilUpl.HasFile))
            {

                FileNameTriggedLabel.Text = FilUpl.FileName;

                pGotFile = true;
                FilUpl.SaveAs(Server.MapPath(TempPath) + FilUpl.FileName);
                pFileName = FilUpl.FileName;
                if (GetExtension(FilUpl.FileName).ToLower().Equals("swf"))
                {
                    UploadFlasher.Visible = true;
                    UploadImage.Visible = false;
                    UploadFlasher.FlashFile = TempPath + FilUpl.FileName;

                }
                else
                {

                    UploadFlasher.Visible = false;
                    UploadImage.Visible = true;
                    UploadImage.ImageUrl = TempPath + FilUpl.FileName;
                    System.Drawing.Image CheckSize = System.Drawing.Image.FromStream(FilUpl.PostedFile.InputStream);
                    InfoLabel.Text = CheckSize.PhysicalDimension.Width + "x" + CheckSize.PhysicalDimension.Height + " / " + FilUpl.PostedFile.ContentLength + " byte";
                    InfoLabel.Visible = true;
                }

            }//fine if
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine LoadButton_Click


    protected void ClearButton_Click(object sender, EventArgs e)
    {
        //SetDefaultImage();
        try
        {

            UploadFlasher.Visible = false;
            UploadImage.Visible = true;

            if (FileNameTriggedLabel.Text.Length > 0)
                File.Delete(Server.MapPath(TempPath + FileNameTriggedLabel.Text));

            FileNameTriggedLabel.Text = "ImgNonDisponibile.jpg";
            UploadImage.ImageUrl = savePath + FileNameTriggedLabel.Text;
            pGotFile = false;
            pFileName = string.Empty;
            InfoLabel.Visible = false;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine ClearButton_Click


    public string GetTrigged()
    {
        return FileNameTriggedLabel.Text;
    }
    
</script>

<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="conditional">
    <Triggers>
        <asp:PostBackTrigger ControlID="LoadButton" />
        <asp:PostBackTrigger ControlID="ClearButton" />
    </Triggers>
    <ContentTemplate>
        <asp:Label ID="FileNameTriggedLabel" runat="server" Visible="false"></asp:Label>
        <table border="0" cellspacing="0" cellpadding="2">
            <tr>
                <td>
                    <cc1:Flasher ID="UploadFlasher" runat="server" Visible="false">
                    </cc1:Flasher>
                    <asp:Image ID="UploadImage" runat="server" /><br />
                    <asp:Label ID="InfoLabel" runat="server" Visible="false" SkinID="FileUploadNotes"></asp:Label>
                </td>
                <td>
                    <asp:Panel ID="MessagePanel" runat="server">
                        <asp:Label ID="Message1Label" runat="server" SkinID="FileUploadNotes"></asp:Label><br />
                        <asp:Label ID="Message2Label" runat="server" SkinID="FileUploadNotes"></asp:Label><br />
                        <asp:Label ID="Message3Label" runat="server" SkinID="FileUploadNotes"></asp:Label><br />
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="CambiaImmagineLabel" SkinID="FileUploadNotes" runat="server" Text="Cambia immagine"></asp:Label>
                    <br />
                    <asp:FileUpload ID="FilUpl" runat="server" />
                    <asp:LinkButton ID="LoadButton" runat="server" Text="Carica" OnClick="LoadButton_Click"
                        ValidationGroup="myLoadButtonValidationGroup" SkinID="ZSSM_Button01" />
                    <asp:LinkButton ID="ClearButton" runat="server" Text="Rimuovi" OnClick="ClearButton_Click"
                        SkinID="ZSSM_Button01" />
                    <asp:CustomValidator ID="ErrorMsg" runat="server" ErrorMessage="CustomValidator"
                        OnServerValidate="ErrorMsg_ServerValidate" SkinID="ZSSM_Validazione01" Display="Dynamic"></asp:CustomValidator>
                    <asp:CustomValidator ID="LoadButtonCustomValidator" runat="server" ErrorMessage="CustomValidator"
                        OnServerValidate="LoadButton_ServerValidate" SkinID="ZSSM_Validazione01" Display="Dynamic"
                        ValidationGroup="myLoadButtonValidationGroup"></asp:CustomValidator>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
