﻿<%@ Control Language="C#" ClassName="CategorieMultiple" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">

    
    private string myCOLUMN_ID = string.Empty;
    private string myTABLE = string.Empty;
    private string myCOLUMN_IDCAT = string.Empty;
    private string myCOL_SPLIT = "1";
    private string myPAGE_ID = string.Empty;
    private string myMODE = string.Empty;
    private string myZeusIdModulo = string.Empty;
    private string myZeusLangCode = "ITA";



    public string COLUMN_ID
    {
        get { return myCOLUMN_ID; }
        set { myCOLUMN_ID = value; }
    }

    public string TABLE
    {
        get { return myTABLE; }
        set { myTABLE = value; }
    }


    public string COLUMN_IDCAT
    {
        get { return myCOLUMN_IDCAT; }
        set { myCOLUMN_IDCAT = value; }
    }


    public string RepeatColumns
    {

        get { return myCOL_SPLIT; }
        set { myCOL_SPLIT = value; }
    }

    public string PAGE_ID
    {
        get { return myPAGE_ID; }
        set { myPAGE_ID = value; }
    }

    public string MODE
    {
        get { return myMODE; }
        set { myMODE = value; }
    }

    public string ZeusIdModulo
    {
        get { return myZeusIdModulo; }
        set { myZeusIdModulo = value; }
    }

    public string ZeusLangCode
    {
        get { return myZeusLangCode; }
        set { myZeusLangCode = value; }
    }





    //##############################################################################################################
    //################################################ Categorie ###################################################
    //############################################################################################################## 
    public bool GetCategorie()
    {

        if (myZeusIdModulo.Length <= 0)
            return false;

        if (myZeusLangCode.Length <= 0)
            return false;


        if (myMODE.ToUpper().Equals("EDT"))
        {

            if (myCOLUMN_IDCAT.Length <= 0)
                return false;

            if (myTABLE.Length <= 0)
                return false;

            if (myCOLUMN_ID.Length <= 0)
                return false;

            if (myPAGE_ID.Length <= 0)
                return false;
        }//fine if




        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;


        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {


            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();




                sqlQuery += "SELECT [ID1Categoria],[CatLiv2] FROM [vwCategorie_All] ";
                sqlQuery += " WHERE [Livello] = 2 AND ZeusIdModulo = '" + Server.HtmlEncode(myZeusIdModulo) + "'";
                sqlQuery += " AND ZeusLangCode='" + Server.HtmlEncode(myZeusLangCode) + "'";
                sqlQuery += " AND PW_Zeus1=1";
                sqlQuery += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";

                //Response.Write("DEB "+sqlQuery+"<br />");
                command.CommandText = sqlQuery;


                SqlDataReader reader = command.ExecuteReader();


                if (!reader.HasRows)
                {
                    reader.Close();
                    return false;
                }


                ArrayList myArray = new ArrayList();

                while (reader.Read())
                {
                    string[] myData = new string[2];


                    if (reader["CatLiv2"] != DBNull.Value)
                        myData[0] = reader["CatLiv2"].ToString();
                    else myData[0] = string.Empty;

                    if (reader["ID1Categoria"] != DBNull.Value)
                        myData[1] = Convert.ToInt32(reader["ID1Categoria"]).ToString();

                    myArray.Add(myData);

                }//fine while

                reader.Close();


                string[][] myAllData = (string[][])myArray.ToArray(typeof(string[]));
                CheckBox myCheckBox = null;
                int column_index = 0;


                myArray = new ArrayList();

                if (MODE.ToUpper().Equals("EDT"))
                {

                    sqlQuery = "SELECT " + myCOLUMN_IDCAT;
                    sqlQuery += " FROM " + myTABLE;
                    sqlQuery += " WHERE " + myCOLUMN_ID + "=" + myPAGE_ID;

                    //Response.Write("DEB "+sqlQuery+"<br />");

                    command.CommandText = sqlQuery;

                    reader = command.ExecuteReader();



                    while (reader.Read())
                        if (reader[myCOLUMN_IDCAT] != DBNull.Value)
                            myArray.Add(reader[myCOLUMN_IDCAT].ToString());

                    reader.Close();

                }//fine if

                string[] SelectedElement = (string[])myArray.ToArray(typeof(string));
                bool find = false;
                int index = 0;


                this.Controls.Add(new LiteralControl("<table width=\"100%\" border=\"0\" cellspacing=\"1\" class=\"NSTD1_NestedTable1\">"));
                for (int i = 0; i < myAllData.Length; ++i)
                {


                    

                    sqlQuery = "SELECT ID1Categoria,CatLiv3";
                    sqlQuery += " FROM vwCategorie_All ";
                    sqlQuery += " WHERE ZeusIdModulo = '" + Server.HtmlEncode(ZeusIdModulo) + "' ";
                    sqlQuery += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    sqlQuery += " AND ID2CategoriaParent = " + myAllData[i][1];
                    sqlQuery += " AND PW_Zeus1=1";
                    sqlQuery += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";

                    //Response.Write("DEB "+sqlQuery+"<br />");

                    command.CommandText = sqlQuery;

                    reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                       
                        this.Controls.Add(new LiteralControl("<tr>"));
                        this.Controls.Add(new LiteralControl("<td colspan=\"" + myCOL_SPLIT + "\" class=\"tdParent\">"));
                        this.Controls.Add(new LiteralControl(myAllData[i][0]));
                        this.Controls.Add(new LiteralControl("</td>"));
                        this.Controls.Add(new LiteralControl("</tr>"));

                        while (reader.Read())
                            if ((reader["ID1Categoria"] != DBNull.Value)
                                && (reader["CatLiv3"] != DBNull.Value))
                            {

                                myCheckBox = new CheckBox();
                                myCheckBox.ID = "CheckBox_ID" + reader["ID1Categoria"].ToString();


                                if (MODE.ToUpper().Equals("EDT"))
                                {
                                    for (int m = 0; m < SelectedElement.Length; ++m)
                                        if (reader["ID1Categoria"].ToString().Equals(SelectedElement[m]))
                                        {
                                            myCheckBox.Checked = true;

                                            find = true;

                                            break;
                                        }



                                    if (column_index == 0)
                                        this.Controls.Add(new LiteralControl("<tr>"));

                                    if (!find)
                                        this.Controls.Add(new LiteralControl("<td>"));
                                    else this.Controls.Add(new LiteralControl("<td class=\"tdElementTrue\">"));

                                    this.Controls.Add(myCheckBox);

                                    find = false;

                                    this.Controls.Add(new LiteralControl("<img src=\"/Zeus/SiteImg/Spc.gif\"  width=\"5\" height=\"15\"/>"));
                                    this.Controls.Add(new LiteralControl(reader["CatLiv3"].ToString()));

                                    this.Controls.Add(new LiteralControl("</td>"));

                                    ++column_index;


                                    if (column_index == Convert.ToInt32(myCOL_SPLIT))
                                    {
                                        this.Controls.Add(new LiteralControl("</tr>"));
                                        column_index = 0;
                                    }
                                    ++index;


                                }//fine if
                                else
                                {


                                    if (column_index == 0)
                                        this.Controls.Add(new LiteralControl("<tr>"));

                                    this.Controls.Add(new LiteralControl("<td>"));


                                    this.Controls.Add(myCheckBox);

                                    this.Controls.Add(new LiteralControl("<img src=\"/Zeus/SiteImg/Spc.gif\"  width=\"5\" height=\"15\"/>"));
                                    this.Controls.Add(new LiteralControl(reader["CatLiv3"].ToString()));

                                    this.Controls.Add(new LiteralControl("</td>"));

                                    ++column_index;

                                    if (column_index == Convert.ToInt32(myCOL_SPLIT))
                                    {
                                        this.Controls.Add(new LiteralControl("</tr>"));
                                        column_index = 0;
                                    }

                                }//fine else

                            }//fine if

                        if ((column_index < Convert.ToInt32(myCOL_SPLIT))
                           && (column_index > 0))
                        {
                            while (column_index < Convert.ToInt32(myCOL_SPLIT))
                            {
                                this.Controls.Add(new LiteralControl("<td>"));
                                this.Controls.Add(new LiteralControl("</td>"));
                                ++column_index;
                            }
                            this.Controls.Add(new LiteralControl("</tr>"));
                            column_index = 0;
                        }


                        if ((i + 1) < myAllData.Length)
                        {
                            this.Controls.Add(new LiteralControl("<tr>"));
                            this.Controls.Add(new LiteralControl("<td colspan=\"" + myCOL_SPLIT + "\" class=\"tdSeparator\">"));
                            this.Controls.Add(new LiteralControl("<img src=\"/Zeus/SiteImg/Spc.gif\" width=\"10\" height=\"1\" />"));
                            this.Controls.Add(new LiteralControl("</td>"));
                            this.Controls.Add(new LiteralControl("</tr>"));
                        }
                        
                    }//fine if
                   
                    reader.Close();

                    


                }//fine for

                this.Controls.Add(new LiteralControl("</table>"));




                return true;
            }
            catch (Exception p)
            {

                Response.Write(p.ToString());

            }
        }//fine using



        return false;
    }//fine GetCategorie







    public bool SetCategorie()
    {


        if (myCOLUMN_IDCAT.Length <= 0)
            return false;

        if (myTABLE.Length <= 0)
            return false;

        if (myCOLUMN_ID.Length <= 0)
            return false;

        if (myPAGE_ID.Length <= 0)
            return false;



        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;



        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            SqlTransaction transaction = null;

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                transaction = connection.BeginTransaction();
                command.Transaction = transaction;


                if (MODE.ToUpper().Equals("EDT"))
                {


                    sqlQuery = " DELETE FROM " + myTABLE;
                    sqlQuery += " WHERE " + myCOLUMN_ID + "=" + myPAGE_ID;
                    command.CommandText = sqlQuery;
                    //Response.Write("DEB " + sqlQuery + " <br />");
                    command.ExecuteNonQuery();
                }//fine if


                sqlQuery = "SELECT ID1Categoria";
                sqlQuery += " FROM vwCategorie_All ";
                sqlQuery += " WHERE ZeusIdModulo = '" + Server.HtmlEncode(ZeusIdModulo) + "' ";
                sqlQuery += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                sqlQuery += " AND Livello=3 ";
                sqlQuery += " AND PW_Zeus1=1";
                sqlQuery += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";

                //Response.Write("DEB "+sqlQuery+"<br />");

                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();

                string ID = string.Empty;
                delinea myDelinea = new delinea();
                CheckBox myCheckBox = null;
                ArrayList myArray = new ArrayList();

                while (reader.Read())
                    if (reader["ID1Categoria"] != DBNull.Value)
                    {
                        ID = "CheckBox_ID" + reader["ID1Categoria"].ToString();
                        myCheckBox = (CheckBox)myDelinea.FindControlRecursive(this, ID);

                        if ((myCheckBox != null)
                            && (myCheckBox.Checked))
                        {
                            myArray.Add(myCheckBox.ID.Replace("CheckBox_ID", string.Empty));
                            Response.Write("DEB find: " + ID);
                        }

                    } //fine if

                reader.Close();


                string[] ID2Element = (string[])myArray.ToArray(typeof(string));

                for (int i = 0; i < ID2Element.Length; ++i)
                {
                    sqlQuery = " INSERT INTO " + myTABLE;
                    sqlQuery += " (" + myCOLUMN_ID + "," + myCOLUMN_IDCAT + ")";
                    sqlQuery += "  VALUES ";
                    sqlQuery += " (" + myPAGE_ID + "," + ID2Element[i] + ")";

                    //Response.Write("DEB " + sqlQuery + " <br />");
                    command.CommandText = sqlQuery;
                    command.ExecuteNonQuery();


                }//fine for


                transaction.Commit();

                return true;
            }
            catch (Exception p)
            {

                Response.Write(p.ToString());

                if (transaction != null)
                    transaction.Rollback();

                return false;
            }
        }//fine using

    }//fine SetCategorie



    public bool PrintCategorie()
    {


        if (myZeusIdModulo.Length <= 0)
            return false;

        if (myZeusLangCode.Length <= 0)
            return false;



        if (myCOLUMN_IDCAT.Length <= 0)
            return false;

        if (myTABLE.Length <= 0)
            return false;

        if (myCOLUMN_ID.Length <= 0)
            return false;

        if (myPAGE_ID.Length <= 0)
            return false;





        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;


        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {


            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();



                sqlQuery += "SELECT [ID1Categoria],[CatLiv2] FROM [vwCategorie_All] ";
                sqlQuery += " WHERE [Livello] = 2 AND ZeusIdModulo = '" + Server.HtmlEncode(myZeusIdModulo) + "'";
                sqlQuery += " AND ZeusLangCode='" + Server.HtmlEncode(myZeusLangCode) + "'";
                sqlQuery += " AND PW_Zeus1=1";
                sqlQuery += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";

                //Response.Write("DEB "+sqlQuery+"<br />");
                command.CommandText = sqlQuery;


                SqlDataReader reader = command.ExecuteReader();


                if (!reader.HasRows)
                {
                    reader.Close();
                    return false;
                }


                ArrayList myArray = new ArrayList();

                while (reader.Read())
                {
                    string[] myData = new string[2];


                    if (reader["CatLiv2"] != DBNull.Value)
                        myData[0] = reader["CatLiv2"].ToString();
                    else myData[0] = string.Empty;

                    if (reader["ID1Categoria"] != DBNull.Value)
                        myData[1] = Convert.ToInt32(reader["ID1Categoria"]).ToString();

                    myArray.Add(myData);

                }//fine while

                reader.Close();


                string[][] myAllData = (string[][])myArray.ToArray(typeof(string[]));

                Image myImage = null;
                Label myLabel = null;
                int index = 0;
                int column_index = 0;

                this.Controls.Add(new LiteralControl("<table width=\"100%\" border=\"0\" cellspacing=\"1\" class=\"NSTD1_NestedTable1\">"));

                for (int i = 0; i < myAllData.Length; ++i)
                {


                    

                    sqlQuery = "SELECT ID1Categoria,CatLiv3";
                    sqlQuery += " FROM vwCategorie_All ";
                    sqlQuery += " WHERE ZeusIdModulo = '" + Server.HtmlEncode(ZeusIdModulo) + "' ";
                    sqlQuery += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    sqlQuery += " AND ID2CategoriaParent = " + myAllData[i][1];
                    sqlQuery += " AND PW_Zeus1=1";
                    sqlQuery += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";

                    //Response.Write("DEB "+sqlQuery+"<br />");

                    command.CommandText = sqlQuery;

                    reader = command.ExecuteReader();


                    if (reader.HasRows)
                    {




                        this.Controls.Add(new LiteralControl("<tr>"));
                        this.Controls.Add(new LiteralControl("<td colspan=\"" + myCOL_SPLIT + "\" class=\"tdParent\">"));
                        this.Controls.Add(new LiteralControl(myAllData[i][0]));
                        this.Controls.Add(new LiteralControl("</td>"));
                        this.Controls.Add(new LiteralControl("</tr>"));

                        // this.Controls.Add(new LiteralControl("<table border=\"0\"  cellpadding=\"0\" cellspacing=\"5\">"));

                        while (reader.Read())
                            if ((reader["ID1Categoria"] != DBNull.Value)
                                && (!reader.IsDBNull(1)))
                            {

                                if (column_index == 0)
                                    this.Controls.Add(new LiteralControl("<tr>"));

                                this.Controls.Add(new LiteralControl("<td>"));

                                myImage = new Image();
                                myImage.ID = "PRIVATEDELINEAImage_ID" + reader["ID1Categoria"].ToString();
                                myImage.ImageUrl = "~/Zeus/SiteImg/Ico1_StatusKo.gif";
                                this.Controls.Add(myImage);

                                this.Controls.Add(new LiteralControl("<img src=\"/Zeus/SiteImg/Spc.gif\"  width=\"10\"/>"));
                                myLabel = new Label();
                                myLabel.Text = reader.GetString(1);
                                myLabel.SkinID = "FieldValue";
                                this.Controls.Add(myLabel);

                                Controls.Add(new LiteralControl("</td>"));

                                ++column_index;

                                if (column_index == Convert.ToInt32(myCOL_SPLIT))
                                {
                                    this.Controls.Add(new LiteralControl("</tr>"));
                                    column_index = 0;
                                }


                                ++index;
                            }//fine if


                        if ((column_index < Convert.ToInt32(myCOL_SPLIT))
                                 && (column_index > 0))
                        {
                            while (column_index < Convert.ToInt32(myCOL_SPLIT))
                            {
                                this.Controls.Add(new LiteralControl("<td>"));
                                this.Controls.Add(new LiteralControl("</td>"));
                                ++column_index;
                            }
                            this.Controls.Add(new LiteralControl("</tr>"));
                            column_index = 0;
                        }



                    }//fine if

                    reader.Close();
                }//fine for

                this.Controls.Add(new LiteralControl("</table>"));


                sqlQuery = "SELECT " + myCOLUMN_IDCAT;
                sqlQuery += " FROM " + myTABLE;
                sqlQuery += " WHERE " + myCOLUMN_ID + " = " + myPAGE_ID;
                command.CommandText = sqlQuery;
                //Response.Write("DEB " + sqlQuery + " <br />");
                reader = command.ExecuteReader();

                if (!reader.HasRows)
                {
                    reader.Close();
                    return false;
                }

                string ID = string.Empty;
                delinea myDelinea = new delinea();




                while (reader.Read())
                    for (int i = 0; i < index; ++i)
                        if (reader[myCOLUMN_IDCAT] != DBNull.Value)
                        {
                            ID = "PRIVATEDELINEAImage_ID" + reader[myCOLUMN_IDCAT].ToString();
                            myImage = (Image)myDelinea.FindControlRecursive(this, ID);

                            if (myImage != null)
                                myImage.ImageUrl = "~/Zeus/SiteImg/Ico1_StatusOk.gif";

                        } //fine if



                reader.Close();


                return true;
            }
            catch (Exception p)
            {

                Response.Write(p.ToString());
                return false;
            }
        }//fine using

    }//fine PrintCategorie
    //##############################################################################################################
    //############################################# FINE Categorie ###################################################
    //############################################################################################################## 

    
    
</script>



