﻿<%@ Control Language="C#" ClassName="ProfiloMarketing_Edt" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">
    
    
    private string UserID = string.Empty;
    private string myRedirectPath = string.Empty;


    public string UID
    {
        set { UserID = value; }
        get { return UserID; }
    }

    public string RedirectPath
    {
        set { myRedirectPath = value; }
        get { return myRedirectPath; }
    }


    protected void Page_Load(object sender, EventArgs e)
    { 
        if(!Page.IsPostBack)
            BindTheData();

       // Response.Write("Hello world");

        
        
    }//fine PageLoad


    private string GetLang()
    {
        string Lang = "ITA";

        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;

    }//fine GetLang



    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("~/Zeus/Account/ZML_Profili.xml"));


            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {

                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                            if (myLabel != null)
                                if (childNode.InnerText!=string.Empty)
                                myLabel.Text = childNode.InnerText;

                        }
                        catch (Exception)
                        {

                        }
                    }//fine foreach
                }//fine foreach

            }//fine for
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine ReadXML_Localization



    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
           
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("~/Zeus/Account/ZMC_Profili.xml"));

            Panel ZMCF_Categoria1 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Categoria1");

            if (ZMCF_Categoria1 != null)
                ZMCF_Categoria1.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Categoria1").InnerText);

            Panel ZMCF_ConsensoDatiPersonali = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_ConsensoDatiPersonali");

            if (ZMCF_ConsensoDatiPersonali != null)
                ZMCF_ConsensoDatiPersonali.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_ConsensoDatiPersonali").InnerText);


            Panel ZMCF_ConsensoDatiPersonali2 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_ConsensoDatiPersonali2");

            if (ZMCF_ConsensoDatiPersonali2 != null)
                ZMCF_ConsensoDatiPersonali2.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_ConsensoDatiPersonali2").InnerText);


            Panel ZMCF_Comunicazioni1 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Comunicazioni1");

            if (ZMCF_Comunicazioni1 != null)
                ZMCF_Comunicazioni1.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni1").InnerText);


            Panel ZMCF_Comunicazioni2 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Comunicazioni2");

            if (ZMCF_Comunicazioni2 != null)
                ZMCF_Comunicazioni2.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni2").InnerText);


            Panel ZMCF_Comunicazioni3 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Comunicazioni3");

            if (ZMCF_Comunicazioni3 != null)
                ZMCF_Comunicazioni3.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni3").InnerText);

            Panel ZMCF_Comunicazioni4 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Comunicazioni4");

            if (ZMCF_Comunicazioni4 != null)
                ZMCF_Comunicazioni4.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni4").InnerText);

            Panel ZMCF_Comunicazioni5 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Comunicazioni5");

            if (ZMCF_Comunicazioni5 != null)
                ZMCF_Comunicazioni5.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni5").InnerText);


            Panel ZMCF_Comunicazioni6 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Comunicazioni6");

            if (ZMCF_Comunicazioni6 != null)
                ZMCF_Comunicazioni6.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni6").InnerText);


            Panel ZMCF_Comunicazioni7 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Comunicazioni7");

            if (ZMCF_Comunicazioni7 != null)
                ZMCF_Comunicazioni7.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni7").InnerText);



            Panel ZMCF_Comunicazioni8 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Comunicazioni8");

            if (ZMCF_Comunicazioni8 != null)
                ZMCF_Comunicazioni8.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni8").InnerText);



            Panel ZMCF_Comunicazioni9 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Comunicazioni9");

            if (ZMCF_Comunicazioni9 != null)
                ZMCF_Comunicazioni9.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni9").InnerText);


            Panel ZMCF_Comunicazioni10 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Comunicazioni10");

            if (ZMCF_Comunicazioni10 != null)
                ZMCF_Comunicazioni10.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni10").InnerText);


            SetQueryOfID2CategoriaDropDownList(mydoc.SelectSingleNode(myXPath + "ZMCD_ZIMCategoria1").InnerText
                          , GetLang(), mydoc.SelectSingleNode(myXPath + "ZMCD_Cat1Liv").InnerText);

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }

    }//fine ReadXML
    




  

    //##############################################################################################################
    //################################################ CATEGORIE ###################################################
    //############################################################################################################## 


    private bool SetQueryOfID2CategoriaDropDownList(string ZeusIdModulo,
       string ZeusLangCode, string PZV_Cat1Liv)
    {
        try
        {


            SqlDataSource dsCategoria = (SqlDataSource)ProfiloMarketingFormView.FindControl("dsCategoria");
            DropDownList ID2CategoriaDropDownList = (DropDownList)ProfiloMarketingFormView.FindControl("ID2CategoriaDropDownList");
            HiddenField ID2CategoriaHiddenField = (HiddenField)ProfiloMarketingFormView.FindControl("ID2CategoriaHiddenField");
            //ID2CategoriaHiddenField.Value = "0";

            switch (PZV_Cat1Liv)
            {

                case "123":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv1Liv2Liv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "Catliv1Liv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;


                case "23":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv2Liv3]";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;

                default:
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;
            }

            ID2CategoriaDropDownList.SelectedIndex = ID2CategoriaDropDownList.Items.IndexOf(ID2CategoriaDropDownList.Items.FindByValue(ID2CategoriaHiddenField.Value));


            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }//fine SetQueryOfID2CategoriaDropDownList


    protected void ID2CategoriaDropDownList_SelectIndexChange(object sender, EventArgs e)
    {
        HiddenField ID2CategoriaHiddenField = (HiddenField)ProfiloMarketingFormView.FindControl("ID2CategoriaHiddenField");
        DropDownList ID2CategoriaDropDownList = (DropDownList)sender;

        ID2CategoriaHiddenField.Value = ID2CategoriaDropDownList.SelectedValue;

    }//fine ID2CategoriaDropDownList_SelectIndexChange

    //##############################################################################################################
    //############################################ FINE CATEGORIE ##################################################
    //############################################################################################################## 


    private string GetCausalHumanReadble(string RegCasual)
    {

        if (RegCasual.Length == 0)
            return string.Empty;

        string CausalHumanReadble = string.Empty;

        try
        {

            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("~/App_Data/RegistrationCausal.xml"));
            CausalHumanReadble = mydoc.SelectSingleNode("RootCasual/RegDescription[@RegCausal='" + RegCasual + "']").InnerText;

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

        return CausalHumanReadble;

    }//fine GetCausalHumanReadble
    
    public bool BindTheData()
    {
        try
        {

            ProfiloMarketingSqlDataSource.SelectCommand += " WHERE ([UserID] = '" + UserID + "')";
            //ProfiloMarketingSqlDataSource.UpdateCommand += " WHERE ([UserID] = '" + UserID + "')";
            ProfiloMarketingFormView.DataSourceID = "ProfiloMarketingSqlDataSource";
            ProfiloMarketingFormView.DataBind();

            UIDHiddenField.Value = UserID;

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }


    }//fine BindTheData 


    protected void RecordEdtUser_DataBinding(object sender, EventArgs e)
    {
        HiddenField UtenteCreazione = (HiddenField)sender;
        MembershipUser user = Membership.GetUser();
        object userKey = user.ProviderUserKey;

        UtenteCreazione.Value = userKey.ToString();
    }

    protected void RecordEdtDate_DataBinding(object sender, EventArgs e)
    {
        HiddenField DataCreazione = (HiddenField)sender;
        DataCreazione.Value = DateTime.Now.ToString();

    }

    protected void ProfiloMarketingFormView_DataBound(object sender, EventArgs e)
    {
        ReadXML("PROFILI", GetLang());
        ReadXML_Localization("PROFILI", GetLang());
    }

    protected void ProfiloMarketingSqlDataSource_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.Exception == null)
            if (myRedirectPath.Length > 0)
                Response.Redirect(myRedirectPath);
            else Response.Redirect("~/Zeus/System/Message.aspx");
    }//fine ProfiloMarketingSqlDataSource_Updated

</script>


<asp:HiddenField ID="UIDHiddenField" runat="server" />
<asp:FormView ID="ProfiloMarketingFormView" DefaultMode="Edit" runat="server" Width="100%"
 OnDataBound="ProfiloMarketingFormView_DataBound">
    <EditItemTemplate>
        <div class="BlockBox">
            <div class="BlockBoxHeader">
                <asp:Label ID="Residenza_Label" runat="server" Text="Preferenze"></asp:Label></div>
            <table>
            
             <asp:Panel ID="ZMCF_Categoria1" runat="server">
                        <tr>
                            <td class="BlockBoxValue" colspan="2">
                                <asp:Label ID="ZML_Categoria1" SkinID="FieldDescription" runat="server" Text="Categoria"></asp:Label>
                                <asp:DropDownList ID="ID2CategoriaDropDownList" runat="server" OnSelectedIndexChanged="ID2CategoriaDropDownList_SelectIndexChange"
                                    AppendDataBoundItems="True">
                                      <asp:ListItem Text="> Non inserito" Value="0" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:HiddenField ID="ID2CategoriaHiddenField" runat="server" Value='<%# Bind("ID2Categoria1") %>' />
                                <asp:SqlDataSource ID="dsCategoria" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>">
                                </asp:SqlDataSource>
                            </td>
                        </tr>
                    </asp:Panel>
                        <asp:Panel ID="ZMCF_ConsensoDatiPersonali" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="CheckBoxDatiPersonali" runat="server" Checked='<%# Bind("ConsensoDatiPersonali") %>' />
                                    <asp:Label SkinID="FieldValue" ID="ZML_ConsensoDatiPersonali" runat="server" Text="Default_ZMCF_ConsensoDatiPersonali"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_ConsensoDatiPersonali2" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="CheckBoxDatiPersonali2" runat="server" Checked='<%# Bind("ConsensoDatiPersonali2") %>' />
                                    <asp:Label SkinID="FieldValue" ID="ZML_ConsensoDatiPersonali2" runat="server" Text="Default_ZMCF_ConsensoDatiPersonali2"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Comunicazioni1" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="Comunicazioni1CheckBox" runat="server" Checked='<%# Bind("Comunicazioni1") %>' />
                                    <asp:Label SkinID="FieldValue" ID="ZML_Comunicazioni1" runat="server" Text="Default_ZMCF_Comunicazioni1"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Comunicazioni2" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="Comunicazioni2CheckBox" runat="server" Checked='<%# Bind("Comunicazioni2") %>' />
                                    <asp:Label SkinID="FieldValue" ID="ZML_Comunicazioni2" runat="server" Text="Default_ZMCF_Comunicazioni2"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Comunicazioni3" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="Comunicazioni3CheckBox" runat="server"  Checked='<%# Bind("Comunicazioni3") %>'/>
                                    <asp:Label SkinID="FieldValue" ID="ZML_Comunicazioni3" runat="server" Text="Default_ZMCF_Comunicazioni3"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Comunicazioni4" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="Comunicazioni4CheckBox" runat="server"  Checked='<%# Bind("Comunicazioni4") %>'/>
                                    <asp:Label SkinID="FieldValue" ID="ZML_Comunicazioni4" runat="server" Text="Default_ZMCF_Comunicazioni4"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Comunicazioni5" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="Comunicazioni5CheckBox" runat="server"  Checked='<%# Bind("Comunicazioni5") %>' />
                                    <asp:Label SkinID="FieldValue" ID="ZML_Comunicazioni5" runat="server" Text="Default_ZMCF_Comunicazioni5"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Comunicazioni6" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="Comunicazioni6CheckBox" runat="server"  Checked='<%# Bind("Comunicazioni6") %>' />
                                    <asp:Label SkinID="FieldValue" ID="ZML_Comunicazioni6" runat="server" Text="Default_ZMCF_Comunicazioni6"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Comunicazioni7" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="Comunicazioni7CheckBox" runat="server"  Checked='<%# Bind("Comunicazioni7") %>' />
                                    <asp:Label SkinID="FieldValue" ID="ZML_Comunicazioni7" runat="server" Text="Default_ZMCF_Comunicazioni7"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Comunicazioni8" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="Comunicazioni8CheckBox" runat="server"  Checked='<%# Bind("Comunicazioni8") %>' />
                                    <asp:Label SkinID="FieldValue" ID="ZML_Comunicazioni8" runat="server" Text="Default_ZMCF_Comunicazioni8"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Comunicazioni9" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="Comunicazioni9CheckBox" runat="server"  Checked='<%# Bind("Comunicazioni9") %>' />
                                    <asp:Label SkinID="FieldValue" ID="ZML_Comunicazioni9" runat="server" Text="Default_ZMCF_Comunicazioni9"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_Comunicazioni10" runat="server">
                            <tr>
                                <td class="BlockBoxValue" colspan="2">
                                    <asp:CheckBox ID="Comunicazioni10CheckBox" runat="server"  Checked='<%# Bind("Comunicazioni10") %>' />
                                    <asp:Label SkinID="FieldValue" ID="ZML_Comunicazioni10" runat="server" Text="Default_ZMCF_Comunicazioni10"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                          <tr>
                                <td class="BlockBoxValue" colspan="2">
                                <asp:Label ID="ProfessioneDescriptionLabel" SkinID="FieldDescription" runat="server">Causale registrazione:</asp:Label>
                                    <asp:Label SkinID="FieldValue" ID="CausaleLabel" runat="server" Text='<%# GetCausalHumanReadble(Eval("RegCausal").ToString()) %>'></asp:Label>
                                </td>
                            </tr>
                    </table>

            <asp:HiddenField ID="RecordEdtDate" runat="server" Value='<%# Bind("RecordEdtDate") %>'
                OnDataBinding="RecordEdtDate_DataBinding" />
            <asp:HiddenField ID="RecordEdtUser" runat="server" Value='<%# Bind("RecordEdtUser") %>'
                OnDataBinding="RecordEdtUser_DataBinding" />
        </div>
        <div class="BlockBox">
            <div class="BlockBoxHeader">
                <asp:Label ID="Label8" runat="server" Text="Ricerca interna utenti"></asp:Label>
            </div>
            <table>
                <asp:Panel ID="Panel1" runat="server">
                    <tr>
                        <td class="BlockBoxValue">
                            <asp:Label SkinID="FieldValue" ID="Label9" runat="server" Text="Tag ricerca utenti Zeus"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="RicercaUtentiTextBox" Width="200" SkinID="TextBox" Text='<%# Bind("Meta_ZeusSearch") %>'></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Label SkinID="FieldValue" ID="Label10" runat="server" Text="Separare le keywords con virgola"></asp:Label>
                        </td>
                    </tr>
                </asp:Panel>
            </table>
        </div>
        <div align="center" style="margin:0;padding:0 0 8px 0;border:0px;display:block;">
            <asp:LinkButton ID="UpdateButton" runat="server" SkinID="ZSSM_Button01" CommandName="Update"
                Text="Salva dati"></asp:LinkButton>
        </div>
        <asp:HiddenField ID="zdtRecordNewUser" runat="server" Value='<%# Eval("RecordNewUser") %>' />
        <asp:HiddenField ID="zdtRecordNewDate" runat="server" Value='<%# Eval("RecordNewDate") %>' />
        <asp:HiddenField ID="zdtRecordEdtUser" runat="server" Value='<%# Eval("RecordEdtUser") %>' />
        <asp:HiddenField ID="zdtRecordEdtDate" runat="server" Value='<%# Eval("RecordEdtDate") %>' />
    </EditItemTemplate>
</asp:FormView>
<asp:SqlDataSource ID="ProfiloMarketingSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
    SelectCommand="SELECT  ConsensoDatiPersonali,ConsensoDatiPersonali2
    ,Comunicazioni1,Comunicazioni2,Comunicazioni3,Comunicazioni4,Comunicazioni5
    ,Comunicazioni6,Comunicazioni7,Comunicazioni8,Comunicazioni9,Comunicazioni10
    ,RecordNewDate,RecordNewUser,RecordEdtDate,RecordEdtUser ,[ID2Categoria1],RegCausal, Meta_ZeusSearch
    FROM tbProfiliMarketing"
    UpdateCommand=" SET DATEFORMAT dmy; UPDATE [tbProfiliMarketing] SET  
     [ConsensoDatiPersonali]=@ConsensoDatiPersonali,
    [ConsensoDatiPersonali2]=@ConsensoDatiPersonali2, 
    Comunicazioni1=@Comunicazioni1,
    Comunicazioni2=@Comunicazioni2,
    Comunicazioni3=@Comunicazioni3,
    Comunicazioni4=@Comunicazioni4,
    Comunicazioni5=@Comunicazioni5,
    Comunicazioni6=@Comunicazioni6,
    Comunicazioni7=@Comunicazioni7,
    Comunicazioni8=@Comunicazioni8,
    Comunicazioni9=@Comunicazioni9,
    Comunicazioni10=@Comunicazioni10,
    Meta_ZeusSearch=@Meta_ZeusSearch,
    [RecordEdtUser] = @RecordEdtUser, 
    [RecordEdtDate] = @RecordEdtDate ,
    [ID2Categoria1]=@ID2Categoria1
    WHERE [UserID] =@UserID"
    OnUpdated="ProfiloMarketingSqlDataSource_Updated">
    <UpdateParameters>
    <asp:Parameter Name="ID2Categoria1" Type="Int32" />
        <asp:Parameter Name="ConsensoDatiPersonali" Type="Boolean" />
        <asp:Parameter Name="ConsensoDatiPersonali2" Type="Boolean" />
        <asp:Parameter Name="Comunicazioni1" Type="Boolean" />
        <asp:Parameter Name="Comunicazioni2" Type="Boolean" />
        <asp:Parameter Name="Comunicazioni3" Type="Boolean" />
        <asp:Parameter Name="Comunicazioni4" Type="Boolean" />
        <asp:Parameter Name="Comunicazioni5" Type="Boolean" />
        <asp:Parameter Name="Comunicazioni6" Type="Boolean" />
        <asp:Parameter Name="Comunicazioni7" Type="Boolean" />
        <asp:Parameter Name="Comunicazioni8" Type="Boolean" />
        <asp:Parameter Name="Comunicazioni9" Type="Boolean" />
        <asp:Parameter Name="Comunicazioni10" Type="Boolean" />
        <asp:Parameter Name="Meta_ZeusSearch" Type="String" />
        <asp:Parameter Name="RecordEdtUser" />
        <asp:Parameter Name="RecordEdtDate" Type="DateTime" />
        <asp:ControlParameter ControlID="UIDHiddenField" Name="UserID"  />
    </UpdateParameters>
</asp:SqlDataSource>
