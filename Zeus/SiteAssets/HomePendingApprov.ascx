﻿<%@ Control Language="C#" ClassName="HomePendingApprov" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.DataItemIndex > -1)
        {

            Image Image1 = (Image)e.Row.FindControl("Image1");
            Label DataVerifica = (Label)e.Row.FindControl("DataVerifica");

            try
            {
                if (DataVerifica.Text.ToString().Length > 0)
                {
                    Image1.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";
                }
            }
            catch (Exception)
            {
            }
        }
    }



    
</script>

<asp:GridView ID="GridView2" runat="server" AllowPaging="True" AllowSorting="True" DataSourceID="dsPendingApprov"
    AutoGenerateColumns="False" PageSize="30" OnRowDataBound="GridView1_RowDataBound">
    <Columns>
        <asp:BoundField DataField="Cognome" HeaderText="Cognome" SortExpression="Cognome" />
        <asp:BoundField DataField="Nome" HeaderText="Nome" SortExpression="Nome" />
        <asp:BoundField DataField="CodiceSocioStr" HeaderText="Cod. Socio" SortExpression="CodiceSocioStr" ItemStyle-HorizontalAlign="Center" />
        <asp:BoundField DataField="RagioneSociale" HeaderText="Azienda" SortExpression="RagioneSociale" />
        <asp:BoundField DataField="TipologiaQuota" HeaderText="Tipologia" SortExpression="TipologiaQuota" ItemStyle-HorizontalAlign="Center" />
        <asp:BoundField DataField="TotaleQuota" HeaderText="Quota EUR" SortExpression="TotaleQuota" ItemStyle-HorizontalAlign="Right" />
        <asp:BoundField DataField="DataRichiestaSocio" HeaderText="Data richiesta" SortExpression="DataRichiestaSocio" ItemStyle-HorizontalAlign="Center" />

        <asp:TemplateField HeaderText="DataVerifica" Visible="False">
            <ItemTemplate>
                <asp:Label ID="DataVerifica" runat="server" Text='<%# Eval("DataVerifica") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Pagamento" SortExpression="DataVerifica">
            <ItemTemplate>
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" />
            </ItemTemplate>
            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
        </asp:TemplateField>
        <asp:HyperLinkField DataNavigateUrlFields="UserId" DataNavigateUrlFormatString="/Zeus/Soci/Soci_Dsp.aspx?UID={0}"
            HeaderText="Dettaglio" Text="Dettaglio">
            <ControlStyle CssClass="GridView_ZeusButton1" />
            <ItemStyle HorizontalAlign="Center" />
        </asp:HyperLinkField>
        <asp:HyperLinkField DataNavigateUrlFields="UserId" DataNavigateUrlFormatString="/Zeus/Soci/Soci_Appr.aspx?UID={0}"
            HeaderText="Approvazione" Text="Approv./ Rifiut.">
            <ControlStyle CssClass="GridView_ZeusButton1" />
            <ItemStyle HorizontalAlign="Center" />
        </asp:HyperLinkField>
    </Columns>
</asp:GridView>
<asp:SqlDataSource ID="dsPendingApprov" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
    SelectCommand="SET DATEFORMAT dmy; SELECT * FROM [vwSociInAttesaApprovazione_Lst] "></asp:SqlDataSource>
<div class="VertSpacerMedium">
</div>
