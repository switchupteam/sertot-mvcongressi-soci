﻿<%@ Control Language="C#" ClassName="ProfiloSocietario_Dtl" %>


<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">

    
    private string UserID = string.Empty;

    public string UID
    {
        set { UserID = value; }
        get { return UserID; }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
            BindTheData();

       

    }//fine PageLoad


    private string GetLang()
    {
        string Lang = "ITA";

        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;

    }//fine GetLang



    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_Profili.xml"));

            Panel ZMCF_BusinessBook = (Panel)ProfiloSocietarioFormView.FindControl("ZMCF_BusinessBook");

            if (ZMCF_BusinessBook != null)
                ZMCF_BusinessBook.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BusinessBook").InnerText);

            
            return true;
        }
        catch (Exception)
        {
            return false;
        }

    }//fine ReadXML
    


   

    public bool BindTheData()
    {
        try
        {

            ProfiloSocietarioSqlDataSource.SelectCommand += " WHERE ([UserID] = '" + UserID + "')";
            ProfiloSocietarioFormView.DataSourceID = "ProfiloSocietarioSqlDataSource";
            ProfiloSocietarioSqlDataSource.DataBind();

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }


    }//fine BindTheData 

    protected void Clear(object sender, EventArgs e)
    {
        Label Nazione = (Label)sender;
        Nazione.Text = Nazione.Text.Replace(">", "");

    }//fine Clear



    protected void IdentificativoLabel_DataBinding(object sender, EventArgs e)
    {
        Label IdentificativoLabel = (Label)sender;

        if (IdentificativoLabel.Text.Length == 0)
            IdentificativoLabel.Text = "Non inserita";

    }//fine IdentificativoLabel_DataBinding


    protected void ProfiloSocietarioFormView_DataBound(object sender, EventArgs e)
    {
        ReadXML("PROFILI", GetLang());
    }
    
</script>

<asp:FormView ID="ProfiloSocietarioFormView" runat="server" Width="100%" OnDataBound="ProfiloSocietarioFormView_DataBound">
    <ItemTemplate>
            <div class="BlockBoxHeader">
                <asp:Label ID="ProfilosocietarioLabel" runat="server" Text="Affiliazione"></asp:Label></div>
            <table border="0" cellpadding="2" cellspacing="2">
                <tr>
                    <td>
                        <asp:Label ID="RagioneSocialeDescriptionLabel" SkinID="FieldDescription" runat="server">Istituto:</asp:Label>
                        <asp:Label ID="RagioneSocialeLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("RagioneSociale") %>'>
                        </asp:Label>
                    </td>
                </tr>
                <asp:Panel ID="ZMCF_BusinessBook" runat="server">
                  <tr>
                    <td>
                        <asp:Label ID="Label1" SkinID="FieldDescription" runat="server">Azienda collegata:</asp:Label>
                        <asp:Label ID="IdentificativoLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("Identificativo") %>' OnDataBinding="IdentificativoLabel_DataBinding">
                        </asp:Label>
                    </td>
                </tr>
                </asp:Panel>
              <%--  <tr>
                    <td>
                        <asp:Label ID="PartitaIVADescriptionLabel" SkinID="FieldDescription" runat="server">Partita IVA:</asp:Label>
                        <asp:Label ID="PartitaIVALabel" runat="server" SkinID="FieldValue" Text='<%# Eval("PartitaIVA") %>'>
                        </asp:Label>
                    </td>
                </tr>--%>
                <tr>
                    <td>
                        <asp:Label ID="S1_IndirizzoDescriptionLabel" SkinID="FieldDescription" runat="server">Istituto - Indirizzo:</asp:Label>
                        <asp:Label ID="S1_IndirizzoLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("S1_Indirizzo") %>'>
                        </asp:Label>
                    </td>
                </tr>
               <%-- <tr>
                    <td>
                        <asp:Label ID="S1_NumeroDescriptionLabel" SkinID="FieldDescription" runat="server">Istituto - Numero:</asp:Label>
                        <asp:Label ID="S1_NumeroLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("S1_Numero") %>'></asp:Label>
                    </td>
                </tr>--%>
                <tr>
                    <td>
                        <asp:Label ID="S1_CittaDescriptionLabel" SkinID="FieldDescription" runat="server">Istituto - Città:</asp:Label>
                        <asp:Label ID="S1_CittaLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("S1_Citta") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="S1_ComuneDescriptionLabel" SkinID="FieldDescription" runat="server">Istituto - Comune:</asp:Label>
                        <asp:Label ID="S1_ComuneLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("S1_Comune") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="S1_CapDescriptionLabel" SkinID="FieldDescription" runat="server">Istituto - CAP:</asp:Label>
                        <asp:Label ID="S1_CapLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("S1_Cap") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="S1_ProvinciaEstesaDescriptionLabel" SkinID="FieldDescription" runat="server">Istituto - Provincia:</asp:Label>
                        <asp:Label ID="S1_ProvinciaEstesaLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("S1_ProvinciaEstesa") %>'>
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="S1_ProvinciaSiglaDescriptionLabel" SkinID="FieldDescription" runat="server">Istituto - Provincia (sigla):</asp:Label>
                        <asp:Label ID="S1_ProvinciaSiglaLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("S1_ProvinciaSigla") %>'>
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="S1_NazioneDescriptionLabel" SkinID="FieldDescription" runat="server">Istituto - Nazione:</asp:Label>
                        <asp:Label ID="S1_NazioneLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("S1_Nazione") %>'
                            OnDataBinding="Clear">
                        </asp:Label>
                    </td>
                </tr>
                <asp:Panel ID="ContinentePanel" runat="server" Visible="false">
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="S1_ContinenteDescriptionLabel" SkinID="FieldDescription" runat="server">Istituto - Continente: </asp:Label>
                            <asp:Label ID="S1_ContinenteLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("S1_Continente") %>'>
                            </asp:Label>
                        </td>
                    </tr>
                </asp:Panel>
                <%--<tr>
                    <td>
                        <asp:Label ID="S2_IndirizzoDescriptionLabel" SkinID="FieldDescription" runat="server">Sede legale - Indirizzo:</asp:Label>
                        <asp:Label ID="S2_IndirizzoLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("S2_Indirizzo") %>'>
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="S2_NumeroDescriptionLabel" SkinID="FieldDescription" runat="server">Sede legale - Numero:</asp:Label>
                        <asp:Label ID="S2_NumeroLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("S2_Numero") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="S2_CittaDescriptionLabel" SkinID="FieldDescription" runat="server"> Sede legale - Citta:</asp:Label>
                        <asp:Label ID="S2_CittaLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("S2_Citta") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="S2_ComuneDescriptionLabel" SkinID="FieldDescription" runat="server">Sede legale - Comune:</asp:Label>
                        <asp:Label ID="S2_ComuneLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("S2_Comune") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="S2_CapDescriptionLabel" SkinID="FieldDescription" runat="server">Sede legale - CAP:</asp:Label>
                        <asp:Label ID="S2_CapLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("S2_Cap") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="S2_ProvinciaEstesaDescriptionLabel" SkinID="FieldDescription" runat="server">Sede legale - Provincia:</asp:Label>
                        <asp:Label ID="S2_ProvinciaEstesaLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("S2_ProvinciaEstesa") %>'>
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="S2_ProvinciaSiglaDescriptionLabel" SkinID="FieldDescription" runat="server">Sede legale - Provincia (sigla):</asp:Label>
                        <asp:Label ID="S2_ProvinciaSiglaLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("S2_ProvinciaSigla") %>'>
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="S2_NazioneDescriptionLabel" SkinID="FieldDescription" runat="server">Sede legale - Nazione:</asp:Label>
                        <asp:Label ID="S2_NazioneLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("S2_Nazione") %>'
                            OnDataBinding="Clear">
                        </asp:Label>
                    </td>
                </tr>
                <asp:Panel ID="ContinenteSedePanel" runat="server" Visible="false">
                    <tr>
                        <td>
                            <asp:Label ID="S2_ContinenteDescriptionLabel" SkinID="FieldDescription" runat="server">Sede legale - Continente:</asp:Label>
                            <asp:Label ID="S2_ContinenteLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("S2_Continente") %>'>
                            </asp:Label>
                        </td>
                    </tr>
                </asp:Panel>--%>
   <%--             <tr>
                    <td>
                        <asp:Label ID="Telefono1DescriptionLabel" SkinID="FieldDescription" runat="server">Telefono principale:</asp:Label>
                        <asp:Label ID="Telefono1Label" SkinID="FieldValue" runat="server" Text='<%# Eval("Telefono1") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Telefono2DescriptionLabel" SkinID="FieldDescription" runat="server">Telefono alternativo:</asp:Label>
                        <asp:Label ID="Telefono2Label" SkinID="FieldValue" runat="server" Text='<%# Eval("Telefono2") %>'></asp:Label>
                    </td>
                </tr>--%>
       <%--         <tr>
                    <td>
                        <asp:Label ID="FaxDescriptionLabel" SkinID="FieldDescription" runat="server">Fax:</asp:Label>
                        <asp:Label ID="FaxLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("Fax") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="WebSiteLabel" SkinID="FieldDescription" runat="server">WebSite:</asp:Label>
                        <asp:HyperLink ID="WebSiteHyperLink" SkinID="FieldValue" runat="server" NavigateUrl='<%# Eval("WebSite") %>'
                            Text='<%# Eval("WebSite") %>' Target="_blank"></asp:HyperLink>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Label ID="EnabledLabel" runat="server" Visible="false" Text="Attenzione: dati non presenti."></asp:Label>
                    </td>
                </tr>--%>
            </table>
    </ItemTemplate>
</asp:FormView>
<asp:SqlDataSource ID="ProfiloSocietarioSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
    SelectCommand="SELECT [UserId], [RagioneSociale], [PartitaIVA], [S1_Indirizzo], [S1_Numero],
     [S1_Citta], [S1_Comune], [S1_Cap], [S1_ProvinciaEstesa], [S1_ProvinciaSigla], [S1_ID2Nazione], 
     [S1_Nazione], [S1_Continente], [S2_Indirizzo], [S2_Numero], [S2_Citta], [S2_Comune], [S2_Cap], 
     [S2_ProvinciaEstesa], [S2_ProvinciaSigla], [S2_ID2Nazione], [S2_Nazione], [S2_Continente], [Telefono1],
      [Telefono2], [Fax], [WebSite],[Identificativo] FROM [vwProfiliSocietari]">
</asp:SqlDataSource>
