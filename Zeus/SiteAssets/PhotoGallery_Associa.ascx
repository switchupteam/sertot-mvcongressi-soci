﻿<%@ Control Language="C#" ClassName="PhotoGallery_Associa" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">

    static int UP = 4;
    static int DOWN = 5;


    private string _ZIMGalleryAssociation = string.Empty;

    public string ZIMGalleryAssociation
    {
        get { return _ZIMGalleryAssociation; }
        set { _ZIMGalleryAssociation = value; }
    }


    private string _TitoloControlID = string.Empty;

    public string TitoloControlID
    {
        get { return _TitoloControlID; }
        set { _TitoloControlID = value; }
    }


    private string _IDContenuto = string.Empty;

    public string IDContenuto
    {
        get { return _IDContenuto; }
        set { _IDContenuto = value; }
    }

    private string _ZeusIdModuloContenuto = string.Empty;

    public string ZeusIdModuloContenuto
    {
        get { return _ZeusIdModuloContenuto; }
        set { _ZeusIdModuloContenuto = value; }
    }  

    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();

        if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM"))
            || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang")))
            Response.Redirect("~/Zeus/System/Message.aspx");


    }//fine Page__Load

    //FUNZIONI PER L'ORDINAMENTO  


    private int getLastOrder()
    {

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT MAX(Ordinamento)";
                sqlQuery += " FROM vwPhotoGallery_Associazioni ";
                sqlQuery += " WHERE ID2Function=@ID2Function AND ZeusLangCode=@ZeusLangCode  AND ZIMFunction=@ZIMFunction";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID2Function", System.Data.SqlDbType.Int);
                command.Parameters["@ID2Function"].Value = Server.HtmlEncode(Request.QueryString["XRI"]);

                command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ZeusLangCode"].Value = Server.HtmlEncode(Request.QueryString["Lang"]);

                command.Parameters.Add("@ZIMFunction", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ZIMFunction"].Value = Server.HtmlEncode(Request.QueryString["ZIM"]);

                SqlDataReader reader = command.ExecuteReader();

                int myReturn = 0;

                while (reader.Read())
                    if (!reader.IsDBNull(0))
                        myReturn = reader.GetInt32(0);


                reader.Close();
                return myReturn;
            }
            catch (Exception p)
            {

                Response.Write(p.ToString());
                return -1;
            }
        }//fine Using

    }//fine getLastOrder


    static int LIVELLI = 1;

    private bool CheckIntegrityR(string ZeusIdModulo, string ZeusLangCode, string ID2Function)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            SqlTransaction transaction = null;

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                transaction = connection.BeginTransaction();
                command.Transaction = transaction;



                ArrayList myArray = new ArrayList();
               
                string[][] myMenuChange = null;
               
                int myOrder = 1;

                
                SqlDataReader reader = null;


                for (int index = 1; index <= LIVELLI; ++index)
                {


                        sqlQuery = "SELECT ID1PhotoAssoc,Titolo,Ordinamento";
                        sqlQuery += " FROM vwPhotoGallery_Associazioni";
                        sqlQuery += " WHERE ZeusLangCode=@ZeusLangCode";
                        sqlQuery += " AND ZIMFunction=@ZIMFunction";
                        sqlQuery += " AND ID2Function=@ID2Function";
                        sqlQuery += " ORDER BY Ordinamento,ID1PhotoAssoc";
                        command.CommandText = sqlQuery;
                        command.Parameters.Clear();

                        command.Parameters.Add("@ZIMFunction", System.Data.SqlDbType.NVarChar, 5);
                        command.Parameters["@ZIMFunction"].Value = ZeusIdModulo;

                        command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.NVarChar, 3);
                        command.Parameters["@ZeusLangCode"].Value = ZeusLangCode;

                        command.Parameters.Add("@ID2Function", System.Data.SqlDbType.Int, 4);
                        command.Parameters["@ID2Function"].Value = ID2Function;

                        reader = command.ExecuteReader();

                        myOrder = 1;

                        while (reader.Read())
                        {

                            if (myOrder != reader.GetInt32(2))
                            {

                                string[] myData = new string[3];
                                myData[0] = reader.GetInt32(0).ToString();
                                myData[1] = reader.GetString(1);
                                myData[2] = myOrder.ToString();
                                myArray.Add(myData);
                            }

                            ++myOrder;
                        }//fine while

                        reader.Close();

                 


                    myMenuChange = (string[][])myArray.ToArray(typeof(string[]));
                    myArray = new ArrayList();


                    for (int i = 0; i < myMenuChange.Length; ++i)
                    {

                        sqlQuery = "UPDATE tbxPhotoGallery_Associazioni ";
                        sqlQuery += " SET Ordinamento=@Ordinamento";
                        sqlQuery += " WHERE ID1PhotoAssoc=@ID1PhotoAssoc";
                        command.CommandText = sqlQuery;
                        command.Parameters.Clear();

                        command.Parameters.Add("@Ordinamento", System.Data.SqlDbType.NVarChar, 4);
                        command.Parameters["@Ordinamento"].Value = myMenuChange[i][2];

                        command.Parameters.Add("@ID1PhotoAssoc", System.Data.SqlDbType.Int, 4);
                        command.Parameters["@ID1PhotoAssoc"].Value = myMenuChange[i][0];

                        command.ExecuteNonQuery();


                    }//fine for




                }//fine for LIVELLI
                transaction.Commit();
              
                return true;
            }
            catch (Exception p)
            {

                //Response.Write(p.ToString());
                transaction.Rollback();
                return false;
            }
        }//fine using
    }//fine CheckIntegrityR


    //FINE FUNZIONI PER ORDINAMENTO  


    //FUNZIONI PER LA GRIDVIEW CHE ULIZZANO L'ORDINAMENTO
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {


        SqlTransaction transaction = null;

        try
        {

            int index = Convert.ToInt32(e.CommandArgument);
            int indexNew = index;

            if (e.CommandName.ToString().Equals("Up"))
                --indexNew;
            else if (e.CommandName.ToString().Equals("Down"))
                ++indexNew;



            GridViewRow row = GridView1.Rows[index];
            Label ID1PhotoAssoc = (Label)row.FindControl("ID1PhotoAssoc");
            Label Ordinamento = (Label)row.FindControl("Ordinamento");


            GridViewRow rowNew = GridView1.Rows[indexNew];
            Label ID1PhotoAssocNew = (Label)rowNew.FindControl("ID1PhotoAssoc");
            Label OrdinamentoNew = (Label)rowNew.FindControl("Ordinamento");

            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            string sqlQuery = string.Empty;


            using (SqlConnection connection = new SqlConnection(
               strConnessione))
            {


                connection.Open();

                SqlCommand command = connection.CreateCommand();

                transaction = connection.BeginTransaction();
                command.Transaction = transaction;


                sqlQuery = "UPDATE tbxPhotoGallery_Associazioni";
                sqlQuery += " SET Ordinamento=" + OrdinamentoNew.Text;
                sqlQuery += " WHERE ID1PhotoAssoc=" + ID1PhotoAssoc.Text + ";";
                sqlQuery += "UPDATE tbxPhotoGallery_Associazioni";
                sqlQuery += " SET Ordinamento=" + Ordinamento.Text;
                sqlQuery += " WHERE ID1PhotoAssoc=" + ID1PhotoAssocNew.Text;
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();


                transaction.Commit();



            }//fine using

            GridView1.DataBind();


        }
        catch (Exception p)
        {
            //Response.Write(p.ToString());

            if (transaction != null)
                transaction.Rollback();
        }

    }//fine GridView1_RowCommand

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {

            if (e.Row.DataItemIndex > -1)
            {

                Label Ordinamento = (Label)e.Row.FindControl("Ordinamento");
                int ordinamentoRow = Convert.ToInt32(Ordinamento.Text);

                int maxordine = getLastOrder();


                //Response.Write("DEB ordinamentoRow: " + ordinamentoRow+"<br />");
                //Response.Write("DEB maxordine: " + maxordine + "<br />");

                if (ordinamentoRow == 1)
                    e.Row.Cells[UP].Text = string.Empty;

                if (ordinamentoRow == maxordine)
                    e.Row.Cells[DOWN].Text = string.Empty;



                //immagini attivo
                Image Image1 = (Image)e.Row.FindControl("Image1");
                Label AttivoArea1 = (Label)e.Row.FindControl("AttivoArea1");

                if ((Image1 != null) && (AttivoArea1 != null))
                    if (Convert.ToBoolean(AttivoArea1.Text.ToString()))
                        Image1.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";


                if (e.Row.Cells[UP].Controls.Count > 0)
                {
                    e.Row.Cells[UP].Attributes.Add("onmouseover", "showImageUP('" + e.Row.Cells[UP].Controls[0].ClientID + "','2');");
                    e.Row.Cells[UP].Attributes.Add("onmouseout", "showImageUP('" + e.Row.Cells[UP].Controls[0].ClientID + "','1');");
                }

                if (e.Row.Cells[DOWN].Controls.Count > 0)
                {
                    e.Row.Cells[DOWN].Attributes.Add("onmouseover", "showImageDOWN('" + e.Row.Cells[DOWN].Controls[0].ClientID + "','2');");
                    e.Row.Cells[DOWN].Attributes.Add("onmouseout", "showImageDOWN('" + e.Row.Cells[DOWN].Controls[0].ClientID + "','1');");
                }



            }//fine if
        }
        catch (Exception p)
        {

            //Response.Write(p.ToString());
        }

    }//fine GridView1_RowDataBound

    //void GridView1_RowUpdating(Object sender, GridViewUpdateEventArgs e)
    //{

    //    int index = GridView1.EditIndex;
    //    GridViewRow row = GridView1.Rows[index];

    //    CheckBox AttivoArea1CheckBox = (CheckBox)row.FindControl("AttivoArea1CheckBox");

    //    Response.Write("DEB update " + AttivoArea1CheckBox.Checked+"<br />");
    //    e.NewValues["PW_Area1"] = AttivoArea1CheckBox.Checked;


    //}//fine GridView1_RowUpdating


    



    protected void GridView1_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {
        CheckIntegrityR(Server.HtmlEncode(Request.QueryString["ZIM"])
            , Server.HtmlEncode(Request.QueryString["Lang"])
            , Server.HtmlEncode(Request.QueryString["XRI"]));

    }//fine GridView1_RowDeleted


    protected void AggiungiLinkButton_Click(object sender, EventArgs e)
    {
        string myRedirect = "~/Zeus/PhotoGallery/Associa.aspx?";
        myRedirect += "ZIM="+Server.HtmlEncode(Request.QueryString["ZIM"]);
        myRedirect += "&Lang=" + Server.HtmlEncode(Request.QueryString["Lang"]);
        myRedirect += "&XRI=" + Server.HtmlEncode(Request.QueryString["XRI"]);
        
        Response.Redirect(myRedirect);

    }//fine AggiungiLinkButton_Click

   

    private bool AddNewGallery()
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        string Titolo = string.Empty;
        
        delinea myDelinea= new delinea();

        Label myTitolo = (Label)myDelinea.FindControlRecursive(Page, TitoloControlID);

        if (myTitolo != null)
            Titolo = myTitolo.Text;
        
        try
        {

            object CreatorGUID = Membership.GetUser().ProviderUserKey;
            object ZeusID = Guid.NewGuid();
            
            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();


                sqlQuery = @"INSERT INTO [tbPhotoGallery]
           ([Titolo]
           ,[ID2Categoria]
           ,[DescBreve]
           ,[Descrizione]
           ,[Immagine1]
           ,[ZeusLangCode]
           ,[ZeusIdModulo]
           ,[ZeusId]
           ,[PW_Area1]
           ,[PWI_Area1]
           ,[PWF_Area1]
           ,[RecordNewUser]
           ,[RecordNewDate])
VALUES
           ('" + Titolo +  @"'
           ,0
           ,''
           ,''
           ,''
           ,'"+GetLang()+@"'
           ,'" + ZIMGalleryAssociation +@"'
           ,@ZeusId
           ,0
           ,@PWI_Area1
           ,@PWF_Area1
           ,@RecordNewUser
           ,@RecordNewDate);SELECT SCOPE_IDENTITY(); ";

                command.CommandText = sqlQuery;
                command.Parameters.Add("@ZeusId", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@ZeusId"].Value = ZeusID;
                command.Parameters.Add("@PWI_Area1", System.Data.SqlDbType.SmallDateTime);
                command.Parameters["@PWI_Area1"].Value = DateTime.Now;
                command.Parameters.Add("@PWF_Area1", System.Data.SqlDbType.SmallDateTime);
                command.Parameters["@PWF_Area1"].Value = DateTime.Now.AddYears(50);
                command.Parameters.Add("@RecordNewUser", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@RecordNewUser"].Value = CreatorGUID;
                command.Parameters.Add("@RecordNewDate", System.Data.SqlDbType.SmallDateTime);
                command.Parameters["@RecordNewDate"].Value = DateTime.Now;




                int Identity = Convert.ToInt32(command.ExecuteScalar());

                sqlQuery = @"INSERT INTO [tbxPhotoGallery_Associazioni]
           ([ID2PhotoGallery]
           ,[ZIMFunction]
           ,[ID2Function]
           ,[Ordinamento]
           ,[PW_Area1]
           ,[ZeusLangCode])
     VALUES
            (" + Identity + @"
           ,'" + ZeusIdModuloContenuto + @"'
           ," + IDContenuto + @"
           ," + (getLastOrder() + 1).ToString() +@"
           ,1
           ,'"+GetLang()+"')";

                command.Parameters.Clear();
                command.CommandText = sqlQuery;
                
                
                

                command.ExecuteNonQuery();

                Response.Redirect("/Zeus/PhotoGallery/Gallery_Fotografie.aspx?ZID="+ZeusID+"&Lang="+GetLang()+"&ZIM="+ZIMGalleryAssociation);

                return true;

            }//fine Using
        }
        catch (Exception p)
        {

            System.Web.HttpContext.Current.Response.Write(p.ToString());
            return false;
        }
    
    }//fine AddNewGallery

    private string GetLang()
    {
        string Lang = "ITA";

        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;

    }//fine GetLang

    protected void NuovoLinkButton_Click(object sender, EventArgs e)
    {
            AddNewGallery();
    
    }//fine NuovoLinkButton_Click

    
</script>

<script language="javascript" type="text/javascript">
            function showImageUP(imgId,typ) {
                var objImg = document.getElementById(imgId);
                if (objImg) {
                    if (typ == "1")
                        objImg.src = "../SiteImg/Bt3_ArrowUp_Off.gif";
                   else if (typ == "2")
                       objImg.src = "../SiteImg/Bt3_ArrowUp_On.gif";
               }
           }
           
           function showImageDOWN(imgId,typ) {
                var objImg = document.getElementById(imgId);
                if (objImg) {
                    if (typ == "1")
                        objImg.src = "../SiteImg/Bt3_ArrowDown_Off.gif";
                   else if (typ == "2")
                       objImg.src = "../SiteImg/Bt3_ArrowDown_On.gif";
               }
           }
</script>

<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="PhotoGallerySqlDataSource"
    OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand" CellPadding="0"
    DataKeyNames="ID1PhotoAssoc" Width="100%" OnRowDeleted="GridView1_RowDeleted" >
    <Columns>
        <asp:TemplateField HeaderText="ID1Menu" SortExpression="ID1Menu" Visible="False">
            <ItemTemplate>
                <asp:Label ID="ID1PhotoAssoc" runat="server" Text='<%# Eval("ID1PhotoAssoc") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Ordinamento" SortExpression="Ordinamento" Visible="False">
            <ItemTemplate>
                <asp:Label ID="Ordinamento" runat="server" Text='<%# Eval("Ordinamento") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Galleria">
            <ItemTemplate>
                <asp:Label ID="Galleria" runat="server" Text='<%# Eval("Titolo") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Categoria">
            <ItemTemplate>
                <asp:Label ID="categoria" runat="server" Text='<%# Eval("categoria") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:ButtonField CommandName="Up" HeaderText="Ord." ShowHeader="True" ImageUrl="~/Zeus/SiteImg/Bt3_ArrowUp_Off.gif"
            ButtonType="Image" ControlStyle-Width="16" ControlStyle-Height="16" ControlStyle-BorderWidth="0">
            <ItemStyle HorizontalAlign="Center" />
            <ControlStyle CssClass="GridView_Ord1" />
        </asp:ButtonField>
        <asp:ButtonField CommandName="Down" HeaderText="Ord." ShowHeader="True" ButtonType="Image"
            ImageUrl="~/Zeus/SiteImg/Bt3_ArrowDown_Off.gif" ControlStyle-Width="16" ControlStyle-Height="16"
            ControlStyle-BorderWidth="0">
            <ItemStyle HorizontalAlign="Center" />
            <ControlStyle CssClass="GridView_Ord1" />
        </asp:ButtonField>
        <asp:TemplateField HeaderText="Attivo">
            <ItemTemplate>
                <asp:Label ID="AttivoArea1" runat="server" Text='<%# Eval("PW_Area1") %>' Visible="false"></asp:Label>
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" /></ItemTemplate>
            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            <EditItemTemplate>
                <asp:CheckBox ID="AttivoArea1CheckBox" runat="server" Checked='<%# Bind("PW_Area1") %>' />
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:CommandField ButtonType="Link" HeaderText="Rimuovi" ShowDeleteButton="true" 
        DeleteText="Rimuovi" ControlStyle-CssClass="GridView_Button1" ItemStyle-HorizontalAlign="Center"
             />
        <asp:CommandField ButtonType="Link" HeaderText="Pubblica" ShowEditButton="true" UpdateText="Salva"
            EditText="Modifica" ControlStyle-CssClass="GridView_Button1" ItemStyle-HorizontalAlign="Center"
            CancelText="Annulla" />
    </Columns>
    <EmptyDataTemplate>
        Nessuna galleria fotografica è associata a questo contenuto
        <br />
        <br />
        <br />
        <br />
        <br />
    </EmptyDataTemplate>
    <EmptyDataRowStyle BackColor="White" Width="500px" BorderColor="Red" BorderWidth="0px"
        Height="200px" />
</asp:GridView>
 <div style="padding: 10px 0 0 0; margin: 0;">
<asp:LinkButton ID="AggiungiLinkButton" runat="server" Text="Associa galleria" 
SkinID="ZSSM_Button01" OnClick="AggiungiLinkButton_Click"></asp:LinkButton>
<img src="/Zeus/SiteImg/Spc.gif" width="10px" />
<asp:LinkButton ID="NuovoLinkButton" runat="server" Text="Crea nuova galleria" 
SkinID="ZSSM_Button01" OnClick="NuovoLinkButton_Click"></asp:LinkButton>
</div>
<asp:SqlDataSource ID="PhotoGallerySqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
    SelectCommand="
    IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS  WHERE  TABLE_NAME = 'vwPhotoGallery_Associazioni'))
BEGIN

    
    SELECT 
       [ID1PhotoAssoc]
      ,[Titolo]
      ,[Categoria]
      ,[Ordinamento]
      ,[ZIMFunction]
      ,[ID2Function]
      ,[ZeusLangCode]
      ,[PW_Area1]
      ,[ID2PhotoGallery]
  FROM [vwPhotoGallery_Associazioni] 
  WHERE ZIMFunction=@ZIMFunction AND ZeusLangCode=@ZeusLangCode AND ID2Function=@ID2Function ORDER BY Ordinamento ASC
   END"
    UpdateCommand=" SET DATEFORMAT dmy; UPDATE [tbxPhotoGallery_Associazioni]
         SET [PW_Area1] =@PW_Area1 
         WHERE [ID1PhotoAssoc] =@ID1PhotoAssoc"
      DeleteCommand="DELETE FROM tbxPhotoGallery_Associazioni  WHERE [ID1PhotoAssoc] =@ID1PhotoAssoc"   >
      <UpdateParameters>
      <asp:Parameter Name="PW_Area1" Type="Boolean" />
      </UpdateParameters>
    <SelectParameters>
        <asp:QueryStringParameter DefaultValue="0" Name="ZIMFunction" QueryStringField="ZIM"
            Type="String" />
        <asp:QueryStringParameter DefaultValue="0" Name="ZeusLangCode" QueryStringField="Lang"
            Type="String" />
        <asp:QueryStringParameter DefaultValue="0" Name="ID2Function" QueryStringField="XRI"
            Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>
