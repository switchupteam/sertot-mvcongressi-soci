﻿<%@ Control Language="C#" ClassName="ProfiloSocietario_Edt" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">

    
    private string UserID = string.Empty;
    private string myRedirectPath = string.Empty;


    public string UID
    {
        set { UserID = value; }
        get { return UserID; }
    }

    public string RedirectPath
    {
        set { myRedirectPath = value; }
        get { return myRedirectPath; }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
            BindTheData();

      

    }//fine PageLoad




    private string GetLang()
    {
        string Lang = "ITA";

        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;

    }//fine GetLang



    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_Profili.xml"));

            Panel ZMCF_BusinessBook = (Panel)ProfiloSocietarioFormView.FindControl("ZMCF_BusinessBook");

            if (ZMCF_BusinessBook != null)
                ZMCF_BusinessBook.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BusinessBook").InnerText);


            return true;
        }
        catch (Exception)
        {
            return false;
        }

    }//fine ReadXML

    public bool BindTheData()
    {
        try
        {

            ProfiloSocietarioSqlDataSource.SelectCommand += " WHERE ([UserID] = '" + UserID + "')";
            //ProfiloSocietarioSqlDataSource.UpdateCommand += " WHERE ([UserID] = '" + UserID + "')";
            ProfiloSocietarioFormView.DataSourceID = "ProfiloSocietarioSqlDataSource";
            ProfiloSocietarioFormView.DataBind();

            UIDHiddenField.Value = UserID;

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }


    }//fine BindTheData 


    protected void RecordEdtUser_DataBinding(object sender, EventArgs e)
    {
        HiddenField UtenteCreazione = (HiddenField)sender;
        MembershipUser user = Membership.GetUser();
        object userKey = user.ProviderUserKey;

        UtenteCreazione.Value = userKey.ToString();
    }

    protected void RecordEdtDate_DataBinding(object sender, EventArgs e)
    {
        HiddenField DataCreazione = (HiddenField)sender;

        DataCreazione.Value = DateTime.Now.ToString();

    }




    protected void TextBoxNumero_DataBinding(object sender, EventArgs e)
    {
        TextBox TextBoxNumero = (TextBox)sender;
        try
        {
            TextBoxNumero.Text = TextBoxNumero.Text.Substring(0, 10);

        }
        catch (Exception) { }
    }


    protected void ProfiloSocietarioFormView_DataBound(object sender, EventArgs e)
    {
        ReadXML("PROFILI", GetLang());
    }

    protected void ProfiloSocietarioSqlDataSource_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.Exception == null)
            if (myRedirectPath.Length > 0)
                Response.Redirect(myRedirectPath);
            else Response.Redirect("~/Zeus/System/Message.aspx");
    }//fine ProfiloSocietarioSqlDataSource_Updated
    
</script>

<asp:HiddenField ID="UIDHiddenField" runat="server" />
<asp:FormView ID="ProfiloSocietarioFormView" DefaultMode="Edit" runat="server" Width="100%"
    OnDataBound="ProfiloSocietarioFormView_DataBound">
    <EditItemTemplate>
        <asp:SqlDataSource ID="dsNazioni2" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
            SelectCommand="SELECT [ID1Nazione], [Nazione] FROM [tbNazioni] ORDER BY [Nazione]">
        </asp:SqlDataSource>
        <div class="BlockBox">
            <div class="BlockBoxHeader">
                <asp:Label ID="Residenza_Label" runat="server" Text="Azienda / ente / società"></asp:Label></div>
            <table cellpadding="2" cellspacing="1" border="0">
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label2" SkinID="FieldDescription" runat="server" Text="Label">Istituto</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TextBoxCognome" Text='<%# Bind("RagioneSociale") %>' runat="server"
                            Columns="50" MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                <asp:Panel ID="ZMCF_BusinessBook" runat="server">
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label13" SkinID="FieldDescription" runat="server" Text="Label">Azienda collegata</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:DropDownList ID="AziendaCollegataDropDownList" runat="server" DataSourceID="AziendaCollegataSqlDataSource"
                                DataTextField="Identificativo" DataValueField="ID1BsnBook" AppendDataBoundItems="true"
                                SelectedValue='<%# Bind("ID2BusinessBook") %>'>
                                <asp:ListItem Text="&gt; Non inserita" Value="0" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="AziendaCollegataSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                SelectCommand="SELECT [ID1BsnBook],[Identificativo]
                                FROM [tbBusinessBook]
                                WHERE [PW_Zeus1]=1"></asp:SqlDataSource>
                        </td>
                    </tr>
                </asp:Panel>
 <%--               <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label3" SkinID="FieldDescription" runat="server" Text="Label">Partita IVA o codice fiscale</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TextBoxNome" runat="server" Text='<%# Bind("PartitaIVA") %>' Columns="50"
                            MaxLength="50"></asp:TextBox>
                    </td>
                </tr>--%>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label5" SkinID="FieldDescription" runat="server" Text="Label">Indirizzo</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TextBoxIndirizzo" Text='<%# Bind("S1_Indirizzo") %>' runat="server"
                            Columns="50" MaxLength="50"></asp:TextBox>
                       <%-- <asp:Label ID="Label6" SkinID="FieldDescription" runat="server" Text="Label">Numero</asp:Label>
                        <asp:TextBox ID="TextBoxNumero" Text='<%# Bind("S1_Numero") %>' runat="server" Columns="10"
                            MaxLength="10"></asp:TextBox>--%>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label7" SkinID="FieldDescription" runat="server" Text="Label">Città</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TextBoxCitta" Text='<%# Bind("S1_Citta") %>' runat="server" Columns="50"
                            MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label8" SkinID="FieldDescription" runat="server" Text="Label">Comune</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TextBoxComune" runat="server" Columns="50" MaxLength="50" Text='<%# Bind("S1_Comune") %>'></asp:TextBox>
                        <asp:Label ID="Label9" SkinID="FieldDescription" runat="server" Text="Label">CAP</asp:Label>
                        <asp:TextBox ID="TextBoxCAP" runat="server" Columns="5" MaxLength="5" Text='<%# Bind("S1_Cap") %>'></asp:TextBox>
                        <asp:RangeValidator SkinID="ZSSM_Validazione01" Display="Dynamic" ID="RangeValidator1"
                            runat="server" ControlToValidate="TextBoxCAP" ErrorMessage="CAP non corretto"
                            MaximumValue="99999" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label10" SkinID="FieldDescription" runat="server" Text="Label">Provincia</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TextBoxProvincia" Text='<%# Bind("S1_ProvinciaEstesa") %>' runat="server"
                            Columns="50" MaxLength="50"></asp:TextBox>
                        <asp:Label ID="Label11" SkinID="FieldDescription" runat="server" Text="Label">Sigla provincia</asp:Label>
                        <asp:TextBox ID="TextBoxSiglaProvincia" runat="server" Columns="2" MaxLength="2"
                            Text='<%# Bind("S1_ProvinciaSigla") %>'></asp:TextBox>
                        <asp:RegularExpressionValidator SkinID="ZSSM_Validazione01" Display="Dynamic" ID="RegularExpressionValidator3"
                            runat="server" ControlToValidate="TextBoxSiglaProvincia" ErrorMessage="Sigla provincia non corretta"
                            ValidationExpression="^[A-Za-z]{2}$"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label12" SkinID="FieldDescription" runat="server" Text="Label">Nazione</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:DropDownList ID="DropDownListNazione" runat="server" DataSourceID="dsNazioni2"
                            DataTextField="Nazione" DataValueField="ID1Nazione" SelectedValue='<%# Bind("S1_ID2Nazione") %>'>
                            <asp:ListItem Text="&gt;  Non inserita" Selected="True" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                            SelectCommand="SELECT [ID1Nazione], [Nazione] FROM [tbNazioni] ORDER BY [Nazione]">
                        </asp:SqlDataSource>
                    </td>
                </tr>
            </table>
        </div>
       <%-- <div class="BlockBox">
            <div class="BlockBoxHeader">
                <asp:Label ID="Label1" runat="server" Text="Sede ufficio"></asp:Label></div>
            <table cellpadding="2" cellspacing="1" border="0">
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label14" SkinID="FieldDescription" runat="server" Text="Label">Indirizzo</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TextBoxData" Text='<%# Bind("S2_Indirizzo") %>' runat="server" Columns="50"
                            MaxLength="50"></asp:TextBox>
                       <asp:Label ID="Label15" SkinID="FieldDescription" runat="server" Text="Label">Numero</asp:Label>
                        <asp:TextBox ID="TextBoxNumeroUfficio" runat="server" Text='<%# Bind("S2_Numero") %>'
                            OnDataBinding="TextBoxNumero_DataBinding" Columns="10" MaxLength="10"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label16" SkinID="FieldDescription" runat="server" Text="Label">Città</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TextBoxCittaUfficio" Text='<%# Bind("S2_Citta") %>' runat="server"
                            Columns="50" MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label17" SkinID="FieldDescription" runat="server" Text="Label">Comune</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TextBoxComuneUfficio" Text='<%# Bind("S2_Comune") %>' runat="server"
                            Columns="50" MaxLength="50"></asp:TextBox>
                        <asp:Label ID="Label18" SkinID="FieldDescription" runat="server" Text="Label">CAP</asp:Label>
                        <asp:TextBox ID="TextBoxCapUfficio" Text='<%# Bind("S2_Cap") %>' runat="server" Columns="5"
                            MaxLength="5"></asp:TextBox>
                        <asp:RangeValidator SkinID="ZSSM_Validazione01" Display="Dynamic" ID="RangeValidator3"
                            runat="server" ControlToValidate="TextBoxCapUfficio" ErrorMessage="CAP non corretto"
                            MaximumValue="99999" MinimumValue="0"></asp:RangeValidator>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label19" SkinID="FieldDescription" runat="server" Text="Label">Provincia</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TextBoxProvinciaUfficio" runat="server" Columns="50" MaxLength="50"
                            Text='<%# Bind("S2_ProvinciaEstesa") %>'></asp:TextBox>
                        <asp:Label ID="Label20" SkinID="FieldDescription" runat="server" Text="Label">Sigla provincia</asp:Label>
                        <asp:TextBox ID="TextBoxSiglaUfficio" runat="server" Columns="2" MaxLength="2" Text='<%# Bind("S2_ProvinciaSigla") %>'></asp:TextBox>
                        <asp:RegularExpressionValidator SkinID="ZSSM_Validazione01" Display="Dynamic" ID="RegularExpressionValidator7"
                            runat="server" ControlToValidate="TextBoxSiglaUfficio" ErrorMessage="Sigla provincia non corretta"
                            ValidationExpression="^[A-Za-z]{2}$"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label21" SkinID="FieldDescription" runat="server">Nazione</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:DropDownList ID="DropDownListNazioneNascita" runat="server" DataSourceID="dsNazioni2"
                                DataTextField="Nazione" DataValueField="ID1Nazione" SelectedValue='<%# Bind("S2_ID2Nazione") %>'>
                            </asp:DropDownList>
                        </td>
                    </tr>
            </table>
        </div>--%>
        <div class="BlockBox">
            <div class="BlockBoxHeader">
                <asp:Label ID="Label4" runat="server" Text="Contatti"></asp:Label></div>
            <table cellpadding="2" cellspacing="1" border="0">
               <%-- <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label23" SkinID="FieldDescription" runat="server">Telefono principale</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TextBoxTelefonoCasa" Text='<%# Bind("Telefono1") %>' runat="server"
                            Columns="20" MaxLength="20"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label24" SkinID="FieldDescription" runat="server">Telefono alternativo</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TextBoxTelefonoCellulare" Text='<%# Bind("Telefono2") %>' runat="server"
                            Columns="20" Rows="20"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label25" SkinID="FieldDescription" runat="server">Fax</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TextBoxFax" runat="server" Text='<%# Bind("Fax") %>' Columns="20"
                            MaxLength="20"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label26" SkinID="FieldDescription" runat="server">Sito web</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="WebSiteTextBox" runat="server" Text='<%# Bind("WebSite") %>' Columns="100"
                            MaxLength="100"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="WebSiteRegularExpressionValidator" runat="server"
                            ValidationExpression="(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?"
                            ErrorMessage="Indirizzo web non corretto" ControlToValidate="WebSiteTextBox"
                            SkinID="ZSSM_Validazione01" Display="Dynamic"></asp:RegularExpressionValidator>
                    </td>
                </tr>--%>
            </table>
        </div>
        <asp:HiddenField ID="RecordEdtDate" runat="server" Value='<%# Bind("RecordEdtDate") %>'
            OnDataBinding="RecordEdtDate_DataBinding" />
        <asp:HiddenField ID="RecordEdtUser" runat="server" Value='<%# Bind("RecordEdtUser") %>'
            OnDataBinding="RecordEdtUser_DataBinding" />
        <div align="center" style="margin: 0; padding: 0 0 8px 0; border: 0px; display: block;">
            <asp:LinkButton ID="UpdateButton" SkinID="ZSSM_Button01" runat="server" CommandName="Update"
                Text="Salva dati"></asp:LinkButton>
        </div>
        <asp:HiddenField ID="zdtRecordNewUser" runat="server" Value='<%# Eval("RecordNewUser") %>' />
        <asp:HiddenField ID="zdtRecordNewDate" runat="server" Value='<%# Eval("RecordNewDate") %>' />
        <asp:HiddenField ID="zdtRecordEdtUser" runat="server" Value='<%# Eval("RecordEdtUser") %>' />
        <asp:HiddenField ID="zdtRecordEdtDate" runat="server" Value='<%# Eval("RecordEdtDate") %>' />
    </EditItemTemplate>
</asp:FormView>
<%--<asp:SqlDataSource ID="ProfiloSocietarioSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
    SelectCommand="SELECT RagioneSociale, PartitaIVA, S1_Indirizzo, S1_Numero, S1_Citta, S1_Comune, 
    S1_Cap, S1_ProvinciaEstesa, S1_ProvinciaSigla, S1_ID2Nazione,S2_Indirizzo, S2_Numero, 
    S2_Citta, S2_Comune, S2_Cap, S2_ProvinciaEstesa, S2_ProvinciaSigla, S2_ID2Nazione, Telefono1, 
    Telefono2, Fax,WebSite,RecordNewUser, RecordNewDate, RecordEdtUser, RecordEdtDate ,ID2BusinessBook
    FROM tbProfiliSocietari" UpdateCommand=" SET DATEFORMAT dmy; UPDATE tbProfilisocietari 
    SET RagioneSociale=@RagioneSociale, PartitaIVA=@PartitaIVA, 
    S1_Indirizzo=@S1_Indirizzo, S1_Numero=@S1_Numero, S1_Citta=@S1_Citta, 
    S1_Comune=@S1_Comune, S1_Cap=@S1_Cap, S1_ProvinciaEstesa=@S1_ProvinciaEstesa, 
    S1_ProvinciaSigla=@S1_ProvinciaSigla, S1_ID2Nazione=@S1_ID2Nazione,S2_Indirizzo=@S2_Indirizzo, 
    S2_Numero=@S2_Numero, S2_Citta=@S2_Citta, S2_Comune=@S2_Comune, S2_Cap=@S2_Cap, 
    S2_ProvinciaEstesa=@S2_ProvinciaEstesa, S2_ProvinciaSigla=@S2_ProvinciaSigla, S2_ID2Nazione=@S2_ID2Nazione, 
    Telefono1=@Telefono1, Telefono2=@Telefono2, Fax=@Fax,WebSite=@WebSite, RecordEdtUser=@RecordEdtUser, 
    RecordEdtDate=@RecordEdtDate, ID2BusinessBook=@ID2BusinessBook WHERE [UserID] =@UserID"
    OnUpdated="ProfiloSocietarioSqlDataSource_Updated">--%>
<asp:SqlDataSource ID="ProfiloSocietarioSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
    SelectCommand="SELECT RagioneSociale, S1_Indirizzo, S1_Numero, S1_Citta, S1_Comune, 
    S1_Cap, S1_ProvinciaEstesa, S1_ProvinciaSigla, S1_ID2Nazione,S2_Indirizzo, S2_Numero, 
    S2_Citta, S2_Comune, S2_Cap, S2_ProvinciaEstesa, S2_ProvinciaSigla, S2_ID2Nazione, 
    RecordNewUser, RecordNewDate, RecordEdtUser, RecordEdtDate ,ID2BusinessBook
    FROM tbProfiliSocietari" UpdateCommand=" SET DATEFORMAT dmy; UPDATE tbProfilisocietari 
    SET RagioneSociale=@RagioneSociale, 
    S1_Indirizzo=@S1_Indirizzo, S1_Numero=@S1_Numero, S1_Citta=@S1_Citta, 
    S1_Comune=@S1_Comune, S1_Cap=@S1_Cap, S1_ProvinciaEstesa=@S1_ProvinciaEstesa, 
    S1_ProvinciaSigla=@S1_ProvinciaSigla, S1_ID2Nazione=@S1_ID2Nazione, 
    RecordEdtUser=@RecordEdtUser, RecordEdtDate=@RecordEdtDate, ID2BusinessBook=@ID2BusinessBook WHERE [UserID] =@UserID"
    OnUpdated="ProfiloSocietarioSqlDataSource_Updated">
    <UpdateParameters>
        <asp:Parameter Name="RagioneSociale" />
        <%--<asp:Parameter Name="PartitaIVA" />--%>
        <asp:Parameter Name="S1_Indirizzo" />
        <asp:Parameter Name="S1_Numero" />
        <asp:Parameter Name="S1_Citta" />
        <asp:Parameter Name="S1_Comune" />
        <asp:Parameter Name="S1_Cap" />
        <asp:Parameter Name="S1_ProvinciaEstesa" />
        <asp:Parameter Name="S1_ProvinciaSigla" />
        <asp:Parameter Name="S1_ID2Nazione" />
<%--        <asp:Parameter Name="S2_Indirizzo" />
        <asp:Parameter Name="S2_Numero" />
        <asp:Parameter Name="S2_Citta" />
        <asp:Parameter Name="S2_Comune" />
        <asp:Parameter Name="S2_Cap" />
        <asp:Parameter Name="S2_ProvinciaEstesa" />
        <asp:Parameter Name="S2_ProvinciaSigla" />
        <asp:Parameter Name="S2_ID2Nazione" />--%>
<%--        <asp:Parameter Name="Telefono1" />
        <asp:Parameter Name="Telefono2" />
        <asp:Parameter Name="Fax" />
        <asp:Parameter Name="Website" />--%>
        <asp:Parameter Name="ID2BusinessBook" />
        <asp:Parameter Name="RecordEdtUser" />
        <asp:Parameter Name="RecordEdtDate" Type="DateTime" />
        <asp:ControlParameter ControlID="UIDHiddenField" Name="UserID" />
    </UpdateParameters>
</asp:SqlDataSource>
