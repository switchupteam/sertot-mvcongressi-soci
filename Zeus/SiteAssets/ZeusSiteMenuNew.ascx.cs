﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Zeus_SiteAssets_ZeusSiteMenuNew : System.Web.UI.UserControl
{
    private string connectionString = ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        // This is necessary because Safari and Chrome browsers don't display the Menu control correctly.
        if (Request.ServerVariables["http_user_agent"].IndexOf("Safari", StringComparison.CurrentCultureIgnoreCase) != -1)
            Page.ClientTarget = "uplevel";
    }

    protected override void AddedControl(Control control, int index)
    {
        // This is necessary because Safari and Chrome browsers don't display the Menu control correctly.
        // Add this to the code in your master page.
        if (Request.ServerVariables["http_user_agent"].IndexOf("Safari", StringComparison.CurrentCultureIgnoreCase) != -1)
            this.Page.ClientTarget = "uplevel";
        base.AddedControl(control, index);
    }

    protected void Page_Init()
    {
        //Repeater MenuLv1_Repeater = (Repeater)Page.FindControl("MenuLv1_Repeater");
        MenuLv1_Repeater.DataSource = GetVociMenu();
        MenuLv1_Repeater.DataBind();

        //Nascondo se non ci sono video
        if (MenuLv1_Repeater.Items.Count == 0)
            MenuLv1_Repeater.Visible = false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {


    }

    protected List<VoceMenuZeus> GetVociMenu(int livello = 1, int idParent = 0)
    {
        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            List<VoceMenuZeus> listaMenu = new List<VoceMenuZeus>();

            try
            {
                connection.Open();

                string sqlQuery = @"SELECT ID1Menu, Menu, Url, Roles 
                                    FROM vwSiteMenu_All 
                                    WHERE ZeusIdModulo = @ZeusIdModulo AND Livello = @Livello 
                                        AND ID2MenuParent = @ID2MenuParent AND AttivoArea1 = 1 
                                    ORDER BY Ordinamento";

                SqlCommand command = connection.CreateCommand();
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ZeusIdModulo", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ZeusIdModulo"].Value = "SMNZS";
                command.Parameters.Add("@Livello", System.Data.SqlDbType.Int);
                command.Parameters["@Livello"].Value = livello;
                command.Parameters.Add("@ID2MenuParent", System.Data.SqlDbType.Int);
                command.Parameters["@ID2MenuParent"].Value = idParent;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (IsInRoles(reader.GetString(3)))
                    {
                        listaMenu.Add(new VoceMenuZeus()
                        {
                            Id = Convert.ToInt32(reader["ID1Menu"]),
                            Titolo = reader["Menu"].ToString(),
                            Url = reader["Url"].ToString()
                        });
                    }
                }

                reader.Close();

                return listaMenu;
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
            }
        }

        return null;
    }

    private string[] GetAllZeusAdminRoles()
    {
        string[] AllZeusAdminRoles = null;

        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                string sqlQuery = @"SELECT RoleName
                                    FROM tbZeusRoles
                                    WHERE IsAdminRole = 1";
                command.CommandText = sqlQuery;


                ArrayList myArray = new ArrayList();

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                    if (reader["RoleName"] != DBNull.Value)
                        myArray.Add(reader["RoleName"].ToString());

                reader.Close();

                AllZeusAdminRoles = (string[])myArray.ToArray(typeof(string));

            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
            }
        }

        return AllZeusAdminRoles;
    }

    private bool IsInRoles(string AllRoles)
    {
        try
        {
            if (AllRoles.Length == 0)
                return Roles.IsUserInRole("ZeusAdmin");


            string[] myAllZeusAdminRoles = GetAllZeusAdminRoles();


            for (int ii = 0; ii < myAllZeusAdminRoles.Length; ++ii)
                if (Roles.IsUserInRole(myAllZeusAdminRoles[ii]))
                    return true;


            char[] mySeparator = { ',' };
            AllRoles = AllRoles.Remove(AllRoles.Length - 1);

            string[] myRoles = AllRoles.Split(mySeparator);

            for (int ii = 0; ii < myRoles.Length; ++ii)
            {
                //  Response.Write("DEB roles:" + myRoles[i] + "" + Roles.IsUserInRole(myRoles[i]) + "<br />");
                if ((myRoles[ii].Length > 0) && (Roles.IsUserInRole(myRoles[ii])))
                    return true;
            }


        }
        catch (Exception p)
        {
            //Response.Write(p.ToString());
        }

        return false;

    }

    protected void MenuLv1_Repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater MenuLv2_Repeater = (Repeater)e.Item.FindControl("MenuLv2_Repeater");
            MenuLv2_Repeater.DataSource = GetVociMenu(2, Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Id")));
            MenuLv2_Repeater.DataBind();

            //Nascondo se non ci sono video
            if (MenuLv2_Repeater.Items.Count == 0)
                e.Item.Visible = false;
        }
    }

    protected void MenuLv2_Repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater MenuLv3_Repeater = (Repeater)e.Item.FindControl("MenuLv3_Repeater");
            MenuLv3_Repeater.DataSource = GetVociMenu(3, Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Id")));
            MenuLv3_Repeater.DataBind();

            //Nascondo se non ci sono video
            if (MenuLv3_Repeater.Items.Count == 0)
            {
                e.Item.Visible = false;
                e.Item.FindControl("freccia").Visible = false;
            }
        }
    }
}

public class VoceMenuZeus
{
    public int Id { get; set; }
    public string Titolo { get; set; }
    public string Url { get; set; }

    public VoceMenuZeus()
    {

    }
}