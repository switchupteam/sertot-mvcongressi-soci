﻿<%@ Control Language="C#" ClassName="dlAllegato" %>
<%@ Import Namespace="System.IO" %>

<script runat="server">


    private bool pGotFile;
    private string pFileExt;
    private string pFileName;
    private HttpPostedFile pFilePost;
    private bool pRequired;
    private string pVgroup;

    private string[] pFileTypeRange;
    private bool Ind = false;

    private int minWidth = 0;
    private int minHeight = 0;
    private int maxWidth = 0;
    private int maxHeight = 0;
    private int minSize = 0;
    private int maxSize = 0;


    private string sizeError = string.Empty;
    private string phDimensionError = string.Empty;
    private string formatError = string.Empty;
    private string requiredError = string.Empty;
    private string savePath = string.Empty;
    private bool showInfo = true;



    //Get function
    public bool GotFile
    {
        get { return pGotFile; }
    }
    public string FileExt
    {
        get { return pFileExt; }
    }
    public string FileName
    {
        get { return pFileName; }
        set { pFileName = value; }
    }
    public HttpPostedFile FilePost
    {
        get { return pFilePost; }
    }

    public string SavePath
    {
        set { savePath = value; }
        get { return savePath; }
    }


    public bool ShowInfo
    {
        set { showInfo = value; }
        get { return showInfo; }
    }


    //Set function

    public bool Required
    {
        set { pRequired = value; }
    }
    public string Vgroup
    {
        set { pVgroup = value; }
    }
    public string FileTypeRange
    {
        set { pFileTypeRange = value.ToString().Split(','); }
    }
    public int MinWidthPx
    {
        set { minWidth = value; }
    }

    public int MinHeightPx
    {
        set { minHeight = value; }
    }

    public int MaxWidthPx
    {
        set { maxWidth = value; }
    }

    public int MaxHeightPx
    {
        set { maxHeight = value; }
    }

    public int MaxSizeKb
    {
        set { maxSize = value; }
    }

    public int MinSizeKb
    {
        set { minSize = value; }
    }

    public string SizeErrorMessage
    {
        set { sizeError = value; }
    }

    public string PhDimensionErrorMessage
    {
        set { phDimensionError = value; }
    }

    public string FormatErrorMessage
    {
        set { formatError = value; }
    }

    public string RequiredErrorMessage
    {
        set { requiredError = value; }
    }




    //              Page_Load
    protected void Page_Init()
    {
        //if ((!Page.IsPostBack)&&(!Page.IsCallback))
        //{
        try
        {
            ErrorMsg.ValidationGroup = pVgroup;
            MessagePanel.Visible = showInfo;

            if ((pFileTypeRange != null) && (pFileTypeRange.Length > 0))
            {
                Message1Label.Text = "Formati consentiti: ";
                foreach (string str in pFileTypeRange)
                    Message1Label.Text += str + ", ";

                Message1Label.Text = Message1Label.Text.Substring(0, Message1Label.Text.Length - 2);
            }
            else Message1Label.Text = "Formati consentiti: tutti";

            if((maxWidth>0)||(maxHeight>0))
                Message2Label.Text = "Dimensione: " + maxWidth + "x" + maxHeight + " pixel - 72dpi";
            else Message2Label.Text = "Dimensione: qualsiasi";
            
            if(maxSize>0)
                Message3Label.Text = "Peso massimo: " + maxSize + "Kb";
            else Message3Label.Visible = false;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
        //}

    }//fine Page_Load


    public void myDataBind()
    {
        try
        {
            ErrorMsg.ValidationGroup = pVgroup;
            MessagePanel.Visible = showInfo;
            Message1Label.Text = "Formati consentiti: ";

            if (pFileTypeRange != null)
                foreach (string str in pFileTypeRange)
                    Message1Label.Text += str + ", ";

            Message1Label.Text = Message1Label.Text.Substring(0, Message1Label.Text.Length - 2);
            Message2Label.Text = "Dimensione: " + maxWidth + "x" + maxHeight + " pixel - 72dpi";
            Message3Label.Text = "Peso massimo: " + maxSize + "Kb";
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine DataBind


    private string CheckFileName(string filePath, string fileName)
    {

        int counter = 1;
        string finalPath = filePath;
        string strFile_name = System.IO.Path.GetFileNameWithoutExtension(fileName);
        string strFile_extension = System.IO.Path.GetExtension(fileName);

        if (System.IO.File.Exists(filePath + fileName))
        {

            // il file è già sul server
            filePath += fileName;
            while (System.IO.File.Exists(filePath))
            {
                counter++;
                filePath = System.IO.Path.Combine(finalPath, strFile_name + counter.ToString() + strFile_extension);

            }//fine while


            fileName = strFile_name + counter.ToString() + strFile_extension;


        }
        //else fileName =strFile_name;

        return fileName;

    }


    public bool SaveFile()
    {
        try
        {
            if (Page.IsValid)
            {
                if (this.GotFile)
                {
                    string filePath = System.Web.HttpContext.Current.Server.MapPath(this.savePath);
                    pFileName = CheckFileName(filePath, this.FileName);
                    //salviamo il file sul server
                    this.FilePost.SaveAs(filePath + pFileName);
                }
                return true;
            }
            return false;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }//fine SaveFile

    public string SaveFileAndGetName()
    {
        string Name = string.Empty;
        
        try
        {
            if (Page.IsValid)
            {
                if (this.GotFile)
                {
                    string filePath = System.Web.HttpContext.Current.Server.MapPath(this.savePath);
                    pFileName = CheckFileName(filePath, this.FileName);
                    //salviamo il file sul server
                    this.FilePost.SaveAs(filePath + pFileName);
                }
                
            }
            
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            
        }

        return Name;
        
    }//fine SaveFile

    public bool SaveFile(string fileName)
    {
        try
        {
            if (Page.IsValid)
            {
                if (this.GotFile)
                {
                    string filePath = System.Web.HttpContext.Current.Server.MapPath(this.savePath);

                    if (File.Exists(filePath + fileName))
                        File.Delete(filePath + fileName);

                    this.FilePost.SaveAs(filePath + fileName);

                }
                return true;
            }
            return false;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }//fine SaveFile



    protected void ErrorMsg_ServerValidate(object source, ServerValidateEventArgs args)
    {
        try
        {

            if (FilUpl.HasFile)
            {
                pGotFile = true;
                pFileExt = GetExtension(FilUpl.PostedFile.FileName);
                pFilePost = FilUpl.PostedFile;
                pFileName = FilUpl.FileName;

                if (pFileTypeRange != null)
                {
                    foreach (string str in pFileTypeRange)
                    {
                        if (str.ToLower().Equals(pFileExt.ToLower()))
                        {
                            Ind = true;
                            break;
                        }
                    }
                }
                else Ind = true;

                if (!Ind)
                {
                    args.IsValid = false;
                    if (formatError.Length == 0)
                        ErrorMsg.Text = "Formato del file non consentito";
                    else ErrorMsg.Text = formatError;
                    return;
                }

                System.Drawing.Image CheckSize = null;

                try
                {
                    CheckSize = System.Drawing.Image.FromStream(FilUpl.PostedFile.InputStream);
                }
                catch (Exception) { }

                if (CheckSize != null)
                {
                    if (minWidth > 0)
                    {

                        if (CheckSize.PhysicalDimension.Width < minWidth)
                        {
                            args.IsValid = false;
                            if (phDimensionError.Length == 0)
                                ErrorMsg.Text = "Dimensione immagine non corretta";
                            else ErrorMsg.Text = phDimensionError;
                            return;
                        }

                    }
                    if (minHeight > 0)
                    {

                        if (CheckSize.PhysicalDimension.Height < minHeight)
                        {
                            args.IsValid = false;
                            if (phDimensionError.Length == 0)
                                ErrorMsg.Text = "Dimensione immagine non corretta";
                            else ErrorMsg.Text = phDimensionError;
                            return;
                        }
                    }
                    if (maxWidth > 0)
                    {

                        if (CheckSize.PhysicalDimension.Width > maxWidth)
                        {
                            args.IsValid = false;
                            if (phDimensionError.Length == 0)
                                ErrorMsg.Text = "Dimensione immagine non corretta";
                            else ErrorMsg.Text = phDimensionError;
                            return;
                        }
                    }
                    if (maxHeight > 0)
                    {

                        if (CheckSize.PhysicalDimension.Height > maxHeight)
                        {
                            args.IsValid = false;
                            if (phDimensionError.Length == 0)
                                ErrorMsg.Text = "Dimensione immagine non corretta";
                            else ErrorMsg.Text = phDimensionError;
                            return;
                        }
                    }
                }//fine if CheckSize!=null
                if (minSize > 0)
                {

                    if (FilUpl.PostedFile.ContentLength < (minSize * 1024))
                    {
                        args.IsValid = false;
                        if (sizeError.Length == 0)
                            ErrorMsg.Text = "Il file non supera il peso minimo";
                        else ErrorMsg.Text = sizeError;
                        return;
                    }
                }
                if (maxSize > 0)
                {

                    if (FilUpl.PostedFile.ContentLength > (maxSize * 1024))
                    {
                        args.IsValid = false;
                        if (sizeError.Length == 0)
                            ErrorMsg.Text = "Il file supera il peso massimo";
                        else ErrorMsg.Text = sizeError;
                        return;
                    }
                }
            }
            else
            {

                pGotFile = false;
                if (pRequired)
                {
                    args.IsValid = false;
                    if (requiredError.Length == 0)
                        ErrorMsg.Text = "Attenzione il file upload e' vuoto";
                    else ErrorMsg.Text = requiredError;
                }
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }//fine ErrorMsg_ServerValidate


    private string GetExtension(string FileName)
    {
        string[] split = FileName.Split('.');
        string Extension = split[split.Length - 1];
        return Extension;
    }


    
</script>

<asp:FileUpload ID="FilUpl" runat="server" />
<asp:CustomValidator ID="ErrorMsg" runat="server" ErrorMessage="CustomValidator"
    OnServerValidate="ErrorMsg_ServerValidate" CssClass="Warning" Display="Dynamic"></asp:CustomValidator>
<asp:Panel ID="MessagePanel" runat="server">
    <br />
    <asp:Label ID="Message1Label" runat="server"></asp:Label><br />
    <asp:Label ID="Message2Label" runat="server"></asp:Label><br />
    <asp:Label ID="Message3Label" runat="server"></asp:Label><br />
    <br />
</asp:Panel>
