<%@ Control Language="C#" ClassName="dlImageAutoResize" %>
<%@ Import Namespace="System.IO" %>

<script runat="server">


    private string pFileName;
    private string pThumbFileName;
    private string[] pFileTypeRange;
    private int minWidth = 0;
    private int minHeight = 0;
    private int maxWidth = 0;
    private int maxHeight = 0;
    private int minSize = 0;
    private int maxSize = 0;
    private float dpi = 0;

    private int quality = 100;
    private int qualityTmb = 100;
    private int compressionThresholdKb = 100;
    private int compressionThresholdKbTmb = 100;
    private int widthTmb = 0;
    private int heightTmb = 0;
    private int widthFinal = 0;
    private int heightFinal = 0;


    private bool crop = false;
    private bool pRequired;
    private string pVgroup;

    private string formatError = string.Empty;
    private string requiredError = string.Empty;
    private string phDimensionError = string.Empty;

    private string savePath = string.Empty;
    private string saveThumbPath = string.Empty;
    private bool showInfo = true;

    private string defaultImage = string.Empty;
    private string infoMessage1 = string.Empty;
    private string infoMessage2 = string.Empty;
    private string infoMessage3 = string.Empty;

    static string TempPath = "~/ZeusInc/Temp/";


    public int CompressionThresholdKbTmb
    {
        set { compressionThresholdKbTmb = value; }
        get { return compressionThresholdKbTmb; }
    }


    public int CompressionThresholdKb 
    {
        set { compressionThresholdKb = value; }
        get { return compressionThresholdKb; }
    }

    public int QualityTmb 
    {
        set { qualityTmb = value; }
        get { return qualityTmb; }
    
    }
    
    public int Quality
    {
        set { quality = value; }
        get { return quality; }
    }


    public int WidthFinal
    {
        set { widthFinal = value; }
        get { return widthFinal; }
    }


    public int HeightFinal
    {
        set { heightFinal = value; }
        get { return heightFinal; }
    }


    public bool Crop
    {
        set { crop = value; }
        get { return crop; }

    }

    public int HeightTmb
    {
        set { heightTmb = value; }
        get { return heightTmb; }
    }


    public int WidthTmb
    {
        set { widthTmb = value; }
        get { return widthTmb; }
    }

    public string FormatErrorMessage
    {
        set { formatError = value; }
    }

    public string RequiredErrorMessage
    {
        set { requiredError = value; }
    }


    public string PhDimensionErrorMessage
    {
        set { phDimensionError = value; }
    }

    public float Dpi
    {
        set { dpi = value; }
        get { return dpi; }
    }



    public string InfoMessage1
    {
        set { infoMessage1 = value; }
        get { return infoMessage1; }
    }

    public string InfoMessage2
    {
        set { infoMessage2 = value; }
        get { return infoMessage2; }
    }

    public string InfoMessage3
    {
        set { infoMessage3 = value; }
        get { return infoMessage3; }
    }


    public string DefaultImage
    {
        set { defaultImage = value; }
        get { return defaultImage; }
    }


    public bool Required
    {
        set { pRequired = value; }
    }
    public string Vgroup
    {
        set { pVgroup = value; }
    }

    public string FileName
    {
        get { return pFileName; }
    }


    public string ThumbFileName
    {
        get { return pThumbFileName; }
    }


    public string SavePath
    {
        set { savePath = value; }
        get { return savePath; }
    }


    public string SaveThumbPath
    {
        set { saveThumbPath = value; }
        get { return saveThumbPath; }

    }


    public bool ShowInfo
    {
        set { showInfo = value; }
        get { return showInfo; }
    }

    public string FileTypeRange
    {
        set { pFileTypeRange = value.ToString().Split(','); }
    }
    public int MinWidthPx
    {
        set { minWidth = value; }
    }

    public int MinHeightPx
    {
        set { minHeight = value; }
    }

    public int MaxWidthPx
    {
        set { maxWidth = value; }
    }

    public int MaxHeightPx
    {
        set { maxHeight = value; }
    }

    public int MaxSizeKb
    {
        set { maxSize = value; }
    }

    public int MinSizeKb
    {
        set { minSize = value; }
    }

    //_________________________________________________________________________________________________________________//

    protected void Page_Init()
    {
        try
        {
            ErrorMsg.ValidationGroup = pVgroup;
            FileNameTriggedLabel.Text = string.Empty;

            SetDefaultImage();


            MessagePanel.Visible = showInfo;
            Message1Label.Text = "Formati consentiti: ";
            if (pFileTypeRange != null)
                foreach (string str in pFileTypeRange)
                    Message1Label.Text += str + ", ";

            Message1Label.Text = Message1Label.Text.Substring(0, Message1Label.Text.Length - 2);
            Message2Label.Text = "Dimensione: " + maxWidth + "x" + maxHeight + " pixel - 72dpi";
            //Message3Label.Text = "Peso massimo: " + maxSize + "Kb";
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
        //}

    }//fine Page_Init


    public bool isValid()
    {
        return Page.IsValid;

    }//fine isValid

    public void SetDefaultImage()
    {
        try
        {
            if (defaultImage.Length > 0)
            {
                UploadImage.ImageUrl = defaultImage;
                FileInfo myFile = new FileInfo(Server.MapPath(defaultImage));
                System.Drawing.Image myImage = System.Drawing.Image.FromFile(Server.MapPath(defaultImage));
                InfoLabel.Text = myImage.PhysicalDimension.Width + "x" + myImage.PhysicalDimension.Height + " / " + myFile.Length + " bytes";
                InfoLabel.Visible = true;
                myImage.Dispose();
            }
            else 
            {
                UploadImage.Visible = false;
                InfoLabel.Visible = false;
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine SetDefaultImage


    public void SetInfoMessage()
    {
        Message1Label.Text = infoMessage1;
        Message2Label.Text = infoMessage2;
        Message3Label.Text = infoMessage3;

    }//fine SetInfoMessage

    public void SetInfoMessageAuto()
    {
        Message1Label.Text = "Formati consentiti: ";
        if (pFileTypeRange != null)
            foreach (string str in pFileTypeRange)
                Message1Label.Text += str + ", ";

        Message1Label.Text.Remove(Message1Label.Text.Length - 1, 1);

        if (maxWidth != minWidth)
        {
            Message2Label.Text = " Larghezza: min " + minWidth.ToString();
            Message2Label.Text += " max " + maxWidth.ToString();
        }
        else Message2Label.Text = "Larghezza: " + maxWidth.ToString();
        Message2Label.Text += " pixel - 72dpi ";

        if (maxHeight != minHeight)
        {
            Message2Label.Text += " <br /> Altezza: min " + minHeight.ToString();
            Message2Label.Text += " max " + maxHeight.ToString();
        }
        else Message2Label.Text += " <br /> Altezza: " + maxHeight.ToString();

        Message2Label.Text += " pixel - " + dpi.ToString() + "dpi";
        //Message3Label.Text = "Peso massimo: " + maxSize.ToString() + " Kb";

    }//fine SetInfoMessageAuto


    private string GetExtension(string FileName)
    {
        string[] split = FileName.Split('.');
        string Extension = split[split.Length - 1];
        return Extension;

    }//fine GetExtension


    private string CheckFileName(string filePath, string fileName)
    {

        int counter = 1;
        string finalPath = filePath;
        string strFile_name = System.IO.Path.GetFileNameWithoutExtension(fileName);
        string strFile_extension = System.IO.Path.GetExtension(fileName);

        if (System.IO.File.Exists(filePath + fileName))
        {

            // il file � gi� sul server
            filePath += fileName;
            while (System.IO.File.Exists(filePath))
            {
                counter++;
                filePath = System.IO.Path.Combine(finalPath, strFile_name + counter.ToString() + strFile_extension);

            }//fine while


            fileName = strFile_name + counter.ToString() + strFile_extension;


        }
        //else fileName =strFile_name;

        return fileName;

    }//fine CheckFileName


    private bool ThumbnailCallback()
    {
        return false;
    }

    
    private System.Drawing.Imaging.ImageCodecInfo GetEncoderInfo(string mimeType)
    {
        System.Drawing.Imaging.ImageCodecInfo[] encoders = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders();
        for (int j = 0; j < encoders.Length; j++)
            if (encoders[j].MimeType == mimeType)
                return encoders[j];

        return null;
    }//fine GetEncoderInfo



    private string GetMimeType(string Ext) 
    {
        string myMimeType = "image/jpeg";

        switch (Ext.ToLower()) 
        { 
            case "jpeg":
                myMimeType = "image/jpeg";
                break;

            case "jpg":
                myMimeType = "image/jpeg";
                break;

            case "gif":
                myMimeType = "image/gif";
                break;
        
        }//fine switch

        return myMimeType;

    }//fine GetMimeType


    private static System.Drawing.Imaging.ImageFormat GetImageFormat(string Format)
    {

        switch (Format.ToLower())
        {
            case ".jpg":
                return System.Drawing.Imaging.ImageFormat.Jpeg;
                break;

            case ".jpeg":
                return System.Drawing.Imaging.ImageFormat.Jpeg;
                break;

            case ".gif":
                return System.Drawing.Imaging.ImageFormat.Gif;
                break;

            default:
                return System.Drawing.Imaging.ImageFormat.Jpeg;
                break;

        }//fine switch



    }// fine GetImageFormat

    
    public bool ResizeAndSave(string Path)
    {
        try
        {
            if (dpi == 0)
                return false;


            if (heightFinal == 0)
                return false;

            if (widthFinal == 0)
                return false;

            if (FilUpl.HasFile)
            {
                string FileName = string.Empty;


                FileName = CheckFileName(Server.MapPath(Path), FilUpl.FileName);



                System.Drawing.Image normalImage = System.Drawing.Image.FromStream(FilUpl.FileContent);
                System.Drawing.Bitmap myBitmap;

                if ((normalImage.Width <= widthFinal) && (normalImage.Height <= heightFinal))
                    myBitmap = new System.Drawing.Bitmap(FilUpl.FileContent);
                else
                {

                    int Height = 0;
                    int Width = 0;


                    if (normalImage.Height > normalImage.Width)
                    {
                        Height = heightFinal;
                        Width = (widthFinal * normalImage.Width) / normalImage.Height;
                    }
                    else
                    {
                        Width = widthFinal;
                        Height = (heightFinal * normalImage.Height) / normalImage.Width;
                    }

                    myBitmap = new System.Drawing.Bitmap(normalImage.GetThumbnailImage(Width, Height, ThumbnailCallback, IntPtr.Zero));
                }


                myBitmap.SetResolution(dpi, dpi);

                

                if (FilUpl.PostedFile.ContentLength > compressionThresholdKb * (1024))
                {
                    System.Drawing.Imaging.EncoderParameters eps = new System.Drawing.Imaging.EncoderParameters(1);
                    eps.Param[0] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
                    System.Drawing.Imaging.ImageCodecInfo ici = GetEncoderInfo(GetMimeType(GetExtension(FileName)));
                    myBitmap.Save(Server.MapPath(Path + FileName), ici, eps);
                }
                else
                    myBitmap.Save(Server.MapPath(Path + FileName), GetImageFormat(System.IO.Path.GetExtension(FileName)));

                FileInfo myFile = new FileInfo(Server.MapPath(Path + FileName));
                FileNameTriggedLabel.Text = FileName;
                pFileName = FileName;
                UploadImage.ImageUrl = Path + FileName;
                InfoLabel.Text = myBitmap.PhysicalDimension.Width + "x" + myBitmap.PhysicalDimension.Height + " / " + myFile.Length + " bytes";
                InfoLabel.Visible = true;
                normalImage.Dispose();
                myBitmap.Dispose();
                return true;

            }
            else if (FileNameTriggedLabel.Text.Length > 0)
            {
                string filePath = System.Web.HttpContext.Current.Server.MapPath(this.savePath);
                pFileName = CheckFileName(filePath, FileNameTriggedLabel.Text);
                File.Copy(Server.MapPath(TempPath + FileNameTriggedLabel.Text), filePath + pFileName);
                File.Delete(Server.MapPath(TempPath + FileNameTriggedLabel.Text));
                return true; 
            }

           
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

        return false;
    }//fine ResizeAndSave 


    public bool SaveThumbImage(string name, string SrcPath, string FinalPath)
    {

        if ((heightTmb == 0) || (widthTmb == 0))
            return false;

        if (name.Length > 0)
        {
            int Height = 0;
            int Width = 0;

            try
            {
                System.Drawing.Image normalImage = System.Drawing.Image.FromFile(System.Web.HttpContext.Current.Server.MapPath(SrcPath + name));
                System.Drawing.Bitmap myBitmap;
                
                if (!crop)
                {
                    

                    if (normalImage.Height > normalImage.Width)
                    {
                        Height = heightTmb;
                        Width = (widthTmb * normalImage.Width) / normalImage.Height;
                    }
                    else
                    {
                        Width = widthTmb;
                        Height = (heightTmb * normalImage.Height) / normalImage.Width;
                    }


                    myBitmap = new System.Drawing.Bitmap(normalImage.GetThumbnailImage(Width, Height, ThumbnailCallback, IntPtr.Zero));
                    
                }
                else
                {
                    
                    //CALCOLO PER IL CROP AL CONTRARIO RISPETTO A SOPRA
                    if (normalImage.Height > normalImage.Width)
                    {
                        Width = widthTmb;
                        Height = (heightTmb * normalImage.Height) / normalImage.Width;
                    }
                    else
                    {
                        Height = heightTmb;
                        Width = (widthTmb * normalImage.Width) / normalImage.Height;
                    }

                    myBitmap = new System.Drawing.Bitmap(normalImage.GetThumbnailImage(Width, Height, ThumbnailCallback, IntPtr.Zero));

                    double Difference = Math.Abs(Width - Height) / 2;

                    if (Width > Height)
                        myBitmap = CropBitmap(myBitmap, Convert.ToInt32(Difference), 0, widthTmb, heightTmb);
                    else myBitmap = CropBitmap(myBitmap, 0, Convert.ToInt32(Difference), widthTmb, heightTmb);

                }//fine else

                myBitmap.SetResolution(dpi, dpi);
               
                FileInfo myFile = new FileInfo(System.Web.HttpContext.Current.Server.MapPath(SrcPath + name));
                pThumbFileName = CheckFileName(Server.MapPath(FinalPath), name);

                if (myFile.Length > compressionThresholdKbTmb * (1024))
                {
                    System.Drawing.Imaging.EncoderParameters eps = new System.Drawing.Imaging.EncoderParameters(1);
                    eps.Param[0] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, qualityTmb);
                    System.Drawing.Imaging.ImageCodecInfo ici = GetEncoderInfo(GetMimeType(GetExtension(FileName)));
                    myBitmap.Save(Server.MapPath(FinalPath + pThumbFileName), ici, eps);
                }
                else
                    myBitmap.Save(Server.MapPath(FinalPath + pThumbFileName), GetImageFormat(System.IO.Path.GetExtension(name)));

                
                
                normalImage.Dispose();
                myBitmap.Dispose();


                return true;
            }
            catch (Exception p)
            {
                System.Web.HttpContext.Current.Response.Write(p.ToString());
                return false;
            }
        }
        else return false;
    }//fine saveThumbImage



    private System.Drawing.Bitmap CropBitmap(System.Drawing.Bitmap bitmap, int cropX, int cropY, int cropWidth, int cropHeight)
    {
        System.Drawing.Rectangle rect = new System.Drawing.Rectangle(cropX, cropY, cropWidth, cropHeight);
        System.Drawing.Bitmap cropped = bitmap.Clone(rect, bitmap.PixelFormat);
        return cropped;
    }//fine CropBitmap

    protected void myServerValidate(object source, ServerValidateEventArgs args)
    {
        try
        {

            CustomValidator myValidator = (CustomValidator)source;

            if (FilUpl.HasFile)
            {

                
                //VALIDAZIONE FORMATI
                string pFileExt = GetExtension(FilUpl.PostedFile.FileName);
                bool Ind = false;

                if (pFileTypeRange != null)
                {
                    foreach (string str in pFileTypeRange)
                    {
                        if (str.ToLower().Equals(pFileExt.ToLower()))
                        {
                            Ind = true;
                            break;
                        }
                    }
                }
                else Ind = true;

                if (!Ind)
                {
                    args.IsValid = false;
                    if (formatError.Length == 0)
                        myValidator.Text = "Formato del file non consentito";
                    else myValidator.Text = formatError;
                    return;
                }

                
                
                
                //VALIDAZIONI DIMENSIONI IMMAGINE MIN E MAX
                System.Drawing.Image CheckSize = null;

                try
                {
                    CheckSize = System.Drawing.Image.FromStream(FilUpl.PostedFile.InputStream);
                }
                catch (Exception) { }

                if (CheckSize != null)
                {
                    if (minWidth > 0)
                    {

                        if (CheckSize.PhysicalDimension.Width < minWidth)
                        {
                            args.IsValid = false;
                            if (phDimensionError.Length == 0)
                                LoadButtonCustomValidator.Text = "Dimensione immagine non corretta";
                            else LoadButtonCustomValidator.Text = phDimensionError;
                            return;
                        }

                    }
                    if (minHeight > 0)
                    {

                        if (CheckSize.PhysicalDimension.Height < minHeight)
                        {
                            args.IsValid = false;
                            if (phDimensionError.Length == 0)
                                LoadButtonCustomValidator.Text = "Dimensione immagine non corretta";
                            else LoadButtonCustomValidator.Text = phDimensionError;
                            return;
                        }
                    }
                    if (maxWidth > 0)
                    {

                        if (CheckSize.PhysicalDimension.Width > maxWidth)
                        {
                            args.IsValid = false;
                            if (phDimensionError.Length == 0)
                                LoadButtonCustomValidator.Text = "Dimensione immagine non corretta";
                            else LoadButtonCustomValidator.Text = phDimensionError;
                            return;
                        }
                    }
                    if (maxHeight > 0)
                    {

                        if (CheckSize.PhysicalDimension.Height > maxHeight)
                        {
                            args.IsValid = false;
                            if (phDimensionError.Length == 0)
                                LoadButtonCustomValidator.Text = "Dimensione immagine non corretta";
                            else LoadButtonCustomValidator.Text = phDimensionError;
                            return;
                        }
                    }
                }//fine if CheckSize!=null
                
            }//fine if
            else
            {

                //VALIDAZIONE REQUIRED
                if ((pRequired) && (FileNameTriggedLabel.Text.Length <= 0))
                {
                    args.IsValid = false;
                    if (requiredError.Length == 0)
                        myValidator.Text = "Attenzione il file upload e' vuoto";
                    else myValidator.Text = requiredError;
                }

            }//fine else
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }//fine myServerValidate



    public bool ClearTempPath()
    {

        try
        {

            if (TempPath.Length == 0)
                return false;

            string[] Files = Directory.GetFiles(Server.MapPath( TempPath));


            for (int i = 0; i < Files.Length; ++i)
            {

                FileInfo myFileInfo = new FileInfo(Files[i]);

                if (DateTime.Compare(myFileInfo.CreationTime, DateTime.Now.AddDays(-7)) <= 0)
                {
                    //Response.Write("DEB Clear:" + Files[i] + "<br />");
                    //Response.Write("    Date:" + myFileInfo.CreationTime + "<br />");
                    File.Delete(Files[i]);
                }

            }//fine for

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }//fine ClearTempPath
    
    

    protected void LoadButton_Click(object sender, EventArgs e)
    {
        if ((Page.IsValid) && (FilUpl.HasFile))
        {
            if(ResizeAndSave(TempPath))
                UploadImage.Visible = true;

        }//fine if

    }//fine LoadButton_Click


    public void ShowImage()
    {
        UploadImage.Visible = true;
    }


    public string GetTrigged()
    {
        return FileNameTriggedLabel.Text;
    }
    
</script>

<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="conditional">
    <Triggers>
        <asp:PostBackTrigger ControlID="LoadButton" />
    </Triggers>
    <ContentTemplate>
        <asp:Label ID="FileNameTriggedLabel" runat="server" Visible="false"></asp:Label>
        <table border="0" cellspacing="0" cellpadding="2">
            <tr>
                <td>
                    <asp:Image ID="UploadImage" runat="server" /><br />
                    <asp:Label ID="InfoLabel" runat="server" Visible="false" SkinID="FileUploadNotes"></asp:Label>
                </td>
            </tr>
        </table>
        <table border="0" cellspacing="0" cellpadding="2">
            <tr>
                <td>
                   <%-- <asp:Label ID="CambiaImmagineLabel" SkinID="FileUploadNotes" runat="server" Text="Cambia immagine"></asp:Label>
                    <br />
                   --%> <asp:FileUpload ID="FilUpl" runat="server" />
                    <asp:LinkButton ID="LoadButton" runat="server" Text="Anteprima" OnClick="LoadButton_Click"
                        ValidationGroup="myLoadButtonValidationGroup" SkinID="ZSSM_Button01" />
                    <asp:CustomValidator ID="ErrorMsg" runat="server" ErrorMessage="CustomValidator"
                        OnServerValidate="myServerValidate" SkinID="ZSSM_Validazione01" Display="Dynamic"></asp:CustomValidator>
                    <asp:CustomValidator ID="LoadButtonCustomValidator" runat="server" ErrorMessage="CustomValidator"
                        OnServerValidate="myServerValidate" SkinID="ZSSM_Validazione01" Display="Dynamic"
                        ValidationGroup="myLoadButtonValidationGroup"></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="MessagePanel" runat="server">
                        <asp:Label ID="Message1Label" runat="server" SkinID="FileUploadNotes"></asp:Label><br />
                        <asp:Label ID="Message2Label" runat="server" SkinID="FileUploadNotes"></asp:Label><br />
                        <asp:Label ID="Message3Label" runat="server" SkinID="FileUploadNotes"></asp:Label>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
