<%@ Control Language="C#" ClassName="ZeusDataTRacking" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">

    delinea myDelinea = new delinea();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            HiddenField zdtRecordNewUser = (HiddenField)myDelinea.FindControlRecursive(Page, "zdtRecordNewUser");
            HiddenField zdtRecordNewDate = (HiddenField)myDelinea.FindControlRecursive(Page, "zdtRecordNewDate");
            HiddenField zdtRecordEdtUser = (HiddenField)myDelinea.FindControlRecursive(Page, "zdtRecordEdtUser");
            HiddenField zdtRecordEdtDate = (HiddenField)myDelinea.FindControlRecursive(Page, "zdtRecordEdtDate");

            LabelDataCreazione.Text = zdtRecordNewDate.Value.ToString();
            
            if (zdtRecordEdtDate.Value.Length > 0)
                LabelUltimoAggiornamento.Text = zdtRecordEdtDate.Value.ToString();
            else LabelUltimoAggiornamento.Text = "Nessuna modifica";

            if (zdtRecordNewUser.Value.Length > 0)
            {
                SqlDataSourceUtenteNew.SelectCommand += " WHERE ([UserID] ='" + zdtRecordNewUser.Value.ToString() + "')";
                SqlDataSourceUtenteNew.DataBind();
            }
            else SqlDataSourceUtenteNew.SelectCommand = string.Empty;

            if (zdtRecordEdtUser.Value.Length > 0)
            {
                SqlDataSourceUtenteEdt.SelectCommand += " WHERE ([UserID] ='" + zdtRecordEdtUser.Value.ToString() + "')";
                SqlDataSourceUtenteEdt.DataBind();
            }
            else SqlDataSourceUtenteEdt.SelectCommand = string.Empty;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine Page_Load

</script>

<table border="0" cellpadding="2" cellspacing="1" class="BlockBoxDataTracking" style="width: 100%">
    <tr>
        <td colspan="4" class="BlockBoxHeader">
            <asp:Label ID="DataTrackingLabel" runat="server" Text="Data Tracking"></asp:Label>
        </td>
    </tr>
    <tr>
        <td align="center">
            <asp:Label ID="Label1" SkinID="FieldDescription" runat="server">Data creazione:</asp:Label>
            <asp:Label ID="LabelDataCreazione" SkinID="FieldValue" runat="server"></asp:Label>
        </td>
        <td align="center">
            <asp:FormView ID="FormView1" runat="server" DataSourceID="SqlDataSourceUtenteNew">
                <ItemTemplate>
                    <asp:Label ID="Label2" SkinID="FieldDescription" runat="server">Utente:</asp:Label>
                    <asp:HyperLink CssClass="DataTrackingLabel" SkinID="FieldValue" ID="HyperLink1" runat="server"
                        NavigateUrl='<%# Eval("UserID", "~/Zeus/Account/Account_Dtl.aspx?UID={0}") %>'
                        Text='<%# Eval("Identificativo1", "{0}") %>'></asp:HyperLink><br />
                </ItemTemplate>
            </asp:FormView>
        </td>
        <td align="center">
            <asp:Label ID="Label3" SkinID="FieldDescription" runat="server">Ultimo aggiornamento:</asp:Label>
            <asp:Label ID="LabelUltimoAggiornamento" SkinID="FieldValue" runat="server"></asp:Label>
        </td>
        <td align="center">
            <asp:FormView ID="FormView2" runat="server" 
                DataSourceID="SqlDataSourceUtenteEdt">
                <ItemTemplate>
                    <asp:Label ID="Label4" SkinID="FieldDescription" runat="server">Utente:</asp:Label>
                    <asp:HyperLink ID="HyperLink2" SkinID="FieldValue" runat="server" NavigateUrl='<%# Eval("UserID", "~/Zeus/Account/Account_Dtl.aspx?UID={0}") %>' Text='<%# Eval("Identificativo1", "{0}") %>'></asp:HyperLink></ItemTemplate></asp:FormView></td></tr></table>
<asp:SqlDataSource ID="SqlDataSourceUtenteNew" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
    SelectCommand="SELECT [UserID], [UserName], [Identificativo1] FROM [vwAccount_Lst_All]">
</asp:SqlDataSource>
<asp:SqlDataSource ID="SqlDataSourceUtenteEdt" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
    SelectCommand="SELECT [UserID], [UserName], [Identificativo1] FROM [vwAccount_Lst_All] ">
</asp:SqlDataSource>
