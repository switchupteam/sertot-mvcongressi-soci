﻿<%@ Control Language="C#" ClassName="ProfiloMarketing_Dtl" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">

    private string UserID = string.Empty;

    public string UID
    {
        set { UserID = value; }
        get { return UserID; }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindTheData();
        }

    }//fine PageLoad



    private string GetLang()
    {
        string Lang = "ITA";

        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;

    }//fine GetLang



    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("~/Zeus/Account/ZML_Profili.xml"));


            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {

                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                            if (myLabel != null)
                                if (childNode.InnerText != string.Empty)
                                myLabel.Text = childNode.InnerText;

                        }
                        catch (Exception)
                        {

                        }
                    }//fine foreach
                }//fine foreach

            }//fine for
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine ReadXML_Localization



    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_Profili.xml"));

            Panel ZMCF_Categoria1 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Categoria1");
            
            if (ZMCF_Categoria1 != null)
                ZMCF_Categoria1.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Categoria1").InnerText);

            Panel ZMCF_ConsensoDatiPersonali = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_ConsensoDatiPersonali");
            
            if (ZMCF_ConsensoDatiPersonali != null)
                ZMCF_ConsensoDatiPersonali.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_ConsensoDatiPersonali").InnerText);


            Panel ZMCF_ConsensoDatiPersonali2 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_ConsensoDatiPersonali2");

            if (ZMCF_ConsensoDatiPersonali2 != null)
                ZMCF_ConsensoDatiPersonali2.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_ConsensoDatiPersonali2").InnerText);


            Panel ZMCF_Comunicazioni1 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Comunicazioni1");

            if (ZMCF_Comunicazioni1 != null)
                ZMCF_Comunicazioni1.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni1").InnerText);


            Panel ZMCF_Comunicazioni2 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Comunicazioni2");

            if (ZMCF_Comunicazioni2 != null)
                ZMCF_Comunicazioni2.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni2").InnerText);


            Panel ZMCF_Comunicazioni3 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Comunicazioni3");
            
            if (ZMCF_Comunicazioni3 != null)
                ZMCF_Comunicazioni3.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni3").InnerText);

            Panel ZMCF_Comunicazioni4 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Comunicazioni4");

            if (ZMCF_Comunicazioni4 != null)
                ZMCF_Comunicazioni4.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni4").InnerText);

            Panel ZMCF_Comunicazioni5 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Comunicazioni5");

            if (ZMCF_Comunicazioni5 != null)
                ZMCF_Comunicazioni5.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni5").InnerText);


            Panel ZMCF_Comunicazioni6 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Comunicazioni6");
            
            if (ZMCF_Comunicazioni6 != null)
                ZMCF_Comunicazioni6.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni6").InnerText);


            Panel ZMCF_Comunicazioni7 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Comunicazioni7");

            if (ZMCF_Comunicazioni7 != null)
                ZMCF_Comunicazioni7.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni7").InnerText);



            Panel ZMCF_Comunicazioni8 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Comunicazioni8");
            
            if (ZMCF_Comunicazioni8 != null)
                ZMCF_Comunicazioni8.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni8").InnerText);



            Panel ZMCF_Comunicazioni9 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Comunicazioni9");
            
            if (ZMCF_Comunicazioni9 != null)
                ZMCF_Comunicazioni9.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni9").InnerText);


            Panel ZMCF_Comunicazioni10 = (Panel)ProfiloMarketingFormView.FindControl("ZMCF_Comunicazioni10");
            
            if (ZMCF_Comunicazioni10 != null)
                ZMCF_Comunicazioni10.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Comunicazioni10").InnerText);


            SetQueryOfID2CategoriaDropDownList(mydoc.SelectSingleNode(myXPath + "ZMCD_ZIMCategoria1").InnerText
                          , GetLang(), mydoc.SelectSingleNode(myXPath + "ZMCD_Cat1Liv").InnerText);

            return true;
        }
        catch (Exception)
        {
            return false;
        }

    }//fine ReadXML


    

   


    //##############################################################################################################
    //################################################ CATEGORIE ###################################################
    //############################################################################################################## 


    private bool SetQueryOfID2CategoriaDropDownList(string ZeusIdModulo,
       string ZeusLangCode, string PZV_Cat1Liv)
    {
        try
        {
            //Response.Write("DEB ZeusIdModulo :" + ZeusIdModulo+"<br />");
            //Response.Write("DEB ZeusLangCode: " + ZeusLangCode + "<br />");
            //Response.Write("DEB PZV_Cat1Liv: " + PZV_Cat1Liv + "<br />");


            if (ZeusIdModulo.Length == 0)
                return false;

            if (ZeusLangCode.Length == 0)
                return false;

            if (PZV_Cat1Liv.Length == 0)
                return false;

            SqlDataSource dsCategoria = (SqlDataSource)ProfiloMarketingFormView.FindControl("dsCategoria");
            DropDownList ID2CategoriaDropDownList = (DropDownList)ProfiloMarketingFormView.FindControl("ID2CategoriaDropDownList");
            HiddenField ID2CategoriaHiddenField = (HiddenField)ProfiloMarketingFormView.FindControl("ID2CategoriaHiddenField");
            Label CategoriaLabel = (Label)ProfiloMarketingFormView.FindControl("CategoriaLabel");
            //ID2CategoriaHiddenField.Value = "0";

            if ((ID2CategoriaHiddenField != null) &&
                (ID2CategoriaHiddenField.Value.Equals("0")))
            {
                CategoriaLabel.Text = "Non inserita";
                return true;
            }

            switch (PZV_Cat1Liv)
            {

                case "123":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv1Liv2Liv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "Catliv1Liv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;


                case "23":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv2Liv3]";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;

                default:
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;
            }

            ID2CategoriaDropDownList.SelectedIndex = ID2CategoriaDropDownList.Items.IndexOf(ID2CategoriaDropDownList.Items.FindByValue(ID2CategoriaHiddenField.Value));


            CategoriaLabel.Text = ID2CategoriaDropDownList.SelectedItem.Text;

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }//fine SetQueryOfID2CategoriaDropDownList




    //##############################################################################################################
    //############################################ FINE CATEGORIE ##################################################
    //############################################################################################################## 


    private string GetCausalHumanReadble(string RegCasual)
    {

        if (RegCasual.Length == 0)
            return string.Empty;

        string CausalHumanReadble = string.Empty;

        try
        {

            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("~/App_Data/RegistrationCausal.xml"));
            CausalHumanReadble = mydoc.SelectSingleNode("RootCasual/RegDescription[@RegCausal='" + RegCasual + "']").InnerText;

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

        return CausalHumanReadble;

    }//fine GetCausalHumanReadble
    
    
    public bool BindTheData()
    {
        try
        {

            ProfiloMarketingSqlDataSource.SelectCommand += " WHERE ([UserID] = '" + UserID + "')";
            ProfiloMarketingFormView.DataSourceID = "ProfiloMarketingSqlDataSource";
            ProfiloMarketingSqlDataSource.DataBind();

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }


    }//fine BindTheData 



    protected void ProfiloMarketingFormView_DataBound(object sender, EventArgs e)
    {
        ReadXML("PROFILI", GetLang());
        ReadXML_Localization("PROFILI", GetLang());
    }
</script>

<asp:FormView ID="ProfiloMarketingFormView" runat="server" Width="100%" 
    ondatabound="ProfiloMarketingFormView_DataBound">
    <ItemTemplate>
        <div class="BlockBoxHeader">
            <asp:Label ID="ProfiloPersonaleLabel1" runat="server" Text="Preferenze"></asp:Label></div>
        <table>
            <asp:Panel ID="ZMCF_Categoria1" runat="server">
                <tr>
                    <td>
                        <asp:Label ID="ZML_Categoria1" SkinID="FieldDescription" runat="server" Text="Categoria:"></asp:Label>
                        <asp:Label ID="CategoriaLabel" runat="server" SkinID="FieldValue"></asp:Label>
                        <asp:DropDownList ID="ID2CategoriaDropDownList" runat="server" Visible="false" AppendDataBoundItems="True">
                            <asp:ListItem Text="> Seleziona" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:HiddenField ID="ID2CategoriaHiddenField" runat="server" Value='<%# Eval("ID2Categoria1") %>' />
                        <asp:SqlDataSource ID="dsCategoria" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>">
                        </asp:SqlDataSource>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="ZMCF_ConsensoDatiPersonali" runat="server">
                <tr>
                    <td class="BlockBoxValue" colspan="2">
                        <asp:CheckBox ID="CheckBoxDatiPersonali" runat="server" Checked='<%# Bind("ConsensoDatiPersonali") %>'
                            Enabled="false" />
                        <asp:Label SkinID="FieldValue" ID="ZML_ConsensoDatiPersonali" runat="server" Text="Default_ZMCF_ConsensoDatiPersonali"></asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="ZMCF_ConsensoDatiPersonali2" runat="server">
                <tr>
                    <td class="BlockBoxValue" colspan="2">
                        <asp:CheckBox ID="CheckBoxDatiPersonali2" runat="server" Checked='<%# Bind("ConsensoDatiPersonali2") %>'
                            Enabled="false" />
                        <asp:Label SkinID="FieldValue" ID="ZML_ConsensoDatiPersonali2" runat="server" Text="Default_ZMCF_ConsensoDatiPersonali2"></asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="ZMCF_Comunicazioni1" runat="server">
                <tr>
                    <td class="BlockBoxValue" colspan="2">
                        <asp:CheckBox ID="Comunicazioni1CheckBox" runat="server" Checked='<%# Bind("Comunicazioni1") %>'
                            Enabled="false" />
                        <asp:Label SkinID="FieldValue" ID="ZML_Comunicazioni1" runat="server" Text="Default_ZMCF_Comunicazioni1"></asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="ZMCF_Comunicazioni2" runat="server">
                <tr>
                    <td class="BlockBoxValue" colspan="2">
                        <asp:CheckBox ID="Comunicazioni2CheckBox" runat="server" Checked='<%# Bind("Comunicazioni2") %>'
                            Enabled="false" />
                        <asp:Label SkinID="FieldValue" ID="ZML_Comunicazioni2" runat="server" Text="Default_ZMCF_Comunicazioni2"></asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="ZMCF_Comunicazioni3" runat="server">
                <tr>
                    <td class="BlockBoxValue" colspan="2">
                        <asp:CheckBox ID="Comunicazioni3CheckBox" runat="server" Checked='<%# Bind("Comunicazioni3") %>'
                            Enabled="false" />
                        <asp:Label SkinID="FieldValue" ID="ZML_Comunicazioni3" runat="server" Text="Default_ZMCF_Comunicazioni3"></asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="ZMCF_Comunicazioni4" runat="server">
                <tr>
                    <td class="BlockBoxValue" colspan="2">
                        <asp:CheckBox ID="Comunicazioni4CheckBox" runat="server" Checked='<%# Bind("Comunicazioni4") %>'
                            Enabled="false" />
                        <asp:Label SkinID="FieldValue" ID="ZML_Comunicazioni4" runat="server" Text="Default_ZMCF_Comunicazioni4"></asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="ZMCF_Comunicazioni5" runat="server">
                <tr>
                    <td class="BlockBoxValue" colspan="2">
                        <asp:CheckBox ID="Comunicazioni5CheckBox" runat="server" Checked='<%# Bind("Comunicazioni5") %>'
                            Enabled="false" />
                        <asp:Label SkinID="FieldValue" ID="ZML_Comunicazioni5" runat="server" Text="Default_ZMCF_Comunicazioni5"></asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="ZMCF_Comunicazioni6" runat="server">
                <tr>
                    <td class="BlockBoxValue" colspan="2">
                        <asp:CheckBox ID="Comunicazioni6CheckBox" runat="server" Checked='<%# Bind("Comunicazioni6") %>'
                            Enabled="false" />
                        <asp:Label SkinID="FieldValue" ID="ZML_Comunicazioni6" runat="server" Text="Default_ZMCF_Comunicazioni6"></asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="ZMCF_Comunicazioni7" runat="server">
                <tr>
                    <td class="BlockBoxValue" colspan="2">
                        <asp:CheckBox ID="Comunicazioni7CheckBox" runat="server" Checked='<%# Bind("Comunicazioni7") %>'
                            Enabled="false" />
                        <asp:Label SkinID="FieldValue" ID="ZML_Comunicazioni7" runat="server" Text="Default_ZMCF_Comunicazioni7"></asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="ZMCF_Comunicazioni8" runat="server">
                <tr>
                    <td class="BlockBoxValue" colspan="2">
                        <asp:CheckBox ID="Comunicazioni8CheckBox" runat="server" Checked='<%# Bind("Comunicazioni8") %>'
                            Enabled="false" />
                        <asp:Label SkinID="FieldValue" ID="ZML_Comunicazioni8" runat="server" Text="Default_ZMCF_Comunicazioni8"></asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="ZMCF_Comunicazioni9" runat="server">
                <tr>
                    <td class="BlockBoxValue" colspan="2">
                        <asp:CheckBox ID="Comunicazioni9CheckBox" runat="server" Checked='<%# Bind("Comunicazioni9") %>'
                            Enabled="false" />
                        <asp:Label SkinID="FieldValue" ID="ZML_Comunicazioni9" runat="server" Text="Default_ZMCF_Comunicazioni9"></asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="ZMCF_Comunicazioni10" runat="server">
                <tr>
                    <td class="BlockBoxValue" colspan="2">
                        <asp:CheckBox ID="Comunicazioni10CheckBox" runat="server" Checked='<%# Bind("Comunicazioni10") %>'
                            Enabled="false" />
                        <asp:Label SkinID="FieldValue" ID="ZML_Comunicazioni10" runat="server" Text="Default_ZMCF_Comunicazioni10"></asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <tr>
                <td class="BlockBoxValue" colspan="2">
                    <asp:Label ID="ProfessioneDescriptionLabel" SkinID="FieldDescription" runat="server">Causale registrazione:</asp:Label>
                    <asp:Label SkinID="FieldValue" ID="CausaleLabel" runat="server" Text='<%# GetCausalHumanReadble(Eval("RegCausal").ToString()) %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="BlockBoxValue" colspan="2">
                    <asp:Label ID="Label1" SkinID="FieldDescription" runat="server">Tag ricerca utenti Zeus:</asp:Label>
                    <asp:Label SkinID="FieldValue" ID="Label2" runat="server" Text='<%# Bind("Meta_ZeusSearch") %>'></asp:Label>
                </td>
            </tr>

        </table>
    </ItemTemplate>
</asp:FormView>
<asp:SqlDataSource ID="ProfiloMarketingSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
    SelectCommand="SELECT  ConsensoDatiPersonali,ConsensoDatiPersonali2, 
     Comunicazioni1,Comunicazioni2,Comunicazioni3,Comunicazioni4,Comunicazioni5
     ,Comunicazioni6,Comunicazioni7,Comunicazioni8,Comunicazioni9,Comunicazioni10,ID2Categoria1,RegCausal,Meta_ZeusSearch
    FROM tbProfiliMarketing"></asp:SqlDataSource>
