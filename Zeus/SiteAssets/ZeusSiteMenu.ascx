﻿<%@ Control Language="C#" ClassName="SiteMenu02" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">

    
    protected void Page_PreInit(object sender, EventArgs e)
    {
        // This is necessary because Safari and Chrome browsers don't display the Menu control correctly.
        if (Request.ServerVariables["http_user_agent"].IndexOf("Safari", StringComparison.CurrentCultureIgnoreCase) != -1)
            Page.ClientTarget = "uplevel";
    }

    protected override void AddedControl(Control control, int index)
    {
        // This is necessary because Safari and Chrome browsers don't display the Menu control correctly.
        // Add this to the code in your master page.
        if (Request.ServerVariables["http_user_agent"].IndexOf("Safari", StringComparison.CurrentCultureIgnoreCase) != -1)
            this.Page.ClientTarget = "uplevel";
        base.AddedControl(control, index);
    }
    
    
    protected void Page_Init()
    {
        if (Request.UserAgent.Contains("AppleWebKit"))
            Request.Browser.Adapters.Clear();

        XmlDataSource1.CacheDuration = 0;
        XmlDataSource1.EnableCaching = false;
        XmlDataSource1.CacheExpirationPolicy = DataSourceCacheExpiry.Absolute;
        XmlDataSource1.Data = makeMenuFromSQL();
        Menu1.DataSourceID = "XmlDataSource1";
        Menu1.DataBind();
      
    }//fine Page_Init
    
    protected void Page_Load(object sender, EventArgs e)
    {

       
    }//fine Page_Load


    public void ReloadMenu()
    {
        Menu1.Items.Clear();
        XmlDataSource1.Data = makeMenuFromSQL();
        Menu1.DataSourceID = "XmlDataSource1";
        Menu1.DataBind();

    }//fine ReloadMenu


    private XmlElement PrintSubChild(string ID2MenuParent, XmlElement childNode, XmlDocument xmlDoc)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            try
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT ID1Menu, Menu, Url, Roles FROM vwSiteMenu_All WHERE (ZeusIdModulo = @ZeusIdModulo) AND (Livello = @Livello) AND ID2MenuParent=@ID2MenuParent AND (AttivoArea1=1) ORDER BY Ordinamento";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ZeusIdModulo", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ZeusIdModulo"].Value = "SMNZS";
                command.Parameters.Add("@Livello", System.Data.SqlDbType.Int);
                command.Parameters["@Livello"].Value = "3";
                command.Parameters.Add("@ID2MenuParent", System.Data.SqlDbType.Int);
                command.Parameters["@ID2MenuParent"].Value = ID2MenuParent;

                SqlDataReader reader = command.ExecuteReader();


                while (reader.Read())
                {
                    if (IsInRoles(reader.GetString(3)))
                    {
                        XmlElement childNode2 = xmlDoc.CreateElement("subchild");
                        childNode2.SetAttribute("title", reader.GetString(1));
                        childNode2.SetAttribute("description", reader.GetString(1));

                        if (!reader.IsDBNull(2))
                            childNode2.SetAttribute("url", reader.GetString(2));
                        else childNode2.SetAttribute("url", "~/Zeus/Home.aspx");

                       

                        childNode.AppendChild(childNode2);
                    }

                }//fine while


                reader.Close();

            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
            }
        }//fine Using

        return childNode;

    }//fine PrintSubChild





    private XmlElement PrintChild(string ID2MenuParent, XmlElement childNode, XmlDocument xmlDoc)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            try
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT ID1Menu, Menu, Url, Roles FROM vwSiteMenu_All WHERE (ZeusIdModulo = @ZeusIdModulo) AND (Livello = @Livello) AND ID2MenuParent=@ID2MenuParent AND (AttivoArea1=1) ORDER BY Ordinamento";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ZeusIdModulo", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ZeusIdModulo"].Value = "SMNZS";
                command.Parameters.Add("@Livello", System.Data.SqlDbType.Int);
                command.Parameters["@Livello"].Value = "2";
                command.Parameters.Add("@ID2MenuParent", System.Data.SqlDbType.Int);
                command.Parameters["@ID2MenuParent"].Value = ID2MenuParent;

                SqlDataReader reader = command.ExecuteReader();


                while (reader.Read())
                {
                    if (IsInRoles(reader.GetString(3)))
                    {
                        XmlElement childNode2 = xmlDoc.CreateElement("child");
                        childNode2.SetAttribute("title", reader.GetString(1));
                        childNode2.SetAttribute("description", reader.GetString(1));

                        if ((!reader.IsDBNull(2)) && (reader.GetString(2).Length > 0))
                            childNode2.SetAttribute("url", reader.GetString(2));
                        else childNode2.SetAttribute("url", string.Empty);

                     

                        childNode2 = PrintSubChild(reader.GetInt32(0).ToString(), childNode2, xmlDoc);
                        childNode.AppendChild(childNode2);
                    }

                }//fine while


                reader.Close();

            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
            }
        }//fine Using

        return childNode;

    }//fine PrintChild



    private XmlElement PrintParent(XmlElement childNode, XmlDocument xmlDoc)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            try
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT ID1Menu, Menu, Url, Roles FROM vwSiteMenu_All WHERE (ZeusIdModulo = @ZeusIdModulo) AND (Livello = @Livello) AND ID2MenuParent=@ID2MenuParent AND (AttivoArea1=1) ORDER BY Ordinamento";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ZeusIdModulo", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ZeusIdModulo"].Value = "SMNZS";
                command.Parameters.Add("@Livello", System.Data.SqlDbType.Int);
                command.Parameters["@Livello"].Value = "1";
                command.Parameters.Add("@ID2MenuParent", System.Data.SqlDbType.Int);
                command.Parameters["@ID2MenuParent"].Value = "0";

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (IsInRoles(reader.GetString(3)))
                    {
                        XmlElement childNode2 = xmlDoc.CreateElement("parent");
                        childNode2.SetAttribute("title", reader.GetString(1));
                        childNode2.SetAttribute("description", reader.GetString(1));

                        if ((!reader.IsDBNull(2))&&(reader.GetString(2).Length>0))
                            childNode2.SetAttribute("url", reader.GetString(2));
                        else childNode2.SetAttribute("url", string.Empty);

                        childNode2 = PrintChild(reader.GetInt32(0).ToString(), childNode2, xmlDoc);
                        childNode.AppendChild(childNode2);
                    }
                }//fine while

                reader.Close();

            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
            }
        }//fine Using

        return childNode;
    }//fine PrintParent




    private string[] GetAllZeusAdminRoles()
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;
        string[] AllZeusAdminRoles = null;
        
        
        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            try
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT [RoleName]";
                sqlQuery += " FROM [tbZeusRoles]";
                sqlQuery += " WHERE [IsAdminRole]=1";
                command.CommandText = sqlQuery;
               
                SqlDataReader reader = command.ExecuteReader();

                ArrayList myArray = new ArrayList();
                
                while (reader.Read())
                    if (reader["RoleName"] != DBNull.Value)
                        myArray.Add(reader["RoleName"].ToString());
                
                reader.Close();

                AllZeusAdminRoles = (string[])myArray.ToArray(typeof(string));

            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
            }
        }//fine Using

        return AllZeusAdminRoles;
    }//fine GetAllZeusAdminRoles


    private bool IsInRoles(string AllRoles)
    {
        try
        {
            //Response.Write("DEB "+AllRoles+"<br />");
            
            if (AllRoles.Length == 0)
                return Roles.IsUserInRole("ZeusAdmin");


            string[] myAllZeusAdminRoles = GetAllZeusAdminRoles();


            for (int i = 0; i < myAllZeusAdminRoles.Length; ++i)
                if (Roles.IsUserInRole(myAllZeusAdminRoles[i]))
                    return true;
            
            
            char[] mySeparator = { ',' };
            AllRoles = AllRoles.Remove(AllRoles.Length - 1);

            string[] myRoles = AllRoles.Split(mySeparator);

            for (int i = 0; i < myRoles.Length; ++i)
            {
              //  Response.Write("DEB roles:" + myRoles[i] + "" + Roles.IsUserInRole(myRoles[i]) + "<br />");
                if ((myRoles[i].Length>0)
                    &&(Roles.IsUserInRole(myRoles[i])))
                    return true;
            }


        }
        catch (Exception p)
        {
            //Response.Write(p.ToString());
        }

        return false;

    }//fine IsInRoles

    private string makeMenuFromSQL()
    {

        XmlDocument xmlDoc = new XmlDocument();
        XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", null);
        XmlElement rootNode = xmlDoc.CreateElement("zeus");
        xmlDoc.InsertBefore(xmlDeclaration, xmlDoc.DocumentElement);
        xmlDoc.AppendChild(rootNode);

        PrintParent(rootNode, xmlDoc);

        return xmlDoc.InnerXml;


    }//fine makeMenuFromSQL
    
    
</script>
<!--[if lte IE 7]>
<style type="text/css">
.Static02MenuStyle {display:inline;}
</style>
<![endif]--> 

<asp:Menu ID="Menu1" SkinId="SiteMenu02" runat="server" 
    StaticItemFormatString="{0}" StaticSubMenuIndent=""
    StaticDisplayLevels="1" Orientation="Horizontal" 
    DynamicHorizontalOffset="-1" DynamicEnableDefaultPopOutImage="false" 
    StaticEnableDefaultPopOutImage="false" DynamicPopOutImageTextFormatString="" DynamicPopOutImageUrl="~/Zeus/SiteImg/Zeus1/Menu_Arrow_Off.gif">
    <DataBindings>
        <asp:MenuItemBinding DataMember="parent" TextField="title" ToolTipField="description"
            NavigateUrlField="url"  Selectable="false" />
        <asp:MenuItemBinding DataMember="child" TextField="title" ToolTipField="description"
            NavigateUrlField="url" PopOutImageUrl="~/Zeus/SiteImg/Zeus1/Menu_Arrow_Off.gif" />
        <asp:MenuItemBinding DataMember="subchild" TextField="title" ToolTipField="description"
            NavigateUrlField="url" />
    </DataBindings>
</asp:Menu>
<asp:XmlDataSource ID="XmlDataSource1" runat="server" XPath="/zeus/parent"></asp:XmlDataSource>

