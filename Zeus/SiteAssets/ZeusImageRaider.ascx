<%@ Control Language="C#" ClassName="ImageRaider" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Data.SqlClient" %>


<script runat="server">
    /*##############################################################################################################*/
    /*########################################## PARAMETRI INTERNI #################################################*/
    /*##############################################################################################################*/


    //PARAMETRI COMPRESSIONE IMMAGINI
    private float dpi = 72;
    private int qualityTmb = 100;
    private int quality = 100;

    private bool crop = false;

    //PARAMETRI DI VALIDAZIONE DELLE IMMAGINI
    private bool pRequired;
    private string pVgroup;
    private string formatError = string.Empty;
    private string requiredError = string.Empty;
    private string phDimensionError = string.Empty;
    private string sizeError = string.Empty;

    //INFORMAZIONI IMMAGINI
    private bool showInfo = true;
    private string defaultBetaImage = string.Empty;
    private string defaultGammaImage = string.Empty;
    private string infoMessage1 = string.Empty;
    private string infoMessage2 = string.Empty;
    private string infoMessage3 = string.Empty;

    //INFORMAZIONI PER IL PZ
    private string myZeusIdModulo = string.Empty;
    private string myZeusIdModuloIndice = "1";
    private string myZeusLang = string.Empty;
    private bool myBindPzFromDB = false;
    private string myPZD_BkgColor = "#FFFFFF";


    //INFORMAZIONI PER LA FASE DI EDT (RELOAD IMMAGINE)
    private bool isEditMode = false;
    private string defaultEditBetaImage = string.Empty;
    private string defaultEditGammaImage = string.Empty;

    static string TempPathBeta = "~/ZeusInc/TempBeta/";
    static string TempPathGamma = "~/ZeusInc/TempGamma/";


    bool isEnabled = true;

    public bool Enabled
    {
        set { isEnabled = value; }
        get { return isEnabled; }
    }



    public bool IsEditMode
    {
        set { isEditMode = value; }
        get { return isEditMode; }
    }

    public string DefaultEditBetaImage
    {
        set { defaultEditBetaImage = value; }
        get { return defaultEditBetaImage; }

    }

    public string DefaultEditGammaImage
    {
        set { defaultEditGammaImage = value; }
        get { return defaultEditGammaImage; }

    }


    public bool BindPzFromDB
    {
        set { myBindPzFromDB = value; }
        get { return myBindPzFromDB; }

    }

    public string ZeusIdModuloIndice
    {
        set { myZeusIdModuloIndice = value; }
        get { return myZeusIdModuloIndice; }

    }

    public string ZeusLangCode
    {
        set { myZeusLang = value; }
        get { return myZeusLang; }

    }


    public bool Crop
    {
        set { crop = value; }
        get { return crop; }

    }

    public string FormatErrorMessage
    {
        set { formatError = value; }
    }

    public string RequiredErrorMessage
    {
        set { requiredError = value; }
    }

    public string PhDimensionErrorMessage
    {
        set { phDimensionError = value; }
    }

    public string SizeErrorMessage
    {
        set { sizeError = value; }
    }

    public string InfoMessage1
    {
        set { infoMessage1 = value; }
        get { return infoMessage1; }
    }

    public string InfoMessage2
    {
        set { infoMessage2 = value; }
        get { return infoMessage2; }
    }

    public string InfoMessage3
    {
        set { infoMessage3 = value; }
        get { return infoMessage3; }
    }


    public string DefaultBetaImage
    {
        set { defaultBetaImage = value; }
        get { return defaultBetaImage; }
    }

    public string DefaultGammaImage
    {
        set { defaultGammaImage = value; }
        get { return defaultGammaImage; }
    }


    public bool Required
    {
        set { pRequired = value; }
    }

    public string Vgroup
    {
        set { pVgroup = value; }
    }


    public bool ShowInfo
    {
        set { showInfo = value; }
        get { return showInfo; }
    }

    /*##############################################################################################################*/
    /*########################################## FINE PARAMETRI INTERNI ############################################*/
    /*##############################################################################################################*/

    /*--------------------------------------------------------------------------------------------------------------------------*/

    /*##############################################################################################################*/
    /*########################################## PARAMETRI IMMAGINE ALPHA ##########################################*/
    /*##############################################################################################################*/
    private string[] pFileTypeRange;
    private int minWidth = 0;
    private int minHeight = 0;
    private int maxWidth = 0;
    private int maxHeight = 0;
    private int minSize = 0;
    private int maxSize = 0;

    public string ImgAlpha_PermitExtensions
    {
        set { pFileTypeRange = value.ToString().Split(','); }
    }


    public int ImgAlpha_WidthMin
    {
        set { minWidth = value; }
    }

    public int ImgAlpha_HeightMin
    {
        set { minHeight = value; }
    }

    public int ImgAlpha_WidthMax
    {
        set { maxWidth = value; }
    }

    public int ImgAlpha_HeightMax
    {
        set { maxHeight = value; }
    }

    public int ImgAlpha_MaxSizeKb
    {
        set { maxSize = value; }
    }

    public int ImgAlpha_MinSizeKb
    {
        set { minSize = value; }
    }

    /*##############################################################################################################*/
    /*######################################FINE PARAMETRI IMMAGINE ALPHA ##########################################*/
    /*##############################################################################################################*/

    /*----------------------------------------------------------------------------------------------------------------------------*/

    /*##############################################################################################################*/
    /*########################################## PARAMETRI IMMAGINE BETA ##########################################*/
    /*##############################################################################################################*/
    private int compressionThresholdKb = 100;
    private int widthFinal = 0;
    private int heightFinal = 0;
    private string savePath = string.Empty;
    private int myImgBeta_RaidMode = 0;
    private string pFileName;

    public string ImgBeta_Path
    {
        set { savePath = value; }
        get { return savePath; }
    }

    public int ImgBeta_Width
    {
        set { widthFinal = value; }
        get { return widthFinal; }
    }


    public int ImgBeta_Height
    {
        set { heightFinal = value; }
        get { return heightFinal; }
    }


    public int ImgBeta_WeightMaxCompress
    {
        set { compressionThresholdKb = value; }
        get { return compressionThresholdKb; }
    }


    public string ImgBeta_FileName
    {
        get { return pFileName; }
        set { pFileName = value; }
    }

    public int ImgBeta_RaidMode
    {
        set { myImgBeta_RaidMode = value; }
        get { return myImgBeta_RaidMode; }
    }


    /*##############################################################################################################*/
    /*###################################### FINE PARAMETRI IMMAGINE BETA ##########################################*/
    /*##############################################################################################################*/

    /*----------------------------------------------------------------------------------------------------------------------------*/

    /*##############################################################################################################*/
    /*########################################## PARAMETRI IMMAGINE GAMMA #########################################*/
    /*##############################################################################################################*/
    private string saveThumbPath = string.Empty;
    private int compressionThresholdKbTmb = 100;
    private int widthTmbFinal = 0;
    private int heightTmbFinal = 0;
    private string pFileNameTmb;
    private int myImgGamma_RaidMode = 0;



    public int ImgGamma_WeightMaxCompress
    {
        set { compressionThresholdKbTmb = value; }
        get { return compressionThresholdKbTmb; }
    }


    public int ImgGamma_Width
    {
        set { heightTmbFinal = value; }
        get { return heightTmbFinal; }
    }


    public int ImgGamma_Height
    {
        set { widthTmbFinal = value; }
        get { return widthTmbFinal; }
    }


    public string ImgGamma_FileName
    {
        get { return pFileNameTmb; }
        set { pFileNameTmb = value; }
    }

    public string ImgGamma_Path
    {
        set { saveThumbPath = value; }
        get { return saveThumbPath; }

    }

    public int ImgGamma_RaidMode
    {
        set { myImgGamma_RaidMode = value; }
        get { return myImgGamma_RaidMode; }
    }


    /*##############################################################################################################*/
    /*###################################### FINE PARAMETRI IMMAGINE GAMMA #########################################*/
    /*##############################################################################################################*/


    /*##############################################################################################################*/
    /*##############################################################################################################*/
    /*##############################################################################################################*/
    /*##############################################################################################################*/
    /*##############################################################################################################*/

    protected void Page_Init()
    {

        DisablePageCaching();

        try
        {

            if ((Request.QueryString["ZIM"] != null)
            && (Request.QueryString["ZIM"].Length > 0))
            {

                myZeusIdModulo = Server.HtmlEncode(Request.QueryString["ZIM"]);

                if (myZeusIdModuloIndice.Length > 0)
                    myZeusIdModulo += myZeusIdModuloIndice;

            }//fine if


            if (BindPzFromDB)
                BindTheDataSQL();

            if ((ImgBeta_RaidMode == 0) && (ImgGamma_RaidMode == 0))
                ImageRaiderPanel.Enabled = false;

            ReloadButton.Visible = isEditMode;

            ErrorMsg.ValidationGroup = pVgroup;
            FileNameBetaTriggedLabel.Text = string.Empty;
            FileNameGammaTriggedLabel.Text = string.Empty;

            if (!IsEditMode)
            {
                SetDefaultGammaImage();
                SetDefaultBetaImage();
            }
            else
            {
                if (ImgBeta_RaidMode == 0)
                {
                    UploadBetaImage.Visible = false;
                    InfoBetaLabel.Visible = false;
                }

                if (ImgGamma_RaidMode == 0)
                {
                    UploadGammaImage.Visible = false;
                    InfoGammaLabel.Visible = false;
                }
            }//fine else

            SetInfoMessageBeta();
            SetInfoMessageGamma();


            SetInfoMessageAuto();

        }
        catch (Exception p)
        {
            //Response.Write(p.ToString());
        }
        //}

    }//fine Page_Init

    public static void DisablePageCaching()
    {

        //Used for disabling page caching
        HttpContext.Current.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
        HttpContext.Current.Response.Cache.SetValidUntilExpires(false);
        HttpContext.Current.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.Cache.SetNoStore();

    }


    protected void Page_Load(object sender, EventArgs e)
    {

        string firstScript = "/SiteJS/jquery-1.2.6.js";
        string secondScript = "/SiteJS/jQuery.BlockUI.js";

        HtmlGenericControl Include = new HtmlGenericControl("script");
        Include.Attributes.Add("type", "text/javascript");
        Include.Attributes.Add("src", firstScript);
        this.Page.Header.Controls.Add(Include);

        Include = new HtmlGenericControl("script");
        Include.Attributes.Add("type", "text/javascript");
        Include.Attributes.Add("src", secondScript);
        this.Page.Header.Controls.Add(Include);


        LoadButton.Attributes.Add("onfocus", "javascript:return SetIsLoadButtonToTrue();");


        LoadButton.Visible = ClearButton.Visible = FilUpl.Visible = isEnabled;

        if (isEditMode)
            ReloadButton.Visible = isEnabled;

    }//fine Page_Load



    private string GetFileNameFromPath(string Path)
    {
        string theFileName = string.Empty;

        try
        {

            int index = 0;
            int i = 0;

            foreach (char c in Path)
            {
                if (c.Equals('/'))
                    index = i;

                ++i;
            }

            if (index > 0)
                theFileName = Path.Substring(index + 1);


        }
        catch (Exception p)
        {
            //Response.Write(p.ToString());
        }

        return theFileName;

    }//fine GetFileNameFromPath


    private bool BindTheDataSQL()
    {

        try
        {

            if (myZeusIdModulo.Length == 0)
                return false;

            if (myZeusLang.Length == 0)
                return false;

            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            string sqlQuery = "SELECT PZL_BoxDescription, PZL_ComponentDescription, ImgPreview_BetaDefault, ImgPreview_GammaDefault, ImgAlpha_PermitExtensions,";
            sqlQuery += " ImgAlpha_WidthMin, ImgAlpha_WidthMax, ImgAlpha_HeightMin, ImgAlpha_HeightMax, ImgAlpha_WeightMax, ImgBeta_RaidMode, ImgBeta_Path,";
            sqlQuery += " ImgBeta_Width, ImgBeta_Height, ImgBeta_WeightMaxCompress, ImgGamma_RaidMode, ImgGamma_Path, ImgGamma_Width, ImgGamma_Height,";
            sqlQuery += " ImgGamma_WeightMaxCompress";
            sqlQuery += " FROM tbPZ_ImageRaider";
            sqlQuery += " WHERE ZeusIdModuloIndice = '" + myZeusIdModulo + "' AND ZeusLangCode='" + myZeusLang + "'";
            //////Response.Write(sqlQuery);

            using (SqlConnection conn = new SqlConnection(strConnessione))
            {
                SqlCommand command = new SqlCommand(sqlQuery, conn);

                conn.Open();
                SqlDataReader reader = command.ExecuteReader();
                /*fino a quando ci sono record*/

                if (!reader.HasRows)
                {
                    reader.Close();
                    return false;
                }


                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                        BoxDescriptionLabel.Text = reader.GetString(0);

                    if (!reader.IsDBNull(1))
                        ComponentDescriptionLabel.Text = reader.GetString(1);

                    if (!reader.IsDBNull(2))
                    {
                        DefaultBetaImage = reader.GetString(2);
                        pFileName = GetFileNameFromPath(reader.GetString(2));
                    }
                    if (!reader.IsDBNull(3))
                    {
                        DefaultGammaImage = reader.GetString(3);
                        pFileNameTmb = GetFileNameFromPath(reader.GetString(3));
                    }
                    if (!reader.IsDBNull(4))
                        ImgAlpha_PermitExtensions = reader.GetString(4);

                    if (!reader.IsDBNull(5))
                        ImgAlpha_WidthMin = reader.GetInt32(5);

                    if (!reader.IsDBNull(6))
                        ImgAlpha_WidthMax = reader.GetInt32(6);

                    if (!reader.IsDBNull(7))
                        ImgAlpha_HeightMin = reader.GetInt32(7);

                    if (!reader.IsDBNull(8))
                        ImgAlpha_HeightMax = reader.GetInt32(8);
                    // ImgAlpha_MinSizeKb = 1;

                    if (!reader.IsDBNull(9))
                        ImgAlpha_MaxSizeKb = reader.GetInt32(9);

                    if (!reader.IsDBNull(10))
                        ImgBeta_RaidMode = reader.GetInt32(10);

                    if (!reader.IsDBNull(11))
                        ImgBeta_Path = reader.GetString(11);

                    if (!reader.IsDBNull(12))
                        ImgBeta_Width = reader.GetInt32(12);

                    if (!reader.IsDBNull(13))
                        ImgBeta_Height = reader.GetInt32(13);

                    if (!reader.IsDBNull(14))
                        ImgBeta_WeightMaxCompress = reader.GetInt32(14);

                    if (!reader.IsDBNull(15))
                        ImgGamma_RaidMode = reader.GetInt32(15);

                    if (!reader.IsDBNull(16))
                        ImgGamma_Path = reader.GetString(16);

                    if (!reader.IsDBNull(17))
                        ImgGamma_Width = reader.GetInt32(17);

                    if (!reader.IsDBNull(18))
                        ImgGamma_Height = reader.GetInt32(18);

                    if (!reader.IsDBNull(19))
                        ImgGamma_WeightMaxCompress = reader.GetInt32(19);


                }//fine while
                reader.Close();
                conn.Close();
            }
            return true;
        }
        catch (Exception p)
        {
            //Response.Write(p.ToString());
            return false;
        }

    }//fine BindTheDataSQL


    public bool isValid()
    {
        return Page.IsValid;

    }//fine isValid

    public void SetDefaultBetaImage()
    {
        try
        {
            if ((defaultBetaImage.Length > 0) && (ImgBeta_RaidMode > 0))
            {
                UploadBetaImage.ImageUrl = defaultBetaImage;
                FileInfo myFile = new FileInfo(Server.MapPath(defaultBetaImage));
                System.Drawing.Image myImage = System.Drawing.Image.FromFile(Server.MapPath(defaultBetaImage));
                InfoBetaLabel.Text = myImage.PhysicalDimension.Width + "x" + myImage.PhysicalDimension.Height + " pixel, " + (double)(myFile.Length / 1024) + " KB";
                InfoBetaLabel.Visible = true;
                myImage.Dispose();

            }
            else
            {
                UploadBetaImage.Visible = false;
                InfoBetaLabel.Visible = false;

            }
        }
        catch (Exception p)
        {
            //Response.Write(p.ToString());
        }

    }//fine SetDefaultImage



    public void SetDefaultGammaImage()
    {
        try
        {
            if ((defaultGammaImage.Length > 0) && (ImgGamma_RaidMode > 0))
            {
                UploadGammaImage.ImageUrl = defaultGammaImage;
                FileInfo myFile = new FileInfo(Server.MapPath(defaultGammaImage));
                System.Drawing.Image myImage = System.Drawing.Image.FromFile(Server.MapPath(defaultGammaImage));
                InfoGammaLabel.Text = myImage.PhysicalDimension.Width + "x" + myImage.PhysicalDimension.Height + " pixel " + (double)(myFile.Length / 1024) + " KB";
                InfoGammaLabel.Visible = true;
                myImage.Dispose();
            }
            else
            {
                UploadGammaImage.Visible = false;
                InfoGammaLabel.Visible = false;
            }
        }
        catch (Exception p)
        {
            //Response.Write(p.ToString());
        }

    }//fine SetDefaultImage




    public void SetDefaultEditBetaImage()
    {
        try
        {

            if (ImgBeta_RaidMode > 0)
            {
                if (defaultEditBetaImage.Length <= 0)
                    defaultEditBetaImage = FileNameEditBeta.Value;

                UploadBetaImage.ImageUrl = ImgBeta_Path + defaultEditBetaImage;
                FileInfo myFile = new FileInfo(Server.MapPath(ImgBeta_Path + defaultEditBetaImage));
                System.Drawing.Image myImage = System.Drawing.Image.FromFile(Server.MapPath(ImgBeta_Path + defaultEditBetaImage));
                InfoBetaLabel.Text = myImage.PhysicalDimension.Width + "x" + myImage.PhysicalDimension.Height + " pixe, " + (double)(myFile.Length / 1024) + " KB";
                InfoBetaLabel.Visible = true;
                myImage.Dispose();
                FileNameEditBeta.Value = FileNameBetaTriggedLabel.Text = defaultEditBetaImage;


            }
            else
            {
                UploadBetaImage.Visible = false;
                InfoBetaLabel.Visible = false;

            }
        }
        catch (Exception p)
        {
            //Response.Write(p.ToString());
        }

    }//fine SetDefaultEditBetaImage


    public void SetDefaultEditGammaImage()
    {
        try
        {
            if (ImgGamma_RaidMode > 0)
            {

                if (defaultEditGammaImage.Length <= 0)
                    defaultEditGammaImage = FileNameEditGamma.Value;

                UploadGammaImage.ImageUrl = ImgGamma_Path + defaultEditGammaImage;
                FileInfo myFile = new FileInfo(Server.MapPath(ImgGamma_Path + defaultEditGammaImage));
                System.Drawing.Image myImage = System.Drawing.Image.FromFile(Server.MapPath(ImgGamma_Path + defaultEditGammaImage));
                InfoGammaLabel.Text = myImage.PhysicalDimension.Width + "x" + myImage.PhysicalDimension.Height + " pixel, " + (double)(myFile.Length / 1024) + " KB";
                InfoGammaLabel.Visible = true;
                myImage.Dispose();
                FileNameEditGamma.Value = FileNameGammaTriggedLabel.Text = defaultEditGammaImage;

            }
            else
            {
                UploadGammaImage.Visible = false;
                InfoGammaLabel.Visible = false;

            }
        }
        catch (Exception p)
        {
            //Response.Write(p.ToString());
        }

    }//fine SetDefaultEditGammaImage


    public void SetInfoMessage()
    {
        Message1Label.Text = infoMessage1;
        Message2Label.Text = infoMessage2;
        Message3Label.Text = infoMessage3;

    }//fine SetInfoMessage

    public void SetInfoMessageAuto()
    {
        Message1Label.Text = "Formati grafici consentiti: ";
        if (pFileTypeRange != null)
            foreach (string str in pFileTypeRange)
                Message1Label.Text += str + ", ";

        Message1Label.Text.Remove(Message1Label.Text.Length - 1, 1);

        if (maxWidth != minWidth)
        {
            Message2Label.Text = " Larghezza: min " + minWidth.ToString();
            Message2Label.Text += " max " + maxWidth.ToString();
        }
        else Message2Label.Text = "Larghezza: " + maxWidth.ToString();
        Message2Label.Text += " pixel";

        if (maxHeight != minHeight)
        {
            Message2Label.Text += " <br /> Altezza: min " + minHeight.ToString();
            Message2Label.Text += " max " + maxHeight.ToString();
        }
        else Message2Label.Text += " <br /> Altezza: " + maxHeight.ToString();
        Message2Label.Text += " pixel";

        Message3Label.Text = "Peso massimo: " + maxSize.ToString() + " KB";

    }//fine SetInfoMessageAuto



    private void SetInfoMessageBeta()
    {
        if ((ImgBeta_RaidMode != 0) && (ImgBeta_RaidMode != 1))
        {
            MessageBetaLabel.Text = "Dimensioni finali:";

            if (ImgBeta_RaidMode == 2)
                MessageBetaLabel.Text += " massimo";

            MessageBetaLabel.Text += " " + ImgBeta_Width + "x" + ImgBeta_Height + " pixel, peso massimo " + ImgBeta_WeightMaxCompress + " KB";
            MessageBetaLabel.Visible = true;
        }
        else MessageBetaLabel.Visible = false;


    }//fine SetInfoMessageBeta

    private void SetInfoMessageGamma()
    {
        if ((ImgGamma_RaidMode != 0) && (ImgGamma_RaidMode != 1))
        {
            MessageGammaLabel.Text = "Dimensioni finali:";

            if (ImgGamma_RaidMode == 2)
                MessageGammaLabel.Text += " massimo";

            MessageGammaLabel.Text += " " + ImgGamma_Width + "x" + ImgGamma_Height + " pixel, peso massimo " + ImgGamma_WeightMaxCompress + " KB";
            MessageGammaLabel.Visible = true;
        }
        else MessageGammaLabel.Visible = false;

    }//fine SetInfoMessageBeta


    private string GetExtension(string FileName)
    {
        string[] split = FileName.Split('.');
        string Extension = split[split.Length - 1];
        return Extension;

    }//fine GetExtension


    private string myHtmlEncode(string HTML)
    {
        HTML = HTML.Replace("�", "e");
        HTML = HTML.Replace("�", "e");
        HTML = HTML.Replace("�", "e");
        HTML = HTML.Replace("�", "e");
        HTML = HTML.Replace("�", "o");
        HTML = HTML.Replace("�", "o");
        HTML = HTML.Replace("�", "u");
        HTML = HTML.Replace("�", "u");
        HTML = HTML.Replace("�", "u");
        HTML = HTML.Replace("�", "a");
        HTML = HTML.Replace("�", "a");
        HTML = HTML.Replace("�", "i");
        HTML = HTML.Replace("�", "i");
        HTML = HTML.Replace("�", "i");
        HTML = HTML.Replace("�", "c");
        HTML = HTML.Replace("_", string.Empty);
        HTML = HTML.Replace(" ", string.Empty);
        HTML = System.Text.RegularExpressions.Regex.Replace(HTML, @"[^\w-]+", string.Empty);
        return HTML;
    }


    private string CheckFileName(string filePath, string fileName)
    {

        int counter = 1;
        string finalPath = filePath;
        string strFile_name = System.IO.Path.GetFileNameWithoutExtension(fileName);
        strFile_name = myHtmlEncode(strFile_name);

        if (strFile_name.Length > 25)
            strFile_name = strFile_name.Substring(0, 25);

        string strFile_extension = System.IO.Path.GetExtension(fileName);

        if (System.IO.File.Exists(filePath + fileName))
        {

            // il file � gi� sul server
            filePath += fileName;
            while (System.IO.File.Exists(filePath))
            {
                counter++;
                filePath = System.IO.Path.Combine(finalPath, strFile_name + counter.ToString() + strFile_extension);

            }//fine while


            fileName = strFile_name + counter.ToString() + strFile_extension;


        }
        //else fileName =strFile_name;

        ////Response.Write("DEB "+fileName+"<br />");
        return fileName;

    }//fine CheckFileName


    private bool ThumbnailCallback()
    {
        return false;
    }


    private System.Drawing.Imaging.ImageCodecInfo GetEncoderInfo(string mimeType)
    {
        System.Drawing.Imaging.ImageCodecInfo[] encoders = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders();
        for (int j = 0; j < encoders.Length; j++)
            if (encoders[j].MimeType == mimeType)
                return encoders[j];

        return null;
    }//fine GetEncoderInfo



    //private string GetMimeType(string strFileName)
    //{
    //    string retval = string.Empty;

    //    switch (System.IO.Path.GetExtension(strFileName).ToLower())
    //    {

    //        case ".art": retval = "image/x-jg"; break;
    //        case ".bm": retval = "image/bmp"; break;
    //        case ".bmp": retval = "image/bmp"; break;
    //        case ".g3": retval = "image/g3fax"; break;
    //        case ".gif": retval = "image/gif"; break;
    //        case ".ico": retval = "image/x-icon"; break;
    //        case ".ief": retval = "image/ief"; break;
    //        case ".iefs": retval = "image/ief"; break;
    //        case ".jfif": retval = "image/jpeg"; break;
    //        case ".jfif-tbnl": retval = "image/jpeg"; break;
    //        case ".jpe": retval = "image/jpeg"; break;
    //        case ".jpeg": retval = "image/jpeg"; break;
    //        case ".jpg": retval = "image/jpeg"; break;
    //        case ".jps": retval = "image/x-jps"; break;
    //        case ".jut": retval = "image/jutvision"; break;
    //        case ".nif": retval = "image/x-niff"; break;
    //        case ".niff": retval = "image/x-niff"; break;
    //        case ".pct": retval = "image/x-pict"; break;
    //        case ".pcx": retval = "image/x-pcx"; break;
    //        case ".pgm": retval = "image/x-portable-greymap"; break;
    //        case ".pic": retval = "image/pict"; break;
    //        case ".pict": retval = "image/pict"; break;
    //        case ".pm": retval = "image/x-xpixmap"; break;
    //        case ".png": retval = "image/png"; break;
    //        case ".ppm": retval = "image/x-portable-pixmap"; break;
    //        case ".qif": retval = "image/x-quicktime"; break;
    //        case ".rast": retval = "image/cmu-raster"; break;
    //        case ".rf": retval = "image/vnd.rn-realflash"; break;
    //        case ".rgb": retval = "image/x-rgb"; break;
    //        case ".rp": retval = "image/vnd.rn-realpix"; break;
    //        case ".svf": retval = "image/vnd.dwg"; break;
    //        case ".tif": retval = "image/tiff"; break;
    //        case ".tiff": retval = "image/tiff"; break;
    //        case ".turbot": retval = "image/florian"; break;
    //        case ".wbmp": retval = "image/vnd.wap.wbmp"; break;
    //        case ".xif": retval = "image/vnd.xiff"; break;
    //        case ".xpm": retval = "image/xpm"; break;
    //        case ".xwd": retval = "image/x-xwd"; break;





    //    }//fine switch

    //    return retval;

    //}//fine GetMimeType


    private string GetMimeType(string Filename)
    {
        string mime = "application/octetstream";
        string ext = System.IO.Path.GetExtension(Filename).ToLower();
        Microsoft.Win32.RegistryKey rk = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
        if (rk != null && rk.GetValue("Content Type") != null)
            mime = rk.GetValue("Content Type").ToString();
        return mime;
    }


    private static System.Drawing.Imaging.ImageFormat GetImageFormat(string Format)
    {

        switch (Format.ToLower())
        {
            case ".jpg":
                return System.Drawing.Imaging.ImageFormat.Jpeg;
                break;

            case ".jpeg":
                return System.Drawing.Imaging.ImageFormat.Jpeg;
                break;

            case ".gif":
                return System.Drawing.Imaging.ImageFormat.Gif;
                break;

            case ".png":
                return System.Drawing.Imaging.ImageFormat.Png;
                break;

            default:
                return System.Drawing.Imaging.ImageFormat.Jpeg;
                break;

        }//fine switch



    }// fine GetImageFormat



    //##############################################################################################################
    //######################## Algoritmi di trattamenti immagine / ritaglio ########################################
    //############################################################################################################## 



    private static System.Drawing.Imaging.ImageCodecInfo GetEncoder(System.Drawing.Imaging.ImageFormat format)
    {

        System.Drawing.Imaging.ImageCodecInfo[] codecs = System.Drawing.Imaging.ImageCodecInfo.GetImageDecoders();

        foreach (System.Drawing.Imaging.ImageCodecInfo codec in codecs)
        {
            if (codec.FormatID == format.Guid)
            {
                return codec;
            }
        }
        return null;
    }


    private System.Drawing.Image GetThumbnailImage(System.Drawing.Image normalImage, int myWidth, int myHeight)
    {
        System.Drawing.Image myImage;

        System.Drawing.Bitmap srcBmp = new System.Drawing.Bitmap(normalImage);

        System.Drawing.Bitmap target = new System.Drawing.Bitmap(myWidth, myHeight);


        // //Response.Write("DEB GetThumbnailImage myWidth:" + myWidth + " - myHeight:" + myHeight+"<br />");

        using (System.Drawing.Graphics graphics = System.Drawing.Graphics.FromImage(target))
        {
            graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            graphics.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
            graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            graphics.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
            graphics.FillRectangle(System.Drawing.Brushes.White, 0, 0, myWidth, myHeight);
            graphics.DrawImage(srcBmp, 0, 0, myWidth, myHeight);

            System.Drawing.Imaging.EncoderParameters eps = new System.Drawing.Imaging.EncoderParameters(1);
            eps.Param[0] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100);
            System.Drawing.Imaging.ImageCodecInfo ici = GetEncoder(System.Drawing.Imaging.ImageFormat.Bmp);


            using (MemoryStream memoryStream = new MemoryStream())
            {
                target.Save(memoryStream, ici, eps);
                myImage = System.Drawing.Image.FromStream(memoryStream);
                target.Dispose();
                srcBmp.Dispose();
            }

        }

        return myImage;
    }//fine GetThumbnailImage


    //private System.Drawing.Bitmap Clone(System.Drawing.Bitmap srcBmp, int x, int y, int myWidth, int myHeight)
    //{


    //    System.Drawing.Bitmap target = new System.Drawing.Bitmap(myWidth, myHeight);

    //    using (System.Drawing.Graphics graphics = System.Drawing.Graphics.FromImage(target))
    //    {
    //        graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
    //        graphics.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
    //        graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
    //        graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
    //        graphics.DrawImage(srcBmp, new System.Drawing.Rectangle(0, 0, myWidth, myWidth), x, y, myWidth, myHeight, System.Drawing.GraphicsUnit.Pixel);

    //    }

    //    return target;

    //}//fine Clone

    private System.Drawing.Image Algoritmo_RL(System.Drawing.Image normalImage, int myWidth)
    {
        int newHeight = 0;
        int newWidth = myWidth;

        newHeight = normalImage.Height * newWidth / normalImage.Width;

        System.Drawing.Bitmap b = new System.Drawing.Bitmap(newWidth, newHeight);
        System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(b);
        g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
        g.DrawImage(normalImage, 0, 0, newWidth, newHeight);
        g.Dispose();
        b.SetResolution(72, 72);

        return (System.Drawing.Image)b;

        //try
        //{
        //    int newHeight = 0;
        //    int newWidth = myWidth;

        //    newHeight = normalImage.Height * newWidth / normalImage.Width;


        //    if (newHeight == newWidth)
        //        normalImage = normalImage.GetThumbnailImage(newWidth, newHeight, ThumbnailCallback, IntPtr.Zero);
        //    else normalImage = GetThumbnailImage(normalImage, newWidth, newHeight);

        //}
        //catch (Exception p)
        //{
        //    ////Response.Write(p.ToString());
        //}

        //return normalImage;

    }//fine Algoritmo_RL



    private System.Drawing.Image Algoritmo_RL(System.Drawing.Image normalImage, int myWidth, int mode)
    {
        try
        {
            int newHeight = 0;
            int newWidth = myWidth;

            newHeight = normalImage.Height * newWidth / normalImage.Width;


            if (mode == 2)
                normalImage = normalImage.GetThumbnailImage(newWidth, newHeight, ThumbnailCallback, IntPtr.Zero);
            else
            {

                if (newHeight == newWidth)
                    normalImage = normalImage.GetThumbnailImage(newWidth, newHeight, ThumbnailCallback, IntPtr.Zero);
                else normalImage = GetThumbnailImage(normalImage, newWidth, newHeight);
            }

        }
        catch (Exception p)
        {
            ////Response.Write(p.ToString());
        }

        return normalImage;

    }//fine Algoritmo_RL



    private System.Drawing.Image Algoritmo_RA(System.Drawing.Image normalImage, int myHeght)
    {
        int newHeight = myHeght;
        int newWidth = 0;

        newWidth = normalImage.Width * newHeight / normalImage.Height;

        System.Drawing.Bitmap b = new System.Drawing.Bitmap(newWidth, newHeight);
        System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(b);
        g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
        g.DrawImage(normalImage, 0, 0, newWidth, newHeight);
        g.Dispose();
        b.SetResolution(72, 72);

        return (System.Drawing.Image)b;

        //try
        //{
        //    int newHeight = myHeght;
        //    int newWidth = 0;


        //    newWidth = normalImage.Width * newHeight / normalImage.Height;

        //    if (newHeight == newWidth)
        //        normalImage = normalImage.GetThumbnailImage(newWidth, newHeight, ThumbnailCallback, IntPtr.Zero);
        //    else normalImage = GetThumbnailImage(normalImage, newWidth, newHeight);

        //}
        //catch (Exception p)
        //{
        //    ////Response.Write(p.ToString());
        //}

        //return normalImage;

    }//fine Algoritmo_RA


    private System.Drawing.Image Algoritmo_RA(System.Drawing.Image normalImage, int myHeght, int mode)
    {
        try
        {
            int newHeight = myHeght;
            int newWidth = 0;


            newWidth = normalImage.Width * newHeight / normalImage.Height;

            if (mode == 2)
                normalImage = normalImage.GetThumbnailImage(newWidth, newHeight, ThumbnailCallback, IntPtr.Zero);

            else
            {

                if (newHeight == newWidth)
                    normalImage = normalImage.GetThumbnailImage(newWidth, newHeight, ThumbnailCallback, IntPtr.Zero);
                else normalImage = GetThumbnailImage(normalImage, newWidth, newHeight);
            }

        }
        catch (Exception p)
        {
            ////Response.Write(p.ToString());
        }

        return normalImage;

    }//fine Algoritmo_RA


    private System.Drawing.Bitmap Algoritmo_CA_Beta(System.Drawing.Bitmap myBitmap)
    {
        try
        {
            double startY = System.Math.Abs(myBitmap.Height - ImgBeta_Height) / 2;
            //double startY = System.Math.Abs(myBitmap.Width - ImgBeta_Width) / 2;
            myBitmap = myBitmap.Clone(new System.Drawing.Rectangle(0, (int)startY, ImgBeta_Width, ImgBeta_Height), myBitmap.PixelFormat);

        }
        catch (Exception p)
        {
            //////Response.Write("Algoritmo_CA_Beta:" +p.ToString());
        }

        return myBitmap;

    }//fine Algoritmo_CA


    private System.Drawing.Bitmap Algoritmo_CL_Beta(System.Drawing.Bitmap myBitmap)
    {
        try
        {

            //double startX = System.Math.Abs(myBitmap.Height - ImgBeta_Height) / 2;
            double startX = System.Math.Abs(myBitmap.Width - ImgBeta_Width) / 2;

            myBitmap = myBitmap.Clone(new System.Drawing.Rectangle((int)startX, 0, ImgBeta_Width, ImgBeta_Height), myBitmap.PixelFormat);

        }
        catch (Exception p)
        {
            //////Response.Write("Algoritmo_CL_Beta:" +p.ToString());
        }

        return myBitmap;

    }//fine Algoritmo_CL


    private System.Drawing.Bitmap Algoritmo_CA_Gamma(System.Drawing.Bitmap myBitmap)
    {
        try
        {
            double startY = System.Math.Abs(myBitmap.Width - ImgGamma_Width) / 2;

            myBitmap = myBitmap.Clone(new System.Drawing.Rectangle(0, (int)startY, ImgGamma_Width, ImgGamma_Height), myBitmap.PixelFormat);
        }
        catch (Exception p)
        {
            ////Response.Write(p.ToString());
        }

        return myBitmap;

    }//fine Algoritmo_CA


    private System.Drawing.Bitmap Algoritmo_CL_Gamma(System.Drawing.Bitmap myBitmap)
    {
        try
        {

            double startX = System.Math.Abs(myBitmap.Height - ImgGamma_Height) / 2;

            myBitmap = myBitmap.Clone(new System.Drawing.Rectangle((int)startX, 0, ImgGamma_Width, ImgGamma_Height), myBitmap.PixelFormat);

        }
        catch (Exception p)
        {
            ////Response.Write(p.ToString());
        }

        return myBitmap;

    }//fine Algoritmo_CL


    private double GetSizeRatio(System.Drawing.Image myImage)
    {
        return (double)myImage.Width / myImage.Height;
    }



    private bool NeedsBetaTransformation()
    {
        try
        {

            System.Drawing.Image myImage = System.Drawing.Image.FromStream(FilUpl.FileContent);

            //////Response.Write("DEB " + myImage.Width + " - " + ImgBeta_Width + " - " + myImage.Height + " - " + ImgBeta_Height + "<br />");

            if ((myImage.Width == ImgBeta_Width) && (myImage.Height == ImgBeta_Height))
                return false;
            else if ((myImage.Width >= ImgBeta_Width) || (myImage.Height >= ImgBeta_Height))
                return true;
            else return false;
        }
        catch (Exception p)
        {
            //Response.Write(p.ToString());
            return false;
        }

    }//fine NeedsBetaTransformation

    private bool NeedsGammaTransformation()
    {
        try
        {

            System.Drawing.Image myImage = System.Drawing.Image.FromStream(FilUpl.FileContent);

            if ((myImage.Width == ImgGamma_Width) && (myImage.Height == ImgGamma_Height))
                return false;
            else if ((myImage.Width >= ImgGamma_Width) || (myImage.Height >= ImgGamma_Height))
                return true;
            else return false;
        }
        catch (Exception p)
        {
            //Response.Write(p.ToString());
            return false;
        }

    }//fine NeedsGammaTransformation

    //##############################################################################################################
    //################### FINE Algoritmi di trattamenti immagine / ritaglio ########################################
    //############################################################################################################## 


    private static System.Drawing.Image resizeImage(System.Drawing.Image imgToResize, System.Drawing.Size size)
    {
        int sourceWidth = imgToResize.Width;
        int sourceHeight = imgToResize.Height;

        float nPercent = 0;
        float nPercentW = 0;
        float nPercentH = 0;

        nPercentW = ((float)size.Width / (float)sourceWidth);
        nPercentH = ((float)size.Height / (float)sourceHeight);

        if (nPercentH < nPercentW)
            nPercent = nPercentH;
        else
            nPercent = nPercentW;

        int destWidth = (int)(sourceWidth * nPercent);
        int destHeight = (int)(sourceHeight * nPercent);

        System.Drawing.Bitmap b = new System.Drawing.Bitmap(destWidth, destHeight);
        System.Drawing.Graphics g = System.Drawing.Graphics.FromImage((System.Drawing.Image)b);
        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

        g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
        g.Dispose();

        return (System.Drawing.Image)b;
    }//fine resizeImage


    private static System.Drawing.Image ResizeImageForce(System.Drawing.Image imgToResize, System.Drawing.Size size)
    {

        System.Drawing.Bitmap b = new System.Drawing.Bitmap(size.Width, size.Height);
        System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(b);
        g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
        g.DrawImage(imgToResize, 0, 0, size.Width, size.Height);
        g.Dispose();
        b.SetResolution(72, 72);
        return (System.Drawing.Image)b;

        //System.Drawing.Bitmap b = new System.Drawing.Bitmap(size.Width, size.Height);
        //System.Drawing.Graphics g = System.Drawing.Graphics.FromImage((System.Drawing.Image)b);
        //g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

        //g.DrawImage(imgToResize, 0, 0, size.Width, size.Height);
        //g.Dispose();

        //b.SetResolution(72, 72);
        //return (System.Drawing.Image)b;
    }//fine ResizeImageForce


    public bool GenerateBeta(string myPath)
    {

        try
        {
            string FileName = string.Empty;

            if (myPath.Length == 0)
                myPath = ImgBeta_Path;

            if (myImgBeta_RaidMode == 0)
                return false;


            if (myImgBeta_RaidMode == 1)
            {



                if (FilUpl.HasFile)
                {
                    //FileName = "BETA_" + this.ID + CheckFileName(Server.MapPath(myPath), FilUpl.FileName);
                    FileName = CheckFileName(Server.MapPath(myPath), FilUpl.FileName);
                    FilUpl.SaveAs(Server.MapPath(myPath) + FileName);
                }
                else if (FileNameBetaTriggedLabel.Text.Length > 0)
                {
                    string filePath = Server.MapPath(this.savePath);
                    FileName = CheckFileName(filePath, FileNameBetaTriggedLabel.Text);
                    File.Copy(Server.MapPath(TempPathBeta + FileNameBetaTriggedLabel.Text), filePath + FileName);
                    File.Delete(Server.MapPath(TempPathBeta + FileNameBetaTriggedLabel.Text));
                }
                else FileName = pFileName;

                FileNameBetaTriggedLabel.Text = FileName;
                pFileName = FileName;
                UploadBetaImage.ImageUrl = myPath + FileName;
                InfoBetaLabel.Visible = false;
                return true;

            }//fine if




            if (heightFinal == 0)
                return false;

            if (widthFinal == 0)
                return false;

            if (FilUpl.HasFile)
            {
                System.Drawing.Image myImage = System.Drawing.Image.FromStream(FilUpl.FileContent);
                //FileName = "BETA_" + this.ID + CheckFileName(Server.MapPath(myPath), FilUpl.FileName);
                myImage = ResizeImageForce(myImage, new System.Drawing.Size(myImage.Width, myImage.Height));



                FileName = CheckFileName(Server.MapPath(myPath), FilUpl.FileName);


                bool MustSaveMe = false;

                if ((ImgBeta_RaidMode == 2) && (NeedsBetaTransformation()))
                {

                    MustSaveMe = true;

                    if (ImgBeta_Width > ImgBeta_Height)
                        myImage = Algoritmo_RA(System.Drawing.Image.FromStream(FilUpl.FileContent), ImgBeta_Height, ImgBeta_RaidMode);
                    else
                        myImage = Algoritmo_RL(System.Drawing.Image.FromStream(FilUpl.FileContent), ImgBeta_Width, ImgBeta_RaidMode);
                }

                if ((ImgBeta_RaidMode == 3) && (NeedsBetaTransformation()))
                {

                    MustSaveMe = true;
                    double AlphaRatio = GetSizeRatio(System.Drawing.Image.FromStream(FilUpl.FileContent));
                    double BetaRatio = (double)ImgBeta_Width / ImgBeta_Height;


                    if (AlphaRatio == 1)
                    {
                        if (BetaRatio == 1)
                            myImage = Algoritmo_RL(System.Drawing.Image.FromStream(FilUpl.FileContent), ImgBeta_Width);
                        else if (BetaRatio > 1)
                            myImage = Algoritmo_CA_Beta(new System.Drawing.Bitmap(Algoritmo_RL(System.Drawing.Image.FromStream(FilUpl.FileContent), ImgBeta_Width)));
                        else if (BetaRatio < 1)
                            myImage = Algoritmo_CL_Beta(new System.Drawing.Bitmap(Algoritmo_RA(System.Drawing.Image.FromStream(FilUpl.FileContent), ImgBeta_Height)));

                    }
                    else if (AlphaRatio > 1)
                    {
                        if (BetaRatio == 1)
                            myImage = Algoritmo_CL_Beta(new System.Drawing.Bitmap(Algoritmo_RA(System.Drawing.Image.FromStream(FilUpl.FileContent), ImgBeta_Height)));
                        else if (BetaRatio > 1)
                        {
                            if (AlphaRatio > BetaRatio)
                                myImage = Algoritmo_CL_Beta(new System.Drawing.Bitmap(Algoritmo_RA(System.Drawing.Image.FromStream(FilUpl.FileContent), ImgBeta_Height)));
                            else if (AlphaRatio <= BetaRatio)
                                myImage = Algoritmo_CA_Beta(new System.Drawing.Bitmap(Algoritmo_RL(System.Drawing.Image.FromStream(FilUpl.FileContent), ImgBeta_Width)));
                        }
                        else if (BetaRatio < 1)
                            myImage = Algoritmo_CL_Beta(new System.Drawing.Bitmap(Algoritmo_RA(System.Drawing.Image.FromStream(FilUpl.FileContent), ImgBeta_Height)));
                    }
                    else if (AlphaRatio < 1)
                    {
                        if (BetaRatio == 1)
                            myImage = Algoritmo_CA_Beta(new System.Drawing.Bitmap(Algoritmo_RL(System.Drawing.Image.FromStream(FilUpl.FileContent), ImgBeta_Width)));
                        else if (BetaRatio > 1)
                            myImage = Algoritmo_CA_Beta(new System.Drawing.Bitmap(Algoritmo_RL(System.Drawing.Image.FromStream(FilUpl.FileContent), ImgBeta_Width)));
                        else if (BetaRatio < 1)
                        {
                            if (AlphaRatio > BetaRatio)
                                myImage = Algoritmo_CA_Beta(new System.Drawing.Bitmap(Algoritmo_RL(System.Drawing.Image.FromStream(FilUpl.FileContent), ImgBeta_Width)));
                            else if (AlphaRatio <= BetaRatio)
                                myImage = Algoritmo_CL_Beta(new System.Drawing.Bitmap(Algoritmo_RA(System.Drawing.Image.FromStream(FilUpl.FileContent), ImgBeta_Height)));
                        }

                    }

                }//fine RaidMode3

                if ((ImgBeta_RaidMode == 4) && (NeedsBetaTransformation()))
                {
                    MustSaveMe = true;
                    using (myImage)
                    {

                        myImage = resizeImage(myImage, new System.Drawing.Size(ImgBeta_Width, ImgBeta_Height));

                        int width = ImgBeta_Width;
                        int height = ImgBeta_Height;
                        int x = 0;
                        int y = 0;

                        bool doCanvas = true;

                        if (myImage.Width == myImage.Height)
                            doCanvas = false;


                        x = (int)(width - myImage.Width) / 2;
                        y = (int)(height - myImage.Height) / 2;

                        if (doCanvas)
                        {

                            //System.Drawing.Image Frame;
                            //Frame = System.Drawing.Image.FromFile(Server.MapPath("~/ZeusInc/ImageRaider/Canvas_White.jpg"));


                            System.Drawing.Bitmap Frame = new System.Drawing.Bitmap(width, height);

                            for (int i = 0; i < width; ++i)
                                for (int j = 0; j < height; ++j)
                                    Frame.SetPixel(i, j, System.Drawing.Color.White);

                            using (Frame)
                            {
                                using (var bitmap = new System.Drawing.Bitmap(width, height))
                                {
                                    using (var canvas = System.Drawing.Graphics.FromImage(bitmap))
                                    {
                                        canvas.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                                        canvas.DrawImage((System.Drawing.Image)Frame, new System.Drawing.Rectangle(0, 0, width, height), new System.Drawing.Rectangle(0, 0, width, height), System.Drawing.GraphicsUnit.Pixel);
                                        canvas.DrawImage(myImage, x, y, myImage.Width, myImage.Height);
                                        canvas.Save();
                                    }//fine using
                                    //MemoryStream mtMemoryStream = new MemoryStream();
                                    //bitmap.Save(mtMemoryStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                                    //myImage = System.Drawing.Image.FromStream(mtMemoryStream);
                                    //mtMemoryStream.Dispose();
                                    //myImage = (System.Drawing.Image)bitmap;
                                    myImage = ResizeImageForce((System.Drawing.Image)bitmap, new System.Drawing.Size(ImgBeta_Width, ImgBeta_Height));
                                    //bitmap.Save(Server.MapPath("~/ZeusInc/") + DateTime.Now.Millisecond.ToString() + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);


                                }//fine using
                            }//fine using

                        }//fine if
                        else
                            myImage = resizeImage(myImage, new System.Drawing.Size(ImgBeta_Width, ImgBeta_Height));

                    }//fine using


                }//fine RaidMode4

                MemoryStream myMemoryStream = new MemoryStream();
                //////Response.Write("DEB " + FilUpl.PostedFile.ContentLength+" - "+ compressionThresholdKb * (1024)+"<br />");
                if (FilUpl.PostedFile.ContentLength > compressionThresholdKb * (102400))
                {

                    MustSaveMe = true;

                    System.Drawing.Imaging.EncoderParameters eps = new System.Drawing.Imaging.EncoderParameters(1);
                    eps.Param[0] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
                    System.Drawing.Imaging.ImageCodecInfo ici = GetEncoderInfo(GetMimeType(FileName));


                    myImage.Save(myMemoryStream, ici, eps);


                    while (myMemoryStream.Length > compressionThresholdKb * (102400))
                    {
                        quality = quality - 1;
                        eps = new System.Drawing.Imaging.EncoderParameters(1);
                        eps.Param[0] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
                        ici = GetEncoderInfo(GetMimeType(FileName));
                        myMemoryStream.Close();
                        myMemoryStream = new MemoryStream();
                        myImage.Save(myMemoryStream, ici, eps);
                    }

                    myImage = System.Drawing.Image.FromStream(myMemoryStream);


                }//fine if


                if ((myImage != null) && (MustSaveMe))
                    myImage.Save(Server.MapPath(myPath + FileName), GetImageFormat(System.IO.Path.GetExtension(FileName)));
                else if (!MustSaveMe)
                    FilUpl.SaveAs(Server.MapPath(myPath + FileName));

                myMemoryStream.Close();

                FileInfo myFile = new FileInfo(Server.MapPath(myPath + FileName));
                FileNameBetaTriggedLabel.Text = FileName;
                pFileName = FileName;
                UploadBetaImage.ImageUrl = myPath + FileName;
                UploadBetaImage.Visible = true;
                InfoBetaLabel.Text = myImage.PhysicalDimension.Width + "x" + myImage.PhysicalDimension.Height + " pixel, " + ((double)(myFile.Length / 1024)).ToString() + " KB";
                InfoBetaLabel.Visible = true;
                myImage.Dispose();
                return true;


            }//fine if has file
            else if ((FileNameBetaTriggedLabel.Text.Length > 0)
                && (FileNameBetaTriggedLabel.Text.IndexOf("ImgNonDisponibile") == -1))
            {
                string filePath = System.Web.HttpContext.Current.Server.MapPath(this.savePath);
                pFileName = CheckFileName(filePath, FileNameBetaTriggedLabel.Text);
                File.Copy(Server.MapPath(TempPathBeta + FileNameBetaTriggedLabel.Text), filePath + pFileName);
                File.Delete(Server.MapPath(TempPathBeta + FileNameBetaTriggedLabel.Text));

                return true;
            }

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

        return false;
    }//fine GenerateBeta 


    public bool GenerateGamma(string myPath)
    {
        try
        {
            string FileName = string.Empty;

            if (myPath.Length == 0)
                myPath = ImgGamma_Path;

            if (myImgGamma_RaidMode == 0)
                return false;


            if (myImgGamma_RaidMode == 1)
            {


                if (FilUpl.HasFile)
                {
                    FileName = CheckFileName(Server.MapPath(myPath), FilUpl.FileName);
                    FilUpl.SaveAs(Server.MapPath(myPath) + FileName);
                }
                else if (FileNameGammaTriggedLabel.Text.Length > 0)
                {
                    string filePath = Server.MapPath(this.savePath);
                    FileName = CheckFileName(filePath, FileNameGammaTriggedLabel.Text);
                    File.Copy(Server.MapPath(TempPathGamma + FileNameGammaTriggedLabel.Text), filePath + FileName);
                    File.Delete(Server.MapPath(TempPathGamma + FileNameGammaTriggedLabel.Text));
                }
                else FileName = pFileNameTmb;

                FileNameGammaTriggedLabel.Text = FileName;
                pFileNameTmb = FileName;
                UploadGammaImage.ImageUrl = myPath + FileName;
                InfoGammaLabel.Visible = false;
                return true;
            }//fine if

            if (heightFinal == 0)
                return false;

            if (widthFinal == 0)
                return false;

            if (FilUpl.HasFile)
            {


                System.Drawing.Image myImage = System.Drawing.Image.FromStream(FilUpl.FileContent);

                myImage = ResizeImageForce(myImage, new System.Drawing.Size(myImage.Width, myImage.Height));

                FileName = CheckFileName(Server.MapPath(myPath), FilUpl.FileName);
                bool MustSaveMe = false;

                if ((ImgGamma_RaidMode == 2) && (NeedsGammaTransformation()))
                {
                    MustSaveMe = true;

                    if (ImgGamma_Width > ImgGamma_Height)
                        myImage = Algoritmo_RA(System.Drawing.Image.FromStream(FilUpl.FileContent), ImgGamma_Height, ImgGamma_RaidMode);
                    else
                        myImage = Algoritmo_RL(System.Drawing.Image.FromStream(FilUpl.FileContent), ImgGamma_Width, ImgGamma_RaidMode);
                }

                if ((ImgGamma_RaidMode == 3) && (NeedsGammaTransformation()))
                {


                    MustSaveMe = true;
                    double AlphaRatio = GetSizeRatio(System.Drawing.Image.FromStream(FilUpl.FileContent));
                    double GammaRatio = (double)ImgGamma_Width / ImgGamma_Height;


                    if (AlphaRatio == 1)
                    {
                        if (GammaRatio == 1)
                            myImage = Algoritmo_RL(System.Drawing.Image.FromStream(FilUpl.FileContent), ImgGamma_Width);
                        else if (GammaRatio > 1)
                            myImage = Algoritmo_CA_Gamma(new System.Drawing.Bitmap(Algoritmo_RL(System.Drawing.Image.FromStream(FilUpl.FileContent), ImgGamma_Width)));
                        else if (GammaRatio < 1)
                            myImage = Algoritmo_CL_Gamma(new System.Drawing.Bitmap(Algoritmo_RA(System.Drawing.Image.FromStream(FilUpl.FileContent), ImgGamma_Height)));

                    }
                    else if (AlphaRatio > 1)
                    {
                        if (GammaRatio == 1)
                            myImage = Algoritmo_CL_Gamma(new System.Drawing.Bitmap(Algoritmo_RA(System.Drawing.Image.FromStream(FilUpl.FileContent), ImgGamma_Height)));
                        else if (GammaRatio > 1)
                        {
                            if (AlphaRatio > GammaRatio)
                                myImage = Algoritmo_CL_Gamma(new System.Drawing.Bitmap(Algoritmo_RA(System.Drawing.Image.FromStream(FilUpl.FileContent), ImgGamma_Height)));
                            else if (AlphaRatio <= GammaRatio)
                                myImage = Algoritmo_CA_Gamma(new System.Drawing.Bitmap(Algoritmo_RL(System.Drawing.Image.FromStream(FilUpl.FileContent), ImgGamma_Width)));
                        }
                        else if (GammaRatio < 1)
                            myImage = Algoritmo_CL_Gamma(new System.Drawing.Bitmap(Algoritmo_RA(System.Drawing.Image.FromStream(FilUpl.FileContent), ImgGamma_Height)));
                    }
                    else if (AlphaRatio < 1)
                    {
                        if (GammaRatio == 1)
                            myImage = Algoritmo_CA_Gamma(new System.Drawing.Bitmap(Algoritmo_RL(System.Drawing.Image.FromStream(FilUpl.FileContent), ImgGamma_Width)));
                        else if (GammaRatio > 1)
                            myImage = Algoritmo_CA_Gamma(new System.Drawing.Bitmap(Algoritmo_RL(System.Drawing.Image.FromStream(FilUpl.FileContent), ImgGamma_Width)));
                        else if (GammaRatio < 1)
                        {
                            if (AlphaRatio > GammaRatio)
                                myImage = Algoritmo_CA_Gamma(new System.Drawing.Bitmap(Algoritmo_RL(System.Drawing.Image.FromStream(FilUpl.FileContent), ImgGamma_Width)));
                            else if (AlphaRatio <= GammaRatio)
                                myImage = Algoritmo_CL_Gamma(new System.Drawing.Bitmap(Algoritmo_RA(System.Drawing.Image.FromStream(FilUpl.FileContent), ImgGamma_Height)));
                        }

                    }

                }//fine RaidMode3
                if ((ImgGamma_RaidMode == 4) && (NeedsGammaTransformation()))
                {
                    MustSaveMe = true;
                    using (myImage)
                    {

                        myImage = resizeImage(myImage, new System.Drawing.Size(ImgGamma_Width, ImgGamma_Height));

                        int width = ImgGamma_Width;
                        int height = ImgGamma_Height;
                        int x = 0;
                        int y = 0;

                        bool doCanvas = true;

                        if (myImage.Width == myImage.Height)
                            doCanvas = false;


                        x = (int)(width - myImage.Width) / 2;
                        y = (int)(height - myImage.Height) / 2;

                        if (doCanvas)
                        {

                            //System.Drawing.Image Frame;
                            //Frame = System.Drawing.Image.FromFile(Server.MapPath("~/ZeusInc/ImageRaider/Canvas_White.jpg"));


                            System.Drawing.Bitmap Frame = new System.Drawing.Bitmap(width, height);

                            for (int i = 0; i < width; ++i)
                                for (int j = 0; j < height; ++j)
                                    Frame.SetPixel(i, j, System.Drawing.Color.White);

                            using (Frame)
                            {
                                using (var bitmap = new System.Drawing.Bitmap(width, height))
                                {
                                    using (var canvas = System.Drawing.Graphics.FromImage(bitmap))
                                    {
                                        canvas.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                                        canvas.DrawImage((System.Drawing.Image)Frame, new System.Drawing.Rectangle(0, 0, width, height), new System.Drawing.Rectangle(0, 0, width, height), System.Drawing.GraphicsUnit.Pixel);
                                        canvas.DrawImage(myImage, x, y, myImage.Width, myImage.Height);
                                        canvas.Save();
                                    }//fine using
                                    //MemoryStream mtMemoryStream = new MemoryStream();
                                    //bitmap.Save(mtMemoryStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                                    //myImage = System.Drawing.Image.FromStream(mtMemoryStream);
                                    //mtMemoryStream.Dispose();
                                    //myImage = (System.Drawing.Image)bitmap;
                                    myImage = ResizeImageForce((System.Drawing.Image)bitmap, new System.Drawing.Size(ImgGamma_Width, ImgGamma_Height));
                                    //bitmap.Save(Server.MapPath("~/ZeusInc/") + DateTime.Now.Millisecond.ToString() + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);


                                }//fine using
                            }//fine using

                        }//fine if
                        else
                            myImage = resizeImage(myImage, new System.Drawing.Size(ImgGamma_Width, ImgGamma_Height));

                    }//fine using

                }//fine RaidMode4


                MemoryStream myMemoryStream = new MemoryStream();

                if (FilUpl.PostedFile.ContentLength > compressionThresholdKbTmb * (102400))
                {

                    MustSaveMe = true;




                    System.Drawing.Imaging.EncoderParameters eps = new System.Drawing.Imaging.EncoderParameters(1);
                    eps.Param[0] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, qualityTmb);
                    System.Drawing.Imaging.ImageCodecInfo ici = GetEncoderInfo(GetExtension(FileName));

                    if ((eps != null)
                        && (ici != null))
                    {


                        myImage.Save(myMemoryStream, ici, eps);
                        MustSaveMe = true;

                        while (myMemoryStream.Length > compressionThresholdKbTmb * (102400))
                        {

                            qualityTmb = qualityTmb - 1;
                            eps = new System.Drawing.Imaging.EncoderParameters(1);
                            eps.Param[0] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, qualityTmb);
                            ici = GetEncoderInfo(GetMimeType(FileName));
                            myMemoryStream.Close();
                            myMemoryStream = new MemoryStream();
                            myImage.Save(myMemoryStream, ici, eps);
                        }

                        myImage = System.Drawing.Image.FromStream(myMemoryStream);

                    }
                }

                if ((myImage != null) && (MustSaveMe))
                    myImage.Save(Server.MapPath(myPath + FileName), GetImageFormat(System.IO.Path.GetExtension(FileName)));
                else if (!MustSaveMe)
                    FilUpl.SaveAs(Server.MapPath(myPath + FileName));

                myMemoryStream.Close();

                FileInfo myFile = new FileInfo(Server.MapPath(myPath + FileName));
                FileNameGammaTriggedLabel.Text = FileName;
                pFileNameTmb = FileName;
                UploadGammaImage.ImageUrl = myPath + FileName;
                UploadGammaImage.Visible = true;
                InfoGammaLabel.Text = myImage.PhysicalDimension.Width + "x" + myImage.PhysicalDimension.Height + " pixel, " + ((double)(myFile.Length / 1024)).ToString() + " KB";
                InfoGammaLabel.Visible = true;
                myImage.Dispose();
                return true;

            }//fine if
            else if ((FileNameGammaTriggedLabel.Text.Length > 0)
                && (FileNameGammaTriggedLabel.Text.IndexOf("ImgNonDisponibile")) == -1)
            {
                string filePath = System.Web.HttpContext.Current.Server.MapPath(this.saveThumbPath);
                pFileNameTmb = CheckFileName(filePath, FileNameGammaTriggedLabel.Text);
                File.Copy(Server.MapPath(TempPathGamma + FileNameGammaTriggedLabel.Text), filePath + pFileNameTmb);
                File.Delete(Server.MapPath(TempPathGamma + FileNameGammaTriggedLabel.Text));

                return true;
            }


        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }

        return false;
    }//fine GenereateGamma



    private System.Drawing.Bitmap CropBitmap(System.Drawing.Bitmap bitmap, int cropX, int cropY, int cropWidth, int cropHeight)
    {
        System.Drawing.Rectangle rect = new System.Drawing.Rectangle(cropX, cropY, cropWidth, cropHeight);
        System.Drawing.Bitmap cropped = bitmap.Clone(rect, bitmap.PixelFormat);
        return cropped;
    }//fine CropBitmap

    protected void myServerValidate(object source, ServerValidateEventArgs args)
    {
        try
        {

            CustomValidator myValidator = (CustomValidator)source;

            if (FilUpl.HasFile)
            {


                //VALIDAZIONE FORMATI
                string pFileExt = GetExtension(FilUpl.FileName);
                bool Ind = false;

                if (pFileTypeRange != null)
                {
                    foreach (string str in pFileTypeRange)
                    {
                        if (str.ToLower().Equals(pFileExt.ToLower()))
                        {
                            Ind = true;
                            break;
                        }
                    }
                }
                else Ind = true;

                if (!Ind)
                {
                    args.IsValid = false;
                    if (formatError.Length == 0)
                        myValidator.Text = "Formato del file non consentito";
                    else myValidator.Text = formatError;
                    return;
                }

                //VALIDAZIONI DIMENSIONI IMMAGINE MIN E MAX
                System.Drawing.Image CheckSize = null;

                try
                {
                    CheckSize = System.Drawing.Image.FromStream(FilUpl.PostedFile.InputStream);
                }
                catch (Exception) { }

                if (CheckSize != null)
                {
                    if (minWidth > 0)
                    {

                        if (CheckSize.PhysicalDimension.Width < minWidth)
                        {
                            args.IsValid = false;
                            if (phDimensionError.Length == 0)
                                LoadButtonCustomValidator.Text = "Dimensione immagine non corretta";
                            else LoadButtonCustomValidator.Text = phDimensionError;
                            return;
                        }

                    }
                    if (minHeight > 0)
                    {

                        if (CheckSize.PhysicalDimension.Height < minHeight)
                        {
                            args.IsValid = false;
                            if (phDimensionError.Length == 0)
                                LoadButtonCustomValidator.Text = "Dimensione immagine non corretta";
                            else LoadButtonCustomValidator.Text = phDimensionError;
                            return;
                        }
                    }
                    if (maxWidth > 0)
                    {

                        if (CheckSize.PhysicalDimension.Width > maxWidth)
                        {
                            args.IsValid = false;
                            if (phDimensionError.Length == 0)
                                LoadButtonCustomValidator.Text = "Dimensione immagine non corretta";
                            else LoadButtonCustomValidator.Text = phDimensionError;
                            return;
                        }
                    }
                    if (maxHeight > 0)
                    {

                        if (CheckSize.PhysicalDimension.Height > maxHeight)
                        {
                            args.IsValid = false;
                            if (phDimensionError.Length == 0)
                                LoadButtonCustomValidator.Text = "Dimensione immagine non corretta";
                            else LoadButtonCustomValidator.Text = phDimensionError;
                            return;
                        }
                    }
                }//fine if CheckSize!=null

                if (minSize > 0)
                {

                    if (FilUpl.PostedFile.ContentLength < (minSize * 1024))
                    {
                        args.IsValid = false;
                        if (sizeError.Length == 0)
                            LoadButtonCustomValidator.Text = "Il file non supera il peso minimo";
                        else LoadButtonCustomValidator.Text = sizeError;
                        return;
                    }
                }
                if (maxSize > 0)
                {

                    if (FilUpl.PostedFile.ContentLength > (maxSize * 1024))
                    {
                        args.IsValid = false;
                        if (sizeError.Length == 0)
                            LoadButtonCustomValidator.Text = "Il file supera il peso massimo";
                        else LoadButtonCustomValidator.Text = sizeError;
                        return;
                    }
                }

            }//fine if
            else
            {

                //VALIDAZIONE REQUIRED
                if ((pRequired) && (FileNameBetaTriggedLabel.Text.Length <= 0))
                {
                    args.IsValid = false;
                    if (requiredError.Length == 0)
                        myValidator.Text = "Attenzione il file upload e' vuoto";
                    else myValidator.Text = requiredError;
                }

            }//fine else
        }
        catch (Exception p)
        {
            //Response.Write(p.ToString());
        }
    }//fine myServerValidate



    public bool ClearTempPathBeta()
    {
        try
        {
            if (TempPathBeta.Length == 0)
                return false;

            string[] Files = Directory.GetFiles(Server.MapPath(TempPathBeta));

            for (int i = 0; i < Files.Length; ++i)
            {
                FileInfo myFileInfo = new FileInfo(Files[i]);

                myFileInfo.IsReadOnly = false;

                if (DateTime.Compare(myFileInfo.CreationTime, DateTime.Now.AddDays(-7)) <= 0)
                    File.Delete(Files[i]);

            }//fine for

            return true;
        }
        catch (Exception p)
        {
            ////Response.Write(p.ToString());
            return false;
        }
    }//fine ClearTempPathBeta

    public bool ClearTempPathGamma()
    {
        try
        {
            if (TempPathGamma.Length == 0)
                return false;

            string[] Files = Directory.GetFiles(Server.MapPath(TempPathGamma));

            for (int i = 0; i < Files.Length; ++i)
            {
                FileInfo myFileInfo = new FileInfo(Files[i]);

                myFileInfo.IsReadOnly = false;

                if (DateTime.Compare(myFileInfo.CreationTime, DateTime.Now.AddDays(-7)) <= 0)
                    File.Delete(Files[i]);

            }//fine for

            return true;
        }
        catch (Exception p)
        {
            ////Response.Write(p.ToString());
            return false;
        }
    }//fine ClearTempPathGamma




    protected void LoadButton_Click(object sender, EventArgs e)
    {
        if ((Page.IsValid) && (FilUpl.HasFile))
        {

            if (GenerateBeta(TempPathBeta))
            {
                UploadBetaImage.Visible = true;
                MessageBetaLabel.Visible = false;
            }

            if (GenerateGamma(TempPathGamma))
            {
                UploadGammaImage.Visible = true;
                MessageGammaLabel.Visible = false;
            }

        }//fine if

    }//fine LoadButton_Click


    protected void ClearButton_Click(object sender, EventArgs e)
    {

        try
        {

            if (ImgBeta_RaidMode != 0)
            {
                UploadBetaImage.Visible = true;

                if (FileNameBetaTriggedLabel.Text.Length > 0)
                    File.Delete(Server.MapPath(TempPathBeta + FileNameBetaTriggedLabel.Text));

                if (IsEditMode)
                    FileNameEditBeta.Value = FileNameBetaTriggedLabel.Text;

                pFileName = FileNameBetaTriggedLabel.Text = GetFileNameFromPath(DefaultBetaImage);
                UploadBetaImage.ImageUrl = defaultBetaImage;
                InfoBetaLabel.Visible = false;
                MessageBetaLabel.Visible = true;
            }

            if (ImgGamma_RaidMode != 0)
            {
                UploadGammaImage.Visible = true;

                if (FileNameGammaTriggedLabel.Text.Length > 0)
                    File.Delete(Server.MapPath(TempPathGamma + FileNameGammaTriggedLabel.Text));

                if (IsEditMode)
                    FileNameEditGamma.Value = FileNameGammaTriggedLabel.Text;


                pFileNameTmb = FileNameGammaTriggedLabel.Text = GetFileNameFromPath(DefaultGammaImage);
                UploadGammaImage.ImageUrl = defaultGammaImage;
                InfoGammaLabel.Visible = false;
                MessageGammaLabel.Visible = true;
            }
        }
        catch (Exception p)
        {
            //Response.Write(p.ToString());
        }

    }//fine ClearButton_Click


    protected void ReloadButton_Click(object sender, EventArgs e)
    {
        try
        {
            SetDefaultEditBetaImage();
            SetDefaultEditGammaImage();
        }
        catch (Exception p)
        {
            //Response.Write(p.ToString());
        }

    }//fine ReloadButton_Click


    public void ShowBetaImage()
    {
        UploadBetaImage.Visible = true;
    }

    public void ShowGammaImage()
    {
        UploadGammaImage.Visible = true;
    }


    public string GetBetaTrigged()
    {
        return FileNameBetaTriggedLabel.Text;
    }

    public string GetGammaTrigged()
    {
        return FileNameGammaTriggedLabel.Text;

    }

    //public override void Dispose()
    //{
    //    base.Dispose();
    //    ClearTempPathBeta();
    //}


</script>

<script language="javascript">


    var isLoadButton = false;

    window.onbeforeunload = function () {
        if (isLoadButton) {
            $.blockUI({
                message: '<h1><img src="/SiteImg/wait.gif" /> Loading...</h1>',
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: '.5',
                    color: '#fff'
                }
            });//fine blockUIz
        }//fine if
    }//fine function

    function SetIsLoadButtonToTrue() {
        isLoadButton = true;
    }


</script>

<%--<asp:ScriptManager ID="myScriptManager" runat="server" />--%>
<asp:UpdatePanel ID="myUpdatePanel" runat="server" UpdateMode="conditional">
    <Triggers>
        <asp:PostBackTrigger ControlID="LoadButton" />
        <asp:PostBackTrigger ControlID="ClearButton" />
        <asp:PostBackTrigger ControlID="ReloadButton" />
    </Triggers>
    <ContentTemplate>
        <asp:Panel ID="ImageRaiderPanel" runat="server">
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="BoxDescriptionLabel" runat="server">Immagine</asp:Label>
                </div>
                <table>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="ComponentDescriptionLabel" runat="server" SkinID="FieldDescription"
                                Text="Label">Immagine</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <table cellpadding="5" cellspacing="0" border="0">
                                <tr>
                                    <td valign="middle">
                                        <asp:Label ID="FileNameBetaTriggedLabel" runat="server" Visible="false"></asp:Label>
                                        <asp:Label ID="FileNameGammaTriggedLabel" runat="server" Visible="false"></asp:Label>
                                        <asp:HiddenField ID="FileNameEditBeta" runat="server" />
                                        <asp:HiddenField ID="FileNameEditGamma" runat="server" />
                                        <table border="0" cellspacing="0" cellpadding="2">
                                            <tr>
                                                <td>
                                                    <asp:Panel ID="MessagePanel" runat="server">
                                                        <asp:Label ID="Message1Label" runat="server" SkinID="FileUploadNotes"></asp:Label><br />
                                                        <asp:Label ID="Message2Label" runat="server" SkinID="FileUploadNotes"></asp:Label><br />
                                                        <asp:Label ID="Message3Label" runat="server" SkinID="FileUploadNotes"></asp:Label><br />
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:FileUpload ID="FilUpl" runat="server" /><br />
                                                    <br />
                                                    <asp:Button ID="LoadButton" runat="server" Text="Carica immagine" OnClick="LoadButton_Click"
                                                        ValidationGroup="myLoadButtonValidationGroup" SkinID="ZSSM_Button01" />
                                                    <asp:Button ID="ClearButton" runat="server" Text="Predefinita" OnClick="ClearButton_Click"
                                                        SkinID="ZSSM_Button01" />
                                                    <asp:Button ID="ReloadButton" runat="server" Text="Rimuovi" OnClick="ReloadButton_Click"
                                                        SkinID="ZSSM_Button01" />
                                                    <asp:CustomValidator ID="ErrorMsg" runat="server" ErrorMessage="CustomValidator"
                                                        OnServerValidate="myServerValidate" SkinID="ZSSM_Validazione01" Display="Dynamic"></asp:CustomValidator>
                                                    <asp:CustomValidator ID="LoadButtonCustomValidator" runat="server" ErrorMessage="CustomValidator"
                                                        OnServerValidate="myServerValidate" SkinID="ZSSM_Validazione01" Display="Dynamic"
                                                        ValidationGroup="myLoadButtonValidationGroup"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td valign="top">
                                        <table border="0" cellspacing="0" cellpadding="2">
                                            <tr>
                                                <td>
                                                    <asp:Image ID="UploadBetaImage" runat="server" Style="max-width: 100%;" /><br />
                                                    <asp:Label ID="InfoBetaLabel" runat="server" Visible="false" SkinID="FileUploadNotes"></asp:Label><br />
                                                    <asp:Label ID="MessageBetaLabel" runat="server" SkinID="FileUploadNotes" Visible="false"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Image ID="UploadGammaImage" runat="server" Style="max-width: 100%;" /><br />
                                                    <asp:Label ID="InfoGammaLabel" runat="server" Visible="false" SkinID="FileUploadNotes"></asp:Label><br />
                                                    <asp:Label ID="MessageGammaLabel" runat="server" SkinID="FileUploadNotes" Visible="false"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
