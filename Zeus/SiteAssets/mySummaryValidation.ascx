﻿<%@ Control Language="C#" ClassName="mySummaryValidation" %>


<script runat="server">

    private string myCssClass = string.Empty;
    private string myErrorMessage = "Attenzione: i dati non sono stati salvati. Correggere gli errori nella pagina ed effettuare nuovamente il salvataggio.<br /><br />";
  
    public string CssClass
    {
        get { return myCssClass; }
        set { myCssClass = value; }
    }

    public string ErrorMessage
    {
        get { return myErrorMessage; }
        set { myErrorMessage = value; }
    }

    
  
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (myCssClass.Length > 0)
                ErrorLabel.CssClass = myCssClass;

            if (myErrorMessage.Length > 0)
                ErrorLabel.Text = myErrorMessage;

        }
    }//fine Page_Load

   
    
</script>

<script language="javascript">
function Validate()
{
      if (typeof(Page_ClientValidate) == 'function')
      {
            var res = Page_ClientValidate();

            if (res)
                  document.getElementById('<%=ErrorLabel.ClientID%>').style.display = 'none';
            else
                  document.getElementById('<%=ErrorLabel.ClientID%>').style.display = '';
      }//fine if
}//fine Validate
</script>
            <asp:Label runat="server" ID="ErrorLabel" SkinID="Validazione01" Style="display: none;" />
