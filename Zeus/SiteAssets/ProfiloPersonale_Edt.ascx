﻿<%@ Control Language="C#" ClassName="ProfiloPersonale_Edt" %>

<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">

    
    private string UserID = string.Empty;
    private string myRedirectPath = string.Empty;


    public string UID
    {
        set { UserID = value; }
        get { return UserID; }
    }

    public string RedirectPath
    {
        set { myRedirectPath = value; }
        get { return myRedirectPath; }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindTheData();
            
        }
    }//fine PageLoad


   

    public bool BindTheData()
    {
        try
        {

            ProfiloPersonaleSqlDataSource.SelectCommand += " WHERE ([UserID] = '" + UserID + "')";
           // ProfiloPersonaleSqlDataSource.UpdateCommand += " WHERE ([UserID] = '" + UserID + "')";
            ProfiloPersonaleFormView.DataSourceID = "ProfiloPersonaleSqlDataSource";
            ProfiloPersonaleFormView.DataBind();

            UIDHiddenField.Value = UserID;

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }


    }//fine BindTheData 


    
    protected void RecordEdtUser_DataBinding(object sender, EventArgs e)
    {
        HiddenField UtenteCreazione = (HiddenField)sender;
        MembershipUser user = Membership.GetUser();
        object userKey = user.ProviderUserKey;

        UtenteCreazione.Value = userKey.ToString();
    }

    protected void RecordEdtDate_DataBinding(object sender, EventArgs e)
    {
        HiddenField DataCreazione = (HiddenField)sender;
        DataCreazione.Value = DateTime.Now.ToString();

    }

    protected void TextBoxData_DataBinding(object sender, EventArgs e)
    {
        TextBox TextBoxData = (TextBox)sender;
        try
        {
            TextBoxData.Text = TextBoxData.Text.Substring(0, 10);

        }
        catch (Exception) { }
    }


    protected void ProfiloPersonaleSqlDataSource_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.Exception == null)
            if (myRedirectPath.Length > 0)
                Response.Redirect(myRedirectPath);
            else Response.Redirect("~/Zeus/System/Message.aspx");
    }//fine ProfiloPersonaleSqlDataSource_Updated
    
</script>

<asp:HiddenField ID="UIDHiddenField" runat="server" />
<asp:FormView ID="ProfiloPersonaleFormView" DefaultMode="Edit" runat="server" Width="100%">
    <EditItemTemplate>
        <asp:SqlDataSource ID="dsNazioni2" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
            SelectCommand="SELECT [ID1Nazione], [Nazione] FROM [tbNazioni] ORDER BY [Nazione]">
        </asp:SqlDataSource>
        <div class="BlockBox">
            <div class="BlockBoxHeader">
                <asp:Label ID="UtenteLabel21" runat="server" Text="Utente"></asp:Label></div>
            <table cellpadding="2" cellspacing="1" border="0">
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label1" SkinID="FieldDescription" runat="server">Cognome *</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TextBoxCognome" Text='<%# Bind("Cognome") %>' runat="server" Columns="50"
                            MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator SkinID="ZSSM_Validazione01" Display="Dynamic" ID="RequiredFieldValidator1"
                            runat="server" ControlToValidate="TextBoxCognome" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label2" SkinID="FieldDescription" runat="server">Nome *</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TextBoxNome" runat="server" Text='<%# Bind("Nome") %>' Columns="50"
                            MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator SkinID="ZSSM_Validazione01" Display="Dynamic" ID="RequiredFieldValidator2"
                            runat="server" ControlToValidate="TextBoxNome" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                    </td>
                </tr>
               
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label3" SkinID="FieldDescription" runat="server">Specializzazione</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TextBoxQualifica" Text='<%# Bind("Professione") %>' runat="server"
                            Columns="20" MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label14" SkinID="FieldDescription" runat="server">Titolo</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TextBox1" Text='<%# Bind("Qualifica") %>' runat="server"
                            Columns="10" MaxLength="10"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label SkinID="FieldDescription" ID="SessoLabel" runat="server" Text="Sesso"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:DropDownList ID="SessoDropDownlist"  runat="server" SelectedValue='<%# Bind("Sesso") %>' >
                                <asp:ListItem  Text="Non inserito" Value="X"></asp:ListItem>
                                <asp:ListItem  Text="Maschio" Value="M"></asp:ListItem>
                                <asp:ListItem  Text="Femmina" Value="F"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label4" SkinID="FieldDescription" runat="server">Codice fiscale personale</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TextBoxCodiceFiscale" Text='<%# Bind("CodiceFiscale") %>' runat="server"
                            Columns="16" MaxLength="16"></asp:TextBox>
                        <%--<asp:RegularExpressionValidator SkinID="ZSSM_Validazione01" Display="Dynamic" ID="CodiceFiscaleRegularExpressionValidator"
                            runat="server" ControlToValidate="TextBoxCodiceFiscale" ErrorMessage="Codice fiscale non corretto"
                            ValidationExpression="^[A-Za-z]{6}\d{2}[A-Za-z]\d{2}[A-Za-z]\d{3}[A-Za-z]$"></asp:RegularExpressionValidator>--%>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="LabelPartitaIva" SkinID="FieldDescription" runat="server" Text="Label">Partita IVA</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TextBoxPartitaIva" runat="server" Text='<%# Bind("PartitaIVA") %>' Columns="50"
                            MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>
        <div class="BlockBox">
            <div class="BlockBoxHeader">
                <asp:Label ID="Label5" runat="server" Text="Residenza"></asp:Label></div>
            <table cellpadding="2" cellspacing="1" border="0">
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label6" SkinID="FieldDescription" runat="server">Indirizzo</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TextBoxIndirizzo" Text='<%# Bind("Indirizzo") %>' runat="server"
                            Columns="50" MaxLength="50"></asp:TextBox>
                        <%--<asp:Label ID="Label7" SkinID="FieldDescription" runat="server"> Numero</asp:Label>
                        <asp:TextBox ID="TextBoxNumero" Text='<%# Bind("Numero") %>' runat="server" Columns="10"
                            MaxLength="10"></asp:TextBox>--%>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label8" SkinID="FieldDescription" runat="server">Città</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TextBoxCitta" Text='<%# Bind("Citta") %>' runat="server" Columns="50"
                            MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label9" SkinID="FieldDescription" runat="server">Comune</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TextBoxComune" runat="server" Columns="50" MaxLength="50" Text='<%# Bind("Comune") %>'></asp:TextBox>
                        <asp:Label ID="Label10" SkinID="FieldDescription" runat="server">&nbsp;CAP</asp:Label>
                        <asp:TextBox ID="TextBoxCAP" runat="server" Columns="5" MaxLength="5" Text='<%# Bind("Cap") %>'></asp:TextBox>
                        <asp:RangeValidator SkinID="ZSSM_Validazione01" Display="Dynamic" ID="RangeValidator1"
                            runat="server" ControlToValidate="TextBoxCAP" ErrorMessage="CAP non corretto"
                            MaximumValue="99999" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label11" SkinID="FieldDescription" runat="server">Provincia</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TextBoxProvincia" Text='<%# Bind("ProvinciaEstesa") %>' runat="server"
                            Columns="50" MaxLength="50"></asp:TextBox>
                        <asp:Label ID="Label12" SkinID="FieldDescription" runat="server">&nbsp;Sigla provincia</asp:Label>
                        <asp:TextBox ID="TextBoxSiglaProvincia" runat="server" Columns="2" MaxLength="2"
                            Text='<%# Bind("ProvinciaSigla") %>'></asp:TextBox>
                        <asp:RegularExpressionValidator SkinID="ZSSM_Validazione01" Display="Dynamic" ID="RegularExpressionValidator3"
                            runat="server" ControlToValidate="TextBoxSiglaProvincia" ErrorMessage="Sigla provincia non corretta"
                            ValidationExpression="^[A-Za-z]{2}$"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label13" SkinID="FieldDescription" runat="server">Nazione</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:DropDownList ID="DropDownListNazione" runat="server" DataSourceID="dsNazioni2"
                            SelectedValue='<%# Bind("ID2Nazione") %>' DataTextField="Nazione" DataValueField="ID1Nazione">
                            <asp:ListItem Text="&gt;  Non inserita" Selected="True" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                            SelectCommand="SELECT [ID1Nazione], [Nazione] FROM [tbNazioni] ORDER BY [Nazione]">
                        </asp:SqlDataSource>
                    </td>
                </tr>
            </table>
        </div>
        <div class="BlockBox">
            <div class="BlockBoxHeader">
                <asp:Label ID="NascitaLabel14" runat="server" Text="Nascita"></asp:Label></div>
            <table cellpadding="2" cellspacing="1" border="0">
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label15" SkinID="FieldDescription" runat="server">Data</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TextBoxData" Text='<%# Bind("NascitaData") %>' runat="server" Columns="10"
                            MaxLength="10" OnDataBinding="TextBoxData_DataBinding"></asp:TextBox>
                        <asp:RegularExpressionValidator SkinID="ZSSM_Validazione01" Display="Dynamic" ID="RegularExpressionValidator2"
                            runat="server" ControlToValidate="TextBoxData" ErrorMessage="Fomato data richiesto: GG/MM/AAAA"
                            ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label16" SkinID="FieldDescription" runat="server">Città</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TextBoxCittaNascita" Text='<%# Bind("NascitaCitta") %>' runat="server"
                            Columns="50" Rows="50"></asp:TextBox>
                        <asp:Label ID="Label17" SkinID="FieldDescription" runat="server">&nbsp;Sigla provincia</asp:Label>
                        <asp:TextBox ID="TextBoxProvinciaNascita" Text='<%# Bind("NascitaProvinciaSigla") %>'
                            runat="server" Columns="2" MaxLength="2"></asp:TextBox>
                        <asp:RegularExpressionValidator SkinID="ZSSM_Validazione01" Display="Dynamic" ID="RegularExpressionValidator5"
                            runat="server" ControlToValidate="TextBoxProvinciaNascita" ErrorMessage="Sigla provincia non corretta"
                            ValidationExpression="^[A-Za-z]{2}$"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label18" SkinID="FieldDescription" runat="server">Nazione</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:DropDownList ID="DropDownListNazioneNascita" runat="server" DataSourceID="dsNazioni2"
                            SelectedValue='<%# Bind("NascitaID2Nazione") %>' DataTextField="Nazione" DataValueField="ID1Nazione">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </div>
        <div class="BlockBox">
            <div class="BlockBoxHeader">
                <asp:Label ID="ContattiLabel14" runat="server" Text="Contatti"></asp:Label></div>
            <table cellpadding="2" cellspacing="1" border="0">
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label20" SkinID="FieldDescription" runat="server">Telefono</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TextBoxTelefonoCasa" Text='<%# Bind("Telefono1") %>' runat="server"
                            Columns="20" MaxLength="20"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label22" SkinID="FieldDescription" runat="server">Telefono cellulare</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TextBoxTelefonoCellulare" Text='<%# Bind("Telefono2") %>' runat="server"
                            Columns="20" Rows="20"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label23" SkinID="FieldDescription" runat="server">Fax</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TextBoxFax" runat="server" Text='<%# Bind("Fax") %>' Columns="20"
                            MaxLength="20"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label24" SkinID="FieldDescription" runat="server">Note</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <CustomWebControls:TextArea ID="TextArea1" runat="server" Text='<%# Bind("Note") %>'
                            Columns="100" Rows="5" Height="100" TextMode="MultiLine" MaxLength="199"></CustomWebControls:TextArea>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label25" SkinID="FieldDescription" runat="server">Affiliazione</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TextBoxAffiliazione" runat="server" Text='<%# Bind("Affiliazione") %>' Columns="50"
                            MaxLength="200"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label26" SkinID="FieldDescription" runat="server">Sito web</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:TextBox ID="TextBoxSitoWeb" runat="server" Text='<%# Bind("SitoWeb") %>' Columns="50"
                            MaxLength="200"></asp:TextBox>
                    </td>
                </tr>
            </table>
     </div>
        <asp:HiddenField ID="RecordEdtDate" runat="server" Value='<%# Bind("RecordEdtDate") %>'
            OnDataBinding="RecordEdtDate_DataBinding" />
        <asp:HiddenField ID="RecordEdtUser" runat="server" Value='<%# Bind("RecordEdtUser") %>'
            OnDataBinding="RecordEdtUser_DataBinding" />
        <div align="center" style="margin:0;padding:0 0 8px 0;border:0px;display:block;">
            <asp:LinkButton ID="UpdateButton" runat="server" SkinID="ZSSM_Button01" CommandName="Update"
                Text="Salva dati"></asp:LinkButton></div>
        <asp:HiddenField ID="zdtRecordNewUser" runat="server" Value='<%# Eval("RecordNewUser") %>' />
        <asp:HiddenField ID="zdtRecordNewDate" runat="server" Value='<%# Eval("RecordNewDate") %>' />
        <asp:HiddenField ID="zdtRecordEdtUser" runat="server" Value='<%# Eval("RecordEdtUser") %>' />
        <asp:HiddenField ID="zdtRecordEdtDate" runat="server" Value='<%# Eval("RecordEdtDate") %>' />
    </EditItemTemplate>
</asp:FormView>
<asp:SqlDataSource ID="ProfiloPersonaleSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
    SelectCommand="SELECT [UserId], [Cognome], [Nome], [CodiceFiscale], [PartitaIva], [Qualifica],[Professione],
    [Sesso], [Indirizzo], [Numero], [Citta], 
    [Cap], [ProvinciaSigla], [ID2Nazione],[Comune], [NascitaData],[ProvinciaEstesa], [NascitaProvinciaSigla], 
    [NascitaID2Nazione],[NascitaCitta], [Telefono1], [Telefono2], [Fax], [Note], [Affiliazione], [SitoWeb],
    [RecordNewDate],[RecordNewUser],[RecordEdtDate],[RecordEdtUser] FROM [tbProfiliPersonali]"
    UpdateCommand=" SET DATEFORMAT dmy; UPDATE tbProfiliPersonali SET Cognome =@Cognome, Nome =@Nome,CodiceFiscale =@CodiceFiscale, PartitaIva=@PartitaIva,
     Qualifica =@Qualifica,Professione=@Professione,Sesso =@Sesso, Indirizzo =@Indirizzo, Numero =@Numero, Citta =@Citta, Comune =@Comune, Cap =@Cap,
      ProvinciaEstesa =@ProvinciaEstesa, ProvinciaSigla =@ProvinciaSigla, ID2Nazione =@ID2Nazione, NascitaData =@NascitaData,
       NascitaCitta =@NascitaCitta, NascitaProvinciaSigla =@NascitaProvinciaSigla, NascitaID2Nazione =@NascitaID2Nazione, 
       Telefono1 =@Telefono1, Telefono2 =@Telefono2, Fax =@Fax, Note =@Note, Affiliazione = @Affiliazione, SitoWeb = @SitoWeb,
       RecordEdtUser =@RecordEdtUser, RecordEdtDate =@RecordEdtDate WHERE [UserID] =@UserID"
    OnUpdated="ProfiloPersonaleSqlDataSource_Updated">
    <UpdateParameters>
        <asp:Parameter Name="Cognome" Type="string" />
        <asp:Parameter Name="Nome" Type="string" />
        <asp:Parameter Name="CodiceFiscale" Type="string" />
        <asp:Parameter Name="PartitaIVA" Type="String" />
        <asp:Parameter Name="Titolo" Type="string" />
        <asp:Parameter Name="Specializzazione" Type="string" />
        <asp:Parameter Name="Sesso" Type="string" />
        <asp:Parameter Name="Indirizzo" Type="string" />
        <asp:Parameter Name="Numero" />
        <asp:Parameter Name="Citta" Type="string" />
        <asp:Parameter Name="Cap" />
        <asp:Parameter Name="ProvinciaSigla" Type="string" />
        <asp:Parameter Name="ID2Nazione" />
        <asp:Parameter Name="NascitaData" Type="datetime" />
        <asp:Parameter Name="NascitaProvinciaSigla" Type="string" />
        <asp:Parameter Name="NascitaID2Nazione" />
        <asp:Parameter Name="Telefono1" />
        <asp:Parameter Name="Telefono2" />
        <asp:Parameter Name="Fax" />
        <asp:Parameter Name="Note" />
        <asp:Parameter Name="Affiliazione" />
        <asp:Parameter Name="SitoWeb" />
        <asp:Parameter Name="RecordEdtUser" />
        <asp:Parameter Name="RecordEdtDate" Type="datetime" />
        <asp:ControlParameter ControlID="UIDHiddenField" Name="UserID"  />
    </UpdateParameters>
</asp:SqlDataSource>
