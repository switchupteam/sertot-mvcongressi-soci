﻿<%@ WebHandler Language="C#" Class="AsyncUpload" %>

using System;
using System.Web;
using System.Drawing;
using System.Web.SessionState;

public class AsyncUpload : IHttpHandler ,IRequiresSessionState
{


    private string CheckFileName(string filePath, string fileName)
    {
        int counter = 1;
        string finalPath = filePath;
        string strFile_name = System.IO.Path.GetFileNameWithoutExtension(fileName);
        string strFile_extension = System.IO.Path.GetExtension(fileName);

        if (System.IO.File.Exists(filePath + fileName))
        {
            filePath += fileName;
            while (System.IO.File.Exists(filePath))
            {
                counter++;
                filePath = System.IO.Path.Combine(finalPath, strFile_name + counter.ToString() + strFile_extension);

            }//fine while
            fileName = strFile_name + counter.ToString() + strFile_extension;
        }
        return fileName;
    }//fine CheckFileName
    
    public void ProcessRequest(HttpContext context)
    {
        //context.Response.ContentType = "text/plain";
        //context.Response.Write("Hello World");

        //http://blog.stevensanderson.com/2008/11/24/jquery-ajax-uploader-plugin-with-progress-bar/

        try
        {


            string filePath = "~/ZeusInc/TempUploadRaider/";

            //write your handler implementation here.
            if (context.Request.Files.Count <= 0)
            {
                context.Response.Write("No file uploaded");
            }
            else
            {
                for (int i = 0; i < context.Request.Files.Count; ++i)
                {
                    HttpPostedFile file = context.Request.Files[i];

                    string pFileName = CheckFileName(context.Server.MapPath(filePath), file.FileName);
                    
                    file.SaveAs(context.Server.MapPath(filePath + pFileName));
                    context.Response.Write("File uploaded");

                    if ((context.Request.QueryString["PAGE_ID"] != null)
                        && (context.Request.QueryString["PAGE_ID"].Length>0))
                        context.Session["FILENAME" + context.Request.QueryString["PAGE_ID"].ToString()] = pFileName;
                }
            }

            
            
        }
        catch (Exception p)
        {
            context.Response.Write(p.ToString());
        }
    }//fine ProcessRequest

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}//fine classe