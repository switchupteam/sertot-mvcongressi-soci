﻿<%@ Control Language="C#" ClassName="UploadRaider" %>
<%@ Import Namespace="System.IO" %>

<script runat="server">

    private string mySavePath = "/";


    public string FileTypeRange
    {
        set { FILE_TYPES.Value = value; }
        get { return FILE_TYPES.Value; }
    }


    

    public string INFOClientID
    {
        get { return INFO.ClientID; }
    }

    public string MaxSizeKb
    {
        set { FILE_SIZE_LIMIT.Value = value; }
        get { return FILE_SIZE_LIMIT.Value; }
    }

    public string SavePath
    {
        set { mySavePath = value; }
        get { return mySavePath; }
    }

    public string FileName
    {
        get
        {

            if (Session["FILENAME" + PAGE_ID.Value] != null)
                return Session["FILENAME" + PAGE_ID.Value].ToString();


            return string.Empty;

        }
    }


    public bool GotFile()
    {
        if (Session["FILENAME" + PAGE_ID.Value] != null)
            return true;


        return false;
    }

    public bool SaveFile()
    {
        try
        {
            string SourcePath = Server.MapPath("~/ZeusInc/TempUploadRaider/" + FileName);
            string DestinationPath = Server.MapPath(mySavePath + FileName);
            File.Copy(SourcePath, DestinationPath,true);

            try { File.Delete(SourcePath); }
            catch (Exception) { }


            return true;

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }//fine SaveFile


  
    protected void Page_Load(object sender, EventArgs e)
    {
        PAGE_ID.Value = this.ID;
        SESSION_ID.Value = Session.SessionID;

      
    }//fine Page_Load



    protected void Page_Unload(object sender, EventArgs e)
    {
        if (Session["FILENAME" + PAGE_ID.Value] != null)
            Session["FILENAME" + PAGE_ID.Value] = null;
    }
</script>

<style>
    DIV#MainContents
    {
        background-color: White;
        border: 1px solid gray;
    }
    H1
    {
        margin-top: 0;
        padding-top: 0;
    }
    DIV.ProgressBar
    {
        width: 300px;
        padding: 0;
        border: 1px solid black;
        margin-right: 1em;
        height: .75em;
        margin-left: 1em;
        display: -moz-inline-stack;
        display: inline-block;
        zoom: 1; *display:inline;}
    DIV.ProgressBar DIV
    {
        background-color: #666666;
        font-size: 1pt;
        height: 100%;
        float: left;
    }
</style>



<script>
/// jQuery plugin to add support for SwfUpload
/// (c) 2008 Steven Sanderson

(function($) {

   var PAGE_ID=$('#<%= PAGE_ID.ClientID %>').val();

    $.fn.makeAsyncUploader = function(options) {
        return this.each(function() {
            // Put in place a new container with a unique ID
            var id = $(this).attr("id"+PAGE_ID);
            var container = $("<span class='asyncUploader'/>");
            container.append($("<div class='ProgressBar'> <div>&nbsp;</div> </div>"));
            container.append($("<span id='" + id + "_completedMessage'/>"));
            container.append($("<span id='" + id + "_uploading' ><input type='button' value='Annulla' class='ZSSM_Button01_Button' /></span>"));
            container.append($("<span id='" + id + "_info' class='LabelSkin_CorpoTesto'></span>"));
            container.append($("<span id='" + id + "_swf'/>"));
            container.append($("<input type='hidden' name='" + id + "_filename'/>"));
            container.append($("<input type='hidden' name='" + id + "_guid'/>"));
            $(this).before(container).remove();
            $("div.ProgressBar", container).hide();
            $("span[id$=_uploading]", container).hide();
            $("span[id$=_info]", container).hide();


            // Instantiate the uploader SWF
            var swfu
            var width = 109, height = 22;
            if (options) {
                width = options.width || width;
                height = options.height || height;
            }
            var defaults = {
                //flash_url: "swfupload.swf",
                upload_url: "/Home/AsyncUpload",
                file_size_limit: "3 MB",
                file_types: "*.*",
                file_types_description: "All Files",
                debug: false,

                button_image_url: "blankButton.png",
                button_width: width,
                button_height: height,
                button_placeholder_id: id + "_swf",
                button_text: "<font face='Arial' size='13pt'>Choose file</span>",
                button_text_left_padding: (width - 70) / 2,
                button_text_top_padding: 1,
                

                // Called when the user chooses a new file from the file browser prompt (begins the upload)
                file_queued_handler: function(file) { swfu.startUpload(); },

                // Called when a file doesn't even begin to upload, because of some error
                file_queue_error_handler: function(file, code, msg) { alert("Attenzione, file non caricato: " + msg); },

                // Called when an error occurs during upload
                upload_error_handler: function(file, code, msg) { alert("Attenzione, file non caricato: " + msg); },

                // Called when upload is beginning (switches controls to uploading state)
                upload_start_handler: function() {
                    swfu.setButtonDimensions(0, height);
                    $("input[name$=_filename]", container).val("");
                    $("input[name$=_guid]", container).val("");
                    $("div.ProgressBar div", container).css("width", "0px");
                    $("div.ProgressBar", container).show();
                    $("span[id$=_uploading]", container).show();
                    $("span[id$=_info]", container).show();
                    $("span[id$=_completedMessage]", container).html("").hide();
                   
                    if (options.disableDuringUpload)
                        $(options.disableDuringUpload).attr("disabled", "disabled");
                        
                        $('#<%= ALLERT.ClientID %>').html("");
                        
                        $('#<%= INFO.ClientID %>').html("");
                        

                        
                        
                },

                // Called when upload completed successfully (puts success details into hidden fields)
                upload_success_handler: function(file, response) {
                    $("input[name$=_filename]", container).val(file.name);
                    $("input[name$=_guid]", container).val(response);
                    $("span[id$=_completedMessage]", container).html("Caricato <b>{0}</b> ({1} KB)"
                                .replace("{0}", file.name)
                                .replace("{1}", Math.round(file.size / 1024))
                            );
                    $("span[id$=_info]", container).hide();    
                     
                },

                // Called when upload is finished (either success or failure - reverts controls to non-uploading state)
                upload_complete_handler: function() {
                    var clearup = function() {
                        $("div.ProgressBar", container).hide();
                        $("span[id$=_completedMessage]", container).show();
                        $("span[id$=_uploading]", container).hide();
                        $("span[id$=_info]", container).hide();
                        swfu.setButtonDimensions(width, height);
                    };
                    if ($("input[name$=_filename]", container).val() != "") // Success
                        $("div.ProgressBar div", container).animate({ width: "100%" }, { duration: "fast", queue: false, complete: clearup });
                    else // Fail
                        clearup();

                    if (options.disableDuringUpload)
                        $(options.disableDuringUpload).removeAttr("disabled");
                        
                        
                },

                // Called periodically during upload (moves the Mauis bar along)
                upload_progress_handler: function(file, bytes, total) {
                    var percent = 100 * bytes / total;
                    $("div.ProgressBar div", container).animate({ width: percent + "%" }, { duration: 500, queue: false });
                    $("span[id$=_info]", container).html("Caricamento in corso (<b>{0} KB</b> su <b>{1} KB</b>)"
                     .replace("{0}", Math.round(bytes / 1024))
                     .replace("{1}", Math.round(total / 1024))
                     );
                }
            };
            swfu = new SWFUpload($.extend(defaults, options || {}));

            // Called when user clicks "cancel" (forces the upload to end, and eliminates progress bar immediately)
            $("span[id$=_uploading] input[type='button']", container).click(function() {
                swfu.cancelUpload(null, false);
            });

            // Give the effect of preserving state, if requested
            if (options.existingFilename || "" != "") {
                $("span[id$=_completedMessage]", container).html("Caricato <b>{0}</b> ({1} KB)"
                                .replace("{0}", options.existingFilename)
                                .replace("{1}", options.existingFileSize ? Math.round(options.existingFileSize / 1024) : "?")
                            ).show();
                $("input[name$=_filename]", container).val(options.existingFilename);
            }
            if (options.existingGuid || "" != "")
                $("input[name$=_guid]", container).val(options.existingGuid);
        });
    }
})(jQuery);
</script>

<script>
    $(function() {
    
            var FILE_SIZE_LIMIT = $('#<%= FILE_SIZE_LIMIT.ClientID %>').val();
            var FILE_TYPES=$('#<%= FILE_TYPES.ClientID %>').val();
            var PAGE_ID=$('#<%= PAGE_ID.ClientID %>').val();
            var SESSION_ID=$('#<%= SESSION_ID.ClientID %>').val();
           
            //alert(FILE_SIZE_LIMIT);
    
        $('#<%= yourID.ClientID %>').makeAsyncUploader({
            upload_url: "/Zeus/SiteAssets/UploadRaider/AsyncUpload.ashx?PAGE_ID="+PAGE_ID+"&DELINEA_ASPSESSID="+SESSION_ID, 
            flash_url: '/Zeus/SiteAssets/UploadRaider/swfupload.swf',
            button_image_url: '/Zeus/SiteAssets/UploadRaider/blankButton.png',
            file_size_limit: FILE_SIZE_LIMIT,
            button_text: "",
            debug:false,
            button_window_mode: "transparent",
            file_types:FILE_TYPES,
            
            
            
   
            
            file_queue_error_handler: function(file, code, msg) {
              $('#<%= INFO.ClientID %>').html("");
              $('#<%= ALLERT.ClientID %>').html("Il file supera la dimensione consentita di "+FILE_SIZE_LIMIT+" "); 
            
            },
            
           
            upload_error_handler: function(file, code, msg) { 
             $('#<%= INFO.ClientID %>').html("");

            $('#<%= ALLERT.ClientID %>').html("Il file supera la dimensione consentita di "+FILE_SIZE_LIMIT+" " ); 
           
            },
            
            
            upload_success_handler: function(file, response) {
                     $('#<%= ALLERT.ClientID %>').html("");
                     $('#<%= INFO.ClientID %>').html("Caricamento completato del file <b>{0}</b> ({1} KB)"
                                .replace("{0}", file.name)
                                .replace("{1}", Math.round(file.size / 1024)));
                    

                }


        
        });
    });        
</script>



<asp:HiddenField ID="FILE_SIZE_LIMIT" runat="server" Value="100 MB" />
<asp:HiddenField ID="FILE_TYPES" runat="server" Value="*.*" />
<asp:HiddenField ID="PAGE_ID" runat="server" />
<asp:HiddenField ID="SESSION_ID" runat="server" />

<input type="file" id="yourID" name="yourID"  runat="server" />

<asp:Label ID="ALLERT" runat="server" CssClass="ZSSM_Validazione01"></asp:Label>
<asp:Label ID="INFO" runat="server" CssClass="LabelSkin_CorpoTesto"></asp:Label>




    
