﻿<%@ Control Language="C#" ClassName="ProfiloPersonale_Dtl" %>

<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">

    
    
    private string UserID = string.Empty;

    public string UID
    {
        set { UserID = value; }
        get { return UserID; }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindTheData();
        }

    }//fine PageLoad



    
    
    public bool BindTheData()
    {
        try
        {

            ProfiloPersonaleSqlDataSource.SelectCommand += " WHERE ([UserID] = '" + UserID + "')";
            ProfiloPersonaleFormView.DataSourceID = "ProfiloPersonaleSqlDataSource";
            ProfiloPersonaleSqlDataSource.DataBind();

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }


    }//fine BindTheData 



   

    protected void Clear(object sender, EventArgs e)
    {
        Label Nazione = (Label)sender;
        Nazione.Text = Nazione.Text.Replace(">", "");
    }//fine Clear



    protected void SessoLabel_DataBinding(object sender, EventArgs e)
    {
        Label SessoLabel = (Label)sender;

        switch (SessoLabel.Text)
        { 
            case "M":
                SessoLabel.Text = "Maschio";
                break;

            case "F":
                SessoLabel.Text = "Femmina";
                break;
                
            default:
                SessoLabel.Text = "Non inserito";
                break;
        
        }//fine switch

    }//fine SessoLabel_DataBinding

</script>

<asp:FormView ID="ProfiloPersonaleFormView" runat="server" DataKeyField="UserId"
    DataSourceID="ProfiloPersonaleSqlDataSource" Width="100%">
    <ItemTemplate>
        <div class="BlockBoxHeader">
            <asp:Label ID="ProfiloPersonaleLabel1" runat="server" Text="Profilo personale"></asp:Label></div>
        <table border="0" cellpadding="2" cellspacing="2">
            <tr>
                <td>
                    <asp:Label ID="CognomeDescriptionLabel" SkinID="FieldDescription" runat="server"
                        Text="Cognome:"></asp:Label>
                    <asp:Label ID="CognomeLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("Cognome") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="NomeDescriptionLabel" SkinID="FieldDescription" runat="server">Nome:</asp:Label>
                    <asp:Label ID="NomeLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("Nome") %>'></asp:Label>
                </td>
            </tr>
            
            <tr>
                <td>
                    <asp:Label ID="CodiceFiscaleDescriptionLabel" SkinID="FieldDescription" runat="server">Codice fiscale:</asp:Label>
                    <asp:Label ID="CodiceFiscaleLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("CodiceFiscale") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="PartitaIVADescriptionLabel" SkinID="FieldDescription" runat="server">Partita IVA:</asp:Label>
                    <asp:Label ID="PartitaIVALabel" runat="server" SkinID="FieldValue" Text='<%# Eval("PartitaIVA") %>'>
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="QualificaDescriptionLabel" SkinID="FieldDescription" runat="server">Titolo:</asp:Label>
                    <asp:Label ID="QualificaLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("Qualifica") %>'></asp:Label>
                </td>
            </tr>
             <tr>
                <td>
                    <asp:Label ID="ProfessioneDescriptionLabel" SkinID="FieldDescription" runat="server">Specializzazione:</asp:Label>
                    <asp:Label ID="ProfessioneLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("Professione") %>'></asp:Label>
                </td>
            </tr>
             <tr>
                <td>
                    <asp:Label ID="SessoDescriptionLabel" SkinID="FieldDescription" runat="server">Sesso:</asp:Label>
                    <asp:Label ID="SessoLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("Sesso") %>' OnDataBinding="SessoLabel_DataBinding"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="IndirizzoDescriptionLabel" SkinID="FieldDescription" runat="server">Indirizzo:</asp:Label>
                    <asp:Label ID="IndirizzoLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("Indirizzo") %>'></asp:Label>
                </td>
            </tr>
           <%-- <tr>
                <td>
                    <asp:Label ID="NumeroDescriptionLabel" SkinID="FieldDescription" runat="server">Numero:</asp:Label>
                    <asp:Label ID="NumeroLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("Numero") %>'></asp:Label>
                </td>
            </tr>--%>
            <tr>
                <td>
                    <asp:Label ID="CittaDescriptionLabel" SkinID="FieldDescription" runat="server">Città:</asp:Label>
                    <asp:Label ID="CittaLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("Citta") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="CapDescriptionLabel" SkinID="FieldDescription" runat="server">CAP:</asp:Label>
                    <asp:Label ID="CapLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("Cap") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="ProvinciaSiglaDescriptionLabel" SkinID="FieldDescription" runat="server">Provincia (sigla):</asp:Label>
                    <asp:Label ID="ProvinciaSiglaLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("ProvinciaSigla") %>'>
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="NazioneDescriptionLabel" SkinID="FieldDescription" runat="server">Nazione:</asp:Label>
                    <asp:Label ID="NazioneLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("Nazione") %>'
                        OnDataBinding="Clear"></asp:Label>
                </td>
            </tr>
            <asp:Panel ID="ContinentePanel" runat="server" Visible="false">
                <tr>
                    <td class="BlockBoxDescription">
                        Continente:
                    </td>
                    <td class="BlockBoxValue">
                        <asp:Label ID="ContinenteLabel" runat="server" Text='<%# Eval("Continente") %>'>
                        </asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <tr>
                <td>
                    <asp:Label ID="NascitaDataDescriptionLabel" SkinID="FieldDescription" runat="server">Data di nascita:</asp:Label>
                    <asp:Label ID="NascitaDataLabel" runat="server"
                     SkinID="FieldValue" Text='<%# Eval("NascitaData","{0:d}") %>'>
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="NascitaProvinciaDescriptionLabel" SkinID="FieldDescription" runat="server">Provincia di nascita (sigla):</asp:Label>
                    <asp:Label ID="NascitaProvinciaSiglaLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("NascitaProvinciaSigla") %>'>
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="NascitaNazioneDEscriptionLabel" SkinID="FieldDescription" runat="server">Nazione di nascita:</asp:Label>
                    <asp:Label ID="NascitaNazioneLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("NascitaNazione") %>'
                        OnDataBinding="Clear">
                    </asp:Label>
                </td>
            </tr>
            <asp:Panel ID="ContinenteNascitaPanel" runat="server" Visible="false">
                <tr>
                    <td>
                        <asp:Label ID="NascitaContinenteDescriptionLabel" SkinID="FieldDescription" runat="server">Continente di nascita:</asp:Label>
                        <asp:Label ID="NascitaContinenteLabel" runat="server" Text='<%# Eval("NascitaContinente") %>'></asp:Label>
                    </td>
                </tr>
            </asp:Panel>
            <tr>
                <td>
                    <asp:Label ID="Telefono1DescriptionLabl" SkinID="FieldDescription" runat="server"> Telefono:</asp:Label>
                    <asp:Label ID="Telefono1Label" SkinID="FieldValue" runat="server" Text='<%# Eval("Telefono1") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Telefono2DescriptionLabel" SkinID="FieldDescription" runat="server">Telefono cellulare:</asp:Label>
                    <asp:Label ID="Telefono2Label" SkinID="FieldValue" runat="server" Text='<%# Eval("Telefono2") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="FaxDescriptionLabel" SkinID="FieldDescription" runat="server">Fax:</asp:Label>
                    <asp:Label ID="FaxLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("Fax") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="NoteDescriptionLabel" SkinID="FieldDescription" runat="server">Note:</asp:Label>
                    <asp:Label ID="NoteLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("Note") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="AffiliazioneDescriptionLabel" SkinID="FieldDescription" runat="server">Affiliazione:</asp:Label>
                    <asp:Label ID="AffiliazioneLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("Affiliazione") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="SitoWebDescriptionLabel" SkinID="FieldDescription" runat="server">Sito web:</asp:Label>
                    <asp:Label ID="SitoWebLabel" runat="server" SkinID="FieldValue" Text='<%# Eval("SitoWeb") %>'></asp:Label>
                </td>
            </tr
            <tr>
                <td align="center" colspan="2">
                    <asp:Label ID="EnabledLabel" runat="server" Visible="false" Text="Attenzione: dati non presenti."></asp:Label>
                </td>
            </tr>
            <tr>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
<asp:SqlDataSource ID="ProfiloPersonaleSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
    SelectCommand="SELECT [UserId], [Cognome], [Nome], [CodiceFiscale], [PartitaIVA], [Qualifica],[Professione],
    [Indirizzo], [Numero], [Citta], [Cap], [ProvinciaSigla], [Nazione], [Continente], 
    [NascitaData], [NascitaProvinciaSigla], [NascitaNazione], [NascitaContinente], [Telefono1], 
    [Telefono2], [Fax], [Note],[Sesso], [Affiliazione], [SitoWeb] FROM [vwProfiliPersonali]">
</asp:SqlDataSource>
