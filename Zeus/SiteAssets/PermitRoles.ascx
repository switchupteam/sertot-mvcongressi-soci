﻿<%@ Control Language="C#" ClassName="PermitRoles" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">

    
    private string mySQL_TABLE = string.Empty;
    private string myPAGE_TYPE = string.Empty;
    private string myZeusIdModulo = string.Empty;
    private string myZeusLangCode = string.Empty;
    private string myEDT_ID_BUTTON = string.Empty;
    private string myDEL_ID_BUTTON = string.Empty;
    private string myEMAIL_ID_BUTTON = string.Empty;

    private bool mySHOW_DTL_BUTTON_IN_LST = true;
    private bool mySHOW_EDT_BUTTON_IN_LST = true;
    private bool mySHOW_EMAIL_BUTTON_IN_LST = true;
    


    public string SQL_TABLE
    {
        get { return mySQL_TABLE; }
        set { mySQL_TABLE = value; }
    }

    public string PAGE_TYPE
    {
        get { return myPAGE_TYPE; }
        set { myPAGE_TYPE = value; }
    }

    public string ZeusIdModulo
    {
        get { return myZeusIdModulo; }
        set { myZeusIdModulo = value; }
    }


    public string ZeusLangCode
    {
        get { return myZeusLangCode; }
        set { myZeusLangCode = value; }
    }


    public string EDT_ID_BUTTON
    {
        get { return myEDT_ID_BUTTON; }
        set { myEDT_ID_BUTTON = value; }
    }

   
    public string DEL_ID_BUTTON
    {
        get { return myDEL_ID_BUTTON; }
        set { myDEL_ID_BUTTON = value; }
    }


    public string EMAIL_ID_BUTTON
    {
        get { return myEMAIL_ID_BUTTON; }
        set { myEMAIL_ID_BUTTON = value; }
    }

    public bool SHOW_DTL_BUTTON_IN_LST
    {
        get { return mySHOW_DTL_BUTTON_IN_LST; }
        set { mySHOW_DTL_BUTTON_IN_LST = value; }
    }

    public bool SHOW_EDT_BUTTON_IN_LST
    {
        get { return mySHOW_EDT_BUTTON_IN_LST; }
        set { mySHOW_EDT_BUTTON_IN_LST = value; }
    }

    public bool SHOW_EMAIL_BUTTON_IN_LST
    {
        get { return mySHOW_EMAIL_BUTTON_IN_LST; }
        set { mySHOW_EMAIL_BUTTON_IN_LST = value; }
    }


    /*########################################################################################################################*/


    protected void Page_Load(object sender, EventArgs e)
    {
        //BindTheData();
    }


    private bool CheckPermit(string AllRoles)
    {

        if (AllRoles.Length == 0)
            return false;

        char[] delimiter = { ',' };
        bool IsPermit = false;

        foreach (string roles in Roles.GetRolesForUser(Membership.GetUser().UserName))
        {
            string[] PressRoles = AllRoles.Split(delimiter);

            for (int i = 0; i < PressRoles.Length; ++i)
            {
                if (PressRoles[i].Trim().Equals(roles.Trim()))
                {
                    IsPermit = true;
                    break;
                }
            }//fine for

        }//fine else

        return IsPermit;

    }//fine CheckPermit

    private bool CheckParameters()
    {
        
            //if ((myPAGE_TYPE.Length > 0)
            //    && (mySQL_TABLE.Length > 0)
            //    && (myZeusIdModulo.Length > 0)
            //    && (myZeusLangCode.Length > 0))
                return true;
        

        return false;

    }//fine CheckParameters


    private bool AllowFromPZTable()
    {
        bool Allow = false;

        try
        {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            string sqlQuery = "SELECT ";

            switch (myPAGE_TYPE.ToUpper())
            {
                case "NEW":
                    sqlQuery += " PermitRoles_New ";
                    break;

                case "EDT":
                    sqlQuery += " PermitRoles_Edt ";
                    break;

                case "EDTCOM":
                    sqlQuery += " PermitRoles_Edt ";
                    break;
                    
                case "LST":
                    sqlQuery += " PermitRoles_Lst ";
                    break;

                case "LSTCOM":
                    sqlQuery += " PermitRoles_Lst ";
                    break;

                case "NEWLANG":
                    sqlQuery += " PermitRoles_NewLang ";
                    break;

                case "SRC":
                    sqlQuery += " PermitRoles_Src ";
                    break;

                case "DTL":
                    sqlQuery += " PermitRoles_Dtl ";
                    break;

                case "DTLCOM":
                    sqlQuery += " PermitRoles_Dtl ";
                    break;


                case "DEL":
                    sqlQuery += " PermitRoles_Del ";
                    break;

                case "BOA":
                    sqlQuery += " PermitRoles_Boa ";
                    break;

                case "CTRL":
                    sqlQuery += " PermitRoles_Ctrl ";
                    break;

                case "DUP":
                    sqlQuery += " PermitRoles_Dup ";
                    break;


                case "SND":
                    sqlQuery += " PermitRoles_SendEmail ";
                    break;
                    
            }//fine switch

            sqlQuery += " FROM " + mySQL_TABLE;
            sqlQuery += " WHERE ZeusIdModulo = '" + Server.HtmlEncode(myZeusIdModulo) + "' ";
            sqlQuery += " AND ZeusLangCode='" + Server.HtmlEncode(myZeusLangCode) + "'";
            //Response.Write(sqlQuery);

            using (SqlConnection conn = new SqlConnection(strConnessione))
            {
                SqlCommand command = new SqlCommand(sqlQuery, conn);

                conn.Open();
                SqlDataReader reader = command.ExecuteReader();
                /*fino a quando ci sono record*/
                while (reader.Read())
                    if (!reader.IsDBNull(0))
                        Allow = CheckPermit(reader.GetString(0));

                reader.Close();

            }//fine using

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());

        }

        return Allow;

    }//fine AllowFromPZTable


    private bool AllowButtonFromPZTable()
    {

        try
        {
            LinkButton myLinkButton = null;
            LinkButton mySecondLinkButton = null;
           
            bool IsMultipleButton = false;
            bool IsMultipleCommunicatorButton = false;
            delinea myDelinea = new delinea();

            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            string sqlQuery = "SELECT ";

            switch (myPAGE_TYPE.ToUpper())
            {

                case "EDT":

                    if (myDEL_ID_BUTTON.Length > 0)
                         myLinkButton =(LinkButton) myDelinea.FindControlRecursive(Page, myDEL_ID_BUTTON);
                    
                    sqlQuery += " PermitRoles_Del ";
                    break;

                case "LST":

                    IsMultipleButton = true;

                    sqlQuery += " PermitRoles_Edt , PermitRoles_Dtl ";
                    break;


                case "LSTCOM":

                    IsMultipleCommunicatorButton = true;

                    sqlQuery += " PermitRoles_Edt , PermitRoles_Dtl,PermitRoles_SendEmail ";
                    break;

                case "DTL":
                    
                    if (myEDT_ID_BUTTON.Length > 0)
                        myLinkButton = (LinkButton)myDelinea.FindControlRecursive(Page, myEDT_ID_BUTTON);

                    if (myDEL_ID_BUTTON.Length > 0)
                        mySecondLinkButton = (LinkButton)myDelinea.FindControlRecursive(Page, myDEL_ID_BUTTON);

                    IsMultipleButton = true;

                    sqlQuery += " PermitRoles_Edt, PermitRoles_Del";
                    break;

                case "DTLCOM":

                    if (myEDT_ID_BUTTON.Length > 0)
                        myLinkButton = (LinkButton)myDelinea.FindControlRecursive(Page, myEDT_ID_BUTTON);



                    if (myEMAIL_ID_BUTTON.Length > 0)
                        mySecondLinkButton = (LinkButton)myDelinea.FindControlRecursive(Page, myEMAIL_ID_BUTTON);

                    IsMultipleButton = true;

                    sqlQuery += " PermitRoles_Edt, PermitRoles_SendEmail";
                    break;
                    
                default:
                    return false;
                    break;


            }//fine switch


            sqlQuery += " FROM " + mySQL_TABLE;
            sqlQuery += " WHERE ZeusIdModulo = '" + Server.HtmlEncode(myZeusIdModulo) + "' ";
            sqlQuery += " AND ZeusLangCode='" + Server.HtmlEncode(myZeusLangCode) + "'";
            //Response.Write(sqlQuery);

            using (SqlConnection conn = new SqlConnection(strConnessione))
            {
                SqlCommand command = new SqlCommand(sqlQuery, conn);

                conn.Open();
                SqlDataReader reader = command.ExecuteReader();
                /*fino a quando ci sono record*/
                while (reader.Read())
                {

                    if (IsMultipleCommunicatorButton)
                    {
                        //PermitRoles_Edt , PermitRoles_Dtl,PermitRoles_SendEmail 

                        if (reader["PermitRoles_Edt"] != DBNull.Value)
                        {
                            bool myPermit = CheckPermit(reader["PermitRoles_Edt"].ToString());
                            mySHOW_EDT_BUTTON_IN_LST = myPermit;
                        }

                        if (reader["PermitRoles_Dtl"] != DBNull.Value)
                        {
                            bool myPermit = CheckPermit(reader["PermitRoles_Dtl"].ToString());
                            mySHOW_DTL_BUTTON_IN_LST = myPermit;
                        }

                        if (reader["PermitRoles_SendEmail"] != DBNull.Value)
                        {
                            bool myPermit = CheckPermit(reader["PermitRoles_SendEmail"].ToString());
                            mySHOW_EMAIL_BUTTON_IN_LST = myPermit;
                        }
                            
                    }
                    else
                    {


                        if (!reader.IsDBNull(0))
                        {
                            bool myPermit = CheckPermit(reader.GetString(0));

                            if (myLinkButton != null)
                                myLinkButton.Visible = myPermit;

                            mySHOW_EDT_BUTTON_IN_LST = myPermit;

                        }

                        if ((IsMultipleButton)
                            && (!reader.IsDBNull(1)))
                        {
                            bool myPermit = CheckPermit(reader.GetString(1));

                            if (mySecondLinkButton != null)
                                mySecondLinkButton.Visible = myPermit;

                            mySHOW_DTL_BUTTON_IN_LST = myPermit;
                        }

                    }//fine else

                    
                    
                }//fine while 


               // Response.Write("DEB "+mySHOW_DTL_BUTTON_IN_LST+"<br />");       
                reader.Close();

            }//fine using

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());

        }


        return true;
    }//fine AllowButtonFromPZTable



    public void BindTheData()
    {
        if (CheckParameters())
        {
            AllowButtonFromPZTable();
            
            if (!AllowFromPZTable())
                Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223599");
        }

    }//fine BindTheData



    
</script>

