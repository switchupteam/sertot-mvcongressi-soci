﻿<%@ Control Language="C#" ClassName="SiteMenuNew" CodeFile="ZeusSiteMenuNew.ascx.cs" Inherits="Zeus_SiteAssets_ZeusSiteMenuNew" %>

<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<asp:Repeater runat="server" ID="MenuLv1_Repeater" OnItemDataBound="MenuLv1_Repeater_ItemDataBound">
    <HeaderTemplate>
        <ul class="MenuLv1">
    </HeaderTemplate>
    <ItemTemplate>
        <li>
            <a class="linkLv1" href="<%# Eval("Url") %>"><%# Eval("Titolo") %></a>

            <asp:Repeater runat="server" ID="MenuLv2_Repeater" OnItemDataBound="MenuLv2_Repeater_ItemDataBound">
                <HeaderTemplate>
                    <ul class="MenuLv2">
                </HeaderTemplate>
                <ItemTemplate>
                    <li>
                        <a class="linkLv2" href="<%# Eval("Url") %>"><%# Eval("Titolo") %></a>
                        <span runat="server" id="freccia">></span>

                        <asp:Repeater runat="server" ID="MenuLv3_Repeater">
                            <HeaderTemplate>
                                <ul class="MenuLv3">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li>
                                    <a class="linkLv3" href="<%# Eval("Url") %>"><%# Eval("Titolo") %></a>
                                </li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ul>
                            </FooterTemplate>
                        </asp:Repeater>
                    </li>
                </ItemTemplate>
                <FooterTemplate>
                    </ul>
                </FooterTemplate>
            </asp:Repeater>
        </li>
    </ItemTemplate>
    <FooterTemplate>
        </ul>
    </FooterTemplate>
</asp:Repeater>
