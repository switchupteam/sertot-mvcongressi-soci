﻿<%@ Control Language="C#" ClassName="dlLoadFile" %>

<script runat="server">

    
    private bool pGotFile;
    private string[] pFileTypeRange;
    private bool Ind = false;

    //private int minSize = 0;
    //private int maxSize = 0;

    private string pFileExt;
    private string pFileName;
    private HttpPostedFile pFilePost;

    private bool pRequired;
    private string pVgroup;

    private string sizeError = string.Empty;
    private string formatError = string.Empty;
    private string requiredError = string.Empty;
    private string savePath = string.Empty;

    private bool showInfo = true;

    private string myValidateButtonID = string.Empty;


    public string ValidateButtonID
    {
        get { return myValidateButtonID; }
        set { myValidateButtonID = value; }
    }
    
    
    //Get function

    public bool IsValid
    {
        get { return ErrorMsg.IsValid; }
    }
    public bool GotFile
    {
        get { return pGotFile; }
    }
    public string FileExt
    {
        get { return pFileExt; }
    }
    public string FileName
    {
        get { return pFileName; }
        set { pFileName = value; }
    }
    public HttpPostedFile FilePost
    {
        get { return pFilePost; }
    }

    public string SavePath
    {
        set { savePath = value; }
        get { return savePath; }
    }

    public bool Required
    {
        set { pRequired = value; }
    }

    public string Vgroup
    {
        set { pVgroup = value; }
    }

    public string FileTypeRange
    {
        set { pFileTypeRange = value.ToString().Split(','); }
    }


    //public int MaxSizeKb
    //{
    //    set { maxSize = value; }
    //}

    //public int MinSizeKb
    //{
    //    set { minSize = value; }
    //}

    //public string SizeErrorMessage
    //{
    //    set { sizeError = value; }
    //}

    public string FormatErrorMessage
    {
        set { formatError = value; }
    }

    public string RequiredErrorMessage
    {
        set { requiredError = value; }
    }


    public bool ShowInfo
    {
        set { showInfo = value; }
        get { return showInfo; }
    }




    private Control FindControlRecursive(Control Root, string Id)
    {
        if (Root.ID == Id)
            return Root;

        foreach (Control Ctl in Root.Controls)
        {
            Control FoundCtl = FindControlRecursive(Ctl, Id);
            if (FoundCtl != null)
                return FoundCtl;
        }

        return null;
    }//fine FindControlRecursive
    
    protected void Page_Init()
    {
        try
        {
            ErrorMsg.ValidationGroup = pVgroup;
            MessagePanel.Visible = showInfo;
            Message1Label.Text = "Formati consentiti: ";
            if (pFileTypeRange != null)
                foreach (string str in pFileTypeRange)
                    Message1Label.Text += str + ", ";

            Message1Label.Text = Message1Label.Text.Substring(0, Message1Label.Text.Length - 2);
            //Message3Label.Text = "Peso massimo: " + maxSize + "Kb";
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }//fine Page_Init


    public bool isValid()
    {
        return Page.IsValid;

    }//fine isValid


    public void SetInfoMessageAuto()
    {
        Message1Label.Text = "Formati consentiti: ";
        if (pFileTypeRange != null)
            foreach (string str in pFileTypeRange)
                Message1Label.Text += str + ", ";

        Message1Label.Text.Remove(Message1Label.Text.Length - 1, 1);
        //Message3Label.Text = "Peso massimo: " + maxSize.ToString() + " Kb";

    }//fine SetInfoMessageAuto


    private string CheckFileName(string filePath, string fileName)
    {

        int counter = 1;
        string finalPath = filePath;
        string strFile_name = System.IO.Path.GetFileNameWithoutExtension(fileName);
        string strFile_extension = System.IO.Path.GetExtension(fileName);

        if (System.IO.File.Exists(filePath + fileName))
        {

            // il file è già sul server
            filePath += fileName;
            while (System.IO.File.Exists(filePath))
            {
                counter++;
                filePath = System.IO.Path.Combine(finalPath, strFile_name + counter.ToString() + strFile_extension);

            }//fine while


            fileName = strFile_name + counter.ToString() + strFile_extension;


        }
        //else fileName =strFile_name;

        return fileName;

    }//fine CheckFileName


    public bool SaveFile()
    {
        try
        {

            string filePath = System.Web.HttpContext.Current.Server.MapPath(this.savePath);

            if (FilUpl.HasFile)
            {
                pFileName = CheckFileName(filePath, FilUpl.FileName.Replace(" ","_"));
                pFileName = pFileName.Replace(" ", "_");
                FilUpl.SaveAs(filePath + pFileName);
                return true;
            }

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

        return false;
    }//fine SaveFile



    private string GetExtension(string FileName)
    {
        string[] split = FileName.Split('.');
        string Extension = split[split.Length - 1];
        return Extension;

    }//fine GetExtension


    protected void ErrorMsg_ServerValidate(object source, ServerValidateEventArgs args)
    {
        try
        {

            if (FilUpl.HasFile)
            {
                pGotFile = true;
                pFileExt = GetExtension(FilUpl.PostedFile.FileName);
                pFilePost = FilUpl.PostedFile;
                pFileName = FilUpl.FileName;

                if (pFileTypeRange != null)
                {
                    foreach (string str in pFileTypeRange)
                    {
                        if (str.ToLower().Equals(pFileExt.ToLower()))
                        {
                            Ind = true;
                            break;
                        }
                    }
                }
                else Ind = true;

                if (!Ind)
                {
                    args.IsValid = false;
                    if (formatError.Length == 0)
                        ErrorMsg.Text = "Formato del file non consentito";
                    else ErrorMsg.Text = formatError;
                    return;
                }


                //if (minSize > 0)
                //{

                //    if (FilUpl.PostedFile.ContentLength < (minSize * 1024))
                //    {
                //        args.IsValid = false;
                //        if (sizeError.Length == 0)
                //            ErrorMsg.Text = "Il file non supera il peso minimo";
                //        else ErrorMsg.Text = sizeError;
                //        return;
                //    }
                //}
                //if (maxSize > 0)
                //{

                //    if (FilUpl.PostedFile.ContentLength > (maxSize * 1024))
                //    {
                //        args.IsValid = false;
                //        if (sizeError.Length == 0)
                //            ErrorMsg.Text = "Il file supera il peso massimo";
                //        else ErrorMsg.Text = sizeError;
                //        return;
                //    }
                //}

            }//fine if
            else
            {

                pGotFile = false;
                if (pRequired)
                {
                    args.IsValid = false;
                    if (requiredError.Length == 0)
                        ErrorMsg.Text = "Attenzione il file upload e' vuoto";
                    else ErrorMsg.Text = requiredError;
                }

            }//fine else


            LinkButton SaveButton = (LinkButton)FindControlRecursive(Page, myValidateButtonID);

            if (SaveButton != null)
                if (!args.IsValid)
                    SaveButton.CommandName = string.Empty;
            
            
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }//fine ErrorMsg_ServerValidate
    
    
</script>

<table border="0" cellspacing="0" cellpadding="2">
    <tr>
        <td>
            <asp:FileUpload ID="FilUpl" runat="server" />
            <asp:CustomValidator ID="ErrorMsg" runat="server" ErrorMessage="CustomValidator"
                OnServerValidate="ErrorMsg_ServerValidate" SkinID="ZSSM_Validazione01" Display="Dynamic"></asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Panel ID="MessagePanel" runat="server">
                <asp:Label ID="Message1Label" runat="server" SkinID="FileUploadNotes"></asp:Label><br />
                <asp:Label ID="Message3Label" runat="server" SkinID="FileUploadNotes"></asp:Label><br />
            </asp:Panel>
        </td>
    </tr>
</table>
