﻿<%@ Page Language="C#" %>

<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Drawing.Imaging" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
   
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            delinea myDelinea = new delinea();

            //Se l'immagine beta non ha bisogno di crop, non passo i parametri, quindi controllo che non esista il primo parametro per escludere questa operazione sull'immagine
            if (Request.QueryString["FILE_BETA_NAME"] != null)
            {
                if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "FILE_BETA_NAME"))
                    FILE_BETA_NAME.Value = Request.QueryString["FILE_BETA_NAME"];

                if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "BETA_X"))
                    BETA_X.Value = Request.QueryString["BETA_X"];

                if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "BETA_Y"))
                    BETA_Y.Value = Request.QueryString["BETA_Y"];

                if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "BETA_W"))
                    BETA_W.Value = Request.QueryString["BETA_W"];

                if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "BETA_H"))
                    BETA_H.Value = Request.QueryString["BETA_H"];

                if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "BETA_WE"))
                    BETA_WE.Value = Request.QueryString["BETA_WE"];



                string ImageBETA_Name = FILE_BETA_NAME.Value;

                int BETA_w = Convert.ToInt32(BETA_W.Value);
                int BETA_h = Convert.ToInt32(BETA_H.Value);
                int BETA_x = Convert.ToInt32(BETA_X.Value);
                int BETA_y = Convert.ToInt32(BETA_Y.Value);

                Response.Write("DEB ImageName: " + ImageBETA_Name + "<br />");

                byte[] BETA_CropImage = Crop(Server.MapPath("~/ZeusInc/TempBeta/") + ImageBETA_Name, BETA_w, BETA_h, BETA_x, BETA_y);

                using (MemoryStream ms = new MemoryStream(BETA_CropImage, 0, BETA_CropImage.Length))
                {

                    ms.Write(BETA_CropImage, 0, BETA_CropImage.Length);

                    using (System.Drawing.Image CroppedImage = System.Drawing.Image.FromStream(ms, true))
                    {
                        //MODIFICA per errate dimensioni immagini in output
                        //aggiunta conversione immagine in jpeg
                        long quality = 100;
                        System.Drawing.Imaging.ImageCodecInfo jgpEncoder = GetEncoder(System.Drawing.Imaging.ImageFormat.Jpeg);

                        // Create an Encoder object based on the GUID
                        // for the Quality parameter category.
                        System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;

                        // Create an EncoderParameters object.
                        // An EncoderParameters object has an array of EncoderParameter
                        // objects. In this case, there is only one
                        // EncoderParameter object in the array.
                        System.Drawing.Imaging.EncoderParameters myEncoderParameters = new System.Drawing.Imaging.EncoderParameters(1);

                        System.Drawing.Imaging.EncoderParameter myEncoderParameter = new System.Drawing.Imaging.EncoderParameter(myEncoder, quality);
                        myEncoderParameters.Param[0] = myEncoderParameter;
                        MemoryStream msApp = new MemoryStream();
                        //CroppedImage.Save(Server.MapPath("~/ZeusInc/TempBeta/") + ImageBETA_Name, jgpEncoder, myEncoderParameters);
                        CroppedImage.Save(msApp, jgpEncoder, myEncoderParameters);
                        while ((msApp.Length > (Convert.ToInt32(BETA_WE.Value) * (1000))) && (quality > 1))
                        {
                            myEncoder = System.Drawing.Imaging.Encoder.Quality;
                            myEncoderParameters = new System.Drawing.Imaging.EncoderParameters(1);
                            myEncoderParameter = new System.Drawing.Imaging.EncoderParameter(myEncoder, quality--);
                            myEncoderParameters.Param[0] = myEncoderParameter;
                            msApp.Close();
                            msApp = new MemoryStream();
                            CroppedImage.Save(msApp, jgpEncoder, myEncoderParameters);
                        }
                        Response.Write(msApp.Length);
                        CroppedImage.Save(Server.MapPath("~/ZeusInc/TempBeta/") + ImageBETA_Name, jgpEncoder, myEncoderParameters);
                        //CroppedImage.Save(Server.MapPath("~/ZeusInc/TempBeta/") + ImageBETA_Name);
                    }

                }//fine using

            } 
            
            
            /*---------------------------------------------------------------------------------------------------*/

            
             //Se l'immagine gamma non ha bisogno di crop, non passo i parametri, quindi controllo che non esista il primo parametro per escludere questa operazione sull'immagine
            if (Request.QueryString["FILE_GAMMA_NAME"] != null)
            {
                if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "FILE_GAMMA_NAME"))
                    FILE_GAMMA_NAME.Value = Request.QueryString["FILE_GAMMA_NAME"];

                if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "GAMMA_X"))
                    GAMMA_X.Value = Request.QueryString["GAMMA_X"];

                if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "GAMMA_Y"))
                    GAMMA_Y.Value = Request.QueryString["GAMMA_Y"];

                if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "GAMMA_W"))
                    GAMMA_W.Value = Request.QueryString["GAMMA_W"];

                if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "GAMMA_H"))
                    GAMMA_H.Value = Request.QueryString["GAMMA_H"];

                if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "GAMMA_WE"))
                    GAMMA_WE.Value = Request.QueryString["GAMMA_WE"];

                string ImageGAMMA_Name = FILE_GAMMA_NAME.Value;

                int GAMMA_w = Convert.ToInt32(GAMMA_W.Value);
                int GAMMA_h = Convert.ToInt32(GAMMA_H.Value);
                int GAMMA_x = Convert.ToInt32(GAMMA_X.Value);
                int GAMMA_y = Convert.ToInt32(GAMMA_Y.Value);

                Response.Write("DEB ImageName: " + ImageGAMMA_Name + "<br />");

                byte[] GAMMA_CropImage = Crop(Server.MapPath("~/ZeusInc/TempGamma/") + ImageGAMMA_Name, GAMMA_w, GAMMA_h, GAMMA_x, GAMMA_y);

                using (MemoryStream ms = new MemoryStream(GAMMA_CropImage, 0, GAMMA_CropImage.Length))
                {

                    ms.Write(GAMMA_CropImage, 0, GAMMA_CropImage.Length);

                    using (System.Drawing.Image CroppedImage = System.Drawing.Image.FromStream(ms, true))
                    {
                        long quality = 100;
                        System.Drawing.Imaging.ImageCodecInfo jgpEncoder = GetEncoder(System.Drawing.Imaging.ImageFormat.Jpeg);

                        // Create an Encoder object based on the GUID
                        // for the Quality parameter category.
                        System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;

                        // Create an EncoderParameters object.
                        // An EncoderParameters object has an array of EncoderParameter
                        // objects. In this case, there is only one
                        // EncoderParameter object in the array.
                        System.Drawing.Imaging.EncoderParameters myEncoderParameters = new System.Drawing.Imaging.EncoderParameters(1);

                        System.Drawing.Imaging.EncoderParameter myEncoderParameter = new System.Drawing.Imaging.EncoderParameter(myEncoder, 90L);
                        myEncoderParameters.Param[0] = myEncoderParameter;

                        MemoryStream msApp = new MemoryStream();
                        CroppedImage.Save(msApp, jgpEncoder, myEncoderParameters);
                        while ((msApp.Length > (Convert.ToInt32(GAMMA_WE.Value) * (1000))) && (quality > 1))
                        {
                            myEncoder = System.Drawing.Imaging.Encoder.Quality;
                            myEncoderParameters = new System.Drawing.Imaging.EncoderParameters(1);
                            myEncoderParameter = new System.Drawing.Imaging.EncoderParameter(myEncoder, quality--);
                            myEncoderParameters.Param[0] = myEncoderParameter;
                            msApp.Close();
                            msApp = new MemoryStream();
                            CroppedImage.Save(msApp, jgpEncoder, myEncoderParameters);
                        }
                        //Response.Write(msApp.Length);
                        CroppedImage.Save(Server.MapPath("~/ZeusInc/TempGamma/") + ImageGAMMA_Name, jgpEncoder, myEncoderParameters);
                    }

                }//fine using
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine CropButton_Click




    static byte[] Crop(string Img, int Width, int Height, int X, int Y)
    {

        try
        {

            using (System.Drawing.Image OriginalImage = System.Drawing.Image.FromFile(Img))
            {

                using (System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(Width, Height))
                {

                    bmp.SetResolution(OriginalImage.HorizontalResolution, OriginalImage.VerticalResolution);

                    using (System.Drawing.Graphics Graphic = System.Drawing.Graphics.FromImage(bmp))
                    {

                        Graphic.SmoothingMode = SmoothingMode.AntiAlias;

                        Graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;

                        Graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;

                        Graphic.DrawImage(OriginalImage, new System.Drawing.Rectangle(0, 0, Width, Height), X, Y, Width, Height, System.Drawing.GraphicsUnit.Pixel);

                        MemoryStream ms = new MemoryStream();

                        bmp.Save(ms, OriginalImage.RawFormat);

                        return ms.GetBuffer();

                    }//fine using

                }//fine using

            }//fine using

        }

        catch (Exception Ex)
        {

            throw (Ex);

        }

    }//fine Crop
    private static System.Drawing.Imaging.ImageCodecInfo GetEncoder(System.Drawing.Imaging.ImageFormat format)
    {

        System.Drawing.Imaging.ImageCodecInfo[] codecs = System.Drawing.Imaging.ImageCodecInfo.GetImageDecoders();

        foreach (System.Drawing.Imaging.ImageCodecInfo codec in codecs)
        {
            if (codec.FormatID == format.Guid)
            {
                return codec;
            }
        }
        return null;
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:HiddenField ID="FILE_BETA_NAME" runat="server" />
        <asp:HiddenField ID="BETA_X" runat="server" />
        <asp:HiddenField ID="BETA_Y" runat="server" />
        <asp:HiddenField ID="BETA_W" runat="server" />
        <asp:HiddenField ID="BETA_H" runat="server" />
        <asp:HiddenField ID="BETA_WE" runat="server" />
        <asp:HiddenField ID="FILE_GAMMA_NAME" runat="server" />
        <asp:HiddenField ID="GAMMA_X" runat="server" />
        <asp:HiddenField ID="GAMMA_Y" runat="server" />
        <asp:HiddenField ID="GAMMA_W" runat="server" />
        <asp:HiddenField ID="GAMMA_H" runat="server" />
        <asp:HiddenField ID="GAMMA_WE" runat="server" />
    </div>
    </form>
</body>
</html>
