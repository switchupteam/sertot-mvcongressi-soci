﻿<%@ WebHandler Language="C#" Class="AsyncUpload" %>

using System;
using System.Web;
using System.Drawing;
using System.Web.SessionState;

public class AsyncUpload : IHttpHandler, IRequiresSessionState
{


    private string CheckFileName(string filePath, string fileName)
    {
        int counter = 1;
        string finalPath = filePath;
        string strFile_name = System.IO.Path.GetFileNameWithoutExtension(fileName);
        string strFile_extension = System.IO.Path.GetExtension(fileName);

        if (System.IO.File.Exists(filePath + fileName))
        {
            filePath += fileName;
            while (System.IO.File.Exists(filePath))
            {
                counter++;
                filePath = System.IO.Path.Combine(finalPath, strFile_name + counter.ToString() + strFile_extension);

            }//fine while
            fileName = strFile_name + counter.ToString() + strFile_extension;
        }
        return fileName;
    }//fine CheckFileName



    private static System.Drawing.Image resizeImage(System.Drawing.Image imgToResize, System.Drawing.Size size)
    {
        int sourceWidth = imgToResize.Width;
        int sourceHeight = imgToResize.Height;

        float nPercent = 0;
        float nPercentW = 0;
        float nPercentH = 0;

        nPercentW = ((float)size.Width / (float)sourceWidth);
        nPercentH = ((float)size.Height / (float)sourceHeight);

        if (nPercentH < nPercentW)
            nPercent = nPercentH;
        else
            nPercent = nPercentW;

        int destWidth = (int)(sourceWidth * nPercent);
        int destHeight = (int)(sourceHeight * nPercent);

        System.Drawing.Bitmap b = new System.Drawing.Bitmap(destWidth, destHeight);
        System.Drawing.Graphics g = System.Drawing.Graphics.FromImage((System.Drawing.Image)b);
        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

        g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
        g.Dispose();

        return (System.Drawing.Image)b;
    }//fine resizeImage

    private static System.Drawing.Image resizeImageR(System.Drawing.Image imgToResize, System.Drawing.Size size)
    {
        int sourceWidth = imgToResize.Width;
        int sourceHeight = imgToResize.Height;

        float nPercent = 0;
        float nPercentW = 0;
        float nPercentH = 0;

        nPercentW = ((float)size.Width / (float)sourceWidth);
        nPercentH = ((float)size.Height / (float)sourceHeight);

        if (nPercentH > nPercentW)
            nPercent = nPercentH;
        else
            nPercent = nPercentW;

        int destWidth = (int)(sourceWidth * nPercent);
        int destHeight = (int)(sourceHeight * nPercent);

        System.Drawing.Bitmap b = new System.Drawing.Bitmap(destWidth, destHeight);
        System.Drawing.Graphics g = System.Drawing.Graphics.FromImage((System.Drawing.Image)b);
        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

        g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
        g.Dispose();

        return (System.Drawing.Image)b;
    }//fine resizeImageR


    private static System.Drawing.Image ResizeImageForce(System.Drawing.Image imgToResize, System.Drawing.Size size)
    {

        System.Drawing.Bitmap b = new System.Drawing.Bitmap(size.Width, size.Height);
        System.Drawing.Graphics g = System.Drawing.Graphics.FromImage((System.Drawing.Image)b);
        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

        g.DrawImage(imgToResize, 0, 0, size.Width, size.Height);
        g.Dispose();

        b.SetResolution(72, 72);
        return (System.Drawing.Image)b;
    }//fine ResizeImageForce


    private static System.Drawing.Image DoCanvas(System.Drawing.Image imgToResize, System.Drawing.Size size, string myPZD_BkgColor)
    {
        using (imgToResize)
        {

            imgToResize = resizeImage(imgToResize, new System.Drawing.Size(size.Width, size.Height));

            int width = size.Width;
            int height = size.Height;
            int x = 0;
            int y = 0;

            bool doCanvas = true;

            if (imgToResize.Width == imgToResize.Height)
                doCanvas = false;


            x = (int)(width - imgToResize.Width) / 2;
            y = (int)(height - imgToResize.Height) / 2;

            if (doCanvas)
            {

                //System.Drawing.Image Frame;
                //Frame = System.Drawing.Image.FromFile(Server.MapPath("~/ZeusInc/ImageRaider/Canvas_White.jpg"));


                System.Drawing.Bitmap Frame = new System.Drawing.Bitmap(width, height);




                for (int i = 0; i < width; ++i)
                    for (int j = 0; j < height; ++j)
                        Frame.SetPixel(i, j, System.Drawing.ColorTranslator.FromHtml(myPZD_BkgColor));

                using (Frame)
                {
                    using (var bitmap = new System.Drawing.Bitmap(width, height))
                    {
                        using (var canvas = System.Drawing.Graphics.FromImage(bitmap))
                        {
                            canvas.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                            canvas.DrawImage((System.Drawing.Image)Frame, new System.Drawing.Rectangle(0, 0, width, height), new System.Drawing.Rectangle(0, 0, width, height), System.Drawing.GraphicsUnit.Pixel);
                            canvas.DrawImage(imgToResize, x, y, imgToResize.Width, imgToResize.Height);
                            canvas.Save();
                        }//fine using
                        imgToResize = ResizeImageForce((System.Drawing.Image)bitmap, new System.Drawing.Size(size.Width, size.Height));

                    }//fine using
                }//fine using

            }//fine if
            else
                imgToResize = resizeImage(imgToResize, new System.Drawing.Size(size.Width, size.Height));

        }//fine using


        return imgToResize;
        
    }//fine DoCanvas


    private bool ClearTempPath(string TempPath, string PAGE_ID)
    {
        try
        {
            if (TempPath.Length == 0)
                return false;

            string[] Files = System.IO.Directory.GetFiles(System.Web.HttpContext.Current.Server.MapPath(TempPath));

            TimeSpan ts = new TimeSpan();

            for (int i = 0; i < Files.Length; ++i)
            {
                System.IO.FileInfo myFileInfo = new System.IO.FileInfo(Files[i]);

                ts = new TimeSpan(DateTime.Now.Ticks - myFileInfo.CreationTime.Ticks);

                //Response.Write("DEB PAGE_ID.Value: " + PAGE_ID.Value + " " + (long)ts.TotalMinutes + "<br />");

                if (((long)ts.TotalMinutes > 1)
                    && (myFileInfo.Name.IndexOf(PAGE_ID) > -1))
                {

                    myFileInfo.IsReadOnly = false;
                    System.IO.File.Delete(Files[i]);
                }//fine if

            }//fine for

            return true;
        }
        catch (Exception p)
        {
            System.Web.HttpContext.Current.Response.Write(p.ToString());
            return false;
        }
    }//fine ClearTempPath
    
    

    public void ProcessRequest(HttpContext context)
    {
        //context.Response.ContentType = "text/plain";
        //context.Response.Write("Hello World");

        //http://blog.stevensanderson.com/2008/11/24/jquery-ajax-uploader-plugin-with-progress-bar/

        try
        {
           
            //context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            

            //string filePath = "~/ZeusInc/TempUploadRaider/";

            //write your handler implementation here.
            if (context.Request.Files.Count <= 0)
            {
                context.Response.Write("No file uploaded");
            }
            else
            {
                string ReturnMessage = "OK";
                
                
                for (int i = 0; i < context.Request.Files.Count; ++i)
                {
                    HttpPostedFile file = context.Request.Files[i];

                    //string pFileName = CheckFileName(context.Server.MapPath(filePath), file.FileName);

                    if ((context.Request.QueryString["PAGE_ID"] == null)
                        || (context.Request.QueryString["PAGE_ID"].Length == 0))
                        return;

                    int minWidth = 0;
                    int minHeight = 0;
                    int maxWidth = 0;
                    int maxHeight = 0;

                    try
                    {
                        minWidth = Convert.ToInt32(context.Request.QueryString["MIN_WIDTH"]);
                    }
                    catch (Exception)
                    {

                    }
                    try
                    {
                        minHeight = Convert.ToInt32(context.Request.QueryString["MIN_HEIGHT"]);
                    }
                    catch (Exception)
                    {

                    }
                    try
                    {
                        maxWidth = Convert.ToInt32(context.Request.QueryString["MAX_WIDTH"]);
                    }
                    catch (Exception)
                    {

                    }

                    try
                    {
                        maxHeight = Convert.ToInt32(context.Request.QueryString["MAX_HEIGHT"]);
                    }
                    catch (Exception)
                    {

                    }
                    
                   
                    
                    /*-------------------------------------------------------------------------------------------*/
                    /*---------------------------------------   BETA   ------------------------------------------*/
                    /*-------------------------------------------------------------------------------------------*/
                   

                    int ImgBeta_Mode = 0;
                    int ImgBeta_Width = 0;
                    int ImgBeta_Height = 0;
                    int ImgBeta_Weight = 10;
                    try
                    {
                        ImgBeta_Weight = Convert.ToInt32(context.Request.QueryString["BETA_WEIGHT"]);
                    }
                    catch (Exception)
                    {

                    }

                    try
                    {
                        ImgBeta_Mode = Convert.ToInt32(context.Request.QueryString["BETA_MODE"]);
                    }
                    catch (Exception)
                    {

                    }
                    
                    try
                    {
                        ImgBeta_Width = Convert.ToInt32(context.Request.QueryString["BETA_WIDTH"]);
                    }
                    catch (Exception)
                    {

                    }
                    try
                    {
                        ImgBeta_Height = Convert.ToInt32(context.Request.QueryString["BETA_HEIGHT"]);
                    }
                    catch (Exception)
                    {

                    }
                    

                    System.Drawing.Image myBeta_Image = System.Drawing.Image.FromStream(file.InputStream);
                    

                    if ((ImgBeta_Width > 0)
                        && (ImgBeta_Height > 0)
                        && (ImgBeta_Mode > 0))
                    {



                        if ((minWidth > 0)
                            && (myBeta_Image.PhysicalDimension.Width < minWidth))
                        {
                            context.Response.Write("ERR1");
                            myBeta_Image.Dispose();
                            return;
                        }

                        if ((minHeight > 0)
                            && (myBeta_Image.PhysicalDimension.Height < minHeight))
                        {
                            context.Response.Write("ERR1");
                            myBeta_Image.Dispose();
                            return;
                        }

                        if ((maxWidth > 0)
                            && (myBeta_Image.PhysicalDimension.Width > maxWidth))
                        {
                            context.Response.Write("ERR1");
                            myBeta_Image.Dispose();
                            return;
                        }

                        if ((maxHeight > 0)
                            && (myBeta_Image.PhysicalDimension.Height > maxHeight))
                        {
                            context.Response.Write("ERR1");
                            myBeta_Image.Dispose();
                            return;
                        }

                        //context.Response.Write("DEB ImgGamma_Mode:" + ImgBeta_Mode);
                        
                        switch (ImgBeta_Mode)
                        {
                            case 1:
                                myBeta_Image = ResizeImageForce(myBeta_Image, new System.Drawing.Size(ImgBeta_Width, ImgBeta_Height));
                                break;

                            case 2:
                                myBeta_Image = resizeImage(myBeta_Image, new System.Drawing.Size(ImgBeta_Width, ImgBeta_Height));
                                break;

                            case 3:
                                myBeta_Image = resizeImageR(myBeta_Image, new System.Drawing.Size(ImgBeta_Width, ImgBeta_Height));
                                break;

                            case 4:
                                myBeta_Image = DoCanvas(myBeta_Image, new System.Drawing.Size(ImgBeta_Width, ImgBeta_Height), "#FFFFFF");
                                break;

                        }//fine switch


                        string BETA_FileName = context.Request.QueryString["PAGE_ID"] + "BETA_" + file.FileName.Replace(" ",string.Empty);
                        context.Session["FILENAME_BETA" + context.Request.QueryString["PAGE_ID"].ToString()] = BETA_FileName;
                            
                        string SAVE_BETA_PATH = context.Server.MapPath("~/ZeusInc/TempBeta/" + BETA_FileName);

                        //try
                        //{

                        //    System.IO.File.Delete(SAVE_BETA_PATH);
                        //}
                        //catch (Exception) { }
                        
                        //MODIFICA Per dimensioni file in caso di rad 1 o 2

                        if (ImgBeta_Mode != 3)
                        {
                            System.Drawing.Imaging.ImageCodecInfo jgpEncoder;
                            long quality = 100;
                            ClearTempPath("~/ZeusInc/TempBeta/", context.Request.QueryString["PAGE_ID"]);

                            if(System.IO.Path.GetExtension(file.FileName) == ".png")
                            {
                                jgpEncoder = GetEncoder(System.Drawing.Imaging.ImageFormat.Png);
                            }
                            else
                            { 
                                jgpEncoder = GetEncoder(System.Drawing.Imaging.ImageFormat.Jpeg);
                            }
                            
                            // Create an Encoder object based on the GUID
                            // for the Quality parameter category.
                            System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;

                            // Create an EncoderParameters object.
                            // An EncoderParameters object has an array of EncoderParameter
                            // objects. In this case, there is only one
                            // EncoderParameter object in the array.
                            System.Drawing.Imaging.EncoderParameters myEncoderParameters = new System.Drawing.Imaging.EncoderParameters(1);

                            System.Drawing.Imaging.EncoderParameter myEncoderParameter = new System.Drawing.Imaging.EncoderParameter(myEncoder, 90L);
                            myEncoderParameters.Param[0] = myEncoderParameter;
                            System.IO.MemoryStream msApp = new System.IO.MemoryStream();
                            myBeta_Image.Save(msApp, jgpEncoder, myEncoderParameters);
                            while ((msApp.Length > (Convert.ToInt32(ImgBeta_Weight) * (1000))) && (quality > 1))
                            {
                                myEncoder = System.Drawing.Imaging.Encoder.Quality;
                                myEncoderParameters = new System.Drawing.Imaging.EncoderParameters(1);
                                myEncoderParameter = new System.Drawing.Imaging.EncoderParameter(myEncoder, quality--);
                                myEncoderParameters.Param[0] = myEncoderParameter;
                                msApp.Close();
                                msApp = new System.IO.MemoryStream();
                                myBeta_Image.Save(msApp, jgpEncoder, myEncoderParameters);
                            }
                            
                            myBeta_Image.Save(SAVE_BETA_PATH, jgpEncoder, myEncoderParameters);
                            //myBeta_Image.Save(SAVE_BETA_PATH);
                        }
                        else
                        {
                            myBeta_Image.Save(SAVE_BETA_PATH);
                        }
                    }//fine if

                    myBeta_Image.Dispose();




                    /*-------------------------------------------------------------------------------------------*/
                    /*---------------------------------------   GAMMA   ------------------------------------------*/
                    /*-------------------------------------------------------------------------------------------*/


                    int ImgGamma_Mode = 0;
                    int ImgGamma_Width = 0;
                    int ImgGamma_Height = 0;
                    int ImgGamma_Weight = 10;


                    try
                    {
                        ImgGamma_Mode = Convert.ToInt32(context.Request.QueryString["GAMMA_MODE"]);
                    }
                    catch (Exception)
                    {

                    }
                    try
                    {
                        ImgGamma_Weight = Convert.ToInt32(context.Request.QueryString["GAMMA_WEIGHT"]);
                    }
                    catch (Exception)
                    {

                    }
                    
                    
                    try
                    {
                        ImgGamma_Width = Convert.ToInt32(context.Request.QueryString["GAMMA_WIDTH"]);
                    }
                    catch (Exception)
                    {

                    }
                    try
                    {
                        ImgGamma_Height = Convert.ToInt32(context.Request.QueryString["GAMMA_HEIGHT"]);
                    }
                    catch (Exception)
                    {

                    }


                    System.Drawing.Image myGamma_Image = System.Drawing.Image.FromStream(file.InputStream);



                   
                   

                    if ((ImgGamma_Width > 0)
                        && (ImgGamma_Height > 0)
                        
                        && (ImgGamma_Mode > 0))
                    {

                        

                        if ((minWidth > 0)
                            && (myGamma_Image.PhysicalDimension.Width < minWidth))
                        {
                            context.Response.Write("ERR1");
                            myGamma_Image.Dispose();
                            return;
                        }

                        if ((minHeight > 0)
                            && (myGamma_Image.PhysicalDimension.Height < minHeight))
                        {
                            context.Response.Write("ERR1");
                            myGamma_Image.Dispose();
                            return;
                        }

                        if ((maxWidth > 0)
                            && (myGamma_Image.PhysicalDimension.Width > maxWidth))
                        {
                            context.Response.Write("ERR1");
                            myGamma_Image.Dispose();
                            return;
                        }

                        if ((maxHeight > 0)
                            && (myGamma_Image.PhysicalDimension.Height > maxHeight))
                        {
                            context.Response.Write("ERR1");
                            myGamma_Image.Dispose();
                            return;
                        }


                        switch (ImgGamma_Mode)
                        {
                            case 1:
                                myGamma_Image = ResizeImageForce(myGamma_Image, new System.Drawing.Size(ImgGamma_Width, ImgGamma_Height));
                                break;

                            case 2:
                                myGamma_Image = resizeImage(myGamma_Image, new System.Drawing.Size(ImgGamma_Width, ImgGamma_Height));
                                break;

                            case 3:
                                myGamma_Image = resizeImageR(myGamma_Image, new System.Drawing.Size(ImgGamma_Width, ImgGamma_Height));
                                break;

                            case 4:
                                myGamma_Image = DoCanvas(myGamma_Image, new System.Drawing.Size(ImgGamma_Width, ImgGamma_Height), "#FFFFFF");
                                break;

                        }//fine switch


                        string GAMMA_FileName = context.Request.QueryString["PAGE_ID"] + "GAMMA_" + file.FileName.Replace(" ", string.Empty);
                        context.Session["FILENAME_GAMMA" + context.Request.QueryString["PAGE_ID"].ToString()] = GAMMA_FileName;
                        string SAVE_GAMMA_PATH = context.Server.MapPath("~/ZeusInc/TempGamma/" + GAMMA_FileName);

                        //try
                        //{

                        //    System.IO.File.Delete(SAVE_GAMMA_PATH);
                        //}
                        //catch (Exception) { }

                        if (ImgGamma_Mode != 3)
                        {
                            System.Drawing.Imaging.ImageCodecInfo jgpEncoder;
                            long quality = 100;
                            ClearTempPath("~/ZeusInc/TempGamma/", context.Request.QueryString["PAGE_ID"]);

                            if (System.IO.Path.GetExtension(file.FileName) == ".png")
                            {
                                jgpEncoder = GetEncoder(System.Drawing.Imaging.ImageFormat.Png);
                            }
                            else
                            {
                                jgpEncoder = GetEncoder(System.Drawing.Imaging.ImageFormat.Jpeg);
                            }

                            // Create an Encoder object based on the GUID
                            // for the Quality parameter category.
                            System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;

                            // Create an EncoderParameters object.
                            // An EncoderParameters object has an array of EncoderParameter
                            // objects. In this case, there is only one
                            // EncoderParameter object in the array.
                            System.Drawing.Imaging.EncoderParameters myEncoderParameters = new System.Drawing.Imaging.EncoderParameters(1);

                            System.Drawing.Imaging.EncoderParameter myEncoderParameter = new System.Drawing.Imaging.EncoderParameter(myEncoder, 90L);
                            myEncoderParameters.Param[0] = myEncoderParameter;
                            System.IO.MemoryStream msApp = new System.IO.MemoryStream();
                            myGamma_Image.Save(msApp, jgpEncoder, myEncoderParameters);
                            while ((msApp.Length > (Convert.ToInt32(ImgGamma_Weight) * (1000))) && (quality > 1))
                            {
                                myEncoder = System.Drawing.Imaging.Encoder.Quality;
                                myEncoderParameters = new System.Drawing.Imaging.EncoderParameters(1);
                                myEncoderParameter = new System.Drawing.Imaging.EncoderParameter(myEncoder, quality--);
                                myEncoderParameters.Param[0] = myEncoderParameter;
                                msApp.Close();
                                msApp = new System.IO.MemoryStream();
                                myGamma_Image.Save(msApp, jgpEncoder, myEncoderParameters);
                            }

                            myGamma_Image.Save(SAVE_GAMMA_PATH, jgpEncoder, myEncoderParameters);
                            //myBeta_Image.Save(SAVE_BETA_PATH);
                        }
                        else
                        {
                            myGamma_Image.Save(SAVE_GAMMA_PATH);
                        }


                    }//fine if

                    myGamma_Image.Dispose();



                    if ((ImgBeta_Mode == 3)
                        && (ImgGamma_Mode == 3))
                        ReturnMessage = "OKBETAGAMMA3";
                    else if (ImgBeta_Mode == 3)
                        ReturnMessage = "OKBETA3";
                    else if (ImgGamma_Mode == 3)
                        ReturnMessage = "OKGAMMA3";
                    
                    context.Response.Write(ReturnMessage);

                    break;


                }//fine for
            }//fine else



        }
        catch (Exception p)
        {
            context.Response.Write(p.ToString());
        }
    }//fine ProcessRequest

    private System.Drawing.Imaging.ImageCodecInfo GetEncoder(System.Drawing.Imaging.ImageFormat format)
    {

        System.Drawing.Imaging.ImageCodecInfo[] codecs = System.Drawing.Imaging.ImageCodecInfo.GetImageDecoders();

        foreach (System.Drawing.Imaging.ImageCodecInfo codec in codecs)
        {
            if (codec.FormatID == format.Guid)
            {
                return codec;
            }
        }
        return null;
    }
    
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}//fine classe