﻿<%@ Control Language="C#" ClassName="ImageRaiderCrop" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<%--<link href="jquery.Jcrop.css" rel="stylesheet" type="text/css" />--%>

<script runat="server">

    /*########################################################################################################################*/
    private string myImgBeta_SavePath = "/";
    private string myImgGamma_SavePath = "/";

    private bool isEnabled = true;
    private bool isEditMode = false;
    private bool myBindPzFromDB = false;
    private string myZeusIdModulo = string.Empty;
    private string myZeusIdModuloIndice = string.Empty;
    private string myZeusLang = string.Empty;
    private string myPZD_BkgColor = "#FFFFFF";

    private string defaultBetaImage = string.Empty;
    private string defaultGammaImage = string.Empty;
    private string defaultEditBetaImage = string.Empty;
    private string defaultEditGammaImage = string.Empty;
    private string imageAlt = string.Empty;

    public bool Enabled {
        set { isEnabled = value; }
        get { return isEnabled; }
    }

    public bool IsEditMode {
        set { isEditMode = value; }
        get { return isEditMode; }
    }

    public string ImageAlt {
        set { ImageAltTextBox.Text = value; }
        get { return ImageAltTextBox.Text; }
    }

    public bool BindPzFromDB {
        set { myBindPzFromDB = value; }
        get { return myBindPzFromDB; }

    }



    public string ZeusIdModulo {
        set { myZeusIdModulo = value; }
        get { return myZeusIdModulo; }

    }

    public string ZeusIdModuloIndice {
        set { myZeusIdModuloIndice = value; }
        get { return myZeusIdModuloIndice; }

    }

    public string ZeusLangCode {
        set { myZeusLang = value; }
        get { return myZeusLang; }

    }

    public string PZD_BkgColor {
        set { myPZD_BkgColor = value; }
        get { return myPZD_BkgColor; }

    }

    public bool Required {
        set { LoadButtonCustomValidator.Enabled = value; }
    }


    public string DefaultBetaImage {
        set {
            defaultBetaImage = value;

            if (defaultBetaImage == "null")
                isEmptyImage = true;
        }
        get { return defaultBetaImage; }

    }

    public string DefaultGammaImage {
        set {
            defaultGammaImage = value;
            if (defaultGammaImage == "null")
                isEmptyImage = true;

        }
        get { return defaultGammaImage; }

    }

    public string DefaultEditBetaImage {
        set {
            defaultEditBetaImage = value;
            if (defaultEditBetaImage == "null")
                isEmptyImage = true;
        }
        get { return defaultEditBetaImage; }

    }

    public string DefaultEditGammaImage {
        set {
            defaultEditGammaImage = value;


        }
        get { return defaultEditGammaImage; }

    }

    public bool isValid() {
        return Page.IsValid;

    }

    public string Vgroup {
        set { LoadButtonCustomValidator.ValidationGroup = value; }
        get { return LoadButtonCustomValidator.ValidationGroup; }
    }

    /*--------------------------------------------------------------------------------------------------------------------*/

    public string ImgAlpha_PermitExtensions {
        set { FILE_TYPES.Value = value; }
        get { return FILE_TYPES.Value; }
    }


    public string ImgAlpha_WidthMin {
        set { MIN_WIDTH.Value = value; }
        get { return MIN_WIDTH.Value; }
    }

    public string ImgAlpha_HeightMin {
        set { MIN_HEIGHT.Value = value; }
        get { return MIN_HEIGHT.Value; }
    }

    public string ImgAlpha_WidthMax {
        set { MAX_WIDTH.Value = value; }
        get { return MAX_WIDTH.Value; }
    }

    public string ImgAlpha_HeightMax {
        set { MAX_HEIGHT.Value = value; }
        get { return MAX_HEIGHT.Value; }
    }

    public string ImgAlpha_MaxSizeKb {
        set { FILE_SIZE_LIMIT.Value = value; }
        get { return FILE_SIZE_LIMIT.Value; }
    }


    /*--------------------------------------------------------------------------------------------------------------------*/

    public string ImgBeta_Path {
        set { myImgBeta_SavePath = value; }
        get { return myImgBeta_SavePath; }
    }

    public string ImgBeta_Width {
        set {
            BETA_WIDTH.Value = value;
            BETA_X.Value = "0";
            BETA_Y.Value = value;
        }
        get { return BETA_WIDTH.Value; }
    }


    public string ImgBeta_Height {
        set {
            BETA_HEIGHT.Value = value;
            BETA_W.Value = "0";
            BETA_H.Value = value;
        }
        get { return BETA_HEIGHT.Value; }
    }

    public string ImgBeta_Weight {
        set {
            BETA_WEIGHT.Value = value;
        }
        get { return BETA_WEIGHT.Value; }
    }

    public string ImgBeta_RaidMode {
        set { BETA_MODE.Value = value; }
        get { return BETA_MODE.Value; }
    }

    /*--------------------------------------------------------------------------------------------------------------------*/

    public string ImgGamma_Path {
        set { myImgGamma_SavePath = value; }
        get { return myImgGamma_SavePath; }
    }

    public string ImgGamma_Width {
        set { GAMMA_WIDTH.Value = value; }
        get { return GAMMA_WIDTH.Value; }
    }


    public string ImgGamma_Height {
        set { GAMMA_HEIGHT.Value = value; }
        get { return GAMMA_HEIGHT.Value; }
    }

    public string ImgGamma_Weight {
        set {
            GAMMA_WEIGHT.Value = value;
        }
        get { return GAMMA_WEIGHT.Value; }
    }

    public string ImgGamma_RaidMode {
        set { GAMMA_MODE.Value = value; }
        get { return GAMMA_MODE.Value; }
    }

    /*########################################################################################################################*/


    public string INFOClientID {
        get { return INFO.ClientID; }
    }

    //public string MaxSizeKb
    //{
    //    set { FILE_SIZE_LIMIT.Value = value; }
    //    get { return FILE_SIZE_LIMIT.Value; }
    //}



    public string ImgBeta_FileName {
        get {


            if (Session["FILENAME_BETA" + this.ID] != null)
                return Session["FILENAME_BETA" + this.ID].ToString();


            return string.Empty;

        }
        set {
            //            if (Session["FILENAME_BETA" + this.ID] != null)
            Session["FILENAME_BETA" + this.ID] = value;
        }
    }

    public string ImgGamma_FileName {
        get {

            if (Session["FILENAME_GAMMA" + this.ID] != null)
                return Session["FILENAME_GAMMA" + this.ID].ToString();


            return string.Empty;

        }

        set {
            //            if (Session["FILENAME_GAMMA" + this.ID] != null)
            Session["FILENAME_GAMMA" + this.ID] = value;
        }
    }


    public bool GotImgBetaFile() {
        if (Session["FILENAME_BETA" + this.ID] != null)
            return true;


        return false;
    }




    private bool isDefaultImage {
        get {

            return isDefault.Value.ToUpper() == "TRUE";
            
            if (Session["isDefaultImage" + this.ID] != null)
                return bool.Parse(Session["isDefaultImage" + this.ID].ToString());
            return false;
        }
        set {
            isDefault.Value = value.ToString();
            Session["isDefaultImage" + this.ID] = value;
        }
    }

    private bool isEmptyImage {
        get {
            return isEmpty.Value.ToUpper() == "TRUE";
            
            if (Session["isEmptyImage" + this.ID] != null)
                return bool.Parse(Session["isEmptyImage" + this.ID].ToString());
            return false;


            //string eventTarget = (this.Request["__EVENTTARGET"] == null) ? string.Empty : this.Request["__EVENTTARGET"];
            //if (eventTarget == "isEmptyImage") {
            //    Session["isEmptyImage" + PAGE_ID.Value] = string.Empty;                
            //}            


        }
        set {
            Session["isEmptyImage" + this.ID] = value;
            //            FinalBETA_CropImage.Visible = !value;
            isEmpty.Value = value.ToString();
            if (value) {
                FinalBETA_CropImage.CssClass += " imgNascosta";
            }
            else {
                FinalBETA_CropImage.CssClass = FinalBETA_CropImage.CssClass.Replace(" imgNascosta", "");
            }

            //            FinalGAMMA_CropImage.Visible = !value;
        }
    }





    public bool GotImgGammaFile() {
        if (Session["FILENAME_GAMMA" + this.ID] != null)
            return true;


        return false;
    }

    public bool SaveImgBeta_File() {

        if (isDefaultImage) {
            ImgBeta_FileName = DefaultEditBetaImage;
            ImgGamma_FileName = defaultEditGammaImage;
            return true;
        }

        if (isEmptyImage) {
            ImgBeta_FileName = "null";
            ImgGamma_FileName = "null";
            return true;
        }


        try {
            string SourcePath = Server.MapPath("~/ZeusInc/TempBeta/" + ImgBeta_FileName);
            string Final_Name = CheckFileName(Server.MapPath(myImgBeta_SavePath), ImgBeta_FileName.Replace(this.ID + "BETA_", string.Empty));
            string DestinationPath = Server.MapPath(myImgBeta_SavePath + Final_Name);
            File.Copy(SourcePath, DestinationPath, false);

            ImgBeta_FileName = Final_Name;

            try { File.Delete(SourcePath); }
            catch (Exception) { }


            return true;

        }
        catch (Exception p) {
            Response.Write(p.ToString());
            return false;
        }
    }//fine SaveImgBeta_File

    public bool SaveImgGamma_File() {

        if (isDefaultImage) {
            ImgBeta_FileName = DefaultEditBetaImage;
            ImgGamma_FileName = defaultEditGammaImage;
            return true;
        }

        if (isEmptyImage) {
            ImgBeta_FileName = "null";
            ImgGamma_FileName = "null";
            return true;
        }

        try {
            string SourcePath = Server.MapPath("~/ZeusInc/TempGamma/" + ImgGamma_FileName);
            string Final_Name = CheckFileName(Server.MapPath(myImgGamma_SavePath), ImgGamma_FileName.Replace(this.ID + "GAMMA_", string.Empty));
            string DestinationPath = Server.MapPath(myImgGamma_SavePath + Final_Name);
            File.Copy(SourcePath, DestinationPath, false);

            ImgGamma_FileName = Final_Name;

            try { File.Delete(SourcePath); }
            catch (Exception) { }


            return true;

        }
        catch (Exception p) {
            Response.Write(p.ToString());
            return false;
        }
    }//fine SaveImgBeta_File


    private string myHtmlEncode(string HTML) {
        HTML = HTML.Replace("è", "e");
        HTML = HTML.Replace("é", "e");
        HTML = HTML.Replace("ê", "e");
        HTML = HTML.Replace("ë", "e");
        HTML = HTML.Replace("ò", "o");
        HTML = HTML.Replace("ô", "o");
        HTML = HTML.Replace("ù", "u");
        HTML = HTML.Replace("û", "u");
        HTML = HTML.Replace("ü", "u");
        HTML = HTML.Replace("à", "a");
        HTML = HTML.Replace("â", "a");
        HTML = HTML.Replace("ì", "i");
        HTML = HTML.Replace("î", "i");
        HTML = HTML.Replace("ï", "i");
        HTML = HTML.Replace("ç", "c");
        HTML = HTML.Replace("_", string.Empty);
        HTML = HTML.Replace(" ", string.Empty);
        HTML = System.Text.RegularExpressions.Regex.Replace(HTML, @"[^\w-]+", string.Empty);
        return HTML;
    }


    private string CheckFileName(string filePath, string fileName) {
        int counter = 1;
        string finalPath = filePath;
        string strFile_name = System.IO.Path.GetFileNameWithoutExtension(fileName);
        strFile_name = myHtmlEncode(strFile_name);

        if (strFile_name.Length > 25)
            strFile_name = strFile_name.Substring(0, 25);

        string strFile_extension = System.IO.Path.GetExtension(fileName);

        if (System.IO.File.Exists(filePath + fileName)) {


            filePath += fileName;
            while (System.IO.File.Exists(filePath)) {
                counter++;
                filePath = System.IO.Path.Combine(finalPath, strFile_name + counter.ToString() + strFile_extension);

            }//fine while

            fileName = strFile_name + counter.ToString() + strFile_extension;
        }


        return fileName;
    }//fine CheckFileName


    protected void Page_Init(object sender, EventArgs e) {

        PAGE_ID.Value = this.ID;




        if (BindPzFromDB)
            BindTheDataSQL();


        string myJavaSCritpt = JQueryBETACrop() + " " + JQueryGAMMACrop();

        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartUpScript" + ZeusIdModuloIndice, myJavaSCritpt, true);


        SetInfoMessageBeta();
        SetInfoMessageGamma();
        SetInfoMessageAuto();


        if (ImgBeta_RaidMode.Equals("0"))
            FinalBETA_CropImage.Visible = false;

        if (ImgGamma_RaidMode.Equals("0"))
            FinalGAMMA_CropImage.Visible = false;




        SESSION_ID.Value = Session.SessionID;



    }//fine Page_Init


    protected void Page_Load(object sender, EventArgs e) {
        Response.Cache.SetExpires(DateTime.UtcNow.AddYears(-4));
        Response.Cache.SetValidUntilExpires(false);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
        Response.Cache.SetNoStore();
        Response.ExpiresAbsolute = DateTime.Now.Subtract(new TimeSpan(1, 0, 0, 0));
        Response.Expires = 0;
        Response.CacheControl = "no-cache";
        Response.AppendHeader("Pragma", "no-cache");
        Response.Cache.AppendCacheExtension("must-revalidate, proxy-revalidate, post-check=0, pre-check=0");

        try {
            ClearFolder(new DirectoryInfo(Environment.GetFolderPath
              (Environment.SpecialFolder.InternetCache)));
        }
        catch (Exception) { }

        ClearTempPath("~/ZeusInc/TempBeta/");
        ClearTempPath("~/ZeusInc/TempGamma/");



        MY_INDEX.Value = ZeusIdModuloIndice;


        if (this.IsPostBack) {
            string eventTarget = (this.Request["__EVENTTARGET"] == null) ? string.Empty : this.Request["__EVENTTARGET"];
            string eventArgument = (this.Request["__EVENTARGUMENT"] == null) ? string.Empty : this.Request["__EVENTARGUMENT"];

            if (eventTarget == "SetSessionVariable") {
                Session["FILENAME_BETA" + PAGE_ID.Value] = string.Empty;
                Session["FILENAME_GAMMA" + PAGE_ID.Value] = string.Empty;
            }
        }
        else {
            isDefaultImage = false;
            isEmptyImage = DefaultEditBetaImage == "null";
        }
    }//fine Page_Load




    public void SetDefaultEditBetaImage() {
        if (isEditMode)
            SetDefaultBetaImage();

    }//fine SetDefaultBetaImage

    public void SetDefaultBetaImage() {
        if (DefaultEditBetaImage.Length > 0) {
            FinalBETA_CropImage.ImageUrl = ImgBeta_Path + DefaultEditBetaImage;
            BETA_FILENAME.Text = "OK";
        }
    }//fine SetDefaultBetaImage


    public void SetDefaultEditGammaImage() {
        if (isEditMode)
            SetDefaultGammaImage();

    }//fine SetDefaultGammaImage


    public void SetDefaultGammaImage() {
        if (DefaultEditGammaImage.Length > 0) {
            FinalGAMMA_CropImage.ImageUrl = ImgGamma_Path + DefaultEditGammaImage;
            GAMMA_FILENAME.Text = "OK";
        }
    }//fine SetDefaultGammaImage


    protected void btnDefault_ServerClick(object sender, EventArgs e) {
        isDefaultImage = true;
        isEmptyImage = false;

        Session["FILENAME_BETA" + PAGE_ID.Value] = FinalBETA_CropImage.ImageUrl;
        Session["FILENAME_GAMMA" + PAGE_ID.Value] = FinalGAMMA_CropImage.ImageUrl;

        //        ImgBeta_FileName = FinalBETA_CropImage.ImageUrl;
        //        ImgGamma_FileName = FinalGAMMA_CropImage.ImageUrl;

        SetDefaultBetaImage();
        SetDefaultGammaImage();
    }


    public void SetEmptyEditBetaImage() {
        if (isEditMode)
            SetEmptyBetaImage();
    }//fine SetDefaultBetaImage

    public void SetEmptyBetaImage() {
        if (DefaultEditBetaImage.Length > 0) {
            FinalBETA_CropImage.ImageUrl = "null";
            BETA_FILENAME.Text = "OK";
        }
    }//fine SetDefaultBetaImage


    public void SetEmptyEditGammaImage() {
        if (isEditMode)
            SetEmptyGammaImage();
    }//fine SetDefaultGammaImage    

    public void SetEmptyGammaImage() {
        if (DefaultEditGammaImage.Length > 0) {
            FinalGAMMA_CropImage.ImageUrl = "null";
            GAMMA_FILENAME.Text = "OK";
        }
    }//fine SetDefaultGammaImage    

    protected void btnEmpty_ServerClick(object sender, EventArgs e) {
        isEmptyImage = true;
        isDefaultImage = false;


        Session["FILENAME_BETA" + PAGE_ID.Value] = FinalBETA_CropImage.ImageUrl;
        Session["FILENAME_GAMMA" + PAGE_ID.Value] = FinalGAMMA_CropImage.ImageUrl;

        //        ImgBeta_FileName = FinalBETA_CropImage.ImageUrl;
        //        ImgGamma_FileName = FinalGAMMA_CropImage.ImageUrl;

        SetEmptyBetaImage();
        SetEmptyGammaImage();
    }


    private bool BindTheDataSQL() {

        try {

            try {
                if (ZeusIdModulo.Length == 0)
                    ZeusIdModulo = Request.QueryString["ZIM"];
            }
            catch (Exception) { }


            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            string sqlQuery = "SELECT PZL_BoxDescription, PZL_ComponentDescription,";
            sqlQuery += "ImgPreview_BetaDefault, ImgPreview_GammaDefault, ImgAlpha_PermitExtensions,";
            sqlQuery += " ImgAlpha_WidthMin, ImgAlpha_WidthMax, ImgAlpha_HeightMin, ";
            sqlQuery += " ImgAlpha_HeightMax, ImgAlpha_WeightMax, ImgBeta_RaidMode, ImgBeta_Path,";
            sqlQuery += " ImgBeta_Width, ImgBeta_Height, ImgBeta_WeightMaxCompress, ";
            sqlQuery += " ImgGamma_RaidMode, ImgGamma_Path, ImgGamma_Width, ImgGamma_Height,";
            sqlQuery += " ImgGamma_WeightMaxCompress,PZD_BkgColor";
            sqlQuery += " FROM tbPZ_ImageRaider";
            sqlQuery += " WHERE ZeusIdModuloIndice =@ZeusIdModuloIndice  AND ZeusLangCode=@ZeusLangCode";




            using (SqlConnection conn = new SqlConnection(strConnessione)) {
                SqlCommand command = new SqlCommand(sqlQuery, conn);

                command.Parameters.Add("@ZeusIdModuloIndice", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ZeusIdModuloIndice"].Value = ZeusIdModulo + ZeusIdModuloIndice;
                command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ZeusLangCode"].Value = ZeusLangCode;

                conn.Open();
                SqlDataReader reader = command.ExecuteReader();
                /*fino a quando ci sono record*/

                if (!reader.HasRows) {
                    reader.Close();
                    return false;
                }


                while (reader.Read()) {

                    if (reader["PZD_BkgColor"] != DBNull.Value)
                        myPZD_BkgColor = reader["PZD_BkgColor"].ToString();

                    if (reader["PZL_BoxDescription"] != DBNull.Value)
                        BoxDescriptionLabel.Text = reader["PZL_BoxDescription"].ToString();

                    if (reader["PZL_ComponentDescription"] != DBNull.Value)
                        ComponentDescriptionLabel.Text = reader["PZL_ComponentDescription"].ToString();

                    if (reader["ImgPreview_BetaDefault"] != DBNull.Value) {
                        FinalBETA_CropImage.ImageUrl = reader["ImgPreview_BetaDefault"].ToString();
                        try {

                            DefaultBetaImage = System.IO.Path.GetFileName(reader["ImgPreview_BetaDefault"].ToString());
                            defaultEditBetaImage = DefaultBetaImage;
                        }
                        catch (Exception) { }
                    }

                    if (reader["ImgPreview_GammaDefault"] != DBNull.Value) {
                        FinalGAMMA_CropImage.ImageUrl = reader["ImgPreview_GammaDefault"].ToString();
                        try {
                            DefaultGammaImage = System.IO.Path.GetFileName(reader["ImgPreview_GammaDefault"].ToString());
                            DefaultEditGammaImage = DefaultGammaImage;
                        }
                        catch (Exception) { }
                    }

                    if (reader["ImgAlpha_PermitExtensions"] != DBNull.Value) {

                        string PermitExtensions = reader["ImgAlpha_PermitExtensions"].ToString();
                        char[] mySplit = { ',' };
                        string[] AllExtensions = PermitExtensions.Split(mySplit);

                        PermitExtensions = string.Empty;

                        for (int i = 0; i < AllExtensions.Length; ++i)
                            PermitExtensions += "*." + AllExtensions[i] + ";";


                        ImgAlpha_PermitExtensions = PermitExtensions;
                    }

                    if (reader["ImgAlpha_WidthMin"] != DBNull.Value)
                        ImgAlpha_WidthMin = Convert.ToInt32(reader["ImgAlpha_WidthMin"]).ToString();

                    if (reader["ImgAlpha_WidthMax"] != DBNull.Value)
                        ImgAlpha_WidthMax = Convert.ToInt32(reader["ImgAlpha_WidthMax"]).ToString();

                    if (reader["ImgAlpha_HeightMin"] != DBNull.Value)
                        ImgAlpha_HeightMin = Convert.ToInt32(reader["ImgAlpha_HeightMin"]).ToString();

                    if (reader["ImgAlpha_HeightMax"] != DBNull.Value)
                        ImgAlpha_HeightMax = Convert.ToInt32(reader["ImgAlpha_HeightMax"]).ToString();

                    if (reader["ImgAlpha_WeightMax"] != DBNull.Value)
                        ImgAlpha_MaxSizeKb = Convert.ToInt32(reader["ImgAlpha_WeightMax"]).ToString();



                    if (reader["ImgBeta_RaidMode"] != DBNull.Value)
                        ImgBeta_RaidMode = Convert.ToInt32(reader["ImgBeta_RaidMode"]).ToString();

                    if (reader["ImgBeta_Path"] != DBNull.Value)
                        ImgBeta_Path = reader["ImgBeta_Path"].ToString();

                    if (reader["ImgBeta_Width"] != DBNull.Value)
                        ImgBeta_Width = Convert.ToInt32(reader["ImgBeta_Width"]).ToString();

                    if (reader["ImgBeta_Height"] != DBNull.Value)
                        ImgBeta_Height = Convert.ToInt32(reader["ImgBeta_Height"]).ToString();

                    if (reader["ImgBeta_WeightMaxCompress"] != DBNull.Value)
                        ImgBeta_Weight = Convert.ToInt32(reader["ImgBeta_WeightMaxCompress"]).ToString();



                    if (reader["ImgGamma_RaidMode"] != DBNull.Value)
                        ImgGamma_RaidMode = Convert.ToInt32(reader["ImgGamma_RaidMode"]).ToString();

                    if (reader["ImgGamma_Path"] != DBNull.Value)
                        ImgGamma_Path = reader["ImgGamma_Path"].ToString();

                    if (reader["ImgGamma_Width"] != DBNull.Value)
                        ImgGamma_Width = Convert.ToInt32(reader["ImgGamma_Width"]).ToString();

                    if (reader["ImgGamma_Height"] != DBNull.Value)
                        ImgGamma_Height = Convert.ToInt32(reader["ImgGamma_Height"]).ToString();

                    if (reader["ImgGamma_WeightMaxCompress"] != DBNull.Value)
                        ImgGamma_Weight = Convert.ToInt32(reader["ImgGamma_WeightMaxCompress"]).ToString();


                }//fine while
                reader.Close();
                conn.Close();
            }
            return true;
        }
        catch (Exception p) {
            //Response.Write(p.ToString());
            return false;
        }

    }//fine BindTheDataSQL



    public void SetInfoMessageAuto() {
        Message1Label.Text = "Formati grafici consentiti: ";
        if (ImgAlpha_PermitExtensions != null)
            Message1Label.Text += ImgAlpha_PermitExtensions;

        Message1Label.Text.Remove(Message1Label.Text.Length - 1, 1);

        if (ImgAlpha_WidthMax != ImgAlpha_WidthMin) {
            Message2Label.Text = " Larghezza: min " + ImgAlpha_WidthMin.ToString();
            Message2Label.Text += " max " + ImgAlpha_WidthMax.ToString();
        }
        else Message2Label.Text = "Larghezza: " + ImgAlpha_WidthMax.ToString();
        Message2Label.Text += " pixel";

        if (ImgAlpha_HeightMax != ImgAlpha_HeightMin) {
            Message2Label.Text += " <br /> Altezza: min " + ImgAlpha_HeightMin.ToString();
            Message2Label.Text += " max " + ImgAlpha_HeightMax.ToString();
        }
        else Message2Label.Text += " <br /> Altezza: " + ImgAlpha_HeightMax.ToString();
        Message2Label.Text += " pixel";

        Message3Label.Text = "Peso massimo: " + ImgAlpha_MaxSizeKb + " KB";

    }//fine SetInfoMessageAuto



    private void SetInfoMessageBeta() {
        if ((!ImgBeta_RaidMode.Equals("0")) && (!ImgBeta_RaidMode.Equals("1"))) {
            MessageBetaLabel.Text = "Dimensioni finali:";

            if (ImgBeta_RaidMode.Equals("2"))
                MessageBetaLabel.Text += " massimo";

            MessageBetaLabel.Text += " " + ImgBeta_Width + "x" + ImgBeta_Height + " pixel";// +" pixel, peso massimo " + ImgBeta_WeightMaxCompress + " KB";
            MessageBetaLabel.Visible = true;
        }
        else MessageBetaLabel.Visible = false;


    }//fine SetInfoMessageBeta

    private void SetInfoMessageGamma() {
        if ((!ImgGamma_RaidMode.Equals("0")) && (!ImgGamma_RaidMode.Equals("1"))) {
            MessageGammaLabel.Text = "Dimensioni finali:";

            if (ImgGamma_RaidMode.Equals("2"))
                MessageGammaLabel.Text += " massimo";

            MessageGammaLabel.Text += " " + ImgGamma_Width + "x" + ImgGamma_Height;// +" pixel, peso massimo " + ImgGamma_WeightMaxCompress + " KB";
            MessageGammaLabel.Visible = true;
        }
        else MessageGammaLabel.Visible = false;

    }//fine SetInfoMessageBeta




    private void ClearFolder(DirectoryInfo folder) {
        try {
            TimeSpan ts = new TimeSpan();
            foreach (FileInfo file in folder.GetFiles()) {
                ts = new TimeSpan(DateTime.Now.Ticks - file.CreationTime.Ticks);
                if ((long)ts.TotalMinutes < 2)
                    file.Delete();
            }
            foreach (DirectoryInfo subfolder in folder.GetDirectories()) { ClearFolder(subfolder); }
        }
        catch (Exception) { }
    }//fine ClearFolder


    private bool ClearTempPath(string TempPath) {
        try {
            if (TempPath.Length == 0)
                return false;

            string[] Files = Directory.GetFiles(Server.MapPath(TempPath));

            TimeSpan ts = new TimeSpan();

            for (int i = 0; i < Files.Length; ++i) {
                FileInfo myFileInfo = new FileInfo(Files[i]);

                ts = new TimeSpan(DateTime.Now.Ticks - myFileInfo.CreationTime.Ticks);

                //Response.Write("DEB PAGE_ID.Value: " + PAGE_ID.Value + " " + (long)ts.TotalMinutes + "<br />");

                if (((long)ts.TotalMinutes > 1)
                    && (myFileInfo.Name.IndexOf(PAGE_ID.Value.ToString()) > -1)) {

                    myFileInfo.IsReadOnly = false;
                    File.Delete(Files[i]);
                }//fine if

            }//fine for

            return true;
        }
        catch (Exception p) {
            //Response.Write(p.ToString());
            return false;
        }
    }//fine ClearTempPath

    protected void Page_Unload(object sender, EventArgs e) {
        if (!IsPostBack) {

            if (Session["FILENAME_BETA" + PAGE_ID.Value] != null) {
                string SourcePath = Server.MapPath("~/ZeusInc/TempBeta/" + Session["FILENAME_BETA" + PAGE_ID.Value]);
                try { File.Delete(SourcePath); }
                catch (Exception) { }

                Session["FILENAME_BETA" + PAGE_ID.Value] = null;
            }

            if (Session["FILENAME_GAMMA" + PAGE_ID.Value] != null) {
                string SourcePath = Server.MapPath("~/ZeusInc/TempGamma/" + Session["FILENAME_GAMMA" + PAGE_ID.Value]);
                try { File.Delete(SourcePath); }
                catch (Exception) { }

                Session["FILENAME_GAMMA" + PAGE_ID.Value] = null;
            }
        }

    }//fine Page_Unload




    /*#######################################################################################################################*/
    /*#######################################################################################################################*/
    /*#######################################################################################################################*/
    /*#######################################################################################################################*/




    private string JQueryBETACrop() {




        string myJavascript = "function RunMeBETA" + ZeusIdModuloIndice + "() { ";
        myJavascript += " jQuery('#CropBETA" + this.ID + "Image').Jcrop({";
        myJavascript += " onSelect: storeBETA" + this.ID + "Coords,";
        myJavascript += " onChange: storeBETA" + this.ID + "Coords,";
        myJavascript += "  bgColor:     'black',";
        myJavascript += "  bgOpacity:   .4,";
        myJavascript += "  minSize: [ " + BETA_WIDTH.Value + ", " + BETA_HEIGHT.Value + " ],";
        myJavascript += "  maxSize: [ " + BETA_WIDTH.Value + ", " + BETA_HEIGHT.Value + " ],";
        myJavascript += "  setSelect: [ 0,0," + BETA_WIDTH.Value + "," + BETA_HEIGHT.Value + " ],";
        myJavascript += "  trackDocument: false,";
        myJavascript += "  baseClass: 'jcrop',";
        myJavascript += "  addClass: null,";
        myJavascript += "  borderOpacity: .4,";
        myJavascript += "  handleOpacity: .5,";
        myJavascript += "  handlePad: 5,";
        myJavascript += "  handleSize: 9,";
        myJavascript += "  handleOffset: 5,";
        myJavascript += "  edgeMargin: 14,";
        myJavascript += "  aspectRatio: 0,";
        myJavascript += "  keySupport: true,";
        myJavascript += "  cornerHandles: true,";
        myJavascript += "  sideHandles: true,";

        myJavascript += "  boxWidth: 0,";
        myJavascript += "  boxHeight: 0,";
        myJavascript += "  boundary: 8,";
        myJavascript += "  animationDelay: 20,";
        myJavascript += "  swingSpeed: 3,";


        myJavascript += "  drawBorders: true,";
        myJavascript += "  dragEdges: true,";
        myJavascript += "  allowSelect: true,";
        myJavascript += "  allowMove: true,";
        myJavascript += "  allowResize: true";
        //myJavascript += "  aspectRatio: 16 / 9";
        myJavascript += " });";
        myJavascript += " };";
        myJavascript += " ";
        myJavascript += " function storeBETA" + this.ID + "Coords(c) {";
        myJavascript += " jQuery('#" + BETA_X.ClientID + "').val(c.x);";
        myJavascript += " jQuery('#" + BETA_Y.ClientID + "').val(c.y);";
        myJavascript += " jQuery('#" + BETA_W.ClientID + "').val(c.w);";
        myJavascript += " jQuery('#" + BETA_H.ClientID + "').val(c.h);";
        myJavascript += " };";
        myJavascript += " ";

        return myJavascript;

    }//fine JQueryCrop

    private string JQueryGAMMACrop() {

        string myJavascript = "function RunMeGAMMA" + ZeusIdModuloIndice + "() { ";
        myJavascript += " jQuery('#CropGAMMA" + this.ID + "Image').Jcrop({";
        myJavascript += " onSelect: storeGAMMA" + this.ID + "Coords,";
        myJavascript += " onChange: storeGAMMA" + this.ID + "Coords,";
        myJavascript += "  bgColor:     'black',";
        myJavascript += "  bgOpacity:   .4,";
        myJavascript += "  minSize: [ " + GAMMA_WIDTH.Value + ", " + GAMMA_HEIGHT.Value + " ],";
        myJavascript += "  maxSize: [ " + GAMMA_WIDTH.Value + ", " + GAMMA_HEIGHT.Value + " ],";
        myJavascript += "  setSelect: [ 0,0," + GAMMA_WIDTH.Value + "," + GAMMA_HEIGHT.Value + " ],";
        myJavascript += "  trackDocument: false,";
        myJavascript += "  baseClass: 'jcrop',";
        myJavascript += "  addClass: null,";
        myJavascript += "  borderOpacity: .4,";
        myJavascript += "  handleOpacity: .5,";
        myJavascript += "  handlePad: 5,";
        myJavascript += "  handleSize: 9,";
        myJavascript += "  handleOffset: 5,";
        myJavascript += "  edgeMargin: 14,";
        myJavascript += "  aspectRatio: 0,";
        myJavascript += "  keySupport: true,";
        myJavascript += "  cornerHandles: true,";
        myJavascript += "  sideHandles: true,";

        myJavascript += "  boxWidth: 0,";
        myJavascript += "  boxHeight: 0,";
        myJavascript += "  boundary: 8,";
        myJavascript += "  animationDelay: 20,";
        myJavascript += "  swingSpeed: 3,";


        myJavascript += "  drawBorders: true,";
        myJavascript += "  dragEdges: true,";
        myJavascript += "  allowSelect: true,";
        myJavascript += "  allowMove: true,";
        myJavascript += "  allowResize: true";
        //myJavascript += "  aspectRatio: 16 / 9";
        myJavascript += " });";
        myJavascript += " };";
        myJavascript += " ";
        myJavascript += " function storeGAMMA" + this.ID + "Coords(c) {";
        myJavascript += " jQuery('#" + GAMMA_X.ClientID + "').val(c.x);";
        myJavascript += " jQuery('#" + GAMMA_Y.ClientID + "').val(c.y);";
        myJavascript += " jQuery('#" + GAMMA_W.ClientID + "').val(c.w);";
        myJavascript += " jQuery('#" + GAMMA_H.ClientID + "').val(c.h);";
        myJavascript += " };";
        myJavascript += " ";

        return myJavascript;

    }//fine JQueryCrop
</script>

<style>
    DIV#MainContents {
        background-color: White;
        border: 1px solid gray;
    }

    H1 {
        margin-top: 0;
        padding-top: 0;
    }

    DIV.ProgressBar {
        width: 300px;
        padding: 0;
        border: 1px solid black;
        margin-right: 1em;
        height: .75em;
        margin-left: 1em;
        display: -moz-inline-stack;
        display: inline-block;
        zoom: 1;
        *display: inline;
    }

        DIV.ProgressBar DIV {
            background-color: #666666;
            font-size: 1pt;
            height: 100%;
            float: left;
        }
</style>

<script>

    //jQuery(document).ready(RunMe());
    /// jQuery plugin to add support for SwfUpload
    /// (c) 2008 Steven Sanderson

    (function ($) {

        var PAGE_ID = $('#<%= PAGE_ID.ClientID %>').val();

        $.fn.makeAsyncUploader = function (options) {
            return this.each(function () {
                // Put in place a new container with a unique ID
                var id = $(this).attr("id" + PAGE_ID);
                var container = $("<span class='asyncUploader'/>");
                container.append($("<div class='ProgressBar'> <div>&nbsp;</div> </div>"));
                container.append($("<span id='" + id + "_completedMessage'/>"));
                container.append($("<span id='" + id + "_uploading' ><input type='button' value='Annulla' class='ZSSM_Button01_Button' /></span>"));
                container.append($("<span id='" + id + "_info' class='LabelSkin_CorpoTesto'></span>"));
                container.append($("<span id='" + id + "_swf'/>"));
                container.append($("<input type='hidden' name='" + id + "_filename'/>"));
                container.append($("<input type='hidden' name='" + id + "_guid'/>"));
                $(this).before(container).remove();
                $("div.ProgressBar", container).hide();
                $("span[id$=_uploading]", container).hide();
                $("span[id$=_info]", container).hide();


                // Instantiate the uploader SWF
                var swfu
                var width = 109, height = 22;
                if (options) {
                    width = options.width || width;
                    height = options.height || height;
                }
                var defaults = {
                    //flash_url: "swfupload.swf",
                    upload_url: "/Home/AsyncUpload",
                    file_size_limit: "3 MB",
                    file_types: "*.*",
                    file_types_description: "All Files",
                    debug: false,

                    button_image_url: "Bt4_CaricaImmagine.png",
                    button_width: width,
                    button_height: height,
                    button_placeholder_id: id + "_swf",
                    button_text: "<font face='Arial' size='13pt'>Choose file</span>",
                    button_text_left_padding: (width - 70) / 2,
                    button_text_top_padding: 1,


                    // Called when the user chooses a new file from the file browser prompt (begins the upload)
                    file_queued_handler: function (file) { swfu.startUpload(); },

                    // Called when a file doesn't even begin to upload, because of some error
                    file_queue_error_handler: function (file, code, msg) { alert("Attenzione, file non caricato: " + msg); },

                    // Called when an error occurs during upload
                    upload_error_handler: function (file, code, msg) { alert("Attenzione, file non caricato: " + msg); },

                    // Called when upload is beginning (switches controls to uploading state)
                    upload_start_handler: function () {
                        swfu.setButtonDimensions(0, height);
                        $("input[name$=_filename]", container).val("");
                        $("input[name$=_guid]", container).val("");
                        $("div.ProgressBar div", container).css("width", "0px");
                        $("div.ProgressBar", container).show();
                        $("span[id$=_uploading]", container).show();
                        $("span[id$=_info]", container).show();
                        $("span[id$=_completedMessage]", container).html("").hide();

                        if (options.disableDuringUpload)
                            $(options.disableDuringUpload).attr("disabled", "disabled");

                        $('#<%= ALLERT.ClientID %>').html("");

                        $('#<%= INFO.ClientID %>').html("");




                    },

                    // Called when upload completed successfully (puts success details into hidden fields)
                    upload_success_handler: function (file, response) {
                        $("input[name$=_filename]", container).val(file.name);
                        $("input[name$=_guid]", container).val(response);
                        $("span[id$=_completedMessage]", container).html("Caricato <b>{0}</b> ({1} KB)"
                                    .replace("{0}", file.name)
                                    .replace("{1}", Math.round(file.size / 1024))
                                );
                        $("span[id$=_info]", container).hide();

                    },

                    // Called when upload is finished (either success or failure - reverts controls to non-uploading state)
                    upload_complete_handler: function () {
                        var clearup = function () {
                            $("div.ProgressBar", container).hide();
                            $("span[id$=_completedMessage]", container).show();
                            $("span[id$=_uploading]", container).hide();
                            $("span[id$=_info]", container).hide();
                            swfu.setButtonDimensions(width, height);
                        };
                        if ($("input[name$=_filename]", container).val() != "") // Success
                            $("div.ProgressBar div", container).animate({ width: "100%" }, { duration: "fast", queue: false, complete: clearup });
                        else // Fail
                            clearup();

                        if (options.disableDuringUpload)
                            $(options.disableDuringUpload).removeAttr("disabled");


                    },

                    // Called periodically during upload (moves the Mauis bar along)
                    upload_progress_handler: function (file, bytes, total) {
                        var percent = 100 * bytes / total;
                        $("div.ProgressBar div", container).animate({ width: percent + "%" }, { duration: 500, queue: false });
                        $("span[id$=_info]", container).html("Caricamento in corso (<b>{0} KB</b> su <b>{1} KB</b>)"
                         .replace("{0}", Math.round(bytes / 1024))
                         .replace("{1}", Math.round(total / 1024))
                         );
                    }
                };
                swfu = new SWFUpload($.extend(defaults, options || {}));

                // Called when user clicks "cancel" (forces the upload to end, and eliminates progress bar immediately)
                $("span[id$=_uploading] input[type='button']", container).click(function () {
                    swfu.cancelUpload(null, false);
                });

                // Give the effect of preserving state, if requested
                if (options.existingFilename || "" != "") {
                    $("span[id$=_completedMessage]", container).html("Caricato <b>{0}</b> ({1} KB)"
                                    .replace("{0}", options.existingFilename)
                                    .replace("{1}", options.existingFileSize ? Math.round(options.existingFileSize / 1024) : "?")
                                ).show();
                    $("input[name$=_filename]", container).val(options.existingFilename);
                }
                if (options.existingGuid || "" != "")
                    $("input[name$=_guid]", container).val(options.existingGuid);
            });
        }
    })(jQuery);
</script>

<script>
    $(function () {

        var FILE_SIZE_LIMIT = $('#<%= FILE_SIZE_LIMIT.ClientID %>').val();
        var FILE_TYPES = $('#<%= FILE_TYPES.ClientID %>').val();
        var PAGE_ID = $('#<%= PAGE_ID.ClientID %>').val();

        var MIN_WIDTH = $('#<%= MIN_WIDTH.ClientID %>').val();
        var MIN_HEIGHT = $('#<%= MIN_HEIGHT.ClientID %>').val();
        var MAX_WIDTH = $('#<%= MAX_WIDTH.ClientID %>').val();
        var MAX_HEIGHT = $('#<%= MAX_HEIGHT.ClientID %>').val();

        var BETA_MODE = $('#<%= BETA_MODE.ClientID %>').val();
        var BETA_WIDTH = $('#<%= BETA_WIDTH.ClientID %>').val();
        var BETA_HEIGHT = $('#<%= BETA_HEIGHT.ClientID %>').val();
        var BETA_WEIGHT = $('#<%= BETA_WEIGHT.ClientID %>').val();

        var GAMMA_MODE = $('#<%= GAMMA_MODE.ClientID %>').val();
        var GAMMA_WIDTH = $('#<%= GAMMA_WIDTH.ClientID %>').val();
        var GAMMA_HEIGHT = $('#<%= GAMMA_HEIGHT.ClientID %>').val();
        var GAMMA_WEIGHT = $('#<%= GAMMA_WEIGHT.ClientID %>').val();

        var SESSION_ID = $('#<%= SESSION_ID.ClientID %>').val();
        var MY_INDEX = $('#<%= MY_INDEX.ClientID %>').val();

        var d = new Date();
        //alert(FILE_SIZE_LIMIT);

        $('#<%= yourID.ClientID %>').makeAsyncUploader({
            upload_url: "/Zeus/SiteAssets/ZeusImageRaider/AsyncUpload.ashx?PAGE_ID=" + PAGE_ID
                + "&MIN_WIDTH=" + MIN_WIDTH + "&MIN_HEIGHT=" + MIN_HEIGHT + "&MAX_WIDTH=" + MAX_WIDTH + "&MAX_HEIGHT=" + MAX_HEIGHT
                + "&BETA_HEIGHT=" + BETA_HEIGHT + "&BETA_WIDTH=" + BETA_WIDTH + "&BETA_MODE=" + BETA_MODE + "&BETA_WEIGHT=" + BETA_WEIGHT
                + "&GAMMA_HEIGHT=" + GAMMA_HEIGHT + "&GAMMA_WIDTH=" + GAMMA_WIDTH + "&GAMMA_MODE=" + GAMMA_MODE + "&GAMMA_WEIGHT=" + GAMMA_WEIGHT + "&" + d + "=" + d +
                +"&DELINEA_ASPSESSID=" + SESSION_ID

            ,

            flash_url: '/Zeus/SiteAssets/ZeusImageRaider/swfupload.swf',
            button_image_url: '/Zeus/SiteAssets/ZeusImageRaider/Bt4_CaricaImmagine.png',
            file_size_limit: FILE_SIZE_LIMIT,
            button_text: "",
            debug: false,
            button_window_mode: "transparent",
            file_types: FILE_TYPES,







            file_queue_error_handler: function (file, code, msg) {
                $('#<%= INFO.ClientID %>').html("");
                $('#<%= ALLERT.ClientID %>').html("Il file supera la dimensione consentita di " + FILE_SIZE_LIMIT + " KB <br />");
                $('#<%= LoadButtonCustomValidator.ClientID %>').html("");
            },


            upload_error_handler: function (file, code, msg) {
                $('#<%= INFO.ClientID %>').html("");

                $('#<%= ALLERT.ClientID %>').html("Il file supera la dimensione consentita di " + FILE_SIZE_LIMIT + " KB <br />");
                $('#<%= LoadButtonCustomValidator.ClientID %>').html("");
            },


            upload_success_handler: function (file, response) {

                var BETA_FILENAME = PAGE_ID + "BETA_" + file.name.split(' ').join('');
                var myBETA_path = "/ZeusInc/TempBeta/" + BETA_FILENAME;
                var GAMMA_FILENAME = PAGE_ID + "GAMMA_" + file.name.split(' ').join('');
                var myGAMMA_path = "/ZeusInc/TempGamma/" + GAMMA_FILENAME;


                if (response.toString() == "OK") {
                    $('#<%= ALLERT.ClientID %>').html("");
                    $('#<%= INFO.ClientID %>').html("Caricamento completato del file <b>{0}</b> ({1} KB)"
                            .replace("{0}", file.name)
                            .replace("{1}", Math.round(file.size / 1024)));

                    d = new Date();
                    $('#<%= FinalBETA_CropImage.ClientID %>').attr("src", myBETA_path + "?" + d.getTime());
                    $('#<%= FinalBETA_CropImage.ClientID %>').show();
                    $('#<%= FinalGAMMA_CropImage.ClientID %>').attr("src", myGAMMA_path + "?" + d.getTime());
                    $('#<%= FinalGAMMA_CropImage.ClientID %>').show();

                    $('#<%= BETA_FILENAME.ClientID %>').html("OK");
                    $('#<%= GAMMA_FILENAME.ClientID %>').html("OK");
                    $('#<%= LoadButtonCustomValidator.ClientID %>').html("");

                    sessionStorage.setItem("isDefaultImage" + PAGE_ID, false);
                    sessionStorage.setItem("isEmptyImage" + PAGE_ID, false);
                    $('#<%= isEmpty.ClientID %>').val('false');
                    $('#<%= isDefault.ClientID %>').val('false');

                }
                else if (response.toString() == "OKBETAGAMMA3") {

                    $('#<%= ALLERT.ClientID %>').html("");
                    $('#<%= INFO.ClientID %>').html("Caricamento completato del file <b>{0}</b> ({1} KB)"
                            .replace("{0}", file.name)
                            .replace("{1}", Math.round(file.size / 1024)));


                    d = new Date();

                    var mytext = '<table height="100%" align="center" border="0" cellspacing="21px" cellpadding="0">' + '<tr>';

                    if ($('#<%= BETA_MODE.ClientID %>').val() != '0') {
                        mytext = mytext + '<td valign="top" width="50%">'
                        + '<img src="' + myBETA_path + '?' + d.getTime() + '" id="CropBETA' + PAGE_ID + 'Image" onload="beta1Loaded(\'' + MY_INDEX + '\')" />'
                        + '</td>';
                    }
                    if ($('#<%= GAMMA_MODE.ClientID %>').val() != '0') {
                        mytext = mytext + '<td valign="top" width="50%">'
                        + '<img src="' + myGAMMA_path + '?' + d.getTime() + '" id="CropGAMMA' + PAGE_ID + 'Image" onload="gamma1Loaded(\'' + MY_INDEX + '\')" />'
                        + '</td>';
                    }
                    mytext = mytext + '</tr>' + '</table>';



                    $.prompt(mytext, {
                        buttons: { Ok: true, Annulla: false },
                        callback: function (v, m, f) {

                            if (v) {

                                $.post('/Zeus/SiteAssets/ZeusImageRaider/Crop.aspx?FILE_BETA_NAME=' + BETA_FILENAME
                                + '&BETA_X=' + $('#<%= BETA_X.ClientID %>').val()
                            + '&BETA_Y=' + $('#<%= BETA_Y.ClientID %>').val()
                            + '&BETA_W=' + $('#<%= BETA_W.ClientID %>').val()
                            + '&BETA_H=' + $('#<%= BETA_H.ClientID %>').val()
                            + '&BETA_WE=' + $('#<%= BETA_WEIGHT.ClientID %>').val()
                            + '&FILE_GAMMA_NAME=' + GAMMA_FILENAME
                            + '&GAMMA_X=' + $('#<%= GAMMA_X.ClientID %>').val()
                            + '&GAMMA_Y=' + $('#<%= GAMMA_Y.ClientID %>').val()
                            + '&GAMMA_W=' + $('#<%= GAMMA_W.ClientID %>').val()
                            + '&GAMMA_H=' + $('#<%= GAMMA_H.ClientID %>').val()
                            + '&GAMMA_WE=' + $('#<%= GAMMA_WEIGHT.ClientID %>').val()
                            , function (data) {
                                //alert(data);
                                d = new Date();
                                $('#<%= FinalBETA_CropImage.ClientID %>').attr("src", myBETA_path + "?" + d.getTime());
                                $('#<%= FinalBETA_CropImage.ClientID %>').show();
                                $('#<%= FinalGAMMA_CropImage.ClientID %>').attr("src", myGAMMA_path + "?" + d.getTime());
                                $('#<%= FinalGAMMA_CropImage.ClientID %>').show();

                                $('#<%= BETA_FILENAME.ClientID %>').html("OK");
                                $('#<%= GAMMA_FILENAME.ClientID %>').html("OK");
                                $('#<%= LoadButtonCustomValidator.ClientID %>').html("");

                            });
                            }
                            else {
                                //alert('Cancel');
                                setSessionVariable();
                                $('#<%= INFO.ClientID %>').html("");
                            }

                        }
                    });

                    sessionStorage.setItem("isDefaultImage" + PAGE_ID, false);
                    sessionStorage.setItem("isEmptyImage" + PAGE_ID, false);
                    $('#<%= isEmpty.ClientID %>').val('false');
                    $('#<%= isDefault.ClientID %>').val('false');

                    //if(MY_INDEX =="1")
                    //{
                    //  RunMeBETA1();
                    //  RunMeGAMMA1();
                    //}
                    //else if(MY_INDEX =="2")
                    //{
                    //  RunMeBETA2();
                    //  RunMeGAMMA2();
                    //}

                }//fine if 
                else if (response.toString() == "OKBETA3") {

                    $('#<%= ALLERT.ClientID %>').html("");
                    $('#<%= INFO.ClientID %>').html("Caricamento completato del file <b>{0}</b> ({1} KB)"
                            .replace("{0}", file.name)
                            .replace("{1}", Math.round(file.size / 1024)));



                    d = new Date();
                    var mytext = '<table height="100%" align="center" border="0" cellspacing="21px" cellpadding="0">' + '<tr>'

                    if ($('#<%= BETA_MODE.ClientID %>').val() != '0') {
                        mytext = mytext + '<td valign="top" width="50%">'
                        + '<img src="' + myBETA_path + '?' + d.getTime() + '" id="CropBETA' + PAGE_ID + 'Image" onload="beta2Loaded(\'' + MY_INDEX + '\')" />'
                        + '</td>'
                    }
                    if ($('#<%= GAMMA_MODE.ClientID %>').val() != '0') {
                        mytext = mytext + '<td valign="top" width="50%">'
                        + '<img src="' + myGAMMA_path + '?' + d.getTime() + '"  />'
                        + '</td>'
                    }
                    mytext = mytext + '</tr>' + '</table>'

                    $.prompt(mytext, {
                        buttons: { Ok: true, Annulla: false },
                        callback: function (v, m, f) {

                            if (v) {
                                $.post('/Zeus/SiteAssets/ZeusImageRaider/Crop.aspx?FILE_BETA_NAME=' + BETA_FILENAME
                                + '&BETA_X=' + $('#<%= BETA_X.ClientID %>').val()
                            + '&BETA_Y=' + $('#<%= BETA_Y.ClientID %>').val()
                            + '&BETA_W=' + $('#<%= BETA_W.ClientID %>').val()
                            + '&BETA_H=' + $('#<%= BETA_H.ClientID %>').val()
                            + '&BETA_WE=' + $('#<%= BETA_WEIGHT.ClientID %>').val()

                            , function (data) {
                                //alert(data);
                                d = new Date();
                                $('#<%= FinalBETA_CropImage.ClientID %>').attr("src", myBETA_path + "?" + d.getTime());
                                $('#<%= FinalBETA_CropImage.ClientID %>').show();
                                $('#<%= FinalGAMMA_CropImage.ClientID %>').attr("src", myGAMMA_path + "?" + d.getTime());
                                $('#<%= FinalGAMMA_CropImage.ClientID %>').show();

                                $('#<%= BETA_FILENAME.ClientID %>').html("OK");
                                $('#<%= GAMMA_FILENAME.ClientID %>').html("OK");
                                $('#<%= LoadButtonCustomValidator.ClientID %>').html("");

                            });
                            }

                            else {
                                //alert('Cancel');
                                setSessionVariable();
                                $('#<%= INFO.ClientID %>').html("");
                            }

                        }
                    });

                    $('#<%= isEmpty.ClientID %>').val('false');
                    $('#<%= isDefault.ClientID %>').val('false');
                    console.info("$('#<%= isEmpty.ClientID %>').value " + $('#<%= isEmpty.ClientID %>').val());

                    //if(MY_INDEX =="1")
                    //    RunMeBETA1();
                    //else if(MY_INDEX =="2")
                    //    RunMeBETA2();


                }//fine if 
                else if (response.toString() == "OKGAMMA3") {

                    $('#<%= ALLERT.ClientID %>').html("");
                    $('#<%= INFO.ClientID %>').html("Caricamento completato del file <b>{0}</b> ({1} KB)"
                            .replace("{0}", file.name)
                            .replace("{1}", Math.round(file.size / 1024)));



                    d = new Date();
                    var mytext = '<table height="100%" align="center" border="0" cellspacing="21px" cellpadding="0">' + '<tr>'

                    if ($('#<%= BETA_MODE.ClientID %>').val() != '0') {
                        mytext = mytext + '<td valign="top" width="50%">'
                        + '<img src="' + myBETA_path + '?' + d.getTime() + '"  />'
                        + '</td>'
                    }
                    if ($('#<%= GAMMA_MODE.ClientID %>').val() != '0') {
                        mytext = mytext + '<td valign="top" width="50%">'
                        + '<img src="' + myGAMMA_path + '?' + d.getTime() + '" id="CropGAMMA' + PAGE_ID + 'Image" onload="gamma2Loaded(\'' + MY_INDEX + '\')" />'
                        + '</td>'
                    }
                    mytext = mytext + '</tr>' + '</table>'



                    $.prompt(mytext, {

                        buttons: { Ok: true, Annulla: false },
                        callback: function (v, m, f) {

                            if (v) {

                                $.post('/Zeus/SiteAssets/ZeusImageRaider/Crop.aspx?FILE_GAMMA_NAME=' + GAMMA_FILENAME
                                + '&GAMMA_X=' + $('#<%= GAMMA_X.ClientID %>').val()
                            + '&GAMMA_Y=' + $('#<%= GAMMA_Y.ClientID %>').val()
                            + '&GAMMA_W=' + $('#<%= GAMMA_W.ClientID %>').val()
                            + '&GAMMA_H=' + $('#<%= GAMMA_H.ClientID %>').val()
                            + '&GAMMA_WE=' + $('#<%= GAMMA_WEIGHT.ClientID %>').val()
                            , function (data) {
                                //alert(data);
                                d = new Date();
                                $('#<%= FinalBETA_CropImage.ClientID %>').attr("src", myBETA_path + "?" + d.getTime());
                                $('#<%= FinalBETA_CropImage.ClientID %>').show();
                                $('#<%= FinalGAMMA_CropImage.ClientID %>').attr("src", myGAMMA_path + "?" + d.getTime());
                                $('#<%= FinalGAMMA_CropImage.ClientID %>').show();

                                $('#<%= BETA_FILENAME.ClientID %>').html("OK");
                                $('#<%= GAMMA_FILENAME.ClientID %>').html("OK");
                                $('#<%= LoadButtonCustomValidator.ClientID %>').html("");

                            });
                            }

                            else {
                                //alert('Cancel');
                                setSessionVariable();
                                $('#<%= INFO.ClientID %>').html("");
                                 }

                        }
                    });
                    sessionStorage.setItem("isDefaultImage" + PAGE_ID, false);
                    sessionStorage.setItem("isEmptyImage" + PAGE_ID, false);
                    $('#<%= isEmpty.ClientID %>').val('false');
                    $('#<%= isDefault.ClientID %>').val('false');


                    //if(MY_INDEX =="1")
                    //     RunMeGAMMA1();
                    // else if(MY_INDEX =="2")
                    //     RunMeGAMMA2();

                }//fine if 
                else if (response.toString() == "ERR1") {
                    $('#<%= ALLERT.ClientID %>').html("Dimensione immagine non corretta<br />");

                }//fine else



                if (response.toString() != "ERR1") {
                    PageMethods.ResetImgsFlags(PAGE_ID);

                    //                    sessionStorage.setItem("isDefaultImage" + PAGE_ID, false);
                    //                    sessionStorage.setItem("isEmptyImage" + PAGE_ID, false);

                    //                    setIsEmpty();

                    //                    $('#<%= FinalBETA_CropImage.ClientID %>').show();
                    //                    $('#CropBETA' + PAGE_ID + 'Image').show();


                    console.info("isDefaultImage " + PAGE_ID + " " + sessionStorage.getItem("isDefaultImage" + PAGE_ID));
                    console.info("isEmptyImage " + PAGE_ID + " " + sessionStorage.getItem("isEmptyImage" + PAGE_ID));
                    console.info("response " + response.toString());


                    console.info('ClientID ' + '#<%= FinalBETA_CropImage.ClientID %>');
                    console.info('CropBETA ' + '#CropBETA' + PAGE_ID + 'Image');

                }


            }//fine upload_success_handler



        });
    });

    //Funzioni sull'onload dell'immagine per risolvere bug dimensioni in Internet Explorer
    function beta1Loaded(arg) {
        var MY_INDEX = arg;
        var PAGE_ID = $('#<%= PAGE_ID.ClientID %>').val();
        eval('RunMeBETA' + MY_INDEX + '();');
        //if (MY_INDEX == "1") {
        //    RunMeBETA1(); 
        //}
        //else if (MY_INDEX == "2") {
        //    RunMeBETA2();
        //}
        //else if (MY_INDEX == "3") {
        //    RunMeBETA3();
        //}
        //else if (MY_INDEX == "P") {
        //    RunMeBETAP();
        //}
    }

    function gamma1Loaded(arg) {
        var MY_INDEX = arg;
        var PAGE_ID = $('#<%= PAGE_ID.ClientID %>').val();
        eval('RunMeGAMMA' + MY_INDEX + '();');
        //if (MY_INDEX == "1") {
        //    RunMeGAMMA1();
        //}
        //else if (MY_INDEX == "2") {
        //    RunMeGAMMA2();
        //}
        //else if (MY_INDEX == "3") {
        //    RunMeGAMMA3();
        //}
        //else if (MY_INDEX == "P") {
        //    RunMeGAMMAP();
        //}
    }

    function beta2Loaded(arg) {
        var MY_INDEX = arg;
        var PAGE_ID = $('#<%= PAGE_ID.ClientID %>').val();
        eval('RunMeBETA' + MY_INDEX + '();');
        //if (MY_INDEX == "1")
        //    RunMeBETA1();
        //else if (MY_INDEX == "2")
        //    RunMeBETA2();
        //else if (MY_INDEX == "3")
        //    RunMeBETA3();
        //else if (MY_INDEX == "P")
        //    RunMeBETAP();

    }

    function gamma2Loaded(arg) {
        var MY_INDEX = arg;
        var PAGE_ID = $('#<%= PAGE_ID.ClientID %>').val();
    eval('RunMeGAMMA' + MY_INDEX + '();');
    //if (MY_INDEX == "1")
    //    RunMeGAMMA1();
    //else if (MY_INDEX == "2")
    //    RunMeGAMMA2();
    //else if (MY_INDEX == "3")
    //    RunMeGAMMA3();
    //else if (MY_INDEX == "P")
    //    RunMeGAMMAP();

}


function FileUploadRequired(sender, args) {

    if ((document.getElementById("<%=BETA_FILENAME.ClientID%>").innerHTML.length <= 0)
    && ((document.getElementById("<%=GAMMA_FILENAME.ClientID%>").innerHTML.length <= 0)))
        args.IsValid = false;
    else args.IsValid = true;

    return;
}


function setSessionVariable(valueToSetTo) {
    __doPostBack('SetSessionVariable', valueToSetTo);
}

function setIsEmpty(valueToSetTo) {
    __doPostBack('isEmptyImage', valueToSetTo);
}


</script>

<asp:HiddenField ID="FILE_SIZE_LIMIT" runat="server" Value="100 MB" />
<asp:HiddenField ID="FILE_TYPES" runat="server" Value="*.*" />
<asp:HiddenField ID="SESSION_ID" runat="server" />
<asp:HiddenField ID="MY_INDEX" runat="server" />

<asp:Label ID="BETA_FILENAME" runat="server" Style="display: none" />
<asp:Label ID="GAMMA_FILENAME" runat="server" Style="display: none" />
<asp:HiddenField ID="PAGE_ID" runat="server" />
<asp:HiddenField ID="BETA_MODE" runat="server" />
<asp:HiddenField ID="BETA_HEIGHT" runat="server" />
<asp:HiddenField ID="BETA_WIDTH" runat="server" />
<asp:HiddenField ID="BETA_WEIGHT" runat="server" />
<asp:HiddenField ID="HiddenField1" runat="server" />
<asp:HiddenField ID="GAMMA_MODE" runat="server" />
<asp:HiddenField ID="GAMMA_WIDTH" runat="server" />
<asp:HiddenField ID="GAMMA_HEIGHT" runat="server" />
<asp:HiddenField ID="GAMMA_WEIGHT" runat="server" />
<asp:HiddenField ID="MIN_WIDTH" runat="server" />
<asp:HiddenField ID="MIN_HEIGHT" runat="server" />
<asp:HiddenField ID="MAX_WIDTH" runat="server" />
<asp:HiddenField ID="MAX_HEIGHT" runat="server" />
<asp:HiddenField ID="BETA_X" runat="server" />
<asp:HiddenField ID="BETA_Y" runat="server" />
<asp:HiddenField ID="BETA_W" runat="server" />
<asp:HiddenField ID="BETA_H" runat="server" />
<asp:HiddenField ID="GAMMA_X" runat="server" />
<asp:HiddenField ID="GAMMA_Y" runat="server" />
<asp:HiddenField ID="GAMMA_W" runat="server" />
<asp:HiddenField ID="GAMMA_H" runat="server" />

<div class="BlockBox">
    <div class="BlockBoxHeader">
        <asp:Label ID="BoxDescriptionLabel" runat="server">Immagine</asp:Label>
    </div>
    <table>
        <tr>
            <td class="BlockBoxDescription">
                <asp:Label ID="ComponentDescriptionLabel" runat="server" SkinID="FieldDescription"
                    Text="Label">Immagine</asp:Label>
            </td>
            <td class="BlockBoxValue">
                <table cellpadding="5" cellspacing="0" border="0">
                    <tr>
                        <td valign="middle">                                                       
                            <table border="0" cellspacing="0" cellpadding="2">
                                <tr>
                                    <td>
                                        <asp:Panel ID="MessagePanel" runat="server">
                                            <asp:Label ID="Message1Label" runat="server" SkinID="FileUploadNotes"></asp:Label><br />
                                            <asp:Label ID="Message2Label" runat="server" SkinID="FileUploadNotes"></asp:Label><br />
                                            <asp:Label ID="Message3Label" runat="server" SkinID="FileUploadNotes"></asp:Label><br />
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="file" id="yourID" name="yourID" runat="server" /><br />

                                        <input type="button" id="btnEmpty" name="btnEmpty" runat="server" class="imgRaiderBtn" value="RIMUOVI IMMAGINE"
                                            onserverclick="btnEmpty_ServerClick" />
                                        <br />
                                        <input type="button" id="btnDefault" name="btnDefault" runat="server" class="imgRaiderBtn" value="DEFAULT IMMAGINE" style="margin-top: 5px;"
                                            onserverclick="btnDefault_ServerClick" /><br />

                                        <asp:HiddenField runat="server" ID="isDefault" />
                                        <asp:HiddenField runat="server" ID="isEmpty" />

                                        <asp:Label ID="ALLERT" runat="server" CssClass="ZSSM_Validazione01"></asp:Label>
                                        <asp:Label ID="INFO" runat="server" CssClass="LabelSkin_CorpoTesto"></asp:Label>
                                        <asp:CustomValidator ID="LoadButtonCustomValidator" runat="server" ErrorMessage="Obbligatorio"
                                            SkinID="ZSSM_Validazione01" Display="Dynamic" ClientValidationFunction="FileUploadRequired" Enabled="false"></asp:CustomValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top">
                            <table border="0" cellspacing="0" cellpadding="2">
                                <tr>
                                    <td>
                                        <asp:Image ID="FinalBETA_CropImage" runat="server" CssClass="ImgAutoResize" /><br />
                                        <asp:Label ID="InfoBetaLabel" runat="server" Visible="false" SkinID="FileUploadNotes"></asp:Label><br />
                                        <asp:Label ID="MessageBetaLabel" runat="server" SkinID="FileUploadNotes" Visible="false"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Image ID="FinalGAMMA_CropImage" runat="server" CssClass="ImgAutoResize" /><br />
                                        <asp:Label ID="InfoGammaLabel" runat="server" Visible="false" SkinID="FileUploadNotes"></asp:Label><br />
                                        <asp:Label ID="MessageGammaLabel" runat="server" SkinID="FileUploadNotes" Visible="false"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="BlockBoxDescription">
                <asp:Label ID="ImageAltLabel" runat="server" SkinID="FieldDescription">Alt immagine</asp:Label>
            </td>
            <td class="BlockBoxValue">
                <asp:TextBox runat="server" ID="ImageAltTextBox"></asp:TextBox>
            </td>
        </tr>
    </table>
</div>
