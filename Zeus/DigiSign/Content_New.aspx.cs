﻿using ASP;
using System;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

/// <summary>
/// Descrizione di riepilogo per DigiSign_Content_New
/// </summary>
public partial class DigiSign_Content_New : System.Web.UI.Page
{    
    private delinea myDelinea = new delinea();

    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = "Nuovo DigiSign";

        if (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM"))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        ZIM.Value = Server.HtmlDecode(Request.QueryString["ZIM"]);

        ReadXML(ZIM.Value, GetLang());
        ReadXML_Localization(ZIM.Value, GetLang());
        LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");
        InsertButton.Attributes.Add("onclick", "Validate()");
    }

    private string GetLang()
    {
        string Lang = "ITA";

        try
        {
            if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
                    && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
                Lang = Request.QueryString["Lang"];
        }
        catch (Exception)
        {
        }

        return Lang;
    }

    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath ="ZIM/"+ ZeusIdModulo + "/" + ZeusLangCode + "/PAGE[@ID=\"Content_NEW\"]/";
            XmlDocument mydoc = new XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_DigiSign.xml"));


            Panel ZMCF_Link = (Panel)FormView1.FindControl("ZMCF_Link");

            if (mydoc.SelectSingleNode(myXPath + "ZMCF_Link") != null)
                ZMCF_Link.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Link").InnerText);
            

            Panel ZMCF_Contenuto1 = (Panel)FormView1.FindControl("ZMCF_Contenuto1");

            if (mydoc.SelectSingleNode(myXPath + "ZMCF_Contenuto1") != null)
                ZMCF_Contenuto1.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Contenuto1").InnerText);


            Panel ZMCF_Contenuto2 = (Panel)FormView1.FindControl("ZMCF_Contenuto2");

            if (mydoc.SelectSingleNode(myXPath + "ZMCF_Contenuto2") != null)
                ZMCF_Contenuto2.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Contenuto2").InnerText);


            Panel ZMCF_Contenuto3 = (Panel)FormView1.FindControl("ZMCF_Contenuto3");

            if (mydoc.SelectSingleNode(myXPath + "ZMCF_Contenuto3") != null)
                ZMCF_Contenuto3.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Contenuto3").InnerText);


            ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");

            if (mydoc.SelectSingleNode(myXPath + "ZMCF_BoxImmagini") != null)
                ImageRaider1.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxImmagini").InnerText);

            Panel ZMCD_Mode = (Panel)FormView1.FindControl("ZMCD_Mode");
            if (mydoc.SelectSingleNode(myXPath + "ZMCD_Mode") != null && mydoc.SelectSingleNode(myXPath + "ZMCD_Mode").InnerText == "MONO")
            {
                RequiredFieldValidator OrdinamentoRequiredFieldValidator = (RequiredFieldValidator)FormView1.FindControl("OrdinamentoRequiredFieldValidator");
                OrdinamentoRequiredFieldValidator.Enabled = true;
                ZMCD_Mode.Visible = true;
            }
            else
            {
                RequiredFieldValidator OrdinamentoRequiredFieldValidator = (RequiredFieldValidator)FormView1.FindControl("OrdinamentoRequiredFieldValidator");
                OrdinamentoRequiredFieldValidator.Enabled = false;
                ZMCD_Mode.Visible = false;
            }

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath ="ZIM/"+ ZeusIdModulo + "/" + ZeusLangCode + "/PAGE[@ID='Content_NEW']";
            XmlDocument mydoc = new XmlDocument();
            mydoc.Load(Server.MapPath("ZML_DigiSign.xml"));


            Label myLabel = null;
            delinea myDelinea = new delinea();
            XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {

                foreach (XmlNode parentNode in nodelist)
                {
                    foreach (XmlNode childNode in parentNode)
                    {
                        try
                        {
                            if (childNode.Name.Equals("ZML_TitoloPagina"))
                            {
                                TitleField.Value = childNode.InnerText;
                            }
                            else if (childNode.Name.Equals("ZML_TitoloBrowser"))
                            {
                                Page.Title = childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                            }
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    protected void ZeusIdHidden_Load(object sender, EventArgs e)
    {
        HiddenField Guid = (HiddenField)sender;
        if (Guid.Value.Length == 0)
            Guid.Value = System.Guid.NewGuid().ToString();
    }

    protected void UtenteCreazione(object sender, EventArgs e)
    {
        HiddenField UtenteCreazione = (HiddenField)sender;
        UtenteCreazione.Value = Membership.GetUser().ProviderUserKey.ToString();
    }

    protected void DataOggi(object sender, EventArgs e)
    {
        HiddenField DataCreazione = (HiddenField)sender;
        DataCreazione.Value = DateTime.Now.ToString();

    }

    protected void FileUploadCustomValidator_ServerValidate(object sender, ServerValidateEventArgs args)
    {
        try
        {
            ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");
            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");

            if (ImageRaider1.isValid())
                InsertButton.CommandName = "Insert";
            else
                InsertButton.CommandName = string.Empty;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    protected void Save_File_Upload(object sender, EventArgs e)
    {
        HiddenField FileNameBetaHiddenField = (HiddenField)FormView1.FindControl("FileNameBetaHiddenField");
        HiddenField FileNameGammaHiddenField = (HiddenField)FormView1.FindControl("FileNameGammaHiddenField");
        HiddenField Image12AltHiddenField = (HiddenField)FormView1.FindControl("Image12AltHiddenField");
        ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");

        LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");

        if (InsertButton.CommandName != string.Empty)
        {
            if (ImageRaider1.GenerateBeta(string.Empty))
                FileNameBetaHiddenField.Value = ImageRaider1.ImgBeta_FileName;
            else FileNameBetaHiddenField.Value = ImageRaider1.DefaultBetaImage;

            if (ImageRaider1.GenerateGamma(string.Empty))
                FileNameGammaHiddenField.Value = ImageRaider1.ImgGamma_FileName;
            else FileNameGammaHiddenField.Value = ImageRaider1.DefaultGammaImage;
        }
    }

    protected void tbDigiSignSqlDataSource_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        if ((e.Exception == null)
            && (e.AffectedRows > 0))
            Response.Redirect("~/Zeus/DigiSign/Content_Lst.aspx?XRI=" 
                + e.Command.Parameters["@XRI"].Value.ToString() 
                + "&ZIM=" + ZIM.Value 
                + "&Lang=" + GetLang());
        else Response.Redirect("~/Zeus/System/Message.aspx?InsertErr");
    }

    protected void PWF_Area1TextBox_DataBinding(object sender, EventArgs e)
    {
        TextBox DataLimite = (TextBox)sender;
        if (DataLimite.Text.Length == 0)
        {
            DataLimite.Text = "31/12/2040";
        }
    }

    protected void PWI_Area1TextBox_DataBinding(object sender, EventArgs e)
    {
        DateTime Data = new DateTime();
        Data = DateTime.Now;
        TextBox DataCreazione = (TextBox)sender;
        if (DataCreazione.Text.Length == 0)
        {
            DataCreazione.Text = Data.ToString("d");
        }
    }
}