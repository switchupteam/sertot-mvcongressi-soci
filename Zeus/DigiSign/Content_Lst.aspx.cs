﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

/// <summary>
/// Descrizione di riepilogo per Content_Lst
/// </summary>
public partial class DigiSign_Content_Lst : System.Web.UI.Page
{
    private delinea myDelinea = new delinea();

    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = "Elenco DigiSign";

        if (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM"))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        ZIM.Value = Server.HtmlDecode(Request.QueryString["ZIM"]);

        ReadXML(ZIM.Value, GetLang());
        ReadXML_Localization(ZIM.Value, GetLang());

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI"))
            && (Request.QueryString["XRI"].Length > 0) && (isOK()))
            SingleRecordPanel.Visible = true;

        tbDigiSignSqlDataSource.SelectCommand = @"SELECT ID1DigiSign,
                                                Identificativo,
                                                Contenuto1,
                                                ZeusLangCode,
                                                ZeusIdModulo,
                                                RecordNewDate,
                                                PW_Area1,
                                                PWI_Area1,
                                                PWF_Area1,
                                                Ordinamento,
                                                CASE
                                                    WHEN PW_Area1 = 0
                                                    THEN 0
                                                    WHEN
                                                        PW_Area1 = 1
                                                        AND DATEDIFF(second,PWI_Area1,GETDATE()) > 0
                                                        AND DATEDIFF(second,GETDATE(),PWF_Area1) > 0
                                                    THEN 1
                                                    ELSE 0
                                                END AS Attivo
                                                FROM tbDigiSign
                                                WHERE ZeusIsAlive=1
                                                    AND ZeusLangCode=@ZeusLangCode
                                                    AND ZeusIdModulo=@ZeusIdModulo
                                                ORDER BY Attivo DESC, Ordinamento ASC, Identificativo ASC";
    }

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "ZIM/" + ZeusIdModulo + "/" + ZeusLangCode + "/PAGE[@ID='Content_LST']";
            XmlDocument mydoc = new XmlDocument();
            mydoc.Load(Server.MapPath("ZML_DigiSign.xml"));

            Label myLabel = null;
            delinea myDelinea = new delinea();
            XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {

                foreach (XmlNode parentNode in nodelist)
                {
                    foreach (XmlNode childNode in parentNode)
                    {
                        try
                        {
                            if (childNode.Name.Equals("ZML_TitoloPagina"))
                            {
                                TitleField.Value = childNode.InnerText;
                            }
                            else if (childNode.Name.Equals("ZML_TitoloBrowser"))
                            {
                                Page.Title = childNode.InnerText;
                            }
                            else if (childNode.Name.Equals("ZML_Contenuto1"))
                            {
                                GridView2.Columns[1].HeaderText =
                                    GridView1.Columns[1].HeaderText =
                                    childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "ZIM/" + ZeusIdModulo + "/" + ZeusLangCode + "/PAGE[@ID=\"Content_LST\"]/";
            XmlDocument mydoc = new XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_DigiSign.xml"));

            if (mydoc.SelectSingleNode(myXPath + "ZMCF_Contenuto1") != null)
                GridView2.Columns[1].Visible = GridView1.Columns[1].Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Contenuto1").InnerText);
            if (mydoc.SelectSingleNode(myXPath + "ZMCD_Mode") != null)
            {
                if (mydoc.SelectSingleNode(myXPath + "ZMCD_Mode").InnerText != "MONO")
                {
                    GridView1.Columns[3].Visible = false;
                    GridView1.Columns[4].Visible = false;
                    GridView1.Columns[5].Visible = false;
                    GridView1.Columns[6].Visible = false;
                    GridView2.Columns[3].Visible = false;
                    GridView2.Columns[4].Visible = false;
                    GridView2.Columns[5].Visible = false;
                    GridView2.Columns[6].Visible = false;
                    tbDigiSignSqlDataSource.SelectCommand += @" ORDER BY Attivo(SI), Ordinamento DESC, DataInizio DESC";
                }
            }

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    private bool isOK()
    {
        try
        {
            if ((Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/DigiSign/Content_Edt.aspx") > 0)
                || (Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/DigiSign/Content_New.aspx") > 0))
                return true;
            else return false;
        }
        catch (Exception p)
        {
            return false;
        }
    }

    private string GetLang()
    {
        string Lang = "ITA";

        try
        {
            if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
                    && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
                Lang = Request.QueryString["Lang"];
        }
        catch (Exception)
        {
        }

        return Lang;
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            bool valid = true;

            if (e.Row.DataItemIndex > -1)
            {
                try
                {
                    Convert.ToDateTime(e.Row.Cells[5].Text);
                    Convert.ToDateTime(e.Row.Cells[6].Text);
                }
                catch (Exception)
                {
                    valid = false;
                }

                Image Image1 = (Image)e.Row.FindControl("Image1");
                Label PW_Area1 = (Label)e.Row.FindControl("PW_Area1");
                Image FlagImage = (Image)e.Row.FindControl("FlagImage");

                if (valid && Convert.ToBoolean(PW_Area1.Text))
                {
                    if (Convert.ToDateTime(e.Row.Cells[5].Text) <= DateTime.Now && Convert.ToDateTime(e.Row.Cells[6].Text) >= DateTime.Now)
                        Image1.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";
                    if (Convert.ToBoolean(FlagImage.ToolTip))
                        FlagImage.ImageUrl = "~/Zeus/SiteImg/Ico1_Flag_On.gif";

                }
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }
}