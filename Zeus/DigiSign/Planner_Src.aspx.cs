﻿using System;

/// <summary>
/// Descrizione di riepilogo per Planner_Src
/// </summary>
public partial class DigiSign_Planner_Src : System.Web.UI.Page
{
    private delinea myDelinea = new delinea();

    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = "Ricerca DigiSign";

        if (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM"))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        ZIM.Value = Server.HtmlDecode(Request.QueryString["ZIM"]);
    }

    private string GetLang()
    {
        string Lang = "ITA";

        try
        {
            if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
                    && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
                Lang = Request.QueryString["Lang"];
        }
        catch (Exception)
        {
        }

        return Lang;
    }

    protected void SearchButton_Click(object sender, EventArgs e)
    {
        string myRedirect = "Planner_Lst.aspx?ZIM=" + ZIM.Value
             + "&Lang=" + GetLang();

        if (!ID2DigiSignDropDownList.SelectedValue.Equals("0"))
            myRedirect += "&XRI1=" + ID2DigiSignDropDownList.SelectedValue;

        if (!AttivoDropDownList1.SelectedValue.Equals("_"))
            myRedirect += "&XRI2=" + AttivoDropDownList1.SelectedValue;

        Response.Redirect(myRedirect);
    }
}