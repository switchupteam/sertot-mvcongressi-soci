﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="Planner_New.aspx.cs"
    Inherits="DigiSign_Planner_New"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Xml" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <asp:HiddenField ID="ZIM" runat="server" />
    <asp:FormView ID="FormView1" runat="server" DataSourceID="tbDigiSign_PlannerSqlDataSource1"
        DefaultMode="Insert" Width="100%">
        <InsertItemTemplate>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="ZML_BoxDettaglio" runat="server">Digisign</asp:Label></div>
                <table>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label7" runat="server" SkinID="FieldDescription" Text="Label">Identificativo *</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:DropDownList ID="ID2DigiSignDropDownList" runat="server" DataSourceID="tbDigiSignSqlDataSource"
                                DataTextField="Identificativo" DataValueField="ID1DigiSign" SelectedValue='<%# Bind("ID2DigiSign") %>'
                                AppendDataBoundItems="True">
                                <asp:ListItem Selected="True" Value="0">> Seleziona</asp:ListItem>
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="tbDigiSignSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                SelectCommand="SELECT [ID1DigiSign],[Identificativo]
                    FROM [tbDigiSign] 
                    WHERE [ZeusIsAlive]=1 
                    AND ZeusLangCode=@ZeusLangCode AND ZeusIdModulo=@ZeusIdModulo ORDER BY Identificativo">
                                <SelectParameters>
                                    <asp:QueryStringParameter Name="ZeusLangCode" QueryStringField="Lang" />
                                    <asp:QueryStringParameter Name="ZeusIdModulo" QueryStringField="ZIM" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                            <asp:RequiredFieldValidator ID="TitoloPaginaRequiredFieldValidator" ErrorMessage="Obbligatorio"
                                runat="server" ControlToValidate="ID2DigiSignDropDownList" SkinID="ZSSM_Validazione01"
                                InitialValue="0" Display="Dynamic">
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label1" runat="server" SkinID="FieldDescription" Text="Label">Ordinamento (1 - 99) *</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:TextBox ID="OrdinamentoTextBox" runat="server"  Text='<%# Bind("Ordinamento") %>'
                            MaxLength="2" Columns="1"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Obbligatorio"
                                runat="server" ControlToValidate="OrdinamentoTextBox" SkinID="ZSSM_Validazione01"
                                Display="Dynamic">
                            </asp:RequiredFieldValidator>
                            <asp:RangeValidator ID="RangeValidator1" ControlToValidate="OrdinamentoTextBox" MinimumValue="1"
                                MaximumValue="99" Type="Integer" ErrorMessage="Inserire un valore compreso tra 1 e 20"
                                SkinID="ZSSM_Validazione01" runat="server" Display="Dynamic" />
                        </td>
                    </tr>
                    <asp:Panel ID="ZMCF_Duration" runat="server">
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label2" runat="server" SkinID="FieldDescription" Text="Label">Durata *</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:TextBox ID="DurationTextBox" runat="server"  Text='<%# Bind("Duration") %>'
                            MaxLength="3" Columns="4"></asp:TextBox>
                            <asp:Label ID="Label3" runat="server" SkinID="FieldValue" Text="Label"> secondi</asp:Label>
                            <asp:RangeValidator ID="RangeValidator2" ControlToValidate="DurationTextBox" MinimumValue="1"
                                MaximumValue="120" Type="Integer" ErrorMessage="Inserire un valore compreso tra 1 e 120"
                                SkinID="ZSSM_Validazione01" runat="server" Display="Dynamic" />
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Obbligatorio"
                                runat="server" ControlToValidate="DurationTextBox" SkinID="ZSSM_Validazione01"
                                Display="Dynamic">
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    </asp:Panel>
                </table>
            </div>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="Planner" runat="server" Text="Planner"></asp:Label></div>
                <table>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="PZL_PWArea1Label" SkinID="FieldDescription" runat="server"></asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:CheckBox ID="PW_Area1CheckBox" runat="server" Checked='<%# Bind("PW_Area1") %>' />
                            <asp:Label ID="AttivaDa_Label" runat="server" Text="Attiva pubblicazione dalla data"
                                SkinID="FieldValue"></asp:Label>
                            <asp:TextBox ID="PWI_Area1TextBox" runat="server" Text='<%# Bind("PWI_Area1", "{0:d}") %>'
                                Columns="10" MaxLength="10" OnDataBinding="PWI_Area1TextBox_DataBinding"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="PWI_Area1TextBox"
                                SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="PWI_Area1TextBox"
                                SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="*" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$">Formato data richiesto: GG/MM/AAAA</asp:RegularExpressionValidator>
                            <asp:Label ID="AttivaA_Label" runat="server" Text="alla data" SkinID="FieldValue"></asp:Label>
                            <asp:TextBox ID="PWF_Area1TextBox" runat="server" Text='<%# Bind("PWF_Area1", "{0:d}") %>'
                                Columns="10" MaxLength="10" OnDataBinding="PWF_Area1TextBox_DataBinding"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="PWF_Area1TextBox"
                                SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                    ID="RegularExpressionValidator3" runat="server" ControlToValidate="PWF_Area1TextBox"
                                    SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="*" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$">Formato data richiesto: GG/MM/AAAA</asp:RegularExpressionValidator>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="PWI_Area1TextBox"
                                ControlToValidate="PWF_Area1TextBox" SkinID="ZSSM_Validazione01" Display="Dynamic"
                                ErrorMessage="La data iniziale deve essere precedente o uguale alla data finale"
                                Operator="GreaterThanEqual" Type="Date"></asp:CompareValidator>
                        </td>
                    </tr>
                </table>
            </div>
            <asp:HiddenField ID="RecordNewUserHidden" runat="server" Value='<%# Bind("RecordNewUser", "{0}") %>'
                OnDataBinding="UtenteCreazione" />
            <asp:HiddenField ID="RecordNewDateHidden" runat="server" Value='<%# Bind("RecordNewDate", "{0}") %>'
                OnDataBinding="DataOggi" />
            <asp:HiddenField ID="ZeusIdHidden" runat="server" OnDataBinding="ZeusIdHidden_Load"
                Value='<%# Bind("ZeusId", "{0}") %>' />
            <div align="center">
                <dlc:mySummaryValidation ID="mySummaryValidation1" runat="server" SkinID="ZSSM_Validazione01" />
                <asp:LinkButton ID="InsertButton" runat="server" SkinID="ZSSM_Button01" CausesValidation="True"
                    CommandName="Insert" Text="Salva dati"></asp:LinkButton></div>
        </InsertItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="tbDigiSign_PlannerSqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        InsertCommand="SET DATEFORMAT dmy; 
                        INSERT INTO [tbDigiSign_Planner] 
                            ([ID2DigiSign], 
                            [Ordinamento],         
                            [Duration], 
                            [PW_Area1], 
                            [PWI_Area1], 
                            [PWF_Area1], 
                            [RecordNewUser],          
                            [RecordNewDate], 
                            [ZeusId], 
                            [ZeusIsAlive]) 
                        VALUES 
                            (@ID2DigiSign, 
                            @Ordinamento,          
                            @Duration, 
                            @PW_Area1,
                            @PWI_Area1,
                            @PWF_Area1, 
                            @RecordNewUser,           
                            @RecordNewDate, 
                            @ZeusId, 
                            @ZeusIsAlive); 
                        SELECT @XRI = SCOPE_IDENTITY();" 
        OnInserted="tbDigiSign_PlannerSqlDataSource1_Inserted">
        <InsertParameters>
            <asp:Parameter Name="ID2DigiSign" Type="Int32" />
            <asp:Parameter Name="Ordinamento" Type="Int16" />
            <asp:Parameter Name="Duration" Type="Int16"  />
            <asp:Parameter Name="PW_Area1" Type="Boolean" />
            <asp:Parameter Name="PWI_Area1" Type="DateTime" />
            <asp:Parameter Name="PWF_Area1" Type="DateTime" />
            <asp:Parameter Name="RecordNewUser" />
            <asp:Parameter Name="RecordNewDate" Type="DateTime" />
            <asp:Parameter Name="ZeusId" />
            <asp:Parameter Name="ZeusIsAlive" Type="Boolean" DefaultValue="True" />
            <asp:Parameter Direction="Output" Name="XRI" Type="Int32" />
        </InsertParameters>
    </asp:SqlDataSource>
</asp:Content>
