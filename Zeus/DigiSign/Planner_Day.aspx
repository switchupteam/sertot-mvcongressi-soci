﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="Planner_Day.aspx.cs"
    Inherits="DigiSign_Planner_Day"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Xml" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <asp:HiddenField ID="ZIM" runat="server" />
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td style="width: 15%; vertical-align: top;">
                <asp:TextBox ID="FirstDate" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                <asp:Image ID="CalendarioImage" runat="server" ImageUrl="~/Zeus/SiteImg/Calendario.jpg" />
                <ajaxToolkit:CalendarExtender ID="CalendarExtender" runat="server" TargetControlID="FirstDate"
                    PopupButtonID="CalendarioImage" Format="dd/MM/yyyy" />
                <asp:LinkButton ID="SearchLinkButton" CausesValidation="true" runat="server" Text="Cerca"
                    SkinID="ZSSM_Button01" OnClick="SearchLinkButton_Click" ValidationGroup="myValidation"></asp:LinkButton>
                <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="FirstDateRegularExpressionValidator"
                    runat="server" ControlToValidate="FirstDate" SkinID="ZSSM_Validazione01" Display="Dynamic"
                    ErrorMessage="*" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"><br />Formato data richiesto: GG/MM/AAAA</asp:RegularExpressionValidator>

            </td>
            <td style="vertical-align: top;">
                <div class="LayGridTitolo1">
                    <asp:Label ID="Label2" runat="server" SkinID="GridTitolo1" Text="Elenco contenuti disponibili"></asp:Label>
                </div>
                <asp:GridView ID="GridView2" runat="server" AllowPaging="True" AllowSorting="True"
                    AutoGenerateColumns="False" DataSourceID="tbDigiSignSqlDataSource" PageSize="50"
                    OnRowDataBound="GridView1_RowDataBound" DataKeyNames="ID1Planner">
                    <Columns>
                        <asp:TemplateField HeaderText="AttivoArea1" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="AttivoArea1" runat="server" Text='<%# Eval("AttivoArea1") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Identificativo" SortExpression="Identificativo">
                            <ItemTemplate>
                                <asp:Label ID="IdentificativoLabel" runat="server" Text='<%# Eval("Identificativo") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ordinamento" SortExpression="Ordinamento">
                            <ItemTemplate>
                                <asp:Label ID="OrdinamentoLabel" runat="server" Text='<%# Eval("Ordinamento") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="OrdinamentoTextBox" runat="server" Text='<%# Bind("Ordinamento") %>'
                                    MaxLength="2" Columns="5"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Obbligatorio"
                                    runat="server" ControlToValidate="OrdinamentoTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic">
                                </asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="RangeValidator1" ControlToValidate="OrdinamentoTextBox" MinimumValue="1"
                                    MaximumValue="20" Type="Integer" ErrorMessage="Inserire un valore compreso tra 1 e 20"
                                    SkinID="ZSSM_Validazione01" runat="server" Display="Dynamic" />
                            </EditItemTemplate>
                            <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Durata" SortExpression="Duration">
                            <ItemTemplate>
                                <asp:Label ID="DurationLabel" runat="server" Text='<%# Eval("Duration") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="DurationTextBox" runat="server" Text='<%# Bind("Duration") %>' MaxLength="3"
                                    Columns="5"></asp:TextBox>
                                <asp:RangeValidator ID="RangeValidator2" ControlToValidate="DurationTextBox" MinimumValue="1"
                                    MaximumValue="120" Type="Integer" ErrorMessage="Inserire un valore compreso tra 1 e 120"
                                    SkinID="ZSSM_Validazione01" runat="server" Display="Dynamic" />
                            </EditItemTemplate>
                            <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="AttivoArea" SortExpression="AttivoArea1">
                            <ItemTemplate>
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Pub." SortExpression="PW_Area1">
                            <ItemTemplate>
                                <asp:Image ID="FlagImage" runat="server" ToolTip='<%# Eval("PW_Area1") %>' ImageUrl="~/Zeus/SiteImg/Ico1_Flag_Off.gif" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:CheckBox ID="PW_Area1CheckBox" runat="server" Checked='<%# Bind("PW_Area1") %>' />
                            </EditItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Inizio" SortExpression="PWI_Area1">
                            <ItemTemplate>
                                <asp:Label ID="PWI_Area1Label" runat="server" Text='<%# Eval("PWI_Area1","{0:d}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="PWI_Area1TextBox" runat="server" Text='<%# Bind("PWI_Area1", "{0:d}") %>'
                                    Columns="10" MaxLength="10"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="PWI_Area1TextBox"
                                    SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="PWI_Area1TextBox"
                                    SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="*" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$">Formato data richiesto: GG/MM/AAAA</asp:RegularExpressionValidator>
                            </EditItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Fine" SortExpression="PWF_Area1">
                            <ItemTemplate>
                                <asp:Label ID="PWF_Area1Label" runat="server" Text='<%# Eval("PWF_Area1","{0:d}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="PWF_Area1TextBox" runat="server" Text='<%# Bind("PWF_Area1", "{0:d}") %>'
                                    Columns="10" MaxLength="10"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="PWF_Area1TextBox"
                                    SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                        ID="RegularExpressionValidator3" runat="server" ControlToValidate="PWF_Area1TextBox"
                                        SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="*" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$">Formato data richiesto: GG/MM/AAAA</asp:RegularExpressionValidator>
                                <%--<asp:CustomValidator ID="DataCompareCustomValidator" runat="server" SkinID="ZSSM_Validazione01"
                        Display="Dynamic"
                        OnServerValidate="DataCompareCustomValidator_ServerValidate"
                        Text='<%# Eval("PWI_Area1","{0:d}") %>'
                        ToolTip='<%# Eval("PWF_Area1","{0:d}") %>'
                         ErrorMessage="La data iniziale deve essere precedente o uguale alla data finale"></asp:CustomValidator>--%>
                            </EditItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:CommandField ButtonType="Link" HeaderText="Modifica" ShowEditButton="true" UpdateText="Salva"
                            EditText="Modifica" ControlStyle-CssClass="GridView_Button1" ItemStyle-HorizontalAlign="Center"
                            CancelText="Annulla" CausesValidation="true" />
                        <asp:HyperLinkField DataNavigateUrlFields="ID1Planner,ZeusLangCode,ZeusIdModulo"
                            DataNavigateUrlFormatString="~/Zeus/DigiSign/Planner_Del.aspx?XRI={0}&Lang={1}&ZIM={2}"
                            HeaderText="Planning">
                            <ControlStyle CssClass="GridView_ZeusButton1" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:HyperLinkField>
                    </Columns>
                    <EmptyDataTemplate>
                        Dati non presenti
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </EmptyDataTemplate>
                    <EmptyDataRowStyle BackColor="White" Width="500px" BorderColor="Red" BorderWidth="0px"
                        Height="200px" />
                </asp:GridView>
                <asp:SqlDataSource ID="tbDigiSignSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                    SelectCommand="SELECT [ID1Planner]
      ,[ID2DigiSign]
      ,[Ordinamento]
      ,[Duration]
      ,Identificativo
      ,[AttivoArea1]
      ,[PW_Area1]
      ,[PWI_Area1]
      ,[PWF_Area1],ZeusLangCode,ZeusIdModulo 
  FROM [vwDigiSign_Lst_Multi] WHERE  PWI_Area1=@Start"
                    UpdateCommand="UPDATE [tbDigiSign_Planner] SET 
   [Ordinamento] = @Ordinamento, [Duration] = @Duration, [PW_Area1] = @PW_Area1,
    [PWI_Area1] = @PWI_Area1, [PWF_Area1] = @PWF_Area1 
      WHERE [ID1Planner] = @ID1Planner ">
                    <SelectParameters>
                        <asp:ControlParameter Name="Start" ControlID="FirstDate" Type="DateTime" PropertyName="Text" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="Ordinamento" Type="Int16" ConvertEmptyStringToNull="false" DefaultValue="0" />
                        <asp:Parameter Name="Duration" Type="Int16" ConvertEmptyStringToNull="false" DefaultValue="0" />
                        <asp:Parameter Name="PW_Area1" Type="Boolean" />
                        <asp:Parameter Name="PWI_Area1" Type="DateTime" />
                        <asp:Parameter Name="PWF_Area1" Type="DateTime" />
                    </UpdateParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
    </table>
</asp:Content>
