﻿using ASP;
using System;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

/// <summary>
/// Descrizione di riepilogo per Content_Dtl
/// </summary>
public partial class DigiSign_Content_Dtl : System.Web.UI.Page
{
    private delinea myDelinea = new delinea();

    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = "Dettaglio DigiSign";

        if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM"))
            || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI")))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        ZIM.Value = Server.HtmlDecode(Request.QueryString["ZIM"]);
        XRI.Value = Server.HtmlDecode(Request.QueryString["XRI"]);

        ReadXML(ZIM.Value, GetLang());
        ReadXML_Localization(ZIM.Value, GetLang());
    }

    private string GetLang()
    {
        string Lang = "ITA";

        try
        {
            if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
                    && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
                Lang = Request.QueryString["Lang"];
        }
        catch (Exception)
        {
        }

        return Lang;
    }

    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "ZIM/" + ZeusIdModulo + "/" + ZeusLangCode + "/PAGE[@ID=\"Content_DTL\"]/";
            XmlDocument mydoc = new XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_DigiSign.xml"));

            Panel ZMCF_Link = (Panel)FormView1.FindControl("ZMCF_Link");

            if (mydoc.SelectSingleNode(myXPath + "ZMCF_Link") != null)
                ZMCF_Link.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Link").InnerText);

            Panel ZMCF_Contenuto1 = (Panel)FormView1.FindControl("ZMCF_Contenuto1");

            if (mydoc.SelectSingleNode(myXPath + "ZMCF_Contenuto1") != null)
                ZMCF_Contenuto1.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Contenuto1").InnerText);

            Panel ZMCF_Contenuto2 = (Panel)FormView1.FindControl("ZMCF_Contenuto2");

            if (mydoc.SelectSingleNode(myXPath + "ZMCF_Contenuto2") != null)
                ZMCF_Contenuto2.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Contenuto2").InnerText);

            Panel ZMCF_Contenuto3 = (Panel)FormView1.FindControl("ZMCF_Contenuto3");

            if (mydoc.SelectSingleNode(myXPath + "ZMCF_Contenuto3") != null)
                ZMCF_Contenuto3.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Contenuto3").InnerText);

            ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");

            if (mydoc.SelectSingleNode(myXPath + "ZMCF_BoxImmagini") != null)
                ImageRaider1.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxImmagini").InnerText);

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "ZIM/" + ZeusIdModulo + "/" + ZeusLangCode + "/PAGE[@ID='Content_DTL']";
            XmlDocument mydoc = new XmlDocument();
            mydoc.Load(Server.MapPath("ZML_DigiSign.xml"));


            Label myLabel = null;
            delinea myDelinea = new delinea();
            XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {

                foreach (XmlNode parentNode in nodelist)
                {
                    foreach (XmlNode childNode in parentNode)
                    {
                        try
                        {
                            if (childNode.Name.Equals("ZML_TitoloPagina"))
                            {
                                TitleField.Value = childNode.InnerText;
                            }
                            else if (childNode.Name.Equals("ZML_TitoloBrowser"))
                            {
                                Page.Title = childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    protected void UtenteCreazione(object sender, EventArgs e)
    {
        HiddenField UtenteCreazione = (HiddenField)sender;
        UtenteCreazione.Value = Membership.GetUser().ProviderUserKey.ToString();
    }

    protected void DataOggi(object sender, EventArgs e)
    {
        HiddenField DataCreazione = (HiddenField)sender;
        DataCreazione.Value = DateTime.Now.ToString();
    }

    protected void ImageRaider_DataEvaling(object sender, EventArgs e)
    {
        ImageRaider ImageRaider1 = (ImageRaider)sender;
        ImageRaider1.SetDefaultEditBetaImage();
        ImageRaider1.SetDefaultEditGammaImage();
    }

    protected void tbDigiSignSqlDataSource_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {
        if ((e.AffectedRows <= 0)
            || (e.Exception != null))
            Response.Redirect("~/Zeus/System/Message.aspx?SelectErr");
    }

    protected void ModificaButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Zeus/DigiSign/Content_Edt.aspx?XRI=" + XRI.Value + "&ZIM=" + ZIM.Value + "&Lang=" + GetLang());
    }

    protected void DeleteLinkButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Zeus/DigiSign/Content_Del.aspx?XRI=" + Server.HtmlEncode(Request.QueryString["XRI"])
            + "&Lang=" + GetLang()
            + "&ZIM=" + Server.HtmlEncode(Request.QueryString["ZIM"]));
    }
}