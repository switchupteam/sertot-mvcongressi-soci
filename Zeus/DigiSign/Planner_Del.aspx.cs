﻿using System;
using System.Data.SqlClient;

/// <summary>
/// Descrizione di riepilogo per Planner_Del
/// </summary>
public partial class DigiSign_Planner_Del : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();
        
        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI")) && (Request.QueryString["XRI"].Length > 0) && (isOK()))
            if (DeleteRecord(Request.QueryString["XRI"]))
                Response.Redirect("~/Zeus/System/Message.aspx?Msg=7867868456154568&Lang=" + GetLang());

        Response.Redirect("~/Zeus/System/Message.aspx");
    }

    private bool isOK()
    {
        try
        {
            if ((Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/DigiSign/Planner_Lst.aspx") > 0)
                ||(Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/AdFarm/Planner_Lst_PerBanner.aspx") > 0) 
                || (Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/AdFarm/Planner_Lst_PerSlot.aspx") > 0))
                return true;
            else return false;
        }
        catch (Exception)
        {
            return false;
        }
    }

    private string GetLang()
    {
        try
        {
            delinea myDelinea = new delinea();
            string Lang = "ITA";

            if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
            {
                if (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang"))
                    Lang = Request.QueryString["Lang"];
                else Response.Redirect("~/System/Message.aspx");
            }

            return Lang;
        }
        catch (Exception)
        {
            return "ITA";
        }
    }

    private bool DeleteRecord(string XRI)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {
            
            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                sqlQuery = "DELETE FROM tbDigiSign_Planner WHERE ID1Planner=@ID1Planner";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID1Planner", System.Data.SqlDbType.Int, 4);
                command.Parameters["@ID1Planner"].Value = XRI;
                command.ExecuteNonQuery();
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
                return false;
            }            
        }

        return true;
    }    
}