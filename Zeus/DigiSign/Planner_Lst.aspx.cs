﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

/// <summary>
/// Descrizione di riepilogo per Planner_Lst
/// </summary>
public partial class DigiSign_Planner_Lst : System.Web.UI.Page
{
    private delinea myDelinea = new delinea();

    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = "Elenco DigiSign";

        if (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM"))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        ZIM.Value = Server.HtmlDecode(Request.QueryString["ZIM"]);

        ReadXML(ZIM.Value, GetLang());
        ReadXML_Localization(ZIM.Value, GetLang());

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI"))
            && (Request.QueryString["XRI"].Length > 0) && (isOK()))
            SingleRecordPanel.Visible = true;

        if (myDelinea.AntiSQLInjectionNew(Request.QueryString, "XRI1", "int"))
            tbDigiSignSqlDataSource.SelectCommand += " AND ID2DigiSign=" + Request.QueryString["XRI1"];

        if (myDelinea.AntiSQLInjectionNew(Request.QueryString, "XRI2", "int"))
            tbDigiSignSqlDataSource.SelectCommand += " AND AttivoArea1=" + Request.QueryString["XRI2"];
        
        tbDigiSignSqlDataSource.SelectCommand += " ORDER BY identificativo ASC, ordinamento ASC";
    }

    private bool isOK()
    {
        try
        {
            if ((Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/DigiSign/Content_Edt.aspx") > 0)
                || (Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/DigiSign/Content_New.aspx") > 0))
                return true;
            else return false;
        }
        catch (Exception p)
        {
            return false;
        }
    }

    private string GetLang()
    {
        string Lang = "ITA";

        try
        {
            if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
                    && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
                Lang = Request.QueryString["Lang"];
        }
        catch (Exception)
        {
        }

        return Lang;
    }

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath ="ZIM/"+ ZeusIdModulo + "/" + ZeusLangCode + "/PAGE[@ID='Planner_LST']";
            XmlDocument mydoc = new XmlDocument();
            mydoc.Load(Server.MapPath("ZML_DigiSign.xml"));


            Label myLabel = null;
            delinea myDelinea = new delinea();
            XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {

                foreach (XmlNode parentNode in nodelist)
                {
                    foreach (XmlNode childNode in parentNode)
                    {
                        try
                        {
                            if (childNode.Name.Equals("ZML_TitoloPagina"))
                            {
                                TitleField.Value = childNode.InnerText;
                            }
                            else if (childNode.Name.Equals("ZML_TitoloBrowser"))
                            {
                                Page.Title = childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath ="ZIM/"+ ZeusIdModulo + "/" + ZeusLangCode + "/PAGE[@ID=\"Planner_LST\"]/";
            XmlDocument mydoc = new XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_DigiSign.xml"));

            if (mydoc.SelectSingleNode(myXPath + "ZMCF_Duration") != null)
                GridView2.Columns[3].Visible = GridView1.Columns[3].Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Duration").InnerText);

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }
    
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.DataItemIndex > -1)
        {
            Image Image1 = (Image)e.Row.FindControl("Image1");
            Label AttivoArea1 = (Label)e.Row.FindControl("AttivoArea1");
            Image FlagImage = (Image)e.Row.FindControl("FlagImage");

            try
            {
                int intAttivoArea1 = Convert.ToInt32(AttivoArea1.Text.ToString());

                if (intAttivoArea1 == 1)
                    Image1.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";

                if (Convert.ToBoolean(FlagImage.ToolTip))
                    FlagImage.ImageUrl = "~/Zeus/SiteImg/Ico1_Flag_On.gif";

                FlagImage.ToolTip = string.Empty;
            }
            catch (Exception)
            {
            }
        }
    }
}