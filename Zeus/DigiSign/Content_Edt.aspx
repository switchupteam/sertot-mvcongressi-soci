﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="Content_Edt.aspx.cs"
    Inherits="DigiSign_Content_Edt"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Xml" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
     <script type="text/javascript">
         function OnClientModeChange(editor) {
             var mode = editor.get_mode();
             var doc = editor.get_document();
             var head = doc.getElementsByTagName("HEAD")[0];
             var link;

             switch (mode) {
                 case 1: //remove the external stylesheet when displaying the content in Design mode    
                     //var external = doc.getElementById("external");
                     //head.removeChild(external);
                     break;
                 case 2:
                     break;
                 case 4: //apply your external css stylesheet to Preview mode    
                     link = doc.createElement("LINK");
                     link.setAttribute("href", "/SiteCss/Telerik.css");
                     link.setAttribute("rel", "stylesheet");
                     link.setAttribute("type", "text/css");
                     link.setAttribute("id", "external");
                     head.appendChild(link);
                     break;
             }
         }

         function editorCommandExecuted(editor, args) {
             if (!$telerik.isChrome)
                 return;
             var dialogName = args.get_commandName();
             var dialogWin = editor.get_dialogOpener()._dialogContainers[dialogName];
             if (dialogWin) {
                 var cellEl = dialogWin.get_contentElement() || dialogWin.ui.contentCell || dialogWin.ui.content,
                 frame = dialogWin.get_contentFrame();
                 frame.onload = function () {
                     cellEl.style.cssText = "";
                     dialogWin.autoSize();
                 }
             }
         }
    </script>
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <asp:HiddenField ID="ZIM" runat="server" />
    <asp:HiddenField ID="XRI" runat="server" />
    
    <asp:FormView ID="FormView1" runat="server" DataSourceID="tbDigiSignSqlDataSource"
        DefaultMode="Edit" Width="100%">
        <EditItemTemplate>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="ZML_BoxContenuti" runat="server">Dati testuali</asp:Label>
                </div>
                <table>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label3" runat="server" SkinID="FieldDescription" Text="Label">Identificativo *</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:TextBox ID="IdentificativoTextArea1" runat="server" Text='<%# Bind("Identificativo") %>'
                                Columns="100" MaxLength="100"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Obbligatorio"
                                runat="server" ControlToValidate="IdentificativoTextArea1" SkinID="ZSSM_Validazione01"
                                Display="Dynamic">
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <asp:Panel ID="ZMCF_Link" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_Link" runat="server" SkinID="FieldDescription" Text="Label">Link</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="LinkTextBox" runat="server" Text='<%# Bind("Link") %>' Columns="100"
                                    MaxLength="200"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="Url_Validator" runat="server" ControlToValidate="LinkTextBox"
                                    Display="Dynamic" ErrorMessage="Inserire un percorso completo e corretto" ValidationExpression="^(ht|f)tp(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&%\$#_=]*)?$"
                                    SkinID="ZSSM_Validazione01">Inserire un percorso completo e corretto</asp:RegularExpressionValidator>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_Contenuto1" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_Contenuto1" runat="server" SkinID="FieldDescription" Text="Label">Titolo *</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="Contenuto1TextBox" runat="server" Text='<%# Bind("Contenuto1") %>'
                                    Columns="100" MaxLength="500"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="TitoloPaginaRequiredFieldValidator" ErrorMessage="Obbligatorio"
                                    runat="server" ControlToValidate="Contenuto1TextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_Contenuto2" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_Contenuto2" runat="server" SkinID="FieldDescription" Text="Label">Descrizione *</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <CustomWebControls:TextArea ID="Contenuto2TextBox" runat="server" Text='<%# Bind("Contenuto2") %>'
                                    Width="500" Height="200" MaxLength="1000" TextMode="MultiLine"></CustomWebControls:TextArea>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Obbligatorio"
                                    runat="server" ControlToValidate="Contenuto2TextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_Contenuto3" runat="server" Visible="false">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_Contenuto3" runat="server" SkinID="FieldDescription" Text="Label">Terzo contenuto</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <telerik:RadEditor
                                    Language="it-IT" ID="RadEditor1" runat="server"
                                    DocumentManager-DeletePaths="~/ZeusInc/DigiSign/Documents"
                                    DocumentManager-SearchPatterns="*.*"
                                    DocumentManager-ViewPaths="~/ZeusInc/DigiSign/Documents"
                                    DocumentManager-MaxUploadFileSize="52428800"
                                    DocumentManager-UploadPaths="~/ZeusInc/DigiSign/Documents"
                                    FlashManager-DeletePaths="~/ZeusInc/DigiSign/Media"
                                    FlashManager-MaxUploadFileSize="10240000"
                                    FlashManager-ViewPaths="~/ZeusInc/DigiSign/Media"
                                    FlashManager-UploadPaths="~/ZeusInc/DigiSign/Media"
                                    ImageManager-DeletePaths="~/ZeusInc/DigiSign/Images"
                                    ImageManager-ViewPaths="~/ZeusInc/DigiSign/Images"
                                    ImageManager-MaxUploadFileSize="10240000"
                                    ImageManager-SearchPatterns="*.gif, *.png, *.jpg, *.jpe, *.jpeg"
                                    ImageManager-UploadPaths="~/ZeusInc/DigiSign/Images"
                                    ImageManager-ViewMode="Grid"
                                    MediaManager-DeletePaths="~/ZeusInc/DigiSign/Media"
                                    MediaManager-MaxUploadFileSize="10240000"
                                    MediaManager-SearchPatterns="*.asf, *.asx, *.wma, *.wmv, *.avi, *.wav, *.mpeg, *.mpg, *.mpe, *.mov, *.mp3, *.m3u, *.mid, *.midi, *.rm, *.rma"
                                    MediaManager-ViewPaths="~/ZeusInc/DigiSign/Media"
                                    MediaManager-UploadPaths="~/ZeusInc/DigiSign/Media"
                                    TemplateManager-SearchPatterns="*.html,*.htm"
                                    ContentAreaMode="iframe"
                                    OnClientCommandExecuted="editorCommandExecuted"
                                    OnClientModeChange="OnClientModeChange"
                                    Content='<%# Bind("Contenuto3") %>'
                                    ToolsFile="~/Zeus/DigiSign/RadEditor1.xml"
                                    LocalizationPath="~/App_GlobalResources"
                                    AllowScripts="true" RenderMode="Classic" ToolbarMode="Default" EnableViewState="False"
                                    Width="700px" Height="500px">
                                    <CssFiles>
                                        <telerik:EditorCssFile Value="~/asset/css/ZeusTypeFoundry.css" />
                                    </CssFiles>
                                </telerik:RadEditor>
                            </td>
                        </tr>
                    </asp:Panel>
                </table>
            </div>
                   <dlc:ImageRaider ID="ImageRaider1" runat="server" ZeusIdModuloIndice="1" ZeusLangCode="ITA"
                BindPzFromDB="true" IsEditMode="true" DefaultEditBetaImage='<%# Eval("Immagine1") %>'
                DefaultEditGammaImage='<%# Eval("Immagine2") %>' 
                       ImageAlt='<%# Eval("Immagine12Alt") %>'  OnDataBinding="ImageRaider_DataBinding" />

<%--            <dlc:ImageRaider ID="ImageRaider1" runat="server" ZeusIdModuloIndice="1" ZeusLangCode="ITA"
                BindPzFromDB="true" DefaultEditBetaImage='<%# Eval("Immagine1") %>' IsEditMode="true"
                DefaultEditGammaImage='<%# Eval("Immagine2") %>' OnDataBinding="ImageRaider_DataBinding" />--%>

            <asp:Panel ID="ZMCD_Mode" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="Planner" runat="server" Text="Planner"></asp:Label>
                    </div>
                    <table>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="PZL_PWArea1Label" SkinID="FieldDescription" runat="server"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:CheckBox ID="PW_Area1CheckBox" runat="server" Checked='<%# Bind("PW_Area1") %>' />
                                <asp:Label ID="AttivaDa_Label" runat="server" Text="Attiva pubblicazione dalla data"
                                    SkinID="FieldValue"></asp:Label>
                                <asp:TextBox ID="PWI_Area1TextBox" runat="server" Text='<%# Bind("PWI_Area1", "{0:d}") %>'
                                    Columns="10" MaxLength="10" ></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="PWI_Area1TextBox"
                                    SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="*" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$">Formato data richiesto: GG/MM/AAAA</asp:RegularExpressionValidator>
                                <asp:Label ID="AttivaA_Label" runat="server" Text="alla data" SkinID="FieldValue"></asp:Label>
                                <asp:TextBox ID="PWF_Area1TextBox" runat="server" Text='<%# Bind("PWF_Area1", "{0:d}") %>'
                                    Columns="10" MaxLength="10" ></asp:TextBox>
                                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="PWI_Area1TextBox"
                                    ControlToValidate="PWF_Area1TextBox" SkinID="ZSSM_Validazione01" Display="Dynamic"
                                    ErrorMessage="La data iniziale deve essere precedente o uguale alla data finale"
                                    Operator="GreaterThanEqual" Type="Date"></asp:CompareValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label1" runat="server" SkinID="FieldDescription" Text="Label">Ordinamento (1 - 99) *</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="OrdinamentoTextBox" runat="server" Text='<%# Bind("Ordinamento") %>'
                                    MaxLength="2" Columns="1"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="OrdinamentoRequiredFieldValidator" ErrorMessage="Obbligatorio"
                                    runat="server" ControlToValidate="OrdinamentoTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic">
                                </asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="RangeValidator1" ControlToValidate="OrdinamentoTextBox" MinimumValue="1"
                                    MaximumValue="99" Type="Integer" ErrorMessage="Inserire un valore compreso tra 1 e 99"
                                    SkinID="ZSSM_Validazione01" runat="server" Display="Dynamic" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <asp:HiddenField ID="FileNameBetaHiddenField" runat="server" Value='<%# Bind("Immagine1") %>' />
            <asp:HiddenField ID="FileNameGammaHiddenField" runat="server" Value='<%# Bind("Immagine2") %>' />
            <asp:HiddenField ID="Immagine12AltHiddenField" runat="server" Value='<%# Bind("Immagine12Alt") %>' />
            <asp:CustomValidator ID="FileUploadCustomValidator" runat="server" OnServerValidate="FileUploadCustomValidator_ServerValidate"
                Display="Dynamic" SkinID="ZSSM_Validazione01"></asp:CustomValidator>
            <asp:HiddenField ID="RecordEdtUserHidden" runat="server" Value='<%# Bind("RecordEdtUser", "{0}") %>'
                OnDataBinding="UtenteCreazione" />
            <asp:HiddenField ID="RecordEdtDateHidden" runat="server" Value='<%# Bind("RecordEdtDate", "{0}") %>'
                OnDataBinding="DataOggi" />
            <div align="center">
                <dlc:mySummaryValidation ID="mySummaryValidation1" runat="server" SkinID="ZSSM_Validazione01" />
                <asp:LinkButton ID="InsertButton" runat="server" SkinID="ZSSM_Button01" CausesValidation="True"
                    CommandName="Insert" Text="Salva dati" OnClick="Save_File_Upload"></asp:LinkButton>
            </div>
            <asp:HiddenField ID="zdtRecordNewUser" runat="server" Value='<%# Eval("RecordNewUser") %>' />
            <asp:HiddenField ID="zdtRecordNewDate" runat="server" Value='<%# Eval("RecordNewDate") %>' />
            <asp:HiddenField ID="zdtRecordEdtUser" runat="server" Value='<%# Eval("RecordEdtUser") %>' />
            <asp:HiddenField ID="zdtRecordEdtDate" runat="server" Value='<%# Eval("RecordEdtDate") %>' />
        </EditItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="tbDigiSignSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand=" SELECT ID1DigiSign,
                            Identificativo, 
                            ZeusIdModulo, 
                            ZeusLangCode, 
                            Contenuto1, 
                            Contenuto2, 
                            Contenuto3,         
                            Immagine1, 
                            Immagine2, 
                            Immagine12Alt, 
                            RecordNewUser,
                            RecordNewDate,
                            RecordEdtUser, 
                            RecordEdtDate,
                            Link,
                            PW_Area1, 
                            PWI_Area1, 
                            PWF_Area1,
                            Ordinamento 
                        FROM tbDigiSign
                        WHERE ID1DigiSign = @ID1DigiSign"
        UpdateCommand="SET DATEFORMAT dmy; 
                        UPDATE tbDigiSign 
                        SET 
                            Identificativo=@Identificativo, 
                            Contenuto1 = @Contenuto1,
                            Contenuto2 = @Contenuto2, 
                            Contenuto3 = @Contenuto3, 
                            Immagine1 = @Immagine1, 
                            Immagine2 = @Immagine2, 
                            Immagine12Alt = @Immagine12Alt, 
                            RecordEdtUser = @RecordEdtUser, 
                            RecordEdtDate = @RecordEdtDate,
                            Link=@Link,
                            PW_Area1 = @PW_Area1, 
                            PWI_Area1 = @PWI_Area1, 
                            PWF_Area1 = @PWF_Area1,
                            Ordinamento = @Ordinamento  
                        WHERE ID1DigiSign = @ID1DigiSign"
        OnSelected="tbDigiSignSqlDataSource_Selected"
        OnUpdated="tbDigiSignSqlDataSource_Updated">
        <SelectParameters>
            <asp:QueryStringParameter Name="ID1DigiSign" QueryStringField="XRI" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Contenuto1" Type="String" />
            <asp:Parameter Name="Contenuto2" Type="String" />
            <asp:Parameter Name="Contenuto3" Type="String" />
            <asp:Parameter Name="Immagine1" Type="String" />
            <asp:Parameter Name="Immagine2" Type="String" />
            <asp:Parameter Name="Immagine12Alt" Type="String" />
            <asp:Parameter Name="Identificativo" Type="String" />
            <asp:Parameter Name="Link" Type="String" />
            <asp:Parameter Name="PW_Area1" Type="Boolean" />
            <asp:Parameter Name="PWI_Area1" Type="DateTime" />
            <asp:Parameter Name="PWF_Area1" Type="DateTime" />
            <asp:Parameter Name="Ordinamento" Type="Int32" />
            <asp:Parameter Name="RecordEdtUser" />
            <asp:Parameter Name="RecordEdtDate" Type="DateTime" />
            <asp:QueryStringParameter Name="ID1DigiSign" QueryStringField="XRI" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <div style="padding: 10px 0 0 0; margin: 0;">
        <dlc:zeusdatatracking ID="Zeusdatatracking1" runat="server" />
    </div>
</asp:Content>
