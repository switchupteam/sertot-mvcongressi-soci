﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="Planner_Src.aspx.cs"
    Inherits="DigiSign_Planner_Src"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Xml" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <asp:HiddenField ID="ZIM" runat="server" />
    <div class="BlockBox">
        <div class="BlockBoxHeader">
            <asp:Label ID="Ricerca_Label" runat="server" Text="Ricerca"></asp:Label>
        </div>
        <table>
            <tr>
                <td class="BlockBoxDescription">
                    <asp:Label ID="Titolo_Label" SkinID="FieldDescription" runat="server" Text="Identificativo"></asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:DropDownList ID="ID2DigiSignDropDownList" runat="server" DataSourceID="tbDigiSignSqlDataSource"
                        DataTextField="Identificativo" DataValueField="ID1DigiSign"
                        AppendDataBoundItems="True">
                        <asp:ListItem Selected="True" Value="0">> Tutti</asp:ListItem>
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="tbDigiSignSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                        SelectCommand="SELECT [ID1DigiSign],[Identificativo]
                    FROM [tbDigiSign] 
                    WHERE [ZeusIsAlive]=1 
                    AND ZeusLangCode=@ZeusLangCode AND ZeusIdModulo=@ZeusIdModulo ORDER BY Identificativo">
                        <SelectParameters>
                            <asp:QueryStringParameter Name="ZeusLangCode" QueryStringField="Lang" />
                            <asp:QueryStringParameter Name="ZeusIdModulo" QueryStringField="ZIM" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td class="BlockBoxDescription">
                    <asp:Label ID="PZL_Categoria1Label" SkinID="FieldDescription" runat="server" Text="Attivo"></asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:DropDownList ID="AttivoDropDownList1" runat="server" D>
                        <asp:ListItem Selected="True" Value="_">> Tutti</asp:ListItem>
                        <asp:ListItem Value="1">Attivi</asp:ListItem>
                        <asp:ListItem Value="0">Non attivi</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="BlockBoxDescription"></td>
                <td class="BlockBoxValue">
                    <asp:LinkButton SkinID="ZSSM_Button01" ID="SearchButton" runat="server" Text="Cerca"
                        OnClick="SearchButton_Click"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
