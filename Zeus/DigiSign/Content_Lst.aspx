﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="Content_Lst.aspx.cs"
    Inherits="DigiSign_Content_Lst"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Xml" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <asp:HiddenField ID="ZIM" runat="server" />
    <asp:Panel ID="SingleRecordPanel" runat="server" Visible="false">
        <div class="LayGridTitolo1">
            <asp:Label ID="ZML_TitoloGridView_Conferma" SkinID="GridTitolo1" runat="server" Text="Ultimo contenuto creato / aggiornato"></asp:Label>
        </div>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
            AutoGenerateColumns="False" DataSourceID="SingleRecordSqlDatasource" PageSize="50"
            OnRowDataBound="GridView1_RowDataBound">
            <Columns>
                <asp:BoundField DataField="Identificativo" HeaderText="Identificativo" SortExpression="Identificativo" />
                <asp:BoundField DataField="Contenuto1" HeaderText="Titolo" SortExpression="Contenuto1" />
                <asp:BoundField DataField="Ordinamento" HeaderText="Ordinamento" SortExpression="Ordinamento"
                    ItemStyle-HorizontalAlign="Right" />
                <asp:TemplateField SortExpression="PW_Area1" HeaderText="Attivo">
                    <ItemTemplate>
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Pub." SortExpression="PW_Area1">
                    <ItemTemplate>
                        <asp:Image ID="FlagImage" runat="server" ToolTip='<%# Eval("PW_Area1") %>' ImageUrl="~/Zeus/SiteImg/Ico1_Flag_Off.gif" />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:CheckBox ID="PW_Area1CheckBox" runat="server" Checked='<%# Bind("PW_Area1") %>' />
                    </EditItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>
                <asp:BoundField DataField="PWI_Area1" HeaderText="Inizio" SortExpression="PWI_Area1"
                    ItemStyle-HorizontalAlign="Center" DataFormatString="{0:d}" />
                <asp:BoundField DataField="PWF_Area1" HeaderText="Fine" SortExpression="PWF_Area1"
                    ItemStyle-HorizontalAlign="Center" DataFormatString="{0:d}" />
                <asp:HyperLinkField DataNavigateUrlFields="ID1DigiSign,ZeusLangCode,ZeusIdModulo"
                    HeaderText="Dettaglio" Text="Dettaglio" DataNavigateUrlFormatString="Content_Dtl.aspx?XRI={0}&Lang={1}&ZIM={2}">
                    <ControlStyle CssClass="GridView_ZeusButton1" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:HyperLinkField>
                <asp:HyperLinkField DataNavigateUrlFields="ID1DigiSign,ZeusLangCode,ZeusIdModulo"
                    HeaderText="Modifica" Text="Modifica" DataNavigateUrlFormatString="Content_Edt.aspx?XRI={0}&Lang={1}&ZIM={2}">
                    <ControlStyle CssClass="GridView_ZeusButton1" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:HyperLinkField>
                <asp:TemplateField HeaderText="PW_Area1" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="PW_Area1" runat="server" Text='<%# Eval("PW_Area1") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SingleRecordSqlDatasource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
            SelectCommand="SELECT ID1DigiSign,
                                Identificativo,
                                Contenuto1,
                                ZeusLangCode,
                                ZeusIdModulo,
                                RecordNewDate,
                                PW_Area1, 
                                PWI_Area1, 
                                PWF_Area1,
                                Ordinamento           
                            FROM [tbDigiSign] 
                            WHERE ID1DigiSign=@ID1DigiSign">
            <SelectParameters>
                <asp:QueryStringParameter Name="ID1DigiSign" QueryStringField="XRI" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <div class="VertSpacerMedium">
        </div>
    </asp:Panel>
    <div class="LayGridTitolo1">
        <asp:Label ID="ZML_TitoloGridView" runat="server" SkinID="GridTitolo1" Text="Elenco contenuti disponibili"></asp:Label>
    </div>
    <asp:GridView ID="GridView2" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" DataSourceID="tbDigiSignSqlDataSource" PageSize="50"
        OnRowDataBound="GridView1_RowDataBound">
        <Columns>
            <asp:BoundField DataField="Identificativo" HeaderText="Identificativo" SortExpression="Identificativo" />
            <asp:BoundField DataField="Contenuto1" HeaderText="Titolo" SortExpression="Contenuto1" />
            <asp:BoundField DataField="Ordinamento" HeaderText="Ordinamento" SortExpression="Ordinamento"
                ItemStyle-HorizontalAlign="Right" />
            <asp:TemplateField SortExpression="PW_Area1" HeaderText="Attivo">
                <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Pubblicato" SortExpression="PW_Area1">
                <ItemTemplate>
                    <asp:Image ID="FlagImage" runat="server" ToolTip='<%# Eval("PW_Area1") %>' ImageUrl="~/Zeus/SiteImg/Ico1_Flag_Off.gif" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:CheckBox ID="PW_Area1CheckBox" runat="server" Checked='<%# Bind("PW_Area1") %>' />
                </EditItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:BoundField DataField="PWI_Area1" HeaderText="Inizio" SortExpression="PWI_Area1"
                ItemStyle-HorizontalAlign="Center" DataFormatString="{0:d}" />
            <asp:BoundField DataField="PWF_Area1" HeaderText="Fine" SortExpression="PWF_Area1"
                ItemStyle-HorizontalAlign="Center" DataFormatString="{0:d}" />
            <asp:HyperLinkField DataNavigateUrlFields="ID1DigiSign,ZeusLangCode,ZeusIdModulo"
                HeaderText="Dettaglio" Text="Dettaglio" DataNavigateUrlFormatString="Content_Dtl.aspx?XRI={0}&Lang={1}&ZIM={2}">
                <ControlStyle CssClass="GridView_ZeusButton1" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:HyperLinkField>
            <asp:HyperLinkField DataNavigateUrlFields="ID1DigiSign,ZeusLangCode,ZeusIdModulo"
                HeaderText="Modifica" Text="Modifica" DataNavigateUrlFormatString="Content_Edt.aspx?XRI={0}&Lang={1}&ZIM={2}">
                <ControlStyle CssClass="GridView_ZeusButton1" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:HyperLinkField>
            <asp:TemplateField HeaderText="PW_Area1" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="PW_Area1" runat="server" Text='<%# Eval("PW_Area1") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            Dati non presenti
            <br />
            <br />
            <br />
            <br />
            <br />
        </EmptyDataTemplate>
        <EmptyDataRowStyle BackColor="White" Width="500px" BorderColor="Red" BorderWidth="0px"
            Height="200px" />
    </asp:GridView>
    <asp:SqlDataSource ID="tbDigiSignSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>">
        <SelectParameters>
            <asp:QueryStringParameter Name="ZeusLangCode" QueryStringField="Lang" />
            <asp:QueryStringParameter Name="ZeusIdModulo" QueryStringField="ZIM" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
