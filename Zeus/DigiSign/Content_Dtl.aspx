﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="Content_Dtl.aspx.cs"
    Inherits="DigiSign_Content_Dtl"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Xml" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <link href="../../asset/css/ZeusTypeFoundry.css" rel="stylesheet" type="text/css" />
    <link href="../../SiteCss/ZeusSnippets.css" rel="stylesheet" type="text/css" />
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <asp:HiddenField ID="ZIM" runat="server" />
    <asp:HiddenField ID="XRI" runat="server" />
    <asp:FormView ID="FormView1" runat="server" DataSourceID="tbDigiSignSqlDataSource"
        DefaultMode="ReadOnly" Width="100%">
        <ItemTemplate>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="ZML_BoxContenuti" runat="server">Dati testuali</asp:Label></div>
                <table>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label3" runat="server" SkinID="FieldDescription" Text="Label">Identificativo </asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:Label ID="IdentificativoTextArea1" runat="server" Text='<%# Eval("Identificativo") %>'
                                SkinID="FieldValue"></asp:Label>
                        </td>
                    </tr>
                     <asp:Panel ID="ZMCF_Link" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_Link" runat="server" SkinID="FieldDescription" Text="Label">Link</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:Label ID="LinkTextBox" runat="server" Text='<%# Eval("Link") %>' 
                                    SkinID="FieldValue"></asp:Label>
                                
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_Contenuto1" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_Contenuto1" runat="server" SkinID="FieldDescription" Text="Label">Titolo </asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:Label ID="Contenuto1TextBox" runat="server" Text='<%# Eval("Contenuto1") %>'
                                    SkinID="FieldValue"></asp:Label>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_Contenuto2" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_Contenuto2" runat="server" SkinID="FieldDescription" Text="Label">Descrizione </asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:Label ID="Label4" runat="server" Text='<%# Eval("Contenuto2") %>' SkinID="FieldValue"></asp:Label>
                            </td>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_Contenuto3" runat="server" Visible="false">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_Contenuto3" runat="server" SkinID="FieldDescription" Text="Label">Terzo contenuto</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:Label ID="Label5" runat="server" Text='<%# Eval("Contenuto3") %>' SkinID="FieldValue"></asp:Label>
                            </td>
                            </td>
                        </tr>
                    </asp:Panel>
                </table>
            </div>
            <dlc:ImageRaider ID="ImageRaider1" runat="server" ZeusIdModuloIndice="1" ZeusLangCode="ITA"
                BindPzFromDB="true" IsEditMode="true" Enabled="false" DefaultEditBetaImage='<%# Eval("Immagine1") %>'
                DefaultEditGammaImage='<%# Eval("Immagine2") %>' ImageAlt='<%# Eval("Immagine12Alt") %>' OnDataBinding="ImageRaider_DataEvaling" />
            <div align="center">
                <asp:LinkButton ID="ModificaButton" SkinID="ZSSM_Button01" runat="server" Text="Modifica"
                    OnClick="ModificaButton_Click" />
                <asp:LinkButton ID="DeleteLinkButton" runat="server" Text="Elimina" SkinID="ZSSM_Button01"
                    OnClick="DeleteLinkButton_Click"></asp:LinkButton>
            </div>
            <asp:HiddenField ID="zdtRecordNewUser" runat="server" Value='<%# Eval("RecordNewUser") %>' />
            <asp:HiddenField ID="zdtRecordNewDate" runat="server" Value='<%# Eval("RecordNewDate") %>' />
            <asp:HiddenField ID="zdtRecordEdtUser" runat="server" Value='<%# Eval("RecordEdtUser") %>' />
            <asp:HiddenField ID="zdtRecordEdtDate" runat="server" Value='<%# Eval("RecordEdtDate") %>' />
        </ItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="tbDigiSignSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand=" SELECT [ID1DigiSign],Identificativo, [ZeusIdModulo], 
        [ZeusLangCode], [Contenuto1], [Contenuto2], [Contenuto3],
         [Immagine1], [Immagine2], [Immagine12Alt], [RecordNewUser],
          [RecordNewDate],[RecordEdtUser], [RecordEdtDate],Link 
           FROM [tbDigiSign]
            WHERE [ID1DigiSign] = @ID1DigiSign" OnSelected="tbDigiSignSqlDataSource_Selected">
        <SelectParameters>
            <asp:QueryStringParameter Name="ID1DigiSign" QueryStringField="XRI" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <div style="padding: 10px 0 0 0; margin: 0;">
        <dlc:zeusdatatracking ID="Zeusdatatracking1" runat="server" />
    </div>
</asp:Content>
