<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" %>
    
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data" %>

<script runat="server">
    
    delinea myDelinea = new delinea();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Msg"))
            XmlDataSource1.XPath = "SystemMessage/Tipologia/Messaggio[@id='" + Request.QueryString["Msg"] + "']";
        else XmlDataSource1.XPath = "SystemMessage/Tipologia/Messaggio[@id='0000000000000000']";
    }//fine Page_Load

    protected void Image_DataBinding(object sender, EventArgs e)
    {
        Image image = (Image)sender;
        if (image.ImageUrl.Length == 0)
            image.Visible = false;

    }

    protected void HyperLink_DataBinding(object sender, EventArgs e)
    {
        HyperLink link = (HyperLink)sender;
        if (link.NavigateUrl.Length == 0)
            link.Visible = false;
    }

    protected void BackHyperLink_DataBinding(object sender, EventArgs e)
    {
        HyperLink link = (HyperLink)sender;
        Panel BackPanel = (Panel)FormView1.FindControl("BackPanel");
        if (link.Text.Length > 0)
        {

            link.NavigateUrl = "javascript:history.back()";
        }
        else
            BackPanel.Visible = false;
    }


    protected void StaticHyperLink_DataBinding(object sender, EventArgs e)
    {
        HyperLink link = (HyperLink)sender;
        Panel StaticPanel1 = (Panel)FormView1.FindControl("StaticPanel1");

        if (link.Text.Length > 0)
        {
            if (link.NavigateUrl.Length == 0)
                link.Visible = false;
        }
        else
            StaticPanel1.Visible = false;

    }

    protected void StaticHyperLink2_DataBinding(object sender, EventArgs e)
    {
        HyperLink link = (HyperLink)sender;
        Panel StaticPanel2 = (Panel)FormView1.FindControl("StaticPanel2");

        if (link.Text.Length > 0)
        {
            if (link.NavigateUrl.Length == 0)
                link.Visible = false;
        }
        else
            StaticPanel2.Visible = false;

    }

    protected void DynPrevHyperLink_DataBinding(object sender, EventArgs e)
    {
        try
        {

            HyperLink link = (HyperLink)sender;
            Panel DynPrevPanel1 = (Panel)FormView1.FindControl("DynPrevPanel1");
            bool visible = false;
            if ((Request.ServerVariables["HTTP_REFERER"] != null) && (Request.ServerVariables["HTTP_REFERER"].Length > 0) && (link.Text.Length > 0))
            {
                string BackUrl = Request.ServerVariables["HTTP_REFERER"].ToString();
                int start = BackUrl.IndexOf("?");
                if (start > 0)
                {
                    string myQueryString = BackUrl.Substring(start, (BackUrl.Length - start));
                    link.NavigateUrl += myQueryString;
                    visible = true;
                }
            }

            DynPrevPanel1.Visible = visible;
        }
        catch (Exception p)
        {
            //Response.Write(p.ToString());
        }
    }


    protected void DynPrevHyperLink2_DataBinding(object sender, EventArgs e)
    {
        try
        {

            HyperLink link = (HyperLink)sender;
            Panel DynPrevPanel2 = (Panel)FormView1.FindControl("DynPrevPanel2");
            bool visible = false;
            if ((Request.ServerVariables["HTTP_REFERER"] != null) && (Request.ServerVariables["HTTP_REFERER"].Length > 0) && (link.Text.Length > 0))
            {
                string BackUrl = Request.ServerVariables["HTTP_REFERER"].ToString();
                int start = BackUrl.IndexOf("?");
                if (start > 0)
                {
                    string myQueryString = BackUrl.Substring(start, (BackUrl.Length - start));
                    link.NavigateUrl += myQueryString;
                    visible = true;
                }
            }

            DynPrevPanel2.Visible = visible;
        }
        catch (Exception p)
        {
            //Response.Write(p.ToString());
        }
    }


    protected void DynThisHyperLink_DataBinding(object sender, EventArgs e)
    {
        HyperLink link = (HyperLink)sender;
        Panel DynThisPanel1 = (Panel)FormView1.FindControl("DynThisPanel1");

        if (link.Text.Length > 0)
        {
            if (Request.QueryString != null)
            {
                link.NavigateUrl += "?" + Request.QueryString;
            }
        }
        else
            DynThisPanel1.Visible = false;

    }

    protected void DynThisHyperLink2_DataBinding(object sender, EventArgs e)
    {
        HyperLink link = (HyperLink)sender;
        Panel DynThisPanel2 = (Panel)FormView1.FindControl("DynThisPanel2");

        if (link.Text.Length > 0)
        {
            if (Request.QueryString != null)
            {
                link.NavigateUrl += "?" + Request.QueryString;
            }
        }
        else
            DynThisPanel2.Visible = false;
    }


    protected void FormView1_DataBound(object sender, EventArgs e)
    {
        try
        {
            
            Label MessaggioLabel = (Label)FormView1.FindControl("MessaggioLabel");
            Label LabelTitolo = (Label)myDelinea.FindControlRecursive(Page, "LabelTitolo");
            LabelTitolo.Text = MessaggioLabel.Text;
            Page.Header.Title = "MV Congressi Platform - " + MessaggioLabel.Text;
        }
        catch (Exception p) 
        {
            Response.Write(p.ToString());
        }
   
    }//fine FormView1_DataBound
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
<asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <asp:XmlDataSource ID="XmlDataSource1" runat="server" DataFile="~/App_Data/SystemMessageZeus.xml">
    </asp:XmlDataSource>
    <asp:FormView ID="FormView1" runat="server" DataSourceID="XmlDataSource1" DefaultMode="ReadOnly" OnDataBound="FormView1_DataBound" CellPadding="0">
        <ItemTemplate>
            <table  width="100%" border="0" cellspacing="5" cellpadding="0">
                <tr>
                    <td width="100%">
                        <asp:Label  SkinID="SysMsgDesc" ID="MessaggioLabel" runat="server" Text='<%# XPath("Titolo") %>' Visible="false"></asp:Label>
                       
                        </td>
                </tr>
                <tr>
                    <td >
                        <asp:Label SkinID="SysMsgDesc" ID="Label1" runat="server" Text='<%# XPath("Testo", "{0}") %>'></asp:Label></td>
                </tr>
            </table>
            <table width="100%" border="0" cellspacing="3" cellpadding="0">
                <asp:Panel ID="BackPanel" runat="server">
                    <tr>
                        <td width="50" valign="middle">
                            <asp:Image ID="BackImage" runat="server" ImageUrl='<%# XPath("LinkBackIco", "~/Zeus/SiteImg/{0}") %>'
                                OnDataBinding="Image_DataBinding" />
                        <td width="100%" valign="middle">
                            <asp:HyperLink ID="BackHyperLink" SkinID="SysMsgOption" runat="server" Text=' <%# XPath("LinkBackDesc", "{0}") %> '
                                OnDataBinding="BackHyperLink_DataBinding"></asp:HyperLink>
                </asp:Panel>
                <asp:Panel ID="StaticPanel1" runat="server">
                    <tr>
                        <td width="50" valign="middle">
                            <asp:Image ID="StaticImage1" runat="server" ImageUrl='<%# XPath("LinkStatic1Ico", "~/Zeus/SiteImg/{0}") %>'
                                OnDataBinding="Image_DataBinding" />
                        <td width="100%" valign="middle">
                            <asp:HyperLink ID="StaticHyperLink1" SkinID="SysMsgOption" runat="server" NavigateUrl='<%# XPath("LinkStatic1Url") %>'
                                Text=' <%# XPath("LinkStatic1Desc", "{0}") %> ' OnDataBinding="StaticHyperLink_DataBinding"></asp:HyperLink>
                </asp:Panel>
                <asp:Panel ID="StaticPanel2" runat="server">
                    <tr>
                        <td width="50" valign="middle">
                            <asp:Image ID="StaticImage2" runat="server" ImageUrl='<%# XPath("LinkStatic2Ico", "~/Zeus/SiteImg/{0}") %>'
                                OnDataBinding="Image_DataBinding" />
                        <td width="100%" valign="middle">
                            <asp:HyperLink ID="StaticHyperLink2" SkinID="SysMsgOption" runat="server" NavigateUrl='<%# XPath("LinkStatic2Url") %>'
                                Text=' <%# XPath("LinkStatic2Desc", "{0}") %> ' OnDataBinding="StaticHyperLink2_DataBinding"></asp:HyperLink>
                </asp:Panel>
                <asp:Panel ID="DynPrevPanel1" runat="server">
                    <tr>
                        <td width="50" valign="middle">
                            <asp:Image ID="DynPrevImage1" runat="server" ImageUrl='<%# XPath("LinkDynPrev1Ico", "~/Zeus/SiteImg/{0}") %>'
                                OnDataBinding="Image_DataBinding" />
                        <td width="100%" valign="middle">
                            <asp:HyperLink ID="DynPrevHyperLink1" SkinID="SysMsgOption" runat="server" NavigateUrl='<%# XPath("LinkDynPrev1Url", "{0}") %>'
                                Text=' <%# XPath("LinkDynPrev1Desc", "{0}") %> ' OnDataBinding="DynPrevHyperLink_DataBinding"></asp:HyperLink>
                </asp:Panel>
                <asp:Panel ID="DynPrevPanel2" runat="server">
                    <tr>
                        <td width="50" valign="middle">
                            <asp:Image ID="DynPrevImage2" runat="server" ImageUrl='<%# XPath("LinkDynPrev2Ico", "~/Zeus/SiteImg/{0}") %>'
                                OnDataBinding="Image_DataBinding" />
                        <td width="100%" valign="middle">
                            <asp:HyperLink ID="DynPrevHyperLink2" SkinID="SysMsgOption" runat="server" NavigateUrl='<%# XPath("LinkDynPrev2Url", "{0}") %>'
                                Text=' <%# XPath("LinkDynPrev2Desc", "{0}") %> ' OnDataBinding="DynPrevHyperLink2_DataBinding"></asp:HyperLink>
                </asp:Panel>
                <asp:Panel ID="DynThisPanel1" runat="server">
                    <tr>
                        <td width="50" valign="middle">
                            <asp:Image ID="DynThisImage1" runat="server" ImageUrl='<%# XPath("LinkDynThis1Ico", "~/Zeus/SiteImg/{0}") %>'
                                OnDataBinding="Image_DataBinding" />
                        <td width="100%" valign="middle">
                            <asp:HyperLink ID="DynThisHyperLink1" runat="server" NavigateUrl='<%# XPath("LinkDynThis1Url", "{0}") %>'
                                Text=' <%# XPath("LinkDynThis1Desc", "{0}") %> ' OnDataBinding="DynThisHyperLink_DataBinding"></asp:HyperLink>
                </asp:Panel>
                <asp:Panel ID="DynThisPanel2" runat="server">
                    <tr>
                        <td width="50" valign="middle">
                            <asp:Image ID="DynThisImage2" runat="server" ImageUrl='<%# XPath("LinkDynThis1Ico", "~/Zeus/SiteImg/{0}") %>'
                                OnDataBinding="Image_DataBinding" />
                        <td width="100%" valign="middle">
                            <asp:HyperLink ID="DynThisHyperLink2" SkinID="SysMsgOption" runat="server" NavigateUrl='<%# XPath("LinkDynThis1Url", "{0}") %>'
                                Text=' <%# XPath("LinkDynThis1Desc", "{0}") %> ' OnDataBinding="DynThisHyperLink2_DataBinding"></asp:HyperLink>
                </asp:Panel>
            </table>
        </ItemTemplate>
    </asp:FormView>
</asp:Content>
