﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">
  
    static string TitoloPagina = "Query Analyzer";



    protected void Page_Init()
    {
        string firstScript = "/SiteJS/jquery-1.2.6.js";
        string secondScript = "/SiteJS/jQuery.BlockUI.js";

        HtmlGenericControl Include = new HtmlGenericControl("script");
        Include.Attributes.Add("type", "text/javascript");
        Include.Attributes.Add("src", firstScript);
        this.Page.Header.Controls.Add(Include);

        Include = new HtmlGenericControl("script");
        Include.Attributes.Add("type", "text/javascript");
        Include.Attributes.Add("src", secondScript);
        this.Page.Header.Controls.Add(Include);


        ExecuteButton.Attributes.Add("onfocus", "javascript:return SetIsLoadButtonToTrue();");

    }//fine Page_Init

    protected void Page_Load(object sender, EventArgs e)
    {

        TitleField.Value = TitoloPagina;

    }//fine Page_Load


    private bool ExecuteUserQuery(string sqlQuery)
    {
        
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
       
        
        if(sqlQuery.Length==0)
            return false;
        
        
        try
        {

            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = sqlQuery;

                int RowsAffected = 0;
                
                if (sqlQuery.ToUpper().IndexOf("SELECT") > -1)
                {
                    SelectDataGrid.DataSource= command.ExecuteReader();
                    SelectDataGrid.DataBind();
                    RowsAffected = SelectDataGrid.Items.Count;

                    
                }
                else
                {

                    RowsAffected = command.ExecuteNonQuery();
                    SelectDataGrid.Visible = false;
                }

                FinalLabel.Text = "Query eseguita con successo.<br />Affected rows: " + RowsAffected.ToString();
                
                return true;

            }//fine Using
        }
        catch (Exception p)
        {

            InfoLabel.Text= p.ToString();
            FinalLabel.Text = "ATTENZIONE: Query NON eseguita con successo.";
            return false;
        }


    }//fine ExecuteUserQuery
    

    protected void ExecuteButton_Click(object sender, EventArgs e) 
    {
        ExecuteUserQuery(QueryTextArea.Text);
        QueryTextArea.Visible = ExecuteButton.Visible = false;
        
    }//fine ExecuteButton_Click


</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">

    <script language="javascript">
       
       
       var isLoadButton=false;
       
        window.onbeforeunload = function() {
        
         if((isLoadButton)
         &&(document.getElementById("<%=QueryTextArea.ClientID%>").value.length>0))
         {
            $.blockUI({
                message: '<h1><img src="/SiteImg/wait.gif" /> Esecuzione query in corso...</h1>',
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: '.5',
                    color: '#fff'
                }
            });//fine blockUIz
          }//fine if
        }//fine function
        
        function SetIsLoadButtonToTrue()
        {
          isLoadButton=true;
        }

        
    </script>
	<asp:Label ID="Info" runat="server" SkinID="CorpoTesto">Viste: copiare il codice da ALTER in poi (CREATE NON FUNZIONA IN QUESTA VERSIONE)</asp:Label>
    <asp:HiddenField ID="TitleField" runat="server" />
    <div>
    <CustomWebControls:TextArea ID="QueryTextArea" runat="server" MaxLength="5000" Width="800" Height="200"
     TextMode="MultiLine"></CustomWebControls:TextArea>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Obbligatorio"
        runat="server" ControlToValidate="QueryTextArea" SkinID="ZSSM_Validazione01" Display="Dynamic">
    </asp:RequiredFieldValidator>
    <br />
    <asp:Label ID="InfoLabel" runat="server" SkinID="Validazione01"></asp:Label>
    <asp:Button ID="ExecuteButton" runat="server" Text="Execute query" OnClick="ExecuteButton_Click"
        SkinID="ZSSM_Button01" />
    </div>
    <asp:Label ID="FinalLabel" runat="server" SkinID="CorpoTesto"></asp:Label><br />
    <asp:DataGrid ID="SelectDataGrid" runat="server"></asp:DataGrid>
</asp:Content>
