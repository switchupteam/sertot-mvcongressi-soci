﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<script runat="server">
  
    static string TitoloPagina = "Utility Zeus SiteAdmin";

    protected void Page_Load(object sender, EventArgs e)
    {

        TitleField.Value = TitoloPagina;

        //if ((!Membership.GetUser(true).UserName.ToLower().Equals("webstart")) && (!Membership.GetUser(true).UserName.ToLower().Equals("s.gavardi")) && (!Membership.GetUser(true).UserName.ToLower().Equals("p.bianchi")) && (!Membership.GetUser(true).UserName.ToLower().Equals("s.allegri")))
        //    Response.Redirect("~/Zeus/System/Message.aspx");

    }//fine Page_Load
    
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
      <tr>
          <td valign="top" style="width: 50%">
              <div class="BlockBoxDouble">
                  <div class="BlockBoxHeader">
                      <asp:Label ID="PZL_BoxSede1Label" runat="server">Utility verificate</asp:Label></div>
                  <table width="100%">
                      <tr>
                          <td>
                                <asp:HyperLink ID="HyperLink183" SkinID="FieldValue" runat="server" NavigateUrl="UIDGenerator.aspx">Generatore di UniqueIdentifier</asp:HyperLink>
                                <br />
                                <br />
                                <asp:HyperLink ID="HyperLink5" SkinID="FieldValue" runat="server" NavigateUrl="~/Zeus/SiteAdmin/ImportAccount_Excel_x64.aspx">Importazione account utente da file Excel x64</asp:HyperLink>
                                <br />
                                <br /> 
                                <asp:HyperLink ID="HyperLink5a" SkinID="FieldValue" runat="server" NavigateUrl="~/Zeus/SiteAdmin/FindAndReplaceAll.aspx">Find & Replace in tutti i campi stringa del DB corrente</asp:HyperLink>
                                <br />
                                <br />   
                                <asp:HyperLink ID="HyperLink5b" SkinID="FieldValue" runat="server" NavigateUrl="~/Zeus/SiteAdmin/QueryAnalyzer.aspx">Query Analyzer</asp:HyperLink>
                                <br />
                                <br />
</td>
                      </tr>
                  </table>
              </div>          </td>
          <td valign="top" width="10">
              <img src="../SiteImg/Spc.gif" height="5" width="10" />          </td>
          <td valign="top" style="width: 50%">
              <div class="BlockBoxDouble">
                  <div class="BlockBoxHeader">
                      <asp:Label ID="PZL_BoxSede2Label" runat="server">Utility da verificare e/o obsolete</asp:Label></div>
                  <table width="100%">
                      <tr>
                          <td>
                              <p><br />
                                <asp:HyperLink ID="HyperLink184" SkinID="FieldValue" runat="server" NavigateUrl="ClearHtmlTags.aspx">Ripulizia campi da Tag Html</asp:HyperLink>
                                <br />
                                <asp:HyperLink ID="HyperLink186" SkinID="FieldValue" runat="server" NavigateUrl="CopyFields.aspx">Copia record tra due campi della stessa tabella</asp:HyperLink>
                                <br />
                                <asp:HyperLink ID="HyperLink187" SkinID="FieldValue" runat="server" NavigateUrl="SQLFindReplace.aspx">Find &amp; Replace di porzioni SQL in un campo</asp:HyperLink>
                                <br />
                                <br />
                                <asp:HyperLink ID="HyperLink7" SkinID="FieldValue" runat="server" NavigateUrl="~/Zeus/SiteAdmin/DeleteAccount_Excel.aspx">Eliminazione utenti importati da file Excel</asp:HyperLink>
                                <br />
                                <br />
                                <asp:HyperLink ID="HyperLink8" SkinID="FieldValue" runat="server" NavigateUrl="~/Zeus/SiteAdmin/QueryAnalizer.aspx">Query Analizer</asp:HyperLink>
                                <br />
                                <br />
                                <asp:HyperLink ID="HyperLink20" SkinID="FieldValue" runat="server" NavigateUrl="~/Zeus/Utilities/Export_Excel.aspx">Esporta tabelle e viste su Excel</asp:HyperLink>
                                <br />
                                <br />
                                <asp:HyperLink ID="HyperLink185" SkinID="FieldValue" runat="server" NavigateUrl="UrlRewriteGenerator.aspx">Generatore di UrlRewriting versione 1.0</asp:HyperLink>
                                <br />
                                <asp:HyperLink ID="HyperLink6" SkinID="FieldValue" runat="server" NavigateUrl="AggiornaUrlRewriting.aspx">Aggiornamento UrlRewriting dalla versione 1.0 alla 2.0</asp:HyperLink>
                                <br />
                                <br />
                                <asp:HyperLink ID="HyperLink1811" SkinID="FieldValue" runat="server" NavigateUrl="YafForum.aspx">Configurazione YafForum</asp:HyperLink>
                                <br />
                                <br />
                                <asp:HyperLink ID="HyperLink9" SkinID="FieldValue" runat="server" NavigateUrl="Categorie_Riassocia.aspx">Riassocia categorie</asp:HyperLink>
                                <br />
                                <br />
                                <asp:HyperLink ID="HyperLink17" SkinID="FieldValue" runat="server" NavigateUrl="Roles.aspx">Roles</asp:HyperLink>
</p>
                              <p>&nbsp;</p>
                              <p>&nbsp;</p>
                              <p>&nbsp;</p>
                              <p><strong>
                              <asp:Label ID="Titoletto1" SkinID="FieldValue" runat="server">Funzionalità di sistema</asp:Label>
                              </strong><br />
                                  <asp:HyperLink ID="HyperLink2" SkinID="FieldValue" runat="server" NavigateUrl="PZConfig_ImageRaider.aspx">Image Raider</asp:HyperLink>
                                  <br />
                                  <br />
                                  <strong>
                                      <asp:Label ID="Titoletto2" SkinID="FieldValue" runat="server">Moduli base</asp:Label>
                                  </strong>
                                  <br />
                                  <asp:HyperLink ID="HyperLink1" SkinID="FieldValue" runat="server" NavigateUrl="PZConfig_Categorie.aspx">Gestione Categorie</asp:HyperLink>
                                  <br />
                                  <asp:HyperLink ID="HyperLink18" SkinID="FieldValue" runat="server" NavigateUrl="PZConfig_SiteMenu.aspx">Gestione SiteMenu</asp:HyperLink>
                                  <br />
                                  <br />
                                  <strong>
                                      <asp:Label ID="Titoletto3" SkinID="FieldValue" runat="server">Moduli aggiuntivi</asp:Label>
                                  </strong><br />
                                  <asp:HyperLink ID="HyperLink10" SkinID="FieldValue" runat="server" NavigateUrl="PZConfig_AdFarm.aspx">Ad Farm</asp:HyperLink>
                                  <br />
                                  <asp:HyperLink ID="HyperLink14" SkinID="FieldValue" runat="server" NavigateUrl="PZConfig_Brands.aspx">Brands</asp:HyperLink>
                                  <br />
                                  <asp:HyperLink ID="HyperLink12" SkinID="FieldValue" runat="server" NavigateUrl="PZConfig_BusinessBook.aspx">Business Book</asp:HyperLink>
                                  <br />
                                  <asp:HyperLink ID="HyperLink16" SkinID="FieldValue" runat="server" NavigateUrl="PZConfig_Calendar.aspx">Calendar</asp:HyperLink>
                                  <br />
                                  <asp:HyperLink ID="HyperLink15" SkinID="FieldValue" runat="server" NavigateUrl="PZConfig_Catalogo.aspx">Catalogo</asp:HyperLink>
                                  <br />
                                  categorie<br />
                                  <asp:HyperLink ID="HyperLink554325" SkinID="FieldValue" runat="server" NavigateUrl="~/Zeus/SiteAdmin/PZConfig_Communicator.aspx">Communicator</asp:HyperLink>
                                  <br />
                                  <asp:HyperLink ID="HyperLink54354" SkinID="FieldValue" runat="server" NavigateUrl="PZConfig_Domus.aspx">Domus</asp:HyperLink>
                                  <br />
                                  Forum<br />
                                  Image Raider<br />
                                  <asp:HyperLink ID="HyperLink3" SkinID="FieldValue" runat="server" NavigateUrl="PZConfig_PageDesigner.aspx">Page Designer</asp:HyperLink>
                                  <br />
                                  <asp:HyperLink ID="HyperLink4" SkinID="FieldValue" runat="server" NavigateUrl="PZConfig_PhotoGallery.aspx">Photo Gallery</asp:HyperLink>
                                  <br />
                                  <asp:HyperLink ID="HyperLink11" SkinID="FieldValue" runat="server" NavigateUrl="PZConfig_PressRoom.aspx">Press Room</asp:HyperLink>
                                  <br />
                                  Profili<br />
                                  <asp:HyperLink ID="HyperLink13" SkinID="FieldValue" runat="server" NavigateUrl="PZConfig_Publisher.aspx">Publisher</asp:HyperLink>
                                  <br />
                                  <asp:HyperLink ID="HyperLink523" SkinID="FieldValue" runat="server" NavigateUrl="~/Zeus/SiteAdmin/PZConfig_Remark.aspx">Remark</asp:HyperLink>
                                  <br />
                                  <asp:HyperLink ID="HyperLink335" SkinID="FieldValue" runat="server" NavigateUrl="PZConfig_RetailAccomodation.aspx">Retail & Accomodation</asp:HyperLink>
                                  <br />
                                  RSS Central<br />
                                  <asp:HyperLink ID="HyperLink54664" SkinID="FieldValue" runat="server" NavigateUrl="~/Zeus/SiteAdmin/PZConfig_RSSSeeker.aspx">RSS Seeker</asp:HyperLink>
                                  <br />
                                  Site Menu<br />
                                  Virtual Poll<br />
                              </p></td>
                      </tr>
                  </table>
              </div>
                
              <div class="BlockBoxDouble">
                  <div class="BlockBoxHeader">
                      <asp:Label ID="Label1" runat="server">Tabelle</asp:Label></div>
                  <table width="100%">
                      <tr>
                          <td>
                              <p>
                                <asp:HyperLink ID="HyperLink5466tr4" SkinID="FieldValue" runat="server" NavigateUrl="tbBusinessBook.aspx">tbBusinessBook</asp:HyperLink><br />
                                <asp:HyperLink ID="HyperLink19" SkinID="FieldValue" runat="server" NavigateUrl="tbAdFarm_Campaigns.aspx">tbAdFarm_Campaigns</asp:HyperLink><br />
                                <asp:HyperLink ID="HyperLink5466tr4dsfds" SkinID="FieldValue" runat="server" NavigateUrl="tbAdFarm_Slots.aspx">tbAdFarm_Slots</asp:HyperLink><br />
                                <asp:HyperLink ID="sdfdsfsdfsdfsdfsd" SkinID="FieldValue" runat="server" NavigateUrl="tbxAdFarm_SlotFormats.aspx">tbxAdFarm_SlotFormats</asp:HyperLink>
                              </p>                          </td>
                      </tr>
                  </table>
              </div>          </td>
      </tr>
    </table>
</asp:Content>
