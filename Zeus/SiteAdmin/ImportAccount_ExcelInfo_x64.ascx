﻿<%@ Control Language="C#" ClassName="ImportAccount_ExcelInfo_x64" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.OleDb" %>

<script runat="server">

    static string[] OurColumn = { 
"COGNOME"
,"NOME"
,"EMAIL"
,"UserName"
,"Password"
,"NickName"
,"CodiceFiscale"
,"Qualifica"
,"Professione"
,"Sesso"
,"Indirizzo"
,"Numero"
,"Citta"
,"Comune"
,"Cap"
,"ProvinciaEstesa"
,"ProvinciaSigla"
,"NascitaData"
,"NascitaCitta"
,"NascitaProvinciaSigla"
,"Telefono1Personale"
,"Telefono2Personale"
,"FaxPersonale"
,"Note"
,"Jolly1Personale"
,"Jolly2Personale"
,"RagioneSociale"
,"PartitaIVA"
,"S1_Indirizzo"
,"S1_Numero"
,"S1_Citta"
,"S1_Comune"
,"S1_Cap"
,"S1_ProvinciaEstesa"
,"S1_ProvinciaSigla"
,"S2_Indirizzo"
,"S2_Numero"
,"S2_Citta"
,"S2_Comune"
,"S2_Cap"
,"S2_ProvinciaEstesa"
,"S2_ProvinciaSigla"
,"Telefono1"
,"Telefono2"
,"Fax"
,"Website"
,"Jolly1Societario"
,"Jolly2Societario"
,"ConsensoDatiPersonali"
,"ConsensoDatiPersonali2"
,"Comunicazioni1"
,"Comunicazioni2"
,"Comunicazioni3"
,"Comunicazioni4"
,"Comunicazioni5"
,"Comunicazioni6"
,"Comunicazioni7"
,"Comunicazioni8"
,"Comunicazioni9"
,"Comunicazioni10"
,"Jolly1Marketing"
,"Jolly2Marketing"
,"Meta_ZeusSearch"};

    private string myPath = string.Empty;


    public string Path
    {
        get { return myPath; }
        set { myPath = value; }
    }



    public string BindTheData()
    {

        string myColum = string.Empty;
        
        if (myPath.Length <= 0)
            return myColum;
        
        try
        {
            //string strConn = "Provider=Microsoft.Jet.OLEDB.4.0;";
            //strConn += "Data Source=" + Server.MapPath(myPath) + ";";
            //strConn += "Extended Properties=\"Excel 8.0;\"";

            string strConn = "Provider=Microsoft.ACE.OLEDB.12.0;";
            strConn += "Data Source=" + Server.MapPath(myPath) + ";";
            strConn += "Extended Properties=\"Excel 12.0;HDR=YES;\"";

            string sql = "SELECT TOP 1 * FROM [IMPORT$]";


            using (OleDbConnection conn = new OleDbConnection(strConn))
            {
                OleDbCommand cmd = new OleDbCommand(sql, conn);
                conn.Open();
                OleDbDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                DataTable schemaTable = rd.GetSchemaTable();



                if (schemaTable.Rows.Count <= 0)
                    return string.Empty;

                Label myLabel = null;
                
                this.Controls.Clear();
                this.Controls.Add(new LiteralControl("<table>"));
                this.Controls.Add(new LiteralControl("<tr>"));
                this.Controls.Add(new LiteralControl("<td colspan=\"2\">"));
                myLabel = new Label();
                myLabel.Text = "Elenco colonne per l'importazione";
                myLabel.CssClass = "LabelSkin_GridTitolo1";
                this.Controls.Add(myLabel);
                this.Controls.Add(new LiteralControl("</td>"));
                this.Controls.Add(new LiteralControl("</tr>"));

                bool[] myControl = new bool[OurColumn.Length];

                for (int i = 0; i < myControl.Length; ++i)
                    myControl[i] = false;

                foreach (DataRow myField in schemaTable.Rows)
                    for (int i = 0; i < OurColumn.Length; ++i)
                        if (myField["ColumnName"].ToString().ToUpper().Equals(OurColumn[i].ToUpper()))
                            myControl[i] = true;



                
                
                
                for (int i = 0; i < OurColumn.Length; ++i)
                {
                    this.Controls.Add(new LiteralControl("<tr>"));
                    this.Controls.Add(new LiteralControl("<td>"));
                    myLabel = new Label();
                    myLabel.Text = OurColumn[i].ToUpper();
                    myLabel.CssClass = "LabelSkin_CorpoTesto";
                    this.Controls.Add(myLabel);
                    this.Controls.Add(new LiteralControl("</td>"));
                    this.Controls.Add(new LiteralControl("<td>"));

                    if (myControl[i])
                    {
                        this.Controls.Add(new LiteralControl("<img src=\"../SiteImg/Ico1_StatusOk.gif\" />"));
                        myColum +=OurColumn[i];

                        if (i + 2 < OurColumn.Length)
                            myColum += ",";
                    }
                    else
                        this.Controls.Add(new LiteralControl("<img src=\"../SiteImg/Ico1_StatusKo.gif\" />"));

                    this.Controls.Add(new LiteralControl("</td>"));
                    this.Controls.Add(new LiteralControl("</tr>"));
                }
                this.Controls.Add(new LiteralControl("</table>"));

                rd.Close();
                conn.Close();

            }//fine using

        }
        catch (Exception p)
        {
            InfoLabel.Text= p.ToString();
            
        }

        return myColum;

    }//fine BindTheData
    
    
</script>

<asp:Label ID="InfoLabel" runat="server" SkinID="Validazione01" ></asp:Label>