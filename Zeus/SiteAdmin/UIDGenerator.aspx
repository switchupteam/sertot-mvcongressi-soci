﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">
  
    static string TitoloPagina = "Generatore di UniqueIdentifier";

    protected void Page_Load(object sender, EventArgs e)
    {

        TitleField.Value = TitoloPagina;

    }//fine Page_Load


    private bool InsertUIDIntoTable(string COLUMN_ID, string TABLE_NAME, string KEY_ID, int MODE
        , string ZeusIdModulo, string ZeusLangCode)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;
        SqlDataReader reader = null;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            SqlTransaction transaction = null;

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                transaction = connection.BeginTransaction();
                command.Transaction = transaction;



                sqlQuery = "SELECT " + KEY_ID + " FROM " + TABLE_NAME;
                command.CommandText = sqlQuery;

                 reader = command.ExecuteReader();

                ArrayList myArray = new ArrayList();

                while (reader.Read())
                    myArray.Add(reader.GetInt32(0).ToString());


                reader.Close();

                string[] ID1REA = (string[])myArray.ToArray(typeof(string));

                //InfoLabel.Text = "Letti " + ID1REA.Length + " records dalla tabella " + TABLE_NAME + "<br />";

                for (int i = 0; i < ID1REA.Length; ++i)
                {

                    sqlQuery = "UPDATE " + TABLE_NAME + " SET " + COLUMN_ID + "='" + Guid.NewGuid().ToString() + "'";
                    sqlQuery+= " WHERE " + KEY_ID + "=" + ID1REA[i];
                    
                    if (ZeusLangCode!="TUTTI")
                    sqlQuery += " AND ZeusLangCode='" + ZeusLangCode + "'";

                    if (ZeusIdModulo != "TUTTI")
                    sqlQuery += " AND ZeusIdModulo='" + ZeusIdModulo + "'";

                    if (MODE == 0)
                        sqlQuery += " AND " + COLUMN_ID + " IS NULL";

                    command.CommandText = sqlQuery;
                    command.ExecuteNonQuery();
                    //Response.Write("DEB "+sqlQuery+"<br />");

                }//fine for



                transaction.Commit();
                InfoLabel.Text += "Aggiornamento della tabella " + TABLE_NAME + " avvenuto con successo!<br />";
                return true;
            }
            catch (Exception p)
            {

                WarningLabel.Text = p.ToString();
               
                if (reader != null)
                    reader.Close();
                
                
                if(transaction != null)
                    transaction.Rollback();
                return false;
            }
        }//fine using
    }//fine ClearTable



    protected void TabellaDropDownList_SelectIndexChange(object sender, EventArgs e)
    {

        if (TabellaDropDownList.SelectedValue.Equals("0"))
        {
            ColonnaDropDownList.Enabled = false;
            KeyDropDownList.Enabled = false;
            ZeusIdModuloDropDownList.Enabled = false;
            ZeusLangCodeDropDownList.Enabled = false;
        }
        else
        {

            ColonnaDropDownList.Items.Clear();
            ColonnaDropDownList.Items.Add(new ListItem("> Seleziona", "0"));
            ColonnaSqlDataSource.SelectCommand = "SELECT c.name FROM sys.columns c, sys.tables t WHERE c.object_id = t.object_id  AND c.user_type_id=36 AND t.name = '" + TabellaDropDownList.SelectedValue + "'";
            ColonnaDropDownList.DataSourceID = "ColonnaSqlDataSource";
            ColonnaDropDownList.DataBind();
            ColonnaDropDownList.Enabled = true;

            KeyDropDownList.Items.Clear();
            KeyDropDownList.Items.Add(new ListItem("> Seleziona", "0"));
            KeySqlDataSource.SelectCommand = "SELECT c.name FROM sys.columns c, sys.tables t WHERE c.object_id = t.object_id AND c.user_type_id=56 AND t.name = '" + TabellaDropDownList.SelectedValue + "'";
            KeyDropDownList.DataSourceID = "KeySqlDataSource";
            KeyDropDownList.DataBind();
            KeyDropDownList.Enabled = true;

            ZeusIdModuloDropDownList.Items.Clear();
            ZeusIdModuloDropDownList.Items.Add(new ListItem("> Seleziona", "0"));
            ZeusIdModuloSqlDataSource.SelectCommand = "SELECT ZeusIdModulo FROM " + TabellaDropDownList.SelectedValue + " GROUP BY ZeusIdModulo ORDER BY ZeusIdModulo";
            ZeusIdModuloDropDownList.DataSourceID = "ZeusIdModuloSqlDataSource";
            ZeusIdModuloDropDownList.DataBind();
            ZeusIdModuloDropDownList.Enabled = true;
            ListItem item = new ListItem();
            item.Text="> Tutti";
            item.Value = "1";
            ZeusIdModuloDropDownList.Items.Add(item);

            ZeusLangCodeDropDownList.Items.Clear();
            ZeusLangCodeDropDownList.Items.Add(new ListItem("> Seleziona", "0"));
            ZeusLangCodeSqlDataSource.SelectCommand = "SELECT ZeusLangCode FROM " + TabellaDropDownList.SelectedValue + " GROUP BY ZeusLangCode ORDER BY ZeusLangCode";
            ZeusLangCodeDropDownList.DataSourceID = "ZeusLangCodeSqlDataSource";
            ZeusLangCodeDropDownList.DataBind();
            ZeusLangCodeDropDownList.Enabled = true;
            ListItem item2 = new ListItem();
            item2.Text = "> Tutti";
            item2.Value = "1";
            ZeusLangCodeDropDownList.Items.Add(item2);
        }

    }//fine TabellaDropDownList_SelectIndexChange


    protected void ProseguiLinkButton_Click(object sender, EventArgs e)
    {

        if (Page.IsValid)
        {
            UIDLabel.Text = ColonnaDropDownList.SelectedValue;

            TabellaLabel.Text = TabellaDropDownList.SelectedValue;

            KEYLabel.Text = KeyDropDownList.SelectedValue;

            if (ZeusIdModuloDropDownList.SelectedValue.ToString()=="1")
            {
            ZeusIdModuloLabel.Text = "TUTTI";
            }
            else
                      {
            ZeusIdModuloLabel.Text = ZeusIdModuloDropDownList.SelectedValue;
            }

            if (ZeusLangCodeDropDownList.SelectedValue.ToString() == "1")
            {
                ZeusLangCodeLabel.Text = "TUTTI";
            }
            else
            {
                ZeusLangCodeLabel.Text = ZeusLangCodeDropDownList.SelectedValue;
            }
            
            
            SelezioneLabel.Text = DatiDropDownList.Items[DatiDropDownList.SelectedIndex].Text;

            FirstStepPanel.Visible = false;
            SecondStepPanel.Visible = true;
        }//fine if

    }//fine ProseguiLinkButton_Click


    protected void EseguiButton_Click(object sender, EventArgs e)
    {

        if (ConfermaCheckBox.Checked)
        {
            ErrorLabel.Text = string.Empty;
            InsertUIDIntoTable(ColonnaDropDownList.SelectedValue, 
                TabellaDropDownList.SelectedValue, KeyDropDownList.SelectedValue,
                Convert.ToInt32(DatiDropDownList.SelectedValue), ZeusIdModuloDropDownList.SelectedValue, ZeusLangCodeDropDownList.SelectedValue);

            LastStep.Visible = true;
            EseguiLinkButton.Visible = false;
            ConfermaCheckBox.Enabled = false;
        }
        else ErrorLabel.Text = "ATTENZIONE: è necesario confermare l'operazione.";

    }//fine VerificaLinkButton_Click
    
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <asp:Panel ID="FirstStepPanel" runat="server">
        <div class="BlockBox">
            <div class="BlockBoxHeader">
                <asp:Label ID="IdentificativoLabel" runat="server">Generazine UID</asp:Label>
            </div>
            <table>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label1" SkinID="FieldDescription" runat="server">Tabella:</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:DropDownList ID="TabellaDropDownList" runat="server" AppendDataBoundItems="true"
                            DataTextField="name" DataValueField="name" DataSourceID="TabellaSqlDataSource"
                            OnSelectedIndexChanged="TabellaDropDownList_SelectIndexChange" AutoPostBack="true">
                            <asp:ListItem Text="> Seleziona" Value="0" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator" ErrorMessage="Obbligatorio"
                            runat="server" ControlToValidate="TabellaDropDownList" SkinID="ZSSM_Validazione01"
                            Display="Dynamic" InitialValue="0" ValidationGroup="myValidation">
                        </asp:RequiredFieldValidator>
                        <asp:SqlDataSource ID="TabellaSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                            SelectCommand="SELECT name FROM sys.Tables order by name ASC"></asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label2" SkinID="FieldDescription" runat="server">Colonna UID:</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:DropDownList ID="ColonnaDropDownList" runat="server" Enabled="false" AppendDataBoundItems="true"
                            DataTextField="name" DataValueField="name">
                            <asp:ListItem Text="> Seleziona" Value="0" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Obbligatorio"
                            runat="server" ControlToValidate="ColonnaDropDownList" SkinID="ZSSM_Validazione01"
                            Display="Dynamic" InitialValue="0" ValidationGroup="myValidation">
                        </asp:RequiredFieldValidator>
                        <asp:SqlDataSource ID="ColonnaSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>">
                        </asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label4" SkinID="FieldDescription" runat="server">Colonna PRIMARY KEY:</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:DropDownList ID="KeyDropDownList" runat="server" Enabled="false" AppendDataBoundItems="true"
                            DataTextField="name" DataValueField="name">
                            <asp:ListItem Text="> Seleziona" Value="0" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Obbligatorio"
                            runat="server" ControlToValidate="KeyDropDownList" SkinID="ZSSM_Validazione01"
                            Display="Dynamic" InitialValue="0" ValidationGroup="myValidation">
                        </asp:RequiredFieldValidator>
                        <asp:SqlDataSource ID="KeySqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>">
                        </asp:SqlDataSource>
                    </td>
                </tr>
                 <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label13" SkinID="FieldDescription" runat="server">Colonna ZeusIdModulo:</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:DropDownList ID="ZeusIdModuloDropDownList" runat="server" AppendDataBoundItems="true"
                            DataTextField="ZeusIdModulo" DataValueField="ZeusIdModulo" DataSourceID="ZeusIdModuloSqlDataSource" Enabled="false">
                            <asp:ListItem Text="> Seleziona" Value="0" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="> Tutti" Value="*"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="ZeusIdModuloRequiredFieldValidator" ErrorMessage="Obbligatorio"
                            runat="server" ControlToValidate="ZeusIdModuloDropDownList" SkinID="ZSSM_Validazione01"
                            Display="Dynamic" InitialValue="0" ValidationGroup="myValidation">
                        </asp:RequiredFieldValidator>
                        <asp:SqlDataSource ID="ZeusIdModuloSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>">
                        </asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label14" SkinID="FieldDescription" runat="server">Colonna ZeusLangCode:</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:DropDownList ID="ZeusLangCodeDropDownList" runat="server" AppendDataBoundItems="true"
                            DataTextField="ZeusLangCode" DataValueField="ZeusLangCode" DataSourceID="ZeusLangCodeSqlDataSource" Enabled="false">
                            <asp:ListItem Text="> Seleziona" Value="0" Selected="True" ></asp:ListItem>
                            <asp:ListItem Text="> Tutti" Value="1"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="ZeusLangCodeRequiredFieldValidator4" ErrorMessage="Obbligatorio"
                            runat="server" ControlToValidate="ZeusLangCodeDropDownList" SkinID="ZSSM_Validazione01"
                            Display="Dynamic" InitialValue="0" ValidationGroup="myValidation">
                        </asp:RequiredFieldValidator>
                        <asp:SqlDataSource ID="ZeusLangCodeSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>">
                        </asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label3" SkinID="FieldDescription" runat="server">Selezione dati:</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:DropDownList ID="DatiDropDownList" runat="server">
                            <asp:ListItem Text="Solo record NULL" Value="0" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Tutti i record" Value="1"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </div>
        <div align="center">
            <asp:LinkButton ID="ProseguiLinkButton" runat="server" Text="Prosegui" SkinID="ZSSM_Button01"
                CausesValidation="true" ValidationGroup="myValidation" OnClick="ProseguiLinkButton_Click"></asp:LinkButton></div>
    </asp:Panel>
    <asp:Panel ID="SecondStepPanel" runat="server" Visible="false">
        <div class="BlockBox">
            <div class="BlockBoxHeader">
                <asp:Label ID="Label6" runat="server">Generazine UID</asp:Label>
            </div>
            <table>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label7" SkinID="FieldDescription" runat="server">Tabella:</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:Label ID="TabellaLabel" SkinID="FieldValue" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label8" SkinID="FieldDescription" runat="server">Colonna UID:</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:Label ID="UIDLabel" SkinID="FieldValue" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label9" SkinID="FieldDescription" runat="server">Colonna PRIMARY KEY:</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:Label ID="KEYLabel" SkinID="FieldValue" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label15" SkinID="FieldDescription" runat="server">Colonna ZeusIdModulo:</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:Label ID="ZeusIdModuloLabel" SkinID="FieldValue" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label16" SkinID="FieldDescription" runat="server">Colonna ZeusLangCode:</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:Label ID="ZeusLangCodeLabel" SkinID="FieldValue" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label10" SkinID="FieldDescription" runat="server">Selezione dati:</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:Label ID="SelezioneLabel" SkinID="FieldValue" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label11" SkinID="FieldDescription" runat="server">Conferma:</asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:CheckBox ID="ConfermaCheckBox" runat="server" Checked="false" />
                        <asp:Label ID="ErrorLabel" SkinID="Validazione01" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div align="center">
            <asp:LinkButton ID="EseguiLinkButton" runat="server" Text="Esegui" SkinID="ZSSM_Button01"
                OnClick="EseguiButton_Click"></asp:LinkButton></div>
        <asp:Panel ID="LastStep" runat="server" Visible="false">
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="Label12" runat="server">Riassunto esecuzione</asp:Label>
                </div>
                <table>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label5" SkinID="FieldDescription" runat="server">Stato:</asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:Label ID="InfoLabel" SkinID="FieldValue" runat="server"></asp:Label>
                            <asp:Label ID="WarningLabel" SkinID="Validazione01" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
