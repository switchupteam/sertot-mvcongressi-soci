﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Data.OleDb;

public partial class Zeus_SiteAdmin_FindAndReplaceAll : System.Web.UI.Page
{



    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    private bool ListTable()
    {
        List<string[]> result = new List<string[]>();


        try
        {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            string sqlQuery = "SELECT ty.name as tipo, c.name as colonna, o.name as tabella FROM sys.objects o INNER JOIN sys.columns c ON c.object_id = o.object_id INNER JOIN  sys.types ty ON c.user_type_id = ty.user_type_id where (ty.name='nvarchar' OR ty.name='varchar') AND o.name LIKE 'tb%'";



            using (SqlConnection connection = new SqlConnection(strConnessione))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                command.CommandText = sqlQuery;

                //command.Parameters.Add("@accountId", System.Data.SqlDbType.VarChar);
                //command.Parameters["@accountId"].Value = accountId;


                SqlDataReader reader = command.ExecuteReader();

				
				Response.Write("Start");
                while (reader.Read())
                {
                    string[] app=new string[3];
                    //Response.Write(reader["name"].ToString()+"<br>");

                    app[0] = Convert.ToString(reader["tabella"]);
                    app[1] = Convert.ToString(reader["colonna"]);
                    app[2] = Convert.ToString(reader["tipo"]);
                    result.Add(app);
					
					//Response.Write("Table: " + app[0] + " - " + app[1] + " - [" + app[2] + "]<br>");
                }
                reader.Close();
                command.Cancel();

                TextBox StringaDaSostituire = (TextBox)form1.FindControl("StringaDaSostituire");
                TextBox NuovaStringa = (TextBox)form1.FindControl("NuovaStringa");

                result.ForEach(delegate(string[] res)
                {

                    command = connection.CreateCommand();
                    
                    command.CommandText = "update " + res[0] + " set " + res[1] + " = replace(\"" + res[1] + "\", '" + StringaDaSostituire.Text + "', '" + NuovaStringa .Text+ "');";
                    reader = command.ExecuteReader();

                    Response.Write("Table: " + res[0] + " - " + res[1] + " - [" + res[2] + "]<br>");
                    

                    reader.Close();
                    command.Cancel();
                    

                });

                Response.Write("<br><br>End");

                return true;



                


            }//fine using

            //using (var reader = command.ExecuteReader())
            //{
            //    reader.Read();
            //    var table = reader.GetSchemaTable();
            //    foreach (DataColumn column in table.Columns)
            //    {
            //        Console.WriteLine(column.ColumnName);
            //    }
            //}

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }

        return false;
    }//fine IsParentInAchive


    protected void Button1_Click(object sender, EventArgs e)
    {
        CheckBox ConfermaCB = (CheckBox)form1.FindControl("ConfermaCB");
        if (ConfermaCB.Checked==true)
            ListTable();
    }
}