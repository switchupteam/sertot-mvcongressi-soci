﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ OutputCache Duration="1" NoStore="true"  VaryByParam="none"%>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.OleDb" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register Src="ImportAccount_ExcelInfo_x64.ascx" TagName="ImportAccount_ExcelInfo"
    TagPrefix="uc1" %>

<script runat="server">
  
    static string TitoloPagina = "Importazione account utente da file Excel - versione per Server x64";


    protected void Page_Init()
    {
        string firstScript = "/SiteJS/jquery-1.2.6.js";
        string secondScript = "/SiteJS/jQuery.BlockUI.js";

        HtmlGenericControl Include = new HtmlGenericControl("script");
        Include.Attributes.Add("type", "text/javascript");
        Include.Attributes.Add("src", firstScript);
        this.Page.Header.Controls.Add(Include);

        Include = new HtmlGenericControl("script");
        Include.Attributes.Add("type", "text/javascript");
        Include.Attributes.Add("src", secondScript);
        this.Page.Header.Controls.Add(Include);


        LoadButton.Attributes.Add("onfocus", "javascript:return SetIsLoadButtonToTrue();");
        VerificaButton.Attributes.Add("onfocus", "javascript:return SetIsLoadButtonToTrue();");
        CreateUserButton.Attributes.Add("onfocus", "javascript:return SetIsLoadButtonToTrue();");

        Response.AddHeader("CACHE-CONTROL", "NO-CACHE");
        Response.AddHeader("EXPIRES", "0");
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
    }//fine Page_Init

    protected void Page_Load(object sender, EventArgs e)
    {

        TitleField.Value = TitoloPagina;

    }//fine Page_Load


    private string Generate(int ID, int SIZE)
    {
        string chars = "ABCDEFGHILMNOPQRSTUVZ1234567890abcdefghilmnopqrstuvzxykj";
        int i;
        string r = string.Empty;
        ID += Convert.ToInt32(DateTime.Now.Minute);

        Random rnd = new Random(ID);
        for (int j = 1; j <= SIZE; j++)
        {
            i = Convert.ToInt32(rnd.Next(0, chars.Length));
            r += chars.Substring(i, 1);
        }
        return r;
    }//fine GeneratePassword


    private string GenerateUserName(int ID, int SIZE)
    {
        string chars = "1234567890";
        int i;
        string r = string.Empty;
        ID += Convert.ToInt32(DateTime.Now.Minute);

        Random rnd = new Random(ID);
        for (int j = 1; j <= SIZE; j++)
        {
            i = Convert.ToInt32(rnd.Next(0, chars.Length));
            r += chars.Substring(i, 1);
        }
        return r;
    }//fine GenerateUserName

    private string GetImportCode()
    {

        string ImportCode = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString()
            + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();

        return ImportCode + Generate(Convert.ToInt32(DateTime.Now.Millisecond), 16 - ImportCode.Length);
    }//fine GetImportCode




    private string GetExtension(string FileName)
    {
        string[] split = FileName.Split('.');
        string Extension = split[split.Length - 1];
        return Extension;

    }//fine GetExtension

    private bool ClearTempPath()
    {
        try
        {
            

            string[] Files = System.IO.Directory.GetFiles(Server.MapPath("~/ZeusInc/TempDataImport/"));

            for (int i = 0; i < Files.Length; ++i)
            {
                System.IO.FileInfo myFileInfo = new System.IO.FileInfo(Files[i]);

                myFileInfo.IsReadOnly = false;

                if (DateTime.Compare(myFileInfo.CreationTime, DateTime.Now.AddDays(-365)) <= 0)
                    System.IO.File.Delete(Files[i]);

            }//fine for

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }//fine ClearTempPath

    protected void LoadButton_Click(object sender, EventArgs e)
    {
        InfoLabel.Text = string.Empty;

        ClearTempPath();

        if ((FileUpload1.HasFile) && (GetExtension(FileUpload1.FileName).Equals("xls")))
            
        {
          
            string strFile_name = System.IO.Path.GetFileNameWithoutExtension(FileUpload1.FileName);
            string strFile_extension = System.IO.Path.GetExtension(FileUpload1.FileName);
            string myFileName = strFile_name + Generate(Convert.ToInt32(DateTime.Now.Millisecond), 4)+strFile_extension;

            FileUpload1.SaveAs(Server.MapPath("~/ZeusInc/TempDataImport/") + myFileName);
            PathHiddenField.Value = "~/ZeusInc/TempDataImport/" + myFileName;
            VerificaButton.Visible = true;
          

        }//fine if
    }//fine LoadButton_Click

    protected void VerificaButton_Click(object sender, EventArgs e)
    {
        ImportAccount_ExcelInfo1.Path = PathHiddenField.Value;
        CheckedColumnsHiddenField.Value = ImportAccount_ExcelInfo1.BindTheData();
        VerificaButton.Visible = false;
        ImportCodeHiddenField.Value = ImportCodeLabel.Text = GetImportCode();
        ImportCodeLabel.Text = "Codice di importazione utenti: " + ImportCodeLabel.Text;
        ImportCodeLabel.Visible = CreateUserButton.Visible = true;
    }//fine VerificaButton_Click

    protected void CreateUserButton_Click(object sender, EventArgs e)
    {

        string[] CheckedColum = null;
        ArrayList myArray = null;
        char[] mySplit = { ',' };

        string SqlColums = CheckedColumnsHiddenField.Value;
        CheckedColum = CheckedColumnsHiddenField.Value.Split(mySplit);


        if (CheckedColum != null)
        {

            try
            {
                //string strConn = "Provider=Microsoft.Jet.OLEDB.4.0;";
                //strConn += "Data Source=" + Server.MapPath(PathHiddenField.Value) + ";";
                //strConn += "Extended Properties=\"Excel 8.0;\"";

                string strConn = "Provider=Microsoft.ACE.OLEDB.12.0;";
                strConn += "Data Source=" + Server.MapPath(PathHiddenField.Value) + ";";
                strConn += "Extended Properties=\"Excel 12.0;HDR=YES;\"";

                string sql = "SELECT  " + SqlColums + " FROM [IMPORT$]";
                //Response.Write("DEB "+sql+"br />");

                using (OleDbConnection conn = new OleDbConnection(strConn))
                {
                    OleDbCommand cmd = new OleDbCommand(sql, conn);
                    conn.Open();
                    OleDbDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);


                    myArray = new ArrayList();
                    string[] myData = null;

                    while (rd.Read())
                    {
                        myData = new string[CheckedColum.Length];
                        for (int i = 0; i < CheckedColum.Length; ++i)
                        {
                            if (rd[CheckedColum[i]] != null)
                                myData[i] = rd[CheckedColum[i]].ToString();
                            else myData[i] = string.Empty;

                        }
                        myArray.Add(myData);

                    }//fine while

                    rd.Close();

                    conn.Close();

                }//fine using


                string[][] AllData = (string[][])myArray.ToArray(typeof(string[]));
                string[] SingleUser = null;

                bool AllOK = true;
                int index = 0;
                int err = 0;

                for (int i = 0; i < AllData.Length; ++i)
                {
                    SingleUser = new string[CheckedColum.Length];

                    for (int j = 0; j < CheckedColum.Length; ++j)
                        SingleUser[j] = AllData[i][j];

                    if (CreateUser(SingleUser, CheckedColum))
                        ++index;
                    else
                    {
                        AllOK = false;
                        ++err;
                       // break; //DA RIMUOVERE
                    }

                }//fine for

                //FinishLabel.Text = "Codice di importazione " + ImportCodeHiddenField.Value + "<br />";

                if (AllOK)
                    FinishLabel.Text ="<br />"+ index + " utenti importati correttamente.";
                else FinishLabel.Text = "<br />Creati" + index + " utenti importati con successo.<br /> " + err + " non importati per errori.";

                FinishLabel.Visible = true;
                FileUpload1.Visible = false;
                LoadButton.Visible = false;
                CreateUserButton.Visible = false;
                VerificaButton.Visible = false;
                //System.IO.File.Delete(Server.MapPath(PathHiddenField.Value));


            }
            catch (Exception p)
            {
                InfoLabel.Text = p.ToString();

            }



        }//fine if
    }//fine CreateUserButton_Click



   

    private bool CheckUserName(string myUserName)
    {
        try
        {
            if (Membership.GetUser(myUserName).UserName.Length > 0)
                return true;
            else return false;
        }
        catch (Exception p)
        {
            return false;
        }
    }//fine CheckUserName


    private bool CreateUser(string[] myData, string[] myColumns)
    {
        if (myData == null)
            return false;

        if (myColumns == null)
            return false;


        string COGNOME = "-";
        string NOME = "-";
        string EMAIL = ConfigurationManager.AppSettings["MissingMail"];
        string UserName = string.Empty;
        string Password = Generate(Convert.ToInt32(DateTime.Now.Millisecond), 10) + "!";
        string NickName = string.Empty;
        string CodiceFiscale = string.Empty;
        string Qualifica = string.Empty;
        string Professione = string.Empty;
        string Sesso = "X";
        string Indirizzo = string.Empty;
        string Numero = string.Empty;
        string Citta = string.Empty;
        string Comune = string.Empty;
        string Cap = string.Empty;
        string ProvinciaEstesa = string.Empty;
        string ProvinciaSigla = string.Empty;
        string NascitaData = string.Empty;
        string NascitaCitta = string.Empty;
        string NascitaProvinciaSigla = string.Empty;
        string Telefono1Personale = string.Empty;
        string Telefono2Personale = string.Empty;
        string FaxPersonale = string.Empty;
        string Note = string.Empty;
        string Jolly1Personale = "0";
        string Jolly2Personale = string.Empty;
        string RagioneSociale = string.Empty;
        string PartitaIVA = string.Empty;
        string S1_Indirizzo = string.Empty;
        string S1_Numero = string.Empty;
        string S1_Citta = string.Empty;
        string S1_Comune = string.Empty;
        string S1_Cap = string.Empty;
        string S1_ProvinciaEstesa = string.Empty;
        string S1_ProvinciaSigla = string.Empty;
        string S2_Indirizzo = string.Empty;
        string S2_Numero = string.Empty;
        string S2_Citta = string.Empty;
        string S2_Comune = string.Empty;
        string S2_Cap = string.Empty;
        string S2_ProvinciaEstesa = string.Empty;
        string S2_ProvinciaSigla = string.Empty;
        string Telefono1 = string.Empty;
        string Telefono2 = string.Empty;
        string Fax = string.Empty;
        string Website = string.Empty;
        string Jolly1Societario = "0";
        string Jolly2Societario = string.Empty;
        string ConsensoDatiPersonali = "true";
        string ConsensoDatiPersonali2 = "true";
        string Comunicazioni1 = "true";
        string Comunicazioni2 = "true";
        string Comunicazioni3 = "true";
        string Comunicazioni4 = "true";
        string Comunicazioni5 = "true";
        string Comunicazioni6 = "true";
        string Comunicazioni7 = "true";
        string Comunicazioni8 = "true";
        string Comunicazioni9 = "true";
        string Comunicazioni10 = "true";
        string Jolly1Marketing = "0";
        string Jolly2Marketing = string.Empty;
        string Meta_ZeusSearch = string.Empty;
        delinea myDelinea = new delinea();


        for (int i = 0; i < myColumns.Length; ++i)
        {

            switch (myColumns[i].ToLower())
            {
                case "cognome":
                    if (Regex.Replace(myData[i], @"[\W]", string.Empty).Length > 0)
                        COGNOME = Regex.Replace(myData[i], @"[\W]", string.Empty);


                    COGNOME = myDelinea.UppercaseFirst(COGNOME.ToLower());
                    
                    if (COGNOME.Length > 50)
                        COGNOME = COGNOME.Substring(0, 50);
                    
                    break;

                case "nome":
                    if (Regex.Replace(myData[i], @"[\W]", string.Empty).Length > 0)
                        NOME = Regex.Replace(myData[i], @"[\W]", string.Empty);

                    NOME = myDelinea.UppercaseFirst(NOME.ToLower());

                    if (NOME.Length > 50)
                        NOME = NOME.Substring(0, 50);
                    
                    break;

                case "username":

                     if (myData[i].Length > 0)
                        UserName = myData[i];
                    break;

                case "email":

                    Regex myRegex = new Regex(@"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$");
                    
                    if ((myData[i].Length > 0)
                        && (myRegex.IsMatch(myData[i])))
                        EMAIL = myData[i].ToLower();
                    
                    break;



                case "password":
                    if (myData[i].Length > 0)
                        Password = myData[i];
                    break;

                case "nickname":
                    NickName = myData[i];
                    break;

                case "codicfiscale":
                    CodiceFiscale = myData[i];


                    if (CodiceFiscale.Length > 16)
                        CodiceFiscale = CodiceFiscale.Substring(0, 16);
                    break;

                case "qualifica":
                    Qualifica = myData[i];

                    if (Qualifica.Length > 10)
                        Qualifica = Qualifica.Substring(0, 10);
                    break;

                case "professione":
                    Professione = myData[i];

                    if (Professione.Length > 50)
                        Professione = Professione.Substring(0, 50);
                    break;

                case "sesso":
                    if (myData[i].Length > 0)
                        Sesso = myData[i];

                    if (Sesso.Length > 1)
                        Sesso = Sesso.Substring(0, 1);

                    break;

                case "indirizzo":
                    Indirizzo = myData[i];

                    if (Indirizzo.Length > 50)
                        Indirizzo = Indirizzo.Substring(0, 50);
                    
                    break;

                case "numero":
                    Numero = myData[i];

                    if (Numero.Length > 10)
                        Numero = Numero.Substring(0, 10);
                    break;

                case "citta":
                    Citta = myDelinea.UppercaseFirst(myData[i].ToLower());

                    if (Citta.Length > 50)
                        Citta = Citta.Substring(0, 50);
                    
                    break;

                case "comune":
                    Comune = myDelinea.UppercaseFirst(myData[i].ToLower());

                    if (Comune.Length > 50)
                        Comune = Comune.Substring(0, 50);
                    break;

                case "cap":
                    Cap = myData[i];

                    if (Cap.Length > 5)
                        Cap = Cap.Substring(0, 5);
                    
                    break;

                case "provinciaestesa":
                    ProvinciaEstesa = myDelinea.UppercaseFirst(myData[i].ToLower());
                    
                    if (ProvinciaEstesa.Length > 50)
                        ProvinciaEstesa = ProvinciaEstesa.Substring(0, 50);
                    
                    break;

                case "provinciasigla":
                    ProvinciaSigla = myData[i];

                    if (ProvinciaSigla.Length > 2)
                        ProvinciaSigla = ProvinciaSigla.Substring(0, 2);
                    
                    break;

                case "nascitadata":
                    if (myData[i].Length > 0)
                        NascitaData = myData[i];
                    else NascitaData = null;
                    break;

                case "nascitacitta":
                    NascitaCitta = myData[i];

                    if (NascitaCitta.Length > 50)
                        NascitaCitta = NascitaCitta.Substring(0, 50);
                    
                    break;

                case "nascitaprovinciasigla":
                    NascitaProvinciaSigla = myData[i];

                    if (NascitaProvinciaSigla.Length > 2)
                        NascitaProvinciaSigla = NascitaProvinciaSigla.Substring(0, 2);
                    
                    break;

                case "telefono1personale":
                    Telefono1Personale = myData[i];

                    if (Telefono1Personale.Length > 20)
                        Telefono1Personale = Telefono1Personale.Substring(0, 20);
                    
                    break;

                case "telefono2personale":
                    Telefono2Personale = myData[i];

                    if (Telefono2Personale.Length > 20)
                        Telefono2Personale = Telefono2Personale.Substring(0, 20);
                    
                    
                    break;

                case "faxpersonale":
                    FaxPersonale = myData[i];

                    if (FaxPersonale.Length > 20)
                        FaxPersonale = FaxPersonale.Substring(0, 20);
                    break;

                case "note":
                    Note = myData[i];


                    if (Note.Length > 200)
                        Note = Note.Substring(0, 200);
                    break;

                case "jolly1personale":
                    if (myData.Length > 0)
                        Jolly1Personale = myData[i];
                    
                    break;

                case "jolly2personale":
                    Jolly2Personale = myData[i];

                    if (Jolly2Personale.Length > 50)
                        Jolly2Personale = Jolly2Personale.Substring(0, 50);
                    
                    break;

                case "ragionesociale":
                    RagioneSociale = myData[i];

                    if (RagioneSociale.Length > 100)
                        RagioneSociale = RagioneSociale.Substring(0, 100);
                    
                    break;

                case "partitaiva":
                    PartitaIVA = myData[i];


                    if (PartitaIVA.Length > 20)
                        PartitaIVA = PartitaIVA.Substring(0, 20);
                    
                    break;

                case "s1_indirizzo":
                    S1_Indirizzo = myData[i];

                    if (S1_Indirizzo.Length > 50)
                        S1_Indirizzo = S1_Indirizzo.Substring(0, 50);
                    break;

                case "s1_numero":
                    S1_Numero = myData[i];

                    if (S1_Numero.Length > 10)
                        S1_Numero = S1_Numero.Substring(0, 10);
                    
                    break;

                case "s1_citta":
                    S1_Citta = myDelinea.UppercaseFirst(myData[i].ToLower());

                    if (S1_Citta.Length > 50)
                        S1_Citta = S1_Citta.Substring(0, 50);
                    
                    break;

                case "s1_comune":
                    S1_Comune = myDelinea.UppercaseFirst(myData[i].ToLower());

                    if (S1_Comune.Length > 50)
                        S1_Comune = S1_Comune.Substring(0, 50);
                    
                    break;

                case "s1_cap":
                    S1_Cap = myData[i];

                    if (S1_Cap.Length > 5)
                        S1_Cap = S1_Cap.Substring(0, 5);
                    
                    break;

                case "s1_provinciaestesa":
                    S1_ProvinciaEstesa = myDelinea.UppercaseFirst(myData[i].ToLower());

                    if (S1_ProvinciaEstesa.Length > 50)
                        S1_ProvinciaEstesa = S1_ProvinciaEstesa.Substring(0, 50);
                    
                    break;

                case "s1_provinciasigla":
                    S1_ProvinciaSigla = myData[i];

                    if (S1_ProvinciaSigla.Length > 2)
                        S1_ProvinciaSigla = S1_ProvinciaSigla.Substring(0, 2);
                    
                    break;

                case "s2_indirizzo":
                    S2_Indirizzo = myData[i];

                    if (S2_Indirizzo.Length > 50)
                        S2_Indirizzo = S2_Indirizzo.Substring(0, 50);
                    break;

                case "s2_numero":
                    S2_Numero = myData[i];

                    if (S2_Numero.Length > 10)
                        S2_Numero = S2_Numero.Substring(0, 10);
                    break;

                case "s2_citta":
                    S2_Citta = myDelinea.UppercaseFirst(myData[i].ToLower());


                    if (S2_Citta.Length > 50)
                        S2_Citta = S2_Citta.Substring(0, 50);
                    break;

                case "s2_comune":
                    S2_Comune = myDelinea.UppercaseFirst(myData[i].ToLower());

                    if (S2_Comune.Length > 50)
                        S2_Comune = S2_Comune.Substring(0, 50);
                    break;

                case "s2_cap":
                    S2_Cap = myData[i];

                    if (S2_Cap.Length > 5)
                        S2_Cap = S2_Cap.Substring(0, 5);
                    break;

                case "s2_provinciaestesa":
                    S2_ProvinciaEstesa = myData[i];

                    if (S2_ProvinciaEstesa.Length > 50)
                        S2_ProvinciaEstesa = S2_ProvinciaEstesa.Substring(0, 50);
                    
                    break;

                case "s2_provinciasigla":
                    S2_ProvinciaSigla = myData[i];


                    if (S2_ProvinciaSigla.Length > 2)
                        S2_ProvinciaSigla = S2_ProvinciaSigla.Substring(0, 2);
                    break;

                case "telefono1":
                    Telefono1 = myData[i];

                    if (Telefono1.Length > 20)
                        Telefono1 = Telefono1.Substring(0, 20);
                    
                    break;

                case "telefono2":
                    Telefono2 = myData[i];

                    if (Telefono2.Length > 20)
                        Telefono2 = Telefono2.Substring(0, 20);
                    break;

                case "fax":
                    Fax = myData[i];

                    if (Fax.Length > 20)
                        Fax = Fax.Substring(0, 20);
                    break;


                case "website":
                    Website = myData[i];

                    if (Website.Length > 100)
                        Website = Website.Substring(0, 100);
                    
                    break;

                case "jolly1societario":
                    if (myData[i].Length > 0)
                        Jolly1Societario = myData[i];
                    break;

                case "jolly2societario":
                    Jolly2Societario = myData[i];

                    if (Jolly2Societario.Length > 50)
                        Jolly2Societario = Jolly2Societario.Substring(0, 50);
                    break;

                case "consensodatipersonali":
                    if (myData[i].Length > 0)
                        ConsensoDatiPersonali = myData[i];

                    break;

                case "consensodatipersonali2":
                    if (myData[i].Length > 0)
                        ConsensoDatiPersonali2 = myData[i];

                    break;

                case "comunicazioni1":
                    if (myData[i].Length > 0)
                        Comunicazioni1 = myData[i];

                    break;

                case "comunicazioni2":
                    if (myData[i].Length > 0)
                        Comunicazioni2 = myData[i];
                    break;


                case "comunicazioni3":
                    if (myData[i].Length > 0)
                        Comunicazioni3 = myData[i];

                    break;

                case "comunicazioni4":
                    if (myData[i].Length > 0)
                        Comunicazioni4 = myData[i];

                    break;

                case "comunicazioni5":
                    if (myData[i].Length > 0)
                        Comunicazioni5 = myData[i];

                    break;

                case "comunicazioni6":
                    if (myData[i].Length > 0)
                        Comunicazioni6 = myData[i];

                    break;

                case "comunicazioni7":
                    if (myData[i].Length > 0)
                        Comunicazioni7 = myData[i];
                    break;

                case "comunicazioni8":
                    if (myData[i].Length > 0)
                        Comunicazioni8 = myData[i];
                    break;

                case "comunicazioni9":
                    if (myData[i].Length > 0)
                        Comunicazioni9 = myData[i];
                    break;

                case "comunicazioni10":
                    if (myData[i].Length > 0)
                        Comunicazioni10 = myData[i];
                    break;

                case "jolly1marketing":
                    if (myData[i].Length > 0)
                        Jolly1Marketing = myData[i];
                    break;

                case "jolly2marketing":
                    Jolly2Marketing = myData[i];

                    if (Jolly2Marketing.Length > 50)
                        Jolly2Marketing = Jolly2Marketing.Substring(0, 50);
                    break;
                
                case "meta_zeussearch":
                    Meta_ZeusSearch = myData[i];
                    break;

            }//fine switch

        }//fine for

        
        // ------------------------------ Se UserName non è stato ancora impostato -----------------------------
        // -----------------------------------------------------------------------------------------------------
        if (UserName==string.Empty)
        {
        if ((!COGNOME.Equals("-"))
            && (!NOME.Equals("-")))
            UserName = NOME.Replace(" ", "_") + "." + COGNOME.Replace(" ", "_");
        else if ((EMAIL.Length > 0) && (EMAIL.IndexOf(ConfigurationManager.AppSettings["MissingMail"])) == -1)
            UserName = EMAIL.Substring(0, EMAIL.IndexOf("@"));
        else UserName = GenerateUserName(Convert.ToInt32(DateTime.Now.Millisecond), 6);
        }

        // ------------------------------ Qui controllo il valore di UserName ----------------------------------
        // -----------------------------------------------------------------------------------------------------
        int counter = 1;
        // salvo il valore di UserName a cui andrò ad accodare i numeri
        string BareUserName = UserName;
        while (CheckUserName(UserName))
        {
            UserName = BareUserName + counter.ToString();
            ++counter;
        }

        


        //String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["AlaskaConnectionString"].ConnectionString;
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            SqlTransaction transaction = null;

            try
            {

                object UserGUID = Membership.CreateUser(UserName, Password, EMAIL).ProviderUserKey;
                object CreatorGUID = Membership.GetUser().ProviderUserKey;


                connection.Open();
                SqlCommand command = connection.CreateCommand();

                transaction = connection.BeginTransaction();
                command.Transaction = transaction;

                sqlQuery = "SET DATEFORMAT dmy; INSERT INTO [tbProfiliPersonali]";
                sqlQuery += " ([UserId],[Cognome],[Nome],[CodiceFiscale],[Qualifica],[Professione]";
                sqlQuery += " ,[Sesso],[Indirizzo],[Numero],[Citta],[Comune],[Cap],[ProvinciaEstesa]";
                sqlQuery += " ,[ProvinciaSigla],[ID2Nazione],[NascitaCitta],[NascitaProvinciaSigla]";
                sqlQuery += " ,[NascitaID2Nazione],[Telefono1],[Telefono2],[Fax],[Note],[UserMaster],[RecordNewUser]";
                sqlQuery += " ,[RecordNewDate]";
                sqlQuery += " ,[ZeusJolly1],[ZeusJolly2],[ImportCode])";
                sqlQuery += " VALUES ";
                sqlQuery += " (@UserId,@Cognome,@Nome,@CodiceFiscale,@Qualifica,@Professione";
                sqlQuery += " ,@Sesso,@Indirizzo,@Numero,@Citta,@Comune,@Cap,@ProvinciaEstesa";
                sqlQuery += " ,@ProvinciaSigla,@ID2Nazione,@NascitaCitta,@NascitaProvinciaSigla";
                sqlQuery += " ,@NascitaID2Nazione,@Telefono1,@Telefono2,@Fax,@Note,@UserMaster,@RecordNewUser";
                sqlQuery += " ,@RecordNewDate";
                sqlQuery += " ,@ZeusJolly1,@ZeusJolly2,@ImportCode)";

                command.CommandText = sqlQuery;
                command.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@UserId"].Value = UserGUID;

                command.Parameters.Add("@Cognome", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Cognome"].Value = COGNOME;

                command.Parameters.Add("@Nome", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Nome"].Value = NOME;

                command.Parameters.Add("@CodiceFiscale", System.Data.SqlDbType.NVarChar);
                command.Parameters["@CodiceFiscale"].Value = CodiceFiscale;

                command.Parameters.Add("@Qualifica", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Qualifica"].Value = Qualifica;

                command.Parameters.Add("@Professione", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Professione"].Value = Professione;

                command.Parameters.Add("@Sesso", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Sesso"].Value = Sesso;

                command.Parameters.Add("@Indirizzo", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Indirizzo"].Value = Indirizzo;

                command.Parameters.Add("@Numero", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Numero"].Value = Numero;

                command.Parameters.Add("@Citta", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Citta"].Value = Citta;

                command.Parameters.Add("@Comune", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Comune"].Value = Comune;

                command.Parameters.Add("@Cap", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Cap"].Value = Cap;


                command.Parameters.Add("@ProvinciaEstesa", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ProvinciaEstesa"].Value = ProvinciaEstesa;

                command.Parameters.Add("@ProvinciaSigla", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ProvinciaSigla"].Value = ProvinciaSigla;

                command.Parameters.Add("@ID2Nazione", System.Data.SqlDbType.Int);
                command.Parameters["@ID2Nazione"].Value = 1;

                //command.Parameters.Add("@NascitaData",System.Data.SqlDbType.SmallDateTime);
                //command.Parameters["@NascitaData"].Value = NascitaData;

                command.Parameters.Add("@NascitaCitta", System.Data.SqlDbType.NVarChar);
                command.Parameters["@NascitaCitta"].Value = NascitaCitta;

                command.Parameters.Add("@NascitaProvinciaSigla", System.Data.SqlDbType.NVarChar);
                command.Parameters["@NascitaProvinciaSigla"].Value = NascitaProvinciaSigla;

                command.Parameters.Add("@NascitaID2Nazione", System.Data.SqlDbType.Int);
                command.Parameters["@NascitaID2Nazione"].Value = 1;

                command.Parameters.Add("@Telefono1", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Telefono1"].Value = Telefono1Personale;

                command.Parameters.Add("@Telefono2", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Telefono2"].Value = Telefono2;

                command.Parameters.Add("@Fax", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Fax"].Value = Fax;

                command.Parameters.Add("@Note", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Note"].Value = Note;

                command.Parameters.Add("@UserMaster", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@UserMaster"].Value = CreatorGUID;

                command.Parameters.Add("@RecordNewUser", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@RecordNewUser"].Value = CreatorGUID;

                command.Parameters.Add("@RecordNewDate", System.Data.SqlDbType.SmallDateTime);
                command.Parameters["@RecordNewDate"].Value = DateTime.Now;

                command.Parameters.Add("@ZeusJolly1", System.Data.SqlDbType.Int);
                command.Parameters["@ZeusJolly1"].Value = Convert.ToInt32(Jolly1Personale);

                command.Parameters.Add("@ZeusJolly2", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ZeusJolly2"].Value = Jolly2Personale;

                command.Parameters.Add("@ImportCode", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ImportCode"].Value = ImportCodeHiddenField.Value;


                command.ExecuteNonQuery();

                command.Parameters.Clear();
                sqlQuery = "SET DATEFORMAT dmy; INSERT INTO [tbProfiliSocietari]";
                sqlQuery += " ([UserId],[ID2BusinessBook],[RagioneSociale],[PartitaIVA],[S1_Indirizzo]";
                sqlQuery += " ,[S1_Numero],[S1_Citta],[S1_Comune],[S1_Cap],[S1_ProvinciaEstesa],[S1_ProvinciaSigla]";
                sqlQuery += " ,[S1_ID2Nazione],[S2_Indirizzo],[S2_Numero],[S2_Citta],[S2_Comune],[S2_Cap],[S2_ProvinciaEstesa]";
                sqlQuery += " ,[S2_ProvinciaSigla],[S2_ID2Nazione],[Telefono1],[Telefono2],[Fax],[WebSite]";
                sqlQuery += " ,[RecordNewUser],[RecordNewDate],[ZeusJolly1],[ZeusJolly2],[ImportCode])";
                sqlQuery += " VALUES ";
                sqlQuery += " (@UserId,@ID2BusinessBook,@RagioneSociale,@PartitaIVA,@S1_Indirizzo";
                sqlQuery += " ,@S1_Numero,@S1_Citta,@S1_Comune,@S1_Cap,@S1_ProvinciaEstesa,@S1_ProvinciaSigla";
                sqlQuery += " ,@S1_ID2Nazione,@S2_Indirizzo,@S2_Numero,@S2_Citta,@S2_Comune,@S2_Cap,@S2_ProvinciaEstesa";
                sqlQuery += " ,@S2_ProvinciaSigla,@S2_ID2Nazione,@Telefono1,@Telefono2,@Fax,@WebSite";
                sqlQuery += " ,@RecordNewUser,@RecordNewDate,@ZeusJolly1,@ZeusJolly2,@ImportCode)";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@UserId"].Value = UserGUID;

                command.Parameters.Add("@ID2BusinessBook", System.Data.SqlDbType.Int);
                command.Parameters["@ID2BusinessBook"].Value = 0;

                command.Parameters.Add("@RagioneSociale", System.Data.SqlDbType.NVarChar);
                command.Parameters["@RagioneSociale"].Value = RagioneSociale;

                command.Parameters.Add("@PartitaIVA", System.Data.SqlDbType.NVarChar);
                command.Parameters["@PartitaIVA"].Value = PartitaIVA;

                command.Parameters.Add("@S1_Indirizzo", System.Data.SqlDbType.NVarChar);
                command.Parameters["@S1_Indirizzo"].Value = S1_Indirizzo;

                command.Parameters.Add("@S1_Numero", System.Data.SqlDbType.NVarChar);
                command.Parameters["@S1_Numero"].Value = S1_Numero;

                command.Parameters.Add("@S1_Citta", System.Data.SqlDbType.NVarChar);
                command.Parameters["@S1_Citta"].Value = S1_Citta;

                command.Parameters.Add("@S1_Comune", System.Data.SqlDbType.NVarChar);
                command.Parameters["@S1_Comune"].Value = S1_Comune;

                command.Parameters.Add("@S1_Cap", System.Data.SqlDbType.NVarChar);
                command.Parameters["@S1_Cap"].Value = S1_Cap;

                command.Parameters.Add("@S1_ProvinciaEstesa", System.Data.SqlDbType.NVarChar);
                command.Parameters["@S1_ProvinciaEstesa"].Value = S1_ProvinciaEstesa;

                command.Parameters.Add("@S1_ProvinciaSigla", System.Data.SqlDbType.NVarChar);
                command.Parameters["@S1_ProvinciaSigla"].Value = S1_ProvinciaSigla;

                command.Parameters.Add("@S1_ID2Nazione", System.Data.SqlDbType.Int);
                command.Parameters["@S1_ID2Nazione"].Value = 1;


                command.Parameters.Add("@S2_Indirizzo", System.Data.SqlDbType.NVarChar);
                command.Parameters["@S2_Indirizzo"].Value = S2_Indirizzo;

                command.Parameters.Add("@S2_Numero", System.Data.SqlDbType.NVarChar);
                command.Parameters["@S2_Numero"].Value = S2_Numero;

                command.Parameters.Add("@S2_Citta", System.Data.SqlDbType.NVarChar);
                command.Parameters["@S2_Citta"].Value = S2_Citta;

                command.Parameters.Add("@S2_Comune", System.Data.SqlDbType.NVarChar);
                command.Parameters["@S2_Comune"].Value = S2_Comune;

                command.Parameters.Add("@S2_Cap", System.Data.SqlDbType.NVarChar);
                command.Parameters["@S2_Cap"].Value = S2_Cap;

                command.Parameters.Add("@S2_ProvinciaEstesa", System.Data.SqlDbType.NVarChar);
                command.Parameters["@S2_ProvinciaEstesa"].Value = S2_ProvinciaEstesa;

                command.Parameters.Add("@S2_ProvinciaSigla", System.Data.SqlDbType.NVarChar);
                command.Parameters["@S2_ProvinciaSigla"].Value = S2_ProvinciaSigla;

                command.Parameters.Add("@S2_ID2Nazione", System.Data.SqlDbType.Int);
                command.Parameters["@S2_ID2Nazione"].Value = 1;

                command.Parameters.Add("@Telefono1", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Telefono1"].Value = Telefono1;

                command.Parameters.Add("@Telefono2", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Telefono2"].Value = Telefono2;

                command.Parameters.Add("@Fax", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Fax"].Value = Fax;

                command.Parameters.Add("@WebSite", System.Data.SqlDbType.NVarChar);
                command.Parameters["@WebSite"].Value = Website;

                command.Parameters.Add("@RecordNewUser", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@RecordNewUser"].Value = CreatorGUID;

                command.Parameters.Add("@RecordNewDate", System.Data.SqlDbType.SmallDateTime);
                command.Parameters["@RecordNewDate"].Value = DateTime.Now;

                command.Parameters.Add("@ZeusJolly1", System.Data.SqlDbType.Int);
                command.Parameters["@ZeusJolly1"].Value = Convert.ToInt32(Jolly1Societario);

                command.Parameters.Add("@ZeusJolly2", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ZeusJolly2"].Value = Jolly2Societario;

                command.Parameters.Add("@ImportCode", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ImportCode"].Value = ImportCodeHiddenField.Value;
                command.ExecuteNonQuery();


                command.Parameters.Clear();
                sqlQuery = "SET DATEFORMAT dmy; INSERT INTO [tbProfiliMarketing]";
                sqlQuery += " ([UserId],[ID2Categoria1],[ConsensoDatiPersonali],[ConsensoDatiPersonali2],[Comunicazioni1]";
                sqlQuery += ",[Comunicazioni2],[Comunicazioni3],[Comunicazioni4],[Comunicazioni5],[Comunicazioni6]";
                sqlQuery += ",[Comunicazioni7],[Comunicazioni8],[Comunicazioni9],[Comunicazioni10],[RecordNewUser]";
                sqlQuery += ",[RecordNewDate],[ZeusJolly1],[ZeusJolly2],[ImportCode],[RegCausal],[Meta_ZeusSearch])";
                sqlQuery += " VALUES ";
                sqlQuery += " (@UserId,@ID2Categoria1,@ConsensoDatiPersonali,@ConsensoDatiPersonali2,@Comunicazioni1";
                sqlQuery += " ,@Comunicazioni2,@Comunicazioni3,@Comunicazioni4,@Comunicazioni5,@Comunicazioni6";
                sqlQuery += " ,@Comunicazioni7,@Comunicazioni8,@Comunicazioni9,@Comunicazioni10,@RecordNewUser";
                sqlQuery += " ,@RecordNewDate,@ZeusJolly1,@ZeusJolly2,@ImportCode,'IMA',@Meta_ZeusSearch)";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@UserId"].Value = UserGUID;

                command.Parameters.Add("@ID2Categoria1", System.Data.SqlDbType.Int);
                command.Parameters["@ID2Categoria1"].Value = 0;

                command.Parameters.Add("@ConsensoDatiPersonali", System.Data.SqlDbType.Bit);
                command.Parameters["@ConsensoDatiPersonali"].Value = Convert.ToBoolean(ConsensoDatiPersonali);

                command.Parameters.Add("@ConsensoDatiPersonali2", System.Data.SqlDbType.Bit);
                command.Parameters["@ConsensoDatiPersonali2"].Value = Convert.ToBoolean(ConsensoDatiPersonali2);

                command.Parameters.Add("@Comunicazioni1", System.Data.SqlDbType.Bit);
                command.Parameters["@Comunicazioni1"].Value = Convert.ToBoolean(Comunicazioni1);

                command.Parameters.Add("@Comunicazioni2", System.Data.SqlDbType.Bit);
                command.Parameters["@Comunicazioni2"].Value = Convert.ToBoolean(Comunicazioni2);

                command.Parameters.Add("@Comunicazioni3", System.Data.SqlDbType.Bit);
                command.Parameters["@Comunicazioni3"].Value = Convert.ToBoolean(Comunicazioni3);

                command.Parameters.Add("@Comunicazioni4", System.Data.SqlDbType.Bit);
                command.Parameters["@Comunicazioni4"].Value = Convert.ToBoolean(Comunicazioni4);

                command.Parameters.Add("@Comunicazioni5", System.Data.SqlDbType.Bit);
                command.Parameters["@Comunicazioni5"].Value = Convert.ToBoolean(Comunicazioni5);

                command.Parameters.Add("@Comunicazioni6", System.Data.SqlDbType.Bit);
                command.Parameters["@Comunicazioni6"].Value = Convert.ToBoolean(Comunicazioni6);

                command.Parameters.Add("@Comunicazioni7", System.Data.SqlDbType.Bit);
                command.Parameters["@Comunicazioni7"].Value = Convert.ToBoolean(Comunicazioni7);

                command.Parameters.Add("@Comunicazioni8", System.Data.SqlDbType.Bit);
                command.Parameters["@Comunicazioni8"].Value = Convert.ToBoolean(Comunicazioni8);

                command.Parameters.Add("@Comunicazioni9", System.Data.SqlDbType.Bit);
                command.Parameters["@Comunicazioni9"].Value = Convert.ToBoolean(Comunicazioni9);

                command.Parameters.Add("@Comunicazioni10", System.Data.SqlDbType.Bit);
                command.Parameters["@Comunicazioni10"].Value = Convert.ToBoolean(Comunicazioni10);

                command.Parameters.Add("@RecordNewUser", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@RecordNewUser"].Value = CreatorGUID;

                command.Parameters.Add("@RecordNewDate", System.Data.SqlDbType.SmallDateTime);
                command.Parameters["@RecordNewDate"].Value = DateTime.Now;

                command.Parameters.Add("@ZeusJolly1", System.Data.SqlDbType.Int);
                command.Parameters["@ZeusJolly1"].Value = Convert.ToInt32(Jolly1Marketing);

                command.Parameters.Add("@ZeusJolly2", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ZeusJolly2"].Value = Jolly2Marketing;


                command.Parameters.Add("@ImportCode", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ImportCode"].Value = ImportCodeHiddenField.Value;

                command.Parameters.Add("@Meta_ZeusSearch", System.Data.SqlDbType.NVarChar);
                command.Parameters["@Meta_ZeusSearch"].Value = Meta_ZeusSearch;
                
                command.ExecuteNonQuery();

                transaction.Commit();

                return true;
            }
            catch (Exception p)
            {

                InfoLabel.Text = p.ToString();

                if (transaction != null)
                {
                    transaction.Rollback();
                    Membership.DeleteUser(UserName);
                }
                return false;
            }
        }//fine using
    }//fine CreateAllUsers


   
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">

    <script language="javascript">
       
       
       var isLoadButton=false;
       
        window.onbeforeunload = function() {
        
         if(isLoadButton)
         {
            $.blockUI({
                message: '<h1><img src="/SiteImg/wait.gif" /> Caricamento...</h1>',
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: '.5',
                    color: '#fff'
                }
            });//fine blockUIz
          }//fine if
        }//fine function
        
        function SetIsLoadButtonToTrue()
        {
          isLoadButton=true;
        }

        
    </script>

    <asp:HiddenField ID="TitleField" runat="server" />
    <asp:HiddenField ID="CheckedColumnsHiddenField" runat="server" />
    <asp:HiddenField ID="PathHiddenField" runat="server" />
    <asp:HiddenField ID="ImportCodeHiddenField" runat="server" />
    <asp:Label ID="INFO" runat="server" skinid="CorpoTesto" Text=" Il foglio di lavoro Excel (il TAB in basso) si deve chiamare IMPORT.<br />Questa versione richiede che sul Server sia installato il componente 2010 Office System Driver Data Connectivity Component - Access Database Engine. (http://www.microsoft.com/it-it/download/details.aspx?id=13255)"></asp:Label>
    <br />
    <table>
        <tr>
            <td>
                <%--   <dlc:dlLoadFile ID="dlLoadFile1" runat="server" FileTypeRange="xls" SavePath="~/ZeusInc/TempDataImport/"
                    ShowInfo="false" />--%>
                <asp:FileUpload ID="FileUpload1" runat="server" />            </td>
            <td>
                <asp:Button ID="LoadButton" runat="server" Text="Carica" OnClick="LoadButton_Click" />            </td>
        </tr>
    </table>
    <uc1:ImportAccount_ExcelInfo ID="ImportAccount_ExcelInfo1" runat="server" />
    <br />
    <asp:Label ID="InfoLabel" runat="server" SkinID="Validazione01"></asp:Label>
    <br />
    <asp:Button ID="VerificaButton" runat="server" Text="Verifica file caricato" OnClick="VerificaButton_Click"
        Visible="false" />
    <br />
    <asp:Label ID="ImportCodeLabel" runat="server" SkinID="GridTitolo1" Visible="false"></asp:Label>
    <br />
    <asp:Label ID="FinishLabel" runat="server" SkinID="CorpoTesto"  Font-Bold="true" Visible="false"></asp:Label>
    <asp:Button ID="CreateUserButton" runat="server" Text="Crea utenti" OnClick="CreateUserButton_Click"
        Visible="false" />
</asp:Content>
