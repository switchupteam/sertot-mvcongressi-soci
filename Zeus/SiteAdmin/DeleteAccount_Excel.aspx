﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">
  
    static string TitoloPagina = "Delete excel account";



    protected void Page_Init()
    {
        string firstScript = "/SiteJS/jquery-1.2.6.js";
        string secondScript = "/SiteJS/jQuery.BlockUI.js";

        HtmlGenericControl Include = new HtmlGenericControl("script");
        Include.Attributes.Add("type", "text/javascript");
        Include.Attributes.Add("src", firstScript);
        this.Page.Header.Controls.Add(Include);

        Include = new HtmlGenericControl("script");
        Include.Attributes.Add("type", "text/javascript");
        Include.Attributes.Add("src", secondScript);
        this.Page.Header.Controls.Add(Include);


        DeleteButton.Attributes.Add("onfocus", "javascript:return SetIsLoadButtonToTrue();");

    }//fine Page_Init

    protected void Page_Load(object sender, EventArgs e)
    {

        TitleField.Value = TitoloPagina;

    }//fine Page_Load



    private bool DeleteUsers(string ImportCode)
    {



        if (ImportCode.Length <= 0)
            return false;

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        try
        {

            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();


                sqlQuery = "SELECT UserID FROM tbProfiliPersonali WHERE ImportCode='" + ImportCode + "'";
                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();


                if (!reader.HasRows)
                {
                    reader.Close();
                    FinalLabel.Text = "Nessun utente trovato con il codice di importazione " + ImportCode;
                    return false;
                }

                int index = 0;

                while (reader.Read())
                    if (reader["UserID"] != DBNull.Value)
                    {
                        try
                        {
                            Membership.DeleteUser(Membership.GetUser(reader.GetGuid(0)).UserName);
                            ++index;
                        }
                        catch (Exception)
                        { }
                    }

                reader.Close();
                FinalLabel.Text += index + " utenti eliminati";
                return true;

            }//fine Using
        }
        catch (Exception p)
        {

            InfoLabel.Text = p.ToString();
            return false;
        }


    }//fine DeleteUsers

    protected void DeleteButton_Click(object sender, EventArgs e)
    {

        InfoLabel.Text = string.Empty;
        FinalLabel.Text = string.Empty;
        
        if ((Page.IsValid)
            && (CodeTextBox.Text.Length > 0))
        {

            string ImportCode = CodeTextBox.Text;


            if (DeleteUsers(ImportCode))
            {

                String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
                string sqlQuery = string.Empty;

                using (SqlConnection connection = new SqlConnection(
                   strConnessione))
                {

                    SqlTransaction transaction = null;

                    try
                    {


                        connection.Open();
                        SqlCommand command = connection.CreateCommand();

                        transaction = connection.BeginTransaction();
                        command.Transaction = transaction;
                        sqlQuery = "DELETE FROM [tbProfiliPersonali] WHERE ImportCode='" + ImportCode + "'";
                        command.CommandText = sqlQuery;
                        command.ExecuteNonQuery();
                        sqlQuery = "DELETE FROM [tbProfiliSocietari] WHERE ImportCode='" + ImportCode + "'";
                        command.CommandText = sqlQuery;
                        command.ExecuteNonQuery();
                        sqlQuery = "DELETE FROM [tbProfiliMarketing] WHERE ImportCode='" + ImportCode + "'";
                        command.CommandText = sqlQuery;
                        command.ExecuteNonQuery();
                        transaction.Commit();

                        FinalLabel.Text += "<br />Operazione terminata con successo.";


                    }
                    catch (Exception p)
                    {

                        InfoLabel.Text = p.ToString();

                        if (transaction != null)
                            transaction.Rollback();

                    }
                }//fine using
            }//fine if

        }//fine if

    }//fine DeleteButton_Click
    
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">

    <script language="javascript">
       
       
       var isLoadButton=false;
       
        window.onbeforeunload = function() {
        
         if((isLoadButton)
         &&(document.getElementById("<%=CodeTextBox.ClientID%>").value.length>0))
         {
            $.blockUI({
                message: '<h1><img src="/SiteImg/wait.gif" /> Eliminazione utenti in corso...</h1>',
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: '.5',
                    color: '#fff'
                }
            });//fine blockUIz
          }//fine if
        }//fine function
        
        function SetIsLoadButtonToTrue()
        {
          isLoadButton=true;
        }

        
    </script>

    <asp:HiddenField ID="TitleField" runat="server" />
    <asp:Label ID="Label1" runat="server" SkinID="CorpoTesto">IMPORT CODE:</asp:Label>
    <asp:TextBox ID="CodeTextBox" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Obbligatorio"
        runat="server" ControlToValidate="CodeTextBox" SkinID="ZSSM_Validazione01" Display="Dynamic">
    </asp:RequiredFieldValidator>
    <asp:Label ID="InfoLabel" runat="server" SkinID="Validazione01"></asp:Label>
    <asp:Button ID="DeleteButton" runat="server" Text="Elimina utenti" OnClick="DeleteButton_Click"
        SkinID="ZSSM_Button01" />
    <br />
    <asp:Label ID="FinalLabel" runat="server" SkinID="CorpoTesto"></asp:Label>
</asp:Content>
