﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Async="true" Title="Untitled Page"
    Theme="Zeus" %>


<%@ Import Namespace="System.Web.Security" %>
<%@ Import Namespace="System.Web.UI" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Net.Mail" %>

<script runat="server">

    static string TitoloPagina = "Elenco soci";

    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();


        if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "UID"))
        {
            SingleRecordSqlDataSource.SelectCommand += " WHERE UserId='" + Server.HtmlEncode(Request.QueryString["UID"]) + "'";
            GridView2.DataSourceID = "SingleRecordSqlDataSource";
            GridView2.DataBind();
            SingleRecordPanel.Visible = true;
        }


        //        dsElencoUtenti.SelectCommand += " ORDER BY UserName";

        //Response.Write("DEB "+dsElencoUtenti.SelectCommand+"<br />");

        TitleField.Value = TitoloPagina;



    }//fine Page_Load


    private string GetLang()
    {
        string Lang = "ITA";

        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;

    }//fine GetLang



    protected void ImgIco_DataBinding(object sender, EventArgs e)
    {
        Image Ico = (Image)sender;
        if (Ico.ImageUrl == "0")
        {
            Ico.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_Off.gif";
        }
        if (Ico.ImageUrl == "1")
        {
            Ico.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";
        }
    }//fine ImgIco_DataBinding

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.DataItemIndex > -1)
        {
            Image Image1 = (Image)e.Row.FindControl("Image1");
            Label DataVerifica = (Label)e.Row.FindControl("DataVerifica");
            Label TipoSocio = (Label)e.Row.FindControl("TipoSocio");
            try
            {
                if (DataVerifica.Text.ToString().Length > 0 || TipoSocio.Text.Equals("Onorario"))
                {
                    Image1.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";
                }


                //CONTROLLI PULSANTE SOLLECITO EMAIL PER ASSOCIAZIONE SCADUTA
                var LinkShowModal = (HyperLink)e.Row.FindControl("LinkShowModal");
                var idUtente = (HiddenField)e.Row.FindControl("IdUtente");
                bool isAssociazioneValida = false;

                String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(strConnessione))
                {
                    try
                    {
                        connection.Open();
                        SqlCommand command = connection.CreateCommand();

                        string sqlQuery = "SELECT DataVerifica FROM tbQuoteAnnuali WHERE UserID=@UserId";
                        command.CommandText = sqlQuery;
                        command.Parameters.Add("@UserID", System.Data.SqlDbType.VarChar);
                        command.Parameters["@UserID"].Value = idUtente.Value;

                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            string dataVerifica = reader["DataVerifica"].ToString();
                            isAssociazioneValida = !string.IsNullOrWhiteSpace(dataVerifica) && DateTime.Parse(dataVerifica) < DateTime.Now;
                        }

                        reader.Close();
                    }
                    catch (Exception p)
                    {
                        Response.Write(p.ToString());
                    }
                }

                if (!isAssociazioneValida && DateTime.Now >= new DateTime(2019, 2, 1))
                {
                    var nome = (Label)e.Row.FindControl("Nome");
                    var cognome = (Label)e.Row.FindControl("Cognome");
                    var email = (HiddenField)e.Row.FindControl("EmailUtente");

                    LinkShowModal.Attributes.Add("onclick", string.Format("ShowModal('{0}', '{1}', '{2}', '{3}')", idUtente.Value, nome.Text, cognome.Text, email.Value));
                }
                else
                {
                    LinkShowModal.Visible = false;
                }
            }
            catch (Exception ex)
            {
            }
        }
    }



    protected void ConfermaInvioEmail_LinkButton_Click(object sender, EventArgs e)
    {
        var buttonParent = ((LinkButton)sender).Parent;

        var idUtente = (HiddenField)buttonParent.FindControl("idUtenteHiddenModal");
        var emailUtente = (HiddenField)buttonParent.FindControl("emailHiddenModal");


        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        using (SqlConnection connection = new SqlConnection(strConnessione))
        {
            connection.Open();
            SqlCommand command = connection.CreateCommand();

            string sqlQuery = @"SELECT Cognome, Nome, UserName
                                FROM tbProfiliPersonali
                                    INNER JOIN aspnet_Users ON aspnet_Users.UserId = tbProfiliPersonali.UserId
                                WHERE tbProfiliPersonali.UserId = @UserId";
            command.CommandText = sqlQuery;
            command.Parameters.Add("@UserId", System.Data.DbType.Guid);
            command.Parameters["@UserId"].Value = idUtente.Value;

            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    try
                    {
                        System.Net.Configuration.SmtpSection section = ConfigurationManager.GetSection("system.net/mailSettings/smtp") as System.Net.Configuration.SmtpSection;

                        MailMessage mail = new MailMessage();
                        mail.To.Add(new MailAddress(emailUtente.Value));
                        //mail.Bcc.Add(new MailAddress("web@switchup.it"));
                        mail.From = new MailAddress(section.From, "Sito Web Sertot");
                        mail.Subject = "Rinnovo quota associazione Sertot";
                        mail.IsBodyHtml = true;

                        var body = string.Empty;
                        try
                        {
                            body = "Spett.le" + reader["Nome"] + " " + reader["Cognome"];
                            body += "<br />";
                            body += "<strong>La sua associazione, che risponde al codice " + reader["UserName"] + ", è in in scadenza o è già scaduta.</strong>";
                            body += "<br />";
                            body += "<br />";
                            body += "La invitiamo a rinnovarla accedendo tramite il seguente link";
                            body += "<br />";
                            body += "<a href=\"" + HttpContext.Current.Request.Url.Host + "/AreaSoci/GestioneAssociazione.aspx\">Rinnovo associazione</a>";
                            body += "<br />";
                            body += "<br />";
                            body += "Per qualsiasi comunicazione/esigenza, oltre ai <strong>Coordinatori regionali</strong>, può fare riferimento alla Segreteria (<a href='mailto:sertot@mvcongressi.it'>sertot@mvcongressi.it</a>) che si incarica, se del caso, di inoltrare le sue richieste in modo mirato agli Organismi dell’Associazione.";
                            body += "<br />";
                            body += "<br />";
                            body += "Un cordiale saluto";
                            body += "<br />";
                            body += "<br />";
                            body += "LA SEGRETERIA Sertot";
                        }
                        catch (Exception ex)
                        {
                            Response.Write(ex.ToString());
                            return;
                        }

                        mail.Body = body;
                        SmtpClient client = new SmtpClient();
                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                        client.Port = 587;
                        client.EnableSsl = true;
                        client.SendAsync(mail, string.Empty);






                        //MOSTRO IL MESSAGGIO DI CORRETTO INVIO
                        //emailInviataConSuccessoModal.Visible = true;
                        //var emailInviataConSuccesso = (HtmlGenericControl)buttonParent.FindControl("emailInviataModal");
                        //emailInviataConSuccesso.Visible = true;



                        //MOSTRO IL MESSAGGIO DI CORRETTO INVIO
                        //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "emailInviataSuccess", "alert('Successo!');", true);
                    }
                    catch (Exception exep)
                    {
                        Response.Write(exep.ToString());
                        return;
                    }
                }
            }
        }
    }
</script>


<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="ZeusAreaHead">
    <script type="text/javascript">
        //It'svery important that showPopup() is outside of jQuery document load event
        function ShowModal(idUtente, nomeUtente, cognomeUtente, emailUtente) {

            $('[id$="emailInviataConSuccessoModal"]').hide();

            $('[id$="idUtenteHiddenModal"]').val(idUtente);

            $('[id$="nominativoHiddenModal"]').val(nomeUtente + " " + cognomeUtente);
            $('[id$="nominativoModal"]').html(nomeUtente + " " + cognomeUtente);

            $('[id$="emailHiddenModal"]').val(emailUtente);
            $('[id$="emailModal"]').html(emailUtente);

            $('#modalInvioEmailAssociazioneScaduta').modal('show');
        }

        function ShowModalSuccess(idUtente, nomeUtente, cognomeUtente, emailUtente) {
            $('#modalEmailInviataConSuccesso').modal('show');
        }

    </script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <asp:Panel ID="SingleRecordPanel" runat="server" Visible="false">
        <div class="LayGridTitolo1">
            <asp:Label ID="CaptionNewRecordLabel" SkinID="GridTitolo1" runat="server" Text="Ultimo utente aggiornato"></asp:Label>
        </div>
        <asp:GridView ID="GridView2" runat="server" AllowPaging="True" AllowSorting="True" OnRowDataBound="GridView1_RowDataBound"
            PageSize="100" AutoGenerateColumns="False" EmptyDataText="Nessun record di dati da visualizzare."
            DataKeyNames="UserId">
            <Columns>
                <asp:TemplateField HeaderText="Cognome" SortExpression="Cognome">
                    <ItemStyle HorizontalAlign="Left" />
                    <ItemTemplate>
                        <asp:Label ID="Cognome" runat="server" Text='<%# Eval("Cognome") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Nome" SortExpression="Nome">
                    <ItemStyle HorizontalAlign="Left" />
                    <ItemTemplate>
                        <asp:Label ID="Nome" runat="server" Text='<%# Eval("Nome") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="UserName" HeaderText="UserName" SortExpression="UserName">
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>

                <asp:BoundField DataField="CognomeNome" HeaderText="Cognome Nome" SortExpression="CognomeNome">
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="RagioneSociale" HeaderText="Rag. Soc." SortExpression="RagioneSociale">
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="ProvinciaSigla" HeaderText="Prov." SortExpression="ProvinciaSigla">
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="Anno" HeaderText="Quota" SortExpression="Anno">
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="AnnoProgressivo" HeaderText="Progressivo" SortExpression="AnnoProgressivo">
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="DataVerifica" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="DataVerifica" runat="server" Text='<%# Eval("DataVerifica") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Pagamento" SortExpression="DataVerifica">
                    <ItemTemplate>
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="E-Mail">
                    <ItemTemplate>
                        <asp:HyperLink ID="HyperLink1" runat="server" SkinID="FieldValue" NavigateUrl='<%# Eval("Email", "mailto:{0}") %>'
                            Text='<%# Eval("Email", "{0}") %>'></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="LastLoginDate" HeaderText="Ultimo accesso" SortExpression="LastLoginDate">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:HyperLinkField DataNavigateUrlFields="UserID" DataNavigateUrlFormatString="Soci_Dsp.aspx?UID={0}"
                    HeaderText="Account" Text="Dettaglio">
                    <ItemStyle HorizontalAlign="Center" />
                    <ControlStyle CssClass="GridView_ZeusButton1" />
                </asp:HyperLinkField>

                <%--                <asp:HyperLinkField DataNavigateUrlFields="UserID" DataNavigateUrlFormatString="/Zeus/SiteAssets/InvioAvvisoScadenzaAssociazione.ascx?UID={0}"
                    HeaderText="Avviso scadenza" Text="Invio" ControlStyle-CssClass="use-ajax">
                    <ItemStyle HorizontalAlign="Center" />
                    <ControlStyle CssClass="GridView_ZeusButton1" />
                </asp:HyperLinkField>--%>

                <asp:TemplateField HeaderText="Avviso scadenza">
                    <ItemStyle HorizontalAlign="Center" />
                    <ItemTemplate>
                        <asp:HyperLink runat="server" ID="LinkShowModal" CssClass="GridView_ZeusButton1" NavigateUrl="#" Text="Invia" />
                        <%--onClick="ShowRowDetail();"--%>
                        <%--<a href="#" runat="server" class="GridView_ZeusButton1 LinkShowModal" data-toggle="modal" data-target="#modalInvioEmailAssociazioneScaduta" data-userid="<%# Eval("UserID") %>" />--%>

                        <asp:HiddenField runat="server" ID="IdUtente" Value='<%# Eval("UserID") %>' />
                        <asp:HiddenField runat="server" ID="EmailUtente" Value='<%# Eval("Email") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SingleRecordSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
            SelectCommand="SELECT UserID, Email, LastLoginDate, UserName, CognomeNome, ProvinciaSigla, DataVerifica, Anno, AnnoProgressivo,
            RagioneSociale FROM vwSoci_Lst "></asp:SqlDataSource>
        <div class="VertSpacerMedium">
        </div>
    </asp:Panel>
    <div class="LayGridTitolo1">
        <asp:Label ID="CaptionallRecordLabel" runat="server" SkinID="GridTitolo1" Text="Elenco Soci"></asp:Label>
    </div>
    <asp:GridView ID="UserList_GridView" runat="server" AllowPaging="True" AllowSorting="True" OnRowDataBound="GridView1_RowDataBound"
        PageSize="100" AutoGenerateColumns="False" DataSourceID="dsElencoUtenti" EmptyDataText="Nessun record di dati da visualizzare."
        DataKeyNames="UserId">
        <Columns>
            <asp:TemplateField HeaderText="Cognome" SortExpression="Cognome">
                <ItemStyle HorizontalAlign="Left" />
                <ItemTemplate>
                    <asp:Label ID="Cognome" runat="server" Text='<%# Eval("Cognome") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Nome" SortExpression="Nome">
                <ItemStyle HorizontalAlign="Left" />
                <ItemTemplate>
                    <asp:Label ID="Nome" runat="server" Text='<%# Eval("Nome") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:BoundField DataField="CodiceSocioStr" HeaderText="Codice Socio" SortExpression="CodiceSocioStr">
                <ItemStyle HorizontalAlign="left" />
            </asp:BoundField>

            <asp:BoundField DataField="UserName" HeaderText="Username" SortExpression="UserName">
                <ItemStyle HorizontalAlign="left" />
            </asp:BoundField>

            <asp:BoundField DataField="RagioneSociale" HeaderText="Azienda" SortExpression="RagioneSociale">
                <ItemStyle HorizontalAlign="Left" />
            </asp:BoundField>

            <asp:BoundField DataField="ProvinciaSigla" HeaderText="Prov." SortExpression="ProvinciaSigla">
                <ItemStyle HorizontalAlign="center" />
            </asp:BoundField>

            <asp:BoundField DataField="Anno" HeaderText="Quota" SortExpression="Anno">
                <ItemStyle HorizontalAlign="center" />
            </asp:BoundField>

            <asp:BoundField DataField="AnnoProgressivo" HeaderText="Progressivo" SortExpression="AnnoProgressivo">
                <ItemStyle HorizontalAlign="center" />
            </asp:BoundField>

            <asp:TemplateField HeaderText="DataVerifica" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="DataVerifica" runat="server" Text='<%# Eval("DataVerifica") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Socio" Visible="True">
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                <ItemTemplate>
                    <asp:Label ID="TipoSocio" runat="server" Text='<%# Eval("TipoSocio") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Verif. Pagam" SortExpression="DataVerifica">
                <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>

            <asp:BoundField DataField="LastLoginDate" HeaderText="Ultimo accesso" SortExpression="LastLoginDate">
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:HyperLinkField DataNavigateUrlFields="UserID" DataNavigateUrlFormatString="Soci_Dsp.aspx?UID={0}"
                HeaderText="Dettaglio" Text="Dettaglio">
                <ItemStyle HorizontalAlign="Center" />
                <ControlStyle CssClass="GridView_ZeusButton1" />
            </asp:HyperLinkField>
            <%--            <asp:HyperLinkField DataNavigateUrlFields="UserID" DataNavigateUrlFormatString="/Zeus/SiteAssets/InvioAvvisoScadenzaAssociazione.ascx?UID={0}"
                HeaderText="Avviso scadenza" Text="Invio">
                <ItemStyle HorizontalAlign="Center" />
                <ControlStyle CssClass="GridView_ZeusButton1" />
            </asp:HyperLinkField>--%>
            <asp:TemplateField HeaderText="Avviso scadenza">
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:HyperLink runat="server" ID="LinkShowModal" CssClass="GridView_ZeusButton1" NavigateUrl="#" Text="Invia" />
                    <%--onClick="ShowRowDetail();"--%>
                    <%--<a href="#" runat="server" class="GridView_ZeusButton1 LinkShowModal" data-toggle="modal" data-target="#modalInvioEmailAssociazioneScaduta" data-userid="<%# Eval("UserID") %>" />--%>

                    <asp:HiddenField runat="server" ID="IdUtente" Value='<%# Eval("UserID") %>' />
                    <asp:HiddenField runat="server" ID="EmailUtente" Value='<%# Eval("Email") %>' />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="dsElencoUtenti" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="WITH tbSociTemp AS (
	                    SELECT vwSoci_UltimoPagamento.UserID, Email, LastLoginDate, UserName, Cognome, Nome, Anno, AnnoProgressivo, ProvinciaSigla, RagioneSociale, DataVerifica
		                    , CASE WHEN TipoSocio = 'ORD' THEN 'Ordinario' ELSE 'Aggregato' END AS TipoSocio
		                    , P_MARK.CodiceSocioStr
	                    FROM vwSoci_UltimoPagamento 
		                    INNER JOIN aspnet_UsersInRoles AS USER_ROLE ON USER_ROLE.UserId = vwSoci_UltimoPagamento.UserId AND USER_ROLE.RoleId = '38561367-FFC2-4FE2-BBA1-A5D38FB39D0D'
		                    INNER JOIN tbProfiliMarketing AS P_MARK ON P_MARK.UserId = vwSoci_UltimoPagamento.UserId
	                    WHERE (UserName LIKE '%' + @UserName + '%') AND (Email LIKE '%' + @Email + '%') 
	                        AND (Cognome LIKE '%' + @Cognome + '%') AND (Nome LIKE '%' + @Nome + '%')
	                    EXCEPT
	                    SELECT vwSoci_UltimoPagamento.UserID, Email, LastLoginDate, UserName, Cognome, Nome, Anno, AnnoProgressivo, ProvinciaSigla, RagioneSociale, DataVerifica
		                    , CASE WHEN TipoSocio = 'ORD' THEN 'Ordinario' ELSE 'Aggregato' END AS TipoSocio
		                    , P_MARK.CodiceSocioStr
	                    FROM vwSoci_UltimoPagamento 
		                    INNER JOIN aspnet_UsersInRoles AS USER_ROLE ON USER_ROLE.UserId = vwSoci_UltimoPagamento.UserId AND USER_ROLE.RoleId = 'AE5D5F05-2193-43A8-92AD-8CDFD1CBB778'
		                    INNER JOIN tbProfiliMarketing AS P_MARK ON P_MARK.UserId = vwSoci_UltimoPagamento.UserId
	                    WHERE (UserName LIKE '%' + @UserName + '%') AND (Email LIKE '%' + @Email + '%') 
	                        AND (Cognome LIKE '%' + @Cognome + '%') AND (Nome LIKE '%' + @Nome + '%')
	                    UNION
	                    SELECT MEMBER.UserId, MEMBER.Email, MEMBER.LastLoginDate, UTENTE.UserName, P_PERS.Cognome, P_PERS.Nome, NULL, NULL, P_SOC.S1_ProvinciaSigla, P_SOC.RagioneSociale, NULL
		                    , 'Onorario' AS TipoSocio, P_MARK.CodiceSocioStr
	                    FROM aspnet_Membership AS MEMBER
		                    INNER JOIN aspnet_Users AS UTENTE ON UTENTE.UserId = MEMBER.UserId
		                    INNER JOIN tbProfiliPersonali AS P_PERS ON P_PERS.UserId = MEMBER.UserId
		                    INNER JOIN tbProfiliSocietari AS P_SOC ON P_SOC.UserId = MEMBER.UserId
		                    INNER JOIN aspnet_UsersInRoles AS USER_ROLE ON USER_ROLE.UserId = MEMBER.UserId AND USER_ROLE.RoleId = 'AE5D5F05-2193-43A8-92AD-8CDFD1CBB778'
		                    INNER JOIN tbProfiliMarketing AS P_MARK ON P_MARK.UserId = MEMBER.UserId
	                    WHERE (UserName LIKE '%' + @UserName + '%') AND (Email LIKE '%' + @Email + '%') 
	                        AND (Cognome LIKE '%' + @Cognome + '%') AND (Nome LIKE '%' + @Nome + '%')
                    )
                    SELECT UserID, Email, LastLoginDate, UserName, Cognome, Nome, Anno, AnnoProgressivo, ProvinciaSigla, RagioneSociale, DataVerifica
	                    , TipoSocio
	                    , CodiceSocioStr
                    FROM tbSociTemp
                    UNION
                    SELECT P_MARK.UserId, MEMBER.Email, MEMBER.LastLoginDate, UTENTE.UserName, P_PERS.Cognome, P_PERS.Nome, NULL, NULL, P_SOC.S1_ProvinciaSigla, P_SOC.RagioneSociale, NULL
                        , CASE WHEN USER_ROLE.RoleId = '38561367-FFC2-4FE2-BBA1-A5D38FB39D0D' THEN 'Ordinario' ELSE 'Aggregato' END AS TipoSocio, P_MARK.CodiceSocioStr
                    FROM tbProfiliMarketing AS P_MARK
	                    INNER JOIN aspnet_Users AS UTENTE ON UTENTE.UserId = P_MARK.UserId
	                    INNER JOIN tbProfiliPersonali AS P_PERS ON P_PERS.UserId = P_MARK.UserId
	                    INNER JOIN tbProfiliSocietari AS P_SOC ON P_SOC.UserId = P_MARK.UserId
	                    INNER JOIN aspnet_UsersInRoles AS USER_ROLE ON USER_ROLE.UserId = P_MARK.UserId  
	                    INNER JOIN aspnet_Membership AS MEMBER ON MEMBER.UserId = P_MARK.UserId	
                    WHERE P_MARK.UserId NOT IN (SELECT UserId FROM tbSociTemp) AND P_MARK.StatusSocio = 'SOC' 
	                    AND (UserName LIKE '%' + @UserName + '%') AND (Email LIKE '%' + @Email + '%') 
	                    AND (Cognome LIKE '%' + @Cognome + '%') AND (Nome LIKE '%' + @Nome + '%')
                    ORDER BY Cognome">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="%" Name="UserName" QueryStringField="XRE1"
                Type="String" />
            <asp:QueryStringParameter DefaultValue="%" Name="EMail" QueryStringField="XRE2"
                Type="String" />
            <asp:QueryStringParameter DefaultValue="%" Name="Cognome" QueryStringField="XRE3"
                Type="String" />
            <asp:QueryStringParameter DefaultValue="%" Name="Nome" QueryStringField="XRE4" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>


    <%--MODAL--%>
    <div id="modalInvioEmailAssociazioneScaduta" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalInvioEmailAssociazioneScaduta" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Invio email associazione scaduta</h4>
                </div>
                <div class="modal-body">
                    <asp:HiddenField runat="server" ID="idUtenteHiddenModal"></asp:HiddenField>
                    <asp:HiddenField runat="server" ID="nominativoHiddenModal"></asp:HiddenField>
                    <asp:HiddenField runat="server" ID="emailHiddenModal"></asp:HiddenField>
                    <p>
                        Confermi di voler inviare tramite email la notifica di scadenza della quota di associazione a 
                        <asp:Label runat="server" ID="nominativoModal"></asp:Label>-
                        <asp:Label runat="server" ID="emailModal"></asp:Label>
                        ?
                    </p>
                </div>
                <div class="modal-footer">
                    <asp:LinkButton runat="server" class="btn btn-primary" ID="ConfermaInvioEmail_LinkButton" OnClick="ConfermaInvioEmail_LinkButton_Click">Conferma</asp:LinkButton>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
                </div>
            </div>
        </div>
    </div>


    <asp:Panel runat="server" ID="emailInviataConSuccessoModal">
        <div id="modalEmailInviataConSuccesso" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalInvioEmailAssociazioneScaduta" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Invio email associazione scaduta</h4>
                    </div>
                    <div class="modal-body">
                        <h5>Email di sollecito inviata correttamente.</h5>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
