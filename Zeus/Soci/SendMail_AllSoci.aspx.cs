﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Zeus_Account_SendMail_AllSoci : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        /*****************************************************/
        /**************** INIZIO SEO *************************/
        /*****************************************************/
        HtmlMeta description = new HtmlMeta();
        HtmlMeta keywords = new HtmlMeta();
        description.Name = "description";
        keywords.Name = "keywords";
        Page.Title = "Invio Email | " + AresConfig.NomeSito;
        description.Content = "Invio Email";
        keywords.Content = "Invio Email";
        HtmlHead head = Page.Header;
        head.Controls.Add(description);
        head.Controls.Add(keywords);
        /*****************************************************/
        /**************** FINE SEO ***************************/
        /*****************************************************/
    }

    protected void InviaEmailButton_Click(object sender, EventArgs e)
    {
        ProcessoCompletatoPlaceholder.Visible = false;
        EmailNonInviateLabel.Visible = false;

        string strConnessione = ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        try
        {

            using (SqlConnection connection = new SqlConnection(strConnessione))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                //GET TUTTI I SOCI E SOCI ONORARI
                string sqlQuery = @"SELECT DISTINCT UserName
                                    FROM aspnet_Membership	
                                        INNER JOIN aspnet_Users ON aspnet_Users.UserId = aspnet_Membership.UserId 
                                        INNER JOIN aspnet_UsersInRoles ON aspnet_UsersInRoles.UserId = aspnet_Membership.UserId 
                                            AND aspnet_UsersInRoles.RoleId IN ('38561367-FFC2-4FE2-BBA1-A5D38FB39D0D', 'AE5D5F05-2193-43A8-92AD-8CDFD1CBB778')";

                //string sqlQuery = @"SELECT DISTINCT UserName
                //                    FROM aspnet_Membership	
                //                        INNER JOIN aspnet_Users ON aspnet_Users.UserId = aspnet_Membership.UserId 
                //                    WHERE Username IN ('l.cundari', 'p.bianchi')";

                command.CommandText = sqlQuery;

                List<string> emailNonInviate = new List<string>();

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    if (!string.IsNullOrWhiteSpace(reader["UserName"].ToString()))
                    {
                        MembershipUser myUser = Membership.GetUser(reader["UserName"].ToString());

                        bool mailSent = false;
                        mailSent |= SendMailAutenticazioneSocio(myUser.Email, myUser.UserName, myUser.GetPassword(), myUser.IsApproved);

                        if (!mailSent)
                            emailNonInviate.Add(myUser.Email);
                    }
                }

                reader.Close();
                connection.Close();


                //STAMPO LE EMAIL NON INVIATE
                ProcessoCompletatoPlaceholder.Visible = true;
                if (emailNonInviate.Count() > 0)
                    EmailNonInviateLabel.Visible = true;
                EmailNonInviateRepeater.DataSource = emailNonInviate;
                EmailNonInviateRepeater.DataBind();

            }//fine Using
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }


    private bool SendMailAutenticazioneSocio(string email, string username, string password, bool accessoAreaRiservata)
    {
        // ------------- AppRif= "APP", approvato - "RIF", rifiutato ---------------------
        try
        {
            System.Net.Configuration.SmtpSection section = ConfigurationManager.GetSection("system.net/mailSettings/smtp") as System.Net.Configuration.SmtpSection;

            MailMessage mail = new MailMessage();

            mail.To.Add(new MailAddress(email));
            mail.From = new MailAddress(section.From, "Sito Web Società Emiliano Romagnola Triveneta di Ortopedia e Traumatologia ");

            mail.Subject = "Dati di accesso al sito web Società Emiliano Romagnola Triveneta di Ortopedia e Traumatologia ";

            mail.IsBodyHtml = true;
            string myBody = string.Empty;
            try
            {

                string strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
                string strUrl = HttpContext.Current.Request.Url.AbsoluteUri.Replace(strPathAndQuery, "/");

                myBody += "I dati per accedere al sito <a href=\"" + strUrl + "\">Società Emiliano Romagnola Triveneta di Ortopedia e Traumatologia </a> sono i seguenti:";
                myBody += "<br />";
                myBody += "<br />";
                myBody += "Nome utente: " + username;
                myBody += "<br />";
                myBody += "<br />";
                myBody += "Password: " + password;
                myBody += "<br />";
                myBody += "<br />";
                myBody += "Indirizzo E-Mail associato: " + email;
                myBody += "<br />";
                myBody += "<br />";
                myBody += "Accesso aree riservate: " + (accessoAreaRiservata == true ? "consentito" : "negato");
            }
            catch (Exception exep)
            {

            }


            mail.Body = myBody;
            SmtpClient client = new SmtpClient();
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.Port = 587;
            client.EnableSsl = true;
            client.SendAsync(mail, string.Empty);

            return true;
        }
        catch (Exception p)
        {

            Response.Write(p.ToString());
            return false;
        }
    }
}