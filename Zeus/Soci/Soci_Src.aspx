﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" %>
    
    <%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">

    static string TitoloPagina = "Ricerca soci";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;
        mymySetFocus(SearchButton.UniqueID);

        
    }// Page_Load





    private void mymySetFocus(string ID)
    {
        Page.Form.DefaultButton = ID;
        Page.Form.DefaultFocus = ID;

    }//fine mymySetFocus

    protected void SearchButton_Click(object sender, EventArgs e)
    {

        string myRedirect = "Soci_Lst.aspx?XRE1=" + UsarNameTextBox.Text.ToString() +
            "&XRE2=" + EMailTextBox.Text.ToString() +
            "&XRE3=" + CognomeTextBox.Text +
            "&XRE4=" + NomeTextBox.Text;


        
        Response.Redirect(myRedirect);
        
    }//fine SearchButton_Click
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
   <div class="BlockBox">
   <div class="BlockBoxHeader"><asp:Label ID="Ricerca_Label" runat="server" Text="Ricerca soci"></asp:Label></div>
<table>
    
  <tr>
            <td class="BlockBoxDescription">
                <asp:Label ID="Cognome_Label" runat="server"  SkinID="FieldDescription" Text="Cognome"></asp:Label>
            </td>
            <td class="BlockBoxValue">
                <asp:TextBox ID="CognomeTextBox" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="BlockBoxDescription">
                <asp:Label ID="Nome_Label" runat="server"  SkinID="FieldDescription" Text="Nome"></asp:Label>
            </td>
            <td class="BlockBoxValue">
                <asp:TextBox ID="NomeTextBox" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="BlockBoxDescription">
                <asp:Label ID="UserName_Label" runat="server" SkinID="FieldDescription" Text="UserName"></asp:Label>
            </td>
            <td class="BlockBoxValue">
                <asp:TextBox ID="UsarNameTextBox" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="BlockBoxDescription">
                <asp:Label ID="EMail_Label" runat="server"  SkinID="FieldDescription" Text="E-Mail"></asp:Label>
            </td>
            <td class="BlockBoxValue">
                <asp:TextBox ID="EMailTextBox" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
            </td>
        </tr>
   
        <tr><td colspan="2"></td></tr>
        <tr><td class="BlockBoxDescription"></td><td class="BlockBoxValue"><asp:LinkButton ID="SearchButton" SkinID="ZSSM_Button01" runat="server" Text="Cerca" OnClick="SearchButton_Click"></asp:LinkButton></td></tr>
    </table></div>
</asp:Content>
