﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" Theme="Zeus" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<%@ Register Src="~/Zeus/Pagamenti/Pagamenti_Lst.ascx" TagName="Pagamenti_Lst" TagPrefix="uc2" %>

<%@ Register Src="~/Zeus/Soci/Soci_Dsp.ascx" TagName="Soci_Dsp" TagPrefix="uc2" %>

<script runat="server">

    static string TitoloPagina = "Dettaglio Socio";

    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;
        DisableButtonByRole();

        delinea myDelinea = new delinea();

        if (!myDelinea.AntiSQLInjection(Request.QueryString))
            Response.Redirect("~/Zeus/System/Message.aspx");

        ProfiloMarketing_Dtl.UID = Server.HtmlEncode(Request.QueryString["UID"]);
        ProfiloPersonale_Dtl1.UID = Server.HtmlEncode(Request.QueryString["UID"]);
        ProfiloSocietario_Dtl.UID = Server.HtmlEncode(Request.QueryString["UID"]);
        PagamentiNewLinkButton.PostBackUrl = PagamentiNewLinkButton.PostBackUrl + "?UID=" + Server.HtmlEncode(Request.QueryString["UID"]);

        if (!IsAlive(Server.HtmlEncode(Request.QueryString["UID"])))
        {
            ProfiloPersonaleLinkButton.Visible = false;
            ProfiloMarketingLinkButton.Visible = false;
            ProfiloSocietarioLinkButton.Visible = false;

        }//fine if


    }//fine Page_Load


    private bool IsAlive(string myUserID)
    {

        bool myIsAlive = true;
        try
        {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            string sqlQuery = "SELECT aspnet_Users.UserName";
            sqlQuery += " FROM aspnet_Membership INNER JOIN";
            sqlQuery += " aspnet_Users ON aspnet_Membership.UserId = aspnet_Users.UserId";
            sqlQuery += " WHERE     (aspnet_Users.UserId = '" + myUserID + "')";



            using (SqlConnection conn = new SqlConnection(strConnessione))
            {
                SqlCommand command = new SqlCommand(sqlQuery, conn);

                conn.Open();
                SqlDataReader reader = command.ExecuteReader();
                /*fino a quando ci sono record*/

                if (!reader.HasRows)
                    myIsAlive = false;

                reader.Close();

            }//fine using

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }

        return myIsAlive;


    }//fine IsAlive


    private bool DisableButtonByRole()
    {
        try
        {
            delinea myDelinea = new delinea();

            if (myDelinea.CheckIsAdminManaged(Server.HtmlEncode(Request.QueryString["UID"])))
            {
                ProfiloPersonaleLinkButton.Enabled = false;
                ProfiloMarketingLinkButton.Enabled = false;
                ProfiloSocietarioLinkButton.Enabled = false;

            }

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }

    }//fine DisableButtonByRole


    protected void ProfiloPersonaleLinkButton_Load(object sender, EventArgs e)
    {
        LinkButton ProfiloPersonaleLinkButton = (LinkButton)sender;
        ProfiloPersonaleLinkButton.PostBackUrl += "?UID=" + Request.QueryString["UID"].ToString();
    }

    protected void ProfiloMarketingLinkButton_Load(object sender, EventArgs e)
    {
        LinkButton ProfiloMarketingLinkButton = (LinkButton)sender;
        ProfiloMarketingLinkButton.PostBackUrl += "?UID=" + Request.QueryString["UID"].ToString();
    }

    protected void ProfiloSocietarioLinkButton_Load(object sender, EventArgs e)
    {
        LinkButton ProfiloSocietarioLinkButton = (LinkButton)sender;
        ProfiloSocietarioLinkButton.PostBackUrl += "?UID=" + Request.QueryString["UID"].ToString();
    }
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <asp:HiddenField ID="UID" runat="server" Visible="False" />
    <table border="0" cellpadding="2" cellspacing="1" style="width: 100%">
        <tr>
            <td valign="top" colspan="3" style="width: 100%">
                <div style="margin-bottom: 10px;">
                    <asp:LinkButton ID="PagamentiNewLinkButton" runat="server"
                        PostBackUrl="~/Zeus/Pagamenti/Pagamenti_New.aspx" Text="Nuova Quota" SkinID="ZSSM_Button01"></asp:LinkButton>
                </div>
                <uc2:Pagamenti_Lst ID="Pagamenti_Lst1" runat="server" />
                <br />
            </td>
        </tr>
        <tr>
            <td valign="top" style="width: 50%">

                <!--PARTE DEGLI USERCONTROL-->
                <div class="BlockBox">
                    <dlc:ProfiloPersonale_Dtl ID="ProfiloPersonale_Dtl1" runat="server" />
                    <div style="text-align: center">
                        <asp:LinkButton ID="ProfiloPersonaleLinkButton" runat="server" SkinID="ZSSM_Button01"
                            Text="Modifica" PostBackUrl="~/Zeus/Account/Account_Edt_Pers.aspx" OnLoad="ProfiloPersonaleLinkButton_Load"></asp:LinkButton>
                    </div>
                    <div class="VertSpacerMedium">
                    </div>
                </div>
                <div class="BlockBox">
                    <dlc:ProfiloSocietario_Dtl ID="ProfiloSocietario_Dtl" runat="server" />
                    <div style="text-align: center">
                        <asp:LinkButton ID="ProfiloSocietarioLinkButton" SkinID="ZSSM_Button01" runat="server"
                            Text="Modifica" PostBackUrl="~/Zeus/Account/Account_Edt_Soc.aspx" OnLoad="ProfiloSocietarioLinkButton_Load"></asp:LinkButton>
                    </div>
                    <div class="VertSpacerMedium">
                    </div>
                </div>
                <div class="BlockBox">
                    <dlc:ProfiloMarketing_Dtl ID="ProfiloMarketing_Dtl" runat="server" />
                    <div style="text-align: center">
                        <asp:LinkButton ID="ProfiloMarketingLinkButton" runat="server" OnLoad="ProfiloMarketingLinkButton_Load"
                            PostBackUrl="~/Zeus/Account/Account_Edt_Pref.aspx" Text="Modifica" SkinID="ZSSM_Button01"></asp:LinkButton>
                    </div>
                    <div class="VertSpacerMedium">
                    </div>
                </div>
                <!--FINE PARTE DEGLI USERCONTROL-->

            </td>

            <td style="width: 10px;"></td>
            <td valign="top" style="width: 50%">


                <!--PARTE DELLE MEMBERSHIP-->
                <uc2:Soci_Dsp ID="Account_Dtl1" runat="server" />
                <!--FINE PARTE DELLE MEMBERSHIP-->
            </td>
        </tr>
    </table>
    <!-- Version 3.01.001-->
</asp:Content>


