﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" Async="true" Theme="Zeus" %>

<%@ Import Namespace="System.Web.Security" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Net.Mail" %>
<%@ Import Namespace="System.Transactions" %>
<%@ Import Namespace="System.IO" %>


<script runat="server">

    static string TitoloPagina = "Ricerca soci";
    delinea myDelinea = new delinea();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "UID"))
            Response.Redirect("/Zeus/System/Message.aspx?0");


    }// Page_Load

    private string GetEMailByUID(string _UID)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        String sqlQuery = string.Empty;
        string EMail = "";

        using (SqlConnection connection = new SqlConnection(strConnessione))
        {

            connection.Open();
            SqlCommand command = connection.CreateCommand();
            sqlQuery = "SELECT EMail FROM vwAccount_Lst_All WHERE UserID = '" + _UID.ToString() + "'";

            command.CommandText = sqlQuery;
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                EMail = reader["EMail"].ToString();
            }
            reader.Close();

            return EMail;

        }//fine Using

    } // Fine GetEMailByUID

    private bool SendMail(MembershipUserCollection AllUsers, string Email, string AppRif)
    {
        // ------------- AppRif= "APP", approvato - "RIF", rifiutato ---------------------
        try
        {
            System.Net.Configuration.SmtpSection section = ConfigurationManager.GetSection("system.net/mailSettings/smtp") as System.Net.Configuration.SmtpSection;

            MailMessage mail = new MailMessage();

            // ------------- se il destinatario è LISTA allora spedisco all'elenco destinatati previsti
            //if (Email == "LISTA")
            //{
            //    mail.To.Add(new MailAddress("support@switchup.it"));
            //}
            //else
            //{
            mail.To.Add(new MailAddress(Email));
            //}

            // Hop: 20170328 aggiunto per "log"
            //mail.Bcc.Add(new MailAddress("web@switchup.it"));
            mail.From = new MailAddress(section.From, "Sito Web Sertot");

            if (AppRif == "APP")
            {
                mail.Subject = "Conferma associazione Sertot";
            }
            else
            {
                mail.Subject = "Sito Web Sertot - Iscrizione rifiutata";
            }

            mail.IsBodyHtml = true;
            string myBody = string.Empty;

            if (AppRif == "APP")
            {

                foreach (MembershipUser myUser in AllUsers)
                {
                    string tipo = string.Empty;
                    string year = string.Empty;

                    String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
                    string sqlQuery = string.Empty;

                    using (SqlConnection connection = new SqlConnection(strConnessione))
                    {

                        connection.Open();
                        SqlCommand command = connection.CreateCommand();

                        sqlQuery = @"SELECT tbQuote.Descrizione, tbQuoteAnnuali.Anno 
                                    FROM tbQuoteAnnuali 
                                        JOIN tbQuote ON tbQuote.IDQuota = tbQuoteAnnuali.ID2QuotaCosto
                                    WHERE UserID = @UserID";
                        command.CommandText = sqlQuery;
                        command.Parameters.Add("@UserID", System.Data.SqlDbType.UniqueIdentifier);
                        command.Parameters["@UserID"].Value = myUser.ProviderUserKey;

                        SqlDataReader reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            tipo = reader["Descrizione"].ToString();
                            year = (Convert.ToInt32(reader["Anno"].ToString())).ToString();
                        }

                        reader.Close();
                        connection.Close();

                    }//fine Using

                    try
                    {
                        myBody = "Gent.mo,";
                        myBody += "<br />";
                        myBody += "<br />";
                        myBody += "<strong>benvenuto in Sertot, in qualità di SOCIO " + tipo + " per l'anno " + year + ".</strong>";
                        myBody += "<br />";
                        myBody += "Il suo codice Socio è: " + myUser.UserName;
                        myBody += "<br />";
                        myBody += "Per completare la procedura ti invitiamo a versare la quota annuale corrispondente:";
                        myBody += "<br />";
                        myBody += "<br />";
                        myBody += "<ul>";
                        myBody += "<li>";
                        myBody += "Socio 2020 - 50 €";
                        myBody += "</li>";
                        myBody += "<li>";
                        myBody += "Socio 2021 - 50 €";
                        myBody += "</li>";
                        myBody += "</ul>";
                        myBody += "<br />";
                        myBody += "Ti invitiamo ad effettuare il pagamento della quota entro 7 giorni dalla ricezione della presente, solo in questo modo potrai perfezionare la tua iscrizione.";
                        myBody += "<br />";
                        myBody += "<br />";
                        myBody += "Il pagamento potrà essere effettuato tramite:";
                        myBody += "<br />";
                        myBody += "<br />";
                        myBody += "<br />";
                        myBody += "<ul>";
                        myBody += "<li>";

                        string strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
                        string strUrl = HttpContext.Current.Request.Url.AbsoluteUri.Replace(strPathAndQuery, "/");
                        myBody += "<a href=\"" + strUrl + "AreaSoci/PayPal\">Paypal</a>";

                        myBody += "</li>";
                        myBody += "<li>";
                        myBody += "Bonifico bancario Intestato a: SERTOT Banca: CARIPARMA - AG. 1 PARMA IBAN: IT67 G 06230 12701 000084208891 Causale: Nome e Cognome + Quota associativa sertot + Anno. In caso di pagamento tramite bonifico bancario è necessario inviarne copia a <a href=\"mailto:sertot@mvcongressi.it\">sertot@mvcongressi.it</a>";
                        myBody += "</li>";
                        myBody += "</ul>";
                        myBody += "<br />";
                        myBody += "<br />";
                        myBody += "Per poterti contattare ed informarti sulle iniziative <strong>è necessario che compili il Tuo profilo di socio e che lo stesso sia periodicamente aggiornato, soprattutto nella parte riguardante i recapiti mail e telefonici.</strong>";
                        myBody += "<br />";
                        myBody += "<br />";
                        myBody += "Per accedere all'Area Soci del sito web dovrai utilizzare le seguenti credenziali:";
                        myBody += "<br />";
                        myBody += "<br />";
                        myBody += "UserName: " + myUser.UserName;
                        myBody += "<br />";
                        myBody += "Password: " + myUser.GetPassword();
                        myBody += "<br />";
                        myBody += "Indirizzo E-Mail associato: " + myUser.Email;
                        myBody += "<br />";
                        myBody += "<br />";
                        myBody += "Lo status di Socio Sertot Ti permetterà di accedere a tutti i benefit dedicati ";
                        myBody += "<br />";
                        myBody += "<br />";
                        myBody += "<br />";
                        myBody += "Per qualsiasi informazione la preghiamo di non rispondere alla presente mail ma di contattarci alla mail <a href=\"mailto:sertot@mvcongressi.it\">sertot@mvcongressi.it</a>";
                        myBody += "<br />";
                        myBody += "<br />";
                        myBody += "Grazie";
                        myBody += "<br />";
                        myBody += "La segreteria della Società Emiliano Romagnola Triveneta di Ortopedia e Traumatologia ";
                    }
                    catch (System.Web.Security.MembershipPasswordException)
                    {
                    }

                }//fine foreach


            }
            else
            {
                myBody += "La richiesta di associazione Sertot è stata rifiutata.";
                myBody += "<br />";
                myBody += "<br />";
                myBody += "Per qualsiasi informazione la preghiamo di non rispondere alla presente mail ma di contattarci alla mail <a href=\"mailto:Sertot@mvcongrassi.it\">Sertot@mvcongrassi.it</a>";
                myBody += "<br /><br />";
                myBody += "Cordiali saluti";
                myBody += "<br /><br />";
                myBody += "La segreteria della Società Emiliano Romagnola Triveneta di Ortopedia e Traumatologia ";
            }


            mail.Body = myBody;
            SmtpClient client = new SmtpClient();
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.Port = 587;
            client.EnableSsl = true;
            client.SendAsync(mail, string.Empty);

            return true;
        }
        catch (Exception p)
        {

            Response.Write(p.ToString());
            return false;
        }

    }//fine SendMail


    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Zeus");
    }

    protected void UpdateButton_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            DropDownList DropDownListSocio = (DropDownList)FormView1.FindControl("DropDownListSocio");
            HiddenField StatusSocioHiddenField = (HiddenField)FormView1.FindControl("StatusSocioHiddenField");
            HiddenField DataRifiutoSocioHiddenField = (HiddenField)FormView1.FindControl("DataRifiutoSocioHiddenField");
            HiddenField DataAccettazioneSocioHiddenField = (HiddenField)FormView1.FindControl("DataAccettazioneSocioHiddenField");

            DateTime Data = new DateTime();
            Data = DateTime.Now;

            if (DropDownListSocio.SelectedItem.Value.Equals("1"))
            {

                StatusSocioHiddenField.Value = "SOC";
                DataAccettazioneSocioHiddenField.Value = Data.GetDataItaliana();

            }
            else
            {
                StatusSocioHiddenField.Value = "RIF";
                DataRifiutoSocioHiddenField.Value = Data.GetDataItaliana();
            }
        }
    }

    protected void SqlDataSourceAccett_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.AffectedRows == 0 || e.Exception != null)
            Response.Redirect("/System/Message.aspx?UpdateErr");

        DropDownList DropDownListSocio = (DropDownList)FormView1.FindControl("DropDownListSocio");


        // ------------------ Al salvataggio controllo se ho approvato o meno ( DropDownListSocio.SelectedItem.Value.Equals("1") è approvato --------
        // ------------------ Mando la mail con i contenuti a seconda dell'approvazione o meno

        string EMail = GetEMailByUID(Request.QueryString["UID"]);
        var utente = Membership.GetUser(Membership.GetUserNameByEmail(EMail));

        if (DropDownListSocio.SelectedItem.Value.Equals("1"))
        {
            //INSERIMENTO CODICE PROGRESSIVO SOCIO
            SertotUtility c = new SertotUtility();
            string nuovoCodiceSocio = c.NuovoCodiceSocio();

            SqlTransaction SQLTx = null;
            using (TransactionScope txScope = new TransactionScope())
            {
                try
                {
                    object userGUID = utente.ProviderUserKey;

                    String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(strConnessione))
                    {
                        connection.Open();
                        SqlCommand SQLCmd = connection.CreateCommand();

                        SQLTx = connection.BeginTransaction();
                        SQLCmd.Transaction = SQLTx;

                        SQLCmd.Parameters.Clear();
                        InsertCodiceProfiliMarketing(ref SQLCmd, userGUID, nuovoCodiceSocio);
                        SQLCmd.ExecuteNonQuery();

                        SQLTx.Commit();
                    }//fine using

                    txScope.Complete();
                }
                catch (Exception ex)
                {
                    if (SQLTx != null)
                        SQLTx.Rollback();

                    throw;
                }
            }
                                
            SendMail(Membership.FindUsersByEmail(EMail), EMail, "APP");
            // ------------ se mando a LISTA allora automaticamente invio alla lista prevista
            //SendMail(Membership.FindUsersByEmail(EMail), "LISTA", "APP");
        }
        else
        {
            var utenti = Membership.FindUsersByEmail(EMail);

            SqlTransaction SQLTx = null;
            using (TransactionScope txScope = new TransactionScope())
            {
                try
                {
                    object userGUID = utente.ProviderUserKey;

                    String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(strConnessione))
                    {
                        connection.Open();
                        SqlCommand SQLCmd = connection.CreateCommand();

                        SQLTx = connection.BeginTransaction();
                        SQLCmd.Transaction = SQLTx;

                        SQLCmd.Parameters.Clear();
                        DeleteProfiliPersonali(ref SQLCmd, userGUID);
                        SQLCmd.ExecuteNonQuery();

                        SQLCmd.Parameters.Clear();
                        DeleteProfiliSocietari(ref SQLCmd, userGUID);
                        SQLCmd.ExecuteNonQuery();

                        SQLCmd.Parameters.Clear();
                        DeleteProfiliMarketing(ref SQLCmd, userGUID);
                        SQLCmd.ExecuteNonQuery();

                        SQLCmd.Parameters.Clear();
                        DeleteQuotaAnnuale(ref SQLCmd, userGUID);
                        SQLCmd.ExecuteNonQuery();

                        SQLTx.Commit();
                    }//fine using

                    txScope.Complete();
                }
                catch (Exception ex)
                {
                    if (SQLTx != null)
                        SQLTx.Rollback();

                    throw;
                }


                SendMail(utenti, EMail, "RIF");
                // ------------ se mando a LISTA allora automaticamente invio alla lista prevista
                //SendMail(Membership.FindUsersByEmail(EMail), "LISTA", "RIF");
            }
            Membership.DeleteUser(utente.UserName, true);

        } // PagamentiSqlDataSource_Updated


        if (DropDownListSocio.SelectedItem.Value.Equals("1"))
        {
            try
            {
                HiddenField CodiceSocioStrHiddenField = (HiddenField)FormView1.FindControl("CodiceSocioStrHiddenField");
                Roles.AddUserToRole(utente.UserName, "Soci");
            }
            catch (Exception ex)
            {
                //L'UTENTE HA GIA' ASSOCIATO IL RUOLO DI SOCIO, QUINDI NON FACCIO NULLA
            }

            Response.Redirect("/Zeus/System/Message.aspx?Msg=7117800456156068");
        }
        else
        {
            Response.Redirect("/Zeus/System/Message.aspx?Msg=7117800456156090");
        }
    }

    protected void DeleteProfiliPersonali(ref SqlCommand SQLCmd, object userGUID)
    {
        string sqlQuery = string.Empty;
        object CreatorGUID = userGUID;

        sqlQuery = "DELETE FROM tbProfiliPersonali WHERE UserId = @UserId";

        SQLCmd.CommandText = sqlQuery;
        SQLCmd.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
        SQLCmd.Parameters["@UserId"].Value = userGUID;
    }
    protected void DeleteProfiliSocietari(ref SqlCommand SQLCmd, object userGUID)
    {
        string sqlQuery = string.Empty;
        object CreatorGUID = userGUID;

        sqlQuery = "DELETE FROM tbProfiliSocietari WHERE UserId = @UserId";

        SQLCmd.CommandText = sqlQuery;
        SQLCmd.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
        SQLCmd.Parameters["@UserId"].Value = userGUID;
    }
    protected void DeleteProfiliMarketing(ref SqlCommand SQLCmd, object userGUID)
    {
        string sqlQuery = string.Empty;
        object CreatorGUID = userGUID;

        sqlQuery = "DELETE FROM tbProfiliMarketing WHERE UserId = @UserId";

        SQLCmd.CommandText = sqlQuery;
        SQLCmd.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
        SQLCmd.Parameters["@UserId"].Value = userGUID;
    }
    protected void DeleteQuotaAnnuale(ref SqlCommand SQLCmd, object userGUID)
    {
        string sqlQuery = string.Empty;
        object CreatorGUID = userGUID;

        sqlQuery = "DELETE FROM tbQuoteAnnuali WHERE UserId = @UserId";

        SQLCmd.CommandText = sqlQuery;
        SQLCmd.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
        SQLCmd.Parameters["@UserId"].Value = userGUID;
    }
    protected void InsertCodiceProfiliMarketing(ref SqlCommand SQLCmd, object userGUID, string codiceSocio)
    {
        string sqlQuery = string.Empty;
        object CreatorGUID = userGUID;

        sqlQuery = @"UPDATE tbProfiliMarketing
                    SET [CodiceSocioNum] = @CodiceSocioNum, [CodiceSocioStr] = @CodiceSocioStr
                    WHERE UserId = @UserId";

        SQLCmd.CommandText = sqlQuery;
        SQLCmd.Parameters.Add("@CodiceSocioNum", System.Data.SqlDbType.Int);
        SQLCmd.Parameters["@CodiceSocioNum"].Value = Convert.ToInt32(codiceSocio);

        SQLCmd.Parameters.Add("@CodiceSocioStr", System.Data.SqlDbType.NVarChar);
        SQLCmd.Parameters["@CodiceSocioStr"].Value = codiceSocio;

        SQLCmd.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
        SQLCmd.Parameters["@UserId"].Value = userGUID;
    }
    protected void InsertSocioRole(ref SqlCommand SQLCmd, object userGUID)
    {
        string sqlQuery = string.Empty;
        object CreatorGUID = userGUID;

        sqlQuery = @"INSERT INTO aspnet_UsersInRoles (UserId, RoleId)
                    VALUES (@UserId, '38561367-FFC2-4FE2-BBA1-A5D38FB39D0D')";

        SQLCmd.CommandText = sqlQuery;
        SQLCmd.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
        SQLCmd.Parameters["@UserId"].Value = userGUID;
    }
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Value="Approvazione/rifiuto Socio" />
    <asp:FormView ID="FormView1" runat="server" DataSourceID="SqlDataSourceAccett" EnableModelValidation="True" DefaultMode="Edit">
        <EditItemTemplate>
            <br />
            <table width="100%">
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label1" SkinID="FieldDescription" runat="server" Text="Socio"></asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:Label ID="Label2" SkinID="FieldDescription" runat="server" Text='<%# Eval("CognomeNome") %>'></asp:Label>
                    </td>

                </tr>
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="NRegistro" SkinID="FieldDescription" runat="server" Text="Stato"></asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:DropDownList AppendDataBoundItems="true" ID="DropDownListSocio" runat="server">
                            <asp:ListItem Text="&gt;  Non inserito" Selected="True" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Approva" Selected="False" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Rifiuta" Selected="False" Value="2"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator" ErrorMessage="Obbligatorio"
                            runat="server" ControlToValidate="DropDownListSocio" SkinID="ZSSM_Validazione01"
                            Display="Dynamic" InitialValue="0">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>

                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="Label5" SkinID="FieldDescription" runat="server" Text="Note interne"></asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <style>
                            .TextBox {
                                height: auto;
                            }
                        </style>
                        <asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine" Rows="5" Columns="50" CssClass="Multiline"
                            Text='<%# Bind("NoteInterne") %>' />
                    </td>
                </tr>

                <tr>
                    <td colspan="2" style="text-align: center">
                        <br />
                        <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" OnClick="UpdateButton_Click"
                            CommandName="Update" Text="Salva dati" SkinID="ZSSM_Button01" />
                        &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server"
                            CausesValidation="False" CommandName="Cancel" Text="Annulla" SkinID="ZSSM_Button01" />
                        &nbsp;<asp:LinkButton ID="LinkButton1" runat="server"
                            CausesValidation="False" Text="Indietro" SkinID="ZSSM_Button01" OnClick="LinkButton1_Click" />
                    </td>

                </tr>
            </table>
            <br />


            <asp:HiddenField ID="DataAccettazioneSocioHiddenField" runat="server" Value='<%# Bind("DataAccettazioneSocio") %>' />
            <asp:HiddenField ID="DataRifiutoSocioHiddenField" runat="server" Value='<%# Bind("DataRifiutoSocio") %>' />
            <asp:HiddenField ID="StatusSocioHiddenField" runat="server" Value='<%# Bind("StatusSocio") %>' />
            <asp:HiddenField ID="CodiceSocioStrHiddenField" runat="server" Value='<%# Eval("CodiceSocioStr") %>' />
        </EditItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="SqlDataSourceAccett" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SELECT  [CognomeNome], [DataAccettazioneSocio], [DataRifiutoSocio], [StatusSocio], [CodiceSocioStr], [NoteInterne]  FROM [vwSoci_Lst] WHERE ([UserId] = @UserId)"
        UpdateCommand="UPDATE [tbProfiliMarketing] SET [StatusSocio] = @StatusSocio, [DataAccettazioneSocio] = @DataAccettazioneSocio, [DataRifiutoSocio] = @DataRifiutoSocio, [NoteInterne] = @NoteInterne WHERE [UserId] = @UserId"
        OnUpdated="SqlDataSourceAccett_Updated">

        <SelectParameters>
            <asp:QueryStringParameter Name="UserId" QueryStringField="UID" DbType="Guid" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="StatusSocio" Type="String" />
            <asp:Parameter Name="NoteInterne" Type="String" />
            <asp:Parameter Name="DataAccettazioneSocio" Type="DateTime" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="DataRifiutoSocio" Type="DateTime" ConvertEmptyStringToNull="true" />
            <asp:QueryStringParameter Name="UserId" QueryStringField="UID" DbType="Guid" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
