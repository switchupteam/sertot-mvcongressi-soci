﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" CodeFile="SendMail_AllSoci.aspx.cs" Inherits="Zeus_Account_SendMail_AllSoci" Title="Invio Email" Theme="Zeus" Async="true" %>


<asp:Content runat="server" ID="Content1" ContentPlaceHolderID="ZeusAreaHead">
</asp:Content>

<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="ZeusContent">
    <asp:Button runat="server" ID="InviaEmailButton" CssClass="ZSSM_Button01_LinkButton" Text="Invia email" OnClick="InviaEmailButton_Click" />

    <br />
    <br />

    <asp:PlaceHolder runat="server" ID="ProcessoCompletatoPlaceholder" Visible="false">
        <asp:Label runat="server" ID="TestoCompletatoLabel" Text="Processo Completato"></asp:Label>

        <br />
        <br />
        <asp:Label runat="server" ID="EmailNonInviateLabel" Visible="false">Email non inviate:</asp:Label>
        <asp:Repeater runat="server" ID="EmailNonInviateRepeater">
            <ItemTemplate>
                <p><%# Container.DataItem.ToString() %></p>
            </ItemTemplate>
        </asp:Repeater>
    </asp:PlaceHolder>
</asp:Content>

<asp:Content runat="server" ID="Content3" ContentPlaceHolderID="ZeusAreaScript">
</asp:Content>
