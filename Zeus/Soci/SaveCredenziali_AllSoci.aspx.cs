﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Zeus_Account_SaveCredenziali_AllSoci : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        /*****************************************************/
        /**************** INIZIO SEO *************************/
        /*****************************************************/
        HtmlMeta description = new HtmlMeta();
        HtmlMeta keywords = new HtmlMeta();
        description.Name = "description";
        keywords.Name = "keywords";
        Page.Title = "Invio Email | " + AresConfig.NomeSito;
        description.Content = "Invio Email";
        keywords.Content = "Invio Email";
        HtmlHead head = Page.Header;
        head.Controls.Add(description);
        head.Controls.Add(keywords);
        /*****************************************************/
        /**************** FINE SEO ***************************/
        /*****************************************************/
    }

    protected void SalvaCrendenzialiButton_Click(object sender, EventArgs e)
    {
        ProcessoCompletatoPlaceholder.Visible = false;
        EmailNonInviateLabel.Visible = false;

        string strConnessione = ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        try
        {

            using (SqlConnection connection = new SqlConnection(strConnessione))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                string sqlQuery = @"DELETE FROM tbSociExport";
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();


                //GET TUTTI I SOCI E SOCI ONORARI
                sqlQuery = @"SELECT DISTINCT UserName
                                    FROM aspnet_Membership	
                                        INNER JOIN aspnet_Users ON aspnet_Users.UserId = aspnet_Membership.UserId 
                                        INNER JOIN aspnet_UsersInRoles ON aspnet_UsersInRoles.UserId = aspnet_Membership.UserId 
                                            AND aspnet_UsersInRoles.RoleId IN ('38561367-FFC2-4FE2-BBA1-A5D38FB39D0D', 'AE5D5F05-2193-43A8-92AD-8CDFD1CBB778')";

                //string sqlQuery = @"SELECT DISTINCT UserName
                //                    FROM aspnet_Membership	
                //                        INNER JOIN aspnet_Users ON aspnet_Users.UserId = aspnet_Membership.UserId 
                //                    WHERE Username IN ('l.cundari', 'p.bianchi')";

                command.CommandText = sqlQuery;

                List<string> emailNonInviate = new List<string>();
                List<MembershipUser> myUsers = new List<MembershipUser>();

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    if (!string.IsNullOrWhiteSpace(reader["UserName"].ToString()))
                        myUsers.Add(Membership.GetUser(reader["UserName"].ToString()));
                }

                reader.Close();


                foreach (var myUser in myUsers)
                {
                    bool isSaved = false;
                    string codFiscale = null;
                    
                    sqlQuery = @"SELECT DISTINCT CodiceFiscale
                                FROM tbProfiliPersonali
                                WHERE UserId = @UserId";
                    command.Parameters.Clear();
                    command.CommandText = sqlQuery;
                    command.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
                    command.Parameters["@UserId"].Value = myUser.ProviderUserKey;

                    reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        if (!string.IsNullOrWhiteSpace(reader["CodiceFiscale"].ToString()))
                            codFiscale = reader["CodiceFiscale"].ToString();
                    }
                    reader.Close();


                    isSaved |= SalvaCredenzialiSocio(connection, myUser.Email, myUser.UserName, codFiscale, myUser.IsApproved, myUser.LastActivityDate);

                    if (!isSaved)
                        emailNonInviate.Add(myUser.Email);
                }


                connection.Close();


                //STAMPO LE EMAIL NON INVIATE
                ProcessoCompletatoPlaceholder.Visible = true;
                if (emailNonInviate.Count() > 0)
                    EmailNonInviateLabel.Visible = true;
                EmailNonInviateRepeater.DataSource = emailNonInviate;
                EmailNonInviateRepeater.DataBind();

            }//fine Using
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }


    private bool SalvaCredenzialiSocio(SqlConnection connection, string email, string username, string password, bool accessoAreaRiservata, DateTime ultimoAccesso)
    {
        try
        {
            SqlCommand commandSql = connection.CreateCommand();

            //GET TUTTI I SOCI E SOCI ONORARI
            string insertSqlQuery = @"INSERT INTO tbSociExport (Email, Username, Password, UltimoAccesso)
                    VALUES (@Email, @Username, @Password, @UltimoAccesso)";

            commandSql.CommandText = insertSqlQuery;
            commandSql.Parameters.Add("@Email", System.Data.SqlDbType.VarChar);
            commandSql.Parameters["@Email"].Value = email;
            commandSql.Parameters.Add("@Username", System.Data.SqlDbType.VarChar);
            commandSql.Parameters["@Username"].Value = username;
            commandSql.Parameters.Add("@Password", System.Data.SqlDbType.VarChar);
            if (!string.IsNullOrWhiteSpace(password))
                commandSql.Parameters["@Password"].Value = password;
            else 
                commandSql.Parameters["@Password"].Value = DBNull.Value;
            commandSql.Parameters.Add("@UltimoAccesso", System.Data.SqlDbType.DateTime);
            commandSql.Parameters["@UltimoAccesso"].Value = ultimoAccesso;

            commandSql.ExecuteNonQuery();

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }
}