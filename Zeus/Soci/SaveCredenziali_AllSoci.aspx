﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" CodeFile="SaveCredenziali_AllSoci.aspx.cs" Inherits="Zeus_Account_SaveCredenziali_AllSoci" Title="Salva crendenziali soci" Theme="Zeus" %>


<asp:Content runat="server" ID="Content1" ContentPlaceHolderID="ZeusAreaHead">
</asp:Content>

<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="ZeusContent">
    <asp:Button runat="server" ID="SalvaCrendenzialiButton" CssClass="ZSSM_Button01_LinkButton" Text="Salva crendenziali" OnClick="SalvaCrendenzialiButton_Click" />

    <br />
    <br />

    <asp:PlaceHolder runat="server" ID="ProcessoCompletatoPlaceholder" Visible="false">
        <asp:Label runat="server" ID="TestoCompletatoLabel" Text="Processo Completato"></asp:Label>

        <br />
        <br />
        <asp:Label runat="server" ID="EmailNonInviateLabel" Visible="false">Soci non salvati:</asp:Label>
        <asp:Repeater runat="server" ID="EmailNonInviateRepeater">
            <ItemTemplate>
                <p><%# Container.DataItem.ToString() %></p>
            </ItemTemplate>
        </asp:Repeater>
    </asp:PlaceHolder>
</asp:Content>

<asp:Content runat="server" ID="Content3" ContentPlaceHolderID="ZeusAreaScript">
</asp:Content>
