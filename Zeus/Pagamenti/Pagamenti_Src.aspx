﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" %>
    
    <%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">

    static string TitoloPagina = "Ricerca soci";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;
        mymySetFocus(SearchButton.UniqueID);

        
    }// Page_Load





    private void mymySetFocus(string ID)
    {
        Page.Form.DefaultButton = ID;
        Page.Form.DefaultFocus = ID;

    }//fine mymySetFocus

    protected void SearchButton_Click(object sender, EventArgs e)
    {

        string myRedirect = "Pagamenti_Lst.aspx?UID=" + DropDownListSocio.SelectedValue.ToString() +
            "&XRE1=" + DropDownListQuota.SelectedValue.ToString() +
            "&XRE2=" + AnnoTextBox.Text +
            "&XRE3=" + ApprovatoCheckBox.Checked.ToString();


        
        Response.Redirect(myRedirect);
        
    }//fine SearchButton_Click
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
   <div class="BlockBox">
   <div class="BlockBoxHeader"><asp:Label ID="Ricerca_Label" runat="server" Text="Ricerca soci"></asp:Label></div>
<table>
    
   <tr>
                        <td class="BlockBoxDescription">
                                <asp:Label ID="SocioLabel" SkinID="FieldDescription" runat="server" Text="Socio"></asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                                                   <asp:DropDownList AppendDataBoundItems="true" ID="DropDownListSocio" runat="server" DataSourceID="dsSocio" DataTextField="CognomeNome" DataValueField="UserId">
                       <asp:ListItem Text="&gt;  Non inserito" Selected="True" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                <asp:SqlDataSource ID="dsSocio" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                    SelectCommand="SELECT [UserId], [CognomeNome] FROM [vwSoci_Lst] ORDER BY [CognomeNome]">
                </asp:SqlDataSource>
                   </td>
                    </tr>

            <tr>
                        <td class="BlockBoxDescription">
                                <asp:Label ID="Label1" SkinID="FieldDescription" runat="server" Text="Quota"></asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                 <asp:DropDownList AppendDataBoundItems="true" ID="DropDownListQuota" runat="server" DataSourceID="dsQuota" DataTextField="Descrizione" DataValueField="IdQuota" >
                              <asp:ListItem Text="&gt;  Non inserito" Selected="True" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                <asp:SqlDataSource ID="dsQuota" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                    SelectCommand="SELECT [IdQuota], [Descrizione] FROM [tbQuote] ORDER BY [Descrizione]">
                </asp:SqlDataSource>
                   </td>
                    </tr>

        <tr>
                        <td class="BlockBoxDescription">
                  <asp:Label ID="Anno" SkinID="FieldDescription" runat="server" Text="Anno"></asp:Label>
                                        </td>
                        <td class="BlockBoxValue">
                <asp:TextBox ID="AnnoTextBox" runat="server" 
                     />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="AnnoTextBox"
                                                SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator Display="Dynamic" SkinID="ZSSM_Validazione01" ID="RegularExpressionValidator10"
                                                runat="server" ControlToValidate="AnnoTextBox" ErrorMessage="Necessario che sia numerico"
                                                ValidationExpression="^[0-9]+$"></asp:RegularExpressionValidator>
                            <asp:RangeValidator runat="server" id="ValidatoreMaxMin" ControlToValidate="AnnoTextBox"
     MinimumValue="2010" MaximumValue="2040" Type="Double" SkinID="ZSSM_Validazione01" Display="Dynamic"
     Text="L'anno deve essere compreso tra 2010 e 2040"  />
                              </td>
                    </tr>
                                    <tr>
                        <td class="BlockBoxDescription">
                  <asp:Label ID="Approvato" SkinID="FieldDescription" runat="server" Text="Approvato"></asp:Label>
                                        </td>
                        <td class="BlockBoxValue">

                <asp:CheckBox ID="ApprovatoCheckBox" runat="server" 
                     />
                               </td>
                    </tr>
        <tr><td colspan="2"></td></tr>
        <tr><td class="BlockBoxDescription"></td><td class="BlockBoxValue"><asp:LinkButton ID="SearchButton" SkinID="ZSSM_Button01" runat="server" Text="Cerca" OnClick="SearchButton_Click"></asp:LinkButton></td></tr>
    </table></div>
</asp:Content>
