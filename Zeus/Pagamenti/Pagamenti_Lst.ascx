﻿<%@ Control Language="C#" ClassName="Pagamenti_Lst" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();

        if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "UID") && (Request.QueryString["UID"].Length > 0))
        {
            SqlDataSourcePagamenti.SelectCommand += " AND UserId='" + Server.HtmlEncode(Request.QueryString["UID"]) + "'";
        }
        if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRE1") && (Request.QueryString["XRE1"].Length > 0))
        {
            SqlDataSourcePagamenti.SelectCommand += " AND ID2Quota=" + Server.HtmlEncode(Request.QueryString["XRE1"]);
        }
        if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRE2") && (Request.QueryString["XRE2"].Length > 0))
        {
            SqlDataSourcePagamenti.SelectCommand += " AND Anno=" + Server.HtmlEncode(Request.QueryString["XRE2"]);
        }
        SqlDataSourcePagamenti.SelectCommand += " ORDER BY Anno DESC";
        if (!Page.IsPostBack)
            GridView1.DataBind();

        if (GridView1.Rows.Count > 0)
        {
            if (GridView1.Rows[0].Cells[0].Text.Equals(DateTime.Now.Year.ToString()))
            {
                GeneraRinnovoPanel.Visible = false;
            }
        }

    } // Page_Load

    protected void BtnModificaPagamento_Click(object sender, EventArgs e)
    {
        Button myButton = (Button)sender;

        Response.Redirect("~/Zeus/Pagamenti/Pagamenti_Edt.aspx?XRI=" + myButton.ToolTip.ToString() + "&UID=" + Request.QueryString["UID"]);

    }  // ModificaDettaglio

    protected void BtnModificaApprova_Click(object sender, EventArgs e)
    {
        string Risp = string.Empty;

        Button myButton = (Button)sender;
        SertotUtility c = new SertotUtility();
        Risp = c.ImpostaApprovato(myButton.ToolTip.ToString());
        //Response.Write(Risp);
        Response.Redirect(Request.RawUrl);

    }  // ImpostaFatturato

    protected void ImgIco_DataBinding(object sender, EventArgs e)
    {
        Image Ico = (Image)sender;
        if (Ico.ImageUrl.ToString().Length > 0)
        {
            Ico.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";
        }
        else
        {
            Ico.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_Off.gif";
        }
    }//fine ImgIco_DataBinding

    protected void GeneraRinnovoButton_Click(object sender, EventArgs e)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;
        string insert = string.Empty;
        string idquota = string.Empty;
        string idmetodopagamento = string.Empty;
        string pagamentoEur = string.Empty;
        Guid uniqueID = new Guid(Server.HtmlEncode(Request.QueryString["UID"]));

        using (SqlConnection connection = new SqlConnection(strConnessione))
        {
            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = @"SELECT IDQuota, ID1MetodoPagamento FROM [vwQuoteAnnuali_Lst] 
                           WHERE ID1QuotaAnnuale>0 AND UserId=@UserID ORDER BY Anno DESC";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@UserID", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@UserID"].Value = uniqueID;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    idquota = reader["IDQuota"].ToString();
                    idmetodopagamento = reader["ID1MetodoPagamento"].ToString();
                }

                reader.Close();

                sqlQuery = "SELECT (QuotaAssociativa+QuotaAssicurativa) as Quota FROM tbQuote WHERE IDQuota=@IDQuota";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@IDQuota", System.Data.SqlDbType.NVarChar);
                command.Parameters["@IDQuota"].Value = idquota;

                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    pagamentoEur = reader["Quota"].ToString();
                }

                reader.Close();

                insert = @"INSERT INTO [tbQuoteAnnuali] ([UserId], [ID2QuotaCosto], [Anno], [ID2MetodoPagamento], 
                         [PagamentoEur],[RecordNewUser],[RecordNewDate], [ZeusIsAlive] ) 
                         VALUES (@User, @ID2QuotaCosto, @Anno, @ID2MetodoPagamento, @PagamentoEur, @RecordNewUser, @RecordNewDate, 'True')";

                command.CommandText = insert;
                command.Parameters.Add("@User", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@User"].Value = uniqueID;
                command.Parameters.Add("@ID2QuotaCosto", System.Data.SqlDbType.Int);
                command.Parameters["@ID2QuotaCosto"].Value = Convert.ToInt32(idquota);
                command.Parameters.Add("@Anno", System.Data.SqlDbType.SmallInt);
                command.Parameters["@Anno"].Value = DateTime.Now.Year;
                command.Parameters.Add("@ID2MetodoPagamento", System.Data.SqlDbType.TinyInt);
                command.Parameters["@ID2MetodoPagamento"].Value = Convert.ToInt32(idmetodopagamento);
                command.Parameters.Add("@PagamentoEur", System.Data.SqlDbType.Decimal);
                command.Parameters["@PagamentoEur"].Value = Convert.ToDecimal(pagamentoEur);
                command.Parameters.Add("@RecordNewUser", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@RecordNewUser"].Value = Membership.GetUser().ProviderUserKey;
                command.Parameters.Add("@RecordNewDate", System.Data.SqlDbType.SmallDateTime);
                command.Parameters["@RecordNewDate"].Value = DateTime.Now;

                command.ExecuteNonQuery();

                connection.Close();
                Response.Redirect("/Zeus/Soci/Soci_Dsp.aspx?UID=" + uniqueID);
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
            }
        }//fine Using
    }

    protected void linkRicevuta_DataBinding(object sender, EventArgs e)
    {
        HyperLink hyperlink = (HyperLink)sender;

        if (!string.IsNullOrWhiteSpace(hyperlink.NavigateUrl))
            hyperlink.NavigateUrl = "/ZeusInc/Account/RicevuteSoci/" + hyperlink.NavigateUrl;
        else
            hyperlink.Visible = false;

    }

</script>

<script type="text/javascript">

    function CheckCondizioni(sender, args) {

        if (document.getElementById("<%=GeneraRinnovoCheckBox.ClientID%>").checked)
            args.IsValid = true;
        else args.IsValid = false;

        return;
    }

</script>

<asp:GridView ID="GridView1" runat="server" DataSourceID="SqlDataSourcePagamenti" EnableModelValidation="True" DataKeyNames="ID1QuotaAnnuale">
    <Columns>

        <asp:BoundField DataField="Anno" HeaderText="Anno" SortExpression="Anno">
            <ItemStyle HorizontalAlign="Left" />
        </asp:BoundField>

        <asp:TemplateField HeaderText="Pag. verif.">
            <ItemTemplate>
                <asp:Image ID="ImgIcoAccesso" runat="server" OnDataBinding="ImgIco_DataBinding" ImageUrl='<%# Eval("DataVerifica", "{0}") %>' />
            </ItemTemplate>
            <ItemStyle HorizontalAlign="Center" />
        </asp:TemplateField>

        <asp:BoundField DataField="DescrizioneQuota" HeaderText="Tipologia associazione" SortExpression="DescrizioneQuota">
            <ItemStyle HorizontalAlign="Left" />
        </asp:BoundField>

        <asp:BoundField DataField="PagamentoEur" HeaderText="Quota EUR " SortExpression="PagamentoEur">
            <ItemStyle HorizontalAlign="Left" />
        </asp:BoundField>

        <asp:BoundField DataField="metodoPagamento" HeaderText="Pagamento" SortExpression="metodoPagamento">
            <ItemStyle HorizontalAlign="Left" />
        </asp:BoundField>

        <asp:BoundField DataField="NoteSocio" HeaderText="Dettagli e note socio" SortExpression="NoteSocio">
            <ItemStyle HorizontalAlign="Left" />
        </asp:BoundField>

        <asp:BoundField DataField="NoteAirespsa" HeaderText="Note interne" SortExpression="NoteAirespsa">
            <ItemStyle HorizontalAlign="Left" />
        </asp:BoundField>

        <%--                  <asp:HyperLinkField DataNavigateUrlFields="IDPagamento" HeaderText="Modifica" DataNavigateUrlFormatString="Pagamenti_Edt.aspx?XRI={0}" 
                    Text="Modifica">
                    <ControlStyle CssClass="GridView_ZeusButton1" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:HyperLinkField>--%>
        <asp:TemplateField HeaderText="Modifica" ItemStyle-HorizontalAlign="Center">
            <ItemTemplate>
                <asp:Button ID="BtnModificaPagamento" runat="server" Text=" Modifica " ToolTip='<%# Eval("ID1QuotaAnnuale") %>' OnClick="BtnModificaPagamento_Click" SkinID="ZSSM_Button01" CssClass="ZSSM_Button01_Button" />
            </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="Ricevute" ItemStyle-HorizontalAlign="Center">
            <ItemTemplate>
                <asp:HyperLink ID="linkRicevuta" runat="server" Text=" Ricevuta " CssClass="ZSSM_Button01_Button" OnDataBinding="linkRicevuta_DataBinding" NavigateUrl='<%# Eval("Ricevuta") %>' Target="_blank"></asp:HyperLink>
            </ItemTemplate>
        </asp:TemplateField>

        <%--<asp:TemplateField HeaderText="Approva" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                
                <asp:Button ID="BtnModificaApprova" runat="server" Text=" Approva "  OnClick="BtnModificaApprova_Click" SkinID="ZSSM_Button01" ToolTip='<%# Eval("ID1QuotaAnnuale") %>' CssClass="ZSSM_Button01_Button"/>
   </ItemTemplate>
                                </asp:TemplateField>--%>
    </Columns>
</asp:GridView>

<%--GENERA RINNOVO BUTTON--%>
<asp:Panel runat="server" ID="GeneraRinnovoPanel">
    <div style="margin-top: 10px;">
        <asp:Button ID="GeneraRinnovoButton" runat="server" Text="Genera Rinnovo" OnClick="GeneraRinnovoButton_Click" SkinID="ZSSM_Button01" CssClass="ZSSM_Button01_Button" ValidationGroup="myValidation" />
        <asp:CheckBox runat="server" ID="GeneraRinnovoCheckBox" Text="Conferma rinnovo associazione" CssClass="LabelSkin_FieldDescription" />
        <asp:CustomValidator CssClass="LabelSkin_FieldDescription" ID="CustomValidator1" runat="server"
            ErrorMessage="Obbligatorio" ClientValidationFunction="CheckCondizioni" ValidationGroup="myValidation"
            Display="Dynamic" ForeColor="Red"></asp:CustomValidator>
    </div>
</asp:Panel>

<asp:SqlDataSource ID="SqlDataSourcePagamenti" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
    SelectCommand="SELECT [ID1QuotaAnnuale], [Anno], [AnnoProgressivo], [DataVerifica], [DescrizioneQuota], [QuotaAssociativa], [QuotaAssicurativa], [TotaleQuota], [metodoPagamento], [PagamentoEur], [NoteSocio], [NoteAirespsa], IDQuota, ID1MetodoPagamento, Ricevuta FROM [vwQuoteAnnuali_Lst] WHERE ID1QuotaAnnuale>0"></asp:SqlDataSource>
