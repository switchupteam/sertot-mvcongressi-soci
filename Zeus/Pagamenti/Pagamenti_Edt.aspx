﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" Theme="Zeus" EnableEventValidation="false" Async="true" %>

<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Net.Mail" %>

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();
        TitleField.Value = "Modifica pagamento";

        if (string.IsNullOrEmpty(Request.QueryString["XRI"]))
            Response.Redirect("~/System/Message.aspx?0");
        XRI.Value = Request.QueryString["XRI"];

        if (!Page.IsPostBack)
        {
            if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "UID") && (Request.QueryString["UID"].Length > 0))
            {
                ((DropDownList)FormView1.FindControl("DropDownListSocio")).SelectedValue = Request.QueryString["UID"].ToString();
                ((DropDownList)FormView1.FindControl("DropDownListSocio")).Enabled = false;
                UID.Value = Request.QueryString["UID"].ToString();
            }
        }


        HiddenField FilenameRicevutaHiddenField = (HiddenField)FormView1.FindControl("FilenameRicevutaHiddenField");
        TextBox AnnoRicevutaTextBox = (TextBox)FormView1.FindControl("AnnoRicevutaTextBox");

        if (!string.IsNullOrWhiteSpace(FilenameRicevutaHiddenField.Value))
            AnnoRicevutaTextBox.Enabled = false;

        //Response.Write(GetPrice(((DropDownList)FormView1.FindControl("DropDownListQuota")).SelectedItem.Value));

    } // Page_Loadn

    protected void PagamentiSqlDataSource_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {

        if (e.AffectedRows == 0 || e.Exception != null)
            Response.Redirect("/System/Message.aspx?UpdateErr");

        if (UID.Value == "")
        {
            Response.Redirect("~/Pagamenti/Pagamenti_Lst.aspx?XRI=" + XRI.Value);
        }
        else
        {
            Response.Redirect("~/Zeus/Soci/Soci_Dsp.aspx?UID=" + (Request.QueryString["UID"].ToString()));
        }

    } // PagamentiSqlDataSource_Updated

    protected void PagamentiSqlDataSource_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.AffectedRows == 0 || e.Exception != null)
            Response.Redirect("~/System/Message.aspx?SelectErr");
    }


    protected void UtenteCreazione(object sender, EventArgs e)
    {
        HiddenField UtenteCreazione = (HiddenField)sender;
        UtenteCreazione.Value = Membership.GetUser().ProviderUserKey.ToString();

    }

    protected void DataOggi(object sender, EventArgs e)
    {
        HiddenField DataCreazione = (HiddenField)sender;
        DataCreazione.Value = DateTime.Now.ToString();

    }//fine DataOggi

    protected void UpdateButton_Click(object sender, EventArgs e)
    {
        TextBox AnnoTextBox = (TextBox)FormView1.FindControl("AnnoTextBox");
        TextBox AnnoRicevutaTextBox = (TextBox)FormView1.FindControl("AnnoRicevutaTextBox");
        HiddenField AnnoProgressivoHiddenField = (HiddenField)FormView1.FindControl("AnnoProgressivoHiddenField");
        CheckBox ApprovatoCheckBox = (CheckBox)FormView1.FindControl("ApprovatoCheckBox");
        HiddenField VerificatoHiddenField = (HiddenField)FormView1.FindControl("VerificatoHiddenField");
        HiddenField PagamentoEurHiddenField = (HiddenField)FormView1.FindControl("PagamentoEurHiddenField");
        DropDownList DropDownListQuota = (DropDownList)FormView1.FindControl("DropDownListQuota");
        HiddenField FilenameRicevutaHiddenField = (HiddenField)FormView1.FindControl("FilenameRicevutaHiddenField");

        if (ApprovatoCheckBox.Checked)
        {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(strConnessione))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();

                    command.CommandText = @"SELECT TOP (1) Anno, AnnoRicevuta, AnnoProgressivo 
                                        FROM tbQuoteAnnuali
                                        WHERE AnnoProgressivo IS NOT NULL AND AnnoProgressivo != ''
                                            AND AnnoRicevuta = @AnnoRicevuta
                                        ORDER BY AnnoRicevuta DESC, AnnoProgressivo DESC";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@AnnoRicevuta", AnnoRicevutaTextBox.Text);
                    command.Parameters.AddWithValue("@UserId", Request.QueryString["UID"].ToString());


                    SqlDataReader reader = command.ExecuteReader();
                    AnnoProgressivoHiddenField.Value = string.Format("{0}/{1}", "0001", AnnoRicevutaTextBox.Text);
                    if (reader.HasRows)
                    {
                        if (reader.Read())
                        {
                            if (!string.IsNullOrWhiteSpace(reader["AnnoProgressivo"].ToString()))
                            {
                                string oldAnnoProg = reader["AnnoProgressivo"].ToString();
                                string[] oldAnnoProgSplit = oldAnnoProg.Split('/');

                                string newAnnoProg = string.Empty;
                                if (oldAnnoProgSplit[1].Equals(AnnoRicevutaTextBox.Text))
                                {
                                    string num = (Convert.ToInt32(oldAnnoProgSplit[0]) + 1).ToString();
                                    string numResult = num.Count() == 1 ? "000" + num
                                    : num.Count() == 2 ? "00" + num
                                    : num.Count() == 3 ? "0" + num
                                    : num;

                                    newAnnoProg = string.Format("{0}/{1}", numResult, AnnoRicevutaTextBox.Text);
                                }
                                else
                                    newAnnoProg = string.Format("{0}/{1}", "0001", AnnoRicevutaTextBox.Text);

                                AnnoProgressivoHiddenField.Value = newAnnoProg;
                            }
                        }
                    }

                    reader.Close();
                }
                catch (Exception p)
                {
                    Response.Write(p.ToString());
                }
            }

            if (VerificatoHiddenField.Value.ToString().Length == 0)
            {
                DateTime Data = new DateTime();
                Data = DateTime.Now;
                VerificatoHiddenField.Value = Data.GetDataItaliana();

                if (string.IsNullOrWhiteSpace(FilenameRicevutaHiddenField.Value))
                {
                    //CREAZIONE DELLA RICEVUTA
                    try
                    {
                        if (!string.IsNullOrWhiteSpace(UID.Value))
                        {
                            PdfManager pdfManager = new PdfManager();
                            string filename = pdfManager.CreaRicevutaPagamentoQuota(UID.Value, AnnoProgressivoHiddenField.Value, AnnoTextBox.Text);

                            FilenameRicevutaHiddenField.Value = filename;
                        }
                    }
                    catch (Exception exep)
                    {
                        Response.Write("Errore in creazione ricevuta pdf: " + exep.ToString());
                    }
                }
            }
        }
        else
            VerificatoHiddenField.Value = "";

        PagamentoEurHiddenField.Value = GetPrice(DropDownListQuota.SelectedItem.Value);
    }

    protected void ApprovatoCheckBox_DataBinding(object sender, EventArgs e)
    {

    }

    protected void FormView1_DataBound(object sender, EventArgs e)
    {
        HiddenField VerificatoHiddenField = (HiddenField)FormView1.FindControl("VerificatoHiddenField");
        Label ApprovatoInDataLabel = (Label)FormView1.FindControl("ApprovatoInDataLabel");
        if (VerificatoHiddenField.Value.ToString().Length > 0)
        {
            CheckBox ApprovatoCheckBox = (CheckBox)FormView1.FindControl("ApprovatoCheckBox"); ;
            ApprovatoCheckBox.Checked = true;
            ApprovatoInDataLabel.Text = "in data: " + VerificatoHiddenField.Value.ToString();
        }
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Zeus/Soci/Soci_Dsp.aspx?UID=" + (Request.QueryString["UID"].ToString()));
    }

    private string GetPrice(string ID1Pagina)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;
        string myReturn = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {


            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();


                sqlQuery = "SELECT QuotaTotale FROM vwQuote_Lst WHERE IDQuota=@ID2QuotaCosto";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID2QuotaCosto", System.Data.SqlDbType.Int);
                command.Parameters["@ID2QuotaCosto"].Value = ID1Pagina;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (reader["QuotaTotale"] != null)
                        myReturn = reader["QuotaTotale"].ToString();
                }
                reader.Close();
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
            }
        }//fine Using


        return myReturn;

    }//fine

    private bool SendMail(MembershipUserCollection AllUsers, string Email, string AppRif)
    {

        // ------------- AppRif= "APP", approvato - "RIF", rifiutato ---------------------
        try
        {
            System.Net.Configuration.SmtpSection section = ConfigurationManager.GetSection("system.net/mailSettings/smtp") as System.Net.Configuration.SmtpSection;

            MailMessage mail = new MailMessage();

            mail.To.Add(new MailAddress(Email));
            //mail.Bcc.Add(new MailAddress("web@switchup.it"));
            mail.From = new MailAddress(section.From, "Sito Web Sertot");
            mail.Subject = "Conferma pagamento associazione Sertot";
            mail.IsBodyHtml = true;
            string myBody = string.Empty;

            foreach (MembershipUser myUser in AllUsers)
            {
                string tipo = string.Empty;
                string year = string.Empty;

                String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
                string sqlQuery = string.Empty;

                using (SqlConnection connection = new SqlConnection(strConnessione))
                {

                    connection.Open();
                    SqlCommand command = connection.CreateCommand();

                    sqlQuery = @"SELECT tbQuote.Descrizione, tbQuoteAnnuali.Anno FROM tbQuoteAnnuali 
                                    JOIN tbQuote ON tbQuote.IDQuota = tbQuoteAnnuali.ID2QuotaCosto
                                    WHERE UserID = @UserID";
                    command.CommandText = sqlQuery;
                    command.Parameters.Add("@UserID", System.Data.SqlDbType.UniqueIdentifier);
                    command.Parameters["@UserID"].Value = myUser.ProviderUserKey;

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        tipo = reader["Descrizione"].ToString();
                        year = reader["Anno"].ToString();
                    }

                    reader.Close();
                    connection.Close();

                }//fine Using

                try
                {
                    myBody += "Per accedere all'Area Soci del sito web Società Emiliano Romagnola Triveneta di Ortopedia e Traumatologia dovrai utilizzare le seguenti credenziali:";
                    myBody += "<br />";
                    myBody += "<br />";
                    myBody += "UserName: " + myUser.UserName;
                    myBody += "<br />";
                    myBody += "Password: " + myUser.GetPassword();
                    myBody += "<br />";
                    myBody += "Indirizzo E-Mail associato: " + myUser.Email;
                    myBody += "<br />";
                    myBody += "<br />";
                    myBody += "Il Tuo UserName coincide con il Tuo codice Socio. La password può essere modificata in qualunque momento all’interno dell’Area Soci.";
                    myBody += "<br />";
                    myBody += "<br />";
                    myBody += "Per qualsiasi comunicazione/esigenza, puoi fare riferimento alla Segreteria (sertot@mvcongressi.it) che si incarica, se del caso, di inoltrare le Sue/Tue richieste in modo mirato agli Organismi dell’Associazione.";
                    myBody += "<br />";
                    myBody += "<br />";
                    myBody += "Un cordiale saluto";
                    myBody += "<br />";
                    myBody += "<br />";
                    myBody += "LA SEGRETERIA";
                    myBody += "<br />";
                    myBody += "<br />";
                    myBody += "Dr.ssa Raffaella Fazioli";
                }
                catch (System.Web.Security.MembershipPasswordException)
                {

                }
            }

            mail.Body = myBody;
            SmtpClient client = new SmtpClient();
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.Port = 587;
            client.EnableSsl = true;
            client.SendAsync(mail, string.Empty);

            return true;
        }
        catch (Exception p)
        {

            Response.Write(p.ToString());
            return false;
        }

    }//fine SendMail
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="server">
    <asp:HiddenField ID="TitleField" runat="server" />
    <asp:HiddenField ID="XRI" runat="server" />
    <asp:HiddenField ID="UID" runat="server" />
    <div>

        <asp:FormView ID="FormView1" runat="server" DataSourceID="SqlDataSourcePagamenti" EnableModelValidation="True" DefaultMode="Edit" OnDataBound="FormView1_DataBound" RenderOuterTable="false">
            <EditItemTemplate>
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="ZML_BoxCategoria" runat="server" Text="Pagamento"></asp:Label>
                    </div>
                    <table width="100%">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="NRegistro" SkinID="FieldDescription" runat="server" Text="Socio"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:DropDownList AppendDataBoundItems="true" ID="DropDownListSocio" runat="server" DataSourceID="dsSocio" DataTextField="CognomeNome" DataValueField="UserId" SelectedValue='<%# Bind("UserID") %>'>
                                    <asp:ListItem Text="&gt;  Non inserito" Selected="True" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="dsSocio" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                    SelectCommand="SELECT [UserId], [CognomeNome] FROM [vwSoci_Lst] ORDER BY [CognomeNome]"></asp:SqlDataSource>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label1" SkinID="FieldDescription" runat="server" Text="Quota"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:DropDownList AppendDataBoundItems="true" ID="DropDownListQuota" runat="server" DataSourceID="dsQuota" DataTextField="DescPrezzo" DataValueField="IdQuota" SelectedValue='<%# Bind("ID2QuotaCosto") %>'>
                                    <asp:ListItem Text="&gt;  Non inserito" Selected="True" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="dsQuota" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                    SelectCommand="SELECT [IdQuota], [Descrizione], [Descrizione]+' - '+ CONVERT(varchar(10),[QuotaTotale]) AS [DescPrezzo] FROM [vwQuote_Lst] ORDER BY [Descrizione]"></asp:SqlDataSource>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label2" SkinID="FieldDescription" runat="server" Text="Tipo pagamento"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:DropDownList AppendDataBoundItems="true" ID="DropDownList1" runat="server" DataSourceID="dsMetodoPagamento" DataTextField="MetodoPagamento" DataValueField="ID1MetodoPagamento" SelectedValue='<%# Bind("ID2MetodoPagamento") %>'>
                                    <asp:ListItem Text="&gt;  Non inserito" Selected="True" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="dsMetodoPagamento" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                    SelectCommand="SELECT [ID1MetodoPagamento], [MetodoPagamento],[PubOrderAirespsa] FROM [tbMetodiPagamento] WHERE [PubOrderAirespsa]>0 ORDER BY [PubOrderAirespsa]"></asp:SqlDataSource>
                            </td>
                        </tr>

                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label3" SkinID="FieldDescription" runat="server" Text="Note Cliente"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:Label ID="Label4" SkinID="FieldDescription" runat="server" Text='<%# Eval("NoteSocio") %>'></asp:Label>
                            </td>
                        </tr>

                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label5" SkinID="FieldDescription" runat="server" Text="Note interne"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <style>
                                    .TextBox {
                                        height: auto;
                                    }
                                </style>
                                <asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine" Rows="5" Columns="100" CssClass="Multiline"
                                    Text='<%# Bind("NoteAirespsa") %>' />
                            </td>
                        </tr>

                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Anno" SkinID="FieldDescription" runat="server" Text="Anno Associazione"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="AnnoTextBox" runat="server" MaxLength="4" Columns="4" Enabled="false"
                                    Text='<%# Bind("Anno") %>' />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="AnnoTextBox"
                                    SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator Display="Dynamic" SkinID="ZSSM_Validazione01" ID="RegularExpressionValidator10"
                                    runat="server" ControlToValidate="AnnoTextBox" ErrorMessage="Necessario che sia numerico"
                                    ValidationExpression="^[0-9]+$"></asp:RegularExpressionValidator>
                                <asp:RangeValidator runat="server" ID="ValidatoreMaxMin" ControlToValidate="AnnoTextBox"
                                    MinimumValue="2000" MaximumValue="2040" Type="Double" SkinID="ZSSM_Validazione01" Display="Dynamic"
                                    Text="L'anno deve essere compreso tra 2000 e 2040" />
                            </td>
                        </tr>

                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label6" SkinID="FieldDescription" runat="server" Text="Anno Ricevuta"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="AnnoRicevutaTextBox" runat="server" MaxLength="4" Columns="4" Enabled="true"
                                    Text='<%# Bind("AnnoRicevuta") %>' />
                                <asp:Label ID="AnnoRicevutaModificabileLabel" SkinID="FieldDescription" runat="server" Text="modificabile solo se non è ancora stata creata la ricevuta"></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="AnnoRicevutaTextBox"
                                    SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator Display="Dynamic" SkinID="ZSSM_Validazione01" ID="RegularExpressionValidator1"
                                    runat="server" ControlToValidate="AnnoRicevutaTextBox" ErrorMessage="Necessario che sia numerico"
                                    ValidationExpression="^[0-9]+$"></asp:RegularExpressionValidator>
                                <asp:RangeValidator runat="server" ID="RangeValidator1" ControlToValidate="AnnoRicevutaTextBox"
                                    MinimumValue="2000" MaximumValue="2040" Type="Double" SkinID="ZSSM_Validazione01" Display="Dynamic"
                                    Text="L'anno deve essere compreso tra 2000 e 2040" />

                                <asp:HiddenField ID="AnnoProgressivoHiddenField" runat="server" Value='<%# Bind("AnnoProgressivo") %>' />
                            </td>
                        </tr>

                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Approvato" SkinID="FieldDescription" runat="server" Text="Verifica e crea ricevuta"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">

                                <asp:CheckBox ID="ApprovatoCheckBox" runat="server" OnDataBinding="ApprovatoCheckBox_DataBinding" />
                                <asp:Label ID="ApprovatoInDataLabel" SkinID="FieldDescription" runat="server" Text=""></asp:Label>
                                <asp:HiddenField ID="VerificatoHiddenField" runat="server" Value='<%# Bind("DataVerifica") %>' />
                                <asp:HiddenField ID="FilenameRicevutaHiddenField" runat="server" Value='<%# Bind("Ricevuta") %>' />
                            </td>
                        </tr>
                    </table>
                    <br />
                    <asp:HiddenField ID="RecordEdtUserHiddenField" runat="server" Value='<%# Bind("RecordEdtUser", "{0}") %>'
                        OnDataBinding="UtenteCreazione" />
                    <asp:HiddenField ID="RecordEdtDateHiddenField" Visible="false" runat="server" Value='<%# Bind("RecordEdtDate", "{0}") %>'
                        OnDataBinding="DataOggi" />
                    <asp:HiddenField ID="PagamentoEurHiddenField" runat="server" Value='<%# Bind("PagamentoEur") %>' />
                </div>
                <div align="center">
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" OnClick="UpdateButton_Click"
                        CommandName="Update" Text="Aggiorna" SkinID="ZSSM_Button01" />
                    &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server"
                        CausesValidation="False" CommandName="Cancel" Text="Annulla" SkinID="ZSSM_Button01" />
                    &nbsp;<asp:LinkButton ID="LinkButton1" runat="server"
                        CausesValidation="False" Text="Indietro" SkinID="ZSSM_Button01" OnClick="LinkButton1_Click" />
                </div>
            </EditItemTemplate>
        </asp:FormView>
        <asp:SqlDataSource ID="SqlDataSourcePagamenti" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
            SelectCommand="SELECT [ID1QuotaAnnuale], [UserID], [ID2QuotaCosto], [ID2MetodoPagamento], [Anno], [AnnoRicevuta], [AnnoProgressivo], [NoteSocio], [PagamentoEur], [DataVerifica], [NoteAirespsa], [RecordEdtUser], [RecordEdtDate], [Ricevuta] FROM [tbQuoteAnnuali] WHERE ([ID1QuotaAnnuale] = @ID1QuotaAnnuale)"
            UpdateCommand="UPDATE [tbQuoteAnnuali] SET [UserID] = @UserID, [ID2QuotaCosto] = @ID2QuotaCosto, [PagamentoEur] = @PagamentoEur, [ID2MetodoPagamento] = @ID2MetodoPagamento, [NoteAirespsa] = @NoteAirespsa, [Anno] = @Anno, [AnnoRicevuta] = @AnnoRicevuta, [AnnoProgressivo] = @AnnoProgressivo, [DataVerifica] = @DataVerifica, [RecordEdtUser] = @RecordEdtUser, [RecordEdtDate] = @RecordEdtDate, [Ricevuta] = @Ricevuta WHERE [ID1QuotaAnnuale] = @ID1QuotaAnnuale"
            OnUpdated="PagamentiSqlDataSource_Updated"
            OnSelected="PagamentiSqlDataSource_Selected">

            <SelectParameters>
                <asp:QueryStringParameter Name="ID1QuotaAnnuale" QueryStringField="XRI" Type="Int32" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="UserID" />
                <asp:Parameter Name="ID2QuotaCosto" Type="Int32" />
                <asp:Parameter Name="ID2MetodoPagamento" Type="Int16" />
                <asp:Parameter Name="Anno" Type="Int16" />
                <asp:Parameter Name="AnnoRicevuta" Type="Int16" />
                <asp:Parameter Name="AnnoProgressivo" Type="String" />
                <asp:Parameter Name="NoteAirespsa" Type="String" />
                <asp:Parameter Name="DataVerifica" Type="DateTime" ConvertEmptyStringToNull="true" />
                <asp:Parameter Name="PagamentoEur" Type="Decimal" />
                <asp:Parameter Name="Ricevuta" Type="String" />
                <asp:Parameter Name="RecordEdtUser" />
                <asp:Parameter Name="RecordEdtDate" Type="DateTime" />
                <asp:QueryStringParameter Name="ID1QuotaAnnuale" QueryStringField="XRI" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>

    </div>
</asp:Content>
