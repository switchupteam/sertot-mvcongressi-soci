﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" Theme="Zeus" %>

<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.SqlTypes" %>
<%@ Import Namespace="System.Globalization" %>




<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();

        if (!myDelinea.AntiSQLInjection(Request.QueryString))
            Response.Redirect("~/Zeus/System/Message.aspx");


        TitleField.Value = "Nuovo pagamento";

        if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "UID") && (Request.QueryString["UID"].Length > 0))
        {
            DropDownList ddlSocio = ((DropDownList)FormView1.FindControl("DropDownListSocio"));
            ddlSocio.SelectedValue = Request.QueryString["UID"];
            ddlSocio.Enabled = false;
            UID.Value = Request.QueryString["UID"].ToString();
        }

    } // Page_Load


    protected void PagamentoSqlDataSource_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.AffectedRows == 0 || e.Exception != null)
            Response.Redirect("~/System/Message.aspx?InsertErr");

        if (UID.Value == "")
        {
            Response.Redirect("../Pagamenti/Pagamenti_Lst.aspx?XRI=" + e.Command.Parameters["@XRI"].Value.ToString());
        }
        else
        {
            Response.Redirect("../Soci/Soci_Dsp.aspx?UID=" + (Request.QueryString["UID"].ToString()));
        }
    } // PagamentoSqlDataSource_Inserted

    protected void UtenteCreazione(object sender, EventArgs e)
    {
        HiddenField UtenteCreazione = (HiddenField)sender;
        UtenteCreazione.Value = Membership.GetUser().ProviderUserKey.ToString();

    }


    protected void DataOggi(object sender, EventArgs e)
    {
        HiddenField DataCreazione = (HiddenField)sender;
        DataCreazione.Value = DateTime.Now.ToString();

    }//fine DataOggi     

    protected void InsertButton_Click(object sender, EventArgs e)
    {
        bool hasQuota = true;

        TextBox AnnoTextBox = (TextBox)FormView1.FindControl("AnnoTextBox");
        TextBox AnnoRicevutaTextBox = (TextBox)FormView1.FindControl("AnnoRicevutaTextBox");
        HiddenField AnnoProgressivoHiddenField = (HiddenField)FormView1.FindControl("AnnoProgressivoHiddenField");
        CheckBox ApprovatoCheckBox = (CheckBox)FormView1.FindControl("ApprovatoCheckBox");
        HiddenField VerificatoHiddenField = (HiddenField)FormView1.FindControl("VerificatoHiddenField");
        HiddenField PagamentoEurHiddenField = (HiddenField)FormView1.FindControl("PagamentoEurHiddenField");
        DropDownList DropDownMetodoPagamento = (DropDownList)FormView1.FindControl("DropDownListMetodoPagamento");
        DropDownList DropDownListQuota = (DropDownList)FormView1.FindControl("DropDownListQuota");
        HiddenField FilenameRicevutaHiddenField = (HiddenField)FormView1.FindControl("FilenameRicevutaHiddenField");
        TextBox TextboxNote = (TextBox)FormView1.FindControl("TextNote");
        Label lblError = (Label)FormView1.FindControl("lblError");


        int iIdInserted = 0;

        lblError.Visible = false;


        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        if (DropDownListQuota.SelectedValue != null)
        {
            using (SqlConnection connection = new SqlConnection(strConnessione))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();

                    string sqlQuery = "SELECT TOP (1) QuotaAssociativa FROM tbQuote WHERE IDQuota = @IDQuota AND ZeusIsAlive = 1";
                    command.Parameters.Clear();
                    command.CommandText = sqlQuery;
                    command.Parameters.Add("@IDQuota", System.Data.SqlDbType.Int);
                    command.Parameters["@IDQuota"].Value = DropDownListQuota.SelectedValue;

                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.Read())
                        PagamentoEurHiddenField.Value = reader["QuotaAssociativa"].ToString();

                    reader.Close();
                }
                catch (Exception p)
                {
                    Response.Write(p.ToString());
                }
            }
        }

        using (SqlConnection connection = new SqlConnection(strConnessione))
        {
            connection.Open();
            SqlCommand command = connection.CreateCommand();

            try
            {
                string sqlQuery = null;
                command.CommandText = @"SELECT TOP (1) Anno, AnnoRicevuta, AnnoProgressivo 
                                        FROM tbQuoteAnnuali
                                        WHERE AnnoProgressivo IS NOT NULL AND AnnoProgressivo != ''
                                            AND AnnoRicevuta = @AnnoRicevuta
                                        ORDER BY AnnoRicevuta DESC, AnnoProgressivo DESC";
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@AnnoRicevuta", AnnoRicevutaTextBox.Text);

                SqlDataReader reader = command.ExecuteReader();
                AnnoProgressivoHiddenField.Value = string.Format("{0}/{1}", "0001", AnnoRicevutaTextBox.Text);
                if (reader.HasRows)
                {
                    if (reader.Read())
                    {
                        if (!string.IsNullOrWhiteSpace(reader["AnnoProgressivo"].ToString()))
                        {
                            string oldAnnoProg = reader["AnnoProgressivo"].ToString();
                            string[] oldAnnoProgSplit = oldAnnoProg.Split('/');

                            string newAnnoProg = string.Empty;
                            if (oldAnnoProgSplit[1].Equals(AnnoRicevutaTextBox.Text))
                            {
                                string num = (Convert.ToInt32(oldAnnoProgSplit[0]) + 1).ToString();
                                string numResult = num.Count() == 1 ? "000" + num
                                : num.Count() == 2 ? "00" + num
                                : num.Count() == 3 ? "0" + num
                                : num;

                                newAnnoProg = string.Format("{0}/{1}", numResult, AnnoRicevutaTextBox.Text);
                            }
                            else
                                newAnnoProg = string.Format("{0}/{1}", "0001", AnnoRicevutaTextBox.Text);

                            AnnoProgressivoHiddenField.Value = newAnnoProg;
                        }
                    }
                }

                reader.Close();

                bool isAnnoProgressivoNullInDB = true;
                if (!string.IsNullOrWhiteSpace(UID.Value))
                {
                    //CHECK SE ESISTE UNA QUOTA
                    sqlQuery = @"SELECT TOP (1) Anno, AnnoRicevuta, AnnoProgressivo, QuotaAssociativa, QuotaAssociativaTestuale
                                        FROM tbQuoteAnnuali
                                            INNER JOIN tbQuote ON tbQuote.IDQuota = tbQuoteAnnuali.ID2QuotaCosto
                                    WHERE UserId = @UserId AND Anno = @Anno
                                    ORDER BY AnnoProgressivo DESC";
                    command.Parameters.Clear();
                    command.CommandText = sqlQuery;
                    command.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
                    command.Parameters["@UserId"].Value = new Guid(UID.Value);

                    command.Parameters.Add("@Anno", System.Data.SqlDbType.SmallInt);
                    command.Parameters["@Anno"].Value = AnnoTextBox.Text;

                    reader = command.ExecuteReader();
                    if (!reader.HasRows)                    
                        hasQuota = false;                     
                    

                    reader.Close();

                    //SE ALMENO UNA QUOTA NON ESESTE LA CREO
                    if (!hasQuota)
                    {
                        sqlQuery = @"SET DATEFORMAT dmy; 
                                    INSERT INTO [tbQuoteAnnuali] 
                                        ([UserId],[ID2QuotaCosto],[Anno], [AnnoRicevuta], [AnnoProgressivo],[ID2MetodoPagamento],[NoteAirespsa], [DataVerifica],
                                        [PagamentoEur],[RecordNewUser],[RecordNewDate],[ZeusIsAlive])
                                    VALUES
                                        (@UserId,@ID2QuotaCosto,@Anno,@AnnoRicevuta,@AnnoProgressivo,@ID2MetodoPagamento,@NoteAirespsa, @DataVerifica,
                                        @PagamentoEur,@RecordNewUser,@RecordNewDate,1); SELECT SCOPE_IDENTITY()";

                        command.Parameters.Clear();
                        command.CommandText = sqlQuery;

                        command.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
                        command.Parameters["@UserId"].Value = new Guid(UID.Value);

                        command.Parameters.Add("@ID2QuotaCosto", System.Data.SqlDbType.Int);
                        command.Parameters["@ID2QuotaCosto"].Value = DropDownListQuota.SelectedItem.Value;

                        command.Parameters.Add("@Anno", System.Data.SqlDbType.SmallInt);
                        command.Parameters["@Anno"].Value = AnnoTextBox.Text;

                        command.Parameters.Add("@AnnoRicevuta", System.Data.SqlDbType.SmallInt);
                        command.Parameters["@AnnoRicevuta"].Value = AnnoRicevutaTextBox.Text;

                        command.Parameters.Add("@AnnoProgressivo", System.Data.SqlDbType.VarChar);
                        if (!ApprovatoCheckBox.Checked)
                            command.Parameters["@AnnoProgressivo"].Value = DBNull.Value;
                        else
                            command.Parameters["@AnnoProgressivo"].Value = AnnoProgressivoHiddenField.Value;

                        command.Parameters.Add("@ID2MetodoPagamento", System.Data.SqlDbType.Int);
                        command.Parameters["@ID2MetodoPagamento"].Value = DropDownMetodoPagamento.SelectedItem.Value;

                        command.Parameters.Add("@NoteAirespsa", System.Data.SqlDbType.NVarChar);
                        command.Parameters["@NoteAirespsa"].Value = TextboxNote.Text;

                        command.Parameters.Add("@DataVerifica", System.Data.SqlDbType.NVarChar);
                        command.Parameters["@DataVerifica"].Value = DBNull.Value;
                        if (ApprovatoCheckBox.Checked)
                            command.Parameters["@DataVerifica"].Value = !string.IsNullOrEmpty(VerificatoHiddenField.Value) ? VerificatoHiddenField.Value : DateTime.Now.GetDataItaliana();

                        command.Parameters.Add("@PagamentoEur", System.Data.SqlDbType.Decimal);
                        command.Parameters["@PagamentoEur"].Value = PagamentoEurHiddenField.Value;

                        command.Parameters.Add("@RecordNewUser", System.Data.SqlDbType.UniqueIdentifier);
                        command.Parameters["@RecordNewUser"].Value = new Guid(UID.Value);

                        DateTime Data = DateTime.Now;
                        command.Parameters.Add("@RecordNewDate", System.Data.SqlDbType.DateTime);
                        command.Parameters["@RecordNewDate"].Value = Data.ToString();

                        var vIdInserted = command.ExecuteScalar();
                        iIdInserted = int.Parse(vIdInserted.ToString());


                        if (iIdInserted != 0)
                        {
                            CheckBox chkCreaRicevuta = (CheckBox)FormView1.FindControl("chkCreaRicevuta");

                            if (chkCreaRicevuta.Checked && ApprovatoCheckBox.Checked)
                            {
                                Data = new DateTime();
                                Data = DateTime.Now;
                                VerificatoHiddenField.Value = Data.GetDataItaliana();

                                if (string.IsNullOrWhiteSpace(FilenameRicevutaHiddenField.Value))
                                {
                                    //CREAZIONE DELLA RICEVUTA
                                    try
                                    {
                                        if (!string.IsNullOrWhiteSpace(UID.Value))
                                        {
                                            PdfManager pdfManager = new PdfManager();
                                            string filename = pdfManager.CreaRicevutaPagamentoQuota(UID.Value, AnnoProgressivoHiddenField.Value, AnnoTextBox.Text);

                                            FilenameRicevutaHiddenField.Value = filename;
                                        }
                                    }
                                    catch (Exception exep)
                                    {
                                        Response.Write("Errore in creazione ricevuta pdf: " + exep.ToString());
                                        SetErrorLabel(3);
                                    }
                                }

                                try
                                {
                                    sqlQuery = @"UPDATE tbQuoteAnnuali
                                                SET Ricevuta = @Ricevuta
                                            WHERE UserId = @UserId
                                                AND ID1QuotaAnnuale = @IDQuotaAnnuale ";

                                    command = connection.CreateCommand();
                                    command.Parameters.Clear();
                                    command.CommandText = sqlQuery;
                                    command.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
                                    command.Parameters["@UserId"].Value = new Guid(UID.Value);

                                    command.Parameters.Add("@Ricevuta", System.Data.SqlDbType.VarChar);
                                    command.Parameters["@Ricevuta"].Value = FilenameRicevutaHiddenField.Value;

                                    command.Parameters.Add("@IDQuotaAnnuale", System.Data.SqlDbType.Int);
                                    command.Parameters["@IDQuotaAnnuale"].Value = iIdInserted;

                                    command.ExecuteNonQuery();
                                }
                                catch (Exception exep)
                                {
                                    Response.Write(exep.ToString());
                                }
                            }

                            connection.Close();

                            if (Request.QueryString["UID"].Length > 0)
                                Response.Redirect("/Zeus/Soci/Soci_Dsp.aspx?UID=" + Request.QueryString["UID"]);
                            else
                                Response.Redirect("/Zeus/Soci/Soci_Lst.aspx");
                        }
                        else
                        {
                            // Errore durante inserimento record
                            SetErrorLabel(4);
                        }
                    }
                    else
                    {
                        // Esiste già una quota per l'anno specificato
                        SetErrorLabel(2);
                    }
                }
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
                // Errore SQL
                SetErrorLabel(1);
            }
        }

    }

    protected void IndietroButton_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["UID"].Length > 0)
            Response.Redirect("/Zeus/Soci/Soci_Dsp.aspx?UID=" + Request.QueryString["UID"]);
        else
            Response.Redirect("/Zeus/Soci/Soci_Lst.aspx");
    }

    private void SetErrorLabel(int iErrNum)
    {
        Label lblError = (Label)FormView1.FindControl("lblError");
        if (lblError != null)
        {

            switch (iErrNum)
            {
                case 0:
                    lblError.Text = "Salvataggio completato";
                    break;

                case 1:
                    lblError.Text = "Errore generico";
                    break;

                case 2:
                    lblError.Text = "Per l'anno specificato esiste già una quota";
                    break;

                case 3:
                    lblError.Text = "Errore durante la creazione della ricevuta";
                    break;

                case 4:
                    lblError.Text = "Errore durante l'inserimento della quota";
                    break;

                default:
                    break;
            }
        }

        lblError.Visible = true;
    }

</script>

<asp:Content ID="Content2" ContentPlaceHolderID="ZeusAreaHead" runat="server">
    <style type="text/css">
        .lblErr {
            color: red;
            font-weight: bold;
        }
    </style>
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="server">
    <asp:HiddenField ID="TitleField" runat="server" />
    <asp:HiddenField ID="UID" runat="server" />
    <div>

        <asp:FormView ID="FormView1" runat="server" DataSourceID="SqlDataSourcePagamenti" EnableModelValidation="True" DefaultMode="Insert">

            <InsertItemTemplate>
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="ZML_BoxCategoria" runat="server" Text="Pagamento"></asp:Label>
                    </div>
                    <table width="100%">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="NRegistro" SkinID="FieldDescription" runat="server" Text="Socio"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:DropDownList AppendDataBoundItems="true" ID="DropDownListSocio" runat="server" DataSourceID="dsSocio" DataTextField="CognomeNome" DataValueField="UserId" SelectedValue='<%# Bind("UserID") %>'>
                                    <asp:ListItem Text="&gt;  Non inserito" Selected="True" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="dsSocio" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                    SelectCommand="SELECT [UserId], [CognomeNome] FROM [vwSoci_Lst] ORDER BY [CognomeNome]"></asp:SqlDataSource>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator15" ErrorMessage="Obbligatorio"
                                    runat="server" ControlToValidate="DropDownListSocio" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label1" SkinID="FieldDescription" runat="server" Text="Quota"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:DropDownList AppendDataBoundItems="true" ID="DropDownListQuota" runat="server" DataSourceID="dsQuota" DataTextField="DescPrezzo" DataValueField="IdQuota" SelectedValue='<%# Bind("ID2QuotaCosto") %>'>
                                    <asp:ListItem Text="&gt;  Non inserito" Selected="True" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="dsQuota" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                    SelectCommand="SELECT [IdQuota], [Descrizione], [Descrizione]+' - '+ CONVERT(varchar(10),[QuotaTotale]) AS [DescPrezzo] FROM [vwQuote_Lst] ORDER BY [Descrizione]"></asp:SqlDataSource>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Obbligatorio"
                                    runat="server" ControlToValidate="DropDownListQuota" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label2" SkinID="FieldDescription" runat="server" Text="Tipo pagamento"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:DropDownList AppendDataBoundItems="true" ID="DropDownListMetodoPagamento" runat="server" DataSourceID="dsMetodoPagamento" DataTextField="MetodoPagamento" DataValueField="ID1MetodoPagamento" SelectedValue='<%# Bind("ID2MetodoPagamento") %>'>
                                    <asp:ListItem Text="&gt;  Non inserito" Selected="True" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="dsMetodoPagamento" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                    SelectCommand="SELECT [ID1MetodoPagamento], [MetodoPagamento],[PubOrderAirespsa] FROM [tbMetodiPagamento] WHERE [PubOrderAirespsa]>0 ORDER BY [PubOrderAirespsa]"></asp:SqlDataSource>

                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Obbligatorio"
                                    runat="server" ControlToValidate="DropDownListMetodoPagamento" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>

                            </td>
                        </tr>

                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label5" SkinID="FieldDescription" runat="server" Text="Note interne"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <style>
                                    .TextBox {
                                        height: auto;
                                    }
                                </style>
                                <asp:TextBox ID="TextNote" runat="server" TextMode="MultiLine" Rows="5" Columns="100" CssClass="Multiline" Text='<%# Bind("NoteAirespsa") %>' />
                            </td>
                        </tr>

                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Anno" SkinID="FieldDescription" runat="server" Text="Anno associazione"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="AnnoTextBox" runat="server" MaxLength="4" Columns="4"
                                    Text='<%# Bind("Anno") %>' />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="AnnoTextBox"
                                    SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator Display="Dynamic" SkinID="ZSSM_Validazione01" ID="RegularExpressionValidator10"
                                    runat="server" ControlToValidate="AnnoTextBox" ErrorMessage="Necessario che sia numerico"
                                    ValidationExpression="^[0-9]+$"></asp:RegularExpressionValidator>
                                <asp:RangeValidator runat="server" ID="ValidatoreMaxMin" ControlToValidate="AnnoTextBox"
                                    MinimumValue="2010" MaximumValue="2040" Type="Double" SkinID="ZSSM_Validazione01" Display="Dynamic"
                                    Text="L'anno deve essere compreso tra 2010 e 2040" />
                            </td>
                        </tr>

                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label4" SkinID="FieldDescription" runat="server" Text="Anno ricevuta"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="AnnoRicevutaTextBox" runat="server" MaxLength="4" Columns="4"
                                    Text='<%# Bind("AnnoRicevuta") %>' />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="AnnoRicevutaTextBox"
                                    SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="Obbligatorio"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator Display="Dynamic" SkinID="ZSSM_Validazione01" ID="RegularExpressionValidator1"
                                    runat="server" ControlToValidate="AnnoRicevutaTextBox" ErrorMessage="Necessario che sia numerico"
                                    ValidationExpression="^[0-9]+$"></asp:RegularExpressionValidator>
                                <asp:RangeValidator runat="server" ID="RangeValidator1" ControlToValidate="AnnoRicevutaTextBox"
                                    MinimumValue="2010" MaximumValue="2040" Type="Double" SkinID="ZSSM_Validazione01" Display="Dynamic"
                                    Text="L'anno deve essere compreso tra 2010 e 2040" />

                                <asp:HiddenField ID="AnnoProgressivoHiddenField" runat="server" Value='<%# Bind("AnnoProgressivo") %>' />
                            </td>
                        </tr>

                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Approvato" SkinID="FieldDescription" runat="server" Text="Verificato"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">

                                <asp:CheckBox ID="ApprovatoCheckBox" runat="server" />
                                <asp:Label ID="ApprovatoInDataLabel" SkinID="FieldDescription" runat="server" Text=""></asp:Label>
                                <asp:HiddenField ID="VerificatoHiddenField" runat="server" Value='<%# Bind("DataVerifica") %>' />
                                <asp:HiddenField ID="FilenameRicevutaHiddenField" runat="server" Value='<%# Bind("Ricevuta") %>' />
                            </td>
                        </tr>


                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label3" SkinID="FieldDescription" runat="server" Text="Crea ricevuta"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:CheckBox ID="chkCreaRicevuta" runat="server" />
                            </td>
                        </tr>

                    </table>
                    <br />
                    <asp:HiddenField ID="RecordNewUserHiddenField" runat="server" Value='<%# Bind("RecordNewUser", "{0}") %>'
                        OnDataBinding="UtenteCreazione" />
                    <asp:HiddenField ID="RecordNewDateHiddenField" Visible="false" runat="server" Value='<%# Bind("RecordNewDate", "{0}") %>'
                        OnDataBinding="DataOggi" />
                    <asp:HiddenField ID="PagamentoEurHiddenField" runat="server" Value='<%# Bind("PagamentoEur") %>' />

                    <asp:Label runat="server" ID="lblError" Visible="false" CssClass="lblErr"></asp:Label>
                </div>

                <div align="center">
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True"
                        Text="Inserisci" SkinID="ZSSM_Button01" OnClick="InsertButton_Click" />
                    &nbsp;<asp:LinkButton ID="IndietroButton" runat="server"
                        CausesValidation="False" CommandName="Cancel" Text="Indietro" SkinID="ZSSM_Button01" OnClick="IndietroButton_Click" />
                </div>

            </InsertItemTemplate>
        </asp:FormView>
        <asp:SqlDataSource ID="SqlDataSourcePagamenti" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
            OnInserted="PagamentoSqlDataSource_Inserted"></asp:SqlDataSource>

    </div>
</asp:Content>



<asp:Content ID="Content3" ContentPlaceHolderID="ZeusAreaScript" runat="server">
    <script type="text/javascript">
        $(function () {


            document.getElementById('<%= ((CheckBox)FormView1.FindControl("ApprovatoCheckBox")).ClientID %>').onclick = function () {
                console.info("puoi dirlo!");
                document.getElementById('<%= ((CheckBox)FormView1.FindControl("chkCreaRicevuta")).ClientID %>').checked = this.checked;
            //document.getElementById($('#<%= ((CheckBox)FormView1.FindControl("chkCreaRicevuta")).ClientID %>')).checked = this.checked;
            };


        });

    </script>


</asp:Content>
