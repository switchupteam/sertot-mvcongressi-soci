<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"  Theme="Zeus" %>
<%@ Import Namespace="System.Web.Security" %>
<%@ Import Namespace="System.Web.UI" %>
<%@ Import Namespace="System.Web.UI.WebControls" %>
<%@ Import Namespace="System.Web.UI.WebControls.WebParts" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">   
   
    static string TitoloPagina = "Reset password account utente";
    static int MAXLENPASSWORD = 7;
    

    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;
        
        try
        {
            UserNameLabel.Text = Membership.GetUser(true).UserName.ToString();

        }
        catch (Exception)
        {
            CambiaPasswordCustomValidator.ErrorMessage= "Attenzione: l'utente non esiste!";
            CambiaPasswordCustomValidator.IsValid = false;
            TextBoxNewPass.Enabled = false;
            TextBoxConfNewPass.Enabled = false;
            CambiaPasswordLinkButton.Enabled = false;
        }
        
    }//fine Page_Load

    protected void CambiaPasswordLinkButton_Click(object sender, EventArgs e)
    {
        try
        {
            if (CambiaPasswordCustomValidator.IsValid)
            {
                MembershipUser user = Membership.GetUser(true);
                user.ChangePassword(user.GetPassword().ToString(), TextBoxNewPass.Text.ToString());
                Membership.UpdateUser(user);
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/System/Message.aspx?Msg=9867864259784548");
            }
        }
        catch (System.ArgumentException p)
        {

            CambiaPasswordCustomValidator.IsValid = false;
            if (Membership.MinRequiredNonAlphanumericCharacters > 0)
                CambiaPasswordCustomValidator.ErrorMessage = "Caratteri speciali richiesti: almeno " + Membership.MinRequiredNonAlphanumericCharacters;
        }

    }//fine CambiaPasswordLinkButton_Click


    protected void CambiaPasswordCustomValidator_ServerValidate(object sender, ServerValidateEventArgs args)
    {
        CambiaPasswordCustomValidator.ErrorMessage = string.Empty;

        if (TextBoxNewPass.Text.Length < Membership.MinRequiredPasswordLength)
        {
            args.IsValid = false;
            CambiaPasswordCustomValidator.ErrorMessage = "Lunghezza minima password: " + Membership.MinRequiredPasswordLength + " caratteri alfanumerici";
        }
        if (!TextBoxNewPass.Text.Equals(TextBoxConfNewPass.Text))
        {
            args.IsValid = false;
            CambiaPasswordCustomValidator.ErrorMessage = "Password e Conferma password non coincidono";
        }

    }//fine CambiaPasswordCustomValidator_ServerValidate
</script>


<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <div class="BlockBox">
        <div class="BlockBoxHeader">
            <asp:Label ID="InserisciPassword_Label" runat="server" Text="Inserisci nuova password"></asp:Label></div>
        <table cellpadding="2" cellspacing="1" border="0">
            <tr>
                <td class="BlockBoxDescription"><asp:Label ID="UserNameDescriptionLabel" runat="server" SkinID="FieldDescription">UserName</asp:Label></td>
                <td class="BlockBoxValue"><asp:Label ID="UserNameLabel" SkinID="FieldValue" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="BlockBoxDescription"><asp:Label ID="NuovaPasswordLabel" runat="server" SkinID="FieldDescription">Nuova password</asp:Label></td>
                <td class="BlockBoxValue"><asp:TextBox ID="TextBoxNewPass" runat="server" Columns="50" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Obbligatorio"
                        ControlToValidate="TextBoxNewPass" SkinID="ZSSM_Validazione01" Display="Dynamic"></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td class="BlockBoxDescription">
                    <asp:Label ID="ConfermaPasswordLabel" runat="server" SkinID="FieldDescription">Conferma nuova password</asp:Label>
                </td>
                <td class="BlockBoxValue"><asp:TextBox ID="TextBoxConfNewPass" runat="server" Columns="50" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Obbligatorio"
                        ControlToValidate="TextBoxConfNewPass" SkinID="ZSSM_Validazione01" Display="Dynamic"></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:CustomValidator ID="CambiaPasswordCustomValidator" runat="server" SkinID="ZSSM_Validazione01"
                        Display="Dynamic" OnServerValidate="CambiaPasswordCustomValidator_ServerValidate"></asp:CustomValidator>
                </td>
            </tr>
        </table>
    </div>
    <div align="center"><asp:LinkButton ID="CambiaPasswordLinkButton" SkinID="ZSSM_Button01" runat="server" OnClick="CambiaPasswordLinkButton_Click">Cambia password</asp:LinkButton></div>
</asp:Content>
