<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" %>

<%@ Import Namespace="System.Web.Security" %>
<%@ Import Namespace="System.Web.UI" %>
<%@ Import Namespace="System.Web.UI.WebControls" %>
<%@ Import Namespace="System.Web.UI.WebControls.WebParts" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">   
    static string TitoloPagina = "Modifica indirizzo E-Mail";


    protected void Page_Load(object sender, EventArgs args)
    {
        TitleField.Value = TitoloPagina;
        if (!Page.IsPostBack)
        {
            EmailTextBox.Text = Membership.GetUser().Email;
        }
        

    }//fine Page_Load


    protected void UpdateEmailButton_OnClick(object sender, EventArgs args)
    {
        try
        {
            MembershipUser user = Membership.GetUser();
            user.Email = EmailTextBox.Text.ToString();
            Membership.UpdateUser(user);
            Response.Redirect("~/Zeus/Personal/Account_Dtl.aspx");
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }//fine UpdateEmailButton_OnClick
   
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <div class="BlockBox">
        <div class="BlockBoxHeader">
            <asp:Label ID="ModificaMailLabel" runat="server" Text="Modifica indirizzo E-Mail"></asp:Label></div>
        <table>
            <tr>
                <td class="BlockBoxDescription">
                    <asp:Label ID="IndirizzoEMailLabel" SkinID="FieldDescription" runat="server">Indirizzo E-Mail *</asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:TextBox ID="EmailTextBox" runat="server" Columns="50"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="EmailTextBox"
                        ErrorMessage="Indirizzo E-Mail non corretto" SkinID="ZSSM_Validazione01" Display="Dynamic"
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="MailRequiredFieldValidator" runat="server" ErrorMessage="Obbligatorio"
                        ControlToValidate="EmailTextBox" Display="Dynamic" SkinID="ZSSM_Validazione01"></asp:RequiredFieldValidator>
                </td>
            </tr>
        </table>
    </div>
    <div align="center">
    <asp:LinkButton ID="UpdateEmailLinkButton" runat="server" SkinID="ZSSM_Button01" OnClick="UpdateEmailButton_OnClick">Salva dati</asp:LinkButton></div>
                        
</asp:Content>
