<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" %>

<%@ Register Src="~/Zeus/Personal/Account_Dtl.ascx" TagName="AccountDtl" TagPrefix="uc5" %>

<script runat="server">

    static string TitoloPagina = "Dettaglio account utente";

    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;
        ProfiloMarketing_Dtl.UID = Membership.GetUser().ProviderUserKey.ToString();
        ProfiloPersonale_Dtl.UID = Membership.GetUser().ProviderUserKey.ToString();
        ProfiloSocietario_Dtl.UID = Membership.GetUser().ProviderUserKey.ToString();
    }
    
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <link href="../../asset/css/ZeusTypeFoundry.css" rel="stylesheet" type="text/css" />
    <link href="../../SiteCss/ZeusSnippets.css" rel="stylesheet" type="text/css" />
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
            <td style="width: 50%">
                <uc5:AccountDtl ID="AccountDtl1" runat="server" />
                 <div class="BlockBox">
                    <dlc:ProfiloMarketing_Dtl ID="ProfiloMarketing_Dtl" runat="server" />
                    <div style="text-align: center">
                        <asp:LinkButton ID="ProfiloMarketingLinkButton" runat="server" PostBackUrl="~/Zeus/Personal/Account_Edt_Pref.aspx"
                            Text="Modifica" SkinID="ZSSM_Button01"></asp:LinkButton></div>
                    <div class="VertSpacerMedium" ></div>
                </div>
            </td>
            <td valign="top" width="10">
                <img height="10" src="/SiteImg/Spc.gif" width="10" alt="" />
            </td>
            <td style="width: 50%">
            
            <div class="BlockBox">
                    <dlc:ProfiloPersonale_Dtl ID="ProfiloPersonale_Dtl" runat="server" />
                    <div style="text-align: center">
                        <asp:LinkButton ID="ProfiloPersonaleLinkButton" runat="server" PostBackUrl="~/Zeus/Personal/Account_Edt_Pers.aspx"
                            Text="Modifica" SkinID="ZSSM_Button01"></asp:LinkButton></div>
                    <div class="VertSpacerMedium" ></div>
                </div>
                
                 <div class="BlockBox">
                    <dlc:ProfiloSocietario_Dtl ID="ProfiloSocietario_Dtl" runat="server" />
                    <div style="text-align: center">
                        <asp:LinkButton ID="ProfiloSocietarioLinkButton" runat="server" PostBackUrl="~/Zeus/Personal/Account_Edt_Soc.aspx"
                            Text="Modifica" SkinID="ZSSM_Button01"></asp:LinkButton></div>
                    <div class="VertSpacerMedium"></div>
                </div>
            
               
            </td>
        </tr>
    </table>
</asp:Content>
