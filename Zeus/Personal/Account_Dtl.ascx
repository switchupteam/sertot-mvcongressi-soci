<%@ Control Language="C#" ClassName="Account_Dtl_Public1" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">
    
    protected void Page_Load(object sender, EventArgs e)
    {
        MemberShipSqlDataSource.SelectCommand += " WHERE aspnet_Users.UserId='" + Membership.GetUser().ProviderUserKey.ToString() + "'";
       // tbProfiliPersonaliSqlDataSource.SelectCommand += " WHERE UserId='" + Membership.GetUser().ProviderUserKey.ToString() + "'";

        //PZ_UserMaster();

    }//fine Page_Load

    //private bool PZ_UserMaster()
    //{
    //    try
    //    {
    //        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

    //        string sqlQuery = "SELECT PZV_BoxUserMaster , PZL_BoxUserMaster FROM tbPZ_Profili ";


    //        using (SqlConnection conn = new SqlConnection(strConnessione))
    //        {
    //            SqlCommand command = new SqlCommand(sqlQuery, conn);

    //            conn.Open();
    //            SqlDataReader reader = command.ExecuteReader();
    //            /*fino a quando ci sono record*/

    //            if (!reader.HasRows)
    //            {
    //                reader.Close();
    //                return false;
    //            }

    //            while (reader.Read())
    //            {

    //                if (!reader.IsDBNull(0))
    //                    if (UserMasterPanel != null)
    //                     UserMasterPanel.Visible = reader.GetBoolean(0);

    //                if (!reader.IsDBNull(1))
    //                {
    //                    Label UserMasterLabel = (Label)UserMasterFormView.FindControl("UserMasterLabel");
    //                    UserMasterLabel.Text = reader.GetString(1);
    //                }

    //            }//fine while
    //            reader.Close();

    //        }//fine using

    //        return true;
    //    }
    //    catch (Exception p)
    //    {
    //        Response.Write(p.ToString());
    //        return false;
    //    }

    //}//fine PZ_UserMaster

    protected void EmailHyperLink_DataBinding(object sender, EventArgs e)
    {
        HyperLink EmailHyperLink = (HyperLink)sender;
        EmailHyperLink.NavigateUrl += EmailHyperLink.Target.ToString(); ;
        EmailHyperLink.Target = string.Empty;

    }
</script>

<asp:FormView ID="MemberShipFormView" runat="server" DefaultMode="ReadOnly" DataSourceID="MemberShipSqlDataSource"
    Width="100%" CellPadding="0">
    <ItemTemplate>
        <div class="BlockBox">
            <div class="BlockBoxHeader">
                <asp:Label ID="AccountUtenteLabel" runat="server" Text="Account utente"></asp:Label></div>
            <table>
                <tr>
                    <td>
                        <asp:Label ID="UserNameDescriptionLabel" SkinID="FieldDescription" runat="server">UserName:</asp:Label>
                        <asp:Label ID="UserNameLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("UserName", "{0}") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="EmailLabel" SkinID="FieldDescription" runat="server">E-mail:</asp:Label>
                        <asp:HyperLink ID="EmailHyperLink" runat="server" SkinID="FieldValue" NavigateUrl="mailto:"
                            Text='<%# Eval("Email", "{0}") %>' Target='<%# Eval("Email", "{0}") %>' OnDataBinding="EmailHyperLink_DataBinding"></asp:HyperLink>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="DataCreazioneDescriptionLabel" SkinID="FieldDescription" runat="server">Data creazione:</asp:Label>
                        <asp:Label ID="DataCreazioneLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("CreateDate", "{0:d}") %>'></asp:Label>
                    </td>
                </tr>
            </table>
            <div style="text-align: center">
                <asp:LinkButton ID="AccountUtenteLinkButton" SkinID="ZSSM_Button01" runat="server"
                    Text="Modifica E-Mail" PostBackUrl="/Zeus/Personal/ChangeEmail.aspx"></asp:LinkButton>
            </div>
            <div class="VertSpacerMedium">
            </div>
        </div>
        <div class="BlockBox">
            <div class="BlockBoxHeader">
                <asp:Label ID="PasswordLabel" runat="server" Text="Password"></asp:Label></div>
            <table>
                <tr>
                    <td>
                        <asp:Label ID="DataCambiamentoPasswordDescriptionLabel" SkinID="FieldDescription"
                            runat="server">Data ultimo cambio password:</asp:Label>
                        <asp:Label ID="DataCambiamentoPasswordLabel" SkinID="FieldValue" runat="server" Text='<%# Eval("LastPasswordChangedDate", "{0:g}") %>'></asp:Label>
                    </td>
                </tr>
            </table>
            <div style="text-align: center">
                <asp:LinkButton ID="ResetPasswordLinkButton" runat="server" SkinID="ZSSM_Button01"
                    Text="Cambia password" PostBackUrl="/Zeus/Personal/ChangePassword.aspx"></asp:LinkButton>
            </div>
            <div class="VertSpacerMedium">
            </div>
        </div>
    </ItemTemplate>
</asp:FormView>
<asp:SqlDataSource ID="MemberShipSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString%>"
    SelectCommand="SELECT aspnet_Membership.Password, aspnet_Membership.Email,  aspnet_Membership.CreateDate,  aspnet_Membership.LastPasswordChangedDate, aspnet_Users.UserName, aspnet_Users.UserId AS Expr1 FROM aspnet_Membership INNER JOIN aspnet_Users ON aspnet_Membership.UserId = aspnet_Users.UserId">
</asp:SqlDataSource>
<%--<asp:Panel ID="UserMasterPanel" runat="server">
    <asp:FormView ID="UserMasterFormView" runat="server" DataSourceID="tbProfiliPersonaliSqlDataSource"
        DefaultMode="ReadOnly" Width="100%" CellPadding="0">
        <ItemTemplate>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="UserMasterLabel" runat="server" Text="UserMaster"></asp:Label></div>
                <table border="0" cellpadding="2" cellspacing="1">
                    <tr>
                        <td>
                            <asp:Label ID="Label2" SkinID="FieldDescription" runat="server">Utente:</asp:Label>
                            <asp:HyperLink CssClass="DataTrackingLabel" SkinID="FieldValue" ID="HyperLink1" runat="server"
                                NavigateUrl='<%# Eval("UserMaster", "~/Zeus/Account/Account_Dtl.aspx?UID={0}") %>'
                                Text='<%# Eval("Identificativo", "{0}") %>'></asp:HyperLink>
                        </td>
                    </tr>
                </table>
                <div class="VertSpacerMedium">
                </div>
            </div>
        </ItemTemplate>
        <EmptyDataTemplate>
         <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="UserMasterLabel" runat="server" Text="UserMaster"></asp:Label></div>
                <table border="0" cellpadding="2" cellspacing="1">
                    <tr>
                        <td>
                            <asp:Label ID="Label2" SkinID="FieldDescription" runat="server">Utente: non associato</asp:Label>
                            
                        </td>
                    </tr>
                </table>
                <div class="VertSpacerMedium">
                </div>
            </div>
        </EmptyDataTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="tbProfiliPersonaliSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SELECT UserMaster,Identificativo FROM vwUserMaster">
    </asp:SqlDataSource>
</asp:Panel>--%>
