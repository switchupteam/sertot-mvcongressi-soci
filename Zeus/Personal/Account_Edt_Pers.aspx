<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" Theme="Zeus"  %>

<script runat="server">
    static string TitoloPagina = "Modifica profilo personale";
    
    protected void Page_Load(object sender, EventArgs e)
    {

        TitleField.Value = TitoloPagina;
        ProfiloPersonale_Edt1.UID = Membership.GetUser().ProviderUserKey.ToString();
        ProfiloPersonale_Edt1.RedirectPath = "~/Zeus/Personal/Account_Dtl.aspx";
        
    }//fine Page_Load


</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
<asp:HiddenField ID="TitleField" runat="server" Visible="False" />
<dlc:ProfiloPersonale_Edt ID="ProfiloPersonale_Edt1" runat="server" />
<dlc:zeusdatatracking ID="Zeusdatatracking1" runat="server" />
</asp:Content>
