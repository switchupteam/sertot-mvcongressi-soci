﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="Content_Src.aspx.cs"
    Inherits="Publisher_Content_Src"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <asp:HiddenField ID="ZIM" runat="server"  />
    <div class="BlockBox">
        <div class="BlockBoxHeader">
            <asp:Label ID="Dettaglio_contenuto" runat="server" Text="Dettaglio contenuto"></asp:Label></div>
        <table>
            <tr>
                <td class="BlockBoxDescription">
                    <asp:Label ID="Label3" SkinID="FieldDescription" runat="server" Text="Titolo"></asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:TextBox ID="TitoloTextBox" runat="server" MaxLength="100" Columns="100"></asp:TextBox>
                </td>
            </tr>
            <asp:Panel ID="ZMCF_Categoria1" runat="server">
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="ZML_Categoria1Label" SkinID="FieldDescription" runat="server" Text="Categoria *"></asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:DropDownList ID="ID2CategoriaDropDownList" runat="server" AppendDataBoundItems="True">
                            <asp:ListItem Text="> Tutte" Value="0" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="dsCategoria" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>">
                        </asp:SqlDataSource>
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel ID="PZV_BoxCategoria2Panel" runat="server">
                <tr>
                    <td class="BlockBoxDescription">
                        <asp:Label ID="ZML_BoxCategoria2Label" SkinID="FieldDescription" runat="server" Text="Categoria *"></asp:Label>
                    </td>
                    <td class="BlockBoxValue">
                        <asp:DropDownList ID="SezioniDropDownList" runat="server" AppendDataBoundItems="True">
                            <asp:ListItem Text="> Tutte" Value="0" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
            </asp:Panel>
            <tr>
                <td class="BlockBoxDescription">
                </td>
                <td class="BlockBoxValue">
                    <asp:LinkButton SkinID="ZSSM_Button01" ID="SearchButton" runat="server" Text="Cerca"
                        OnClick="SearchButton_Click"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
