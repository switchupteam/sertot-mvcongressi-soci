<%@ Control Language="C#" ClassName="PublisherLangButton" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Xml" %>

<script runat="server">
    

    
    private string myZeusId = string.Empty;
    private string myZeusIdModulo = string.Empty;
    private string myZeusLangCode = string.Empty;


    public string ZeusId
    {
        set { myZeusId = value; }
    }

    public string ZeusIdModulo
    {
        set { myZeusIdModulo = value; }
    }

    public string ZeusLangCode
    {
        set { myZeusLangCode = value; }
    }





    protected override void OnDataBinding(EventArgs e)
    {

        base.OnDataBinding(e);
        BindTheData();
    }


    public bool BindTheData()
    {

        if (myZeusId.Length == 0)
            return false;

        if (myZeusIdModulo.Length == 0)
            return false;

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        try
        {


            string myXPath = "/MODULES/" + myZeusIdModulo;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_Publisher.xml"));


            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);


            if (nodelist[0].ChildNodes.Count == 1)
                return false;

            string[] myLang = new string[nodelist[0].ChildNodes.Count];

            for (int i = 0; i < nodelist[0].ChildNodes.Count; ++i)
                myLang[i] = nodelist[0].ChildNodes[i].Name;

            nodelist = null;
            mydoc = null;

            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();
                SqlDataReader reader = null;


                this.Controls.Add(new LiteralControl("<table border=\"0\" cellpadding=\"2\" cellspacing=\"1\" class=\"BlockBox\" style=\"width: 100%\">"));
                this.Controls.Add(new LiteralControl("<tr>"));
                this.Controls.Add(new LiteralControl("<td class=\"BlockBoxHeader\">"));
                this.Controls.Add(new LiteralControl("<asp:Label ID=\"Label1\" runat=\"server\">Versioni in lingua alternativa</asp:Label></td></tr>"));
                this.Controls.Add(new LiteralControl("<tr><td>"));

                this.Controls.Add(new LiteralControl("<table>"));
                this.Controls.Add(new LiteralControl("<tr>"));

                for (int i = 0; i < myLang.Length; ++i)
                {


                    if (!myLang[i].Equals(myZeusLangCode))
                    {
                        sqlQuery = @"SELECT  ID1Publisher 
FROM [tbPublisher] 
WHERE ZeusId=@ZeusId AND ZeusLangCode=@ZeusLangCode ";

                        command.CommandText = sqlQuery;
                        command.Parameters.Add("@ZeusId", System.Data.SqlDbType.UniqueIdentifier);
                        command.Parameters["@ZeusId"].Value = new Guid(myZeusId);
                        command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.NVarChar);
                        command.Parameters["@ZeusLangCode"].Value = myLang[i];

                        reader = command.ExecuteReader();


                        if (!reader.HasRows)
                        {

                            this.Controls.Add(new LiteralControl("<td>"));
                            HyperLink ButtonNew = new HyperLink();
                            ButtonNew.Text = "Crea versione in lingua " + myLang[i];
                            ButtonNew.SkinID = "ZSSM_Button01";
                            ButtonNew.NavigateUrl = "~/Zeus/Publisher/Content_New_Lang.aspx?ZIM=" + myZeusIdModulo
                + "&LangO=" + myZeusLangCode
                + "&Lang=" + myLang[i]
                + "&ZID=" + myZeusId;

                            this.Controls.Add(ButtonNew);

                            this.Controls.Add(new LiteralControl("</td>"));
                        }
                        else
                        {
                            this.Controls.Add(new LiteralControl("<td>"));


                            string ID = string.Empty;

                            while (reader.Read())
                                if (!reader.IsDBNull(0))
                                    ID = reader.GetInt32(0).ToString();

                            HyperLink ButtonEdt = new HyperLink();
                            ButtonEdt.Text = "Modifica versione in lingua " + myLang[i];
                            ButtonEdt.SkinID = "ZSSM_Button01";
                            ButtonEdt.NavigateUrl = "~/Zeus/Publisher/Content_Edt.aspx?ZIM=" + myZeusIdModulo
                + "&Lang=" + myLang[i]
                + "&XRI=" + ID;


                            this.Controls.Add(ButtonEdt);

                            this.Controls.Add(new LiteralControl("</td>"));
                        }

                        reader.Close();
                        command.Parameters.Clear();

                    }//fine if

                }//fine for

                this.Controls.Add(new LiteralControl("</tr>"));
                this.Controls.Add(new LiteralControl("</table>"));
                this.Controls.Add(new LiteralControl("</td></tr></table>"));

                return true;

            }//fine Using
        }
        catch (Exception p)
        {

            Response.Write(p.ToString());
            return false;
        }


    }//fine BindTheData
    
   
</script>

