﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="Content_Dtl.aspx.cs"
    Inherits="Publisher_Content_Dtl"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register Src="~/Zeus/SiteAssets/PhotoGallery_Associa.ascx" TagName="PhotoGallery_Associa"
    TagPrefix="uc2" %>
<%@ Register Src="~/Zeus/PhotoGallery_Simple/Photo_Lst.ascx" TagName="Photo_Lst"
    TagPrefix="uc3" %>
<%@ Register Src="PublisherLangButton.ascx" TagName="PublisherLangButton" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <link href="../../asset/css/ZeusTypeFoundry.css" rel="stylesheet" type="text/css" />
    <link href="../../SiteCss/ZeusSnippets.css" rel="stylesheet" type="text/css" />
    <asp:HiddenField ID="TitleField" runat="server" />
    <asp:HiddenField ID="XRI" runat="server" />
    <asp:HiddenField ID="ZIM" runat="server" />
    <asp:HiddenField ID="ZID" runat="server" />
    <dlc:PermitRoles ID="myPermitRoles" runat="server" PAGE_TYPE="DTL" SQL_TABLE="tbPz_Publisher"
        EDT_ID_BUTTON="ModificaButton" DEL_ID_BUTTON="DeleteLinkButton" />
    <asp:FormView ID="FormView1" runat="server" DataKeyNames="ID1Publisher" DefaultMode="ReadOnly"
        DataSourceID="tbPublisherSqlDataSource" Width="100%">
        <ItemTemplate>

            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="Dettaglio_contenuto" runat="server" Text="Dettaglio contenuto"></asp:Label>
                </div>
                <table>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="ZML_Titolo" SkinID="FieldDescription" runat="server" Text="Titolo "></asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:Label ID="TitoloLabel" runat="server" Text='<%# Eval("Titolo") %>' SkinID="FieldValue"></asp:Label>
                        </td>
                    </tr>
                    <asp:Panel ID="ZMCF_Sottotitolo" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_Sottotitolo" SkinID="FieldDescription" runat="server" Text="Sottotitolo "></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:Label ID="SottoTitoloLabel" runat="server" Text='<%# Eval("Sottotitolo") %>'
                                    SkinID="FieldValue"></asp:Label>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_Categoria1" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_Categoria1Label" SkinID="FieldDescription" runat="server" Text="Categoria "></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:Label ID="CategoriaLabel" runat="server" SkinID="FieldValue"></asp:Label>
                                <asp:DropDownList ID="ID2CategoriaDropDownList" runat="server" Visible="false" AppendDataBoundItems="True">
                                </asp:DropDownList>
                                <asp:HiddenField ID="ID2CategoriaHiddenField" runat="server" Value='<%# Eval("ID2Categoria1") %>' />
                                <asp:SqlDataSource ID="dsCategoria" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"></asp:SqlDataSource>
                            </td>
                        </tr>
                    </asp:Panel>
                    <%-- <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label31" SkinID="FieldDescription" runat="server" Text="Record ID "></asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:Label ID="ID1PublisherLabel" runat="server" Text='<%# Eval("ID1Publisher") %>'
                                SkinID="FieldValue"></asp:Label>
                            <asp:HyperLink ID="AnteprimaHyperLink" runat="server" Text="Anteprima" NavigateUrl='<%# Eval("AnteprimaLink") %>'
                                SkinID="ZSSM_Button01" Target="_blank"></asp:HyperLink>
                        </td>
                    </tr>--%>
                </table>
            </div>
            <asp:Panel ID="ZMCF_DescBreve" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="Label4" runat="server" Text="Introduzione "></asp:Label>
                    </div>
                    <table>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_DescBreveLabel" SkinID="FieldDescription" runat="server" Text="Descrizione breve "></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:Label runat="server" ID="DescBreveLabel" Text='<%# Eval("DescBreve1") %>' SkinID="FieldValue"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <asp:Panel ID="ZMCF_BoxContenuto1" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="ZML_BoxContenuto1Label" runat="server" Text="Contenuto "></asp:Label>
                    </div>
                    <table>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_Contenuto1Label" SkinID="FieldDescription" runat="server" Text="Contenuto1"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:Label ID="Contenuto1Label" runat="server" Text='<%# Eval("Contenuto1") %>'></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <asp:Panel ID="ZMCF_BoxContenuto2" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="ZML_BoxContenuto2Label" runat="server" Text="Contenuto "></asp:Label>
                    </div>
                    <table>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_Contenuto2Label" SkinID="FieldDescription" runat="server" Text="Contenuto2"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:Label ID="Contenuto2Label" runat="server" Text='<%# Eval("Contenuto2") %>'></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <asp:Panel ID="ZMCF_BoxContenuto3" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="ZML_BoxContenuto3Label" runat="server" Text="Introduzione "></asp:Label>
                    </div>
                    <table>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_Contenuto3Label" SkinID="FieldDescription" runat="server" Text="Contenuto3"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:Label ID="Contenuto3Label" runat="server" Text='<%# Eval("Contenuto3") %>'></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <asp:Panel ID="ZMCF_Autore" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="Label26" runat="server" Text="Autore "></asp:Label>
                    </div>
                    <table>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label11" SkinID="FieldDescription" runat="server" Text="Autore"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:Label ID="AutoreLabel" runat="server" Text='<%# Eval("Autore") %>' SkinID="FieldValue"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label22" SkinID="FieldDescription" runat="server" Text="E-Mail autore"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:HyperLink ID="Autore_EmailLabel" runat="server" SkinID="FieldValue" NavigateUrl='<%# Bind("Autore_Email","mailto:{0}") %>'
                                    Text='<%# Bind("Autore_Email") %>'></asp:HyperLink>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <asp:Panel ID="ZMCF_BoxFonte" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="Label8" runat="server" Text="Fonte "></asp:Label>
                    </div>
                    <table>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label23" SkinID="FieldDescription" runat="server" Text="Fonte"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:Label ID="FonteLabel" runat="server" SkinID="FieldValue" Text='<%# Bind("Fonte") %>'></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label24" SkinID="FieldDescription" runat="server" Text="Link Fonte"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:HyperLink ID="Fonte_UrlLabel" runat="server" SkinID="FieldValue" NavigateUrl='<%# Bind("Fonte_Url") %>'
                                    Target="_blank" Text='<%# Bind("Fonte_Url") %>'></asp:HyperLink>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <dlc:ImageRaider ID="ImageRaider1" runat="server" ZeusIdModuloIndice="1" ZeusLangCode="ITA"
                BindPzFromDB="true" IsEditMode="true" DefaultEditBetaImage='<%# Eval("Immagine1") %>'
                DefaultEditGammaImage='<%# Eval("Immagine2") %>' Enabled="false"
                ImageAlt='<%# Eval("Immagine12Alt") %>' OnDataBinding="ImageRaider_DataBinding" />
            <dlc:ImageRaider ID="ImageRaider2" runat="server" ZeusIdModuloIndice="2" ZeusLangCode="ITA"
                BindPzFromDB="true" IsEditMode="true" DefaultEditBetaImage='<%# Eval("Immagine3") %>'
                DefaultEditGammaImage='<%# Eval("Immagine4") %>'
                ImageAlt='<%# Eval("Immagine34Alt") %>' Enabled="false" OnDataBinding="ImageRaider_DataBinding" />
            <asp:Panel ID="ZMCF_Allegato" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="AllegatoLabel" runat="server" Text="Allegato"></asp:Label>
                    </div>
                    <table>
                        <asp:Panel ID="ZMCF_AllegatoDescrizione" runat="server">
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label ID="DescrizioneLabel" runat="server" Text="Descrizione" SkinID="FieldDescription"></asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:Label ID="Allegato1_DescLabel" runat="server" Text='<%# Eval("Allegato1_Desc", "{0}") %>'
                                        SkinID="FieldValue"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="FileLabel" runat="server" Text="Allegato" SkinID="FieldDescription"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:HyperLink ID="FileAllegatoHyperLink" runat="server" NavigateUrl=' <%# Eval("Allegato1_File", "~/ZeusInc/Publisher/Documents/{0}") %>'
                                    Target="_blank" Text='<%# Eval("Allegato1_File","~/ZeusInc/Publisher/Documents/{0}") %>' SkinID="FieldValue" />
                                <asp:LinkButton runat="server" ID="UrlLinkButton" PostBackUrl='<%# Eval("Allegato1_UrlEsterno", "{0}") %>'
                                    Text='<%# Eval("Allegato1_UrlEsterno", "{0}") %>' SkinID="ZSSM_Button01" OnDataBinding="UrlLinkButton_DataBinding"></asp:LinkButton>
                                <asp:HiddenField ID="SelettoreAllegatoHiddenfield" runat="server" Value='<%# Eval("Allegato1_Selettore", "{0}") %>'
                                    OnDataBinding="SelettoreAllegatoHiddenfield_DataBinding" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <asp:Panel ID="ZMCF_BoxCategoria2" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="ZML_BoxCategoria2Label" runat="server" Text="Sezioni associate"></asp:Label>
                    </div>
                    <dlc:CategorieMultiple ID="CategorieMultiple1" runat="server" COLUMN_ID="ID2Publisher"
                        COLUMN_IDCAT="ID2Categoria2" TABLE="tbxPublisher_ContenutiCategorie" MODE="EDT" />
                </div>
            </asp:Panel>
            <asp:Panel ID="ZMCF_BoxPhotoGallery" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="Label38" runat="server" Text="Gallerie fotografiche associate"></asp:Label>
                    </div>
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>
                                <uc2:PhotoGallery_Associa ID="PhotoGallery_Associa1" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <asp:Panel ID="ZMCF_BoxPhotoGallerySimple" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <a name="PHOTO">
                            <asp:Label ID="Label56" runat="server" Text="Fotografie aggiuntive"></asp:Label></a>
                    </div>
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>
                                <uc3:Photo_Lst ID="PhotoGallery_Simple1" runat="server" OnInit="PhotoGallery_Simple1_Init" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <asp:Panel ID="ZMCF_BoxSEO" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="Label9" runat="server" Text="SEO Search Engine Optimization"></asp:Label>
                    </div>
                    <table>
                        <asp:Panel ID="ZMCF_TitoloBrowser" runat="server">
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label ID="Label42" runat="server" SkinID="FieldDescription" Text="Label">TITLE / Titolo Browser</asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:Label ID="TitoloBrowserTextBox" SkinID="FieldValue" runat="server" Text='<%# Eval("TitoloBrowser") %>'></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>

                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label13" runat="server" SkinID="FieldDescription" Text="Label">Description</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:Label ID="ZMCD_TagDescription" runat="server" Text='<%# Eval("TagDescription") %>'
                                    SkinID="FieldValue"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label14" runat="server" SkinID="FieldDescription" Text="Label">Keywords</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:Label ID="ZMCD_TagKeywords" runat="server" Text='<%# Eval("TagKeywords") %>'
                                    SkinID="FieldValue"></asp:Label>
                            </td>
                        </tr>
                        <asp:Panel ID="ZMCF_TagMeta1" runat="server">
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label ID="ZML_TagMeta1Label" runat="server" SkinID="FieldDescription" Text="Label">Description</asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:Label ID="Label41" runat="server" Text='<%# Bind("TagMeta1Attribute","Attribute: {0} ") %>'
                                        SkinID="FieldValue"></asp:Label>
                                    <img src="../SiteImg/Spc.gif" width="10" />
                                    <asp:Label ID="TagMeta1ValueLabel" runat="server" Text='<%# Bind("TagMeta1Value","Value: {0} ") %>'
                                        SkinID="FieldValue"></asp:Label>
                                    <img src="../SiteImg/Spc.gif" width="10" />
                                    <asp:Label ID="TagMeta1ContentLabel" runat="server" Text='<%# Bind("TagMeta1Content","Content: {0}") %>'
                                        SkinID="FieldValue"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_TagMeta2" runat="server">
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label ID="ZML_TagMeta2Label" runat="server" SkinID="FieldDescription" Text="Label">Description</asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:Label ID="Label40" runat="server" Text='<%# Bind("TagMeta2Attribute","Attribute: {0} ") %>'
                                        SkinID="FieldValue"></asp:Label>
                                    <img src="../SiteImg/Spc.gif" width="10" />
                                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("TagMeta2Value","Value: {0} ") %>'
                                        SkinID="FieldValue"></asp:Label>
                                    <img src="../SiteImg/Spc.gif" width="10" />
                                    <asp:Label ID="Label39" runat="server" Text='<%# Bind("TagMeta2Content","Content: {0}") %>'
                                        SkinID="FieldValue"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_UrlRewrite" runat="server">
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label ID="Label7" runat="server" Text="Url Rewrite" SkinID="FieldDescription"></asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:Label ID="InfoUrlLabel" runat="server" SkinID="FieldValue"></asp:Label><asp:Label ID="ZMCD_UrlDomain" runat="server" SkinID="FieldValue"></asp:Label><asp:Label ID="ZMCD_UrlSection" runat="server" SkinID="FieldValue"></asp:Label><asp:Label ID="ID1Pagina" runat="server" SkinID="FieldValue" Text='<%# Eval("ID1Publisher","{0}/") %>'></asp:Label><asp:Label ID="UrlPageDetailLabel" runat="server" Text='<%# Eval("UrlRewrite","{0}") %>' SkinID="FieldValue"></asp:Label>

                                    <asp:HiddenField ID="UrlRewriteHiddenField" runat="server" Value='<%# Eval("UrlRewrite","{0}") %>' />
                                    <asp:HiddenField ID="CopyHiddenField" runat="server" />

                                </td>
                                <%--<td>
                                    <input id="CopiaInput" value="Copia" type="button" onclick="ClipBoard(); return;"
                                        class="ZSSM_Button01_Button" />
                                </td>--%>
                            </tr>
                        </asp:Panel>

                    </table>
                </div>
            </asp:Panel>

            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="Planner" runat="server" Text="Planner"></asp:Label>
                </div>
                <table>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="ZML_PWArea1_NewLabel" SkinID="FieldDescription" runat="server"></asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:CheckBox ID="PW_Area1CheckBox" runat="server" Checked='<%# Eval("PW_Area1") %>'
                                Enabled="false" />
                            <asp:Label ID="AttivaDa_Label" runat="server" Text="Attiva pubblicazione dalla data"
                                SkinID="FieldValue"></asp:Label>
                            <asp:Label ID="PWI_Area1Label" runat="server" Text='<%# Eval("PWI_Area1", "{0:d}") %>'
                                SkinID="FieldValue" Columns="10" MaxLength="10" OnDataBinding="PWI_Area1Label_DataBinding"></asp:Label>
                            <asp:Label ID="Label12" runat="server" Text="ora" SkinID="FieldValue"></asp:Label>
                            <asp:Label ID="PWI_Area1OreLabel" runat="server" SkinID="FieldValue"></asp:Label>
                            <asp:Label ID="Label10" runat="server" Text="minuti" SkinID="FieldValue"></asp:Label>
                            <asp:Label ID="PWI_Area1MinutiLabel" runat="server" SkinID="FieldValue"></asp:Label>
                            <asp:Label ID="AttivaA_Label" runat="server" Text="alla data" SkinID="FieldValue"></asp:Label>
                            <asp:Label ID="PWF_Area1Label" runat="server" Text='<%# Eval("PWF_Area1", "{0:d}") %>'
                                SkinID="FieldValue" Columns="10" MaxLength="10" OnDataBinding="PWF_Area1Label_DataBinding"></asp:Label>
                            <asp:Label ID="PWF_Area1OreLabel" runat="server" SkinID="FieldValue"></asp:Label>
                            <asp:Label ID="Label16" runat="server" Text="minuti" SkinID="FieldValue"></asp:Label>
                            <asp:Label ID="PWF_Area1MinutiLabel" runat="server" SkinID="FieldValue"></asp:Label>
                        </td>
                    </tr>
                    <asp:Panel ID="ZMCF_Area2_New" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_PWArea2_NewLabel" SkinID="FieldDescription" runat="server"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:CheckBox ID="PW_Area2CheckBox" runat="server" Checked='<%# Eval("PW_Area2") %>'
                                    Enabled="false" />
                                <asp:Label ID="AttivaDa2_Label" runat="server" SkinID="FieldValue" Text="Attiva pubblicazione dalla data"></asp:Label>
                                <asp:Label ID="PWI_Area2Label" runat="server" Text='<%# Eval("PWI_Area2", "{0:d}") %>'
                                    SkinID="FieldValue" Columns="10" MaxLength="10" OnDataBinding="PWI_Area1Label_DataBinding"></asp:Label>
                                <asp:Label ID="Label17" runat="server" Text="ora" SkinID="FieldValue"></asp:Label>
                                <asp:Label ID="PWI_Area2OreLabel" runat="server" SkinID="FieldValue"></asp:Label>
                                <asp:Label ID="Label18" runat="server" Text="minuti" SkinID="FieldValue"></asp:Label>
                                <asp:Label ID="PWI_Area2MinutiLabel" runat="server" SkinID="FieldValue"></asp:Label>
                                <asp:Label ID="AttivaA2_Label" runat="server" SkinID="FieldValue" Text="alla data"></asp:Label>
                                <asp:Label ID="PWF_Area2Label" runat="server" Text='<%# Eval("PWF_Area2", "{0:d}") %>'
                                    SkinID="FieldValue" Columns="10" MaxLength="10" OnDataBinding="PWF_Area1Label_DataBinding"></asp:Label>
                                <asp:Label ID="Label20" runat="server" Text="ora" SkinID="FieldValue"></asp:Label>
                                <asp:Label ID="PWF_Area2OreLabel" runat="server" SkinID="FieldValue"></asp:Label>
                                <asp:Label ID="Label21" runat="server" Text="minuti" SkinID="FieldValue"></asp:Label>
                                <asp:Label ID="PWF_Area2MinutiLabel" runat="server" SkinID="FieldValue"></asp:Label>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_Area3_New" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_PWArea3_NewLabel" SkinID="FieldDescription" runat="server"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:CheckBox ID="PW_Area3CheckBox" runat="server" Checked='<%# Eval("PW_Area3") %>'
                                    Enabled="false" />
                                <asp:Label ID="Label15" runat="server" SkinID="FieldValue" Text="Attiva pubblicazione dalla data"></asp:Label>
                                <asp:Label ID="PWI_Area3Label" runat="server" Text='<%# Eval("PWI_Area3", "{0:d}") %>'
                                    SkinID="FieldValue" Columns="10" MaxLength="10" OnDataBinding="PWI_Area1Label_DataBinding"></asp:Label>
                                <asp:Label ID="Label25" runat="server" Text="ora" SkinID="FieldValue"></asp:Label>
                                <asp:Label ID="PWI_Area3OreLabel" runat="server" SkinID="FieldValue"></asp:Label>
                                <asp:Label ID="Label27" runat="server" Text="minuti" SkinID="FieldValue"></asp:Label>
                                <asp:Label ID="PWI_Area3MinutiLabel" runat="server" SkinID="FieldValue"></asp:Label>
                                <asp:Label ID="Label28" runat="server" SkinID="FieldValue" Text="alla data"></asp:Label>
                                <asp:Label ID="PWF_Area3Label" runat="server" Text='<%# Eval("PWF_Area3", "{0:d}") %>'
                                    SkinID="FieldValue" Columns="10" MaxLength="10" OnDataBinding="PWF_Area1Label_DataBinding"></asp:Label>
                                <asp:Label ID="Label29" runat="server" Text="ora" SkinID="FieldValue"></asp:Label>
                                <asp:Label ID="PWF_Area3OreLabel" runat="server" SkinID="FieldValue"></asp:Label>
                                <asp:Label ID="Label30" runat="server" Text="minuti" SkinID="FieldValue"></asp:Label>
                                <asp:Label ID="PWF_Area3MinutiLabel" runat="server" SkinID="FieldValue"></asp:Label>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_Area4_New" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_PWArea4_NewLabel" SkinID="FieldDescription" runat="server"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:CheckBox ID="PW_Area4CheckBox" runat="server" Checked='<%# Eval("PW_Area4") %>'
                                    Enabled="false" />
                                <asp:Label ID="Label32" runat="server" SkinID="FieldValue" Text="Attiva pubblicazione dalla data"></asp:Label>
                                <asp:Label ID="PWI_Area4Label" runat="server" Text='<%# Eval("PWI_Area4", "{0:d}") %>'
                                    SkinID="FieldValue" Columns="10" MaxLength="10" OnDataBinding="PWI_Area1Label_DataBinding"></asp:Label>
                                <asp:Label ID="Label33" runat="server" Text="ora" SkinID="FieldValue"></asp:Label>
                                <asp:Label ID="PWI_Area4OreLabel" runat="server" SkinID="FieldValue"></asp:Label>
                                <asp:Label ID="Label34" runat="server" Text="minuti" SkinID="FieldValue"></asp:Label>
                                <asp:Label ID="PWI_Area4MinutiLabel" runat="server" SkinID="FieldValue"></asp:Label>
                                <asp:Label ID="Label35" runat="server" SkinID="FieldValue" Text="alla data"></asp:Label>
                                <asp:Label ID="PWF_Area4Label" runat="server" Text='<%# Eval("PWF_Area4", "{0:d}") %>'
                                    SkinID="FieldValue" Columns="10" MaxLength="10" OnDataBinding="PWF_Area1Label_DataBinding"></asp:Label>
                                <asp:Label ID="Label36" runat="server" Text="ora" SkinID="FieldValue"></asp:Label>
                                <asp:Label ID="PWF_Area4OreLabel" runat="server" SkinID="FieldValue"></asp:Label>
                                <asp:Label ID="Label37" runat="server" Text="minuti" SkinID="FieldValue"></asp:Label>
                                <asp:Label ID="PWF_Area4MinutiLabel" runat="server" SkinID="FieldValue"></asp:Label>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_Archivio" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label6" runat="server" SkinID="FieldDescription" Text="Archivio"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:CheckBox ID="ArchivioCheckBox" runat="server" Checked='<%# Eval("Archivio") %>'
                                    Enabled="false" />
                                <asp:Label ID="Label19" runat="server" SkinID="FieldValue" Text="Archivia contenuto"></asp:Label>
                            </td>
                        </tr>
                    </asp:Panel>
                </table>
            </div>
            <uc1:PublisherLangButton ID="PublisherLangButton1" runat="server" ZeusId='<%# Eval("ZeusId") %>'
                ZeusIdModulo='<%# Eval("ZeusIdModulo") %>' ZeusLangCode='<%# Eval("ZeusLangCode") %>' />
            <div align="center">
                <asp:LinkButton ID="ModificaButton" SkinID="ZSSM_Button01" runat="server" Text="Modifica"
                    OnClick="ModificaButton_Click" />
                <img src="../SiteImg/Spc.gif" width="20" />
                <asp:LinkButton ID="DeleteLinkButton" runat="server" Text="Elimina" SkinID="ZSSM_Button01"
                    OnClick="DeleteLinkButton_Click"></asp:LinkButton>
            </div>
            <asp:HiddenField ID="zdtRecordNewUser" runat="server" Value='<%# Eval("RecordNewUser") %>' />
            <asp:HiddenField ID="zdtRecordNewDate" runat="server" Value='<%# Eval("RecordNewDate") %>' />
            <asp:HiddenField ID="zdtRecordEdtUser" runat="server" Value='<%# Eval("RecordEdtUser") %>' />
            <asp:HiddenField ID="zdtRecordEdtDate" runat="server" Value='<%# Eval("RecordEdtDate") %>' />
        </ItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="tbPublisherSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SELECT [ID1Publisher], [ZeusIdModulo], [ZeusLangCode], [Titolo],TitoloBrowser, [Sottotitolo], [ID2Categoria1],
         [DescBreve1],  [Contenuto1], [Contenuto2],[Contenuto3], [Immagine1], [Immagine2],[Immagine12Alt],[Immagine3], [Immagine4],
         [Immagine34Alt],[TagDescription], [TagKeywords], [UrlRewrite], [Autore], [Autore_Email],[Fonte],[Fonte_Url] ,[Archivio], 
          [PW_Area1], [PWI_Area1], [PWF_Area1], [PWF_Area2], [PW_Area2], [PWI_Area2] 
          ,[PW_Area3], [PWI_Area3], [PWF_Area3],[PW_Area4], [PWI_Area4], [PWF_Area4],TagMeta1Value,TagMeta1Content,TagMeta2Value,TagMeta2Content,
          [ZeusId], [ZeusIsAlive],[RecordNewUser], [RecordNewDate],[RecordEdtUser], [RecordEdtDate],[AnteprimaLink],TagMeta1Attribute,TagMeta2Attribute,
          [Allegato1_Desc], 
        [Allegato1_File], [Allegato1_UrlEsterno], [Allegato1_Peso], [Allegato1_Selettore]  
          FROM [vwPublisher_Dtl] 
          WHERE [ID1Publisher] = @ID1Publisher AND ZeusIdModulo=@ZeusIdModulo AND ZeusLangCode=@ZeusLangCode AND ZeusIsAlive=1"
        OnSelected="tbPublisherSqlDataSource_Selected">
        <SelectParameters>
            <asp:QueryStringParameter Name="ID1Publisher" QueryStringField="XRI" />
            <asp:QueryStringParameter Name="ZeusIdModulo" QueryStringField="ZIM" Type="String" />
            <asp:QueryStringParameter Name="ZeusLangCode" QueryStringField="Lang" Type="String"
                DefaultValue="ITA" />
        </SelectParameters>
    </asp:SqlDataSource>
    <div style="padding: 10px 0 0 0; margin: 0;">
        <dlc:zeusdatatracking ID="Zeusdatatracking1" runat="server" />
    </div>
</asp:Content>
