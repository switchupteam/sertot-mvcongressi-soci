﻿using System;
using System.Data.SqlClient;
using System.Web.Security;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per Content_Lst
/// </summary>
public partial class Publisher_Content_Lst : System.Web.UI.Page
{

    static string TitoloPagina = "Publisher";

    protected void Page_Load(object sender, EventArgs e)
    {

        delinea myDelinea = new delinea();
        TitleField.Value = TitoloPagina;

        if (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM"))
            Response.Redirect("/Zeus/System/Message.aspx?0");

        ZIM.Value = Server.HtmlEncode(Request.QueryString["ZIM"]);

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI")) && (Request.QueryString["XRI"].Length > 0) && (isOK()))
            SingleRecordPanel.Visible = true;

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRE")) && (Request.QueryString["XRE"].Length > 0))
            vwPublisher_LstSqlDataSource.SelectCommand += " AND Titolo LIKE '%" + Server.HtmlEncode(Request.QueryString["XRE"]) + "%'";

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI1")) && (!Request.QueryString["XRI1"].Equals("0")))
            vwPublisher_LstSqlDataSource.SelectCommand += " AND ID2Categoria1 = " + Server.HtmlEncode(Request.QueryString["XRI1"]);

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI2")) && (!Request.QueryString["XRI2"].Equals("0")))
        {
            vwPublisher_LstSqlDataSource.SelectCommand += " AND ID1Publisher IN  ";
            vwPublisher_LstSqlDataSource.SelectCommand += "( SELECT ID2Publisher FROM tbxPublisher_ContenutiCategorie WHERE ID2Categoria2=" + Server.HtmlEncode(Request.QueryString["XRI2"]) + ")";
        }


        vwPublisher_LstSqlDataSource.SelectCommand += " ORDER BY PWI_Area1 DESC";


        if (!ReadXML(ZIM.Value, GetLang()))
            Response.Redirect("~/Zeus/System/Message.aspx?1");

        ReadXML_Localization(ZIM.Value, GetLang());


    }//fine Page_Load 



    private bool isOK()
    {
        try
        {
            if ((Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/Publisher/Content_Edt.aspx") > 0) || (Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/Publisher/Content_New.aspx") > 0))
                return true;
            else return false;
        }
        catch (Exception p)
        {
            return false;
        }

    }//fine isOK


    private string GetLang()
    {
        string Lang = "ITA";

        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;

    }//fine GetLang



    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Publisher.xml"));



            TitleField.Value = mydoc.SelectSingleNode(myXPath + "ZML_TitoloPagina_Lst").InnerText;

            GridView1.Columns[8].HeaderText = GridView2.Columns[8].HeaderText =
                   mydoc.SelectSingleNode(myXPath + "ZML_PWArea1Inizio_Lst").InnerText;

            GridView1.Columns[9].HeaderText = GridView2.Columns[9].HeaderText =
                   mydoc.SelectSingleNode(myXPath + "ZML_PWArea1Fine_Lst").InnerText;

            GridView1.Columns[11].HeaderText = GridView2.Columns[11].HeaderText =
                mydoc.SelectSingleNode(myXPath + "ZML_PWArea2Inizio_Lst").InnerText;

            GridView1.Columns[12].HeaderText = GridView2.Columns[12].HeaderText =
              mydoc.SelectSingleNode(myXPath + "ZML_PWArea2Fine_Lst").InnerText;

            GridView1.Columns[14].HeaderText = GridView2.Columns[14].HeaderText =
                 mydoc.SelectSingleNode(myXPath + "ZML_PWArea3Inizio_Lst").InnerText;

            GridView1.Columns[15].HeaderText = GridView2.Columns[15].HeaderText =
                 mydoc.SelectSingleNode(myXPath + "ZML_PWArea3Fine_Lst").InnerText;

            GridView1.Columns[17].HeaderText = GridView2.Columns[17].HeaderText =
                 mydoc.SelectSingleNode(myXPath + "ZML_PWArea4Inizio_Lst").InnerText;

            GridView1.Columns[18].HeaderText = GridView2.Columns[18].HeaderText =
                mydoc.SelectSingleNode(myXPath + "ZML_PWArea4Fine_Lst").InnerText;

            GridView1.Columns[5].HeaderText = GridView2.Columns[5].HeaderText =
            mydoc.SelectSingleNode(myXPath + "ZML_Categoria1_Lst").InnerText;

            GridView1.Columns[6].HeaderText = GridView2.Columns[6].HeaderText =
            mydoc.SelectSingleNode(myXPath + "ZML_Categoria2_Lst").InnerText;

            GridView1.Columns[4].HeaderText = GridView2.Columns[4].HeaderText =
           mydoc.SelectSingleNode(myXPath + "ZML_Titolo").InnerText;


        }
        catch (Exception p)
        {
            //Response.Write(p.ToString());
        }

    }//fine ReadXML_Localization



    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_Publisher.xml"));

            if (!CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_Lst").InnerText))
                Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223599");

            GridView1.Columns[20].Visible = GridView2.Columns[20].Visible =
                CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_Dtl").InnerText);
            GridView1.Columns[21].Visible = GridView2.Columns[21].Visible
                = CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_Edt").InnerText);

            GridView1.Columns[14].Visible = GridView2.Columns[14].Visible =
               Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_PreviewUrl").InnerText);

            GridView1.Columns[5].Visible = GridView2.Columns[5].Visible =
                Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Categoria1_Lst").InnerText);

            GridView1.Columns[6].Visible = GridView2.Columns[6].Visible =
                Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Categoria2_Lst").InnerText);

            GridView1.Columns[10].Visible = GridView1.Columns[11].Visible = GridView1.Columns[12].Visible =
                            GridView2.Columns[10].Visible = GridView2.Columns[11].Visible = GridView2.Columns[12].Visible =
                             Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Area2_Lst").InnerText);

            GridView1.Columns[13].Visible = GridView1.Columns[14].Visible = GridView1.Columns[15].Visible =
                           GridView2.Columns[13].Visible = GridView2.Columns[14].Visible = GridView2.Columns[15].Visible =
                           Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Area3_Lst").InnerText);

            GridView1.Columns[16].Visible = GridView2.Columns[16].Visible =
                            GridView1.Columns[17].Visible = GridView2.Columns[17].Visible =
                      GridView1.Columns[18].Visible = GridView2.Columns[18].Visible =
                      Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Area4_Lst").InnerText);

            GridView1.Columns[19].Visible = GridView2.Columns[19].Visible =
                Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_AnteprimaLst").InnerText);

            Livello.Value = mydoc.SelectSingleNode(myXPath + "ZMCD_Cat2Liv").InnerText;

            ZMCD_UrlSection.Value = mydoc.SelectSingleNode(myXPath + "ZMCD_UrlDomain").InnerText 
                + mydoc.SelectSingleNode(myXPath + "ZMCD_UrlSection").InnerText;

            return true;
        }
        catch (Exception p)
        {
            return false;
        }

    }

    private bool CheckPermit(string AllRoles)
    {

        bool IsPermit = false;

        try
        {
            if (AllRoles.Length == 0)
                return false;

            char[] delimiter = { ',' };

            foreach (string roles in Roles.GetRolesForUser(Membership.GetUser().UserName))
            {
                string[] PressRoles = AllRoles.Split(delimiter);

                for (int i = 0; i < PressRoles.Length; ++i)
                {
                    if (PressRoles[i].Trim().Equals(roles.Trim()))
                    {
                        IsPermit = true;
                        break;
                    }
                }
            }
        }
        catch (Exception)
        { }

        return IsPermit;
    }

    private bool CheckMinuteInDate(string Date1, string TYPE)
    {

        if (Date1.Length == 0)
            return true;

        bool isOK = false;

        string DayMonthYear = Convert.ToDateTime(Date1).ToString("d");
        string DayMonthYearNow = DateTime.Now.ToString("d");

        if (DateTime.Compare(Convert.ToDateTime(DayMonthYear), Convert.ToDateTime(DayMonthYearNow)) == 0)
            switch (TYPE)
            {
                case "I":
                    if (DateTime.Compare(Convert.ToDateTime(Date1), DateTime.Now) <= 0)
                        isOK = true;
                    break;

                case "F":
                    if (DateTime.Compare(Convert.ToDateTime(Date1), DateTime.Now) >= 0)
                        isOK = true;
                    break;

            }

        else 
            isOK = true;

        return isOK;
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.DataItemIndex > -1)
            {
                Image Image1 = (Image)e.Row.FindControl("Image1");
                Label AttivoArea1 = (Label)e.Row.FindControl("AttivoArea1");

                int intAttivoArea1 = Convert.ToInt32(AttivoArea1.Text.ToString());

                if (intAttivoArea1 == 1)
                    if ((CheckMinuteInDate(e.Row.Cells[8].Text, "I"))
                        && (CheckMinuteInDate(e.Row.Cells[9].Text, "F")))
                        Image1.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";

                Image Image2 = (Image)e.Row.FindControl("Image2");
                Label AttivoArea2 = (Label)e.Row.FindControl("AttivoArea2");

                int intAttivoArea2 = Convert.ToInt32(AttivoArea2.Text.ToString());

                if ((intAttivoArea2 == 1) && (AttivoArea2.Visible))
                    if ((CheckMinuteInDate(e.Row.Cells[11].Text, "I"))
                  && (CheckMinuteInDate(e.Row.Cells[12].Text, "F")))
                        Image2.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";

                Image Image3 = (Image)e.Row.FindControl("Image3");
                Label AttivoArea3 = (Label)e.Row.FindControl("AttivoArea3");

                int intAttivoArea3 = Convert.ToInt32(AttivoArea3.Text.ToString());

                if ((intAttivoArea3 == 1) && (AttivoArea3.Visible))
                    if ((CheckMinuteInDate(e.Row.Cells[14].Text, "I"))
                  && (CheckMinuteInDate(e.Row.Cells[15].Text, "F")))
                        Image3.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";

                Image Image4 = (Image)e.Row.FindControl("Image4");
                Label AttivoArea4 = (Label)e.Row.FindControl("AttivoArea4");

                int intAttivoArea4 = Convert.ToInt32(AttivoArea4.Text.ToString());

                if ((intAttivoArea4 == 1) && (AttivoArea4.Visible))
                    if ((CheckMinuteInDate(e.Row.Cells[17].Text, "I"))
                  && (CheckMinuteInDate(e.Row.Cells[18].Text, "F")))
                        Image4.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";

                HyperLink AnteprimaLink = (HyperLink)e.Row.FindControl("AnteprimaLink");

                AnteprimaLink.NavigateUrl = ZMCD_UrlSection.Value + AnteprimaLink.NavigateUrl + ".aspx";
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    protected string GetSezioni(string ID1Publisher)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;
        string Sezioni = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {
            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                switch (Livello.Value)
                {
                    case "23":
                        sqlQuery = "SELECT [CatLiv2Liv3]  ";
                        break;

                    case "3":
                        sqlQuery = "SELECT [CatLiv3]  ";
                        break;

                    default:
                        sqlQuery = "SELECT [CatLiv1Liv2Liv3] ";
                        break;

                }

                sqlQuery += " FROM [vwPublisherCategorie] ";
                sqlQuery += " WHERE ID1Publisher=" + ID1Publisher;
                sqlQuery += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";

                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                    Sezioni += reader.GetString(0) + "<br />";

                reader.Close();
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
            }
        }
        return Sezioni;
    }

}