﻿using ASP;
using System;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per Content_Edt
/// </summary>
public partial class Publisher_Content_Edt : System.Web.UI.Page
{        
    static string TitoloPagina = "Modifica Publisher";

    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;

        delinea myDelinea = new delinea();

        if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM"))
            || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI")))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        ZIM.Value = Server.HtmlEncode(Request.QueryString["ZIM"]);
        XRI.Value = Server.HtmlEncode(Request.QueryString["XRI"]);

        if (!ReadXML(ZIM.Value, GetLang()))
            Response.Write("~/Zeus/System/Message.aspx?1");

        ReadXML_Localization(ZIM.Value, GetLang());
        try
        {
            string myJavascript = AddCompareDateArea1();
            myJavascript += " " + AddCompareDateArea2();
            myJavascript += " " + AddCompareDateArea3();
            myJavascript += " " + AddCompareDateArea4();
            myJavascript += " " + BothRequired1();
            myJavascript += " " + BothRequired2();
            myJavascript += " " + PrintMyJavaScript1();
            myJavascript += " " + PrintMyJavaScript3();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartUpScript", myJavascript, true);

            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");
            InsertButton.CommandName = "Update";

            InsertButton.Attributes.Add("onclick", "Validate()");
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }


    private string GetLang()
    {
        string Lang = "ITA";

        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;
    }

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Publisher.xml"));

            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            if (childNode.Name.Equals("ZML_TitoloPagina_Edt"))
                            {
                                TitleField.Value = childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;

                                else
                                {
                                    myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name + "Label");

                                    if (myLabel != null)
                                        myLabel.Text = childNode.InnerText;
                                }
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            string myXPathEach = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_Publisher.xml"));

            if (!CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_Edt").InnerText))
                Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223599");

            Panel myPanel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPathEach);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            if (childNode.Name.IndexOf("ZMCF_") > -1)
                            {
                                myPanel = (Panel)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myPanel != null)
                                    myPanel.Visible = Convert.ToBoolean(childNode.InnerText);
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }

            ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");

            if (ImageRaider1 != null)
                ImageRaider1.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxImmagini1").InnerText);

            ImageRaider ImageRaider2 = (ImageRaider)FormView1.FindControl("ImageRaider2");

            if (ImageRaider2 != null)
                ImageRaider2.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxImmagini2").InnerText);

            CheckBox ArchivioCheckBox = (CheckBox)FormView1.FindControl("ArchivioCheckBox");
            CheckBox PW_Area1CheckBox = (CheckBox)FormView1.FindControl("PW_Area1CheckBox");
            CheckBox PW_Area2CheckBox = (CheckBox)FormView1.FindControl("PW_Area2CheckBox");
            CheckBox PW_Area3CheckBox = (CheckBox)FormView1.FindControl("PW_Area3CheckBox");

            try
            {
                ArchivioCheckBox.Enabled = PW_Area1CheckBox.Enabled
                               = PW_Area2CheckBox.Enabled 
                               = PW_Area3CheckBox.Enabled
                               = IsUserInAuthorizationRoles(mydoc.SelectSingleNode(myXPath + "ZMCD_AuthorizationRoles").InnerText);
            }
            catch (Exception)
            { }

            if (!Page.IsPostBack)
                SetQueryOfID2CategoriaDropDownList(mydoc.SelectSingleNode(myXPath + "ZMCD_ZIMCategoria1").InnerText
                    , GetLang()
                    , mydoc.SelectSingleNode(myXPath + "ZMCD_Cat1Liv").InnerText);

            CategorieMultiple CategorieMultiple1 = (CategorieMultiple)FormView1.FindControl("CategorieMultiple1");

            if (CategorieMultiple1 != null)
            {
                CategorieMultiple1.ZeusIdModulo = mydoc.SelectSingleNode(myXPath + "ZMCD_ZIMCategoria2").InnerText;
                CategorieMultiple1.ZeusLangCode = GetLang();
                CategorieMultiple1.RepeatColumns = mydoc.SelectSingleNode(myXPath + "ZMCD_ColCategoria2").InnerText;
                CategorieMultiple1.PAGE_ID = XRI.Value;
                CategorieMultiple1.GetCategorie();

            }

            Label ZMCD_UrlSection = (Label)FormView1.FindControl("ZMCD_UrlSection");

            if (ZMCD_UrlSection != null)
                ZMCD_UrlSection.Text = mydoc.SelectSingleNode(myXPath + "ZMCD_UrlSection").InnerText;

            Label ZMCD_UrlDomain = (Label)FormView1.FindControl("ZMCD_UrlDomain");

            if (ZMCD_UrlDomain != null)
                ZMCD_UrlDomain.Text = mydoc.SelectSingleNode(myXPath + "ZMCD_UrlDomain").InnerText;

            Panel ZMCF_AllegatoDescrizione = (Panel)FormView1.FindControl("ZMCF_AllegatoDescrizione");
            if (ZMCF_AllegatoDescrizione != null)
                ZMCF_AllegatoDescrizione.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_AllegatoDescrizione").InnerText);

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }


    private bool CheckPermit(string AllRoles)
    {

        bool IsPermit = false;

        try
        {
            if (AllRoles.Length == 0)
                return false;

            char[] delimiter = { ',' };

            foreach (string roles in Roles.GetRolesForUser(Membership.GetUser().UserName))
            {
                string[] PressRoles = AllRoles.Split(delimiter);

                for (int i = 0; i < PressRoles.Length; ++i)
                {
                    if (PressRoles[i].Trim().Equals(roles.Trim()))
                    {
                        IsPermit = true;
                        break;
                    }
                }
            }
        }
        catch (Exception)
        { }

        return IsPermit;

    }

    private bool IsUserInAuthorizationRoles(string AuthorizationRoles)
    {
        try
        {
            foreach (string roles in Roles.GetRolesForUser(Membership.GetUser().UserName))
                if (roles.Equals("ZeusAdmin"))
                    return true;

                else
                {
                    char[] mySplit = { ',' };

                    string[] myRoles = AuthorizationRoles.Split(mySplit);

                    for (int i = 0; i < myRoles.Length; ++i)
                        if (myRoles[i].Trim().Equals(roles.Trim()))
                            return true;
                }

            return false;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    private string AddCompareDateArea1()
    {
        string myJavascript = " function CompareDateArea1(sender ,args) ";
        myJavascript += " { ";
        myJavascript += " var FirstDate=document.getElementById('";
        myJavascript += FormView1.FindControl("PWI_Area1TextBox").ClientID;
        myJavascript += "').value; ";

        myJavascript += " var FirstHour=document.getElementById('";
        myJavascript += FormView1.FindControl("PWI_Area1OreTextBox").ClientID;
        myJavascript += "').value; ";

        myJavascript += " var FirstMinute=document.getElementById('";
        myJavascript += FormView1.FindControl("PWI_Area1MinutiTextBox").ClientID;
        myJavascript += "').value; ";

        myJavascript += " var SecondDate=document.getElementById('";
        myJavascript += FormView1.FindControl("PWF_Area1TextBox").ClientID;
        myJavascript += "').value; ";

        myJavascript += " var SecondHour=document.getElementById('";
        myJavascript += FormView1.FindControl("PWF_Area1OreTextBox").ClientID;
        myJavascript += "').value; ";

        myJavascript += " var SecondMinute=document.getElementById('";
        myJavascript += FormView1.FindControl("PWF_Area1MinutiTextBox").ClientID;
        myJavascript += "').value; ";

        myJavascript += " if(FirstHour.length==0) FirstHour='00'; ";
        myJavascript += " if(FirstMinute.length==0) FirstMinute='00'; ";

        myJavascript += " FirstDate=FirstDate+' '+ FirstHour+':'+ FirstMinute;";
        myJavascript += " SecondDate=SecondDate+' '+ SecondHour+':'+ SecondMinute;";

        myJavascript += " if ( new Date(FirstDate) >= new Date(SecondDate)) ";
        myJavascript += " args.IsValid=false; ";
        myJavascript += " return; ";
        myJavascript += " } ";

        return myJavascript;

    }

    private string AddCompareDateArea2()
    {
        string myJavascript = " function CompareDateArea2(sender ,args) ";
        myJavascript += " { ";
        myJavascript += " var FirstDate=document.getElementById('";
        myJavascript += FormView1.FindControl("PWI_Area2TextBox").ClientID;
        myJavascript += "').value; ";

        myJavascript += " var FirstHour=document.getElementById('";
        myJavascript += FormView1.FindControl("PWI_Area2OreTextBox").ClientID;
        myJavascript += "').value; ";

        myJavascript += " var FirstMinute=document.getElementById('";
        myJavascript += FormView1.FindControl("PWI_Area2MinutiTextBox").ClientID;
        myJavascript += "').value; ";

        myJavascript += " var SecondDate=document.getElementById('";
        myJavascript += FormView1.FindControl("PWF_Area2TextBox").ClientID;
        myJavascript += "').value; ";

        myJavascript += " var SecondHour=document.getElementById('";
        myJavascript += FormView1.FindControl("PWF_Area2OreTextBox").ClientID;
        myJavascript += "').value; ";

        myJavascript += " var SecondMinute=document.getElementById('";
        myJavascript += FormView1.FindControl("PWF_Area2MinutiTextBox").ClientID;
        myJavascript += "').value; ";

        myJavascript += " if(FirstHour.length==0) FirstHour='00'; ";
        myJavascript += " if(FirstMinute.length==0) FirstMinute='00'; ";

        myJavascript += " FirstDate=FirstDate+' '+ FirstHour+':'+ FirstMinute;";
        myJavascript += " SecondDate=SecondDate+' '+ SecondHour+':'+ SecondMinute;";

        myJavascript += " if ( new Date(FirstDate) >= new Date(SecondDate)) ";
        myJavascript += " args.IsValid=false; ";
        myJavascript += " return; ";
        myJavascript += " } ";

        return myJavascript;

    }

    private string AddCompareDateArea3()
    {
        string myJavascript = " function CompareDateArea3(sender ,args) ";
        myJavascript += " { ";
        myJavascript += " var FirstDate=document.getElementById('";
        myJavascript += FormView1.FindControl("PWI_Area3TextBox").ClientID;
        myJavascript += "').value; ";

        myJavascript += " var FirstHour=document.getElementById('";
        myJavascript += FormView1.FindControl("PWI_Area3OreTextBox").ClientID;
        myJavascript += "').value; ";

        myJavascript += " var FirstMinute=document.getElementById('";
        myJavascript += FormView1.FindControl("PWI_Area3MinutiTextBox").ClientID;
        myJavascript += "').value; ";

        myJavascript += " var SecondDate=document.getElementById('";
        myJavascript += FormView1.FindControl("PWF_Area3TextBox").ClientID;
        myJavascript += "').value; ";

        myJavascript += " var SecondHour=document.getElementById('";
        myJavascript += FormView1.FindControl("PWF_Area3OreTextBox").ClientID;
        myJavascript += "').value; ";

        myJavascript += " var SecondMinute=document.getElementById('";
        myJavascript += FormView1.FindControl("PWF_Area3MinutiTextBox").ClientID;
        myJavascript += "').value; ";

        myJavascript += " if(FirstHour.length==0) FirstHour='00'; ";
        myJavascript += " if(FirstMinute.length==0) FirstMinute='00'; ";

        myJavascript += " FirstDate=FirstDate+' '+ FirstHour+':'+ FirstMinute;";
        myJavascript += " SecondDate=SecondDate+' '+ SecondHour+':'+ SecondMinute;";

        myJavascript += " if ( new Date(FirstDate) >= new Date(SecondDate)) ";
        myJavascript += " args.IsValid=false; ";
        myJavascript += " return; ";
        myJavascript += " } ";

        return myJavascript;

    }

    private string AddCompareDateArea4()
    {
        string myJavascript = " function CompareDateArea4(sender ,args) ";
        myJavascript += " { ";
        myJavascript += " var FirstDate=document.getElementById('";
        myJavascript += FormView1.FindControl("PWI_Area4TextBox").ClientID;
        myJavascript += "').value; ";

        myJavascript += " var FirstHour=document.getElementById('";
        myJavascript += FormView1.FindControl("PWI_Area4OreTextBox").ClientID;
        myJavascript += "').value; ";

        myJavascript += " var FirstMinute=document.getElementById('";
        myJavascript += FormView1.FindControl("PWI_Area4MinutiTextBox").ClientID;
        myJavascript += "').value; ";

        myJavascript += " var SecondDate=document.getElementById('";
        myJavascript += FormView1.FindControl("PWF_Area4TextBox").ClientID;
        myJavascript += "').value; ";

        myJavascript += " var SecondHour=document.getElementById('";
        myJavascript += FormView1.FindControl("PWF_Area4OreTextBox").ClientID;
        myJavascript += "').value; ";

        myJavascript += " var SecondMinute=document.getElementById('";
        myJavascript += FormView1.FindControl("PWF_Area4MinutiTextBox").ClientID;
        myJavascript += "').value; ";

        myJavascript += " if(FirstHour.length==0) FirstHour='00'; ";
        myJavascript += " if(FirstMinute.length==0) FirstMinute='00'; ";

        myJavascript += " FirstDate=FirstDate+' '+ FirstHour+':'+ FirstMinute;";
        myJavascript += " SecondDate=SecondDate+' '+ SecondHour+':'+ SecondMinute;";

        myJavascript += " if ( new Date(FirstDate) >= new Date(SecondDate)) ";
        myJavascript += " args.IsValid=false; ";
        myJavascript += " return; ";
        myJavascript += " } ";

        return myJavascript;

    }

    private string BothRequired1()
    {

        string ClientID1 = FormView1.FindControl("TagMeta1ValueTextBox").ClientID;
        string ClientID2 = FormView1.FindControl("TagMeta1ContentTextBox").ClientID;

        string myJavascript = " function BothRequired1(sender ,args) ";
        myJavascript += " { ";
        myJavascript += " if(document.getElementById('" + ClientID1 + "').value.length>0) ";
        myJavascript += " if(document.getElementById('" + ClientID2 + "').value.length==0)";
        myJavascript += " { ";
        myJavascript += " args.IsValid=false; ";
        myJavascript += " return; ";
        myJavascript += " } ";

        myJavascript += " if(document.getElementById('" + ClientID2 + "').value.length>0) ";
        myJavascript += " if(document.getElementById('" + ClientID1 + "').value.length==0)";
        myJavascript += " { ";
        myJavascript += " args.IsValid=false; ";
        myJavascript += " return; ";
        myJavascript += " } ";
        myJavascript += " return; ";
        myJavascript += " } ";

        return myJavascript;

    }

    private string BothRequired2()
    {
        string ClientID1 = FormView1.FindControl("TagMeta2ValueTextBox").ClientID;
        string ClientID2 = FormView1.FindControl("TagMeta2ContentTextBox").ClientID;

        string myJavascript = " function BothRequired2(sender ,args) ";
        myJavascript += " { ";
        myJavascript += " if(document.getElementById('" + ClientID1 + "').value.length>0) ";
        myJavascript += " if(document.getElementById('" + ClientID2 + "').value.length==0)";
        myJavascript += " { ";
        myJavascript += " args.IsValid=false; ";
        myJavascript += " return; ";
        myJavascript += " } ";

        myJavascript += " if(document.getElementById('" + ClientID2 + "').value.length>0) ";
        myJavascript += " if(document.getElementById('" + ClientID1 + "').value.length==0)";
        myJavascript += " { ";
        myJavascript += " args.IsValid=false; ";
        myJavascript += " return; ";
        myJavascript += " } ";
        myJavascript += " return; ";
        myJavascript += " } ";

        return myJavascript;
    }

    private string PrintMyJavaScript1()
    {
        string MyJavaScript = string.Empty;

        MyJavaScript += "function CheckDescriptionLength(sender, args)";
        MyJavaScript += "{";

        MyJavaScript += "if(document.getElementById('";
        MyJavaScript += FormView1.FindControl("RequiredFile").ClientID;
        MyJavaScript += "').checked || document.getElementById('";
        MyJavaScript += FormView1.FindControl("RequiredURL").ClientID;
        MyJavaScript += "').checked)";
        MyJavaScript += "if(document.getElementById('";
        MyJavaScript += FormView1.FindControl("Allegato1_DescTextBox").ClientID;
        MyJavaScript += "').value.length<=0)";
        MyJavaScript += "args.IsValid = false;";

        MyJavaScript += "return;";
        MyJavaScript += "}";

        return MyJavaScript;
    }

    private string PrintMyJavaScript3()
    {
        string MyJavaScript = string.Empty;

        MyJavaScript += "function CheckUrlLength(sender, args)";
        MyJavaScript += "{";

        MyJavaScript += "if(document.getElementById('";
        MyJavaScript += FormView1.FindControl("RequiredURL").ClientID;
        MyJavaScript += "').checked";
        MyJavaScript += " && document.getElementById('";
        MyJavaScript += FormView1.FindControl("Allegato1_UrlEsternoTextBox").ClientID;
        MyJavaScript += "').value.length<=0";
        MyJavaScript += " && document.getElementById('";
        MyJavaScript += FormView1.FindControl("Allegato1_UrlEsternoHiddenField").ClientID;
        MyJavaScript += "').value.length<=0)";
        MyJavaScript += "args.IsValid = false;";

        MyJavaScript += "return;";
        MyJavaScript += "}";

        return MyJavaScript;
    }

    protected void ZeusIdHidden_Load(object sender, EventArgs e)
    {
        HiddenField Guid = (HiddenField)sender;
        if (Guid.Value.Length == 0)
            Guid.Value = System.Guid.NewGuid().ToString();
    }

    protected void UtenteCreazione(object sender, EventArgs e)
    {
        HiddenField UtenteCreazione = (HiddenField)sender;
        UtenteCreazione.Value = Membership.GetUser().ProviderUserKey.ToString();
    }

    protected void DataOggi(object sender, EventArgs e)
    {
        HiddenField DataCreazione = (HiddenField)sender;
        DataCreazione.Value = DateTime.Now.ToString();
    }

    protected void PWI_Area1TextBox_DataBinding(object sender, EventArgs e)
    {
        TextBox DataCreazione = (TextBox)sender;

        if (DataCreazione.Text.Length == 0)
            DataCreazione.Text = DateTime.Now.ToString("d");
    }

    protected void PWF_Area1TextBox_DataBinding(object sender, EventArgs e)
    {
        TextBox DataFinale = (TextBox)sender;
        if (DataFinale.Text.Length == 0)
            DataFinale.Text = "31/12/2040";
    }

    protected void PWI_Area1HiddenField_DataBinding(object sender, EventArgs e)
    {
        HiddenField PWI_Area1HiddenField = (HiddenField)sender;

        try
        {
            TextBox PWI_Area1OreTextBox = (TextBox)FormView1.FindControl("PWI_Area1OreTextBox");
            TextBox PWI_Area1MinutiTextBox = (TextBox)FormView1.FindControl("PWI_Area1MinutiTextBox");


            PWI_Area1OreTextBox.Text = Convert.ToDateTime(PWI_Area1HiddenField.Value).Hour.ToString();
            PWI_Area1MinutiTextBox.Text = Convert.ToDateTime(PWI_Area1HiddenField.Value).Minute.ToString();
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    protected void PWF_Area1HiddenField_DataBinding(object sender, EventArgs e)
    {
        HiddenField PWF_Area1HiddenField = (HiddenField)sender;

        try
        {
            TextBox PWF_Area1OreTextBox = (TextBox)FormView1.FindControl("PWF_Area1OreTextBox");
            TextBox PWF_Area1MinutiTextBox = (TextBox)FormView1.FindControl("PWF_Area1MinutiTextBox");

            PWF_Area1OreTextBox.Text = Convert.ToDateTime(PWF_Area1HiddenField.Value).Hour.ToString();
            PWF_Area1MinutiTextBox.Text = Convert.ToDateTime(PWF_Area1HiddenField.Value).Minute.ToString();

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    protected void PWI_Area2HiddenField_DataBinding(object sender, EventArgs e)
    {
        HiddenField PWI_Area2HiddenField = (HiddenField)sender;

        try
        {
            TextBox PWI_Area2OreTextBox = (TextBox)FormView1.FindControl("PWI_Area2OreTextBox");
            TextBox PWI_Area2MinutiTextBox = (TextBox)FormView1.FindControl("PWI_Area2MinutiTextBox");

            PWI_Area2OreTextBox.Text = Convert.ToDateTime(PWI_Area2HiddenField.Value).Hour.ToString();
            PWI_Area2MinutiTextBox.Text = Convert.ToDateTime(PWI_Area2HiddenField.Value).Minute.ToString();
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    protected void PWF_Area2HiddenField_DataBinding(object sender, EventArgs e)
    {
        HiddenField PWF_Area2HiddenField = (HiddenField)sender;

        try
        {
            TextBox PWF_Area2OreTextBox = (TextBox)FormView1.FindControl("PWF_Area2OreTextBox");
            TextBox PWF_Area2MinutiTextBox = (TextBox)FormView1.FindControl("PWF_Area2MinutiTextBox");

            PWF_Area2OreTextBox.Text = Convert.ToDateTime(PWF_Area2HiddenField.Value).Hour.ToString();
            PWF_Area2MinutiTextBox.Text = Convert.ToDateTime(PWF_Area2HiddenField.Value).Minute.ToString();

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    protected void PWI_Area3HiddenField_DataBinding(object sender, EventArgs e)
    {
        HiddenField PWI_Area3HiddenField = (HiddenField)sender;

        try
        {
            TextBox PWI_Area3OreTextBox = (TextBox)FormView1.FindControl("PWI_Area3OreTextBox");
            TextBox PWI_Area3MinutiTextBox = (TextBox)FormView1.FindControl("PWI_Area3MinutiTextBox");

            PWI_Area3OreTextBox.Text = Convert.ToDateTime(PWI_Area3HiddenField.Value).Hour.ToString();
            PWI_Area3MinutiTextBox.Text = Convert.ToDateTime(PWI_Area3HiddenField.Value).Minute.ToString();

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    protected void PWF_Area3HiddenField_DataBinding(object sender, EventArgs e)
    {
        HiddenField PWF_Area3HiddenField = (HiddenField)sender;

        try
        {
            TextBox PWF_Area3OreTextBox = (TextBox)FormView1.FindControl("PWF_Area3OreTextBox");
            TextBox PWF_Area3MinutiTextBox = (TextBox)FormView1.FindControl("PWF_Area3MinutiTextBox");


            PWF_Area3OreTextBox.Text = Convert.ToDateTime(PWF_Area3HiddenField.Value).Hour.ToString();
            PWF_Area3MinutiTextBox.Text = Convert.ToDateTime(PWF_Area3HiddenField.Value).Minute.ToString();

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    protected void PWI_Area4HiddenField_DataBinding(object sender, EventArgs e)
    {
        HiddenField PWI_Area4HiddenField = (HiddenField)sender;

        try
        {
            TextBox PWI_Area4OreTextBox = (TextBox)FormView1.FindControl("PWI_Area4OreTextBox");
            TextBox PWI_Area4MinutiTextBox = (TextBox)FormView1.FindControl("PWI_Area4MinutiTextBox");


            PWI_Area4OreTextBox.Text = Convert.ToDateTime(PWI_Area4HiddenField.Value).Hour.ToString();
            PWI_Area4MinutiTextBox.Text = Convert.ToDateTime(PWI_Area4HiddenField.Value).Minute.ToString();

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }//fine PWI_Area4HiddenField_DataBinding


    protected void PWF_Area4HiddenField_DataBinding(object sender, EventArgs e)
    {
        HiddenField PWF_Area4HiddenField = (HiddenField)sender;

        try
        {
            TextBox PWF_Area4OreTextBox = (TextBox)FormView1.FindControl("PWF_Area4OreTextBox");
            TextBox PWF_Area4MinutiTextBox = (TextBox)FormView1.FindControl("PWF_Area4MinutiTextBox");


            PWF_Area4OreTextBox.Text = Convert.ToDateTime(PWF_Area4HiddenField.Value).Hour.ToString();
            PWF_Area4MinutiTextBox.Text = Convert.ToDateTime(PWF_Area4HiddenField.Value).Minute.ToString();

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }//fine PWF_Area4HiddenField_DataBinding


    //##############################################################################################################
    //################################################ CATEGORIE ###################################################
    //############################################################################################################## 

    private bool SetQueryOfID2CategoriaDropDownList(string ZeusIdModulo, string ZeusLangCode, string PZV_Cat1Liv)
    {


        try
        {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;


            SqlDataSource dsCategoria = (SqlDataSource)FormView1.FindControl("dsCategoria");
            DropDownList ID2CategoriaDropDownList = (DropDownList)FormView1.FindControl("ID2CategoriaDropDownList");
            HiddenField ID2CategoriaHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaHiddenField");
            //ID2CategoriaHiddenField.Value = "0";

            switch (PZV_Cat1Liv)
            {

                case "123":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv1Liv2Liv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "Catliv1Liv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;


                case "23":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv2Liv3]";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;

                default:
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;
            }

            ID2CategoriaDropDownList.SelectedIndex = ID2CategoriaDropDownList.Items.IndexOf(ID2CategoriaDropDownList.Items.FindByValue(ID2CategoriaHiddenField.Value));

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }//fine SetQueryOfID2CategoriaDropDownList



    protected void ID2CategoriaDropDownList_SelectIndexChange(object sender, EventArgs e)
    {
        HiddenField ID2CategoriaHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaHiddenField");
        DropDownList ID2CategoriaDropDownList = (DropDownList)sender;

        ID2CategoriaHiddenField.Value = ID2CategoriaDropDownList.SelectedValue;

    }//fine ID2CategoriaDropDownList_SelectIndexChange

    protected void Check_RadioButton_Validate(object sender, ServerValidateEventArgs args)
    {

        HiddenField HiddenNomeFile = (HiddenField)FormView1.FindControl("Allegato1_File");
        HiddenField Allegato_Peso = (HiddenField)FormView1.FindControl("Allegato_Peso");
        RadioButton SelettoreNull = (RadioButton)FormView1.FindControl("SelettoreNull");
        RadioButton RequiredURL = (RadioButton)FormView1.FindControl("RequiredURL");
        RadioButton RequiredFile = (RadioButton)FormView1.FindControl("RequiredFile");
        TextBox Allegato1_DescTextBox = (TextBox)FormView1.FindControl("Allegato1_DescTextBox");
        TextBox Allegato1_UrlEsternoTextBox = (TextBox)FormView1.FindControl("Allegato1_UrlEsternoTextBox");

        HyperLink FileUploadLink = (HyperLink)FormView1.FindControl("FileUploadLink");

        CustomValidator CustomValidator1 = (CustomValidator)sender;


        if (!SelettoreNull.Checked)
        {
            if ((Allegato1_DescTextBox.Visible) && (Allegato1_DescTextBox.Text.Length == 0))
            {
                CustomValidator1.ErrorMessage = "Obbligatorio";
                args.IsValid = false;
                HiddenNomeFile.Value = null;
                Allegato_Peso.Value = null;

            }
            else if ((RequiredURL.Checked) && (Allegato1_UrlEsternoTextBox.Text.Length == 0))
            {
                CustomValidator1.ErrorMessage = "Url obbligatorio";
                args.IsValid = false;
            }

            else args.IsValid = true;

        }
    }//fine Check_RadioButton_Validate

    protected void RadioButton1_CheckedChanged(object sender, EventArgs e)
    {
        HiddenField Allegato_Selettore = (HiddenField)FormView1.FindControl("Allegato_Selettore");
        Allegato_Selettore.Value = "FIL";
    }//fine RadioButton1_CheckedChanged

    protected void RadioButton2_CheckedChanged(object sender, EventArgs e)
    {
        HiddenField Allegato_Selettore = (HiddenField)FormView1.FindControl("Allegato_Selettore");
        Allegato_Selettore.Value = "LNK";
    }//fine RadioButton2_CheckedChanged

    protected void SelettoreNull_CheckedChanged(object sender, EventArgs e)
    {
        HiddenField Allegato_Selettore = (HiddenField)FormView1.FindControl("Allegato_Selettore");
        Allegato_Selettore.Value = string.Empty;
    }//fine SelettoreNull_CheckedChanged


    protected void UrlLinkButton_DataBinding(object sender, EventArgs e)
    {
        LinkButton UrlLinkButton = (LinkButton)sender;
        if (UrlLinkButton.PostBackUrl.Length == 0)
            UrlLinkButton.Visible = false;
        else
        {
            UrlLinkButton.OnClientClick = "window.open('" + UrlLinkButton.PostBackUrl + "'); return false;";
            //UrlLinkButton.PostBackUrl = "javascript:void(0)";
        }
    }//fine UrlLinkButton_DataBinding

    protected void FileUploadLinkButton_Databinding(object sender, EventArgs e)
    {
        LinkButton FileUploadLinkButton = (LinkButton)sender;
        if (FileUploadLinkButton.PostBackUrl.Length == 0)
            FileUploadLinkButton.Visible = false;
        else
        {
            FileUploadLinkButton.OnClientClick = "window.open('/ZeusInc/Publisher/Documents/" + FileUploadLinkButton.PostBackUrl + "'); return false;";
            FileUploadLinkButton.Text = FileUploadLinkButton.PostBackUrl.ToString();
            //FileUploadLinkButton.PostBackUrl = "javascript:void(0)";

        }

    }//fine FileUploadLinkButton_Databinding

    protected void FileUploadLink_DataBinding(object sender, EventArgs e)
    {
        HyperLink FileUploadLink = (HyperLink)sender;
        if (FileUploadLink.NavigateUrl.Length == 0)
            FileUploadLink.Visible = false;
        else
        {
            FileUploadLink.Text = FileUploadLink.NavigateUrl.ToString();
            FileUploadLink.NavigateUrl = "/ZeusInc/Publisher/Documents/" + FileUploadLink.NavigateUrl;
            FileUploadLink.Target = "_blank";

        }

    }//fine FileUploadLink_DataBinding



    protected void Allegato_Selettore_DataBinding(object sender, EventArgs e)
    {
        HiddenField Allegato_Selettore = (HiddenField)sender;

        switch (Allegato_Selettore.Value)
        {
            case "FIL":
                RadioButton RequiredFile = (RadioButton)FormView1.FindControl("RequiredFile");
                RequiredFile.Checked = true;
                break;

            case "LNK":
                RadioButton RequiredURL = (RadioButton)FormView1.FindControl("RequiredURL");
                RequiredURL.Checked = true;
                break;

            default:
                RadioButton SelettoreNull = (RadioButton)FormView1.FindControl("SelettoreNull");
                SelettoreNull.Checked = true;
                break;

        }
    }

    protected void ImageRaider_DataBinding(object sender, EventArgs e)
    {
        ImageRaider ImageRaider1 = (ImageRaider)sender;
        ImageRaider1.SetDefaultEditBetaImage();
        ImageRaider1.SetDefaultEditGammaImage();

    }

    protected void FileUploadCustomValidator_ServerValidate(object sender, ServerValidateEventArgs args)
    {
        try
        {
            ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");
            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");

            if (ImageRaider1.isValid())
                InsertButton.CommandName = "Update";
            else
                InsertButton.CommandName = string.Empty;

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }

    protected void FileUpload2CustomValidator_ServerValidate(object sender, ServerValidateEventArgs args)
    {
        try
        {
            ImageRaider ImageRaider2 = (ImageRaider)FormView1.FindControl("ImageRaider2");
            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");

            if (ImageRaider2.isValid())
                InsertButton.CommandName = "Update";
            else
                InsertButton.CommandName = string.Empty;

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }

    private bool SavePW_Area1()
    {
        try
        {
            TextBox PWI_Area1TextBox = (TextBox)FormView1.FindControl("PWI_Area1TextBox");
            TextBox PWI_Area1OreTextBox = (TextBox)FormView1.FindControl("PWI_Area1OreTextBox");
            TextBox PWI_Area1MinutiTextBox = (TextBox)FormView1.FindControl("PWI_Area1MinutiTextBox");
            TextBox PWF_Area1TextBox = (TextBox)FormView1.FindControl("PWF_Area1TextBox");
            TextBox PWF_Area1OreTextBox = (TextBox)FormView1.FindControl("PWF_Area1OreTextBox");
            TextBox PWF_Area1MinutiTextBox = (TextBox)FormView1.FindControl("PWF_Area1MinutiTextBox");

            RegularExpressionValidator PWI_Area1RegularExpressionValidator = 
                (RegularExpressionValidator)FormView1.FindControl("PWI_Area1RegularExpressionValidator");
            RegularExpressionValidator PWF_Area1RegularExpressionValidator = 
                (RegularExpressionValidator)FormView1.FindControl("PWF_Area1RegularExpressionValidator");

            PWI_Area1TextBox.Text += " " + PWI_Area1OreTextBox.Text + ":" + PWI_Area1MinutiTextBox.Text;
            PWF_Area1TextBox.Text += " " + PWF_Area1OreTextBox.Text + ":" + PWF_Area1MinutiTextBox.Text;

            PWI_Area1RegularExpressionValidator.Visible = PWF_Area1RegularExpressionValidator.Visible = false;


            return true;

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }//fine SavePW_Area1

    private bool SavePW_Area2()
    {
        try
        {
            TextBox PWI_Area2TextBox = (TextBox)FormView1.FindControl("PWI_Area2TextBox");
            TextBox PWI_Area2OreTextBox = (TextBox)FormView1.FindControl("PWI_Area2OreTextBox");
            TextBox PWI_Area2MinutiTextBox = (TextBox)FormView1.FindControl("PWI_Area2MinutiTextBox");
            TextBox PWF_Area2TextBox = (TextBox)FormView1.FindControl("PWF_Area2TextBox");
            TextBox PWF_Area2OreTextBox = (TextBox)FormView1.FindControl("PWF_Area2OreTextBox");
            TextBox PWF_Area2MinutiTextBox = (TextBox)FormView1.FindControl("PWF_Area2MinutiTextBox");

            //CompareValidator PW_Area2CompareValidator = (CompareValidator)FormView1.FindControl("PW_Area2CompareValidator");

            RegularExpressionValidator PWI_Area2RegularExpressionValidator = (RegularExpressionValidator)FormView1.FindControl("PWI_Area2RegularExpressionValidator");
            RegularExpressionValidator PWF_Area2RegularExpressionValidator = (RegularExpressionValidator)FormView1.FindControl("PWF_Area2RegularExpressionValidator");

            PWI_Area2TextBox.Text += " " + PWI_Area2OreTextBox.Text + ":" + PWI_Area2MinutiTextBox.Text;
            PWF_Area2TextBox.Text += " " + PWF_Area2OreTextBox.Text + ":" + PWF_Area2MinutiTextBox.Text;

            PWI_Area2RegularExpressionValidator.Visible = PWF_Area2RegularExpressionValidator.Visible = false;


            return true;

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }//fine SavePW_Area2

    private bool SavePW_Area3()
    {
        try
        {
            TextBox PWI_Area3TextBox = (TextBox)FormView1.FindControl("PWI_Area3TextBox");
            TextBox PWI_Area3OreTextBox = (TextBox)FormView1.FindControl("PWI_Area3OreTextBox");
            TextBox PWI_Area3MinutiTextBox = (TextBox)FormView1.FindControl("PWI_Area3MinutiTextBox");
            TextBox PWF_Area3TextBox = (TextBox)FormView1.FindControl("PWF_Area3TextBox");
            TextBox PWF_Area3OreTextBox = (TextBox)FormView1.FindControl("PWF_Area3OreTextBox");
            TextBox PWF_Area3MinutiTextBox = (TextBox)FormView1.FindControl("PWF_Area3MinutiTextBox");

            //CompareValidator PW_Area3CompareValidator = (CompareValidator)FormView1.FindControl("PW_Area3CompareValidator");

            RegularExpressionValidator PWI_Area3RegularExpressionValidator = (RegularExpressionValidator)FormView1.FindControl("PWI_Area3RegularExpressionValidator");
            RegularExpressionValidator PWF_Area3RegularExpressionValidator = (RegularExpressionValidator)FormView1.FindControl("PWF_Area3RegularExpressionValidator");

            PWI_Area3TextBox.Text += " " + PWI_Area3OreTextBox.Text + ":" + PWI_Area3MinutiTextBox.Text;
            PWF_Area3TextBox.Text += " " + PWF_Area3OreTextBox.Text + ":" + PWF_Area3MinutiTextBox.Text;

            PWI_Area3RegularExpressionValidator.Visible = PWF_Area3RegularExpressionValidator.Visible = false;

            return true;

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }//fine SavePW_Area3


    private bool SavePW_Area4()
    {
        try
        {
            TextBox PWI_Area4TextBox = (TextBox)FormView1.FindControl("PWI_Area4TextBox");
            TextBox PWI_Area4OreTextBox = (TextBox)FormView1.FindControl("PWI_Area4OreTextBox");
            TextBox PWI_Area4MinutiTextBox = (TextBox)FormView1.FindControl("PWI_Area4MinutiTextBox");
            TextBox PWF_Area4TextBox = (TextBox)FormView1.FindControl("PWF_Area4TextBox");
            TextBox PWF_Area4OreTextBox = (TextBox)FormView1.FindControl("PWF_Area4OreTextBox");
            TextBox PWF_Area4MinutiTextBox = (TextBox)FormView1.FindControl("PWF_Area4MinutiTextBox");

            //CompareValidator PW_Area4CompareValidator = (CompareValidator)FormView1.FindControl("PW_Area4CompareValidator");

            RegularExpressionValidator PWI_Area4RegularExpressionValidator = (RegularExpressionValidator)FormView1.FindControl("PWI_Area4RegularExpressionValidator");
            RegularExpressionValidator PWF_Area4RegularExpressionValidator = (RegularExpressionValidator)FormView1.FindControl("PWF_Area4RegularExpressionValidator");

            PWI_Area4TextBox.Text += " " + PWI_Area4OreTextBox.Text + ":" + PWI_Area4MinutiTextBox.Text;
            PWF_Area4TextBox.Text += " " + PWF_Area4OreTextBox.Text + ":" + PWF_Area4MinutiTextBox.Text;

            PWI_Area4RegularExpressionValidator.Visible = PWF_Area4RegularExpressionValidator.Visible = false;

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }//fine SavePW_Area4

    protected void Save_File_Upload(object sender, EventArgs e)
    {

        HiddenField FileNameBetaHiddenField = (HiddenField)FormView1.FindControl("FileNameBetaHiddenField");
        HiddenField FileNameGammaHiddenField = (HiddenField)FormView1.FindControl("FileNameGammaHiddenField");
        HiddenField Immagine12AltHiddenField = (HiddenField)FormView1.FindControl("Immagine12AltHiddenField");
        ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");

        HiddenField FileNameBeta2HiddenField = (HiddenField)FormView1.FindControl("FileNameBeta2HiddenField");
        HiddenField FileNameGamma2HiddenField = (HiddenField)FormView1.FindControl("FileNameGamma2HiddenField");
        HiddenField Immagine34AltHiddenField = (HiddenField)FormView1.FindControl("Immagine34AltHiddenField");
        ImageRaider ImageRaider2 = (ImageRaider)FormView1.FindControl("ImageRaider2");

        LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");

        if (InsertButton.CommandName != string.Empty)
        {
            SavePW_Area1();
            SavePW_Area2();
            SavePW_Area3();
            SavePW_Area4();

            if (ImageRaider1.GenerateBeta(string.Empty))
                FileNameBetaHiddenField.Value = ImageRaider1.ImgBeta_FileName;
            else FileNameBetaHiddenField.Value = ImageRaider1.DefaultBetaImage;

            if (ImageRaider1.GenerateGamma(string.Empty))
                FileNameGammaHiddenField.Value = ImageRaider1.ImgGamma_FileName;
            else FileNameGammaHiddenField.Value = ImageRaider1.DefaultGammaImage;

            //Immagine12AltHiddenField.Value = ImageRaider1.ImageAlt;

            if (ImageRaider2.GenerateBeta(string.Empty))
                FileNameBeta2HiddenField.Value = ImageRaider2.ImgBeta_FileName;
            else FileNameBeta2HiddenField.Value = ImageRaider2.DefaultBetaImage;

            if (ImageRaider2.GenerateGamma(string.Empty))
                FileNameGamma2HiddenField.Value = ImageRaider2.ImgGamma_FileName;
            else FileNameGamma2HiddenField.Value = ImageRaider2.DefaultGammaImage;

            //Immagine34AltHiddenField.Value = ImageRaider2.ImageAlt;

            delinea myDelinea = new delinea();
            HiddenField UrlRewriteHiddenField = (HiddenField)FormView1.FindControl("UrlRewriteHiddenField");
            TextBox UrlPageDetailTextBox = (TextBox)FormView1.FindControl("UrlPageDetailTextBox");

            UrlRewriteHiddenField.Value = myDelinea.myHtmlEncode(UrlPageDetailTextBox.Text.TrimEnd());


            HiddenField Allegato1_File = (HiddenField)FormView1.FindControl("Allegato1_File");
            UploadRaider Allegato_UploadRaider1 = (UploadRaider)FormView1.FindControl("Allegato_UploadRaider1");

            if ((Allegato_UploadRaider1 != null)
                    && (Allegato_UploadRaider1.GotFile()))
            {
                Allegato_UploadRaider1.SaveFile();
                Allegato1_File.Value = Allegato_UploadRaider1.FileName;
            }

        }//fine if
    }//fine Save_File_Upload

    protected void tbPublisherSqlDataSource_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        try
        {

            CategorieMultiple CategorieMultiple1 = (CategorieMultiple)FormView1.FindControl("CategorieMultiple1");

            if ((e.Exception == null)
                && CategorieMultiple1.SetCategorie())
                Response.Redirect("~/Zeus/Publisher/Content_Lst.aspx?XRI=" + Server.HtmlEncode(Request.QueryString["XRI"]) + "&ZIM=" + Server.HtmlEncode(Request.QueryString["ZIM"]) + "&Lang=" + GetLang());
            else Response.Write("~/Zeus/System/Message.aspx?UpdateErr");
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }//fine tbPublisherSqlDataSource_Updated

    protected void DeleteLinkButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Zeus/Publisher/Content_Del.aspx?XRI="
            + XRI.Value
            + "&Lang=" + GetLang() + "&ZIM="
            + ZIM.Value);

    }//fine DeleteLinkButton_Click

    protected void tbPublisherSqlDataSource_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {
        if ((e.Exception != null)
            || (e.AffectedRows <= 0))
            Response.Redirect("~/Zeus/System/Message.aspx?SelErr");
    }

}