﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="Content_Edt.aspx.cs"
    Inherits="Publisher_Content_Edt"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Data.SqlClient" %>
<%--<%@ Register Src="PublisherLangButton.ascx" TagName="PublisherLangButton" TagPrefix="uc1" %>--%>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <script type="text/javascript">
        function OnClientModeChange(editor) {
            var mode = editor.get_mode();
            var doc = editor.get_document();
            var head = doc.getElementsByTagName("HEAD")[0];
            var link;

            switch (mode) {
                case 1: //remove the external stylesheet when displaying the content in Design mode    
                    //var external = doc.getElementById("external");
                    //head.removeChild(external);
                    break;
                case 2:
                    break;
                case 4: //apply your external css stylesheet to Preview mode    
                    link = doc.createElement("LINK");
                    link.setAttribute("href", "/SiteCss/Telerik.css");
                    link.setAttribute("rel", "stylesheet");
                    link.setAttribute("type", "text/css");
                    link.setAttribute("id", "external");
                    head.appendChild(link);
                    break;
            }
        }

        function editorCommandExecuted(editor, args) {
            if (!$telerik.isChrome)
                return;
            var dialogName = args.get_commandName();
            var dialogWin = editor.get_dialogOpener()._dialogContainers[dialogName];
            if (dialogWin) {
                var cellEl = dialogWin.get_contentElement() || dialogWin.ui.contentCell || dialogWin.ui.content,
                frame = dialogWin.get_contentFrame();
                frame.onload = function () {
                    cellEl.style.cssText = "";
                    dialogWin.autoSize();
                }
            }
        }
    </script>
    <asp:HiddenField ID="TitleField" runat="server" />
    <asp:HiddenField ID="XRI" runat="server" />
    <asp:HiddenField ID="ZIM" runat="server" />
    
    <dlc:PermitRoles ID="myPermitRoles" runat="server" PAGE_TYPE="EDT" SQL_TABLE="tbPz_Publisher"
        DEL_ID_BUTTON="DeleteLinkButton" />
    <asp:FormView ID="FormView1" runat="server" DataKeyNames="ID1Publisher" DefaultMode="Edit"
        DataSourceID="tbPublisherSqlDataSource" Width="100%">
        <EditItemTemplate>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="Dettaglio_contenuto" runat="server" Text="Dettaglio contenuto"></asp:Label>
                </div>
                <table>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="ZML_Titolo" SkinID="FieldDescription" runat="server" Text="Titolo *"></asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:TextBox ID="TitoloTextBox" runat="server" Text='<%# Bind("Titolo") %>' MaxLength="100"
                                Columns="100"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator" ErrorMessage="Obbligatorio"
                                runat="server" ControlToValidate="TitoloTextBox" SkinID="ZSSM_Validazione01"
                                Display="Dynamic" ValidationGroup="myValidation">
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator Display="Dynamic" SkinID="ZSSM_Validazione01" ControlToValidate="TitoloTextBox" ValidationGroup="myValidation" ID="RegularExpressionValidator19" ValidationExpression = "^[\s\S]{0,100}$" runat="server" ErrorMessage="Massimo 100 caratteri"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <asp:Panel ID="ZMCF_Sottotitolo" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_Sottotitolo" SkinID="FieldDescription" runat="server" Text="Sottotitolo *"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="SottoTitoloTextBox" runat="server" Text='<%# Bind("Sottotitolo") %>'
                                    MaxLength="200" Columns="100"></asp:TextBox>
                     <%--           <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Obbligatorio"
                                    runat="server" ControlToValidate="SottoTitoloTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ValidationGroup="myValidation">
                                </asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_Categoria1" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_Categoria1Label" SkinID="FieldDescription" runat="server" Text="Categoria *"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:DropDownList ID="ID2CategoriaDropDownList" runat="server" OnSelectedIndexChanged="ID2CategoriaDropDownList_SelectIndexChange"
                                    AppendDataBoundItems="True">
                                </asp:DropDownList>
                                <asp:HiddenField ID="ID2CategoriaHiddenField" runat="server" Value='<%# Bind("ID2Categoria1") %>' />
                                <asp:SqlDataSource ID="dsCategoria" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"></asp:SqlDataSource>
                            </td>
                        </tr>
                    </asp:Panel>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="Label32" SkinID="FieldDescription" runat="server" Text="Record ID "></asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:Label ID="ID1PublisherLabel" runat="server" Text='<%# Eval("ID1Publisher") %>'
                                SkinID="FieldValue"></asp:Label>
                            <%--<asp:HyperLink ID="AnteprimaHyperLink" runat="server" Text="Anteprima" NavigateUrl='<%# Eval("AnteprimaLink") %>'
                                SkinID="ZSSM_Button01" Target="_blank"></asp:HyperLink>--%>
                        </td>
                    </tr>
                </table>
            </div>
            <asp:Panel ID="ZMCF_DescBreve" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="Label4" runat="server" Text="Introduzione "></asp:Label>
                    </div>
                    <table>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_DescBreveLabel" SkinID="FieldDescription" runat="server" Text="Descrizione breve *"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <CustomWebControls:TextArea runat="server" ID="DescBreveTextBox" Columns="100" Rows="4"
                                    TextMode="MultiLine" ValidationGroup="myValidation" Text='<%# Bind("DescBreve1") %>'
                                    EnableTheming="True" Height="100px" MaxLength="600"></CustomWebControls:TextArea>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ErrorMessage="Obbligatorio"
                                    runat="server" ControlToValidate="DescBreveTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ValidationGroup="myValidation">
                                </asp:RequiredFieldValidator>
                                     <asp:RegularExpressionValidator Display="Dynamic" SkinID="ZSSM_Validazione01" ControlToValidate="DescBreveTextBox" ValidationGroup="myValidation" ID="RegularExpressionValidator18" ValidationExpression = "^[\s\S]{0,580}$" runat="server" ErrorMessage="Massimo 580 caratteri"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <asp:Panel ID="ZMCF_BoxContenuto1" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="ZML_BoxContenuto1Label" runat="server" Text="Contenuto "></asp:Label>
                    </div>
                    <table>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_Contenuto1Label" SkinID="FieldDescription" runat="server" Text="Contenuto1"></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ErrorMessage="<br />Obbligatorio"
                                    runat="server" ControlToValidate="RadEditor2" SkinID="ZSSM_Validazione01" Display="Dynamic"
                                    ValidationGroup="myValidation">
                                </asp:RequiredFieldValidator>
                            </td>
                            <td class="BlockBoxValue">
                                <telerik:RadEditor
                                    Language="it-IT" ID="RadEditor2" runat="server"
                                    DocumentManager-DeletePaths="~/ZeusInc/Publisher/Documents"
                                    DocumentManager-SearchPatterns="*.*"
                                    DocumentManager-ViewPaths="~/ZeusInc/Publisher/Documents"
                                    DocumentManager-MaxUploadFileSize="52428800"
                                    DocumentManager-UploadPaths="~/ZeusInc/Publisher/Documents"
                                    FlashManager-DeletePaths="~/ZeusInc/Publisher/Media"
                                    FlashManager-MaxUploadFileSize="10240000"
                                    FlashManager-ViewPaths="~/ZeusInc/Publisher/Media"
                                    FlashManager-UploadPaths="~/ZeusInc/Publisher/Media"
                                    ImageManager-DeletePaths="~/ZeusInc/Publisher/Images"
                                    ImageManager-ViewPaths="~/ZeusInc/Publisher/Images"
                                    ImageManager-MaxUploadFileSize="10240000"
                                    ImageManager-SearchPatterns="*.gif, *.png, *.jpg, *.jpe, *.jpeg"
                                    ImageManager-UploadPaths="~/ZeusInc/Publisher/Images"
                                    ImageManager-ViewMode="Grid"
                                    MediaManager-DeletePaths="~/ZeusInc/Publisher/Media"
                                    MediaManager-MaxUploadFileSize="10240000"
                                    MediaManager-SearchPatterns="*.asf, *.asx, *.wma, *.wmv, *.avi, *.wav, *.mpeg, *.mpg, *.mpe, *.mov, *.mp3, *.m3u, *.mid, *.midi, *.rm, *.rma"
                                    MediaManager-ViewPaths="~/ZeusInc/Publisher/Media"
                                    MediaManager-UploadPaths="~/ZeusInc/Publisher/Media"
                                    TemplateManager-SearchPatterns="*.html,*.htm"
                                    ContentAreaMode="iframe"
                                    OnClientCommandExecuted="editorCommandExecuted"
                                    OnClientModeChange="OnClientModeChange"
                                    Content='<%# Bind("Contenuto1") %>'
                                    ToolsFile="~/Zeus/Publisher/RadEditor1.xml"
                                    LocalizationPath="~/App_GlobalResources"
                                    AllowScripts="true" RenderMode="Classic" ToolbarMode="Default" EnableViewState="False"
                                    Width="700px" Height="500px">
                                    <CssFiles>
                                        <telerik:EditorCssFile Value="~/asset/css/ZeusTypeFoundry.css" />
                                    </CssFiles>
                                </telerik:RadEditor>

                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <asp:Panel ID="ZMCF_BoxContenuto2" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="ZML_BoxContenuto2Label" runat="server" Text="Contenuto "></asp:Label>
                    </div>
                    <table>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_Contenuto2Label" SkinID="FieldDescription" runat="server" Text="Contenuto2"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <telerik:RadEditor
                                    Language="it-IT" ID="RadEditor3" runat="server"
                                    DocumentManager-DeletePaths="~/ZeusInc/Publisher/Documents"
                                    DocumentManager-SearchPatterns="*.*"
                                    DocumentManager-ViewPaths="~/ZeusInc/Publisher/Documents"
                                    DocumentManager-MaxUploadFileSize="52428800"
                                    DocumentManager-UploadPaths="~/ZeusInc/Publisher/Documents"
                                    FlashManager-DeletePaths="~/ZeusInc/Publisher/Media"
                                    FlashManager-MaxUploadFileSize="10240000"
                                    FlashManager-ViewPaths="~/ZeusInc/Publisher/Media"
                                    FlashManager-UploadPaths="~/ZeusInc/Publisher/Media"
                                    ImageManager-DeletePaths="~/ZeusInc/Publisher/Images"
                                    ImageManager-ViewPaths="~/ZeusInc/Publisher/Images"
                                    ImageManager-MaxUploadFileSize="10240000"
                                    ImageManager-SearchPatterns="*.gif, *.png, *.jpg, *.jpe, *.jpeg"
                                    ImageManager-UploadPaths="~/ZeusInc/Publisher/Images"
                                    ImageManager-ViewMode="Grid"
                                    MediaManager-DeletePaths="~/ZeusInc/Publisher/Media"
                                    MediaManager-MaxUploadFileSize="10240000"
                                    MediaManager-SearchPatterns="*.asf, *.asx, *.wma, *.wmv, *.avi, *.wav, *.mpeg, *.mpg, *.mpe, *.mov, *.mp3, *.m3u, *.mid, *.midi, *.rm, *.rma"
                                    MediaManager-ViewPaths="~/ZeusInc/Publisher/Media"
                                    MediaManager-UploadPaths="~/ZeusInc/Publisher/Media"
                                    TemplateManager-SearchPatterns="*.html,*.htm"
                                    ContentAreaMode="iframe"
                                    OnClientCommandExecuted="editorCommandExecuted"
                                    OnClientModeChange="OnClientModeChange"
                                    Content='<%# Bind("Contenuto2") %>'
                                    ToolsFile="~/Zeus/Publisher/RadEditor2.xml"
                                    LocalizationPath="~/App_GlobalResources"
                                    AllowScripts="true" RenderMode="Classic" ToolbarMode="Default" EnableViewState="False"
                                    Width="700px" Height="500px">
                                    <CssFiles>
                                        <telerik:EditorCssFile Value="~/asset/css/ZeusTypeFoundry.css" />
                                    </CssFiles>
                                </telerik:RadEditor>

                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <asp:Panel ID="ZMCF_BoxContenuto3" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="ZML_BoxContenuto3Label" runat="server" Text="Introduzione "></asp:Label>
                    </div>
                    <table>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_Contenuto3Label" SkinID="FieldDescription" runat="server" Text="Contenuto3"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <telerik:RadEditor
                                    Language="it-IT" ID="RadEditor1" runat="server"
                                    ContentAreaMode="iframe"
                                    OnClientCommandExecuted="editorCommandExecuted"
                                    OnClientModeChange="OnClientModeChange"
                                    Content='<%# Bind("Contenuto3") %>'
                                    ToolsFile="~/Zeus/Publisher/RadEditor3.xml"
                                    LocalizationPath="~/App_GlobalResources"
                                    AllowScripts="false" RenderMode="Classic" ToolbarMode="Default" EnableViewState="False" EditModes="Design,Preview"  StripFormattingOptions="AllExceptNewLines"
                                    Width="460px" Height="500px">
                                    <CssFiles>
                                        <telerik:EditorCssFile Value="~/asset/css/ZeusTypeFoundry.css" />
                                    </CssFiles>
                                </telerik:RadEditor>

                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <asp:Panel ID="ZMCF_Autore" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="Label26" runat="server" Text="Autore "></asp:Label>
                    </div>
                    <table>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label11" SkinID="FieldDescription" runat="server" Text="Autore"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="AutoreTextBox" runat="server" MaxLength="100" Columns="50" Text='<%# Bind("Autore") %>'></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label22" SkinID="FieldDescription" runat="server" Text="E-Mail autore"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="Autore_EmailTextBox" runat="server" MaxLength="100" Columns="50"
                                    Text='<%# Bind("Autore_Email") %>'></asp:TextBox>
                                <asp:RegularExpressionValidator ID="MailRegulaeExpressionValidation" runat="server"
                                    Display="Dynamic" ValidationExpression="^((([a-z]|[0-9]|!|#|$|%|&|'|\*|\+|\-|/|=|\?|\^|_|`|\{|\||\}|~)+(\.([a-z]|[0-9]|!|#|$|%|&|'|\*|\+|\-|/|=|\?|\^|_|`|\{|\||\}|~)+)*)@((((([a-z]|[0-9])([a-z]|[0-9]|\-){0,61}([a-z]|[0-9])\.))*([a-z]|[0-9])([a-z]|[0-9]|\-){0,61}([a-z]|[0-9])\.(af|ax|al|dz|as|ad|ao|ai|aq|ag|ar|am|aw|au|at|az|bs|bh|bd|bb|by|be|bz|bj|bm|bt|bo|ba|bw|bv|br|io|bn|bg|bf|bi|kh|cm|ca|cv|ky|cf|td|cl|cn|cx|cc|co|km|cg|cd|ck|cr|ci|hr|cu|cy|cz|dk|dj|dm|do|ec|eg|sv|gq|er|ee|et|fk|fo|fj|fi|fr|gf|pf|tf|ga|gm|ge|de|gh|gi|gr|gl|gd|gp|gu|gt| gg|gn|gw|gy|ht|hm|va|hn|hk|hu|is|in|id|ir|iq|ie|im|il|it|jm|jp|je|jo|kz|ke|ki|kp|kr|kw|kg|la|lv|lb|ls|lr|ly|li|lt|lu|mo|mk|mg|mw|my|mv|ml|mt|mh|mq|mr|mu|yt|mx|fm|md|mc|mn|ms|ma|mz|mm|na|nr|np|nl|an|nc|nz|ni|ne|ng|nu|nf|mp|no|om|pk|pw|ps|pa|pg|py|pe|ph|pn|pl|pt|pr|qa|re|ro|ru|rw|sh|kn|lc|pm|vc|ws|sm|st|sa|sn|cs|sc|sl|sg|sk|si|sb|so|za|gs|es|lk|sd|sr|sj|sz|se|ch|sy|tw|tj|tz|th|tl|tg|tk|to|tt|tn|tr|tm|tc|tv|ug|ua|ae|gb|us|um|uy|uz|vu|ve|vn|vg|vi|wf|eh|ye|zm|zw|com|edu|gov|int|mil|net|org|biz|info|name|pro|aero|coop|museum|arpa))|(((([0-9]){1,3}\.){3}([0-9]){1,3}))|(\[((([0-9]){1,3}\.){3}([0-9]){1,3})\])))$"
                                    ControlToValidate="Autore_EmailTextBox" ErrorMessage="Indirizzo E-Mail non corretto"
                                    SkinID="ZSSM_Validazione01"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <asp:Panel ID="ZMCF_BoxFonte" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="ZML_TitleFonteLabel" runat="server" Text="Fonte "></asp:Label>
                    </div>
                    <table>
                        <asp:Panel ID="ZMCF_Fonte" runat="server">
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label ID="ZML_BoxFonteLabel" SkinID="FieldDescription" runat="server" Text="Fonte"></asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:TextBox ID="FonteTextBox" runat="server" MaxLength="100" Columns="50" Text='<%# Bind("Fonte") %>'></asp:TextBox>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_LinkFonte" runat="server">
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label ID="ZML_BoxLinkFonteLabel" SkinID="FieldDescription" runat="server" Text="Link Fonte"></asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:TextBox ID="Fonte_UrlTextBox" runat="server" MaxLength="200" Columns="50" Text='<%# Bind("Fonte_Url") %>'></asp:TextBox>
                                    <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="RegularExpressionValidator2"
                                        runat="server" ControlToValidate="Fonte_UrlTextBox" Display="Dynamic" ErrorMessage="Inserire un percorso completo e corretto"
                                        ValidationExpression="^(ht|f)tp(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&%\$#_]*)?$"
                                        SkinID="ZSSM_Validazione01">Inserire un percorso completo e corretto</asp:RegularExpressionValidator>
                                </td>
                            </tr>
                        </asp:Panel>
                    </table>
                </div>
            </asp:Panel>
            <dlc:ImageRaider ID="ImageRaider1" runat="server" ZeusIdModuloIndice="1" ZeusLangCode="ITA"
                BindPzFromDB="true" IsEditMode="true" DefaultEditBetaImage='<%# Eval("Immagine1") %>'
                DefaultEditGammaImage='<%# Eval("Immagine2") %>'
                ImageAlt='<%# Eval("Immagine12Alt") %>' OnDataBinding="ImageRaider_DataBinding" />
            <asp:HiddenField ID="FileNameBetaHiddenField" runat="server" Value='<%# Bind("Immagine1") %>' />
            <asp:HiddenField ID="FileNameGammaHiddenField" runat="server" Value='<%# Bind("Immagine2") %>' />
            <asp:HiddenField ID="Immagine12AltHiddenField" runat="server" Value='<%# Bind("Immagine12Alt") %>' />
            <asp:CustomValidator ID="FileUploadCustomValidator" runat="server" OnServerValidate="FileUploadCustomValidator_ServerValidate"
                Display="Dynamic" SkinID="ZSSM_Validazione01" ValidationGroup="myValidation"></asp:CustomValidator>
            <dlc:ImageRaider ID="ImageRaider2" runat="server" ZeusIdModuloIndice="2" ZeusLangCode="ITA"
                BindPzFromDB="true" IsEditMode="true" DefaultEditBetaImage='<%# Eval("Immagine3") %>'
                DefaultEditGammaImage='<%# Eval("Immagine4") %>' 
                ImageAlt='<%# Eval("Immagine34Alt") %>'
                OnDataBinding="ImageRaider_DataBinding" />
            <asp:HiddenField ID="FileNameBeta2HiddenField" runat="server" Value='<%# Bind("Immagine3") %>' />
            <asp:HiddenField ID="FileNameGamma2HiddenField" runat="server" Value='<%# Bind("Immagine4") %>' />
            <asp:HiddenField ID="Immagine34AltHiddenField" runat="server" Value='<%# Bind("Immagine34Alt") %>' />
            <asp:CustomValidator ID="FileUpload2CustomValidator" runat="server" OnServerValidate="FileUpload2CustomValidator_ServerValidate"
                Display="Dynamic" SkinID="ZSSM_Validazione01" ValidationGroup="myValidation"></asp:CustomValidator>
            <asp:Panel ID="ZMCF_Allegato" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="AllegatoLabel" runat="server" Text="Allegato"></asp:Label>
                    </div>
                    <table>
                        <asp:Panel ID="ZMCF_AllegatoDescrizione" runat="server">
                            <asp:Panel ID="Allegato1_DescPanel" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="DescrizioneAllegatoLabel" runat="server" SkinID="FieldDescription"
                                            Text="Descrizione"></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:TextBox ID="Allegato1_DescTextBox" runat="server" Text='<%# Bind("Allegato1_Desc", "{0}") %>'
                                            Columns="100" MaxLength="100"></asp:TextBox>
                                        <asp:CustomValidator ValidationGroup="myValidation" ID="CustomValidator6" runat="server"
                                            ErrorMessage="Obbligatorio" ClientValidationFunction="CheckDescriptionLength"
                                            Display="Dynamic" SkinID="ZSSM_Validazione01"></asp:CustomValidator>
                                    </td>
                                </tr>
                            </asp:Panel>
                        </asp:Panel>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="FileLabel" runat="server" SkinID="FieldDescription" Text="File"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:RadioButton ID="RequiredFile" runat="server" GroupName="AllegatoSelettore" OnCheckedChanged="RadioButton1_CheckedChanged" />
                                <dlc:UploadRaider ID="Allegato_UploadRaider1" runat="server" SavePath="/ZeusInc/Publisher/Documents/"
                                    FileTypeRange="*.*;" MaxSizeKb="100 MB" />
                                <asp:HiddenField ID="Allegato1_File" runat="server" Value='<%# Bind("Allegato1_File", "{0}") %>' />
                                <asp:HyperLink runat="server" ID="FileUploadLink" NavigateUrl='<%# Eval("Allegato1_File", "{0}") %>'
                                    OnDataBinding="FileUploadLink_DataBinding" SkinID="FieldValue">[FileUploadLink]</asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="UrlLabel" runat="server" SkinID="FieldDescription" Text="Url"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:RadioButton ID="RequiredURL" runat="server" GroupName="AllegatoSelettore" OnCheckedChanged="RadioButton2_CheckedChanged" />
                                <asp:TextBox ID="Allegato1_UrlEsternoTextBox" runat="server" Text='<%# Bind("Allegato1_UrlEsterno", "{0}") %>'
                                    Columns="100" MaxLength="80"></asp:TextBox>
                                <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="Url_Validator"
                                    runat="server" ControlToValidate="Allegato1_UrlEsternoTextBox" Display="Dynamic"
                                    ErrorMessage="Inserire un percorso completo e corretto" ValidationExpression="^(ht|f)tp(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&%\$#=_]*)?$"
                                    SkinID="ZSSM_Validazione01">Inserire un percorso completo e corretto</asp:RegularExpressionValidator>
                                <asp:CustomValidator ValidationGroup="myValidation" ID="CustomValidator7" runat="server"
                                    ErrorMessage="Obbligatorio" ClientValidationFunction="CheckUrlLength" Display="Dynamic"
                                    SkinID="ZSSM_Validazione01"></asp:CustomValidator>
                                <asp:HiddenField ID="Allegato1_UrlEsternoHiddenField" runat="server" Value='<%# Eval("Allegato1_UrlEsterno", "{0}") %>' />
                                <asp:LinkButton runat="server" ID="UrlLinkButton" PostBackUrl='<%# Eval("Allegato1_UrlEsterno", "{0}") %>'
                                    Text="Apri 1" OnDataBinding="UrlLinkButton_DataBinding" SkinID="ZSSM_Button01"></asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="NessunoLabel" runat="server" SkinID="FieldDescription" Text="Nessuno"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:RadioButton ID="SelettoreNull" runat="server" GroupName="AllegatoSelettore"
                                    OnCheckedChanged="SelettoreNull_CheckedChanged" />
                                <asp:CustomValidator ValidationGroup="myValidation" ID="RadioButton_Validator" runat="server"
                                    ErrorMessage="Obbligatorio" OnServerValidate="Check_RadioButton_Validate" Display="Dynamic"
                                    SkinID="ZSSM_Validazione01"></asp:CustomValidator>
                                <asp:HiddenField ID="Allegato_Peso" runat="server" Value='<%# Bind("Allegato1_Peso", "{0}") %>' />
                                <asp:HiddenField ID="Allegato_Selettore" runat="server" Value='<%# Bind("Allegato1_Selettore", "{0}") %>'
                                    OnDataBinding="Allegato_Selettore_DataBinding" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <asp:Panel ID="ZMCF_JollyFlag1" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="Label39" runat="server" Text="Proprietà"></asp:Label>
                    </div>
                    <table>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_JollyFlag1Label" runat="server" SkinID="FieldDescription" Text="Label"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:CheckBox ID="JollyFlag1CheckBox" runat="server" Checked='<%# Bind("JollyFlag1") %>' />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <asp:Panel ID="ZMCF_BoxCategoria2" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="ZML_BoxCategoria2Label" runat="server" Text="Sezioni associate"></asp:Label>
                    </div>
                    <dlc:CategorieMultiple ID="CategorieMultiple1" runat="server" COLUMN_ID="ID2Publisher"
                        COLUMN_IDCAT="ID2Categoria2" TABLE="tbxPublisher_ContenutiCategorie" MODE="EDT" />
                </div>
            </asp:Panel>
            <asp:Panel ID="ZMCF_BoxSEO" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="Label9" runat="server" Text="SEO Search Engine Optimization"></asp:Label>
                    </div>
                    <table>
                        <asp:Panel ID="ZMCF_TitoloBrowser" runat="server">
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label ID="Label41" runat="server" SkinID="FieldDescription" Text="Label">TITLE / Titolo Browser </asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:TextBox ID="TitoloBrowserTextBox" runat="server" Text='<%# Bind("TitoloBrowser") %>'
                                        Columns="100" MaxLength="100"></asp:TextBox>
                                    <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator24" runat="server"
                                    ValidationExpression="^[^']+$" Display="Dynamic" ErrorMessage="Non sono consentiti i caratteri: '"
                                    SkinID="ZSSM_Validazione01" ControlToValidate="TitoloBrowserTextBox"></asp:RegularExpressionValidator>--%>
                                </td>
                            </tr>
                        </asp:Panel>

                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label13" runat="server" SkinID="FieldDescription" Text="Label">Description</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="ZMCD_TagDescription" runat="server" Text='<%# Bind("TagDescription") %>'
                                    Columns="130" MaxLength="280"></asp:TextBox>
                                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator22" runat="server"
                                ValidationExpression="^[a-zA-Z0-9_.;:+, ]+$" Display="Dynamic" ErrorMessage="Caratteri consentiti: lettere numeri e .,:;_-+"
                                SkinID="ZSSM_Validazione01" ControlToValidate="ZMCD_TagDescription"></asp:RegularExpressionValidator>--%>
                            </td>
                        </tr>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label14" runat="server" SkinID="FieldDescription" Text="Label">Keywords</asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:TextBox ID="ZMCD_TagKeywords" runat="server" Text='<%# Bind("TagKeywords") %>'
                                    Columns="130" MaxLength="280"></asp:TextBox>
                                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator23" runat="server"
                                ValidationExpression="^[a-zA-Z0-9_.;:+, ]+$" Display="Dynamic" ErrorMessage="Caratteri consentiti: lettere numeri e .,:;_-+"
                                SkinID="ZSSM_Validazione01" ControlToValidate="ZMCD_TagKeywords"></asp:RegularExpressionValidator>--%>
                            </td>
                        </tr>
                        <asp:Panel ID="ZMCF_TagMeta1" runat="server">
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label ID="ZML_TagMeta1Label" runat="server" SkinID="FieldDescription" Text="Label">Description</asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:DropDownList ID="TagMeta1AttributeDropDownList" runat="server" SelectedValue='<%# Bind("TagMeta1Attribute") %>'>
                                        <asp:ListItem Text="name" Value="name" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="http-equiv" Value="http-equiv"></asp:ListItem>
                                        <asp:ListItem Text="property" Value="property"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:Label ID="Value1Label" runat="server" SkinID="FieldValue">Value</asp:Label>
                                    <asp:TextBox ID="TagMeta1ValueTextBox" runat="server" Text='<%# Bind("TagMeta1Value") %>'
                                        Columns="20" MaxLength="30"></asp:TextBox>
                                    <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator20" runat="server"
                                    ValidationExpression="^[a-zA-Z0-9_.;:+, ]+$" Display="Dynamic" ErrorMessage="Caratteri consentiti: lettere numeri e .,:;_-+"
                                    SkinID="ZSSM_Validazione01" ControlToValidate="TagMeta1ValueTextBox"></asp:RegularExpressionValidator>--%>
                                    <img src="../SiteImg/Spc.gif" width="10" />
                                    <asp:Label ID="Label5" runat="server" SkinID="FieldValue">Content </asp:Label>
                                    <asp:TextBox ID="TagMeta1ContentTextBox" runat="server" Text='<%# Bind("TagMeta1Content") %>'
                                        Columns="85" MaxLength="280"></asp:TextBox>
                                    <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator18" runat="server"
                                    ValidationExpression="^[a-zA-Z0-9_.;:+, ]+$" Display="Dynamic" ErrorMessage="Caratteri consentiti: lettere numeri e .,:;_-+"
                                    SkinID="ZSSM_Validazione01" ControlToValidate="TagMeta1ContentTextBox"></asp:RegularExpressionValidator>--%>
                                    <asp:CustomValidator ID="CustomValidator4" runat="server" ValidationGroup="myValidation"
                                        Display="Dynamic" ClientValidationFunction="BothRequired1" ErrorMessage="I campi Value e Content devono essere entrambi compilati oppure vuoti"
                                        SkinID="ZSSM_Validazione01"></asp:CustomValidator>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_TagMeta2" runat="server">
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label ID="ZML_TagMeta2Label" runat="server" SkinID="FieldDescription" Text="Label">Description</asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:DropDownList ID="TagMeta2AttributeDropDownList" runat="server" SelectedValue='<%# Bind("TagMeta2Attribute") %>'>
                                        <asp:ListItem Text="name" Value="name" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="http-equiv" Value="http-equiv"></asp:ListItem>
                                        <asp:ListItem Text="property" Value="property"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:Label ID="Value2Label" runat="server" SkinID="FieldValue">Value</asp:Label>
                                    <asp:TextBox ID="TagMeta2ValueTextBox" runat="server" Text='<%# Bind("TagMeta2Value") %>'
                                        Columns="20" MaxLength="30"></asp:TextBox>
                                    <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator19" runat="server"
                                    ValidationExpression="^[a-zA-Z0-9_.;:+, ]+$" Display="Dynamic" ErrorMessage="Caratteri consentiti: lettere numeri e .,:;_-+"
                                    SkinID="ZSSM_Validazione01" ControlToValidate="TagMeta2ValueTextBox"></asp:RegularExpressionValidator>--%>
                                    <img src="../SiteImg/Spc.gif" width="10" />
                                    <asp:Label ID="Label40" runat="server" SkinID="FieldValue">Content </asp:Label>
                                    <asp:TextBox ID="TagMeta2ContentTextBox" runat="server" Text='<%# Bind("TagMeta2Content") %>'
                                        Columns="85" MaxLength="280"></asp:TextBox>
                                    <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator21" runat="server"
                                    ValidationExpression="^[a-zA-Z0-9_.;:+, ]+$" Display="Dynamic" ErrorMessage="Caratteri consentiti: lettere numeri e .,:;_-+"
                                    SkinID="ZSSM_Validazione01" ControlToValidate="TagMeta2ContentTextBox"></asp:RegularExpressionValidator>--%>
                                    <asp:CustomValidator ID="CustomValidator5" runat="server" ValidationGroup="myValidation"
                                        Display="Dynamic" ClientValidationFunction="BothRequired2" ErrorMessage="I campi Value e Content devono essere entrambi compilati oppure vuoti"
                                        SkinID="ZSSM_Validazione01"></asp:CustomValidator>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="ZMCF_UrlRewrite" runat="server">
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label ID="Label7" runat="server" Text="Page Link / URL Rewriting" SkinID="FieldDescription"></asp:Label>
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:Label ID="ZMCD_UrlDomain" runat="server" SkinID="FieldValue"></asp:Label><asp:Label
                                        ID="ZMCD_UrlSection" runat="server" SkinID="FieldValue"></asp:Label><asp:Label
                                            ID="ID1Publisher" runat="server" SkinID="FieldValue" Text='<%# Eval("ID1Publisher","{0}/") %>'></asp:Label><asp:TextBox
                                                ID="UrlPageDetailTextBox" runat="server" Text='<%# Eval("UrlRewrite") %>' Width="400"></asp:TextBox>
                                    
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator27" ErrorMessage="Obbligatorio"
                                        runat="server" ControlToValidate="UrlPageDetailTextBox" Display="Dynamic" SkinID="ZSSM_Validazione01"
                                        ValidationGroup="myValidation">
                                    </asp:RequiredFieldValidator>
                                    <asp:Label ID="InfoUrlRewritingLabel" runat="server" Text="<br />Possono essere utilizzati solo i caratteri alfanumerici ed i caratteri - _  Tutti gli altri caratteri verranno sostituiti automaticamente."
                                        SkinID="Note"></asp:Label>
                                    <asp:HiddenField ID="UrlRewriteHiddenField" runat="server" Value='<%# Bind("UrlRewrite") %>' />
                                </td>
                            </tr>
                        </asp:Panel>
                    </table>
                </div>
            </asp:Panel>

            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="Planner" runat="server" Text="Planner"></asp:Label>
                </div>
                <table>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="ZML_PWArea1_EdtLabel" SkinID="FieldDescription" runat="server"></asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:CheckBox ID="PW_Area1CheckBox" runat="server" Checked='<%# Bind("PW_Area1") %>' />
                            <asp:Label ID="AttivaDa_Label" runat="server" Text="Attiva pubblicazione dalla data"
                                SkinID="FieldValue"></asp:Label>
                            <asp:TextBox ID="PWI_Area1TextBox" runat="server" Text='<%# Bind("PWI_Area1", "{0:d}") %>'
                                Columns="10" MaxLength="10" OnDataBinding="PWI_Area1TextBox_DataBinding"></asp:TextBox>
                            <asp:Image ID="Calendar1" runat="server" ImageUrl="~/Zeus/SiteImg/Calendario.jpg" />
                            <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator3"
                                runat="server" ControlToValidate="PWI_Area1TextBox" SkinID="ZSSM_Validazione01"
                                Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="PWI_Area1RegularExpressionValidator"
                                runat="server" ControlToValidate="PWI_Area1TextBox" SkinID="ZSSM_Validazione01"
                                Display="Dynamic" ErrorMessage="*" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$">Formato data richiesto: GG/MM/AAAA</asp:RegularExpressionValidator>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="PWI_Area1TextBox"
                                PopupButtonID="Calendar1" Format="dd/MM/yyyy" />
                            <asp:Label ID="Label12" runat="server" Text="ora" SkinID="FieldValue"></asp:Label>
                            <asp:TextBox ID="PWI_Area1OreTextBox" runat="server" Width="15" MaxLength="2"></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator5"
                                runat="server" ControlToValidate="PWI_Area1OreTextBox" SkinID="ZSSM_Validazione01"
                                Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="RegularExpressionValidator1"
                                runat="server" ControlToValidate="PWI_Area1OreTextBox" SkinID="ZSSM_Validazione01"
                                Display="Dynamic" ErrorMessage="*" ValidationExpression="([0-1][0-9]|2[0-3])|[0123456789]">Inserire un valore numerico compreso tra 0 e 23</asp:RegularExpressionValidator>
                            <asp:Label ID="Label10" runat="server" Text="minuti" SkinID="FieldValue"></asp:Label>
                            <asp:TextBox ID="PWI_Area1MinutiTextBox" runat="server" Width="15" MaxLength="2"></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator6"
                                runat="server" ControlToValidate="PWI_Area1MinutiTextBox" SkinID="ZSSM_Validazione01"
                                Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="RegularExpressionValidator6"
                                runat="server" ControlToValidate="PWI_Area1MinutiTextBox" SkinID="ZSSM_Validazione01"
                                Display="Dynamic" ErrorMessage="*" ValidationExpression="([0-5][0-9])|[0123456789]">Inserire un valore numerico compreso tra 0 e 59</asp:RegularExpressionValidator>
                            <asp:Label ID="AttivaA_Label" runat="server" Text="alla data" SkinID="FieldValue"></asp:Label>
                            <asp:TextBox ID="PWF_Area1TextBox" runat="server" Text='<%# Bind("PWF_Area1", "{0:d}") %>'
                                Columns="10" MaxLength="10" OnDataBinding="PWF_Area1TextBox_DataBinding"></asp:TextBox>
                            <asp:Image ID="Calendario2" runat="server" ImageUrl="~/Zeus/SiteImg/Calendario.jpg" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="PWF_Area1TextBox"
                                Format="dd/MM/yyyy" PopupButtonID="Calendario2" />
                            <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator4"
                                runat="server" ControlToValidate="PWF_Area1TextBox" SkinID="ZSSM_Validazione01"
                                Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="PWF_Area1RegularExpressionValidator" runat="server"
                                ControlToValidate="PWF_Area1TextBox" SkinID="ZSSM_Validazione01" Display="Dynamic"
                                ErrorMessage="*" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$">Formato data richiesto: GG/MM/AAAA</asp:RegularExpressionValidator><asp:Label
                                    ID="Label15" runat="server" Text="ora" SkinID="FieldValue"></asp:Label>
                            <asp:TextBox ID="PWF_Area1OreTextBox" runat="server" Width="15" MaxLength="2"></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator7"
                                runat="server" ControlToValidate="PWF_Area1OreTextBox" SkinID="ZSSM_Validazione01"
                                Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="RegularExpressionValidator7"
                                runat="server" ControlToValidate="PWF_Area1OreTextBox" SkinID="ZSSM_Validazione01"
                                Display="Dynamic" ErrorMessage="*" ValidationExpression="([0-1][0-9]|2[0-3])|[0123456789]">Inserire un valore numerico compreso tra 0 e 23</asp:RegularExpressionValidator>
                            <asp:Label ID="Label16" runat="server" Text="minuti" SkinID="FieldValue"></asp:Label>
                            <asp:TextBox ID="PWF_Area1MinutiTextBox" runat="server" Width="15" MaxLength="2"></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator8"
                                runat="server" ControlToValidate="PWF_Area1MinutiTextBox" SkinID="ZSSM_Validazione01"
                                Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="RegularExpressionValidator8"
                                runat="server" ControlToValidate="PWF_Area1MinutiTextBox" SkinID="ZSSM_Validazione01"
                                Display="Dynamic" ErrorMessage="*" ValidationExpression="([0-5][0-9])|[0123456789]">Inserire un valore numerico compreso tra 0 e 59</asp:RegularExpressionValidator>
                            <%-- <asp:CompareValidator ValidationGroup="myValidation" ID="PW_Area1CompareValidator"
                                runat="server" ControlToCompare="PWI_Area1TextBox" ControlToValidate="PWF_Area1TextBox"
                                SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="La data iniziale deve essere precedente o uguale alla data finale"
                                Operator="GreaterThan" Type="Date"></asp:CompareValidator>--%>
                            <asp:CustomValidator ID="CustomValidator1" runat="server" ValidationGroup="myValidation"
                                Display="Dynamic" ClientValidationFunction="CompareDateArea1" ErrorMessage="La data iniziale deve essere precedente o uguale alla data finale"
                                ControlToValidate="PWI_Area2TextBox" SkinID="ZSSM_Validazione01"></asp:CustomValidator>
                        </td>
                    </tr>
                    <asp:Panel ID="ZMCF_Area2_Edt" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_PWArea2_EdtLabel" SkinID="FieldDescription" runat="server"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:CheckBox ID="PW_Area2CheckBox" runat="server" Checked='<%# Bind("PW_Area2") %>' />
                                <asp:Label ID="AttivaDa2_Label" runat="server" SkinID="FieldValue" Text="Attiva pubblicazione dalla data"></asp:Label>
                                <asp:TextBox ID="PWI_Area2TextBox" runat="server" Text='<%# Bind("PWI_Area2", "{0:d}") %>'
                                    Columns="10" MaxLength="10" OnDataBinding="PWI_Area1TextBox_DataBinding"></asp:TextBox>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="PWI_Area2TextBox"
                                    PopupButtonID="Calendario3" Format="dd/MM/yyyy" />
                                <asp:Image ID="Calendario3" runat="server" ImageUrl="~/Zeus/SiteImg/Calendario.jpg" />
                                <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator2"
                                    runat="server" ControlToValidate="PWI_Area2TextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="PWI_Area2RegularExpressionValidator"
                                    runat="server" ControlToValidate="PWI_Area2TextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$">Formato data richiesto: GG/MM/AAAA</asp:RegularExpressionValidator>
                                <asp:Label ID="Label17" runat="server" Text="ora" SkinID="FieldValue"></asp:Label>
                                <asp:TextBox ID="PWI_Area2OreTextBox" runat="server" Width="15" MaxLength="2"></asp:TextBox>
                                <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator9"
                                    runat="server" ControlToValidate="PWI_Area2OreTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="RegularExpressionValidator9"
                                    runat="server" ControlToValidate="PWI_Area2OreTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*" ValidationExpression="([0-1][0-9]|2[0-3])|[0123456789]">Inserire un valore numerico compreso tra 0 e 23</asp:RegularExpressionValidator>
                                <asp:Label ID="Label18" runat="server" Text="minuti" SkinID="FieldValue"></asp:Label>
                                <asp:TextBox ID="PWI_Area2MinutiTextBox" runat="server" Width="15" MaxLength="2"></asp:TextBox>
                                <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator10"
                                    runat="server" ControlToValidate="PWI_Area2MinutiTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="PWF_Area2RegularExpressionValidator"
                                    runat="server" ControlToValidate="PWI_Area2MinutiTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*" ValidationExpression="([0-5][0-9])|[0123456789]">Inserire un valore numerico compreso tra 0 e 59</asp:RegularExpressionValidator>
                                <asp:Label ID="AttivaA2_Label" runat="server" SkinID="FieldValue" Text="alla data"></asp:Label>
                                <asp:TextBox ID="PWF_Area2TextBox" runat="server" Text='<%# Bind("PWF_Area2", "{0:d}") %>'
                                    Columns="10" MaxLength="10" OnDataBinding="PWF_Area1TextBox_DataBinding"></asp:TextBox>
                                <asp:Image ID="Calendario4" runat="server" ImageUrl="~/Zeus/SiteImg/Calendario.jpg" />
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server" TargetControlID="PWF_Area2TextBox"
                                    PopupButtonID="Calendario4" Format="dd/MM/yyyy" />
                                <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator32"
                                    runat="server" ControlToValidate="PWF_Area2TextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                        ID="RegularExpressionValidator5" runat="server" ControlToValidate="PWF_Area2TextBox"
                                        SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="*" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$">Formato data richiesto: GG/MM/AAAA</asp:RegularExpressionValidator>
                                <asp:Label ID="Label20" runat="server" Text="ora" SkinID="FieldValue"></asp:Label>
                                <asp:TextBox ID="PWF_Area2OreTextBox" runat="server" Width="15" MaxLength="2"></asp:TextBox>
                                <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator11"
                                    runat="server" ControlToValidate="PWF_Area2OreTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="RegularExpressionValidator11"
                                    runat="server" ControlToValidate="PWF_Area2OreTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*" ValidationExpression="([0-1][0-9]|2[0-3])|[0123456789]">Inserire un valore numerico compreso tra 0 e 23</asp:RegularExpressionValidator>
                                <asp:Label ID="Label21" runat="server" Text="minuti" SkinID="FieldValue"></asp:Label>
                                <asp:TextBox ID="PWF_Area2MinutiTextBox" runat="server" Width="15" MaxLength="2"></asp:TextBox>
                                <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator12"
                                    runat="server" ControlToValidate="PWF_Area2MinutiTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="RegularExpressionValidator12"
                                    runat="server" ControlToValidate="PWF_Area2MinutiTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*" ValidationExpression="([0-5][0-9])|[0123456789]">Inserire un valore numerico compreso tra 0 e 59</asp:RegularExpressionValidator>
                                <%--<asp:CompareValidator ValidationGroup="myValidation" ID="PW_Area2CompareValidator"
                                    runat="server" ControlToCompare="PWI_Area2TextBox" ControlToValidate="PWF_Area2TextBox"
                                    SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="La data iniziale deve essere precedente o uguale alla data finale"
                                    Operator="GreaterThanEqual" Type="Date"></asp:CompareValidator>--%>
                                <asp:CustomValidator ID="CustomValidator" runat="server" ValidationGroup="myValidation"
                                    Display="Dynamic" ClientValidationFunction="CompareDateArea2" ErrorMessage="La data iniziale deve essere precedente o uguale alla data finale"
                                    ControlToValidate="PWI_Area2TextBox" SkinID="ZSSM_Validazione01"></asp:CustomValidator>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_Area3_Edt" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_PWArea3_EdtLabel" SkinID="FieldDescription" runat="server"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:CheckBox ID="PW_Area3CheckBox" runat="server" Checked='<%# Bind("PW_Area3") %>' />
                                <asp:Label ID="Label25" runat="server" SkinID="FieldValue" Text="Attiva pubblicazione dalla data"></asp:Label>
                                <asp:TextBox ID="PWI_Area3TextBox" runat="server" Text='<%# Bind("PWI_Area3", "{0:d}") %>'
                                    Columns="10" MaxLength="10" OnDataBinding="PWI_Area1TextBox_DataBinding"></asp:TextBox>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender5" runat="server" TargetControlID="PWI_Area3TextBox"
                                    PopupButtonID="Calendario5" Format="dd/MM/yyyy" />
                                <asp:Image ID="Calendario5" runat="server" ImageUrl="~/Zeus/SiteImg/Calendario.jpg" />
                                <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator15"
                                    runat="server" ControlToValidate="PWI_Area3TextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="PWI_Area3RegularExpressionValidator"
                                    runat="server" ControlToValidate="PWI_Area3TextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$">Formato data richiesto: GG/MM/AAAA</asp:RegularExpressionValidator>
                                <asp:Label ID="Label27" runat="server" Text="ora" SkinID="FieldValue"></asp:Label>
                                <asp:TextBox ID="PWI_Area3OreTextBox" runat="server" Width="15" MaxLength="2" Text="00"></asp:TextBox>
                                <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator16"
                                    runat="server" ControlToValidate="PWI_Area3OreTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="RegularExpressionValidator3"
                                    runat="server" ControlToValidate="PWI_Area3OreTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*" ValidationExpression="([0-1][0-9]|2[0-3])|[0123456789]">Inserire un valore numerico compreso tra 0 e 23</asp:RegularExpressionValidator>
                                <asp:Label ID="Label28" runat="server" Text="minuti" SkinID="FieldValue"></asp:Label>
                                <asp:TextBox ID="PWI_Area3MinutiTextBox" runat="server" Width="15" MaxLength="2"
                                    Text="00"></asp:TextBox>
                                <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator17"
                                    runat="server" ControlToValidate="PWI_Area3MinutiTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="PWF_Area3RegularExpressionValidator"
                                    runat="server" ControlToValidate="PWI_Area3MinutiTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*" ValidationExpression="([0-5][0-9])|[0123456789]">Inserire un valore numerico compreso tra 0 e 59</asp:RegularExpressionValidator>
                                <asp:Label ID="Label29" runat="server" SkinID="FieldValue" Text="alla data"></asp:Label>
                                <asp:TextBox ID="PWF_Area3TextBox" runat="server" Text='<%# Bind("PWF_Area3", "{0:d}") %>'
                                    Columns="10" MaxLength="10" OnDataBinding="PWF_Area1TextBox_DataBinding"></asp:TextBox>
                                <asp:Image ID="Calendario6" runat="server" ImageUrl="~/Zeus/SiteImg/Calendario.jpg" />
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender6" runat="server" TargetControlID="PWF_Area3TextBox"
                                    PopupButtonID="Calendario6" Format="dd/MM/yyyy" />
                                <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator18"
                                    runat="server" ControlToValidate="PWF_Area3TextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                        ID="RegularExpressionValidator4" runat="server" ControlToValidate="PWF_Area3TextBox"
                                        SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="*" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$">Formato data richiesto: GG/MM/AAAA</asp:RegularExpressionValidator>
                                <asp:Label ID="Label30" runat="server" Text="ora" SkinID="FieldValue"></asp:Label>
                                <asp:TextBox ID="PWF_Area3OreTextBox" runat="server" Width="15" MaxLength="2" Text="23"></asp:TextBox>
                                <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator19"
                                    runat="server" ControlToValidate="PWF_Area3OreTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="RegularExpressionValidator10"
                                    runat="server" ControlToValidate="PWF_Area3OreTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*" ValidationExpression="([0-1][0-9]|2[0-3])|[0123456789]">Inserire un valore numerico compreso tra 0 e 23</asp:RegularExpressionValidator>
                                <asp:Label ID="Label31" runat="server" Text="minuti" SkinID="FieldValue"></asp:Label>
                                <asp:TextBox ID="PWF_Area3MinutiTextBox" runat="server" Width="15" MaxLength="2"
                                    Text="59"></asp:TextBox>
                                <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator20"
                                    runat="server" ControlToValidate="PWF_Area3MinutiTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="RegularExpressionValidator13"
                                    runat="server" ControlToValidate="PWF_Area3MinutiTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*" ValidationExpression="([0-5][0-9])|[0123456789]">Inserire un valore numerico compreso tra 0 e 59</asp:RegularExpressionValidator>
                                <%--<asp:CompareValidator ValidationGroup="myValidation" ID="PW_Area3CompareValidator"
                                    runat="server" ControlToCompare="PWI_Area3TextBox" ControlToValidate="PWF_Area3TextBox"
                                    SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="La data iniziale deve essere precedente o uguale alla data finale"
                                    Operator="GreaterThanEqual" Type="Date"></asp:CompareValidator>--%>
                                <asp:CustomValidator ID="CustomValidator2" runat="server" ValidationGroup="myValidation"
                                    Display="Dynamic" ClientValidationFunction="CompareDateArea3" ErrorMessage="La data iniziale deve essere precedente o uguale alla data finale"
                                    ControlToValidate="PWI_Area3TextBox" SkinID="ZSSM_Validazione01"></asp:CustomValidator>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_Area4_Edt" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="ZML_PWArea4_EdtLabel" SkinID="FieldDescription" runat="server"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:CheckBox ID="PW_Area4CheckBox" runat="server" Checked='<%# Bind("PW_Area4") %>' />
                                <asp:Label ID="Label33" runat="server" SkinID="FieldValue" Text="Attiva pubblicazione dalla data"></asp:Label>
                                <asp:TextBox ID="PWI_Area4TextBox" runat="server" Text='<%# Bind("PWI_Area4", "{0:d}") %>'
                                    Columns="10" MaxLength="10" OnDataBinding="PWI_Area1TextBox_DataBinding"></asp:TextBox>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender7" runat="server" TargetControlID="PWI_Area4TextBox"
                                    PopupButtonID="Calendario7" Format="dd/MM/yyyy" />
                                <asp:Image ID="Calendario7" runat="server" ImageUrl="~/Zeus/SiteImg/Calendario.jpg" />
                                <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator21"
                                    runat="server" ControlToValidate="PWI_Area4TextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="PWI_Area4RegularExpressionValidator"
                                    runat="server" ControlToValidate="PWI_Area4TextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$">Formato data richiesto: GG/MM/AAAA</asp:RegularExpressionValidator>
                                <asp:Label ID="Label34" runat="server" Text="ora" SkinID="FieldValue"></asp:Label>
                                <asp:TextBox ID="PWI_Area4OreTextBox" runat="server" Width="15" MaxLength="2" Text="00"></asp:TextBox>
                                <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator22"
                                    runat="server" ControlToValidate="PWI_Area4OreTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="RegularExpressionValidator14"
                                    runat="server" ControlToValidate="PWI_Area4OreTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*" ValidationExpression="([0-1][0-9]|2[0-3])|[0123456789]">Inserire un valore numerico compreso tra 0 e 23</asp:RegularExpressionValidator>
                                <asp:Label ID="Label35" runat="server" Text="minuti" SkinID="FieldValue"></asp:Label>
                                <asp:TextBox ID="PWI_Area4MinutiTextBox" runat="server" Width="15" MaxLength="2"
                                    Text="00"></asp:TextBox>
                                <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator23"
                                    runat="server" ControlToValidate="PWI_Area4MinutiTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="PWF_Area4RegularExpressionValidator"
                                    runat="server" ControlToValidate="PWI_Area4MinutiTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*" ValidationExpression="([0-5][0-9])|[0123456789]">Inserire un valore numerico compreso tra 0 e 59</asp:RegularExpressionValidator>
                                <asp:Label ID="Label36" runat="server" SkinID="FieldValue" Text="alla data"></asp:Label>
                                <asp:TextBox ID="PWF_Area4TextBox" runat="server" Text='<%# Bind("PWF_Area4", "{0:d}") %>'
                                    Columns="10" MaxLength="10" OnDataBinding="PWF_Area1TextBox_DataBinding"></asp:TextBox>
                                <asp:Image ID="Calendario8" runat="server" ImageUrl="~/Zeus/SiteImg/Calendario.jpg" />
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender8" runat="server" TargetControlID="PWF_Area4TextBox"
                                    PopupButtonID="Calendario8" Format="dd/MM/yyyy" />
                                <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator24"
                                    runat="server" ControlToValidate="PWF_Area4TextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                        ID="RegularExpressionValidator15" runat="server" ControlToValidate="PWF_Area4TextBox"
                                        SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="*" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$">Formato data richiesto: GG/MM/AAAA</asp:RegularExpressionValidator>
                                <asp:Label ID="Label37" runat="server" Text="ora" SkinID="FieldValue"></asp:Label>
                                <asp:TextBox ID="PWF_Area4OreTextBox" runat="server" Width="15" MaxLength="2" Text="23"></asp:TextBox>
                                <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator25"
                                    runat="server" ControlToValidate="PWF_Area4OreTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="RegularExpressionValidator16"
                                    runat="server" ControlToValidate="PWF_Area4OreTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*" ValidationExpression="([0-1][0-9]|2[0-3])|[0123456789]">Inserire un valore numerico compreso tra 0 e 23</asp:RegularExpressionValidator>
                                <asp:Label ID="Label38" runat="server" Text="minuti" SkinID="FieldValue"></asp:Label>
                                <asp:TextBox ID="PWF_Area4MinutiTextBox" runat="server" Width="15" MaxLength="2"
                                    Text="59"></asp:TextBox>
                                <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator26"
                                    runat="server" ControlToValidate="PWF_Area4MinutiTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="RegularExpressionValidator17"
                                    runat="server" ControlToValidate="PWF_Area4MinutiTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="*" ValidationExpression="([0-5][0-9])|[0123456789]">Inserire un valore numerico compreso tra 0 e 59</asp:RegularExpressionValidator>
                                <%--<asp:CompareValidator ValidationGroup="myValidation" ID="PW_Area4CompareValidator"
                                    runat="server" ControlToCompare="PWI_Area4TextBox" ControlToValidate="PWF_Area4TextBox"
                                    SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="La data iniziale deve essere precedente o uguale alla data finale"
                                    Operator="GreaterThanEqual" Type="Date"></asp:CompareValidator>--%>
                                <asp:CustomValidator ID="CustomValidator3" runat="server" ValidationGroup="myValidation"
                                    Display="Dynamic" ClientValidationFunction="CompareDateArea4" ErrorMessage="La data iniziale deve essere precedente o uguale alla data finale"
                                    ControlToValidate="PWI_Area4TextBox" SkinID="ZSSM_Validazione01"></asp:CustomValidator>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_Archivio" runat="server">
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="Label6" runat="server" SkinID="FieldDescription" Text="Archivio"></asp:Label>
                            </td>
                            <td class="BlockBoxValue">
                                <asp:CheckBox ID="ArchivioCheckBox" runat="server" Checked='<%# Bind("Archivio") %>' />
                                <asp:Label ID="Label19" runat="server" SkinID="FieldValue" Text="Archivia contenuto"></asp:Label>
                            </td>
                        </tr>
                    </asp:Panel>
                </table>
            </div>
            <asp:HiddenField ID="PWI_Area1HiddenField" runat="server" Value='<%# Eval("PWI_Area1") %>'
                OnDataBinding="PWI_Area1HiddenField_DataBinding" />
            <asp:HiddenField ID="PWF_Area1HiddenField" runat="server" Value='<%# Eval("PWF_Area1") %>'
                OnDataBinding="PWF_Area1HiddenField_DataBinding" />
            <asp:HiddenField ID="PWI_Area2HiddenField" runat="server" Value='<%# Eval("PWI_Area2") %>'
                OnDataBinding="PWI_Area2HiddenField_DataBinding" />
            <asp:HiddenField ID="PWF_Area2HiddenField" runat="server" Value='<%# Eval("PWF_Area2") %>'
                OnDataBinding="PWF_Area2HiddenField_DataBinding" />
            <asp:HiddenField ID="PWI_Area3HiddenField" runat="server" Value='<%# Eval("PWI_Area3") %>'
                OnDataBinding="PWI_Area3HiddenField_DataBinding" />
            <asp:HiddenField ID="PWF_Area3HiddenField" runat="server" Value='<%# Eval("PWF_Area3") %>'
                OnDataBinding="PWF_Area3HiddenField_DataBinding" />
            <asp:HiddenField ID="PWI_Area4HiddenField" runat="server" Value='<%# Eval("PWI_Area4") %>'
                OnDataBinding="PWI_Area4HiddenField_DataBinding" />
            <asp:HiddenField ID="PWF_Area4HiddenField" runat="server" Value='<%# Eval("PWF_Area4") %>'
                OnDataBinding="PWF_Area4HiddenField_DataBinding" />
            <asp:HiddenField ID="RecordEdtUserHidden" runat="server" Value='<%# Bind("RecordEdtUser", "{0}") %>'
                OnDataBinding="UtenteCreazione" />
            <asp:HiddenField ID="RecordEdtDateHidden" runat="server" Value='<%# Bind("RecordEdtDate", "{0}") %>'
                OnDataBinding="DataOggi" />
            <%--            <uc1:PublisherLangButton ID="PublisherLangButton1" runat="server" ZeusId='<%# Eval("ZeusId") %>'
                ZeusIdModulo='<%# Eval("ZeusIdModulo") %>' ZeusLangCode='<%# Eval("ZeusLangCode") %>' />--%>
            <div align="center">
                <dlc:mySummaryValidation ID="mySummaryValidation1" runat="server" SkinID="ZSSM_Validazione01" />
                <asp:LinkButton ID="InsertButton" runat="server" SkinID="ZSSM_Button01" CausesValidation="True"
                    CommandName="Update" Text="Salva dati" OnClick="Save_File_Upload" ValidationGroup="myValidation"
                    OnClientClick="this.blur();"></asp:LinkButton>
                <img src="../SiteImg/Spc.gif" width="20" />
                <asp:LinkButton ID="DeleteLinkButton" runat="server" Text="Elimina" SkinID="ZSSM_Button01"
                    OnClick="DeleteLinkButton_Click"></asp:LinkButton>
            </div>
            <div style="padding: 10px 0 0 0; margin: 0;">
                <dlc:zeusdatatracking ID="Zeusdatatracking1" runat="server" />
            </div>
            <asp:HiddenField ID="zdtRecordNewUser" runat="server" Value='<%# Eval("RecordNewUser") %>' />
            <asp:HiddenField ID="zdtRecordNewDate" runat="server" Value='<%# Eval("RecordNewDate") %>' />
            <asp:HiddenField ID="zdtRecordEdtUser" runat="server" Value='<%# Eval("RecordEdtUser") %>' />
            <asp:HiddenField ID="zdtRecordEdtDate" runat="server" Value='<%# Eval("RecordEdtDate") %>' />
        </EditItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="tbPublisherSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SELECT [ID1Publisher], [ZeusIdModulo], [ZeusLangCode], [Titolo],TitoloBrowser, [Sottotitolo], [ID2Categoria1],
         [DescBreve1],  [Contenuto1], [Contenuto2],[Contenuto3], [Immagine1], [Immagine2],[Immagine12Alt],[Immagine3], [Immagine4],
         [Immagine34Alt], [TagDescription], [TagKeywords], [JollyFlag1],[UrlRewrite], [Autore], [Autore_Email],[Fonte],[Fonte_Url],[Archivio], 
          [PW_Area1], [PWI_Area1], [PWF_Area1],[PW_Area3], [PWI_Area3], [PWF_Area3], [PWF_Area2], [PW_Area2], [PWI_Area2], 
          [PW_Area4], [PWI_Area4], [PWF_Area4],TagMeta1Value,TagMeta1Content,TagMeta2Value,TagMeta2Content,
          [ZeusId], [RecordEdtUser], [RecordEdtDate], [ZeusIsAlive],RecordNewDate, RecordNewUser,TagMeta1Attribute,TagMeta2Attribute,
          ZeusId,  ZeusIdModulo, ZeusLangCode , [Allegato1_Desc], [Allegato1_File]
        , [Allegato1_UrlEsterno], [Allegato1_Peso], [Allegato1_Selettore]      
          FROM [tbPublisher] WHERE [ID1Publisher] = @ID1Publisher  AND ZeusIdModulo=@ZeusIdModulo AND ZeusLangCode=@ZeusLangCode AND ZeusIsAlive=1"
        UpdateCommand=" SET DATEFORMAT dmy; UPDATE [tbPublisher] SET [Titolo] = @Titolo, TitoloBrowser=@TitoloBrowser,
           [Sottotitolo] = @Sottotitolo, [ID2Categoria1] = @ID2Categoria1, 
           [DescBreve1] = @DescBreve1, [Contenuto1] = @Contenuto1,
           [Contenuto2] = @Contenuto2,[Contenuto3]=@Contenuto3,
            [Immagine1] = @Immagine1,[Immagine2] = @Immagine2,
            [Immagine3] = @Immagine3,[Immagine4] = @Immagine4,
         [Immagine12Alt] = @Immagine12Alt,[Immagine34Alt] = @Immagine34Alt,
          [TagDescription] = @TagDescription, [TagKeywords] = @TagKeywords, [JollyFlag1]=@JollyFlag1,
          [UrlRewrite] = @UrlRewrite,[Autore] = @Autore,
          [Autore_Email]=@Autore_Email,[Fonte]=@Fonte,[Fonte_Url]=@Fonte_Url,
           [Archivio] = @Archivio, 
           [PW_Area1] = @PW_Area1, [PWI_Area1] = @PWI_Area1,[PWF_Area1] = @PWF_Area1, 
          [PW_Area2] = @PW_Area2, [PWI_Area2] = @PWI_Area2, [PWF_Area2] = @PWF_Area2, 
          [PW_Area3] = @PW_Area3, [PWI_Area3] = @PWI_Area3,[PWF_Area3] = @PWF_Area3,
          [PW_Area4] = @PW_Area4, [PWI_Area4] = @PWI_Area4,[PWF_Area4] = @PWF_Area4,
          [RecordEdtUser] = @RecordEdtUser, [RecordEdtDate] = @RecordEdtDate ,
          TagMeta1Value=@TagMeta1Value,TagMeta1Content=@TagMeta1Content,TagMeta2Value=@TagMeta2Value,
          TagMeta2Content=@TagMeta2Content,TagMeta1Attribute=@TagMeta1Attribute,TagMeta2Attribute=@TagMeta2Attribute,
          [Allegato1_Desc] = @Allegato1_Desc, [Allegato1_File] = @Allegato1_File, 
          [Allegato1_UrlEsterno] = @Allegato1_UrlEsterno, [Allegato1_Peso] = @Allegato1_Peso, 
          [Allegato1_Selettore] = @Allegato1_Selettore 
          WHERE [ID1Publisher] = @ID1Publisher"
        OnUpdated="tbPublisherSqlDataSource_Updated"
        OnSelected="tbPublisherSqlDataSource_Selected">
        <SelectParameters>
            <asp:QueryStringParameter Name="ID1Publisher" QueryStringField="XRI" />
            <asp:QueryStringParameter Name="ZeusIdModulo" QueryStringField="ZIM" Type="String" />
            <asp:QueryStringParameter Name="ZeusLangCode" QueryStringField="Lang" Type="String"
                DefaultValue="ITA" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Titolo" Type="String" />
            <asp:Parameter Name="TitoloBrowser" Type="String" />
            <asp:Parameter Name="Sottotitolo" Type="String" />
            <asp:Parameter Name="ID2Categoria1" Type="Int32" />
            <asp:Parameter Name="DescBreve1" Type="String" />
            <asp:Parameter Name="Contenuto1" Type="String" />
            <asp:Parameter Name="Contenuto2" Type="String" />
            <asp:Parameter Name="Contenuto3" Type="String" />
            <asp:Parameter Name="Immagine1" Type="String" />
            <asp:Parameter Name="Immagine2" Type="String" />
            <asp:Parameter Name="Immagine12Alt" Type="String" />
            <asp:Parameter Name="Immagine3" Type="String" />
            <asp:Parameter Name="Immagine4" Type="String" />
            <asp:Parameter Name="Immagine34Alt" Type="String" />
            <asp:Parameter Name="TagDescription" Type="String" />
            <asp:Parameter Name="TagKeywords" Type="String" />
            <asp:Parameter Name="JollyFlag1" Type="Boolean" />
            <asp:Parameter Name="UrlRewrite" Type="String" />
            <asp:Parameter Name="Autore" Type="String" />
            <asp:Parameter Name="Autore_Email" Type="String" />
            <asp:Parameter Name="Fonte" Type="String" />
            <asp:Parameter Name="Fonte_Url" Type="String" />
            <asp:Parameter Name="Archivio" Type="Boolean" />
            <asp:Parameter Name="PW_Area1" Type="Boolean" />
            <asp:Parameter Name="PWI_Area1" Type="DateTime" />
            <asp:Parameter Name="PWF_Area1" Type="DateTime" />
            <asp:Parameter Name="PW_Area2" Type="Boolean" />
            <asp:Parameter Name="PWI_Area2" Type="DateTime" />
            <asp:Parameter Name="PWF_Area2" Type="DateTime" />
            <asp:Parameter Name="PW_Area3" Type="Boolean" />
            <asp:Parameter Name="PWI_Area3" Type="DateTime" />
            <asp:Parameter Name="PWF_Area3" Type="DateTime" />
            <asp:Parameter Name="PW_Area4" Type="Boolean" />
            <asp:Parameter Name="PWI_Area4" Type="DateTime" />
            <asp:Parameter Name="PWF_Area4" Type="DateTime" />
            <asp:Parameter Name="Allegato1_Desc" Type="String" />
            <asp:Parameter Name="Allegato1_File" Type="String" />
            <asp:Parameter Name="Allegato1_UrlEsterno" Type="String" />
            <asp:Parameter Name="Allegato1_Peso" Type="Decimal" />
            <asp:Parameter Name="Allegato1_Selettore" Type="String" />
            <asp:Parameter Name="RecordEdtUser" />
            <asp:Parameter Type="DateTime" Name="RecordEdtDate" />
            <asp:Parameter Name="TagMeta1Value" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="TagMeta1Content" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="TagMeta2Value" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="TagMeta2Content" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="TagMeta1Attribute" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="TagMeta2Attribute" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
