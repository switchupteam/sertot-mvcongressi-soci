﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using ASP;
using System.Data.SqlClient;

/// <summary>
/// Descrizione di riepilogo per Content_Dtl
/// </summary>
public partial class Publisher_Content_Dtl : System.Web.UI.Page
{
    static string TitoloPagina = "Dettaglio Publisher";

    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;

        delinea myDelinea = new delinea();

        if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM"))
            || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI")))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        ZIM.Value = Server.HtmlEncode(Request.QueryString["ZIM"]);
        XRI.Value = Server.HtmlEncode(Request.QueryString["XRI"]);
        ZID.Value = getZeusId();

        PhotoGallery_Associa PhotoGallery_Associa1 = (PhotoGallery_Associa)FormView1.FindControl("PhotoGallery_Associa1");
        PhotoGallery_Associa1.TitoloControlID = "TitoloLabel";
        PhotoGallery_Associa1.ZeusIdModuloContenuto = ZIM.Value;
        PhotoGallery_Associa1.IDContenuto = XRI.Value;

        if (!ReadXML(ZIM.Value, GetLang()))
            Response.Redirect("~/Zeus/System/Message.aspx?1");

        ReadXML_Localization(ZIM.Value, GetLang());

        try
        {
            string myJavascript = CopyToClipBoard();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartUpScript", myJavascript, true);
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    private string CopyToClipBoard()
    {
        string myJavascript = " function ClipBoard() ";
        myJavascript += " { ";
        myJavascript += " var holdtext =document.getElementById('";
        myJavascript += FormView1.FindControl("CopyHiddenField").ClientID;
        myJavascript += "'); ";
        myJavascript += " var copytext = document.getElementById('";
        myJavascript += FormView1.FindControl("ZMCD_UrlSection").ClientID;
        myJavascript += "'); ";
        myJavascript += " var copytext2 = document.getElementById('";
        myJavascript += FormView1.FindControl("ID1Pagina").ClientID;
        myJavascript += "'); ";
        myJavascript += " var copytext3 = document.getElementById('";
        myJavascript += FormView1.FindControl("UrlPageDetailLabel").ClientID;
        myJavascript += "'); ";
        myJavascript += " holdtext.value = copytext.innerHTML + copytext2.innerHTML + copytext3.innerHTML; ";
        myJavascript += " var Copied  = holdtext.createTextRange();";
        myJavascript += " Copied.select();";
        myJavascript += " Copied.execCommand('copy',false,null);";
        myJavascript += " return; ";
        myJavascript += " } ";

        return myJavascript;
    }

    private string getZeusId()
    {

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT [ZeusId]";
                sqlQuery += " FROM [tbpublisher] ";
                sqlQuery += " WHERE [ID1Publisher]=@ID1Publisher";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID1Publisher", System.Data.SqlDbType.Int);
                command.Parameters["@ID1Publisher"].Value = Server.HtmlEncode(XRI.Value.ToString());

                SqlDataReader reader = command.ExecuteReader();

                string myReturn = "";

                while (reader.Read())
                {
                    myReturn = reader["ZeusId"].ToString();
                }



                reader.Close();
                return myReturn;
            }
            catch (Exception p)
            {

                Response.Write(p.ToString());
                return "";
            }
        }//fine Using

    }//fine getZeusId


    private string GetLang()
    {
        string Lang = "ITA";

        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;
    }

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_Publisher.xml"));


            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {

                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {

                            if (childNode.Name.Equals("ZML_TitoloPagina_Dtl"))
                            {
                                TitleField.Value = childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;

                                else
                                {
                                    myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name + "Label");

                                    if (myLabel != null)
                                        myLabel.Text = childNode.InnerText;
                                }
                            }

                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }

    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            string myXPathEach = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_Publisher.xml"));

            if (!CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_Edt").InnerText))
                Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223599");

            Panel myPanel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPathEach);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            if (childNode.Name.IndexOf("ZMCF_") > -1)
                            {
                                myPanel = (Panel)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myPanel != null)
                                    myPanel.Visible = Convert.ToBoolean(childNode.InnerText);
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }

            ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");

            if (ImageRaider1 != null)
                ImageRaider1.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxImmagini1").InnerText);

            ImageRaider ImageRaider2 = (ImageRaider)FormView1.FindControl("ImageRaider2");

            if (ImageRaider2 != null)
                ImageRaider2.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxImmagini2").InnerText);

            SetQueryOfID2CategoriaDropDownList(mydoc.SelectSingleNode(myXPath + "ZMCD_ZIMCategoria1").InnerText
                , GetLang()
                , mydoc.SelectSingleNode(myXPath + "ZMCD_Cat1Liv").InnerText);

            CategorieMultiple CategorieMultiple1 = (CategorieMultiple)FormView1.FindControl("CategorieMultiple1");

            if (CategorieMultiple1 != null)
            {
                CategorieMultiple1.ZeusIdModulo = mydoc.SelectSingleNode(myXPath + "ZMCD_ZIMCategoria2").InnerText;
                CategorieMultiple1.ZeusLangCode = GetLang();
                CategorieMultiple1.RepeatColumns = mydoc.SelectSingleNode(myXPath + "ZMCD_ColCategoria2").InnerText;
                CategorieMultiple1.PAGE_ID = XRI.Value;
                CategorieMultiple1.PrintCategorie();

            }//fine if

            Label ZMCD_UrlSection = (Label)FormView1.FindControl("ZMCD_UrlSection");

            if (ZMCD_UrlSection != null)
                ZMCD_UrlSection.Text = mydoc.SelectSingleNode(myXPath + "ZMCD_UrlSection").InnerText;

            Label ZMCD_UrlDomain = (Label)FormView1.FindControl("ZMCD_UrlDomain");

            if (ZMCD_UrlDomain != null)
                ZMCD_UrlDomain.Text = mydoc.SelectSingleNode(myXPath + "ZMCD_UrlDomain").InnerText;


            PhotoGallery_Associa PhotoGallery_Associa1 = (PhotoGallery_Associa)FormView1.FindControl("PhotoGallery_Associa1");

            if (PhotoGallery_Associa1 != null)
                PhotoGallery_Associa1.ZIMGalleryAssociation =
                    mydoc.SelectSingleNode(myXPath + "ZMCD_ZIMGalleryAssociation").InnerText;


            Panel ZMCF_AllegatoDescrizione = (Panel)FormView1.FindControl("ZMCF_AllegatoDescrizione");

            if (ZMCF_AllegatoDescrizione != null)
                ZMCF_AllegatoDescrizione.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_AllegatoDescrizione").InnerText);

            if (!CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_NewLang").InnerText))
            {
                PublisherLangButton PublisherLangButton1 = (PublisherLangButton)FormView1.FindControl("PublisherLangButton1");
                PublisherLangButton1.Visible = false;
            }

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }

    }//fine ReadXML


    private bool CheckPermit(string AllRoles)
    {

        bool IsPermit = false;

        try
        {
            if (AllRoles.Length == 0)
                return false;

            char[] delimiter = { ',' };

            foreach (string roles in Roles.GetRolesForUser(Membership.GetUser().UserName))
            {
                string[] PressRoles = AllRoles.Split(delimiter);

                for (int i = 0; i < PressRoles.Length; ++i)
                {
                    if (PressRoles[i].Trim().Equals(roles.Trim()))
                    {
                        IsPermit = true;
                        break;
                    }
                }
            }
        }
        catch (Exception)
        { }

        return IsPermit;
    }

    private bool IsUserInAuthorizationRoles(string AuthorizationRoles)
    {
        try
        {

            foreach (string roles in Roles.GetRolesForUser(Membership.GetUser().UserName))
                if (roles.Equals("ZeusAdmin"))
                    return true;

                else
                {
                    char[] mySplit = { ',' };

                    string[] myRoles = AuthorizationRoles.Split(mySplit);

                    for (int i = 0; i < myRoles.Length; ++i)
                        if (myRoles[i].Equals(roles))
                            return true;
                }

            return false;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    protected void ZeusIdHidden_Load(object sender, EventArgs e)
    {
        HiddenField Guid = (HiddenField)sender;
        if (Guid.Value.Length == 0)
            Guid.Value = System.Guid.NewGuid().ToString();

    }

    protected void UtenteCreazione(object sender, EventArgs e)
    {
        HiddenField UtenteCreazione = (HiddenField)sender;
        UtenteCreazione.Value = Membership.GetUser().ProviderUserKey.ToString();

    }

    protected void DataOggi(object sender, EventArgs e)
    {
        HiddenField DataCreazione = (HiddenField)sender;
        DataCreazione.Value = DateTime.Now.ToString();

    }

    protected void PWI_Area1Label_DataBinding(object sender, EventArgs e)
    {
        Label DataCreazione = (Label)sender;

        if (DataCreazione.Text.Length == 0)
            DataCreazione.Text = DateTime.Now.ToString("d");

    }

    protected void PWF_Area1Label_DataBinding(object sender, EventArgs e)
    {
        Label DataFinale = (Label)sender;
        if (DataFinale.Text.Length == 0)
            DataFinale.Text = "31/12/2040";

    }

    protected void PWI_Area1HiddenField_DataBinding(object sender, EventArgs e)
    {
        HiddenField PWI_Area1HiddenField = (HiddenField)sender;

        try
        {
            Label PWI_Area1OreLabel = (Label)FormView1.FindControl("PWI_Area1OreLabel");
            Label PWI_Area1MinutiLabel = (Label)FormView1.FindControl("PWI_Area1MinutiLabel");


            PWI_Area1OreLabel.Text = Convert.ToDateTime(PWI_Area1HiddenField.Value).Hour.ToString();
            PWI_Area1MinutiLabel.Text = Convert.ToDateTime(PWI_Area1HiddenField.Value).Minute.ToString();

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    protected void PWF_Area1HiddenField_DataBinding(object sender, EventArgs e)
    {
        HiddenField PWF_Area1HiddenField = (HiddenField)sender;

        try
        {
            Label PWF_Area1OreLabel = (Label)FormView1.FindControl("PWF_Area1OreLabel");
            Label PWF_Area1MinutiLabel = (Label)FormView1.FindControl("PWF_Area1MinutiLabel");


            PWF_Area1OreLabel.Text = Convert.ToDateTime(PWF_Area1HiddenField.Value).Hour.ToString();
            PWF_Area1MinutiLabel.Text = Convert.ToDateTime(PWF_Area1HiddenField.Value).Minute.ToString();

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    protected void PWI_Area2HiddenField_DataBinding(object sender, EventArgs e)
    {
        HiddenField PWI_Area2HiddenField = (HiddenField)sender;

        try
        {
            Label PWI_Area2OreLabel = (Label)FormView1.FindControl("PWI_Area2OreLabel");
            Label PWI_Area2MinutiLabel = (Label)FormView1.FindControl("PWI_Area2MinutiLabel");


            PWI_Area2OreLabel.Text = Convert.ToDateTime(PWI_Area2HiddenField.Value).Hour.ToString();
            PWI_Area2MinutiLabel.Text = Convert.ToDateTime(PWI_Area2HiddenField.Value).Minute.ToString();

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    protected void PWF_Area2HiddenField_DataBinding(object sender, EventArgs e)
    {
        HiddenField PWF_Area2HiddenField = (HiddenField)sender;

        try
        {
            Label PWF_Area2OreLabel = (Label)FormView1.FindControl("PWF_Area2OreLabel");
            Label PWF_Area2MinutiLabel = (Label)FormView1.FindControl("PWF_Area2MinutiLabel");


            PWF_Area2OreLabel.Text = Convert.ToDateTime(PWF_Area2HiddenField.Value).Hour.ToString();
            PWF_Area2MinutiLabel.Text = Convert.ToDateTime(PWF_Area2HiddenField.Value).Minute.ToString();

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    protected void PWI_Area3HiddenField_DataBinding(object sender, EventArgs e)
    {
        HiddenField PWI_Area3HiddenField = (HiddenField)sender;

        try
        {
            Label PWI_Area3OreLabel = (Label)FormView1.FindControl("PWI_Area3OreLabel");
            Label PWI_Area3MinutiLabel = (Label)FormView1.FindControl("PWI_Area3MinutiLabel");


            PWI_Area3OreLabel.Text = Convert.ToDateTime(PWI_Area3HiddenField.Value).Hour.ToString();
            PWI_Area3MinutiLabel.Text = Convert.ToDateTime(PWI_Area3HiddenField.Value).Minute.ToString();

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    protected void PWF_Area3HiddenField_DataBinding(object sender, EventArgs e)
    {
        HiddenField PWF_Area3HiddenField = (HiddenField)sender;

        try
        {
            Label PWF_Area3OreLabel = (Label)FormView1.FindControl("PWF_Area3OreLabel");
            Label PWF_Area3MinutiLabel = (Label)FormView1.FindControl("PWF_Area3MinutiLabel");


            PWF_Area3OreLabel.Text = Convert.ToDateTime(PWF_Area3HiddenField.Value).Hour.ToString();
            PWF_Area3MinutiLabel.Text = Convert.ToDateTime(PWF_Area3HiddenField.Value).Minute.ToString();

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    protected void PWI_Area4HiddenField_DataBinding(object sender, EventArgs e)
    {
        HiddenField PWI_Area4HiddenField = (HiddenField)sender;

        try
        {
            TextBox PWI_Area4OreTextBox = (TextBox)FormView1.FindControl("PWI_Area4OreTextBox");
            TextBox PWI_Area4MinutiTextBox = (TextBox)FormView1.FindControl("PWI_Area4MinutiTextBox");


            PWI_Area4OreTextBox.Text = Convert.ToDateTime(PWI_Area4HiddenField.Value).Hour.ToString();
            PWI_Area4MinutiTextBox.Text = Convert.ToDateTime(PWI_Area4HiddenField.Value).Minute.ToString();

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    protected void PWF_Area4HiddenField_DataBinding(object sender, EventArgs e)
    {
        HiddenField PWF_Area4HiddenField = (HiddenField)sender;

        try
        {
            TextBox PWF_Area4OreTextBox = (TextBox)FormView1.FindControl("PWF_Area4OreTextBox");
            TextBox PWF_Area4MinutiTextBox = (TextBox)FormView1.FindControl("PWF_Area4MinutiTextBox");


            PWF_Area4OreTextBox.Text = Convert.ToDateTime(PWF_Area4HiddenField.Value).Hour.ToString();
            PWF_Area4MinutiTextBox.Text = Convert.ToDateTime(PWF_Area4HiddenField.Value).Minute.ToString();

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    private bool SetQueryOfID2CategoriaDropDownList(string ZeusIdModulo, string ZeusLangCode, string PZV_Cat1Liv)
    {
        try
        {


            SqlDataSource dsCategoria = (SqlDataSource)FormView1.FindControl("dsCategoria");
            DropDownList ID2CategoriaDropDownList = (DropDownList)FormView1.FindControl("ID2CategoriaDropDownList");
            HiddenField ID2CategoriaHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaHiddenField");
            //ID2CategoriaHiddenField.Value = "0";

            switch (PZV_Cat1Liv)
            {

                case "123":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv1Liv2Liv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "Catliv1Liv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;


                case "23":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv2Liv3]";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;

                default:
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;
            }


            ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
            ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
            ID2CategoriaDropDownList.DataBind();

            ID2CategoriaDropDownList.SelectedIndex = 
                ID2CategoriaDropDownList.Items.IndexOf(ID2CategoriaDropDownList.Items.FindByValue(ID2CategoriaHiddenField.Value));

            Label CategoriaLabel = (Label)FormView1.FindControl("CategoriaLabel");
            CategoriaLabel.Text = ID2CategoriaDropDownList.SelectedItem.Text;

            return true;
        }
        catch (Exception p)
        {
            return false;
        }
    }

    protected void UrlLinkButton_DataBinding(object sender, EventArgs e)
    {
        LinkButton UrlLinkButton = (LinkButton)sender;
        if (UrlLinkButton.PostBackUrl.Length == 0)
            UrlLinkButton.Visible = false;
        else
        {
            UrlLinkButton.OnClientClick = "window.open('" + UrlLinkButton.PostBackUrl + "'); return false;";
        }
    }


    protected void SelettoreAllegatoHiddenfield_DataBinding(object sender, EventArgs e)
    {

        HiddenField SelettoreAllegatoHiddenfield = (HiddenField)sender;
        HyperLink FileAllegatoHyperLink = (HyperLink)FormView1.FindControl("FileAllegatoHyperLink");
        LinkButton UrlLinkButton = (LinkButton)FormView1.FindControl("UrlLinkButton");

        switch (SelettoreAllegatoHiddenfield.Value)
        {
            case "FIL":
                FileAllegatoHyperLink.Visible = true;
                UrlLinkButton.Visible = false;
                break;

            case "LNK":
                FileAllegatoHyperLink.Visible = false;
                UrlLinkButton.Visible = true;
                break;
        }
    }

    protected void ImageRaider_DataBinding(object sender, EventArgs e)
    {
        ImageRaider ImageRaider1 = (ImageRaider)sender;
        ImageRaider1.SetDefaultEditBetaImage();
        ImageRaider1.SetDefaultEditGammaImage();

    }

    protected void ModificaButton_Click(object sender, EventArgs e)
    {

        Response.Redirect("~/Zeus/Publisher/Content_Edt.aspx?XRI=" + XRI.Value
            + "&ZIM=" + Server.HtmlEncode(Request.QueryString["ZIM"])
            + "&Lang=" + GetLang());

    }


    protected void DeleteLinkButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Zeus/Publisher/Content_Del.aspx?XRI=" + XRI.Value
            + "&Lang=" + GetLang()
            + "&ZIM=" + Server.HtmlEncode(Request.QueryString["ZIM"]));

    }

    protected void tbPublisherSqlDataSource_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {
        if ((e.Exception != null)
            || (e.AffectedRows <= 0))
            Response.Redirect("~/Zeus/System/Message.aspx?SelErr");
    }

    protected void PhotoGallery_Simple1_Init(object sender, EventArgs e)
    {
        Photo_Lst myGallery = (Photo_Lst)sender;
        myGallery.Lang = GetLang();
        myGallery.ZeusIdModulo = ZIM.Value.ToString();
        myGallery.XRI = XRI.Value.ToString();
        myGallery.ZID = ZID.Value.ToString();
    }
}