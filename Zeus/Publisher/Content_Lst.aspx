﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="Content_Lst.aspx.cs"
    Inherits="Publisher_Content_Lst"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" />
    <asp:HiddenField ID="ZIM" runat="server" />
    <asp:HiddenField ID="Livello" runat="server" />
    <asp:HiddenField ID="ZMCD_UrlSection" runat="server" />
    <dlc:PermitRoles ID="myPermitRoles" runat="server" PAGE_TYPE="LST" SQL_TABLE="tbPz_Publisher" />
    <asp:Panel ID="SingleRecordPanel" runat="server" Visible="false">
        <div class="LayGridTitolo1">
            <asp:Label ID="CaptionallRecordLabel" SkinID="GridTitolo1" runat="server" Text="Ultimo contenuto creato / aggiornato"></asp:Label>
        </div>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
            AutoGenerateColumns="False" DataSourceID="SingleRecordSqlDatasource" OnRowDataBound="GridView1_RowDataBound"
            PageSize="30">
            <Columns>
                <asp:TemplateField HeaderText="AttivoArea1" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="AttivoArea1" runat="server" Text='<%# Eval("AttivoArea1") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="AttivoArea2" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="AttivoArea2" runat="server" Text='<%# Eval("AttivoArea2") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="AttivoArea3" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="AttivoArea3" runat="server" Text='<%# Eval("AttivoArea3") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="AttivoArea4" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="AttivoArea4" runat="server" Text='<%# Eval("AttivoArea4") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Titolo" HeaderText="Titolo" SortExpression="Titolo" />
                <asp:BoundField DataField="CatLiv2Liv3" HeaderText="Categoria" SortExpression="CatLiv2Liv3" />
                <asp:TemplateField HeaderText="Sezioni">
                    <ItemTemplate>
                        <asp:Label ID="SezioniLabel" runat="server" Text='<%# GetSezioni(Eval("ID1Publisher").ToString()) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField SortExpression="AttivoArea1" HeaderText="Attivo">
                    <ItemTemplate>
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>
                <asp:BoundField DataField="PWI_Area1" HeaderText="Pub. inizio" SortExpression="PWI_Area1"
                    DataFormatString="{0}">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="PWF_Area1" HeaderText="Pub. fine" SortExpression="PWF_Area1"
                    DataFormatString="{0}">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:TemplateField SortExpression="AttivoArea2" HeaderText="Attivo">
                    <ItemTemplate>
                        <asp:Image ID="Image2" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>
                <asp:BoundField DataField="PWI_Area2" HeaderText="Pub. inizio" SortExpression="PWI_Area2"
                    DataFormatString="{0}">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="PWF_Area2" HeaderText="Pub. fine" SortExpression="PWF_Area2"
                    DataFormatString="{0}">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:TemplateField SortExpression="AttivoArea3" HeaderText="Attivo">
                    <ItemTemplate>
                        <asp:Image ID="Image3" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>
                <asp:BoundField DataField="PWI_Area3" HeaderText="Pub. inizio" SortExpression="PWI_Area3"
                    DataFormatString="{0}">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="PWF_Area3" HeaderText="Pub. fine" SortExpression="PWF_Area3"
                    DataFormatString="{0}">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:TemplateField SortExpression="AttivoArea4" HeaderText="Attivo">
                    <ItemTemplate>
                        <asp:Image ID="Image4" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>
                <asp:BoundField DataField="PWI_Area4" HeaderText="Pub. inizio" SortExpression="PWI_Area4"
                    DataFormatString="{0}">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="PWF_Area4" HeaderText="Pub. fine" SortExpression="PWF_Area4"
                    DataFormatString="{0}">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="Anteprima">
                    <ItemTemplate>
                        <asp:HyperLink ID="AnteprimaLink" runat="server" Text="Anteprima" NavigateUrl='<%# Eval("AnteprimaLink") %>'
                            Target="_blank" CssClass="GridView_ZeusButton1"></asp:HyperLink>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="GridView_ZeusButton1" />
                </asp:TemplateField>
                <asp:HyperLinkField DataNavigateUrlFields="ID1Publisher,ZeusLangCode,ZeusIdModulo"
                    HeaderText="Dettaglio" Text="Dettaglio" DataNavigateUrlFormatString="Content_Dtl.aspx?XRI={0}&Lang={1}&ZIM={2}">
                    <ControlStyle CssClass="GridView_ZeusButton1" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:HyperLinkField>
                <asp:HyperLinkField DataNavigateUrlFields="ID1Publisher,ZeusLangCode,ZeusIdModulo"
                    HeaderText="Modifica" Text="Modifica" DataNavigateUrlFormatString="Content_Edt.aspx?XRI={0}&Lang={1}&ZIM={2}">
                    <ControlStyle CssClass="GridView_ZeusButton1" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:HyperLinkField>
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SingleRecordSqlDatasource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
            SelectCommand="SELECT [ID1Publisher],[Titolo],[CatLiv2Liv3]
            ,[PWI_Area1],[PWF_Area1],[PWI_Area2],[PWF_Area2],[PWI_Area3],[PWF_Area3],[PWI_Area4],[PWF_Area4]
            ,[AttivoArea1],[AttivoArea2],[AttivoArea3],[AttivoArea4],[Archivio]
            ,[ZeusIdModulo],[ZeusLangCode],AnteprimaLink
            FROM [vwPublisher_Lst] 
            WHERE ID1Publisher=@ID1Publisher">
            <SelectParameters>
                <asp:QueryStringParameter Name="ID1Publisher" QueryStringField="XRI" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <div class="VertSpacerMedium">
        </div>
    </asp:Panel>
    <div class="LayGridTitolo1">
        <asp:Label ID="Label2" runat="server" SkinID="GridTitolo1" Text="Elenco contenuti disponibili"></asp:Label>
    </div>
    <asp:GridView ID="GridView2" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" DataSourceID="vwPublisher_LstSqlDataSource" OnRowDataBound="GridView1_RowDataBound"
        PageSize="50">
        <Columns>
            <asp:TemplateField HeaderText="AttivoArea1" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="AttivoArea1" runat="server" Text='<%# Eval("AttivoArea1") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="AttivoArea2" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="AttivoArea2" runat="server" Text='<%# Eval("AttivoArea2") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="AttivoArea3" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="AttivoArea3" runat="server" Text='<%# Eval("AttivoArea3") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="AttivoArea4" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="AttivoArea4" runat="server" Text='<%# Eval("AttivoArea4") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Titolo" HeaderText="Titolo" SortExpression="Titolo" />
            <asp:BoundField DataField="CatLiv2Liv3" HeaderText="Categoria" SortExpression="CatLiv2Liv3" />
            <asp:TemplateField HeaderText="Sezioni">
                <ItemTemplate>
                    <asp:Label ID="SezioniLabel" runat="server" Text='<%# GetSezioni(Eval("ID1Publisher").ToString()) %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField SortExpression="AttivoArea1" HeaderText="Attivo">
                <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:BoundField DataField="PWI_Area1" HeaderText="Pub. inizio" SortExpression="PWI_Area1"
                DataFormatString="{0}">
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="PWF_Area1" HeaderText="Pub. fine" SortExpression="PWF_Area1"
                DataFormatString="{0}">
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:TemplateField SortExpression="AttivoArea2" HeaderText="Attivo">
                <ItemTemplate>
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:BoundField DataField="PWI_Area2" HeaderText="Pub. inizio" SortExpression="PWI_Area2"
                DataFormatString="{0}">
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="PWF_Area2" HeaderText="Pub. fine" SortExpression="PWF_Area2"
                DataFormatString="{0}">
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:TemplateField SortExpression="AttivoArea3" HeaderText="Attivo">
                <ItemTemplate>
                    <asp:Image ID="Image3" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:BoundField DataField="PWI_Area3" HeaderText="Pub. inizio" SortExpression="PWI_Area3"
                DataFormatString="{0}">
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="PWF_Area3" HeaderText="Pub. fine" SortExpression="PWF_Area3"
                DataFormatString="{0}">
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:TemplateField SortExpression="AttivoArea4" HeaderText="Attivo">
                <ItemTemplate>
                    <asp:Image ID="Image4" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:BoundField DataField="PWI_Area4" HeaderText="Pub. inizio" SortExpression="PWI_Area4"
                DataFormatString="{0}">
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="PWF_Area4" HeaderText="Pub. fine" SortExpression="PWF_Area4"
                DataFormatString="{0}">
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="Anteprima">
                <ItemTemplate>
                    <asp:HyperLink ID="AnteprimaLink" runat="server" Text="Anteprima" NavigateUrl='<%# Eval("AnteprimaLink") %>'
                        Target="_blank"></asp:HyperLink>
                </ItemTemplate>
                <ControlStyle CssClass="GridView_ZeusButton1" />
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:HyperLinkField DataNavigateUrlFields="ID1Publisher,ZeusLangCode,ZeusIdModulo"
                HeaderText="Dettaglio" Text="Dettaglio" DataNavigateUrlFormatString="Content_Dtl.aspx?XRI={0}&Lang={1}&ZIM={2}">
                <ControlStyle CssClass="GridView_ZeusButton1" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:HyperLinkField>
            <asp:HyperLinkField DataNavigateUrlFields="ID1Publisher,ZeusLangCode,ZeusIdModulo"
                HeaderText="Modifica" Text="Modifica" DataNavigateUrlFormatString="Content_Edt.aspx?XRI={0}&Lang={1}&ZIM={2}">
                <ControlStyle CssClass="GridView_ZeusButton1" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:HyperLinkField>
        </Columns>
        <EmptyDataTemplate>
            Dati non presenti
            <br />
            <br />
            <br />
            <br />
            <br />
        </EmptyDataTemplate>
        <EmptyDataRowStyle BackColor="White" Width="500px" BorderColor="Red" BorderWidth="0px"
            Height="200px" />
    </asp:GridView>
    <asp:SqlDataSource ID="vwPublisher_LstSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SELECT [ID1Publisher],[Titolo],[CatLiv2Liv3]
        ,[PWI_Area1],[PWF_Area1],[PWI_Area2],[PWF_Area2],[PWI_Area3],[PWF_Area3],[PWI_Area4],[PWF_Area4]
        ,[AttivoArea1],[AttivoArea2],[AttivoArea3],[AttivoArea4],[Archivio],[ZeusIdModulo],[ZeusLangCode],AnteprimaLink 
        FROM [vwPublisher_Lst] 
        WHERE ZeusLangCode=@ZeusLangCode AND ZeusIdModulo=@ZeusIdModulo ">
        <SelectParameters>
            <asp:QueryStringParameter Name="ZeusLangCode" QueryStringField="Lang" />
            <asp:QueryStringParameter Name="ZeusIdModulo" QueryStringField="ZIM" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
