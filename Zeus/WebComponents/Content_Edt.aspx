﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="Content_Edt.aspx.cs"
    Inherits="WebComponents_Content_Edt"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Data.SqlClient" %>
<%--<%@ Register Src="WebComponentsLangButton.ascx" TagName="WebComponentsLangButton" TagPrefix="uc1" %>--%>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <script type="text/javascript">
        function OnClientModeChange(editor) {
            var mode = editor.get_mode();
            var doc = editor.get_document();
            var head = doc.getElementsByTagName("HEAD")[0];
            var link;

            switch (mode) {
                case 1: //remove the external stylesheet when displaying the content in Design mode    
                    //var external = doc.getElementById("external");
                    //head.removeChild(external);
                    break;
                case 2:
                    break;
                case 4: //apply your external css stylesheet to Preview mode    
                    link = doc.createElement("LINK");
                    link.setAttribute("href", "/Zeus/SiteCss/Telerik.css");
                    link.setAttribute("rel", "stylesheet");
                    link.setAttribute("type", "text/css");
                    link.setAttribute("id", "external");
                    head.appendChild(link);
                    break;
            }
        }

        function editorCommandExecuted(editor, args) {
            if (!$telerik.isChrome)
                return;
            var dialogName = args.get_commandName();
            var dialogWin = editor.get_dialogOpener()._dialogContainers[dialogName];
            if (dialogWin) {
                var cellEl = dialogWin.get_contentElement() || dialogWin.ui.contentCell || dialogWin.ui.content,
                frame = dialogWin.get_contentFrame();
                frame.onload = function () {
                    cellEl.style.cssText = "";
                    dialogWin.autoSize();
                }
            }
        }
    </script>
    <asp:HiddenField ID="TitleField" runat="server" />
    <asp:HiddenField ID="XRI" runat="server" />    
    
    <%--    <dlc:PermitRoles ID="myPermitRoles" runat="server" PAGE_TYPE="EDT" SQL_TABLE="tbPz_WebComponents"
        DEL_ID_BUTTON="DeleteLinkButton" />--%>
    <asp:FormView ID="frmMain" runat="server" DefaultMode="Edit" Width="100%" DataSourceID="dsWebCmp">
        <EditItemTemplate>

            <div id="tabs">
                <ul>
                    <li><a href="#Principale">Identificativo: <%# Eval("wcNome") %></a></li>
                </ul>

                <div id="Principale">
                    <div class="BlockBox">
                        <table>
                            <asp:Panel ID="pnlTesto1" runat="server" Visible='<%# Eval("Testo1_IsActive") %>'>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="lblTesto1" SkinID="FieldDescription" runat="server" Text='<%# Bind("Testo1_Label") %>'></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:TextBox ID="txtTesto1" runat="server" Text='<%# Bind("Testo1") %>' MaxLength="100" Columns="112"></asp:TextBox>
                                        <%--
			                            <asp:RequiredFieldValidator ID="rfvTesto1" ErrorMessage="Obbligatorio"
				                            runat="server" ControlToValidate="txtTesto1" SkinID="ZSSM_Validazione01"
				                            Display="Dynamic" ValidationGroup="myValidation">
			                            </asp:RequiredFieldValidator>
                                        --%>
                                    </td>
                                </tr>
                            </asp:Panel>

                            <asp:Panel ID="pnlTesto2" runat="server" Visible='<%# Eval("Testo2_IsActive") %>'>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="lblTesto2" SkinID="FieldDescription" runat="server" Text='<%# Bind("Testo2_Label") %>'></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:TextBox ID="txtTesto2" runat="server" Text='<%# Bind("Testo2") %>' MaxLength="100" Columns="112"></asp:TextBox>
                                        <%--
			                            <asp:RequiredFieldValidator ID="rfvTesto2" ErrorMessage="Obbligatorio"
				                            runat="server" ControlToValidate="txtTesto2" SkinID="ZSSM_Validazione01"
				                            Display="Dynamic" ValidationGroup="myValidation">
			                            </asp:RequiredFieldValidator>
                                        --%>
                                    </td>
                                </tr>
                            </asp:Panel>

                            <asp:Panel ID="pnlTesto3" runat="server" Visible='<%# Eval("Testo3_IsActive") %>'>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="lblTesto3" SkinID="FieldDescription" runat="server" Text='<%# Bind("Testo3_Label") %>'></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:TextBox ID="txtTesto3" runat="server" Text='<%# Bind("Testo3") %>' MaxLength="100" Columns="112"></asp:TextBox>
                                        <%--
			                            <asp:RequiredFieldValidator ID="rfvTesto3" ErrorMessage="Obbligatorio"
				                            runat="server" ControlToValidate="txtTesto3" SkinID="ZSSM_Validazione01"
				                            Display="Dynamic" ValidationGroup="myValidation">
			                            </asp:RequiredFieldValidator>
                                        --%>
                                    </td>
                                </tr>
                            </asp:Panel>

                            <asp:Panel ID="pnlDescBreve1" runat="server" Visible='<%# Eval("DescBreve1_IsActive") %>'>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="lblDescBreve1" SkinID="FieldDescription" runat="server" Text='<%# Bind("DescBreve1_Label") %>'></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <CustomWebControls:TextArea runat="server" ID="txtDescBreve1" Columns="113" Rows="6"
                                            TextMode="MultiLine" ValidationGroup="myValidation" Text='<%# Bind("DescBreve1") %>'
                                            EnableTheming="True" Height="100px" MaxLength="600"></CustomWebControls:TextArea>

                                        <%--
			                            <asp:RequiredFieldValidator ID="rfvDescBreve1" ErrorMessage="Obbligatorio"
				                            runat="server" ControlToValidate="txtDescBreve1" SkinID="ZSSM_Validazione01"
				                            Display="Dynamic" ValidationGroup="myValidation">
			                            </asp:RequiredFieldValidator>
                                        --%>
                                    </td>
                                </tr>
                            </asp:Panel>

                            <asp:Panel ID="pnlContenuto1" runat="server" Visible='<%# Eval("Contenuto1_IsActive") %>'>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="lblContenuto1" SkinID="FieldDescription" runat="server" Text='<%# Bind("Contenuto1_Label") %>'></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <%--<asp:TextBox ID="txtContenuto1" runat="server" Text='<%# Bind("Contenuto1") %>' MaxLength="100" Columns="100"></asp:TextBox>--%>
                                        <telerik:RadEditor
                                            Language="it-IT" ID="txtContenuto1" runat="server" Content='<%# Bind("Contenuto1") %>'
                                            DocumentManager-SearchPatterns="*.*" DocumentManager-MaxUploadFileSize="52428800"
                                            DocumentManager-ViewPaths="~/ZeusInc/WebComponents/Documents" DocumentManager-UploadPaths="~/ZeusInc/WebComponents/Documents" DocumentManager-DeletePaths="~/ZeusInc/WebComponents/Documents"
                                            FlashManager-MaxUploadFileSize="10240000"
                                            FlashManager-ViewPaths="~/ZeusInc/WebComponents/Media" FlashManager-UploadPaths="~/ZeusInc/WebComponents/Media" FlashManager-DeletePaths="~/ZeusInc/WebComponents/Media"
                                            ImageManager-SearchPatterns="*.gif, *.png, *.jpg, *.jpe, *.jpeg"
                                            ImageManager-ViewMode="Grid"
                                            ImageManager-MaxUploadFileSize="10240000"
                                            ImageManager-ViewPaths="~/ZeusInc/WebComponents/Images" ImageManager-UploadPaths="~/ZeusInc/WebComponents/Images" ImageManager-DeletePaths="~/ZeusInc/WebComponents/Images"
                                            MediaManager-SearchPatterns="*.asf, *.asx, *.wma, *.wmv, *.avi, *.wav, *.mpeg, *.mpg, *.mpe, *.mov, *.mp3, *.m3u, *.mid, *.midi, *.rm, *.rma"
                                            MediaManager-MaxUploadFileSize="10240000"
                                            MediaManager-ViewPaths="~/ZeusInc/WebComponents/Media" MediaManager-UploadPaths="~/ZeusInc/WebComponents/Media" MediaManager-DeletePaths="~/ZeusInc/WebComponents/Media"
                                            TemplateManager-SearchPatterns="*.html,*.htm" ContentAreaMode="iframe"
                                            OnClientCommandExecuted="editorCommandExecuted" OnClientModeChange="OnClientModeChange"
                                            ToolsFile="~/Zeus/WebComponents/RadEditor1.xml"
                                            LocalizationPath="~/App_GlobalResources"
                                            AllowScripts="true" RenderMode="Classic" ToolbarMode="Default" EnableViewState="False"
                                            Width="700px" Height="500px" Skin="Silk" NewLineMode="Br">
                                            <CssFiles>
                                                <telerik:EditorCssFile Value="~/asset/css/ZeusTypeFoundry.css" />
                                            </CssFiles>
                                        </telerik:RadEditor>
                                        <%--
			                            <asp:RequiredFieldValidator ID="rfvContenuto1" ErrorMessage="Obbligatorio"
				                            runat="server" ControlToValidate="txtContenuto1" SkinID="ZSSM_Validazione01"
				                            Display="Dynamic" ValidationGroup="myValidation">
			                            </asp:RequiredFieldValidator>
                                        --%>
                                    </td>
                                </tr>
                            </asp:Panel>

                            <asp:Panel ID="pnlContenuto2" runat="server" Visible='<%# Eval("Contenuto2_IsActive") %>'>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="lblContenuto2" SkinID="FieldDescription" runat="server" Text='<%# Bind("Contenuto2_Label") %>'></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <%--<asp:TextBox ID="txtContenuto2" runat="server" Text='<%# Bind("Contenuto2") %>' MaxLength="100" Columns="100"></asp:TextBox>--%>
                                        <telerik:RadEditor
                                            Language="it-IT" ID="txtContenuto2" runat="server" Content='<%# Bind("Contenuto2") %>'
                                            DocumentManager-SearchPatterns="*.*" DocumentManager-MaxUploadFileSize="52428800"
                                            DocumentManager-ViewPaths="~/ZeusInc/WebComponents/Documents" DocumentManager-UploadPaths="~/ZeusInc/WebComponents/Documents" DocumentManager-DeletePaths="~/ZeusInc/WebComponents/Documents"
                                            FlashManager-MaxUploadFileSize="10240000"
                                            FlashManager-ViewPaths="~/ZeusInc/WebComponents/Media" FlashManager-UploadPaths="~/ZeusInc/WebComponents/Media" FlashManager-DeletePaths="~/ZeusInc/WebComponents/Media"
                                            ImageManager-SearchPatterns="*.gif, *.png, *.jpg, *.jpe, *.jpeg"
                                            ImageManager-ViewMode="Grid"
                                            ImageManager-MaxUploadFileSize="10240000"
                                            ImageManager-ViewPaths="~/ZeusInc/WebComponents/Images" ImageManager-UploadPaths="~/ZeusInc/WebComponents/Images" ImageManager-DeletePaths="~/ZeusInc/WebComponents/Images"
                                            MediaManager-SearchPatterns="*.asf, *.asx, *.wma, *.wmv, *.avi, *.wav, *.mpeg, *.mpg, *.mpe, *.mov, *.mp3, *.m3u, *.mid, *.midi, *.rm, *.rma"
                                            MediaManager-MaxUploadFileSize="10240000"
                                            MediaManager-ViewPaths="~/ZeusInc/WebComponents/Media" MediaManager-UploadPaths="~/ZeusInc/WebComponents/Media" MediaManager-DeletePaths="~/ZeusInc/WebComponents/Media"
                                            TemplateManager-SearchPatterns="*.html,*.htm" ContentAreaMode="iframe"
                                            OnClientCommandExecuted="editorCommandExecuted" OnClientModeChange="OnClientModeChange"
                                            ToolsFile="~/Zeus/WebComponents/RadEditor1.xml"
                                            LocalizationPath="~/App_GlobalResources"
                                            AllowScripts="true" RenderMode="Classic" ToolbarMode="Default" EnableViewState="False"
                                            Width="700px" Height="500px" Skin="Silk" NewLineMode="Br">
                                            <CssFiles>
                                                <telerik:EditorCssFile Value="~/asset/css/ZeusTypeFoundry.css" />
                                            </CssFiles>
                                        </telerik:RadEditor>
                                        <%--
			                            <asp:RequiredFieldValidator ID="rfvContenuto2" ErrorMessage="Obbligatorio"
				                            runat="server" ControlToValidate="txtContenuto2" SkinID="ZSSM_Validazione01"
				                            Display="Dynamic" ValidationGroup="myValidation">
			                            </asp:RequiredFieldValidator>
                                        --%>
                                    </td>
                                </tr>
                            </asp:Panel>

                            <asp:Panel ID="pnlImmagine1" runat="server" Visible='<%# Eval("Immagine1_IsActive") %>'>
                                <tr>
                                    <td class="BlockBoxValue" colspan="2">
                                        <%--<asp:TextBox ID="txtImmagine1" runat="server" Text='<%# Bind("Immagine1") %>' MaxLength="100" Columns="100"></asp:TextBox>--%>

                                        <dlc:ImageRaider ID="ImageRaider1" runat="server" ZeusIdModuloIndice="1" ZeusLangCode="ITA" ZeusIdModulo='<%# Eval("ZeusIdModulo") %>'
                                            BindPzFromDB="true" IsEditMode="true" DefaultEditBetaImage='<%# Eval("Immagine1") %>'
                                            DefaultEditGammaImage='<%# Eval("Immagine2") %>'
                                            ImageAlt='<%# Eval("Immagine12Alt") %>' OnDataBinding="ImageRaider_DataBinding" />
                                        <asp:HiddenField ID="FileNameBetaHiddenField" runat="server" Value='<%# Bind("Immagine1") %>' />
                                        <asp:HiddenField ID="FileNameGammaHiddenField" runat="server" Value='<%# Bind("Immagine2") %>' />
                                        <asp:HiddenField ID="Immagine12AltHiddenField" runat="server" Value='<%# Bind("Immagine12Alt") %>' />

                                        <%--
			                            <asp:RequiredFieldValidator ID="rfvImmagine1" ErrorMessage="Obbligatorio"
				                            runat="server" ControlToValidate="txtContenuto2" SkinID="ZSSM_Validazione01"
				                            Display="Dynamic" ValidationGroup="myValidation">
			                            </asp:RequiredFieldValidator>
                                        --%>
                                    </td>
                                </tr>
                            </asp:Panel>

                            <asp:Panel ID="pnlImmagine3" runat="server" Visible='<%# Eval("Immagine3_IsActive") %>'>
                                <tr>
                                    <td class="BlockBoxValue" colspan="2">
                                        <%--<asp:TextBox ID="txtImmagine2" runat="server" Text='<%# Bind("Immagine2") %>' MaxLength="100" Columns="100"></asp:TextBox>--%>
                                        <dlc:ImageRaider ID="ImageRaider2" runat="server" ZeusIdModuloIndice="2" ZeusLangCode="ITA" ZeusIdModulo='<%# Eval("ZeusIdModulo") %>'
                                            BindPzFromDB="true" IsEditMode="true" DefaultEditBetaImage='<%# Eval("Immagine3") %>'
                                            DefaultEditGammaImage='<%# Eval("Immagine4") %>'
                                            ImageAlt='<%# Eval("Immagine34Alt") %>'
                                            OnDataBinding="ImageRaider_DataBinding" />
                                        <asp:HiddenField ID="FileNameBeta2HiddenField" runat="server" Value='<%# Bind("Immagine3") %>' />
                                        <asp:HiddenField ID="FileNameGamma2HiddenField" runat="server" Value='<%# Bind("Immagine4") %>' />
                                        <asp:HiddenField ID="Immagine34AltHiddenField" runat="server" Value='<%# Bind("Immagine34Alt") %>' />

                                        <%--
			                            <asp:RequiredFieldValidator ID="rfvImmagine2" ErrorMessage="Obbligatorio"
				                            runat="server" ControlToValidate="txtImmagine1" SkinID="ZSSM_Validazione01"
				                            Display="Dynamic" ValidationGroup="myValidation">
			                            </asp:RequiredFieldValidator>
                                        --%>
                                    </td>
                                </tr>
                            </asp:Panel>
                        </table>
                    </div>

                    <asp:Panel ID="ZMCF_BoxSEO" runat="server" Visible='<%# bool.Parse(Eval("TitoloBrowser_IsActive").ToString()) || bool.Parse(Eval("TagDescription_IsActive").ToString()) || bool.Parse(Eval("TagKeywords_IsActive").ToString())%>'>
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="Label9" runat="server" Text="SEO Search Engine Optimization"></asp:Label>
                            </div>
                            <table>
                                <asp:Panel ID="pnlTitoloBrowser" runat="server" Visible='<%# Eval("TitoloBrowser_IsActive") %>'>
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="lblTitoloBrowser" SkinID="FieldDescription" runat="server" Text='<%# Bind("TitoloBrowser_Label") %>'></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:TextBox ID="txtTitoloBrowser" runat="server" Text='<%# Bind("TitoloBrowser") %>' MaxLength="100" Columns="100"></asp:TextBox>
                                            <%--
			                                <asp:RequiredFieldValidator ID="rfvTitoloBrowser" ErrorMessage="Obbligatorio"
				                                runat="server" ControlToValidate="txtTitoloBrowser" SkinID="ZSSM_Validazione01"
				                                Display="Dynamic" ValidationGroup="myValidation">
			                                </asp:RequiredFieldValidator>
                                            --%>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlTagDescription" runat="server" Visible='<%# Eval("TagDescription_IsActive") %>'>
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="lblTagDescription" SkinID="FieldDescription" runat="server" Text='<%# Bind("TagDescription_Label") %>'></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:TextBox ID="txtTagDescription" runat="server" Text='<%# Bind("TagDescription") %>' MaxLength="130" Columns="280"></asp:TextBox>
                                            <%--
			                                <asp:RequiredFieldValidator ID="rfvTagDescription" ErrorMessage="Obbligatorio"
				                                runat="server" ControlToValidate="txtTagDescription" SkinID="ZSSM_Validazione01"
				                                Display="Dynamic" ValidationGroup="myValidation">
			                                </asp:RequiredFieldValidator>
                                            --%>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlTagKeywords" runat="server" Visible='<%# Eval("TagKeywords_IsActive") %>'>
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="lblTagKeywords" SkinID="FieldDescription" runat="server" Text='<%# Bind("TagKeywords_Label") %>'></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:TextBox ID="txtTagKeywords" runat="server" Text='<%# Bind("TagKeywords") %>' MaxLength="130" Columns="280"></asp:TextBox>
                                            <%--
			                                <asp:RequiredFieldValidator ID="rfvTagKeywords" ErrorMessage="Obbligatorio"
				                                runat="server" ControlToValidate="txtTagKeywords" SkinID="ZSSM_Validazione01"
				                                Display="Dynamic" ValidationGroup="myValidation">
			                                </asp:RequiredFieldValidator>
                                            --%>
                                        </td>
                                    </tr>
                                </asp:Panel>
                            </table>
                        </div>
                    </asp:Panel>

                    <asp:HiddenField ID="RecordEdtUserHidden" runat="server" Value='<%# Bind("RecordEdtUser", "{0}") %>' OnDataBinding="UtenteEdit" />
                    <asp:HiddenField ID="RecordEdtDateHidden" runat="server" Value='<%# Bind("RecordEdtDate", "{0}") %>' OnDataBinding="DataEdit" />
                    <div style="padding: 10px 0 0 0; margin: 0;">
                        <dlc:zeusdatatracking ID="Zeusdatatracking1" runat="server" />
                    </div>

                    <asp:HiddenField ID="zdtRecordNewUser" runat="server" Value='' />
                    <asp:HiddenField ID="zdtRecordNewDate" runat="server" Value='' />
                    <asp:HiddenField ID="zdtRecordEdtUser" runat="server" Value='<%# Eval("RecordEdtUser") %>' />
                    <asp:HiddenField ID="zdtRecordEdtDate" runat="server" Value='<%# Eval("RecordEdtDate") %>' />

                </div>


            </div>
            <br />
            <div align="center">
                <dlc:mySummaryValidation ID="mySummaryValidation1" runat="server" SkinID="ZSSM_Validazione01" />
                <asp:LinkButton ID="InsertButton" runat="server" SkinID="ZSSM_Button01" CausesValidation="True"
                    CommandName="Update" Text="Salva dati" OnClick="Save_File_Upload" ValidationGroup="myValidation"
                    OnClientClick="this.blur();"></asp:LinkButton>
                <img src="../SiteImg/Spc.gif" width="20" />
            </div>
        </EditItemTemplate>
    </asp:FormView>

    <asp:SqlDataSource ID="dsWebCmp" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SELECT * FROM tbWebComponent As recData
	        INNER JOIN tbWebComponentConfig AS recConfig ON recData.IDComponent = recConfig.IDComponent
        WHERE recData.IDComponent = @IDComponent"
        UpdateCommand="UPDATE tbWebComponent
            SET Testo1 = @Testo1, Testo2 = @Testo2, Testo3 = @Testo3, 
                DescBreve1 = @DescBreve1, Contenuto1 = @Contenuto1, Contenuto2 = @Contenuto2, 
                Immagine1 = @Immagine1, Immagine2 = @Immagine2, Immagine12Alt = @Immagine12Alt, 
                Immagine3 = @Immagine3, Immagine4 = @Immagine4, Immagine34Alt = @Immagine34Alt, 
                TitoloBrowser = @TitoloBrowser, TagDescription = @TagDescription, TagKeywords = @TagKeywords, 
                RecordEdtUser = @RecordEdtUser, RecordEdtDate = @RecordEdtDate 
        WHERE IDComponent = @IDComponent
        AND ZeusLangCode = @ZeusLangCode"
        OnUpdated="dsWebCmp_Updated"
        OnSelected="dsWebCmp_Selected">
        <SelectParameters>
            <asp:QueryStringParameter Name="IDComponent" QueryStringField="XRI" />
            <asp:QueryStringParameter Name="ZeusLangCode" QueryStringField="Lang" Type="String"
                DefaultValue="ITA" />
        </SelectParameters>
        <UpdateParameters>
            <asp:QueryStringParameter Name="IDComponent" QueryStringField="XRI" />
            <asp:QueryStringParameter Name="ZeusLangCode" QueryStringField="Lang" Type="String" DefaultValue="ITA" />

            <asp:Parameter Name="Testo1" Type="String" />
            <asp:Parameter Name="Testo2" Type="String" />
            <asp:Parameter Name="Testo3" Type="String" />
            <asp:Parameter Name="DescBreve1" Type="String" />
            <asp:Parameter Name="Contenuto1" Type="String" />
            <asp:Parameter Name="Contenuto2" Type="String" />
            <asp:Parameter Name="Immagine1" Type="String" />
            <asp:Parameter Name="Immagine2" Type="String" />
            <asp:Parameter Name="Immagine12Alt" Type="String" />
            <asp:Parameter Name="Immagine3" Type="String" />
            <asp:Parameter Name="Immagine4" Type="String" />
            <asp:Parameter Name="Immagine34Alt" Type="String" />
            <asp:Parameter Name="TitoloBrowser" Type="String" />
            <asp:Parameter Name="TagDescription" Type="String" />
            <asp:Parameter Name="TagKeywords" Type="String" />
            <asp:Parameter Name="RecordEdtUser" Type="String" />
            <asp:Parameter Name="RecordEdtDate" Type="DateTime" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
