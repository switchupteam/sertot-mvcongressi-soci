﻿using System;
using System.Data.SqlClient;
using System.Web.Security;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per Content_Lst
/// </summary>
public partial class Publisher_Content_Lst : System.Web.UI.Page {

    static string TitoloPagina = "Componenti Web";

    protected void Page_Load(object sender, EventArgs e) {
        delinea myDelinea = new delinea();
        TitleField.Value = TitoloPagina;

        ZIM.Value = Server.HtmlEncode(Request.QueryString["ZIM"]);

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI")) && (Request.QueryString["XRI"].Length > 0) && (isOK()))
            SingleRecordPanel.Visible = true;

    }//fine Page_Load 



    private bool isOK() {
        try {
            if (Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/WebComponents/Content_Edt.aspx") > 0)
                return true;
            else
                return false;
        }
        catch (Exception p) {
            return false;
        }

    }//fine isOK


    private string GetLang() {
        string Lang = "ITA";

        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;
    }//fine GetLang

}