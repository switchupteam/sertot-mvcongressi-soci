﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="Content_Lst.aspx.cs"
    Inherits="Publisher_Content_Lst"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" />
    <asp:HiddenField ID="ZIM" runat="server" />
    
    <asp:Panel ID="SingleRecordPanel" runat="server" Visible="false">
        <div class="LayGridTitolo1">
            <asp:Label ID="CaptionallRecordLabel" SkinID="GridTitolo1" runat="server" Text="Ultimo contenuto creato / aggiornato"></asp:Label>
        </div>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
            AutoGenerateColumns="False" DataSourceID="SingleRecordSqlDatasource" 
            PageSize="30">
            <Columns>
            <asp:TemplateField HeaderText="IDComponent" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="IDComponent" runat="server" Text='<%# Eval("IDComponent") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>           

            <asp:BoundField DataField="wcNome" HeaderText="Identificativo" SortExpression="wcNome" />
            
            <asp:BoundField DataField="DataModifica" HeaderText="Data Modifica" SortExpression="DataModifica"
                DataFormatString="{0}">
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>

            <asp:HyperLinkField DataNavigateUrlFields="IDComponent,ZeusLangCode,ZeusIdModulo"
                HeaderText="Modifica" Text="Modifica" DataNavigateUrlFormatString="Content_Edt.aspx?XRI={0}&Lang={1}&ZIM={2}">
                <ControlStyle CssClass="GridView_ZeusButton1" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:HyperLinkField>
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SingleRecordSqlDatasource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
            SelectCommand="SELECT wcNome, recData.IDComponent, ISNULL(RecordEdtDate, {d'1900-01-01'}) AS DataModifica, ZeusLangCode, ZeusIdModulo
            FROM tbWebComponent As recData 
                INNER JOIN tbWebComponentConfig AS recConfig ON recData.IDComponent = recConfig.IDComponent
            WHERE recData.IDComponent = @IDComponent">
            <SelectParameters>
                <asp:QueryStringParameter Name="IDComponent" QueryStringField="XRI" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <div class="VertSpacerMedium">
        </div>
    </asp:Panel>
    <div class="LayGridTitolo1">
        <asp:Label ID="Label2" runat="server" SkinID="GridTitolo1" Text="Elenco contenuti disponibili"></asp:Label>
    </div>
    <asp:GridView ID="GridView2" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" DataSourceID="tbWebComponent_LstSqlDataSource"
        PageSize="50">
        <Columns>
            <asp:TemplateField HeaderText="IDComponent" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="IDComponent" runat="server" Text='<%# Eval("IDComponent") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>           

            <asp:BoundField DataField="wcNome" HeaderText="Identificativo" SortExpression="wcNome" />
            
            <asp:BoundField DataField="DataModifica" HeaderText="Data Modifica" SortExpression="DataModifica"
                DataFormatString="{0}">
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>

            <asp:HyperLinkField DataNavigateUrlFields="IDComponent,ZeusLangCode,ZeusIdModulo"
                HeaderText="Modifica" Text="Modifica" DataNavigateUrlFormatString="Content_Edt.aspx?XRI={0}&Lang={1}&ZIM={2}">
                <ControlStyle CssClass="GridView_ZeusButton1" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:HyperLinkField>
        </Columns>
        <EmptyDataTemplate>
            Dati non presenti
            <br />
            <br />
            <br />
            <br />
            <br />
        </EmptyDataTemplate>
        <EmptyDataRowStyle BackColor="White" Width="500px" BorderColor="Red" BorderWidth="0px"
            Height="200px" />
    </asp:GridView>
    <asp:SqlDataSource ID="tbWebComponent_LstSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SELECT wcNome, recData.IDComponent, ISNULL(RecordEdtDate, {d'1900-01-01'}) AS DataModifica, ZeusLangCode, ZeusIdModulo
            FROM tbWebComponent As recData 
                INNER JOIN tbWebComponentConfig AS recConfig ON recData.IDComponent = recConfig.IDComponent
            WHERE ZeusIsAlive = 1 AND ZeusLangCode = @ZeusLangCode
            ORDER BY wcNome">
        <SelectParameters>
            <asp:QueryStringParameter Name="ZeusLangCode" QueryStringField="Lang" />            
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
