﻿using ASP;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Pur essendo simile ad un modulo Publisher o PageDesigner, questa pagina si comporta in modo diverso. Visibilità e label vengono prese dal DB invece che da file XML.
/// Al momento (20170720) non esiste la possibilità di creare una pagina nuova o di creare la versione inglese da Zeus, bisogna farlo da DB.
/// </summary>
public partial class WebComponents_Content_Edt : System.Web.UI.Page {
    public static String strConnessione {
        get { return System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString; }
    }

    static string TitoloPagina = "Modifica WebComponents";
    private string sLang { get; set; }

    protected void Page_Load(object sender, EventArgs e) {
        TitleField.Value = TitoloPagina;

        delinea myDelinea = new delinea();

        if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI")))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        XRI.Value = Server.HtmlEncode(Request.QueryString["XRI"]);

        try {
            LinkButton InsertButton = (LinkButton)frmMain.FindControl("InsertButton");
            InsertButton.CommandName = "Update";

            InsertButton.Attributes.Add("onclick", "Validate()");
        }
        catch (Exception p) {
            Response.Write(p.ToString());
        }
    }

    private string GetLang() {
        string Lang = "ITA";

        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;
    }
    
    protected void ZeusIdHidden_Load(object sender, EventArgs e) {
        HiddenField Guid = (HiddenField)sender;
        if (Guid.Value.Length == 0)
            Guid.Value = System.Guid.NewGuid().ToString();
    }

    protected void UtenteEdit(object sender, EventArgs e) {
        HiddenField RecordEdtUserHidden = (HiddenField)sender;
        RecordEdtUserHidden.Value = Membership.GetUser().ProviderUserKey.ToString();
    }

    protected void DataEdit(object sender, EventArgs e) {
        HiddenField RecordEdtDateHidden = (HiddenField)sender;
        RecordEdtDateHidden.Value = DateTime.Now.ToString();
    }

    protected void ImageRaider_DataBinding(object sender, EventArgs e) {
        ImageRaider ImageRaider1 = (ImageRaider)sender;
        ImageRaider1.SetDefaultEditBetaImage();
        ImageRaider1.SetDefaultEditGammaImage();
    }

    protected void Save_File_Upload(object sender, EventArgs e) {
        HiddenField FileNameBetaHiddenField = (HiddenField)frmMain.FindControl("FileNameBetaHiddenField");
        HiddenField FileNameGammaHiddenField = (HiddenField)frmMain.FindControl("FileNameGammaHiddenField");
        HiddenField Immagine12AltHiddenField = (HiddenField)frmMain.FindControl("Immagine12AltHiddenField");
        ImageRaider ImageRaider1 = (ImageRaider)frmMain.FindControl("ImageRaider1");

        HiddenField FileNameBeta2HiddenField = (HiddenField)frmMain.FindControl("FileNameBeta2HiddenField");
        HiddenField FileNameGamma2HiddenField = (HiddenField)frmMain.FindControl("FileNameGamma2HiddenField");
        HiddenField Immagine34AltHiddenField = (HiddenField)frmMain.FindControl("Immagine34AltHiddenField");
        ImageRaider ImageRaider2 = (ImageRaider)frmMain.FindControl("ImageRaider2");

        LinkButton InsertButton = (LinkButton)frmMain.FindControl("InsertButton");

        if (InsertButton.CommandName != string.Empty) {

            if (ImageRaider1.GenerateBeta(string.Empty))
                FileNameBetaHiddenField.Value = ImageRaider1.ImgBeta_FileName;
            else FileNameBetaHiddenField.Value = ImageRaider1.DefaultBetaImage;

            if (ImageRaider1.GenerateGamma(string.Empty))
                FileNameGammaHiddenField.Value = ImageRaider1.ImgGamma_FileName;
            else FileNameGammaHiddenField.Value = ImageRaider1.DefaultGammaImage;


            if (ImageRaider2.GenerateBeta(string.Empty))
                FileNameBeta2HiddenField.Value = ImageRaider2.ImgBeta_FileName;
            else FileNameBeta2HiddenField.Value = ImageRaider2.DefaultBetaImage;

            if (ImageRaider2.GenerateGamma(string.Empty))
                FileNameGamma2HiddenField.Value = ImageRaider2.ImgGamma_FileName;
            else FileNameGamma2HiddenField.Value = ImageRaider2.DefaultGammaImage;


        }//fine if
    }//fine Save_File_Upload

    protected void dsWebCmp_Updated(object sender, SqlDataSourceStatusEventArgs e) {
        try {
            if (e.Exception == null)
                Response.Redirect("~/Zeus/WebComponents/Content_Lst.aspx?XRI=" + Server.HtmlEncode(Request.QueryString["XRI"]) + "&ZIM=" + Server.HtmlEncode(Request.QueryString["ZIM"]) + "&Lang=" + GetLang());
            else Response.Write("~/Zeus/System/Message.aspx?UpdateErr");
        }
        catch (Exception p) {
            Response.Write(p.ToString());
        }
    }//fine dsWebCmp_Updated

    protected void dsWebCmp_Selected(object sender, SqlDataSourceStatusEventArgs e) {
        if ((e.Exception != null)
            || (e.AffectedRows <= 0))
            Response.Redirect("~/Zeus/System/Message.aspx?SelErr");
    }
}