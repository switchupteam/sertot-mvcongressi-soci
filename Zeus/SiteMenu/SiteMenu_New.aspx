﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="SiteMenu_New.aspx.cs"
    Inherits="SiteMenu_New"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" />
    <asp:HiddenField ID="ZIM" runat="server" />
    <asp:FormView ID="FormView1" runat="server" DefaultMode="Insert" DataSourceID="SiteMenuSqlDataSource"
        Width="100%">
        <InsertItemTemplate>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="VoceMenuLabel" runat="server" Text="Voce menù"></asp:Label>
                </div>
                <table>
                    <tr>
                        <td class="BlockBoxDescription" style="width: 150px">
                            <asp:Label ID="MenuLabel" runat="server" SkinID="FieldDescription" Text="Voce menù *"></asp:Label>
                        </td>
                        <td class="BlockBoxValue" style="width: 827px">
                            <asp:TextBox ID="MenuTextBox" runat="server" Text='<%# Bind("Menu") %>' MaxLength="100"
                                Columns="50"></asp:TextBox>
                            <asp:RequiredFieldValidator Vgroup="myValidation" ID="MenuRequiredFieldValidator"
                                runat="server" ControlToValidate="MenuTextBox" ErrorMessage="Campo obbligatorio"
                                Display="Dynamic" SkinID="ZSSM_Validazione01">Obbligatorio</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="BlockBoxDescription" style="width: 150px">
                            <asp:Label ID="MenuPadreLabel1" runat="server" SkinID="FieldDescription" Text="Menu padre"></asp:Label>
                        </td>
                        <td class="BlockBoxValue" style="width: 827px">
                            <asp:DropDownList ID="MenuParent" runat="server" AppendDataBoundItems="True" Enabled="false"
                               OnSelectedIndexChanged="MenuParent_SaveValue">
                            </asp:DropDownList>
                            <asp:HiddenField ID="ID2MenuParentHiddenField" runat="server" Value='<%# Bind("ID2MenuParent") %>' />
                             <asp:RequiredFieldValidator Vgroup="myValidation" ID="RequiredFieldValidator1"
                                runat="server" ControlToValidate="MenuParent" ErrorMessage="Campo obbligatorio"
                                Display="Dynamic" SkinID="ZSSM_Validazione01">Obbligatorio</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="BlockBoxDescription" style="width: 150px">
                            <asp:Label ID="UrlLabel" runat="server" SkinID="FieldDescription" Text="Url"></asp:Label>
                        </td>
                        <td class="BlockBoxValue" style="width: 827px">
                            <asp:TextBox ID="UrlTextBox" runat="server" Text='<%# Bind("Url") %>' MaxLength="250"
                                Columns="150"></asp:TextBox>
                        </td>
                    </tr>
                    <asp:Panel ID="ZMCF_TargetWindow" runat="server">
                        <tr>
                            <td class="BlockBoxDescription" style="width: 150px">
                                <asp:Label ID="Label1" runat="server" SkinID="FieldDescription" Text="Target"></asp:Label>
                            </td>
                            <td class="BlockBoxValue" style="width: 827px">
                                <asp:DropDownList ID="TargetWindowDropDownList" runat="server" SelectedValue='<%# Bind("TargetWindow") %>'>
                                    <asp:ListItem Text="Stessa finestra" Value="_self" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Nuova finestra" Value="_blank"></asp:ListItem>
                                    <asp:ListItem Text="Rimuovi frameset" Value="_top"></asp:ListItem>
                                    <asp:ListItem Text="Frameset precedente" Value="_parent"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </asp:Panel>
                </table>
            </div>

            <asp:Panel ID="ZMCF_Ruoli" runat="server">
                <div class="BlockBox">
                    <div class="BlockBoxHeader">
                        <asp:Label ID="RuoliLabel" runat="server" Text="Ruoli"></asp:Label>
                    </div>
                    <table>
                        <tr>
                            <td class="BlockBoxDescription">
                                <asp:Label ID="InfoRoleLabel" runat="server" SkinID="FieldDescription" Text="Visualizza menù sui ruoli"></asp:Label>
                            </td>
                            <td class="BlockBoxHeader" style="background-color: #FFFFFF">
                                <asp:CheckBoxList ID="Roles_List" runat="server" RepeatDirection="Vertical" SkinID="FieldValue">
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="Planner" runat="server" Text="Planner"></asp:Label>
                </div>
                <table>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="PZL_PWArea1_NewLabel" SkinID="FieldDescription" runat="server" Text="Menù di navigazione"></asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:CheckBox ID="PW_Area1CheckBox" runat="server" Checked='<%# Bind("PW_Area1") %>' />
                            <asp:Label ID="AttivaDa_Label" runat="server" Text="Attiva pubblicazione dalla data"
                                SkinID="FieldValue"></asp:Label>
                            <asp:TextBox ID="PWI_Area1TextBox" runat="server" Text='<%# Bind("PWI_Area1", "{0:d}") %>'
                                Columns="10" MaxLength="10" OnDataBinding="DataIniziale"></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator3"
                                runat="server" ControlToValidate="PWI_Area1TextBox" SkinID="ZSSM_Validazione01"
                                Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="RegularExpressionValidator2"
                                runat="server" ControlToValidate="PWI_Area1TextBox" SkinID="ZSSM_Validazione01"
                                Display="Dynamic" ErrorMessage="*" 
                                ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$">
                                Formato data richiesto: GG/MM/AAAA</asp:RegularExpressionValidator>
                            <asp:Label ID="AttivaA_Label" runat="server" Text="alla data" SkinID="FieldValue"></asp:Label>
                            <asp:TextBox ID="PWF_Area1TextBox" runat="server" Text='<%# Bind("PWF_Area1", "{0:d}") %>'
                                Columns="10" MaxLength="10" OnDataBinding="DataLimite"></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator4"
                                runat="server" ControlToValidate="PWF_Area1TextBox" SkinID="ZSSM_Validazione01"
                                Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                    ID="RegularExpressionValidator3" runat="server" ControlToValidate="PWF_Area1TextBox"
                                    SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="*" 
                                    ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$">
                                    Formato data richiesto: GG/MM/AAAA</asp:RegularExpressionValidator>
                            <asp:CompareValidator ValidationGroup="myValidation" ID="CompareValidator1" runat="server"
                                ControlToCompare="PWI_Area1TextBox" ControlToValidate="PWF_Area1TextBox" SkinID="ZSSM_Validazione01"
                                Display="Dynamic" ErrorMessage="La data iniziale deve essere precedente o uguale alla data finale"
                                Operator="GreaterThanEqual" Type="Date"></asp:CompareValidator>
                        </td>
                    </tr>
                </table>
            </div>
            <asp:HiddenField ID="RecordNewUserHidden" runat="server" Value='<%# Bind("RecordNewUser", "{0}") %>'
                OnDataBinding="UtenteCreazione" />
            <asp:HiddenField ID="RecordNewDateHidden" runat="server" Value='<%# Bind("RecordNewDate", "{0}") %>'
                OnDataBinding="DataOggi" />
            <asp:HiddenField ID="RolesHiddenField" runat="server" Value='<%# Bind("Roles") %>' />
            <asp:HiddenField ID="OrdinamentoHiddenField" runat="server" Value='<%# Bind("Ordinamento") %>' />
            <asp:HiddenField ID="ZeusIdHidden" runat="server" OnDataBinding="ZeusIdHidden_Load"
                Value='<%# Bind("ZeusId", "{0}") %>' />
            <div align="center">
                <dlc:mySummaryValidation ID="mySummaryValidation1" runat="server" SkinID="ZSSM_Validazione01" />
                <asp:LinkButton ID="InsertButton" runat="server" SkinID="ZSSM_Button01" CausesValidation="True"
                    Vgroup="myValidation" CommandName="Insert" Text="Salva dati" OnClick="InsertButton_Click"></asp:LinkButton>
            </div>
        </InsertItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="SiteMenuSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        InsertCommand=" SET DATEFORMAT dmy; INSERT INTO tbSiteMenu (Menu, ID2MenuParent, Url,TargetWindow, Immagine1, Roles, 
        Ordinamento,ZeusLangCode,ZeusIdModulo, PW_Area1, PWI_Area1, PWF_Area1, RecordNewUser,
         RecordNewDate, ZeusUserEditable, ZeusIsAlive,ZeusId)
          VALUES (@Menu,@ID2MenuParent,@Url,@TargetWindow,@Immagine1,@Roles,@Ordinamento,@ZeusLangCode,@ZeusIdModulo,
          @PW_Area1,@PWI_Area1,@PWF_Area1,@RecordNewUser,@RecordNewDate,@ZeusUserEditable,@ZeusIsAlive,@ZeusId)"
        OnInserted="SiteMenuSqlDataSource_Inserted">
        <InsertParameters>
            <asp:Parameter Name="ID2MenuParent" Type="Int32" DefaultValue="0" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="Menu" Type="String" />
            <asp:Parameter Name="Url" Type="String" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="TargetWindow" Type="String" ConvertEmptyStringToNull="false"
                DefaultValue="_self" />
            <asp:Parameter Name="Immagine1" Type="String" DefaultValue="ImgNonDisponibile.jpg" />
            <asp:Parameter Name="Roles" Type="String" ConvertEmptyStringToNull="false" DefaultValue="" />
            <asp:Parameter Name="Ordinamento" Type="Int32" ConvertEmptyStringToNull="false" DefaultValue="1" />
            <asp:Parameter Name="PW_Area1" Type="Boolean" />
            <asp:Parameter Name="PWI_Area1" Type="DateTime" />
            <asp:Parameter Name="PWF_Area1" Type="DateTime" />
            <asp:Parameter Name="RecordNewUser" />
            <asp:Parameter Name="RecordNewDate" Type="DateTime" />
            <asp:Parameter Name="ZeusUserEditable" Type="Boolean" DefaultValue="True" />
            <asp:Parameter Name="ZeusIsAlive" Type="Boolean" DefaultValue="True" />
            <asp:Parameter Name="ZeusId" />
            <asp:QueryStringParameter Name="ZeusLangCode" QueryStringField="Lang" Type="String" />
            <asp:QueryStringParameter Name="ZeusIdModulo" QueryStringField="ZIM" Type="String" />
        </InsertParameters>
    </asp:SqlDataSource>
</asp:Content>
