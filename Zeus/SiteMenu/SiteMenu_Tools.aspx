﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">
    
    static string TitoloPagina = "Controllo d'integrità voci menu";

    static int LIVELLI = 3;


    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;

    }//fine Page_Load


    private bool CheckIntegrityR(string ZeusIdModulo, string ZeusLangCode)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            SqlTransaction transaction = null;

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                transaction = connection.BeginTransaction();
                command.Transaction = transaction;



                ArrayList myArray = new ArrayList();
                ArrayList ID2MenuParentArrayList = new ArrayList();

                ID2MenuParentArrayList.Add("0");

                string[][] myMenuChange = null;
                string[] ID2MenuParent = null;

                int myOrder = 1;

                InfoLabel.Text = string.Empty;
                SqlDataReader reader = null;


                for (int index = 1; index <= LIVELLI; ++index)
                {


                    ID2MenuParent = (string[])ID2MenuParentArrayList.ToArray(typeof(string));
                    ID2MenuParentArrayList = new ArrayList();

                    for (int i = 0; i < ID2MenuParent.Length; ++i)
                    {

                        sqlQuery = "SELECT ID1Menu,Menu,Ordinamento FROM vwSiteMenu_All WHERE ZeusLangCode=@ZeusLangCode AND ZeusIdModulo=@ZeusIdModulo AND ID2MenuParent=@ID2MenuParent ORDER BY Ordinamento,ID1Menu";
                        command.CommandText = sqlQuery;
                        command.Parameters.Clear();

                        command.Parameters.Add("@ZeusIdModulo", System.Data.SqlDbType.NVarChar, 5);
                        command.Parameters["@ZeusIdModulo"].Value = ZeusIdModulo;

                        command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.NVarChar, 3);
                        command.Parameters["@ZeusLangCode"].Value = ZeusLangCode;

                        command.Parameters.Add("@ID2MenuParent", System.Data.SqlDbType.Int, 4);
                        command.Parameters["@ID2MenuParent"].Value = ID2MenuParent[i];

                        reader = command.ExecuteReader();

                        myOrder = 1;

                        while (reader.Read())
                        {


                            ID2MenuParentArrayList.Add(reader.GetInt32(0).ToString());

                            if (myOrder != reader.GetInt32(2))
                            {

                                string[] myData = new string[3];
                                myData[0] = reader.GetInt32(0).ToString();
                                myData[1] = reader.GetString(1);
                                myData[2] = myOrder.ToString();
                                myArray.Add(myData);
                            }

                            ++myOrder;
                        }//fine while

                        reader.Close();

                    }//fine for


                    myMenuChange = (string[][])myArray.ToArray(typeof(string[]));
                    myArray = new ArrayList();

                    InfoLabel.Text += "----------------------- LIVELLO " + index + " ----------------------------------<br />";

                    for (int i = 0; i < myMenuChange.Length; ++i)
                    {

                        sqlQuery = "UPDATE tbSiteMenu SET Ordinamento=@Ordinamento WHERE ID1Menu=@ID1Menu";
                        command.CommandText = sqlQuery;
                        command.Parameters.Clear();

                        command.Parameters.Add("@Ordinamento", System.Data.SqlDbType.NVarChar, 4);
                        command.Parameters["@Ordinamento"].Value = myMenuChange[i][2];

                        command.Parameters.Add("@ID1Menu", System.Data.SqlDbType.Int, 4);
                        command.Parameters["@ID1Menu"].Value = myMenuChange[i][0];

                        command.ExecuteNonQuery();

                        InfoLabel.Text += "Modificata la voce menù : \"" + myMenuChange[i][1] + "\"<br/>";
                        InfoLabel.Text += "Nuova posizione in ordinamento: " + myMenuChange[i][2] + "<br /><br />";

                    }//fine for




                }//fine for LIVELLI
                transaction.Commit();
                InfoLabel.Text += "Controllo integrità concluso con successo";

                return true;
            }
            catch (Exception p)
            {

                InfoLabel.Text = p.ToString();
                transaction.Rollback();
                return false;
            }
        }//fine using
    }//fine CheckIntegrityR


    protected void CheckIntegrityButton_Click(object sender, EventArgs e)
    {
        InfoLabel.Text = "CONTROLLO IN CORSO.....";
        CheckIntegrityR(Server.HtmlEncode(Request.QueryString["ZIM"]), Server.HtmlEncode(Request.QueryString["Lang"]));

    }//fine CheckIntegrityButton_Click
    
    
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" Visible="False" />
    <div class="BlockBox">
        <div class="BlockBoxHeader">
            <asp:Label ID="Dettaglio_contenuto" runat="server" Text="Controllo integrita menù di navigazione"></asp:Label></div>
        <table>
            <tr>
                <td class="BlockBoxDescription">
                </td>
                <td class="BlockBoxValue">
                    <asp:Label ID="InfoLabel" runat="server" SkinID="FieldValue"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="BlockBoxDescription">
                <asp:Button ID="CheckIntegrityButton" runat="server" OnClick="CheckIntegrityButton_Click"
                        SkinID="ZSSM_Button01" Text="Avvia controllo integrità" />
                </td>
                <td class="BlockBoxValue">
                    
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
