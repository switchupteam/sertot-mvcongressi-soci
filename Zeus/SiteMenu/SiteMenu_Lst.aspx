﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">
    
    static string TitoloPagina = "Elenco menù di navigazione";
    static int UP = 8;
    static int DOWN = 9;
    static int EDT = 12;


    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;

        delinea myDelinea = new delinea();

        if (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM"))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        ZIM.Value = Server.HtmlEncode(Request.QueryString["ZIM"]);

        if (!ReadXML(ZIM.Value, GetLang()))
            Response.Write("~/Zeus/System/Message.aspx?1");


        ReadXML_Localization(ZIM.Value, GetLang());


        Livello1HyperLink.NavigateUrl = "~/Zeus/SiteMenu/SiteMenu_Lst.aspx?ZIM=" + ZIM.Value+ "&Lang=" 
            + GetLang() + "&XRI=1";
        Livello2HyperLink.NavigateUrl = "~/Zeus/SiteMenu/SiteMenu_Lst.aspx?ZIM=" + ZIM.Value
            + "&Lang=" + GetLang() + "&XRI=2";
        Livello3HyperLink.NavigateUrl = "~/Zeus/SiteMenu/SiteMenu_Lst.aspx?ZIM=" + ZIM.Value
            + "&Lang=" + GetLang() + "&XRI=3";
        TutteHyperLink.NavigateUrl = "~/Zeus/SiteMenu/SiteMenu_Lst.aspx?ZIM=" + ZIM.Value
            + "&Lang=" + GetLang() + "&XRI=4";


        if ((myDelinea.AntiSQLInjectionRight(Request.QueryString, "XRI")) && (PopulateLinks(Request.QueryString["XRI"])))
            dsMenu.SelectCommand += " AND Livello=" + Server.HtmlEncode(Request.QueryString["XRI"]);


        dsMenu.SelectCommand += " ORDER BY OrdLiv1, OrdLiv2, OrdLiv3";

    }//fine Page__Load

    private string GetLang()
    {
        string Lang = "ITA";

        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;
    }

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_SiteMenu.xml"));

            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            if (childNode.Name.Equals("ZML_TitoloPagina_Lst"))
                            {
                                TitleField.Value = childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_SiteMenu.xml"));





            if (!Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_PermitLst").InnerText))
                return false;


            GridView1.Columns[12].Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_ShowRolesLst").InnerText);

            string LivString = "";
            string StartLivString = " AND (";
            string ENDLivString = ") ";

            //Check permit 3 levels
            for (int ii = 1; ii <= 3; ii++)
            {
                if (Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_PermitLevel"+ii).InnerText))
                {
                    if (LivString.Length == 0)
                    {
                        LivString += StartLivString;
                        LivString += " (Livello="+ii+") ";
                    }
                    else
                        LivString += " OR (Livello=" + ii + ") ";

                }
                    //4 == Liv1
                    GridView1.Columns[3+ii].Visible =
                                        Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_PermitLevel"+ii).InnerText);
            }
            
            if(LivString.Length>0)
            {
                LivString+=ENDLivString;
                dsMenu.SelectCommand += LivString;
            }
            
            

            return true;
        }
        catch (Exception e)
        {
            return false;
        }

    }//fine ReadXML


    


    private bool PopulateLinks(string XRI)
    {
        try
        {
            switch (XRI)
            {
                case "1":
                    Livello1HyperLink.CssClass = "HyperlinkSkin_Filter_Selected";
                    break;

                case "2":
                    Livello2HyperLink.CssClass = "HyperlinkSkin_Filter_Selected";
                    break;

                case "3":
                    Livello3HyperLink.CssClass = "HyperlinkSkin_Filter_Selected";
                    break;

                default:
                    TutteHyperLink.CssClass = "HyperlinkSkin_Filter_Selected";
                    return false;
                    break;

            }

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }

    }

    private bool Move(string MoveCommand, string Ordinamento, string ID2MenuParent, string ID1MenuNodeToMove)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;
        List<int> RecordToOrder = new List<int>();

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {
            SqlTransaction transaction = null;
            
            try
            {

                int intNewOrder = Convert.ToInt32(Ordinamento);
                int intOldOrder = intNewOrder;                

                connection.Open();

                SqlCommand command = connection.CreateCommand();

                transaction = connection.BeginTransaction();
                command.Transaction = transaction;
                sqlQuery = @"SELECT ID1Menu
                            FROM tbSiteMenu 
                            WHERE ID2MenuParent=@ID2MenuParent AND ZeusIsAlive=1 AND ZeusLangCode=@ZeusLangCode 
                                AND ZeusIdModulo=@ZeusIdModulo 
                            ORDER BY Ordinamento ASC";

                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID2MenuParent", System.Data.SqlDbType.Int, 4);
                command.Parameters["@ID2MenuParent"].Value = ID2MenuParent;

                command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.NVarChar, 3);
                command.Parameters["@ZeusLangCode"].Value = Server.HtmlEncode(Request.QueryString["Lang"]);

                command.Parameters.Add("@ZeusIdModulo", System.Data.SqlDbType.NVarChar, 5);
                command.Parameters["@ZeusIdModulo"].Value = Server.HtmlEncode(Request.QueryString["ZIM"]);

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    RecordToOrder.Add(Convert.ToInt32(reader["ID1Menu"]));
                }

                reader.Close();

                int j = 1;

                foreach (int i in RecordToOrder)
                {
                    command.Parameters.Clear();

                    sqlQuery = @"UPDATE tbSiteMenu 
                                SET Ordinamento = @Ordinamento
                                WHERE ID1Menu = @ID1Menu
                                ";

                    command.Parameters.Add("@ID1Menu", System.Data.SqlDbType.Int);
                    command.Parameters["@ID1Menu"].Value = i;
                    command.Parameters.Add("@Ordinamento", System.Data.SqlDbType.Int);
                    command.Parameters["@Ordinamento"].Value = j;
                    command.CommandText = sqlQuery;

                    command.ExecuteNonQuery();

                    j++;
                }

                sqlQuery = @"SELECT Ordinamento 
                            FROM tbSiteMenu 
                            WHERE ID1Menu = @ID1Menu
                            ";
                command.CommandText = sqlQuery;
                command.Parameters.Clear();

                command.Parameters.Add("@ID1Menu", System.Data.SqlDbType.Int);
                command.Parameters["@ID1Menu"].Value = ID1MenuNodeToMove;

                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    intNewOrder = Convert.ToInt32(reader["Ordinamento"]);
                }

                reader.Close();

                intOldOrder = intNewOrder;

                switch (MoveCommand)
                {
                    case "Up":
                        --intNewOrder;
                        break;

                    case "Down":
                        ++intNewOrder;
                        break;

                    default:
                        return false;
                        break;
                }

                if (intNewOrder >= j || intNewOrder < 1)
                    return true;

                sqlQuery = @"SELECT ID1Menu 
                            FROM tbSiteMenu 
                            WHERE ID2MenuParent=@ID2MenuParent AND ZeusIsAlive=1 AND ZeusLangCode=@ZeusLangCode 
                                AND ZeusIdModulo=@ZeusIdModulo AND Ordinamento=@Ordinamento";

                command.Parameters.Clear();

                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID2MenuParent", System.Data.SqlDbType.Int, 4);
                command.Parameters["@ID2MenuParent"].Value = ID2MenuParent;

                command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.NVarChar, 3);
                command.Parameters["@ZeusLangCode"].Value = Server.HtmlEncode(Request.QueryString["Lang"]);

                command.Parameters.Add("@ZeusIdModulo", System.Data.SqlDbType.NVarChar, 5);
                command.Parameters["@ZeusIdModulo"].Value = Server.HtmlEncode(Request.QueryString["ZIM"]);

                command.Parameters.Add("@Ordinamento", System.Data.SqlDbType.Int, 4);
                command.Parameters["@Ordinamento"].Value = intNewOrder;

                reader = command.ExecuteReader();

                string ID1MenuNodeToSwitch = string.Empty;

                while (reader.Read())
                {
                    ID1MenuNodeToSwitch = reader.GetInt32(0).ToString();
                }

                reader.Close();

                if (ID1MenuNodeToSwitch.Equals(ID1MenuNodeToMove))
                {
                    transaction.Rollback();                 
                    return false;
                }

                sqlQuery = "UPDATE tbSiteMenu SET Ordinamento=@Ordinamento WHERE ID1Menu=@ID1Menu";
                command.CommandText = sqlQuery;
                command.Parameters.Clear();

                command.Parameters.Add("@Ordinamento", System.Data.SqlDbType.NVarChar, 4);
                command.Parameters["@Ordinamento"].Value = intNewOrder.ToString();

                command.Parameters.Add("@ID1Menu", System.Data.SqlDbType.Int, 4);
                command.Parameters["@ID1Menu"].Value = ID1MenuNodeToMove;

                command.ExecuteNonQuery();

                sqlQuery = "UPDATE tbSiteMenu SET Ordinamento=@Ordinamento WHERE ID1Menu=@ID1Menu";
                command.CommandText = sqlQuery;
                command.Parameters.Clear();

                command.Parameters.Add("@Ordinamento", System.Data.SqlDbType.NVarChar, 4);
                command.Parameters["@Ordinamento"].Value = intOldOrder.ToString();

                command.Parameters.Add("@ID1Menu", System.Data.SqlDbType.Int, 4);
                command.Parameters["@ID1Menu"].Value = ID1MenuNodeToSwitch;

                command.ExecuteNonQuery();
                transaction.Commit();
                return true;
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
                transaction.Rollback();            
                return false;
            }

        }//fine Using

    }//fine Move

    private int getLastOrder(string ID2MenuParent)
    {

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT MAX(Ordinamento) FROM vwSiteMenu_All WHERE ID2MenuParent=@ID2MenuParent AND ZeusIsAlive=1 AND ZeusLangCode=@ZeusLangCode  AND ZeusIdModulo=@ZeusIdModulo";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID2MenuParent", System.Data.SqlDbType.Int, 4);
                command.Parameters["@ID2MenuParent"].Value = ID2MenuParent;

                command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.NVarChar, 3);
                command.Parameters["@ZeusLangCode"].Value = Server.HtmlEncode(Request.QueryString["Lang"]);

                command.Parameters.Add("@ZeusIdModulo", System.Data.SqlDbType.NVarChar, 5);
                command.Parameters["@ZeusIdModulo"].Value = Server.HtmlEncode(Request.QueryString["ZIM"]);

                SqlDataReader reader = command.ExecuteReader();

                int myReturn = -1;

                while (reader.Read())
                {
                    myReturn = reader.GetInt32(0);

                }//fine while

                reader.Close();
                return myReturn;
            }
            catch (Exception p)
            {

                Response.Write(p.ToString());
                return -1;
            }
        }//fine Using

    }//fine getLastOrder

    //FINE FUNZIONI PER ORDINAMENTO  


    //FUNZIONI PER LA GRIDVIEW CHE ULIZZANO L'ORDINAMENTO
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int index = Convert.ToInt32(e.CommandArgument);
        GridViewRow row = GridView1.Rows[index];
        Label ID1Menu = (Label)row.FindControl("ID1Menu");
        Label ID1MenuParent = (Label)row.FindControl("ID1MenuParent");
        Label Ordinamento = (Label)row.FindControl("Ordinamento");

        Move(e.CommandName.ToString(), Ordinamento.Text, ID1MenuParent.Text, ID1Menu.Text);
        GridView1.DataBind();


    }//fine GridView1_RowCommand

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {

            if (e.Row.DataItemIndex > -1)
            {

                Label Ordinamento = (Label)e.Row.FindControl("Ordinamento");
                Label ID1MenuParent = (Label)e.Row.FindControl("ID1MenuParent");
                int ordinamentoRow = Convert.ToInt32(Ordinamento.Text);

                int maxordine = getLastOrder(ID1MenuParent.Text);

                if (ordinamentoRow == 1)
                    e.Row.Cells[UP].Text = string.Empty;

                if (ordinamentoRow == maxordine)
                    e.Row.Cells[DOWN].Text = string.Empty;



                //immagini attivo
                Image Image1 = (Image)e.Row.FindControl("Image1");
                Label AttivoArea1 = (Label)e.Row.FindControl("AttivoArea1");

                int intAttivoArea1 = Convert.ToInt32(AttivoArea1.Text.ToString());

                if (intAttivoArea1 == 1)
                    Image1.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";


                Label ZeusUserEditableLabel = (Label)e.Row.FindControl("ZeusUserEditableLabel");
                e.Row.Cells[EDT].Enabled = Convert.ToBoolean(ZeusUserEditableLabel.Text);



                if (e.Row.Cells[UP].Controls.Count > 0)
                {
                    e.Row.Cells[UP].Attributes.Add("onmouseover", "showImageUP('" + e.Row.Cells[UP].Controls[0].ClientID + "','2');");
                    e.Row.Cells[UP].Attributes.Add("onmouseout", "showImageUP('" + e.Row.Cells[UP].Controls[0].ClientID + "','1');");
                }

                if (e.Row.Cells[DOWN].Controls.Count > 0)
                {
                    e.Row.Cells[DOWN].Attributes.Add("onmouseover", "showImageDOWN('" + e.Row.Cells[DOWN].Controls[0].ClientID + "','2');");
                    e.Row.Cells[DOWN].Attributes.Add("onmouseout", "showImageDOWN('" + e.Row.Cells[DOWN].Controls[0].ClientID + "','1');");
                }



            }//fine if
        }
        catch (Exception p)
        {

            Response.Write(p.ToString());
        }

    }//fine GridView1_RowDataBound





    protected bool DeleteSiteMenuRow(string ID1Menu)
    {

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "UPDATE tbSiteMenu SET ZeusIsAlive=0 WHERE ID1Menu=@ID1Menu";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID1Menu", System.Data.SqlDbType.Int);
                command.Parameters["@ID1Menu"].Value = ID1Menu;
                command.ExecuteNonQuery();

                return true;
            }
            catch (Exception p)
            {

                Response.Write(p.ToString());
                return false;
            }
        }//fine Using
    }//fine DeleteSiteMenuRow

    protected void UrlHyperLink_DataBinding(object sender, EventArgs e)
    {
        HyperLink UrlHyperLink = (HyperLink)sender;

        if (UrlHyperLink.NavigateUrl.Length == 0)
            UrlHyperLink.Visible = false;

    }//fine UrlLinkButton_DataBinding


    protected void RolesLabel_DataBinding(object sender, EventArgs e)
    {
        Label RolesLabel = (Label)sender;

        if (RolesLabel.Text.Length > 0)
        {
            RolesLabel.Text = RolesLabel.Text.Trim();
            RolesLabel.Text = RolesLabel.Text.Substring(0, RolesLabel.Text.Length - 1);
        }
        

    }//fine RolesLabel_DataBinding

    protected void EliminaButton_OnClick(object sender, EventArgs e)
    {
        LinkButton EliminaButton = (LinkButton)sender;

        DeleteSiteMenuRow(EliminaButton.ToolTip);
        EliminaButton.ToolTip = string.Empty;
        GridView1.DataBind();


    }//fine EliminaButton_OnClick
    
    
    
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">

    <script language="javascript" type="text/javascript">
            function showImageUP(imgId,typ) {
                var objImg = document.getElementById(imgId);
                if (objImg) {
                    if (typ == "1")
                        objImg.src = "../SiteImg/Bt3_ArrowUp_Off.gif";
                   else if (typ == "2")
                       objImg.src = "../SiteImg/Bt3_ArrowUp_On.gif";
               }
           }
           
           function showImageDOWN(imgId,typ) {
                var objImg = document.getElementById(imgId);
                if (objImg) {
                    if (typ == "1")
                        objImg.src = "../SiteImg/Bt3_ArrowDown_Off.gif";
                   else if (typ == "2")
                       objImg.src = "../SiteImg/Bt3_ArrowDown_On.gif";
               }
           }
    </script>

    <div class="LayGridTitolo1">
        <asp:HiddenField ID="TitleField" runat="server"  />
        <asp:HiddenField ID="ZIM" runat="server"  />
        
        <asp:Label ID="InfoLabel" runat="server" Text="Visualizza voci del menu " SkinID="GridFilter"></asp:Label>
        <asp:HyperLink ID="Livello1HyperLink" runat="server" Text="[Livello 1]" SkinID="GridFilter"></asp:HyperLink>
        <asp:HyperLink ID="Livello2HyperLink" runat="server" Text="[Livello 2]" SkinID="GridFilter"></asp:HyperLink>
        <asp:HyperLink ID="Livello3HyperLink" runat="server" Text="[Livello 3]" SkinID="GridFilter"></asp:HyperLink>
        <asp:HyperLink ID="TutteHyperLink" runat="server" Text="[Tutte]" SkinID="GridFilter"></asp:HyperLink>
    </div>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="dsMenu"
        OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand" CellPadding="0">
        <Columns>
            <asp:TemplateField HeaderText="AttivoArea1" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="AttivoArea1" runat="server" Text='<%# Eval("AttivoArea1") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ID1Menu" SortExpression="ID1Menu" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="ID1Menu" runat="server" Text='<%# Eval("ID1Menu") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ID2MenuParent" SortExpression="ID2MenuParent" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="ID1MenuParent" runat="server" Text='<%# Eval("ID2MenuParent") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Ordinamento" SortExpression="Ordinamento" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="Ordinamento" runat="server" Text='<%# Eval("Ordinamento") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="MenuLiv1" HeaderText="Menù Liv. 1" SortExpression="MenuLiv1" />
            <asp:BoundField DataField="MenuLiv2" HeaderText="Menù Liv. 2" SortExpression="MenuLiv2" />
            <asp:BoundField DataField="MenuLiv3" HeaderText="Menù Liv. 3" SortExpression="MenuLiv3" />
            <asp:BoundField DataField="Livello" HeaderText="Livello" SortExpression="Livello"
                ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
            <asp:ButtonField CommandName="Up" HeaderText="Ord." ShowHeader="True" ImageUrl="~/Zeus/SiteImg/Bt3_ArrowUp_Off.gif"
                ButtonType="Image" ControlStyle-Width="16" ControlStyle-Height="16" ControlStyle-BorderWidth="0">
                <ItemStyle HorizontalAlign="Center" />
                <ControlStyle CssClass="GridView_Ord1" />
            </asp:ButtonField>
            <asp:ButtonField CommandName="Down" HeaderText="Ord." ShowHeader="True" ButtonType="Image"
                ImageUrl="~/Zeus/SiteImg/Bt3_ArrowDown_Off.gif" ControlStyle-Width="16" ControlStyle-Height="16"
                ControlStyle-BorderWidth="0">
                <ItemStyle HorizontalAlign="Center" />
                <ControlStyle CssClass="GridView_Ord1" />
            </asp:ButtonField>
            <asp:TemplateField HeaderText="Pub.CNS" SortExpression="AttivoArea1">
                <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" /></ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Link">
                <ItemTemplate>
                    <asp:HyperLink ID="UrlHyperLink" runat="server" Text="Apri" CssClass="GridView_ZeusButton1" NavigateUrl='<%# Eval("Url")  %>' OnDataBinding="UrlHyperLink_DataBinding" Target="_blank"></asp:HyperLink>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Ruoli">
                <ItemTemplate>
                    <asp:Label ID="RolesLabel" runat="server" CssClass="LabelSkin_CorpoTesto" Text='<%# Eval("Roles")  %>' 
                     OnDataBinding="RolesLabel_DataBinding"/>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:HyperLinkField DataNavigateUrlFields="ID1Menu,ZeusIdModulo,ZeusLangCode,Livello"
                DataNavigateUrlFormatString="SiteMenu_Edt.aspx?XRI={0}&ZIM={1}&Lang={2}&XRI1={3}"
                HeaderText="Modifica" Text="Modifica" >
                <ControlStyle CssClass="GridView_ZeusButton1" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:HyperLinkField>
            <asp:TemplateField HeaderText="Elimina">
                <ItemTemplate>
                    <asp:LinkButton ID="EliminaButton" runat="server" OnClick="EliminaButton_OnClick" Text="Elimina"
                        ToolTip='<%# Eval("ID1Menu") %>' Enabled='<%# Eval("ZeusUserEditable") %>'></asp:LinkButton></ItemTemplate>
                <ControlStyle CssClass="GridView_ZeusButton1" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ZeusUserEditable" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="ZeusUserEditableLabel" runat="server" Text='<%# Eval("ZeusUserEditable") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="dsMenu" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SELECT     ID1Menu, Ordinamento, ID2MenuParent, Menu, ZeusIdModulo, ZeusUserEditable, ZeusLangCode, MenuLiv1Liv2Liv3,  
                      AttivoArea1, Livello, Url, Roles, MenuLiv1, MenuLiv2, MenuLiv3 
        FROM [vwSiteMenu_All] 
        WHERE [ZeusIdModulo] = @ZeusIdModulo AND [ZeusLangCode] = @ZeusLangCode ">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="0" Name="ZeusIdModulo" QueryStringField="ZIM"
                Type="String" />
            <asp:QueryStringParameter DefaultValue="0" Name="ZeusLangCode" QueryStringField="Lang"
                Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
