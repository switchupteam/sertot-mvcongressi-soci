﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per SiteMenu_New
/// </summary>
public partial class SiteMenu_New : System.Web.UI.Page
{      
    static string TitoloPagina = "Nuovo menù di navigazione";

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            TitleField.Value = TitoloPagina;
            delinea myDelinea = new delinea();

            if (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM"))
                Response.Redirect("~/Zeus/System/Message.aspx?0");

            ZIM.Value = Server.HtmlEncode(Request.QueryString["ZIM"]);

            if (!ReadXML(ZIM.Value, GetLang()))
                Response.Redirect("~/Zeus/System/Message.aspx?1");

            ReadXML_Localization(ZIM.Value, GetLang());

            if (!Page.IsPostBack)
            {
                CheckBoxList Roles_List = (CheckBoxList)FormView1.FindControl("Roles_List");
                bool find = false;
                string[] myAllZeusAdminRoles = GetAllZeusAdminRoles();
                string[] myAllRoles = Roles.GetAllRoles();
                ArrayList AllowedRoles = new ArrayList();

                for (int i = 0; i < myAllRoles.Length; ++i)
                {
                    for (int j = 0; j < myAllZeusAdminRoles.Length; ++j)
                    {
                        if (myAllZeusAdminRoles[j].Equals(myAllRoles[i]))
                            find = true;
                    }

                    if (!find)
                        AllowedRoles.Add(myAllRoles[i]);

                    find = false;
                }

                Roles_List.DataSource = (string[])AllowedRoles.ToArray(typeof(string));
                Roles_List.DataBind();
            }

            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");
            InsertButton.Attributes.Add("onclick", "Validate()");
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    private string[] GetAllZeusAdminRoles()
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;
        string[] AllZeusAdminRoles = null;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {
            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT [RoleName]";
                sqlQuery += " FROM [tbZeusRoles]";
                sqlQuery += " WHERE [IsAdminRole]=1";
                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();
                ArrayList myArray = new ArrayList();

                while (reader.Read())
                    if (reader["RoleName"] != DBNull.Value)
                        myArray.Add(reader["RoleName"].ToString());

                reader.Close();
                AllZeusAdminRoles = (string[])myArray.ToArray(typeof(string));
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
            }
        }

        return AllZeusAdminRoles;
    }

    private string GetLang()
    {
        string Lang = "ITA";
        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;
    }

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";
            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_SiteMenu.xml"));

            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            if (childNode.Name.Equals("ZML_TitoloPagina_New"))
                            {
                                TitleField.Value = childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_SiteMenu.xml"));

            if (!Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_PermitNew").InnerText))
                return false;

            Panel ZMCF_Ruoli = (Panel)FormView1.FindControl("ZMCF_Ruoli");

            if (ZMCF_Ruoli != null)
                ZMCF_Ruoli.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Ruoli").InnerText);

            Panel ZMCF_TargetWindow = (Panel)FormView1.FindControl("ZMCF_TargetWindow");

            if (ZMCF_TargetWindow != null)
                ZMCF_TargetWindow.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_TargetWindow").InnerText);

            if (!Page.IsPostBack)
                FillMenuParent(ZeusIdModulo, ZeusLangCode,
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_PermitLevel1").InnerText),
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_PermitLevel2").InnerText),
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_PermitLevel3").InnerText));

            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }

    protected void DataLimite(object sender, EventArgs e)
    {
        TextBox DataLimite = (TextBox)sender;
        if (DataLimite.Text.Length == 0)
        {
            DataLimite.Text = "31/12/2040";
        }
    }

    protected void DataIniziale(object sender, EventArgs e)
    {
        DateTime Data = new DateTime();
        Data = DateTime.Now;
        TextBox DataCreazione = (TextBox)sender;
        if (DataCreazione.Text.Length == 0)
        {
            DataCreazione.Text = Data.ToString("d");
        }
    }

    protected void UtenteCreazione(object sender, EventArgs e)
    {
        HiddenField UtenteCreazione = (HiddenField)sender;
        UtenteCreazione.Value = Membership.GetUser().ProviderUserKey.ToString();
    }

    protected void DataOggi(object sender, EventArgs e)
    {
        HiddenField DataCreazione = (HiddenField)sender;
        DataCreazione.Value = DateTime.Now.ToString();
    }

    protected void ZeusIdHidden_Load(object sender, EventArgs e)
    {
        HiddenField Guid = (HiddenField)sender;

        if (Guid.Value.Length == 0)
            Guid.Value = System.Guid.NewGuid().ToString();
    }

    protected void MenuParent_SaveValue(object sender, EventArgs e)
    {
        DropDownList MenuParent = (DropDownList)sender;
        HiddenField ID2MenuParentHiddenField = (HiddenField)FormView1.FindControl("ID2MenuParentHiddenField");
        ID2MenuParentHiddenField.Value = MenuParent.SelectedValue;
        RequiredFieldValidator RequiredFieldValidator1 = (RequiredFieldValidator)FormView1.FindControl("RequiredFieldValidator1");
        if (ID2MenuParentHiddenField.Value.Length == 0)
            RequiredFieldValidator1.IsValid = false;
        else
            RequiredFieldValidator1.IsValid = true;
    }

    private bool FillMenuParent(string ZeusIdModulo, string ZeusLangCode, bool PermitLevel1, bool PermitLevel2, bool PermitLevel3)
    {
        try
        {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
            DropDownList MenuParent = (DropDownList)FormView1.FindControl("MenuParent");
            HiddenField ID2MenuParentHiddenField = (HiddenField)FormView1.FindControl("ID2MenuParentHiddenField");

            string sqlQuery = string.Empty;

            if ((PermitLevel2) && (PermitLevel3))
                sqlQuery = @"SELECT ID1Menu
                                ,MenuLabel1 
                            FROM vwSiteMenu_All 
                            WHERE ZeusIdModulo=@ZeusIdModulo 
                                AND ZeusLangCode=@ZeusLangCode 
                                AND Livello <> 3 
                            ORDER BY MenuLabel1";

            else if (PermitLevel2)
                sqlQuery = @"SELECT ID1Menu
                                ,MenuLabel1 
                            FROM vwSiteMenu_All 
                            WHERE ZeusIdModulo=@ZeusIdModulo 
                                AND ZeusLangCode=@ZeusLangCode 
                                AND Livello = 1 
                            ORDER BY MenuLabel1";

            else if (PermitLevel3)
                sqlQuery = @"SELECT ID1Menu
                                ,MenuLabel1 
                            FROM vwSiteMenu_All 
                            WHERE ZeusIdModulo=@ZeusIdModulo 
                                AND ZeusLangCode=@ZeusLangCode 
                                AND Livello = 2 
                            ORDER BY MenuLabel1";

            using (SqlConnection connection = new SqlConnection(strConnessione))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ZeusIdModulo", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ZeusIdModulo"].Value = ZeusIdModulo;
                command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ZeusLangCode"].Value = ZeusLangCode;

                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = command;
                dAdapter.Fill(objDs);

               
               // --------------Alla creazione della prima voce di un menù la dropdown MenuParent era vuota, l'IF rows.Count=0 faceva sì che non scrivesse------------------------

                        //if (objDs.Tables[0].Rows.Count > 0)
                        //{
                        
                            MenuParent.ClearSelection();
                            MenuParent.Items.Clear();
                            MenuParent.DataSource = objDs.Tables[0];
                            MenuParent.DataValueField = "ID1Menu";
                            MenuParent.DataTextField = "MenuLabel1";
                            MenuParent.DataBind();
                            MenuParent.Enabled = true;

                            MenuParent.Items.Insert(0, new ListItem("> Seleziona", ""));
                            if (PermitLevel1)
                            {
                                
                                MenuParent.Items.Insert(1, new ListItem("Radice", "0"));
                               
                            }
                            ID2MenuParentHiddenField.Value = "0";


                            if ((objDs.Tables[0].Rows.Count == 0) && (!PermitLevel1))
                            {
                                MenuParent.Enabled = false;
                            }
                            
                                
                       // }
                        
                //------------------ se è la prima voce di quel menù
                        //if (objDs.Tables[0].Rows.Count == 0)
                        //{
                        //    MenuParent.ClearSelection();
                        //    MenuParent.Items.Clear();
                        //    MenuParent.DataSource = objDs.Tables[0];
                        //    MenuParent.DataValueField = "ID1Menu";
                        //    MenuParent.DataTextField = "MenuLabel1";
                        //    MenuParent.DataBind();
                        //    MenuParent.Enabled = true;

                        //    if (PermitLevel1)
                        //    {
                        //        MenuParent.Items.Insert(0, new ListItem("> Seleziona", ""));
                        //        MenuParent.Items.Insert(1, new ListItem("Radice", "0"));
                        //        ID2MenuParentHiddenField.Value = "0";
                        //    }
                        //    else
                        //        MenuParent.Enabled = false;
                        //}
//-------------------------------------



                return true;
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    private int getOrdinamento(string ID2MenuParent)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {
            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT TOP 1 Ordinamento";
                sqlQuery += " FROM tbSiteMenu ";
                sqlQuery += "WHERE ID2MenuParent=@ID2MenuParent AND ZeusIdmodulo=@ZeusIdModulo AND ZeusIsAlive = 1 ORDER BY Ordinamento DESC";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID2MenuParent", System.Data.SqlDbType.Int);
                command.Parameters["@ID2MenuParent"].Value = ID2MenuParent;
                command.Parameters.Add("@ZeusIdModulo", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ZeusIdModulo"].Value = ZIM.Value;

                int myCount = Convert.ToInt32(command.ExecuteScalar()) + 1;
                ++myCount;

                if (myCount != null)
                    return myCount;
                else return 1;
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
                return -1;
            }
        }
    }

    protected void InsertButton_Click(object sender, EventArgs e)
    {
        try
        {
            CheckBoxList Roles_List = (CheckBoxList)FormView1.FindControl("Roles_List");
            HiddenField RolesHiddenField = (HiddenField)FormView1.FindControl("RolesHiddenField");
            HiddenField FileNameHiddenField = (HiddenField)FormView1.FindControl("FileNameHiddenField");
            HiddenField OrdinamentoHiddenField = (HiddenField)FormView1.FindControl("OrdinamentoHiddenField");
            DropDownList MenuParent = (DropDownList)FormView1.FindControl("MenuParent");
            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");

            if ((Page.IsValid) && (InsertButton.CommandName != string.Empty))
            {
                for (int i = 0; i < Roles_List.Items.Count; ++i)
                    if (Roles_List.Items[i].Selected)
                    {
                        RolesHiddenField.Value += Roles_List.Items[i].Text.ToString();
                        RolesHiddenField.Value += ", ";
                    }

                if (MenuParent.Enabled)
                    OrdinamentoHiddenField.Value = getOrdinamento(MenuParent.SelectedValue).ToString();
                else OrdinamentoHiddenField.Value = "1";
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    protected void SiteMenuSqlDataSource_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.Exception == null)
        {
            Response.Redirect("~/Zeus/SiteMenu/SiteMenu_Lst.aspx?ZIM=" + Server.HtmlEncode(Request.QueryString["ZIM"]) 
                + "&Lang=" + Server.HtmlEncode(Request.QueryString["Lang"]));
        }
        else Response.Redirect("~/Zeus/System/Message.aspx?SqlErr");
    }   
}