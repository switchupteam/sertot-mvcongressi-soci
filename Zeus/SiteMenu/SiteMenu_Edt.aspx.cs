﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per SiteMenu_Edt
/// </summary>
public partial class SiteMenu_Edt : System.Web.UI.Page
{
    static string TitoloPagina = "Modifica menù di navigazione";

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            TitleField.Value = TitoloPagina;
            delinea myDelinea = new delinea();

            if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM"))
                || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI"))
                || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI1")))
                Response.Redirect("~/Zeus/System/Message.aspx?0");

            ZIM.Value = Server.HtmlEncode(Request.QueryString["ZIM"]);
            XRI.Value = Server.HtmlEncode(Request.QueryString["XRI"]);

            if (!ReadXML(ZIM.Value, GetLang()))
                Response.Redirect("~/Zeus/System/Message.aspx?1");

            ReadXML_Localization(ZIM.Value, GetLang());

            if (!Page.IsPostBack)
                FillMenuParent(ZIM.Value,
                    GetLang(),
                    Convert.ToInt32(Request.QueryString["XRI1"]));

            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");
            InsertButton.Attributes.Add("onclick", "Validate()");
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    private string GetLang()
    {
        string Lang = "ITA";
        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;
    }

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";
            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_SiteMenu.xml"));

            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {
                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {
                            if (childNode.Name.Equals("ZML_TitoloPagina_Edt"))
                            {
                                TitleField.Value = childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_SiteMenu.xml"));

            if (!Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_PermitEdt").InnerText))
                return false;

            Panel ZMCF_Ruoli = (Panel)FormView1.FindControl("ZMCF_Ruoli");

            if (ZMCF_Ruoli != null)
                ZMCF_Ruoli.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Ruoli").InnerText);

            Panel ZMCF_TargetWindow = (Panel)FormView1.FindControl("ZMCF_TargetWindow");

            if (ZMCF_TargetWindow != null)
                ZMCF_TargetWindow.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_TargetWindow").InnerText);

            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }

    protected void DataLimite(object sender, EventArgs e)
    {
        TextBox DataLimite = (TextBox)sender;
        if (DataLimite.Text.Length == 0)
        {
            DataLimite.Text = "31/12/2040";
        }
    }

    protected void DataIniziale(object sender, EventArgs e)
    {
        DateTime Data = new DateTime();
        Data = DateTime.Now;
        TextBox DataCreazione = (TextBox)sender;
        if (DataCreazione.Text.Length == 0)
        {
            DataCreazione.Text = Data.ToString("d");
        }
    }

    protected void UtenteCreazione(object sender, EventArgs e)
    {
        HiddenField UtenteCreazione = (HiddenField)sender;
        UtenteCreazione.Value = Membership.GetUser().ProviderUserKey.ToString();
    }

    protected void DataOggi(object sender, EventArgs e)
    {
        HiddenField DataCreazione = (HiddenField)sender;
        DataCreazione.Value = DateTime.Now.ToString();
    }

    protected void HiddenImageFile_DataBinding(object sender, EventArgs e)
    {
        HiddenField HiddenImageFile = (HiddenField)sender;
        if (HiddenImageFile.Value.Length == 0)
            HiddenImageFile.Value = "ImgNonDisponibile.jpg";
    }

    protected void MenuParent_SaveValue(object sender, EventArgs e)
    {
        DropDownList MenuParent = (DropDownList)sender;
        HiddenField ID2MenuParentHiddenField = (HiddenField)FormView1.FindControl("ID2MenuParentHiddenField");

        ID2MenuParentHiddenField.Value = MenuParent.SelectedValue;
    }

    protected void ZeusUserEditableHiddenField_DataBinding(object sender, EventArgs e)
    {
        HiddenField ZeusUserEditableHiddenField = (HiddenField)sender;

        if (!Convert.ToBoolean(ZeusUserEditableHiddenField.Value))
            Response.Redirect("~/Zeus/System/Message.aspx?ZeusUserEditable");
    }

    protected void RolesHiddenField_DataBinding(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CheckBoxList Roles_List = (CheckBoxList)FormView1.FindControl("Roles_List");
            HiddenField RolesHiddenField = (HiddenField)sender;

            bool find = false;
            string[] myAllZeusAdminRoles = GetAllZeusAdminRoles();
            string[] myAllRoles = Roles.GetAllRoles();
            ArrayList AllowedRoles = new ArrayList();

            for (int i = 0; i < myAllRoles.Length; ++i)
            {
                for (int j = 0; j < myAllZeusAdminRoles.Length; ++j)
                {
                    if (myAllZeusAdminRoles[j].Equals(myAllRoles[i]))
                        find = true;
                }

                if (!find)
                    AllowedRoles.Add(myAllRoles[i]);

                find = false;
            }

            Roles_List.DataSource = (string[])AllowedRoles.ToArray(typeof(string));
            Roles_List.DataBind();

            if (RolesHiddenField.Value.Length == 0)
                return;

            char[] mySeparator = { ',' };

            RolesHiddenField.Value = RolesHiddenField.Value.Remove(RolesHiddenField.Value.Length - 2);
            string[] myRole = RolesHiddenField.Value.Split(mySeparator);

            for (int i = 0; i < myRole.Length; ++i)
                for (int j = 0; j < Roles_List.Items.Count; ++j)
                    if (myRole[i].ToLower().Trim().Equals(Roles_List.Items[j].Text.ToLower().Trim()))
                        Roles_List.Items[j].Selected = true;
        }
    }

    private string[] GetAllZeusAdminRoles()
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;
        string[] AllZeusAdminRoles = null;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {
            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT [RoleName]";
                sqlQuery += " FROM [tbZeusRoles]";
                sqlQuery += " WHERE [IsAdminRole]=1";
                command.CommandText = sqlQuery;

                SqlDataReader reader = command.ExecuteReader();
                ArrayList myArray = new ArrayList();

                while (reader.Read())
                    if (reader["RoleName"] != DBNull.Value)
                        myArray.Add(reader["RoleName"].ToString());

                reader.Close();

                AllZeusAdminRoles = (string[])myArray.ToArray(typeof(string));
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
            }
        }

        return AllZeusAdminRoles;
    }

    private bool FillMenuParent(string ZeusIdModulo, string ZeusLangCode, int Level)
    {
        try
        {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
            DropDownList MenuParent = (DropDownList)FormView1.FindControl("MenuParent");
            HiddenField ID2MenuParentHiddenField = (HiddenField)FormView1.FindControl("ID2MenuParentHiddenField");
            string sqlQuery = string.Empty;

            switch (Level)
            {
                case 1:
                    MenuParent.Enabled = false;
                    MenuParent.Items.Add("Radice");
                    ID2MenuParentHiddenField.Value = "0";
                    return true;
                    break;

                case 2:
                    sqlQuery = @"SELECT ID1Menu
                                    ,MenuLabel1 
                                FROM vwSiteMenu_All 
                                WHERE ZeusIdModulo=@ZeusIdModulo 
                                    AND ZeusLangCode=@ZeusLangCode 
                                    AND Livello = 1 
                                ORDER BY MenuLabel1";
                    break;

                case 3:
                    sqlQuery = @"SELECT ID1Menu                    
                                    ,MenuLabel1 
                                FROM vwSiteMenu_All
                                WHERE ZeusIdModulo=@ZeusIdModulo 
                                    AND ZeusLangCode=@ZeusLangCode 
                                    AND Livello = 2 
                                ORDER BY MenuLabel1";
                    break;
            }

            using (SqlConnection connection = new SqlConnection(strConnessione))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ZeusIdModulo", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ZeusIdModulo"].Value = ZeusIdModulo;
                command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.NVarChar);
                command.Parameters["@ZeusLangCode"].Value = ZeusLangCode;

                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = command;
                dAdapter.Fill(objDs);

                if (objDs.Tables[0].Rows.Count > 0)
                {
                    MenuParent.ClearSelection();
                    MenuParent.Items.Clear();
                    MenuParent.DataSource = objDs.Tables[0];
                    MenuParent.DataValueField = "ID1Menu";
                    MenuParent.DataTextField = "MenuLabel1";
                    MenuParent.DataBind();
                    MenuParent.Enabled = true;
                }
                else 
                    MenuParent.Enabled = false;

                HiddenField ID2MenuParentOldHiddenField = (HiddenField)FormView1.FindControl("ID2MenuParentOldHiddenField");
                MenuParent.SelectedIndex = MenuParent.Items.IndexOf(MenuParent.Items.FindByValue(ID2MenuParentOldHiddenField.Value));

                return true;
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }

    private int getLastOrder(string ID2MenuParent)
    {
        if (ID2MenuParent.Equals("0"))
            return 0;

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {
            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = @"SELECT MAX(Ordinamento) 
                            FROM tbSiteMenu 
                            WHERE ID2MenuParent=@ID2MenuParent 
                                AND ZeusIsAlive=1 
                                AND ZeusLangCode=@ZeusLangCode  
                                AND ZeusIdModulo=@ZeusIdModulo";

                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID2MenuParent", System.Data.SqlDbType.Int, 4);
                command.Parameters["@ID2MenuParent"].Value = ID2MenuParent;

                command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.NVarChar, 3);
                command.Parameters["@ZeusLangCode"].Value = Server.HtmlEncode(Request.QueryString["Lang"]);

                command.Parameters.Add("@ZeusIdModulo", System.Data.SqlDbType.NVarChar, 5);
                command.Parameters["@ZeusIdModulo"].Value = Server.HtmlEncode(Request.QueryString["ZIM"]);

                SqlDataReader reader = command.ExecuteReader();
                int myReturn = 0;

                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                        myReturn = reader.GetInt32(0);
                }

                reader.Close();
                return myReturn;
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
                return -1;
            }
        }
    }

    private bool ReOrder(string ID2MenuParent)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {
            SqlTransaction transaction = null;

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                transaction = connection.BeginTransaction();
                command.Transaction = transaction;

                sqlQuery = "SELECT ID1Menu,Ordinamento FROM [vwSiteMenu_All] WHERE ID2MenuParent=@ID2MenuParent ORDER BY Ordinamento";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID2MenuParent", System.Data.SqlDbType.Int);
                command.Parameters["@ID2MenuParent"].Value = ID2MenuParent;

                SqlDataReader reader = command.ExecuteReader();
                int index = 1;
                ArrayList myArrayListSiteMenu = new ArrayList();

                while (reader.Read())
                {
                    if (reader.GetInt32(1) != index)
                    {
                        string[] myData = new string[3];

                        myData[0] = reader.GetInt32(0).ToString();
                        myData[1] = index.ToString();
                        myArrayListSiteMenu.Add(myData);
                    }
                    ++index;
                }

                reader.Close();

                string[][] myCategoryToChange = (string[][])myArrayListSiteMenu.ToArray(typeof(string[]));

                for (int i = 0; i < myCategoryToChange.Length; ++i)
                {
                    sqlQuery = "UPDATE tbSiteMenu SET Ordinamento=@Ordinamento WHERE ID1Menu=@ID1Menu";
                    command.CommandText = sqlQuery;
                    command.Parameters.Clear();

                    command.Parameters.Add("@Ordinamento", System.Data.SqlDbType.NVarChar, 4);
                    command.Parameters["@Ordinamento"].Value = myCategoryToChange[i][1];

                    command.Parameters.Add("@ID1Menu", System.Data.SqlDbType.Int, 4);
                    command.Parameters["@ID1Menu"].Value = myCategoryToChange[i][0];

                    command.ExecuteNonQuery();
                }

                transaction.Commit();
                return true;
            }
            catch (Exception p)
            {
                transaction.Rollback();
                Response.Write(p.ToString());
                return false;
            }
        }
    }

    protected void InsertButton_Click(object sender, EventArgs e)
    {
        try
        {
            CheckBoxList Roles_List = (CheckBoxList)FormView1.FindControl("Roles_List");
            HiddenField RolesHiddenField = (HiddenField)FormView1.FindControl("RolesHiddenField");
            HiddenField FileNameHiddenField = (HiddenField)FormView1.FindControl("FileNameHiddenField");
            HiddenField ID2MenuParentHiddenField = (HiddenField)FormView1.FindControl("ID2MenuParentHiddenField");
            HiddenField ID2MenuParentOldHiddenField = (HiddenField)FormView1.FindControl("ID2MenuParentOldHiddenField");
            DropDownList MenuParent = (DropDownList)FormView1.FindControl("MenuParent");
            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");

            if ((Page.IsValid) && (InsertButton.CommandName != string.Empty))
            {
                RolesHiddenField.Value = string.Empty;

                for (int i = 0; i < Roles_List.Items.Count; ++i)
                    if (Roles_List.Items[i].Selected)
                    {
                        RolesHiddenField.Value += Roles_List.Items[i].Text.ToString();
                        RolesHiddenField.Value += ", ";
                    }

                if (!ID2MenuParentHiddenField.Value.Equals(ID2MenuParentOldHiddenField.Value))
                {
                    HiddenField OrdinamentoHiddenField = (HiddenField)FormView1.FindControl("OrdinamentoHiddenField");

                    int Order = getLastOrder(ID2MenuParentHiddenField.Value);
                    ++Order;
                    OrdinamentoHiddenField.Value = Order.ToString();
                }
            }
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }

    protected void SiteMenuSqlDataSource_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.Exception == null)
        {
            HiddenField ID2MenuParentOldHiddenField = (HiddenField)FormView1.FindControl("ID2MenuParentOldHiddenField");
            HiddenField ID2MenuParentHiddenField = (HiddenField)FormView1.FindControl("ID2MenuParentHiddenField");

            if (!ID2MenuParentHiddenField.Value.Equals(ID2MenuParentOldHiddenField.Value))
                ReOrder(ID2MenuParentOldHiddenField.Value);

            Response.Redirect("~/Zeus/SiteMenu/SiteMenu_Lst.aspx?ZIM="
                + ZIM.Value
                + "&Lang=" + GetLang());
        }
        else Response.Redirect("~/Zeus/System/Message.aspx?SqlErr");
    }
}