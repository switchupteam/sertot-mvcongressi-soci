<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="Content_Dtl.aspx.cs"
    Inherits="PageDesigner_Content_Dtl"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register Src="PageDesignerLangButton.ascx" TagName="PageDesignerLangButton" TagPrefix="uc1" %>
<%@ Register Src="../SiteAssets/PhotoGallery_Associa.ascx" TagName="PhotoGallery_Associa"
    TagPrefix="uc2" %>

<%@ Register Src="~/Zeus/PhotoGallery_Simple/Photo_Lst.ascx" TagName="Photo_Lst"
    TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" />
    <asp:HiddenField ID="XRI" runat="server" />
    <asp:HiddenField ID="ZIM" runat="server" />
    <asp:HiddenField ID="ZID" runat="server" />
    <%--Per Gestione TABS--%>
    <asp:HiddenField ID="hdnSelectedTab" runat="server" Value="0" />
    <link href="../../asset/css/ZeusTypeFoundry.css" rel="stylesheet" type="text/css" />
    <link href="../../SiteCss/ZeusSnippets.css" rel="stylesheet" type="text/css" />
    <asp:FormView ID="FormView1" runat="server" DataKeyNames="ID1Pagina" DataSourceID="dsDesignerNew"
        DefaultMode="ReadOnly" Width="100%">
        <ItemTemplate>
            <div id="tabs">

                <ul>
                    <li><a href="#Principale">Principale</a></li>
                    <li><a href="#Immagini">Immagini</a></li>
                    <li><a href="#Contenuti">Contenuti</a></li>
                    <li><a href="#PhotoGallery">Photo Gallery</a></li>

                </ul>
                <div id="Principale">
                    <div class="BlockBox">
                        <div class="BlockBoxHeader">
                            <asp:Label ID="IdentificativoLabel" runat="server">Identificativo pagina</asp:Label>
                        </div>
                        <table>
                            <asp:Panel ID="ZMCF_TitoloPagina" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="Label1" runat="server" SkinID="FieldDescription" Text="Label">Titolo pagina *</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:Label ID="TitoloPaginaTextBox" SkinID="FieldValue" runat="server" Text='<%# Eval("TitoloPagina") %>'></asp:Label>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="ZMCF_Sottotitolo" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="Label9" runat="server" SkinID="FieldDescription" Text="Label">Sottotitolo</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:Label ID="SottotitoloTextBox" runat="server" SkinID="FieldValue" Text='<%# Eval("Sottotitolo", "{0}") %>'></asp:Label>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="ZMCF_ParentPage" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="Label10" runat="server" SkinID="FieldDescription" Text="Label">Pagina padre</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:Label ID="PaginaPadreLabel" SkinID="FieldValue" runat="server"></asp:Label>
                                        <asp:DropDownList ID="PaginaPadreDropDownList" runat="server" DataSourceID="dsTipologiaCategorie"
                                            Visible="false" DataTextField="MenuLabel1" DataValueField="ID1Pagina" SelectedValue='<%# Eval("ID2ParentPage") %>'
                                            AppendDataBoundItems="True" OnDataBound="PaginaPadreDropDownList_DataBound" AutoPostBack="true">
                                            <asp:ListItem Value="0">&gt; Pagina singola / home di sezione</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:SqlDataSource ID="dsTipologiaCategorie" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                            SelectCommand="SELECT [ID1Pagina], [MenuLabel1] FROM [vwPageDesigner_ParentPages_Zeus1] WHERE ([ZeusIdModulo] = @ZeusIdModulo)  ORDER BY [MenuLabel1]">
                                            <SelectParameters>
                                                <asp:QueryStringParameter DefaultValue="0" Name="ZeusIdModulo" QueryStringField="ZIM"
                                                    Type="String" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="ZMCF_Categoria" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="Label11" runat="server" SkinID="FieldDescription" Text="Label">Categoria *</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:Label ID="ID2CategoriaLabel" SkinID="FieldValue" runat="server"></asp:Label>
                                        <asp:DropDownList ID="ID2CategoriaDropDownList" runat="server" Visible="false" AppendDataBoundItems="True">
                                        </asp:DropDownList>
                                        <asp:HiddenField ID="ID2CategoriaHiddenField" runat="server" Value='<%# Eval("ID2Categoria") %>' />
                                        <asp:SqlDataSource ID="dsCategoria" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"></asp:SqlDataSource>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="ZMCF_ZeusJollyText" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_ZeusJollyText" runat="server" SkinID="FieldDescription" Text="Label">Sottotitolo</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:Label ID="ZeusJollyTextTextBox" runat="server" SkinID="FieldValue" Text='<%# Eval("ZeusJollyText", "{0}") %>'></asp:Label>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="PZV_DescrizioneZATPanel" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="PZL_DescrizioneZATLabel" runat="server" SkinID="FieldDescription"
                                            Text="Label">ZeusAssetType</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:Label ID="ZeusAssetTypeTextBox" runat="server" Text='<%# Eval("ZeusAssetType", "{0}") %>'
                                            SkinID="FieldValue"></asp:Label>
                                    </td>
                                </tr>
                            </asp:Panel>
                        </table>
                    </div>
                    <asp:Panel ID="ZMCF_MenuSezione" runat="server" Visible="false">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="Label16" runat="server">Men� di navigazione</asp:Label>
                            </div>
                            <table>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="VoceLabel" runat="server" SkinID="FieldDescription" Text="Label">Voce per men� di sezione </asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:Label ID="SezioneTextBox" runat="server" SkinID="FieldValue" Text='<%# Eval("MenuSezione", "{0}") %>'></asp:Label>
                                </tr>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="Label17" runat="server" SkinID="FieldDescription" Text="Label">Pubblica men� </asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:CheckBox ID="PW_MenuSezioneCheckBox" runat="server" Checked='<%# Eval("PW_MenuSezione") %>'
                                            Enabled="false" />
                                        <asp:Label ID="PosizioneLabel" runat="server" SkinID="FieldValue" Text="Label">in posizione </asp:Label>
                                        <asp:Label ID="PosizioneTextBox" runat="server" SkinID="FieldValue" Text='<%# Eval("Ordinamento1", "{0}") %>'></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_BoxSEO" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="TagPaginaLabel" runat="server">SEO Search Engine Optimization</asp:Label>
                            </div>
                            <table>
                                <asp:Panel ID="ZMCF_TitoloBrowser" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="Label8" runat="server" SkinID="FieldDescription" Text="Label">TITLE / Titolo Browser</asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:Label ID="TitoloBrowserTextBox" SkinID="FieldValue" runat="server" Text='<%# Eval("TitoloBrowser") %>'></asp:Label>
                                        </td>
                                    </tr>
                                </asp:Panel>

                                <asp:Panel ID="ZMCF_TagPagina" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="Label13" runat="server" SkinID="FieldDescription" Text="Label">Description</asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:Label ID="TagDescriptionTextBox" SkinID="FieldValue" runat="server" Text='<%# Eval("TagDescription") %>'></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="Label14" runat="server" SkinID="FieldDescription" Text="Label">Keywords</asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:Label ID="TagKeywordsTextBox" SkinID="FieldValue" runat="server" Text='<%# Eval("TagKeywords") %>'></asp:Label>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="ZMCF_TagMeta1" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="ZML_TagMeta1" runat="server" SkinID="FieldDescription" Text="Label">Description</asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:Label ID="Label41" runat="server" Text='<%# Bind("TagMeta1Attribute","Attribute: {0} ") %>'
                                                SkinID="FieldValue"></asp:Label>
                                            <img src="../SiteImg/Spc.gif" width="10" />
                                            <asp:Label ID="TagMeta1ValueLabel" runat="server" Text='<%# Bind("TagMeta1Value","Value: {0} ") %>'
                                                SkinID="FieldValue"></asp:Label>
                                            <img src="../SiteImg/Spc.gif" width="10" />
                                            <asp:Label ID="TagMeta1ContentLabel" runat="server" Text='<%# Bind("TagMeta1Content","Content: {0}") %>'
                                                SkinID="FieldValue"></asp:Label>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="ZMCF_TagMeta2" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="ZML_TagMeta2" runat="server" SkinID="FieldDescription" Text="Label">Description</asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:Label ID="Label40" runat="server" Text='<%# Bind("TagMeta2Attribute","Attribute: {0} ") %>'
                                                SkinID="FieldValue"></asp:Label>
                                            <img src="../SiteImg/Spc.gif" width="10" />
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("TagMeta2Value","Value: {0} ") %>'
                                                SkinID="FieldValue"></asp:Label>
                                            <img src="../SiteImg/Spc.gif" width="10" />
                                            <asp:Label ID="Label39" runat="server" Text='<%# Bind("TagMeta2Content","Content: {0}") %>'
                                                SkinID="FieldValue"></asp:Label>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="ZMCF_UrlRewrite" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="Label23" runat="server" Text="Url Rewrite" SkinID="FieldDescription"></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:Label ID="ZMCD_UrlDomain" runat="server" SkinID="FieldValue"></asp:Label><asp:Label ID="ZMCD_UrlSection" runat="server" SkinID="FieldValue"></asp:Label><asp:Label ID="ID1Pagina" runat="server" SkinID="FieldValue" Text='<%# Eval("ID1Pagina","{0}/") %>'></asp:Label><asp:Label ID="UrlPageDetailLabel" runat="server" Text='<%# Eval("UrlRewrite","{0}") %>' SkinID="FieldValue"></asp:Label>
                                            <asp:HiddenField ID="UrlRewriteHiddenField" runat="server" Value='<%# Eval("UrlRewrite","{0}") %>' />
                                            <asp:HiddenField ID="CopyHiddenField" runat="server" />

                                        </td>
                                        <%--                                <td>
                                    <input id="CopiaInput" value="Copia" type="button" onclick="ClipBoard(); return;"
                                        class="ZSSM_Button01_Button" />
                                </td>--%>
                                    </tr>
                                </asp:Panel>
                            </table>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_BoxPlanner" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="Label6" runat="server">Planner</asp:Label>
                            </div>
                            <table>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_PWArea1" SkinID="FieldDescription" runat="server"></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:CheckBox ID="PW_Area1CheckBox" runat="server" Checked='<%# Eval("PW_Area1") %>'
                                            Enabled="false" />
                                        <asp:Label ID="AttivaDa_Label" runat="server" Text="Attiva pubblicazione dalla data"
                                            SkinID="FieldValue"></asp:Label>
                                        <asp:Label ID="PWI_Area1TextBox" runat="server" Text='<%# Eval("PWI_Area1", "{0:d}") %>'
                                            SkinID="FieldValue"></asp:Label>
                                        <asp:Label ID="AttivaA_Label" runat="server" Text="alla data" SkinID="FieldValue"></asp:Label>
                                        <asp:Label ID="PWF_Area1TextBox" runat="server" Text='<%# Eval("PWF_Area1", "{0:d}") %>'
                                            SkinID="FieldValue"></asp:Label>
                                    </td>
                                </tr>
                                <asp:Panel ID="ZMCF_Area2" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="ZML_PWArea2" SkinID="FieldDescription" runat="server"></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:CheckBox ID="CheckBox2" runat="server" Checked='<%# Eval("PW_Area2") %>' Enabled="false" />
                                            <asp:Label ID="AttivaDa2_Label" runat="server" SkinID="FieldValue" Text="Attiva pubblicazione dalla data"></asp:Label>
                                            <asp:Label ID="PWI_Area2TextBox" runat="server" Text='<%# Eval("PWI_Area2", "{0:d}") %>'
                                                SkinID="FieldValue"></asp:Label>
                                            <asp:Label ID="AttivaA2_Label" runat="server" SkinID="FieldValue" Text="alla data"></asp:Label>
                                            <asp:Label ID="PWF_Area2TextBox" runat="server" Text='<%# Eval("PWF_Area2", "{0:d}") %>'
                                                SkinID="FieldValue"></asp:Label>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="ArchivioPanel" runat="server" Visible="false">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="Label7" runat="server" SkinID="FieldDescription" Text="Label">Archivia</asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:CheckBox ID="ArchivioCheckBox" runat="server" Checked='<%# Eval("Archivia") %>' />
                                            <asp:Label ID="Label12" runat="server" SkinID="FieldValue" Text="Label">Se selezionato sposta il progetto in archivio.</asp:Label>
                                        </td>
                                    </tr>
                                </asp:Panel>
                            </table>
                        </div>
                    </asp:Panel>
                </div>
                <div id="Immagini">

                    <asp:Panel ID="ZMCF_Immagine1" runat="server">
                        <dlc:ImageRaider ID="ImageRaider1" runat="server" ZeusIdModuloIndice="1" ZeusLangCode="ITA"
                            BindPzFromDB="true" IsEditMode="true" DefaultEditBetaImage='<%# Eval("Immagine1") %>'
                            Enabled="false" DefaultEditGammaImage='<%# Eval("Immagine2") %>'
                            ImageAlt='<%# Eval("Immagine12Alt") %>'
                            OnDataBinding="ImageRaider_DataBinding" />
                        <asp:HiddenField ID="FileNameBetaHiddenField" runat="server" Value='<%# Bind("Immagine1") %>' />
                        <asp:HiddenField ID="FileNameGammaHiddenField" runat="server" Value='<%# Bind("Immagine2") %>' />
                        <asp:HiddenField ID="Immagine12AltHiddenField" runat="server" Value='<%# Bind("Immagine12Alt") %>' />
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_Immagine2" runat="server">

                        <dlc:ImageRaider ID="ImageRaider2" runat="server" ZeusIdModuloIndice="2" ZeusLangCode="ITA"
                            BindPzFromDB="true" IsEditMode="true" DefaultEditBetaImage='<%# Eval("Immagine3") %>'
                            Enabled="false" DefaultEditGammaImage='<%# Eval("Immagine4") %>'
                            OnDataBinding="ImageRaiderCrop_DataBinding" />

                        <asp:HiddenField ID="FileNameBeta2HiddenField" runat="server" Value='<%# Bind("Immagine3") %>' />
                        <asp:HiddenField ID="FileNameGamma2HiddenField" runat="server" Value='<%# Bind("Immagine4") %>' />
                        <asp:HiddenField ID="Image34AltHiddenField" runat="server" Value='<%# Bind("Immagine34Alt") %>' />

                    </asp:Panel>
                </div>
                <div id="Contenuti">

                    <asp:Panel ID="ZMCF_DescBreve1" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="Label2" runat="server">Descrizione</asp:Label>
                            </div>
                            <table>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="Label15" runat="server" SkinID="FieldDescription" Text="Label">Descrizione *</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:Label ID="DexcrizioneTextArea" runat="server" Text='<%# Eval("DescBreve1") %>'
                                            SkinID="FieldValue"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                    <div class="BlockBox">
                        <div class="BlockBoxHeader">
                            <asp:Label ID="Label3" runat="server">Contenuto</asp:Label>
                        </div>
                        <table>
                            <asp:Panel ID="ZMCF_Contenuto1" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_Contenuto1" runat="server" SkinID="FieldDescription" Text="Label">Contenuto principale</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:Label ID="Contenuto1Label" runat="server" Text='<%# Eval("Contenuto1") %>'></asp:Label>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="ZMCF_Contenuto2" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_Contenuto2" runat="server" SkinID="FieldDescription" Text="Label">Contenuto secondario</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:Label ID="Label21" runat="server" Text='<%# Eval("Contenuto2") %>'></asp:Label>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="ZMCF_Contenuto3" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_Contenuto3" runat="server" SkinID="FieldDescription" Text="Label">Terzo Contenuto</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:Label ID="Label19" runat="server" Text='<%# Eval("Contenuto3") %>'></asp:Label>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="ZMCF_Contenuto4" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_Contenuto4" runat="server" SkinID="FieldDescription" Text="Label">Quarto Contenuto</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:Label ID="Label22" runat="server" Text='<%# Eval("Contenuto4") %>'></asp:Label>
                                    </td>
                                </tr>
                            </asp:Panel>
                        </table>
                    </div>



                </div>
                <div id="PhotoGallery">

                    <asp:Panel ID="ZMCF_BoxPhotoGallerySimple" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <a name="PHOTO">
                                    <asp:Label ID="Label56" runat="server" Text="Fotografie aggiuntive"></asp:Label></a>
                            </div>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td>
                                        <uc3:Photo_Lst ID="PhotoGallery_Simple1" runat="server" OnInit="PhotoGallery_Simple1_Init" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_BoxPhotoGallery" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="Label4" runat="server" Text="Gallerie fotografiche associate"></asp:Label>
                            </div>
                            <table>
                                <tr>
                                    <td>
                                        <uc2:PhotoGallery_Associa ID="PhotoGallery_Associa1" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>

                </div>

            </div>

            <asp:Panel ID="PageDesignerLangButtonPanel" runat="server">
                <%--<uc1:PageDesignerLangButton ID="PageDesignerLangButton1" runat="server" />--%>
                <uc1:PageDesignerLangButton ID="PageDesignerLangButton1" runat="server" ZeusId='<%# Eval("ZeusId") %>'
                    ZeusIdModulo='<%# Eval("ZeusIdModulo") %>' ZeusLangCode='<%# Eval("ZeusLangCode") %>' />
            </asp:Panel>
            <div align="center">
                <asp:LinkButton ID="ModificaButton" runat="server" SkinID="ZSSM_Button01" Text="Modifica"
                    ValidationGroup="myValidation" OnClick="ModificaLinkButton_Click"></asp:LinkButton>
                <img src="../SiteImg/Spc.gif" width="20" />
                <asp:LinkButton ID="DeleteLinkButton" runat="server" Text="Elimina" SkinID="ZSSM_Button01"
                    OnClick="DeleteLinkButton_Click"></asp:LinkButton>
            </div>
            <div class="VertSpacerBig">
            </div>
            <dlc:zeusdatatracking ID="Zeusdatatracking1" runat="server" />
            <asp:HiddenField ID="zdtRecordNewUser" runat="server" Value='<%# Eval("RecordNewUser") %>' />
            <asp:HiddenField ID="zdtRecordNewDate" runat="server" Value='<%# Eval("RecordNewDate") %>' />
            <asp:HiddenField ID="zdtRecordEdtUser" runat="server" Value='<%# Eval("RecordEdtUser") %>' />
            <asp:HiddenField ID="zdtRecordEdtDate" runat="server" Value='<%# Eval("RecordEdtDate") %>' />
        </ItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="dsDesignerNew" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SET DATEFORMAT dmy; SELECT   ID1Pagina ,TitoloPagina, TitoloBrowser, Sottotitolo,Archivia,
         ID2ParentPage, ID2Categoria, MenuSezione, Ordinamento1, TagDescription, 
         TagKeywords, Contenuto1,Contenuto2, Contenuto3, Contenuto4, Immagine1,Immagine2,Immagine12Alt, Immagine3,Immagine4,Immagine34Alt, 
         UrlRewrite,PW_Area1, PWI_Area1, PWF_Area1, PW_Area2, PWI_Area2, PWF_Area2,
         RecordNewUser, RecordNewDate, RecordEdtUser, RecordEdtDate,PW_Immagine1,DescBreve1 ,PW_MenuSezione ,ZeusAssetType,
         TagMeta1Value,TagMeta1Content,TagMeta2Value,TagMeta2Content,TagMeta1Attribute,TagMeta2Attribute,ZeusJollyText, 
         ZeusId, ZeusIdModulo, ZeusLangCode
         FROM tbPageDesigner 
         WHERE ID1Pagina=@ID1Pagina AND ZeusIsAlive=1 AND ZeusIdModulo=@ZeusIdModulo AND ZeusLangCode=@ZeusLangCode"
        OnSelected="dsDesignerNew_Selected">
        <SelectParameters>
            <asp:QueryStringParameter Name="ID1Pagina" QueryStringField="XRI" />
            <asp:QueryStringParameter Name="ZeusIdModulo" QueryStringField="ZIM" Type="String" />
            <asp:QueryStringParameter Name="ZeusLangCode" QueryStringField="Lang" Type="String"
                DefaultValue="ITA" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
