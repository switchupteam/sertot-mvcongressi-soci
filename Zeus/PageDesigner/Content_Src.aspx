﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="Content_Src.aspx.cs"
    Inherits="PageDesigner_Content_Src"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" />
    <asp:HiddenField ID="ZIM" runat="server"  />
    <div class="BlockBox">
        <div class="BlockBoxHeader">
            <asp:Label ID="Ricerca_Label" runat="server" Text="Ricerca"></asp:Label></div>
        <table>
            <tr>
                <td class="BlockBoxDescription">
                    <asp:Label ID="Titolo_Label" SkinID="FieldDescription" runat="server" Text="Titolo"></asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:TextBox ID="TitoloTextBox" runat="server" Columns="50" MaxLength="100"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="BlockBoxDescription">
                    <asp:Label ID="PZL_Categoria1Label" SkinID="FieldDescription" runat="server" Text="Categoria"></asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:DropDownList ID="ID2CategoriaDropDownList" runat="server" AppendDataBoundItems="True">
                        <asp:ListItem Text="> Tutte" Value="0" Selected="True"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="dsCategoria" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>">
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td class="BlockBoxDescription">
                    <asp:Label ID="Label2" SkinID="FieldDescription" runat="server" Text="Data creazione"></asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:Label ID="Label3" SkinID="FieldValue" runat="server" Text="Compresa tra"></asp:Label>
                    <asp:TextBox ID="NewDateFirstTextBox" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="NewDateFirstTextBox"
                         Display="Dynamic" ErrorMessage="*" SkinID="ZSSM_Validazione01" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$">Formato data richiesto: gg/mm/aaaa</asp:RegularExpressionValidator>
                    <asp:Label ID="Label4" SkinID="FieldValue" runat="server" Text="e"></asp:Label>
                    <asp:TextBox ID="NewDateLastTextBox" runat="server" Columns="10" MaxLength="10" OnLoad="PWF"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="NewDateLastTextBox"
                         Display="Dynamic" ErrorMessage="*" SkinID="ZSSM_Validazione01" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$">Formato data richiesto: gg/mm/aaaa</asp:RegularExpressionValidator>
                         <asp:CompareValidator ValidationGroup="myValidation" ID="CompareValidator2" runat="server"
                                    ControlToCompare="NewDateFirstTextBox" ControlToValidate="NewDateLastTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="La data iniziale deve essere precedente o uguale alla data finale"
                                    Operator="GreaterThanEqual" Type="Date"></asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td class="BlockBoxDescription">
                    <asp:Label ID="Label1" SkinID="FieldDescription" runat="server" Text="Data modifica"></asp:Label>
                </td>
                <td class="BlockBoxValue">
                    <asp:Label ID="Label43" SkinID="FieldValue" runat="server" Text="Compresa tra"></asp:Label>
                    <asp:TextBox ID="EdtDateFirstTextBox" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="EdtDateFirstTextBox"
                        Display="Dynamic" ErrorMessage="*" SkinID="ZSSM_Validazione01" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$">Formato data richiesto: gg/mm/aaaa</asp:RegularExpressionValidator>
                    <asp:Label ID="Label44" SkinID="FieldValue" runat="server" Text="e"></asp:Label>
                    <asp:TextBox ID="EdtDateLastTextBox" runat="server" Columns="10" MaxLength="10" OnLoad="PWF"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="EdtDateLastTextBox"
                         Display="Dynamic" ErrorMessage="*" SkinID="ZSSM_Validazione01" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$">Formato data richiesto: gg/mm/aaaa</asp:RegularExpressionValidator>
                           <asp:CompareValidator ValidationGroup="myValidation" ID="CompareValidator1" runat="server"
                                    ControlToCompare="EdtDateFirstTextBox" ControlToValidate="EdtDateLastTextBox" SkinID="ZSSM_Validazione01"
                                    Display="Dynamic" ErrorMessage="La data iniziale deve essere precedente o uguale alla data finale"
                                    Operator="GreaterThanEqual" Type="Date"></asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td class="BlockBoxDescription">
                </td>
                <td class="BlockBoxValue">
                    <asp:LinkButton SkinID="ZSSM_Button01" ID="SearchButton" runat="server" Text="Cerca"
                        OnClick="SearchButton_Click"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
