﻿<%@ Control Language="C#" ClassName="PageDisgnerPhoto" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">

    
    static int UP = 3;
    static int DOWN = 4;



    protected void Page_Load(Object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["XRI"]))
        {
            AggiungiHyperLink.NavigateUrl = "Photo_New.aspx?XRI=" + Request.QueryString["XRI"];

            if (!string.IsNullOrEmpty(Request.QueryString["XRI1"]))
                AggiungiHyperLink.NavigateUrl += "&XRI1=" + Request.QueryString["XRI1"];
        }


        if (!string.IsNullOrEmpty(Request.QueryString["ZIM"]))
            AggiungiHyperLink.NavigateUrl += "&ZIM=" + OttieniZIM(Request.QueryString["ZIM"], GetLang()).ToString() + "&ZIMRedirect=" + Request.QueryString["ZIM"];


        if (!string.IsNullOrEmpty(Request.QueryString["Lang"]))
            AggiungiHyperLink.NavigateUrl += "&Lang=" + Request.QueryString["Lang"];

    }//fine Page_Load



    private string GetLang()
    {
        string Lang = "ITA";

        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;

    }//fine GetLang

    private String OttieniZIM(string ZeusIdModulo, String ZeusLangCode)
    {

        try
        {

            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            string myXPathEach = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_PageDesigner.xml"));

            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPathEach);

            return mydoc.SelectSingleNode(myXPath + "ZMCD_ZIMGalleryAssociation").InnerText;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return "Errore";
        }

    }//fine OttieniZIM

    //FUNZIONI PER L'ORDINAMENTO  


    private int getLastOrder()
    {

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT MAX(Ordinamento)";
                sqlQuery += " FROM [tbPageDesigner_Photo] ";
                sqlQuery += " WHERE [ID2PageDesigner]=@ID2PageDesigner";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID2PageDesigner", System.Data.SqlDbType.Int);
                command.Parameters["@ID2PageDesigner"].Value = Server.HtmlEncode(Request.QueryString["XRI"]);

                SqlDataReader reader = command.ExecuteReader();

                int myReturn = 0;

                while (reader.Read())
                    if (!reader.IsDBNull(0))
                        myReturn = reader.GetInt16(0);


                reader.Close();
                return myReturn;
            }
            catch (Exception p)
            {

                Response.Write(p.ToString());
                return -1;
            }
        }//fine Using

    }//fine getLastOrder


    static int LIVELLI = 1;


    private bool CheckIntegrityR(string ID2PageDesigner)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {



            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();




                ArrayList myArray = new ArrayList();

                string[][] myMenuChange = null;

                int myOrder = 1;


                SqlDataReader reader = null;


                for (int index = 1; index <= LIVELLI; ++index)
                {


                    sqlQuery = "SELECT [ID1Photo],Ordinamento";
                    sqlQuery += " FROM [tbPageDesigner_Photo]";
                    sqlQuery += " WHERE [ID2PageDesigner]=@ID2PageDesigner";
                    sqlQuery += " ORDER BY Ordinamento,ID1Photo";
                    command.CommandText = sqlQuery;
                    command.Parameters.Clear();


                    command.Parameters.Add("@ID2PageDesigner", System.Data.SqlDbType.Int, 4);
                    command.Parameters["@ID2PageDesigner"].Value = ID2PageDesigner;

                    reader = command.ExecuteReader();

                    myOrder = 1;

                    while (reader.Read())
                    {

                        if (myOrder != reader.GetInt16(1))
                        {

                            string[] myData = new string[2];
                            myData[0] = reader.GetInt32(0).ToString();
                            myData[1] = myOrder.ToString();
                            myArray.Add(myData);
                        }

                        ++myOrder;
                    }//fine while

                    reader.Close();




                    myMenuChange = (string[][])myArray.ToArray(typeof(string[]));
                    myArray = new ArrayList();


                    for (int i = 0; i < myMenuChange.Length; ++i)
                    {

                        sqlQuery = "UPDATE tbPageDesigner_Photo ";
                        sqlQuery += " SET Ordinamento=@Ordinamento";
                        sqlQuery += " WHERE ID1Photo=@ID1Photo";
                        command.CommandText = sqlQuery;
                        command.Parameters.Clear();

                        command.Parameters.Add("@Ordinamento", System.Data.SqlDbType.NVarChar, 4);
                        command.Parameters["@Ordinamento"].Value = myMenuChange[i][1];

                        command.Parameters.Add("@ID1Photo", System.Data.SqlDbType.Int, 4);
                        command.Parameters["@ID1Photo"].Value = myMenuChange[i][0];

                        command.ExecuteNonQuery();


                    }//fine for




                }//fine for LIVELLI


                return true;
            }
            catch (Exception p)
            {

                Response.Write(p.ToString());

                return false;
            }
        }//fine using
    }//fine CheckIntegrityR


    //FINE FUNZIONI PER ORDINAMENTO  


    //FUNZIONI PER LA GRIDVIEW CHE ULIZZANO L'ORDINAMENTO
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {


        try
        {

            int index = Convert.ToInt32(e.CommandArgument);
            int indexNew = index;

            if (e.CommandName.ToString().Equals("Up"))
                --indexNew;
            else if (e.CommandName.ToString().Equals("Down"))
                ++indexNew;



            GridViewRow row = GridView1.Rows[index];
            Label ID1PhotoAssoc = (Label)row.FindControl("ID1Photo");
            Label Ordinamento = (Label)row.FindControl("Ordinamento");


            GridViewRow rowNew = GridView1.Rows[indexNew];
            Label ID1PhotoAssocNew = (Label)rowNew.FindControl("ID1Photo");
            Label OrdinamentoNew = (Label)rowNew.FindControl("Ordinamento");

            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            string sqlQuery = string.Empty;


            using (SqlConnection connection = new SqlConnection(
               strConnessione))
            {


                connection.Open();

                SqlCommand command = connection.CreateCommand();




                sqlQuery = "UPDATE [tbPageDesigner_Photo]";
                sqlQuery += " SET Ordinamento=" + OrdinamentoNew.Text;
                sqlQuery += " WHERE [ID1Photo]=" + ID1PhotoAssoc.Text + ";";
                sqlQuery += "UPDATE [tbPageDesigner_Photo]";
                sqlQuery += " SET Ordinamento=" + Ordinamento.Text;
                sqlQuery += " WHERE [ID1Photo]=" + ID1PhotoAssocNew.Text;
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();






            }//fine using

            GridView1.DataBind();


        }
        catch (Exception p)
        {
            //Response.Write(p.ToString());

        }


    }//fine GridView1_RowCommand

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {

            if (e.Row.DataItemIndex > -1)
            {

                Label Ordinamento = (Label)e.Row.FindControl("Ordinamento");
                int ordinamentoRow = Convert.ToInt32(Ordinamento.Text);

                int maxordine = getLastOrder();


                //Response.Write("DEB ordinamentoRow: " + ordinamentoRow+"<br />");
                //Response.Write("DEB maxordine: " + maxordine + "<br />");

                if (ordinamentoRow == 1)
                    e.Row.Cells[UP].Text = string.Empty;

                if (ordinamentoRow == maxordine)
                    e.Row.Cells[DOWN].Text = string.Empty;



                //immagini attivo
                Image Image1 = (Image)e.Row.FindControl("Image1");
                Label AttivoArea1 = (Label)e.Row.FindControl("AttivoArea1");

                if ((Image1 != null) && (AttivoArea1 != null))
                    if (Convert.ToBoolean(AttivoArea1.Text.ToString()))
                        Image1.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";


                if (e.Row.Cells[UP].Controls.Count > 0)
                {
                    e.Row.Cells[UP].Attributes.Add("onmouseover", "showImageUP('" + e.Row.Cells[UP].Controls[0].ClientID + "','2');");
                    e.Row.Cells[UP].Attributes.Add("onmouseout", "showImageUP('" + e.Row.Cells[UP].Controls[0].ClientID + "','1');");
                }

                if (e.Row.Cells[DOWN].Controls.Count > 0)
                {
                    e.Row.Cells[DOWN].Attributes.Add("onmouseover", "showImageDOWN('" + e.Row.Cells[DOWN].Controls[0].ClientID + "','2');");
                    e.Row.Cells[DOWN].Attributes.Add("onmouseout", "showImageDOWN('" + e.Row.Cells[DOWN].Controls[0].ClientID + "','1');");
                }

                
                /////////////// Imposto URL per ZIM

                string myNavigateUrl = string.Empty;
                string myZeusIDModulo = string.Empty;
                myZeusIDModulo = ((HyperLink)e.Row.FindControl("lnkUrl")).ToolTip;
                myNavigateUrl = ((HyperLink)e.Row.FindControl("lnkUrl")).NavigateUrl + OttieniZIM(myZeusIDModulo, GetLang());
                ((HyperLink)e.Row.FindControl("lnkUrl")).NavigateUrl = myNavigateUrl;

                /////////////// FINE Imposto URL per ZIM

            }//fine if
        }
        catch (Exception p)
        {

            //Response.Write(p.ToString());
        }

    }//fine GridView1_RowDataBound

    protected void GridView1_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {
        CheckIntegrityR(Server.HtmlEncode(Request.QueryString["XRI"]));

    }//fine GridView1_RowDeleted
    
</script>
<script language="javascript" type="text/javascript">
    function showImageUP(imgId, typ) {
        var objImg = document.getElementById(imgId);
        if (objImg) {
            if (typ == "1")
                objImg.src = "../SiteImg/Bt3_ArrowUp_Off.gif";
            else if (typ == "2")
                objImg.src = "../SiteImg/Bt3_ArrowUp_On.gif";
        }
    }

    function showImageDOWN(imgId, typ) {
        var objImg = document.getElementById(imgId);
        if (objImg) {
            if (typ == "1")
                objImg.src = "../SiteImg/Bt3_ArrowDown_Off.gif";
            else if (typ == "2")
                objImg.src = "../SiteImg/Bt3_ArrowDown_On.gif";
        }
    }
</script>
<div style="padding: 10px 0 0 0; margin: 0;">
    <asp:HyperLink ID="AggiungiHyperLink" runat="server"
        Text="Aggiungi foto"
        SkinID="ZSSM_Button01">
    </asp:HyperLink>
</div>
<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="PhotoGallerySqlDataSource"
    OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand" CellPadding="0"
    DataKeyNames="ID1Photo" Width="100%" OnRowDeleted="GridView1_RowDeleted">
    <Columns>
        <asp:TemplateField HeaderText="ID1Photo" SortExpression="ID1Photo" Visible="False">
            <ItemTemplate>
                <asp:Label ID="ID1Photo" runat="server" Text='<%# Eval("ID1Photo") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Ordinamento" SortExpression="Ordinamento" Visible="False">
            <ItemTemplate>
                <asp:Label ID="Ordinamento" runat="server" Text='<%# Eval("Ordinamento") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Fotografia">
            <ItemTemplate>
                <asp:Image ID="Immagine2Image" runat="server" ImageUrl='<%# Eval("Immagine2","~/ZeusInc/PhotoGallery/Img2Photo/{0}") %>' />
            </ItemTemplate>
            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
        </asp:TemplateField>
        <asp:ButtonField CommandName="Up" HeaderText="Ord." ShowHeader="True" ImageUrl="~/Zeus/SiteImg/Bt3_ArrowUp_Off.gif"
            ButtonType="Image" ControlStyle-Width="16" ControlStyle-Height="16" ControlStyle-BorderWidth="0">
            <ItemStyle HorizontalAlign="Center" />
            <ControlStyle CssClass="GridView_Ord1" />
        </asp:ButtonField>
        <asp:ButtonField CommandName="Down" HeaderText="Ord." ShowHeader="True" ButtonType="Image"
            ImageUrl="~/Zeus/SiteImg/Bt3_ArrowDown_Off.gif" ControlStyle-Width="16" ControlStyle-Height="16"
            ControlStyle-BorderWidth="0">
            <ItemStyle HorizontalAlign="Center" />
            <ControlStyle CssClass="GridView_Ord1" />
        </asp:ButtonField>
        <asp:TemplateField HeaderText="Attivo">
            <ItemTemplate>
                <asp:Label ID="AttivoArea1" runat="server" Text='<%# Eval("Pubblica") %>' Visible="false"></asp:Label>
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" />
            </ItemTemplate>
            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />

        </asp:TemplateField>
        <asp:BoundField DataField="DataCreazione" HeaderText="Data creazione"
            ItemStyle-HorizontalAlign="Center" />

        <asp:CommandField ButtonType="Link" HeaderText="Rimuovi" ShowDeleteButton="true"
            DeleteText="Rimuovi" ControlStyle-CssClass="GridView_Button1" ItemStyle-HorizontalAlign="Center" />
<%--        <asp:HyperLinkField DataNavigateUrlFields="ID2PageDesigner,ID1Photo,ZeusIdModulo,ZeusLangCode,Livello"
            Text="Modifica" DataNavigateUrlFormatString="Photo_Edt.aspx?XRI={0}&XRP={1}&ZIM={2}&Lang={3}&XRI1={4}"
            HeaderText="Modifica">
            <ControlStyle CssClass="GridView_ZeusButton1" />
            <ItemStyle HorizontalAlign="Center" />
        </asp:HyperLinkField>--%>
                         <asp:TemplateField HeaderText="Modifica">
                <ItemTemplate>
                     <asp:HyperLink Text="Modifica" ID="lnkUrl" runat="server" ToolTip='<%# Eval("ZeusIdModulo") %>'  NavigateUrl='<%# String.Format("Photo_Edt.aspx?XRI={0}&XRI1={1}&Lang={2}&ZIMRedirect={3}&ZIM=", Eval("ID2PageDesigner"),Eval("ID1Photo"),Eval("ZeusLangCode"),Eval("ZeusIdModulo")) %>' ControlStyle-CssClass="GridView_Button1"></asp:HyperLink>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />
          </asp:TemplateField>

    </Columns>
    <EmptyDataTemplate>
        Nessuna foto associata a questo contenuto
        <br />
        <br />
        <br />
        <br />
        <br />
    </EmptyDataTemplate>
    <EmptyDataRowStyle BackColor="White" Width="500px" BorderColor="Red" BorderWidth="0px"
        Height="200px" />
</asp:GridView>
<asp:SqlDataSource ID="PhotoGallerySqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
    SelectCommand="SELECT     dbo.tbPageDesigner_Photo.ID1Photo, 
    dbo.tbPageDesigner_Photo.Immagine1,
    dbo.tbPageDesigner_Photo.Immagine2, 
    dbo.tbPageDesigner_Photo.Ordinamento, 
    dbo.tbPageDesigner_Photo.Pubblica, 
    dbo.tbPageDesigner_Photo.ID2PageDesigner, 
    dbo.vwPageDesigner_Lst_All.ZeusIdModulo, 
    dbo.vwPageDesigner_Lst_All.ZeusLangCode,
    dbo.tbPageDesigner_Photo.DataCreazione
FROM         dbo.tbPageDesigner_Photo INNER JOIN
                      dbo.[vwPageDesigner_Lst_All] ON dbo.tbPageDesigner_Photo.ID2PageDesigner = dbo.[vwPageDesigner_Lst_All].ID1Pagina
WHERE [ID2PageDesigner]=@ID2PageDesigner ORDER BY Ordinamento"
    DeleteCommand="DELETE FROM tbPageDesigner_Photo  WHERE [ID1Photo] =@ID1Photo">
    <SelectParameters>
        <asp:QueryStringParameter DefaultValue="0" Name="ID2PageDesigner" QueryStringField="XRI"
            Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>
