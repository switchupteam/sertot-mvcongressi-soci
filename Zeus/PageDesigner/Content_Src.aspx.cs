﻿using System;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per Content_Src
/// </summary>
public partial class PageDesigner_Content_Src : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {

        delinea myDelinea = new delinea();

        if (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM"))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        ZIM.Value = Server.HtmlEncode(Request.QueryString["ZIM"]);

        if (!ReadXML(ZIM.Value, GetLang()))
            Response.Redirect("~/Zeus/System/Message.aspx?1");

        ReadXML_Localization(ZIM.Value, GetLang());

        SetFocus(SearchButton.UniqueID);

    }//fine Page_Load

    private void SetFocus(string ID)
    {
        Page.Form.DefaultButton = ID;
        Page.Form.DefaultFocus = ID;
    }//fine SetFocus


    private string GetLang()
    {
        string Lang = "ITA";

        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;

    }//fine GetLang

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_PageDesigner.xml"));


            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {

                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {

                            if (childNode.Name.Equals("ZML_TitoloPagina_Src"))
                            {
                                TitleField.Value = childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                            }

                        }
                        catch (Exception)
                        {
                        }
                    }//fine foreach
                }//fine foreach

            }//fine for
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine ReadXML_Localization

    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_PageDesigner.xml"));



            if (!CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_Src").InnerText))
                Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223599");


            if (!Page.IsPostBack)
                SetQueryOfID2CategoriaDropDownList(mydoc.SelectSingleNode(myXPath + "ZMCD_ZIMCategoria1").InnerText,
                    GetLang(), mydoc.SelectSingleNode(myXPath + "ZMCD_Cat1Liv").InnerText);

            return true;
        }
        catch (Exception)
        {
            return false;
        }

    }//fine ReadXML


    private bool CheckPermit(string AllRoles)
    {

        bool IsPermit = false;

        try
        {

            if (AllRoles.Length == 0)
                return false;

            char[] delimiter = { ',' };


            foreach (string roles in Roles.GetRolesForUser(Membership.GetUser().UserName))
            {
                string[] PressRoles = AllRoles.Split(delimiter);

                for (int i = 0; i < PressRoles.Length; ++i)
                {
                    if (PressRoles[i].Trim().Equals(roles.Trim()))
                    {
                        IsPermit = true;
                        break;
                    }
                }//fine for

            }//fine else
        }
        catch (Exception)
        { }

        return IsPermit;

    }//fine CheckPermit








    protected void PWF(object sender, EventArgs e)
    {
        TextBox DataLimite = (TextBox)sender;
        if (DataLimite.Text.Length == 0)
            DataLimite.Text = DateTime.Now.ToString("d");

    }


    //##############################################################################################################
    //################################################ CATEGORIE1 ###################################################
    //############################################################################################################## 
    private bool SetQueryOfID2CategoriaDropDownList(string ZeusIdModulo,
      string ZeusLangCode, string PZV_Cat1Liv)
    {
        try
        {

            //SqlDataSource dsCategoria = (SqlDataSource)FormView1.FindControl("dsCategoria");
            //DropDownList ID2CategoriaDropDownList = (DropDownList)FormView1.FindControl("ID2CategoriaDropDownList");
            //HiddenField ID2CategoriaHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaHiddenField");



            switch (PZV_Cat1Liv)
            {

                case "123":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv1Liv2Liv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "Catliv1Liv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;


                case "23":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[MenuLabel2]";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "MenuLabel2";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;

                default:
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;
            }

            return true;
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }
    }//fine SetQueryOfID2CategoriaDropDownList

    //##############################################################################################################
    //############################################FINE CATEGORIE1 ###################################################
    //############################################################################################################## 

    protected void SearchButton_Click(object sender, EventArgs e)
    {

        string MyRedirect = "~/Zeus/PageDesigner/Content_Lst.aspx" +
          "?XRE=" + TitoloTextBox.Text +
          "&DT1I=" + NewDateFirstTextBox.Text +
          "&DT1F=" + NewDateLastTextBox.Text +
          "&DT2I=" + EdtDateFirstTextBox.Text +
          "&DT2F=" + EdtDateLastTextBox.Text +
          "&ZIM=" + Server.HtmlEncode(Request.QueryString["ZIM"]) +
          "&Lang=" + GetLang();


        if (!ID2CategoriaDropDownList.SelectedValue.Equals("0"))
            MyRedirect += "&XRI1=" + ID2CategoriaDropDownList.SelectedValue;


        Response.Redirect(MyRedirect);

    }//fine SearchButton_Click
    
}