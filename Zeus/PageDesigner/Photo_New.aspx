﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page"
    Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">

    static string TitoloPagina = "Inserisci nuova fotografia";

    
    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;


        delinea myDeliena = new delinea();

        if (!myDeliena.AntiSQLInjectionNew(Request.QueryString, "XRI", "int")
            || !myDeliena.AntiSQLInjectionNew(Request.QueryString, "ZIM", "string")
            || !myDeliena.AntiSQLInjectionNew(Request.QueryString, "ZIMRedirect", "string")
            || !myDeliena.AntiSQLInjectionNew(Request.QueryString, "Lang", "string"))
            Response.Redirect("~/Zeus/System/Message.aspx?0");


        ID2PageDesigner.Value = Request.QueryString["XRI"];
        ZeusIdModulo.Value = Request.QueryString["ZIM"];
        ZIMREdirect.Value = Request.QueryString["ZIMRedirect"];
        Lang.Value = Request.QueryString["Lang"];


       

    }//fine Page_Load


    //##############################################################################################################
    //################################################ FILE UPLOAD #################################################
    //############################################################################################################## 

    protected void FileUploadCustomValidator_ServerValidate(object sender, ServerValidateEventArgs args)
    {
        try
        {
            ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");
            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");

            if (ImageRaider1.isValid())
                InsertButton.CommandName = "Insert";
            else
                InsertButton.CommandName = string.Empty;

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine FileUploadCustomValidator
    //##############################################################################################################
    //########################################### FINE FILE UPLOAD #################################################
    //##############################################################################################################


    private int getOrdinamento()
    {

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();



                sqlQuery = "SELECT COUNT(ID1Photo)";
                sqlQuery += " FROM [tbPageDesigner_Photo] ";
                sqlQuery += "WHERE [ID2PageDesigner]=@ID2PageDesigner ";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID2PageDesigner", System.Data.SqlDbType.Int);
                command.Parameters["@ID2PageDesigner"].Value = ID2PageDesigner.Value;
                

                int myCount = Convert.ToInt32(command.ExecuteScalar());
                ++myCount;

                if (myCount != null)
                    return myCount;
                else return 1;

            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
                return -1;
            }
        }//fine Using
    }//fine ClearAllTables
    

    protected void InsertButton_Click(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();

        LinkButton InsertButton = (LinkButton)sender;

        HiddenField FileNameBetaHiddenField = (HiddenField)FormView1.FindControl("FileNameBetaHiddenField");
        HiddenField FileNameGammaHiddenField = (HiddenField)FormView1.FindControl("FileNameGammaHiddenField");
        ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");

        HiddenField Ordinamento = (HiddenField)FormView1.FindControl("Ordinamento");

        
        if (InsertButton.CommandName != string.Empty)
        {

            if (ImageRaider1.GenerateBeta(string.Empty))
			FileNameBetaHiddenField.Value = ImageRaider1.ImgBeta_FileName;
		else FileNameBetaHiddenField.Value = ImageRaider1.DefaultBetaImage;

           if (ImageRaider1.GenerateGamma(string.Empty))
			FileNameGammaHiddenField.Value = ImageRaider1.ImgGamma_FileName;
		else FileNameGammaHiddenField.Value = ImageRaider1.DefaultGammaImage;


            Ordinamento.Value = getOrdinamento().ToString();
            
        }//fine if command
        
    }// fine InsertButton_Click 


    protected void tbPageDesigner_PhotoSqlDataSource_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.AffectedRows == 0 || e.Exception != null)
            Response.Redirect("~/Zeus/System/Message.aspx?InsertErr");


        Response.Redirect("Content_Dtl.aspx?XRI=" + ID2PageDesigner.Value
            + "&ZIM=" + ZIMREdirect.Value
            +"&Lang="+Lang.Value+"#PHOTO");

        
    }//fine tbPageDesigner_PhotoSqlDataSource_Inserted
    
    
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" />
    <asp:HiddenField ID="ID2PageDesigner" runat="server" />
    <asp:HiddenField ID="ZeusIdModulo" runat="server" />
    <asp:HiddenField ID="ZIMREdirect" runat="server" />
    <asp:HiddenField ID="Lang" runat="server" />
    
    
    <asp:FormView ID="FormView1" runat="server" DataKeyNames="ID1Photo" DataSourceID="tbPageDesigner_PhotoSqlDataSource"
        DefaultMode="Insert" Width="100%">
        <InsertItemTemplate>
            <dlc:ImageRaider ID="ImageRaider1" runat="server" ZeusIdModuloIndice="1" ZeusLangCode="ITA"
                BindPzFromDB="true" Required="true"  ZeusIdModulo="PHGFF"/>
            <asp:HiddenField ID="FileNameBetaHiddenField" runat="server" Value='<%# Bind("Immagine1") %>' />
            <asp:HiddenField ID="FileNameGammaHiddenField" runat="server" Value='<%# Bind("Immagine2") %>' />
            <asp:CustomValidator ID="FileUploadCustomValidator" runat="server" OnServerValidate="FileUploadCustomValidator_ServerValidate"
                Display="Dynamic" SkinID="ZSSM_Validazione01" Visible="false"></asp:CustomValidator>
            <div class="BlockBox">
                <div class="BlockBoxHeader">
                    <asp:Label ID="IdentificativoLabel" runat="server">Pubblica</asp:Label></div>
                <table>
                    <tr>
                        <td class="BlockBoxDescription">
                            <asp:Label ID="ZML_Articolo" SkinID="FieldDescription" runat="server" Text="Pubblica"></asp:Label>
                        </td>
                        <td class="BlockBoxValue">
                            <asp:CheckBox ID="PubblicaCheckBox" runat="server" Checked='<%# Bind("Pubblica") %>' />
                        </td>
                    </tr>
                </table>
            </div>
            <asp:HiddenField ID="Ordinamento" runat="server" Value='<%# Bind("Ordinamento") %>' />
             <div align="center">
                <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert"
                    SkinID="ZSSM_Button01" Text="Salva dati" OnClick="InsertButton_Click"></asp:LinkButton></div>
        </InsertItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="tbPageDesigner_PhotoSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        InsertCommand="INSERT INTO tbPageDesigner_Photo 
        (ID2PageDesigner,Immagine1, Immagine2 , Pubblica,Ordinamento,DataCreazione)
VaLUES (@ID2PageDesigner,@Immagine1 , @Immagine2 ,@Pubblica,@Ordinamento, GETDATE())" 
        oninserted="tbPageDesigner_PhotoSqlDataSource_Inserted">
        <InsertParameters>
            <asp:Parameter Name="Immagine1" Type="String" />
            <asp:Parameter Name="Immagine2" Type="String" />
            <asp:Parameter Name="Pubblica" Type="Boolean" />
            <asp:Parameter Name="Ordinamento" Type="Int32" />
            <asp:ControlParameter Name="ID2PageDesigner" ControlID="ID2PageDesigner"  Type="Int32" PropertyName="Value" />
        </InsertParameters>
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="0" Name="ID2PageDesigner" QueryStringField="XRI"
                Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
