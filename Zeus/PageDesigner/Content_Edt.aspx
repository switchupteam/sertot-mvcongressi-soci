<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="Content_Edt.aspx.cs"
    Inherits="PageDesigner_Content_Edt" Theme="Zeus" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Data.SqlClient" %>
<%--<%@ Register Src="PageDesignerLangButton.ascx" TagName="PageDesignerLangButton" TagPrefix="uc1" %>--%>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <script type="text/javascript">
        function OnClientModeChange(editor) {
            var mode = editor.get_mode();
            var doc = editor.get_document();
            var head = doc.getElementsByTagName("HEAD")[0];
            var link;

            switch (mode) {
                case 1: //remove the external stylesheet when displaying the content in Design mode    
                    //var external = doc.getElementById("external");
                    //head.removeChild(external);
                    break;
                case 2:
                    break;
                case 4: //apply your external css stylesheet to Preview mode    
                    link = doc.createElement("LINterK");
                    link.setAttribute("href", "/SiteCss/Telerik.css");
                    link.setAttribute("rel", "stylesheet");
                    link.setAttribute("type", "text/css");
                    link.setAttribute("id", "external");
                    head.appendChild(link);
                    break;
            }
        }

        function editorCommandExecuted(editor, args) {
            if (!$telerik.isChrome)
                return;
            var dialogName = args.get_commandName();
            var dialogWin = editor.get_dialogOpener()._dialogContainers[dialogName];
            if (dialogWin) {
                var cellEl = dialogWin.get_contentElement() || dialogWin.ui.contentCell || dialogWin.ui.content,
                frame = dialogWin.get_contentFrame();
                frame.onload = function () {
                    cellEl.style.cssText = "";
                    dialogWin.autoSize();
                }
            }
        }
    </script>

    <asp:HiddenField ID="TitleField" runat="server" />
    <asp:HiddenField ID="XRI" runat="server" />
    <asp:HiddenField ID="ZIM" runat="server" />
    
    <asp:FormView ID="FormView1" runat="server" DataKeyNames="ID1Pagina" DataSourceID="dsDesignerNew"
        DefaultMode="Edit" Width="100%">
        <EditItemTemplate>
            <div id="tabs">

                <ul>
                    <li><a href="#Principale">Principale</a></li>
                    <li><a href="#Immagini">Immagini</a></li>
                    <li><a href="#Contenuti">Contenuti</a></li>
                </ul>
                <div id="Principale">
                    <div class="BlockBox">
                        <div class="BlockBoxHeader">
                            <asp:Label ID="IdentificativoLabel" runat="server">Identificativo pagina</asp:Label>
                        </div>
                        <table>
                            <asp:Panel ID="ZMCF_TitoloPagina" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="Label1" runat="server" SkinID="FieldDescription" Text="Label">Titolo pagina *</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:TextBox ID="TitoloPaginaTextBox" runat="server" Text='<%# Bind("TitoloPagina") %>'
                                            Columns="100" MaxLength="100"></asp:TextBox>
                                        <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="TitoloPaginaRequiredFieldValidator"
                                            ErrorMessage="Obbligatorio" runat="server" ControlToValidate="TitoloPaginaTextBox"
                                            SkinID="ZSSM_Validazione01" Display="Dynamic">
                                        </asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator Display="Dynamic" SkinID="ZSSM_Validazione01" ControlToValidate="TitoloPaginaTextBox" ValidationGroup="myValidation" ID="RegularExpressionValidator19" ValidationExpression="^[\s\S]{0,100}$" runat="server" ErrorMessage="Massimo 100 caratteri"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="ZMCF_Sottotitolo" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="Label9" runat="server" SkinID="FieldDescription" Text="Label">Sottotitolo</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:TextBox ID="SottotitoloTextBox" runat="server" Columns="100" MaxLength="100"
                                            Text='<%# Bind("Sottotitolo", "{0}") %>'></asp:TextBox>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="ZMCF_ParentPage" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="Label10" runat="server" SkinID="FieldDescription" Text="Label">Pagina padre</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:DropDownList ID="PaginaPadreDropDownList" runat="server" DataSourceID="dsTipologiaCategorie"
                                            DataTextField="MenuLabel1" DataValueField="ID1Pagina" SelectedValue='<%# Bind("ID2ParentPage") %>'
                                            AppendDataBoundItems="True" OnSelectedIndexChanged="DropDownList_SelectIndexChange"
                                            OnDataBound="PaginaPadreDropDownList_DataBound" AutoPostBack="true">
                                            <asp:ListItem Value="0">&gt; Pagina singola / home di sezione</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:SqlDataSource ID="dsTipologiaCategorie" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                                            SelectCommand="SELECT [ID1Pagina], [MenuLabel1] FROM [vwPageDesigner_ParentPages_Zeus1] WHERE ([ZeusIdModulo] = @ZeusIdModulo)  ORDER BY [MenuLabel1]">
                                            <SelectParameters>
                                                <asp:QueryStringParameter DefaultValue="0" Name="ZeusIdModulo" QueryStringField="ZIM"
                                                    Type="String" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="ZMCF_Categoria" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="Label11" runat="server" SkinID="FieldDescription" Text="Label">Categoria *</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:DropDownList ID="ID2CategoriaDropDownList" runat="server" OnSelectedIndexChanged="ID2CategoriaDropDownList_SelectIndexChange"
                                            AppendDataBoundItems="True">
                                        </asp:DropDownList>
                                        <asp:HiddenField ID="ID2CategoriaHiddenField" runat="server" Value='<%# Bind("ID2Categoria") %>' />
                                        <asp:RequiredFieldValidator ID="ID2CategoriaRequiredFieldValidator" ErrorMessage="Obbligatorio"
                                            runat="server" ControlToValidate="ID2CategoriaDropDownList" SkinID="ZSSM_Validazione01"
                                            Display="Dynamic" InitialValue="0" ValidationGroup="myValidation">
                                        </asp:RequiredFieldValidator>
                                        <asp:SqlDataSource ID="dsCategoria" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"></asp:SqlDataSource>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="ZMCF_ZeusJollyText" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_ZeusJollyText" runat="server" SkinID="FieldDescription" Text="Label">Sottotitolo</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:TextBox ID="ZeusJollyTextTextBox" runat="server" Columns="140" MaxLength="200"
                                            Text='<%# Bind("ZeusJollyText", "{0}") %>'></asp:TextBox>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="PZV_DescrizioneZATPanel" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="PZL_DescrizioneZATLabel" runat="server" SkinID="FieldDescription"
                                            Text="Label">ZeusAssetType</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:Label ID="ZeusAssetTypeTextBox" runat="server" Text='<%# Eval("ZeusAssetType", "{0}") %>'
                                            SkinID="FieldValue"></asp:Label>
                                    </td>
                                </tr>
                            </asp:Panel>
                        </table>
                    </div>
                    <asp:Panel ID="ZMCF_MenuSezione" runat="server" Visible="false">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="Label16" runat="server">Men� di navigazione</asp:Label>
                            </div>
                            <table>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="VoceLabel" runat="server" SkinID="FieldDescription" Text="Label">Voce per men� di sezione *</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:TextBox ID="SezioneTextBox" runat="server" Columns="60" MaxLength="40" Text='<%# Bind("MenuSezione", "{0}") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="SezioneRequiredFieldValidator"
                                            ErrorMessage="Obbligatorio" runat="server" ControlToValidate="SezioneTextBox"
                                            SkinID="ZSSM_Validazione01" Display="Dynamic">
                                        </asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="Label17" runat="server" SkinID="FieldDescription" Text="Label">Pubblica men�</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:CheckBox ID="PW_MenuSezioneCheckBox" runat="server" Checked='<%# Bind("PW_MenuSezione") %>' />
                                        <asp:Label ID="PosizioneLabel" runat="server" SkinID="FieldDescription" Text="Label">in posizione *</asp:Label>
                                        <asp:TextBox ID="PosizioneTextBox" runat="server" Columns="4" MaxLength="2" Text='<%# Bind("Ordinamento1", "{0}") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="PosizioneRequiredFieldValidator"
                                            ErrorMessage="Obbligatorio" runat="server" ControlToValidate="PosizioneTextBox"
                                            SkinID="ZSSM_Validazione01" Display="Dynamic">
                                        </asp:RequiredFieldValidator>
                                        <asp:RangeValidator ValidationGroup="myValidation" ID="PosizioneRangeValidator" runat="server"
                                            ControlToValidate="PosizioneTextBox" SkinID="ZSSM_Validazione01" Display="Dynamic"
                                            ErrorMessage="Richiesto numero intero" MaximumValue="9999999999999" MinimumValue="0"
                                            Type="Currency"></asp:RangeValidator>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>

                    <asp:Panel ID="ZMCF_BoxSEO" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="TagPaginaLabel" runat="server">SEO Search Engine Optimization</asp:Label>
                            </div>
                            <table>
                                <asp:Panel ID="ZMCF_TitoloBrowser" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="Label8" runat="server" SkinID="FieldDescription" Text="Label">TITLE / Titolo Browser</asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:TextBox ID="TitoloBrowserTextBox" runat="server" Text='<%# Bind("TitoloBrowser") %>'
                                                Columns="100" MaxLength="100"></asp:TextBox>
                                            <%--<asp:RequiredFieldValidator ValidationGroup="myValidation" ID="TitoloBrowserRequiredFieldValidator"
                                        ErrorMessage="Obbligatorio" runat="server" ControlToValidate="TitoloBrowserTextBox"
                                        SkinID="ZSSM_Validazione01" Display="Dynamic">
                                    </asp:RequiredFieldValidator>--%>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="ZMCF_TagPagina" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="Label13" runat="server" SkinID="FieldDescription" Text="Label">Description</asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:TextBox ID="TagDescriptionTextBox" runat="server" Text='<%# Bind("TagDescription") %>'
                                                Columns="100" MaxLength="280"></asp:TextBox>
                                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" 
                                    ValidationExpression="^[a-zA-Z0-9_.;:+, ]+$" 
                                    Display="Dynamic" ErrorMessage="Caratteri consentiti: lettere numeri e .,:;_-+"
                                   SkinID="ZSSM_Validazione01"
                                    ControlToValidate="TagDescriptionTextBox"></asp:RegularExpressionValidator>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="Label14" runat="server" SkinID="FieldDescription" Text="Label">Keywords</asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:TextBox ID="TagKeywordsTextBox" runat="server" Text='<%# Bind("TagKeywords") %>'
                                                Columns="100" MaxLength="280"></asp:TextBox>
                                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                    ValidationExpression="^[a-zA-Z0-9_.;:+, ]+$" 
                                    Display="Dynamic" ErrorMessage="Caratteri consentiti: lettere numeri e .,:;_-+"
                                   SkinID="ZSSM_Validazione01"
                                    ControlToValidate="TagKeywordsTextBox"></asp:RegularExpressionValidator>--%>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="ZMCF_TagMeta1" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="ZML_TagMeta1" runat="server" SkinID="FieldDescription" Text="Label">Description</asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:DropDownList ID="TagMeta1AttributeDropDownList" runat="server" SelectedValue='<%# Bind("TagMeta1Attribute") %>'>
                                                <asp:ListItem Text="name" Value="name" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="http-equiv" Value="http-equiv"></asp:ListItem>
                                                <asp:ListItem Text="property" Value="property"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:Label ID="Value1Label" runat="server" SkinID="FieldValue">Value</asp:Label>
                                            <asp:TextBox ID="TagMeta1ValueTextBox" runat="server" Text='<%# Bind("TagMeta1Value") %>'
                                                Columns="20" MaxLength="30"></asp:TextBox>
                                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator20" runat="server"
                                    ValidationExpression="^[a-zA-Z0-9_.;:+, ]+$" Display="Dynamic" ErrorMessage="Caratteri consentiti: lettere numeri e .,:;_-+"
                                    SkinID="ZSSM_Validazione01" ControlToValidate="TagMeta1ValueTextBox"></asp:RegularExpressionValidator>--%>
                                            <img src="../SiteImg/Spc.gif" width="10" />
                                            <asp:Label ID="Label18" runat="server" SkinID="FieldValue">Content </asp:Label>
                                            <asp:TextBox ID="TagMeta1ContentTextBox" runat="server" Text='<%# Bind("TagMeta1Content") %>'
                                                Columns="85" MaxLength="280"></asp:TextBox>
                                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator18" runat="server"
                                    ValidationExpression="^[a-zA-Z0-9_.;:+, ]+$" Display="Dynamic" ErrorMessage="Caratteri consentiti: lettere numeri e .,:;_-+"
                                    SkinID="ZSSM_Validazione01" ControlToValidate="TagMeta1ContentTextBox"></asp:RegularExpressionValidator>--%>
                                            <asp:CustomValidator ID="CustomValidator4" runat="server" ValidationGroup="myValidation"
                                                Display="Dynamic" ClientValidationFunction="BothRequired1" ErrorMessage="I campi Value e Content devono essere entrambi compilati oppure vuoti"
                                                SkinID="ZSSM_Validazione01"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="ZMCF_TagMeta2" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="ZML_TagMeta2" runat="server" SkinID="FieldDescription" Text="Label">Description</asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:DropDownList ID="TagMeta2AttributeDropDownList" runat="server" SelectedValue='<%# Bind("TagMeta2Attribute") %>'>
                                                <asp:ListItem Text="name" Value="name" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="http-equiv" Value="http-equiv"></asp:ListItem>
                                                <asp:ListItem Text="property" Value="property"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:Label ID="Value2Label" runat="server" SkinID="FieldValue">Value</asp:Label>
                                            <asp:TextBox ID="TagMeta2ValueTextBox" runat="server" Text='<%# Bind("TagMeta2Value") %>'
                                                Columns="20" MaxLength="30"></asp:TextBox>
                                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator19" runat="server"
                                    ValidationExpression="^[a-zA-Z0-9_.;:+, ]+$" Display="Dynamic" ErrorMessage="Caratteri consentiti: lettere numeri e .,:;_-+"
                                    SkinID="ZSSM_Validazione01" ControlToValidate="TagMeta2ValueTextBox"></asp:RegularExpressionValidator>--%>
                                            <img src="../SiteImg/Spc.gif" width="10" />
                                            <asp:Label ID="Label40" runat="server" SkinID="FieldValue">Content </asp:Label>
                                            <asp:TextBox ID="TagMeta2ContentTextBox" runat="server" Text='<%# Bind("TagMeta2Content") %>'
                                                Columns="85" MaxLength="280"></asp:TextBox>
                                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator21" runat="server"
                                    ValidationExpression="^[a-zA-Z0-9_.;:+, ]+$" Display="Dynamic" ErrorMessage="Caratteri consentiti: lettere numeri e .,:;_-+"
                                    SkinID="ZSSM_Validazione01" ControlToValidate="TagMeta2ContentTextBox"></asp:RegularExpressionValidator>--%>
                                            <asp:CustomValidator ID="CustomValidator5" runat="server" ValidationGroup="myValidation"
                                                Display="Dynamic" ClientValidationFunction="BothRequired2" ErrorMessage="I campi Value e Content devono essere entrambi compilati oppure vuoti"
                                                SkinID="ZSSM_Validazione01"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="ZMCF_UrlRewrite" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="Label5" runat="server" Text="Url Rewrite" SkinID="FieldDescription"></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:Label ID="ZMCD_UrlDomain" runat="server" SkinID="FieldValue"></asp:Label><asp:Label
                                                ID="ZMCD_UrlSection" runat="server" SkinID="FieldValue"></asp:Label><asp:Label ID="ID1Pagina"
                                                    runat="server" SkinID="FieldValue" Text='<%# Eval("ID1Pagina","{0}/") %>'></asp:Label><asp:TextBox
                                                        ID="UrlPageDetailTextBox" runat="server" Text='<%# Eval("UrlRewrite") %>' Width="400"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ErrorMessage="Obbligatorio"
                                                runat="server" ControlToValidate="UrlPageDetailTextBox" Display="Dynamic" SkinID="ZSSM_Validazione01"
                                                ValidationGroup="myValidation">
                                            </asp:RequiredFieldValidator>
                                            <asp:Label ID="InfoUrlRewritingLabel" runat="server" Text="<br />Possono essere utilizzati solo i caratteri alfanumerici ed i caratteri - _  Tutti gli altri caratteri verranno sostituiti automaticamente."
                                                SkinID="Note"></asp:Label>
                                            <asp:HiddenField ID="UrlRewriteHiddenField" runat="server" Value='<%# Bind("UrlRewrite") %>' />
                                        </td>
                                    </tr>
                                </asp:Panel>
                            </table>
                        </div>
                    </asp:Panel>

                    <asp:Panel ID="ZMCF_BoxPlanner" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="Label6" runat="server">Planner</asp:Label>
                            </div>
                            <table>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_PWArea1" SkinID="FieldDescription" runat="server"></asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:CheckBox ID="PW_Area1CheckBox" runat="server" Checked='<%# Bind("PW_Area1") %>' />
                                        <asp:Label ID="AttivaDa_Label" runat="server" Text="Attiva pubblicazione dalla data"
                                            SkinID="FieldValue"></asp:Label>
                                        <asp:TextBox ID="PWI_Area1TextBox" runat="server" Text='<%# Bind("PWI_Area1", "{0:d}") %>'
                                            Columns="10" MaxLength="10" OnDataBinding="PWI_Area1TextBox_DataBinding"></asp:TextBox>
                                        <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator3"
                                            runat="server" ControlToValidate="PWI_Area1TextBox" SkinID="ZSSM_Validazione01"
                                            Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="RegularExpressionValidator2"
                                            runat="server" ControlToValidate="PWI_Area1TextBox" SkinID="ZSSM_Validazione01"
                                            Display="Dynamic" ErrorMessage="*" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$">Formato data richiesto: GG/MM/AAAA</asp:RegularExpressionValidator>
                                        <asp:Label ID="AttivaA_Label" runat="server" Text="alla data" SkinID="FieldValue"></asp:Label>
                                        <asp:TextBox ID="PWF_Area1TextBox" runat="server" Text='<%# Bind("PWF_Area1", "{0:d}") %>'
                                            Columns="10" MaxLength="10" OnDataBinding="PWF_Area1TextBox_DataBinding"></asp:TextBox>
                                        <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator4"
                                            runat="server" ControlToValidate="PWF_Area1TextBox" SkinID="ZSSM_Validazione01"
                                            Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                                ID="RegularExpressionValidator3" runat="server" ControlToValidate="PWF_Area1TextBox"
                                                SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="*" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$">Formato data richiesto: GG/MM/AAAA</asp:RegularExpressionValidator>
                                        <asp:CompareValidator ValidationGroup="myValidation" ID="CompareValidator1" runat="server"
                                            ControlToCompare="PWI_Area1TextBox" ControlToValidate="PWF_Area1TextBox" SkinID="ZSSM_Validazione01"
                                            Display="Dynamic" ErrorMessage="La data iniziale deve essere precedente o uguale alla data finale"
                                            Operator="GreaterThanEqual" Type="Date"></asp:CompareValidator>
                                    </td>
                                </tr>
                                <asp:Panel ID="ZMCF_Area2" runat="server">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="ZML_PWArea2" SkinID="FieldDescription" runat="server"></asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:CheckBox ID="CheckBox2" runat="server" Checked='<%# Bind("PW_Area2") %>' />
                                            <asp:Label ID="AttivaDa2_Label" runat="server" SkinID="FieldValue" Text="Attiva pubblicazione dalla data"></asp:Label>
                                            <asp:TextBox ID="PWI_Area2TextBox" runat="server" Text='<%# Bind("PWI_Area2", "{0:d}") %>'
                                                Columns="10" MaxLength="10" OnDataBinding="PWI_Area1TextBox_DataBinding"></asp:TextBox>
                                            <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator1"
                                                runat="server" ControlToValidate="PWI_Area2TextBox" SkinID="ZSSM_Validazione01"
                                                Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ValidationGroup="myValidation" ID="RegularExpressionValidator4"
                                                runat="server" ControlToValidate="PWI_Area2TextBox" SkinID="ZSSM_Validazione01"
                                                Display="Dynamic" ErrorMessage="*" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$">Formato data richiesto: GG/MM/AAAA</asp:RegularExpressionValidator>
                                            <asp:Label ID="AttivaA2_Label" runat="server" SkinID="FieldValue" Text="alla data"></asp:Label>
                                            <asp:TextBox ID="PWF_Area2TextBox" runat="server" Text='<%# Bind("PWF_Area2", "{0:d}") %>'
                                                Columns="10" MaxLength="10" OnDataBinding="PWF_Area1TextBox_DataBinding"></asp:TextBox>
                                            <asp:RequiredFieldValidator ValidationGroup="myValidation" ID="RequiredFieldValidator2"
                                                runat="server" ControlToValidate="PWF_Area2TextBox" SkinID="ZSSM_Validazione01"
                                                Display="Dynamic" ErrorMessage="*">Obbligatorio</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                                    ID="RegularExpressionValidator5" runat="server" ControlToValidate="PWF_Area2TextBox"
                                                    SkinID="ZSSM_Validazione01" Display="Dynamic" ErrorMessage="*" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$">Formato data richiesto: GG/MM/AAAA</asp:RegularExpressionValidator>
                                            <asp:CompareValidator ValidationGroup="myValidation" ID="CompareValidator2" runat="server"
                                                ControlToCompare="PWI_Area2TextBox" ControlToValidate="PWF_Area2TextBox" SkinID="ZSSM_Validazione01"
                                                Display="Dynamic" ErrorMessage="La data iniziale deve essere precedente o uguale alla data finale"
                                                Operator="GreaterThanEqual" Type="Date"></asp:CompareValidator>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="ArchivioPanel" runat="server" Visible="false">
                                    <tr>
                                        <td class="BlockBoxDescription">
                                            <asp:Label ID="Label7" runat="server" SkinID="FieldDescription" Text="Label">Archivia</asp:Label>
                                        </td>
                                        <td class="BlockBoxValue">
                                            <asp:CheckBox ID="ArchivioCheckBox" runat="server" Checked='<%# Bind("Archivia") %>' />
                                            <asp:Label ID="Label12" runat="server" SkinID="FieldValue" Text="Label">Se selezionato sposta il progetto in archivio.</asp:Label>
                                        </td>
                                    </tr>
                                </asp:Panel>
                            </table>
                        </div>
                    </asp:Panel>
                </div>
                <div id="Immagini">
                    <asp:Panel ID="ZMCF_Immagine1" runat="server">
                        <dlc:ImageRaider ID="ImageRaider1" runat="server" ZeusIdModuloIndice="1" ZeusLangCode="ITA"
                            BindPzFromDB="true" IsEditMode="true" DefaultEditBetaImage='<%# Eval("Immagine1") %>'
                            DefaultEditGammaImage='<%# Eval("Immagine2") %>'
                            ImageAlt='<%# Eval("Immagine12Alt") %>' OnDataBinding="ImageRaider_DataBinding" />
                        <asp:HiddenField ID="FileNameBetaHiddenField" runat="server" Value='<%# Bind("Immagine1") %>' />
                        <asp:HiddenField ID="FileNameGammaHiddenField" runat="server" Value='<%# Bind("Immagine2") %>' />
                        <asp:HiddenField ID="Immagine12AltHiddenField" runat="server" Value='<%# Bind("Immagine12Alt") %>' />
                        <asp:CustomValidator ID="CustomValidator1" runat="server" OnServerValidate="FileUploadCustomValidator_ServerValidate"
                            Display="Dynamic" SkinID="ZSSM_Validazione01" ValidationGroup="myValidation"
                            Visible="false"></asp:CustomValidator>
                    </asp:Panel>
                    <asp:Panel ID="ZMCF_Immagine2" runat="server">
                        <dlc:ImageRaider ID="ImageRaider2" runat="server" ZeusIdModuloIndice="2" ZeusLangCode="ITA"
                            BindPzFromDB="true" IsEditMode="true" DefaultEditBetaImage='<%# Eval("Immagine3") %>'
                            DefaultEditGammaImage='<%# Eval("Immagine4") %>'
                            ImageAlt='<%# Eval("Immagine34Alt") %>' OnDataBinding="ImageRaider_DataBinding" />
                        <asp:HiddenField ID="FileNameBeta2HiddenField" runat="server" Value='<%# Bind("Immagine3") %>' />
                        <asp:HiddenField ID="FileNameGamma2HiddenField" runat="server" Value='<%# Bind("Immagine4") %>' />
                        <asp:HiddenField ID="Image34AltHiddenField" runat="server" Value='<%# Bind("Immagine34Alt") %>' />
                        <asp:CustomValidator ID="CustomValidator2" runat="server" OnServerValidate="FileUploadCustomValidator_ServerValidate"
                            Display="Dynamic" SkinID="ZSSM_Validazione01" ValidationGroup="myValidation"
                            Visible="false"></asp:CustomValidator>
                    </asp:Panel>

                </div>
                <div id="Contenuti">
                    <asp:Panel ID="ZMCF_DescBreve1" runat="server">
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="Label2" runat="server">Descrizione</asp:Label>
                            </div>
                            <table>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="Label15" runat="server" SkinID="FieldDescription" Text="Label">Descrizione *</asp:Label>
                                        <asp:CustomValidator ValidationGroup="myValidation" ID="DescrizioneCustomValidator"
                                            runat="server" ErrorMessage="Obbligatorio" OnServerValidate="DescrizioneCustomValidator_ServerValidate"
                                            Display="Dynamic" SkinID="ZSSM_Validazione01"></asp:CustomValidator>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <CustomWebControls:TextArea ID="DexcrizioneTextArea" runat="server" Columns="100"
                                            MaxLength="500" TextMode="MultiLine" Text='<%# Bind("DescBreve1") %>' Height="200"></CustomWebControls:TextArea>
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </asp:Panel>
                    <div class="BlockBox">
                        <div class="BlockBoxHeader">
                            <asp:Label ID="Label3" runat="server">Contenuto</asp:Label>
                        </div>
                        <table>
                            <asp:Panel ID="ZMCF_Contenuto1" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_Contenuto1" runat="server" SkinID="FieldDescription" Text="Label">Contenuto principale</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <telerik:RadEditor
                                            Language="it-IT" ID="RadEditor1" runat="server"
                                            DocumentManager-DeletePaths="~/ZeusInc/PageDesigner/Documents"
                                            DocumentManager-SearchPatterns="*.*"
                                            DocumentManager-ViewPaths="~/ZeusInc/PageDesigner/Documents"
                                            DocumentManager-MaxUploadFileSize="52428800"
                                            DocumentManager-UploadPaths="~/ZeusInc/PageDesigner/Documents"
                                            FlashManager-DeletePaths="~/ZeusInc/PageDesigner/Media"
                                            FlashManager-MaxUploadFileSize="10240000"
                                            FlashManager-ViewPaths="~/ZeusInc/PageDesigner/Media"
                                            FlashManager-UploadPaths="~/ZeusInc/PageDesigner/Media"
                                            ImageManager-DeletePaths="~/ZeusInc/PageDesigner/Images"
                                            ImageManager-ViewPaths="~/ZeusInc/PageDesigner/Images"
                                            ImageManager-MaxUploadFileSize="10240000"
                                            ImageManager-SearchPatterns="*.gif, *.png, *.jpg, *.jpe, *.jpeg"
                                            ImageManager-UploadPaths="~/ZeusInc/PageDesigner/Images"
                                            ImageManager-ViewMode="Grid"
                                            MediaManager-DeletePaths="~/ZeusInc/PageDesigner/Media"
                                            MediaManager-MaxUploadFileSize="10240000"
                                            MediaManager-SearchPatterns="*.asf, *.asx, *.wma, *.wmv, *.avi, *.wav, *.mpeg, *.mpg, *.mpe, *.mov, *.mp3, *.m3u, *.mid, *.midi, *.rm, *.rma"
                                            MediaManager-ViewPaths="~/ZeusInc/PageDesigner/Media"
                                            MediaManager-UploadPaths="~/ZeusInc/PageDesigner/Media"
                                            TemplateManager-SearchPatterns="*.html,*.htm"
                                            ContentAreaMode="iframe"
                                            OnClientCommandExecuted="editorCommandExecuted"
                                            OnClientModeChange="OnClientModeChange"
                                            Content='<%# Bind("Contenuto1") %>'
                                            ToolsFile="~/Zeus/PageDesigner/RadEditor1.xml"
                                            LocalizationPath="~/App_GlobalResources"
                                            AllowScripts="true" RenderMode="Classic" ToolbarMode="Default" EnableViewState="False"
                                            Width="700px" Height="500px">
                                            <CssFiles>
                                                <telerik:EditorCssFile Value="~/asset/css/ZeusTypeFoundry.css" />
                                            </CssFiles>
                                        </telerik:RadEditor>

                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="ZMCF_Contenuto2" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_Contenuto2" runat="server" SkinID="FieldDescription" Text="Label">Contenuto secondario</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <telerik:RadEditor
                                            Language="it-IT" ID="RadEditor2" runat="server"
                                            DocumentManager-DeletePaths="~/ZeusInc/PageDesigner/Documents"
                                            DocumentManager-SearchPatterns="*.*"
                                            DocumentManager-ViewPaths="~/ZeusInc/PageDesigner/Documents"
                                            DocumentManager-MaxUploadFileSize="52428800"
                                            DocumentManager-UploadPaths="~/ZeusInc/PageDesigner/Documents"
                                            FlashManager-DeletePaths="~/ZeusInc/PageDesigner/Media"
                                            FlashManager-MaxUploadFileSize="10240000"
                                            FlashManager-ViewPaths="~/ZeusInc/PageDesigner/Media"
                                            FlashManager-UploadPaths="~/ZeusInc/PageDesigner/Media"
                                            ImageManager-DeletePaths="~/ZeusInc/PageDesigner/Images"
                                            ImageManager-ViewPaths="~/ZeusInc/PageDesigner/Images"
                                            ImageManager-MaxUploadFileSize="10240000"
                                            ImageManager-SearchPatterns="*.gif, *.png, *.jpg, *.jpe, *.jpeg"
                                            ImageManager-UploadPaths="~/ZeusInc/PageDesigner/Images"
                                            ImageManager-ViewMode="Grid"
                                            MediaManager-DeletePaths="~/ZeusInc/PageDesigner/Media"
                                            MediaManager-MaxUploadFileSize="10240000"
                                            MediaManager-SearchPatterns="*.asf, *.asx, *.wma, *.wmv, *.avi, *.wav, *.mpeg, *.mpg, *.mpe, *.mov, *.mp3, *.m3u, *.mid, *.midi, *.rm, *.rma"
                                            MediaManager-ViewPaths="~/ZeusInc/PageDesigner/Media"
                                            MediaManager-UploadPaths="~/ZeusInc/PageDesigner/Media"
                                            TemplateManager-SearchPatterns="*.html,*.htm"
                                            ContentAreaMode="iframe"
                                            OnClientCommandExecuted="editorCommandExecuted"
                                            OnClientModeChange="OnClientModeChange"
                                            Content='<%# Bind("Contenuto2") %>'
                                            ToolsFile="~/Zeus/PageDesigner/RadEditor2.xml"
                                            LocalizationPath="~/App_GlobalResources"
                                            AllowScripts="true" RenderMode="Classic" ToolbarMode="Default" EnableViewState="False"
                                            Width="700px" Height="500px">
                                            <CssFiles>
                                                <telerik:EditorCssFile Value="~/asset/css/ZeusTypeFoundry.css" />
                                            </CssFiles>
                                        </telerik:RadEditor>

                                    </td>
                                </tr>
                            </asp:Panel>


                            <asp:Panel ID="ZMCF_Contenuto3" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_Contenuto3" runat="server" SkinID="FieldDescription" Text="Label">Contenuto Link Download</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <telerik:RadEditor
                                            Language="it-IT" ID="RadEditor3" runat="server"
                                            DocumentManager-DeletePaths="~/ZeusInc/PageDesigner/Documents"
                                            DocumentManager-SearchPatterns="*.*"
                                            DocumentManager-ViewPaths="~/ZeusInc/PageDesigner/Documents"
                                            DocumentManager-MaxUploadFileSize="52428800"
                                            DocumentManager-UploadPaths="~/ZeusInc/PageDesigner/Documents"
                                            FlashManager-DeletePaths="~/ZeusInc/PageDesigner/Media"
                                            FlashManager-MaxUploadFileSize="10240000"
                                            FlashManager-ViewPaths="~/ZeusInc/PageDesigner/Media"
                                            FlashManager-UploadPaths="~/ZeusInc/PageDesigner/Media"
                                            ImageManager-DeletePaths="~/ZeusInc/PageDesigner/Images"
                                            ImageManager-ViewPaths="~/ZeusInc/PageDesigner/Images"
                                            ImageManager-MaxUploadFileSize="10240000"
                                            ImageManager-SearchPatterns="*.gif, *.png, *.jpg, *.jpe, *.jpeg"
                                            ImageManager-UploadPaths="~/ZeusInc/PageDesigner/Images"
                                            ImageManager-ViewMode="Grid"
                                            MediaManager-DeletePaths="~/ZeusInc/PageDesigner/Media"
                                            MediaManager-MaxUploadFileSize="10240000"
                                            MediaManager-SearchPatterns="*.asf, *.asx, *.wma, *.wmv, *.avi, *.wav, *.mpeg, *.mpg, *.mpe, *.mov, *.mp3, *.m3u, *.mid, *.midi, *.rm, *.rma"
                                            MediaManager-ViewPaths="~/ZeusInc/PageDesigner/Media"
                                            MediaManager-UploadPaths="~/ZeusInc/PageDesigner/Media"
                                            TemplateManager-SearchPatterns="*.html,*.htm"
                                            ContentAreaMode="iframe"
                                            OnClientCommandExecuted="editorCommandExecuted"
                                            OnClientModeChange="OnClientModeChange"
                                            Content='<%# Bind("Contenuto3") %>'
                                            ToolsFile="~/Zeus/PageDesigner/RadEditor3.xml"
                                            LocalizationPath="~/App_GlobalResources"
                                            AllowScripts="true" RenderMode="Classic" ToolbarMode="Default" EnableViewState="False"
                                            Width="700px" Height="500px">
                                            <CssFiles>
                                                <telerik:EditorCssFile Value="~/asset/css/ZeusTypeFoundry.css" />
                                            </CssFiles>
                                        </telerik:RadEditor>

                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="ZMCF_Contenuto4" runat="server">
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label ID="ZML_Contenuto4" runat="server" SkinID="FieldDescription" Text="Label">Contenuto Fondo Blu</asp:Label>
                                    </td>
                                    <td class="BlockBoxValue">
                                        <telerik:RadEditor
                                            Language="it-IT" ID="RadEditor4" runat="server"
                                            DocumentManager-DeletePaths="~/ZeusInc/PageDesigner/Documents"
                                            DocumentManager-SearchPatterns="*.*"
                                            DocumentManager-ViewPaths="~/ZeusInc/PageDesigner/Documents"
                                            DocumentManager-MaxUploadFileSize="52428800"
                                            DocumentManager-UploadPaths="~/ZeusInc/PageDesigner/Documents"
                                            FlashManager-DeletePaths="~/ZeusInc/PageDesigner/Media"
                                            FlashManager-MaxUploadFileSize="10240000"
                                            FlashManager-ViewPaths="~/ZeusInc/PageDesigner/Media"
                                            FlashManager-UploadPaths="~/ZeusInc/PageDesigner/Media"
                                            ImageManager-DeletePaths="~/ZeusInc/PageDesigner/Images"
                                            ImageManager-ViewPaths="~/ZeusInc/PageDesigner/Images"
                                            ImageManager-MaxUploadFileSize="10240000"
                                            ImageManager-SearchPatterns="*.gif, *.png, *.jpg, *.jpe, *.jpeg"
                                            ImageManager-UploadPaths="~/ZeusInc/PageDesigner/Images"
                                            ImageManager-ViewMode="Grid"
                                            MediaManager-DeletePaths="~/ZeusInc/PageDesigner/Media"
                                            MediaManager-MaxUploadFileSize="10240000"
                                            MediaManager-SearchPatterns="*.asf, *.asx, *.wma, *.wmv, *.avi, *.wav, *.mpeg, *.mpg, *.mpe, *.mov, *.mp3, *.m3u, *.mid, *.midi, *.rm, *.rma"
                                            MediaManager-ViewPaths="~/ZeusInc/PageDesigner/Media"
                                            MediaManager-UploadPaths="~/ZeusInc/PageDesigner/Media"
                                            TemplateManager-SearchPatterns="*.html,*.htm"
                                            ContentAreaMode="iframe"
                                            OnClientCommandExecuted="editorCommandExecuted"
                                            OnClientModeChange="OnClientModeChange"
                                            Content='<%# Bind("Contenuto4") %>'
                                            ToolsFile="~/Zeus/PageDesigner/RadEditor4.xml"
                                            LocalizationPath="~/App_GlobalResources"
                                            AllowScripts="true" RenderMode="Classic" ToolbarMode="Default" EnableViewState="False"
                                            Width="700px" Height="500px">
                                            <CssFiles>
                                                <telerik:EditorCssFile Value="~/asset/css/ZeusTypeFoundry.css" />
                                            </CssFiles>
                                        </telerik:RadEditor>

                                    </td>
                                </tr>
                            </asp:Panel>

                        </table>
                    </div>
                </div>
                <asp:HiddenField ID="HiddenFieldRecordEdtUser" runat="server" Value='<%# Bind("RecordEdtUser") %>'
                    OnDataBinding="UtenteCreazione" />
                <asp:HiddenField ID="HiddenFieldRecordEdtDate" runat="server" Value='<%# Bind("RecordEdtDate") %>'
                    OnDataBinding="DataOggi" />
                <%--<uc1:PageDesignerLangButton ID="PageDesignerLangButton1" runat="server" ZeusId='<%# Eval("ZeusId") %>'
                ZeusIdModulo='<%# Eval("ZeusIdModulo") %>' ZeusLangCode='<%# Eval("ZeusLangCode") %>' />--%>
            </div>
            <br />
            <div align="center">
                <dlc:mySummaryValidation ID="mySummaryValidation1" runat="server" SkinID="ZSSM_Validazione01" />
                <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Update"
                    SkinID="ZSSM_Button01" Text="Salva dati" ValidationGroup="myValidation" OnClick="Save_File_Upload"></asp:LinkButton>
                <img src="../SiteImg/Spc.gif" width="20" />
                <asp:LinkButton ID="DeleteLinkButton" runat="server" Text="Elimina" SkinID="ZSSM_Button01"
                    OnClick="DeleteLinkButton_Click"></asp:LinkButton>
            </div>
            <div class="VertSpacerBig">
            </div>
            <dlc:zeusdatatracking ID="Zeusdatatracking1" runat="server" />
            <asp:HiddenField ID="zdtRecordNewUser" runat="server" Value='<%# Eval("RecordNewUser") %>' />
            <asp:HiddenField ID="zdtRecordNewDate" runat="server" Value='<%# Eval("RecordNewDate") %>' />
            <asp:HiddenField ID="zdtRecordEdtUser" runat="server" Value='<%# Eval("RecordEdtUser") %>' />
            <asp:HiddenField ID="zdtRecordEdtDate" runat="server" Value='<%# Eval("RecordEdtDate") %>' />
        </EditItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="dsDesignerNew" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SET DATEFORMAT dmy;  SELECT   ID1Pagina ,TitoloPagina, TitoloBrowser, Sottotitolo,Archivia, ID2ParentPage, 
        ID2Categoria, MenuSezione, Ordinamento1, TagDescription, TagKeywords, Contenuto1,Contenuto2, Contenuto3, Contenuto4,
        Immagine1,Immagine2,Immagine12Alt, Immagine3,Immagine4,Immagine34Alt, UrlRewrite,
        PW_Area1, PWI_Area1, PWF_Area1, PW_Area2, PWI_Area2, PWF_Area2,
        RecordNewUser, RecordNewDate, RecordEdtUser, RecordEdtDate,
        PW_Immagine1,DescBreve1 ,PW_MenuSezione,ZeusAssetType,TagMeta1Value,TagMeta1Content,
        TagMeta2Value,TagMeta2Content,TagMeta1Attribute,TagMeta2Attribute,ZeusJollyText,
        ZeusId,  ZeusIdModulo, ZeusLangCode   
        FROM tbPageDesigner WHERE ID1Pagina=@ID1Pagina AND ZeusIsAlive=1 
        AND ZeusIdModulo=@ZeusIdModulo AND ZeusLangCode=@ZeusLangCode"
        UpdateCommand=" SET DATEFORMAT dmy; UPDATE tbPageDesigner 
        SET TitoloPagina=@TitoloPagina, TitoloBrowser=@TitoloBrowser,
        Archivia=@Archivia, Sottotitolo=@Sottotitolo, ID2ParentPage=@ID2ParentPage, 
        ID2Categoria=@ID2Categoria, MenuSezione=@MenuSezione, Ordinamento1=@Ordinamento1, 
        TagDescription=@TagDescription, TagKeywords=@TagKeywords, Contenuto1=@Contenuto1,
        Contenuto2=@Contenuto2,Contenuto3=@Contenuto3, Contenuto4=@Contenuto4, Immagine1=@Immagine1, UrlRewrite=@UrlRewrite,
        PW_Area1=@PW_Area1, PWI_Area1=@PWI_Area1,
         PWF_Area1=@PWF_Area1, PW_Area2=@PW_Area2, PWI_Area2=@PWI_Area2, PWF_Area2=@PWF_Area2, 
         RecordEdtUser=@RecordEdtUser, RecordEdtDate=@RecordEdtDate,PW_Immagine1=@PW_Immagine1,Immagine2=@Immagine2,Immagine12Alt = @Immagine12Alt,
        Immagine3=@Immagine3,Immagine4=@Immagine4,Immagine34Alt = @Immagine34Alt,
        DescBreve1=@DescBreve1 ,PW_MenuSezione=@PW_MenuSezione,TagMeta1Value=@TagMeta1Value,TagMeta1Content=@TagMeta1Content,TagMeta2Value=@TagMeta2Value,
          TagMeta2Content=@TagMeta2Content,TagMeta1Attribute=@TagMeta1Attribute,TagMeta2Attribute=@TagMeta2Attribute,ZeusJollyText=@ZeusJollyText   
         WHERE ID1Pagina=@ID1Pagina"
        OnUpdated="dsDesignerNew_Updated" OnSelected="dsDesignerNew_Selected">
        <SelectParameters>
            <asp:QueryStringParameter Name="ID1Pagina" QueryStringField="XRI" Type="Int32" />
            <asp:QueryStringParameter Name="ZeusIdModulo" QueryStringField="ZIM" Type="String" />
            <asp:QueryStringParameter Name="ZeusLangCode" QueryStringField="Lang" Type="String"
                DefaultValue="ITA" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="TitoloPagina" Type="String" />
            <asp:Parameter Name="TitoloBrowser" Type="String" />
            <asp:Parameter Name="ID2ParentPage" Type="Int16" />
            <asp:Parameter Name="Contenuto1" Type="String" />
            <asp:Parameter Name="TagDescription" Type="String" />
            <asp:Parameter Name="TagKeywords" Type="String" />
            <asp:Parameter Name="PW_Area1" Type="Boolean" />
            <asp:Parameter Name="PWI_Area1" Type="DateTime" />
            <asp:Parameter Name="PWF_Area1" Type="DateTime" />
            <asp:Parameter Name="ZeusIsAlive" Type="Boolean" />
            <asp:Parameter Name="Archivia" Type="Boolean" />
            <asp:Parameter Name="ZeusId" />
            <asp:Parameter Name="ZeusAssetType" Type="String" />
            <asp:Parameter Name="RecordEdtUser" />
            <asp:Parameter Name="RecordEdtDate" Type="DateTime" />
            <asp:Parameter Name="MenuSezione" />
            <asp:Parameter Name="Ordinamento1" />
            <asp:Parameter Name="Contenuto2" Type="String" />
            <asp:Parameter Name="Contenuto3" Type="String" />
            <asp:Parameter Name="Contenuto4" Type="String" />
            <asp:Parameter Name="PW_Area2" Type="Boolean" />
            <asp:Parameter Name="PWI_Area2" Type="DateTime" />
            <asp:Parameter Name="PWF_Area2" Type="DateTime" />
            <asp:Parameter Name="ZeusIdModulo" />
            <asp:Parameter Name="Sottotitolo" Type="String" />
            <asp:Parameter Name="ID2Categoria" />
            <asp:Parameter Name="Immagine1" Type="String" />
            <asp:Parameter Name="PW_Immagine1" />
            <asp:Parameter Name="Immagine2" Type="String" />
            <asp:Parameter Name="Immagine12Alt" Type="String" />
            <asp:Parameter Name="Immagine3" Type="String" />
            <asp:Parameter Name="Immagine4" Type="String" />
            <asp:Parameter Name="Immagine34Alt" Type="String" />
            <asp:Parameter Name="UrlRewrite" Type="String" />
            <asp:Parameter Name="DescBreve1" Type="String" />
            <asp:Parameter Name="PW_MenuSezione" Type="Boolean" />
            <asp:Parameter Name="TagMeta1Value" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="TagMeta1Content" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="TagMeta2Value" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="TagMeta2Content" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="TagMeta1Attribute" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="TagMeta2Attribute" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="ZeusJollyText" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
