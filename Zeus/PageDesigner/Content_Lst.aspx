<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" Title="Untitled Page" CodeFile="Content_Lst.aspx.cs"
    Inherits="PageDesigner_Content_Lst"
    Theme="Zeus" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server"  />
    <asp:HiddenField ID="ZIM" runat="server"  />
   <asp:HiddenField ID="ZMCD_UrlSection" runat="server" />
    <asp:Panel ID="SingleRecordPanel" runat="server" Visible="false">
        <div class="LayGridTitolo1">
            <asp:Label ID="CaptionNewRecordLabel" SkinID="GridTitolo1" runat="server" Text="Ultimo contenuto creato / aggiornato"></asp:Label>
        </div>
        <asp:GridView ID="GridView2" runat="server" AllowPaging="True" AllowSorting="True"
            AutoGenerateColumns="False" PageSize="30" OnRowDataBound="GridView1_RowDataBound">
            <Columns>
                <asp:TemplateField HeaderText="Tipo" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="ID2ParentPage" runat="server" Text='<%# Eval("ID2ParentPage") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="AttivoArea1" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="AttivoArea1" runat="server" Text='<%# Eval("AttivoArea1") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="AttivoArea2" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="AttivoArea2" runat="server" Text='<%# Eval("AttivoArea2") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:BoundField DataField="ZeusAssetType" HeaderText="ZeusAssetType" SortExpression="ZeusAssetType" />
                <asp:BoundField DataField="TitoloPagina" HeaderText="Titolo" SortExpression="TitoloPagina" />
                <asp:BoundField DataField="SottoTitolo" HeaderText="Sottotitolo" SortExpression="SottoTitolo" />
                <asp:BoundField DataField="CatLiv3" HeaderText="Categoria" SortExpression="CatLiv3" />
                <asp:TemplateField HeaderText="Tipo" SortExpression="ID2ParentPage">
                    <ItemTemplate>
                        <asp:Image ID="ID2Tipo" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_PaginaPadre.gif" /></ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>
                <asp:BoundField DataField="TitoloPaginaParent" HeaderText="Titolo pagina padre" SortExpression="TitoloPaginaParent" />
                <asp:TemplateField HeaderText="Men�" SortExpression="PW_MenuSezione">
                    <ItemTemplate>
                        <asp:Image ID="PW_MenuSezioneImage" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Flag_Off.gif"
                            ToolTip='<%# Eval("PW_MenuSezione") %>' OnDataBinding="PW_MenuSezioneImage_DataBinding" /></ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>
                <asp:BoundField DataField="Ordinamento1" HeaderText="Ord." SortExpression="Ordinamento1"
                    ItemStyle-HorizontalAlign="Right" />
                <asp:BoundField DataField="MenuSezione" HeaderText="Men� navigaz." SortExpression="MenuSezione" />
                <asp:TemplateField HeaderText="AttivoArea" SortExpression="AttivoArea1">
                    <ItemTemplate>
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" /></ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="AttivoArea" SortExpression="AttivoArea2">
                    <ItemTemplate>
                        <asp:Image ID="Image2" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" /></ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>
               <asp:TemplateField HeaderText="Anteprima">
                <ItemTemplate>
                    <asp:HyperLink ID="AnteprimaLink" runat="server" Text="Anteprima" NavigateUrl='<%# Eval("AnteprimaLink") %>'
                        Target="_blank" CssClass="GridView_ZeusButton1"  OnDataBinding="AnteprimaLink_DataBinding"></asp:HyperLink>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
                <asp:HyperLinkField DataNavigateUrlFields="ID1Pagina,ZeusIdModulo,ZeusLangCode" DataNavigateUrlFormatString="/Zeus/PageDesigner/Content_Dtl.aspx?XRI={0}&amp;ZIM={1}&amp;Lang={2}"
                    HeaderText="Dettaglio" Text="Dettaglio" >
                    <ControlStyle CssClass="GridView_ZeusButton1" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:HyperLinkField>
                <asp:HyperLinkField DataNavigateUrlFields="ID1Pagina,ZeusIdModulo,ZeusLangCode" DataNavigateUrlFormatString="/Zeus/PageDesigner/Content_Edt.aspx?XRI={0}&amp;ZIM={1}&amp;Lang={2}"
                    HeaderText="Modifica" Text="Modifica" >
                    <ControlStyle CssClass="GridView_ZeusButton1" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:HyperLinkField>
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="dsPageDesignerLstSingleRecord" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
            SelectCommand="SET DATEFORMAT dmy; SELECT [ID1Pagina], [TitoloPagina],[SottoTitolo], [ZeusIdModulo], [ZeusAssetType], 
            [ID2Categoria], [CatLiv1Liv2Liv3], [CatLiv3], [ID2ParentPage], [TitoloPaginaParent], [AttivoArea1], 
            [AttivoArea2], [Ordinamento1], [ZeusLangCode],[ZeusID],[MenuSezione],[PW_MenuSezione],[ZeusAssetType],AnteprimaLink
            FROM [vwPageDesigner_Lst_All] "></asp:SqlDataSource>
        <div class="VertSpacerMedium">
        </div>
    </asp:Panel>
    <div class="LayGridTitolo1">
        <asp:Label ID="CaptionallRecordLabel" SkinID="GridTitolo1" runat="server" Text="Elenco contenuti disponibili"></asp:Label>
    </div>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" DataSourceID="dsPageDesignerLst" PageSize="30" OnRowDataBound="GridView1_RowDataBound">
        <Columns>
            <asp:TemplateField HeaderText="Tipo" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="ID2ParentPage" runat="server" Text='<%# Eval("ID2ParentPage") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="AttivoArea1" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="AttivoArea1" runat="server" Text='<%# Eval("AttivoArea1") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="AttivoArea2" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="AttivoArea2" runat="server" Text='<%# Eval("AttivoArea2") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ZeusAssetType" HeaderText="ZeusAssetType" SortExpression="ZeusAssetType" />
            <asp:BoundField DataField="TitoloPagina" HeaderText="Titolo" SortExpression="TitoloPagina" />
            <asp:BoundField DataField="SottoTitolo" HeaderText="Sottotitolo" SortExpression="SottoTitolo" />
            <asp:BoundField DataField="CatLiv3" HeaderText="Categoria" SortExpression="CatLiv3" />
            <asp:TemplateField HeaderText="Tipo" SortExpression="ID2ParentPage">
                <ItemTemplate>
                    <asp:Image ID="ID2Tipo" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_PaginaPadre.gif" /></ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:BoundField DataField="TitoloPaginaParent" HeaderText="Titolo pagina padre" SortExpression="TitoloPaginaParent" />
            <asp:TemplateField HeaderText="Men�" SortExpression="PW_MenuSezione">
                <ItemTemplate>
                    <asp:Image ID="PW_MenuSezioneImage" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Flag_Off.gif"
                        ToolTip='<%# Eval("PW_MenuSezione") %>' OnDataBinding="PW_MenuSezioneImage_DataBinding" /></ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:BoundField DataField="Ordinamento1" HeaderText="Ord." SortExpression="Ordinamento1"
                ItemStyle-HorizontalAlign="Right" />
            <asp:BoundField DataField="MenuSezione" HeaderText="Men� navigaz." SortExpression="MenuSezione" />
            <asp:TemplateField HeaderText="AttivoArea" SortExpression="AttivoArea1">
                <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" /></ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="AttivoArea" SortExpression="AttivoArea2">
                <ItemTemplate>
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Zeus/SiteImg/Ico1_Attivo_Off.gif" /></ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
               <asp:TemplateField HeaderText="Anteprima">
                <ItemTemplate>
                    <asp:HyperLink ID="AnteprimaLink" runat="server" Text="Anteprima" NavigateUrl='<%# Eval("AnteprimaLink") %>'
                        Target="_blank" CssClass="GridView_ZeusButton1" OnDataBinding="AnteprimaLink_DataBinding" ></asp:HyperLink>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:HyperLinkField DataNavigateUrlFields="ID1Pagina,ZeusIdModulo,ZeusLangCode" DataNavigateUrlFormatString="/Zeus/PageDesigner/Content_Dtl.aspx?XRI={0}&amp;ZIM={1}&amp;Lang={2}"
                HeaderText="Dettaglio" Text="Dettaglio" >
                <ControlStyle CssClass="GridView_ZeusButton1" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:HyperLinkField>
            <asp:HyperLinkField DataNavigateUrlFields="ID1Pagina,ZeusIdModulo,ZeusLangCode" DataNavigateUrlFormatString="/Zeus/PageDesigner/Content_Edt.aspx?XRI={0}&amp;ZIM={1}&amp;Lang={2}"
                HeaderText="Modifica" Text="Modifica" >
                <ControlStyle CssClass="GridView_ZeusButton1" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:HyperLinkField>
        </Columns>
        <EmptyDataTemplate>
            <div style="text-align: center;">
                Dati non presenti</div>
        </EmptyDataTemplate>
        <EmptyDataRowStyle BackColor="#FFFFFF" Height="200px" />
    </asp:GridView>
    <asp:SqlDataSource ID="dsPageDesignerLst" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        SelectCommand="SET DATEFORMAT dmy; SELECT ID1Pagina, TitoloPagina, Sottotitolo, ZeusIdModulo, ZeusAssetType, ID2Categoria,
         CatLiv1Liv2Liv3, CatLiv3, ID2ParentPage, TitoloPaginaParent, AttivoArea1, AttivoArea2, 
         Ordinamento1, ZeusLangCode, ZeusId, [MenuSezione],[PW_MenuSezione] ,ZeusAssetType,AnteprimaLink
         FROM vwPageDesigner_Lst_All WHERE ZeusIdModulo=@ZeusIdModulo AND ZeusLangCode=@ZeusLangCode">
         <SelectParameters>
         <asp:QueryStringParameter Name="ZeusIdModulo" QueryStringField="ZIM" Type="String" />
         <asp:QueryStringParameter Name="ZeusLangCode" QueryStringField="Lang" Type="String"  DefaultValue="ITA"/>
         </SelectParameters>
         </asp:SqlDataSource>
</asp:Content>
