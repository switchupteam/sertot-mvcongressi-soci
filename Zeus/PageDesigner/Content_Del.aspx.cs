﻿using System;
using System.Data.SqlClient;
using System.Web.Security;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per Content_Del
/// </summary>
public partial class PageDesigner_Content_Del : System.Web.UI.Page
{

    static string TitoloPagina = "Elimina pagina web";

    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;

        delinea myDelinea = new delinea();

        if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM"))
             || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI")))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        ZIM.Value = Server.HtmlEncode(Request.QueryString["ZIM"]);
        XRI.Value = Server.HtmlEncode(Request.QueryString["XRI"]);

        if (!ReadXML(ZIM.Value, GetLang()))
            Response.Redirect("~/Zeus/System/Message.aspx?1");

        ReadXML_Localization(ZIM.Value, GetLang());

        TitoloLabel.Text = GetPageTitle(XRI.Value);

    }//fine Page_Load

    private bool isOK()
    {
        try
        {
            if ((Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/PageDesigner/Content_Dtl.aspx") > 0)
                || (Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/PageDesigner/Content_Edt.aspx") > 0))
                return true;
            else return false;
        }
        catch (Exception)
        {
            return false;
        }

    }//fine isOK

    private string GetLang()
    {
        string Lang = "ITA";

        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;

    }//fine GetLang

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_PageDesigner.xml"));


            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {

                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {

                            if (childNode.Name.Equals("ZML_TitoloPagina_Del"))
                            {
                                TitleField.Value = childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                            }

                        }
                        catch (Exception)
                        {

                        }
                    }//fine foreach
                }//fine foreach

            }//fine for
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine ReadXML_Localization

    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_PageDesigner.xml"));



            if (!CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_Del").InnerText))
                Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223599");


            return true;
        }
        catch (Exception)
        {
            return false;
        }

    }//fine ReadXML

    private bool CheckPermit(string AllRoles)
    {

        bool IsPermit = false;

        try
        {

            if (AllRoles.Length == 0)
                return false;

            char[] delimiter = { ',' };


            foreach (string roles in Roles.GetRolesForUser(Membership.GetUser().UserName))
            {
                string[] PressRoles = AllRoles.Split(delimiter);

                for (int i = 0; i < PressRoles.Length; ++i)
                {
                    if (PressRoles[i].Trim().Equals(roles.Trim()))
                    {
                        IsPermit = true;
                        break;
                    }
                }//fine for

            }//fine else
        }
        catch (Exception)
        { }

        return IsPermit;

    }//fine CheckPermit

    private string GetPageTitle(string ID1Pagina)
    {

        string PageTitle = string.Empty;
        try
        {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            string sqlQuery = "SELECT TitoloPagina";
            sqlQuery += " FROM tbPageDesigner ";
            sqlQuery += " WHERE ID1Pagina=" + Server.HtmlEncode(ID1Pagina);
            //Response.Write(sqlQuery);

            using (SqlConnection conn = new SqlConnection(strConnessione))
            {
                SqlCommand command = new SqlCommand(sqlQuery, conn);

                conn.Open();
                SqlDataReader reader = command.ExecuteReader();
                /*fino a quando ci sono record*/


                while (reader.Read())
                    if (reader["TitoloPagina"] != DBNull.Value)
                        PageTitle = reader["TitoloPagina"].ToString();

                reader.Close();

            }//fine Using

        }
        catch (Exception p)
        {
            ErrorLabel.Text = p.ToString();
        }

        return PageTitle;

    }//fine GetPageTitle


    private bool DeleteRecord(string ID1Pagina)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                sqlQuery = "DELETE FROM tbPageDesigner WHERE ID1Pagina=" + Server.HtmlEncode(ID1Pagina);
                command.CommandText = sqlQuery;
                command.ExecuteNonQuery();
            }
            catch (Exception p)
            {

                ErrorLabel.Text = p.ToString();
                return false;
            }

        }//fine Using

        return true;

    }//fine DeleteRecord


    protected void EseguiButton_Click(object sender, EventArgs e)
    {
        if (ConfermaCheckBox.Checked)
        {
            if (DeleteRecord(Request.QueryString["XRI"]))
                Response.Redirect("~/Zeus/System/Message.aspx?Msg=12345957136223598");
        }
        else ErrorLabel.Text = "Per rimuovere il contenuto è necessario confermare l'eliminazione.";
    }//fine EseguiButton_Click

}