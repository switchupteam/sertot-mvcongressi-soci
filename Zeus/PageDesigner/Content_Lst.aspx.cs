﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per Content_Lst
/// </summary>
public partial class PageDesigner_Content_Lst : System.Web.UI.Page
{
    delinea myDelinea = new delinea();


    protected void Page_Load(object sender, EventArgs e)
    {

        if (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM"))
            Response.Redirect("/Zeus/System/Message.aspx?0");


        ZIM.Value = Server.HtmlEncode(Request.QueryString["ZIM"]);


        if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "TitoloPagina"))
            dsPageDesignerLst.SelectCommand += " AND TitoloPagina LIKE '%" + Request.QueryString["TitoloPagina"].ToString() + "%'";

        if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "IDCat3"))
        {
            if (!Request.QueryString["IDCat3"].Equals("0"))
                dsPageDesignerLst.SelectCommand += " AND ID2Categoria=" + Request.QueryString["IDCat3"].ToString();
        }

        if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "TYPE"))
        {
            if (Request.QueryString["TYPE"].Equals("1"))
                dsPageDesignerLst.SelectCommand += " AND ID2ParentPage = 0";
            else if (Request.QueryString["TYPE"].Equals("2"))
                dsPageDesignerLst.SelectCommand += " AND ID2ParentPage <> 0";
        }

        if (myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Cont"))
        {
            if (Request.QueryString["Cont"].Equals("1"))
                dsPageDesignerLst.SelectCommand += " AND AttivoArea1 = 1";
            else if (Request.QueryString["Cont"].Equals("2"))
                dsPageDesignerLst.SelectCommand += " AND AttivoArea1 = 0";
        }

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRE")) && (Request.QueryString["XRE"].Length > 0))
            dsPageDesignerLst.SelectCommand += " AND TitoloPagina LIKE '%" + Server.HtmlEncode(Request.QueryString["XRE"]) + "%'";

        string startDate = "01/01/2000";
        string endDate = "01/01/2050";


        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "DT1I")) && (Request.QueryString["DT1I"].Length > 0))
            startDate = Server.HtmlEncode(Request.QueryString["DT1I"]);

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "DT1F")) && (Request.QueryString["DT1F"].Length > 0))
            endDate = Server.HtmlEncode(Request.QueryString["DT1F"]);

        dsPageDesignerLst.SelectCommand += " AND CONVERT(datetime, CONVERT(varchar, RecordNewDate, 103)) BETWEEN '"
            + startDate
            + "' AND '" + endDate + "'";

        startDate = "01/01/2000";
        endDate = "01/01/2050";

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "DT2I")) && (Request.QueryString["DT2I"].Length > 0))
            startDate = Server.HtmlEncode(Request.QueryString["DT2I"]);

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "DT2F")) && (Request.QueryString["DT2F"].Length > 0))
            endDate = Server.HtmlEncode(Request.QueryString["DT2F"]);

        dsPageDesignerLst.SelectCommand += " AND CONVERT(datetime, CONVERT(varchar, RecordNewDate, 103)) BETWEEN '"
            + startDate
            + "' AND '" + endDate + "'";

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI1")) && (Request.QueryString["XRI1"].Length > 0))
            dsPageDesignerLst.SelectCommand += " AND ID2Categoria=" + Server.HtmlEncode(Request.QueryString["XRI1"]);

        dsPageDesignerLst.SelectCommand += " ORDER BY [TitoloPaginaParent] ASC , TitoloPagina ASC";

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI")) && (Request.QueryString["XRI"].Length > 0) && (isOK()))
        {
            dsPageDesignerLstSingleRecord.SelectCommand += " WHERE ID1Pagina='" + HttpUtility.HtmlEncode(Request.QueryString["XRI"].ToString()) + "'";
            GridView2.DataSourceID = "dsPageDesignerLstSingleRecord";
            GridView2.DataBind();
            SingleRecordPanel.Visible = true;
        }

        if (!ReadXML(ZIM.Value, GetLang()))
            Response.Redirect("~/Zeus/System/Message.aspx?1");

        ReadXML_Localization(ZIM.Value, GetLang());

    }

    private bool isOK()
    {
        try
        {
            if ((Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/PageDesigner/Content_Edt.aspx") > 0) || 
                (Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/PageDesigner/Content_New.aspx") > 0) || 
                (Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/Zeus/PageDesigner/Content_New_Lang.aspx") > 0))
                return true;
            else return false;
        }
        catch (Exception p)
        {
            return false;
        }
    }

    private string GetLang()
    {
        string Lang = "ITA";

        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;
    }

    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_PageDesigner.xml"));

            TitleField.Value = mydoc.SelectSingleNode(myXPath + "ZML_TitoloPagina_Lst").InnerText;

            GridView2.Columns[12].HeaderText = GridView1.Columns[12].HeaderText =
                   mydoc.SelectSingleNode(myXPath + "ZML_PWArea1_Lst").InnerText;

            GridView2.Columns[13].HeaderText = GridView1.Columns[13].HeaderText =
                   mydoc.SelectSingleNode(myXPath + "ZML_PWArea2_Lst").InnerText;

            GridView2.Columns[3].HeaderText = GridView1.Columns[3].HeaderText =
                mydoc.SelectSingleNode(myXPath + "ZML_DescrizioneZAT").InnerText;
        }
        catch (Exception p)
        {            
        }
    }

    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_PageDesigner.xml"));

            if (!CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_Lst").InnerText))
                Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223599");

            GridView1.Columns[14].Visible = GridView2.Columns[14].Visible =
                CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_Dtl").InnerText);
            GridView1.Columns[15].Visible = GridView2.Columns[15].Visible
                = CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_Edt").InnerText);

            GridView2.Columns[5].Visible = GridView1.Columns[5].Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Sottotitolo").InnerText);

            GridView2.Columns[7].Visible = GridView2.Columns[8].Visible =
                           GridView1.Columns[7].Visible = GridView1.Columns[8].Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_ParentPage").InnerText);

            GridView2.Columns[6].Visible = GridView1.Columns[6].Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Categoria").InnerText);

            GridView1.Columns[9].Visible = GridView1.Columns[10].Visible = GridView1.Columns[11].Visible =
                        GridView2.Columns[9].Visible = GridView2.Columns[10].Visible = GridView2.Columns[11].Visible
                            =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_MenuSezione").InnerText);

            GridView2.Columns[12].Visible = GridView1.Columns[12].Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxPlanner").InnerText);

            GridView2.Columns[13].Visible = GridView1.Columns[13].Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Area2_Lst").InnerText);

            GridView2.Columns[3].Visible = GridView1.Columns[3].Visible =
                Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_DescrizioneZAT").InnerText);

            //Nascondo pulsante anteprima
            GridView2.Columns[14].Visible = GridView1.Columns[14].Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_AnteprimaLst").InnerText);

            
            ZMCD_UrlSection.Value = mydoc.SelectSingleNode(myXPath + "ZMCD_UrlDomain").InnerText 
                + mydoc.SelectSingleNode(myXPath + "ZMCD_UrlSection").InnerText;

            return true;
        }
        catch (Exception p)
        {            
            return false;
        }
    }

    private bool CheckPermit(string AllRoles)
    {
        bool IsPermit = false;

        try
        {
            if (AllRoles.Length == 0)
                return false;

            char[] delimiter = { ',' };

            foreach (string roles in Roles.GetRolesForUser(Membership.GetUser().UserName))
            {
                string[] PressRoles = AllRoles.Split(delimiter);

                for (int i = 0; i < PressRoles.Length; ++i)
                {
                    if (PressRoles[i].Trim().Equals(roles.Trim()))
                    {
                        IsPermit = true;
                        break;
                    }
                }//fine for

            }//fine else
        }
        catch (Exception)
        { }

        return IsPermit;
    }

    protected void AnteprimaLink_DataBinding(object sender, EventArgs e)
    {
        HyperLink AnteprimaLink = (HyperLink)sender;
        AnteprimaLink.NavigateUrl = ZMCD_UrlSection.Value + AnteprimaLink.NavigateUrl;
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.DataItemIndex > -1)
        {
            Image Image2 = (Image)e.Row.FindControl("Image2");
            Label AttivoArea2 = (Label)e.Row.FindControl("AttivoArea2");

            Image Image1 = (Image)e.Row.FindControl("Image1");
            Label AttivoArea1 = (Label)e.Row.FindControl("AttivoArea1");

            Image ID2ParentPage = (Image)e.Row.FindControl("ID2Tipo");
            Label ID2Tipo = (Label)e.Row.FindControl("ID2ParentPage");

            try
            {
                int intAttivoArea1 = Convert.ToInt32(AttivoArea1.Text.ToString());
                if (intAttivoArea1 == 1)
                {
                    Image1.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";
                }

                int intAttivoArea2 = Convert.ToInt32(AttivoArea2.Text.ToString());
                if (intAttivoArea2 == 1)
                {
                    Image2.ImageUrl = "~/Zeus/SiteImg/Ico1_Attivo_On.gif";
                }

                int intID2Tipo = Convert.ToInt32(ID2Tipo.Text.ToString());
                if (intID2Tipo != 0)
                {
                    ID2ParentPage.ImageUrl = "~/Zeus/SiteImg/Ico1_PaginaFiglio.gif";
                }
            }
            catch (Exception)
            {
            }
        }
    }

    protected void PW_MenuSezioneImage_DataBinding(object sender, EventArgs e)
    {
        Image PW_MenuSezioneImage = (Image)sender;

        if (Convert.ToBoolean(PW_MenuSezioneImage.ToolTip))
            PW_MenuSezioneImage.ImageUrl = "~/Zeus/SiteImg/Ico1_Flag_On.gif";

        PW_MenuSezioneImage.ToolTip = string.Empty;
    }   
}