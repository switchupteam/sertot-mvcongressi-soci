﻿using ASP;
using CustomWebControls;
using System;
using System.Data.SqlClient;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

/// <summary>
/// Descrizione di riepilogo per Content_Edt
/// </summary>
public partial class PageDesigner_Content_Edt : System.Web.UI.Page {

    delinea myDelinea = new delinea();

    static string TitoloPagina = "Modifica pagina web";

    protected void Page_Load(object sender, EventArgs e) {
        TitleField.Value = TitoloPagina;

        if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM"))
            || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI")))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        ZIM.Value = Server.HtmlEncode(Request.QueryString["ZIM"]);
        XRI.Value = Server.HtmlEncode(Request.QueryString["XRI"]);

        if (!ReadXML(ZIM.Value, GetLang()))
            Response.Redirect("~/Zeus/System/Message.aspx?1");

        ReadXML_Localization(ZIM.Value, GetLang());




        try {

            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");
            InsertButton.Attributes.Add("onclick", "Validate()");

            string myJavascript = " " + BothRequired1();
            myJavascript += " " + BothRequired2();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartUpScript", myJavascript, true);


        }
        catch (Exception p) {
            Response.Write(p.ToString());
        }

    }//fine Page_Load



    private string BothRequired1() {

        string ClientID1 = FormView1.FindControl("TagMeta1ValueTextBox").ClientID;
        string ClientID2 = FormView1.FindControl("TagMeta1ContentTextBox").ClientID;


        string myJavascript = " function BothRequired1(sender ,args) ";
        myJavascript += " { ";
        myJavascript += " if(document.getElementById('" + ClientID1 + "').value.length>0) ";
        myJavascript += " if(document.getElementById('" + ClientID2 + "').value.length==0)";
        myJavascript += " { ";
        myJavascript += " args.IsValid=false; ";
        myJavascript += " return; ";
        myJavascript += " } ";

        myJavascript += " if(document.getElementById('" + ClientID2 + "').value.length>0) ";
        myJavascript += " if(document.getElementById('" + ClientID1 + "').value.length==0)";
        myJavascript += " { ";
        myJavascript += " args.IsValid=false; ";
        myJavascript += " return; ";
        myJavascript += " } ";
        myJavascript += " return; ";
        myJavascript += " } ";


        return myJavascript;

    }//fine BothRequired1

    private string BothRequired2() {

        string ClientID1 = FormView1.FindControl("TagMeta2ValueTextBox").ClientID;
        string ClientID2 = FormView1.FindControl("TagMeta2ContentTextBox").ClientID;


        string myJavascript = " function BothRequired2(sender ,args) ";
        myJavascript += " { ";
        myJavascript += " if(document.getElementById('" + ClientID1 + "').value.length>0) ";
        myJavascript += " if(document.getElementById('" + ClientID2 + "').value.length==0)";
        myJavascript += " { ";
        myJavascript += " args.IsValid=false; ";
        myJavascript += " return; ";
        myJavascript += " } ";

        myJavascript += " if(document.getElementById('" + ClientID2 + "').value.length>0) ";
        myJavascript += " if(document.getElementById('" + ClientID1 + "').value.length==0)";
        myJavascript += " { ";
        myJavascript += " args.IsValid=false; ";
        myJavascript += " return; ";
        myJavascript += " } ";
        myJavascript += " return; ";
        myJavascript += " } ";


        return myJavascript;

    }//fine BothRequired2

    private string GetIDParentCategory(string ID1Pagina) {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;
        string myReturn = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione)) {


            try {
                connection.Open();
                SqlCommand command = connection.CreateCommand();


                sqlQuery = "SELECT ID2Categoria FROM vwPageDesigner_ParentPages_Zeus1 WHERE ID1Pagina=@ID1Pagina";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID1Pagina", System.Data.SqlDbType.Int);
                command.Parameters["@ID1Pagina"].Value = ID1Pagina;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                    myReturn = reader.GetInt32(0).ToString();

                reader.Close();
            }
            catch (Exception p) {
                Response.Write(p.ToString());
            }
        }//fine Using


        return myReturn;

    }//fine GetIDParentCategory


    private bool ChangeAllChildsCategory(string ID2ParentPage, string ID2Categoria) {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione)) {


            try {
                connection.Open();
                SqlCommand command = connection.CreateCommand();


                sqlQuery = "UPDATE tbPageDesigner SET ID2Categoria=@ID2Categoria WHERE ID2ParentPage=@ID2ParentPage";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID2Categoria", System.Data.SqlDbType.Int);
                command.Parameters["@ID2Categoria"].Value = ID2Categoria;
                command.Parameters.Add("@ID2ParentPage", System.Data.SqlDbType.Int);
                command.Parameters["@ID2ParentPage"].Value = ID2ParentPage;

                command.ExecuteNonQuery();

                return true;
            }
            catch (Exception p) {
                Response.Write(p.ToString());
                return false;
            }
        }//fine Using

    }//fine CahngAllChildsCategory




    private string GetLang() {
        string Lang = "ITA";

        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;

    }//fine GetLang



    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode) {
        try {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_PageDesigner.xml"));


            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i) {

                foreach (System.Xml.XmlNode parentNode in nodelist) {
                    foreach (System.Xml.XmlNode childNode in parentNode) {
                        try {

                            if (childNode.Name.Equals("ZML_TitoloPagina_Edt")) {
                                TitleField.Value = childNode.InnerText;
                            }
                            else {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                            }

                        }
                        catch (Exception) {

                        }
                    }//fine foreach
                }//fine foreach

            }//fine for
        }
        catch (Exception p) {
            Response.Write(p.ToString());
        }

    }//fine ReadXML_Localization



    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode) {
        try {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_PageDesigner.xml"));



            if (!CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_Edt").InnerText))
                Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223599");


            //if (!CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_NewLang").InnerText))
            //{
            //    PageDesignerLangButton PageDesignerLangButton1 = (PageDesignerLangButton)FormView1.FindControl("PageDesignerLangButton1");
            //    PageDesignerLangButton1.Visible = false;
            //}

            Panel ZMCF_Sottotitolo = (Panel)FormView1.FindControl("ZMCF_Sottotitolo");

            if (ZMCF_Sottotitolo != null)
                ZMCF_Sottotitolo.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Sottotitolo").InnerText);



            Panel ZMCF_ParentPage = (Panel)FormView1.FindControl("ZMCF_ParentPage");

            if (ZMCF_ParentPage != null)
                ZMCF_ParentPage.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_ParentPage").InnerText);


            Panel ZMCF_Categoria = (Panel)FormView1.FindControl("ZMCF_Categoria");

            if (ZMCF_Categoria != null)
                ZMCF_Categoria.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Categoria").InnerText);


            Panel ZMCF_MenuSezione = (Panel)FormView1.FindControl("ZMCF_MenuSezione");

            if (ZMCF_MenuSezione != null)
                ZMCF_MenuSezione.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_MenuSezione").InnerText);

            Panel ZMCF_DescBreve1 = (Panel)FormView1.FindControl("ZMCF_DescBreve1");

            if (ZMCF_DescBreve1 != null)
                ZMCF_DescBreve1.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_DescBreve1").InnerText);

            Panel ZMCF_Contenuto1 = (Panel)FormView1.FindControl("ZMCF_Contenuto1");

            if (ZMCF_Contenuto1 != null)
                ZMCF_Contenuto1.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Contenuto1").InnerText);

            Panel ZMCF_Contenuto2 = (Panel)FormView1.FindControl("ZMCF_Contenuto2");

            if (ZMCF_Contenuto2 != null)
                ZMCF_Contenuto2.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Contenuto2").InnerText);

            Panel ZMCF_Contenuto3 = (Panel)FormView1.FindControl("ZMCF_Contenuto3");

            if (ZMCF_Contenuto3 != null)
                ZMCF_Contenuto3.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Contenuto3").InnerText);

            Panel ZMCF_Contenuto4 = (Panel)FormView1.FindControl("ZMCF_Contenuto4");

            if (ZMCF_Contenuto4 != null)
                ZMCF_Contenuto4.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Contenuto4").InnerText);


            Panel ZMCF_TagMeta1 = (Panel)FormView1.FindControl("ZMCF_TagMeta1");

            if (ZMCF_TagMeta1 != null)
                ZMCF_TagMeta1.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_TagMeta1").InnerText);


            Panel ZMCF_TagMeta2 = (Panel)FormView1.FindControl("ZMCF_TagMeta2");

            if (ZMCF_TagMeta2 != null)
                ZMCF_TagMeta2.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_TagMeta2").InnerText);


            Panel ZMCF_Immagine1 = (Panel)FormView1.FindControl("ZMCF_Immagine1");

            if (ZMCF_Immagine1 != null)
                ZMCF_Immagine1.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Immagine1").InnerText);

            Panel ZMCF_Immagine2 = (Panel)FormView1.FindControl("ZMCF_Immagine2");

            if (ZMCF_Immagine2 != null)
                ZMCF_Immagine2.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Immagine2").InnerText);


            Panel ZMCF_ZeusJollyText = (Panel)FormView1.FindControl("ZMCF_ZeusJollyText");

            if (ZMCF_ZeusJollyText != null)
                ZMCF_ZeusJollyText.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_ZeusJollyText").InnerText);

            Panel ZMCF_UrlRewrite = (Panel)FormView1.FindControl("ZMCF_UrlRewrite");

            if (ZMCF_UrlRewrite != null)
                ZMCF_UrlRewrite.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_UrlRewrite").InnerText);


            Panel ZMCF_BoxPlanner = (Panel)FormView1.FindControl("ZMCF_BoxPlanner");

            if (ZMCF_BoxPlanner != null)
                ZMCF_BoxPlanner.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxPlanner").InnerText);

            Panel ZMCF_BoxSEO = (Panel)FormView1.FindControl("ZMCF_BoxSEO");

            if (ZMCF_BoxSEO != null)
                ZMCF_BoxSEO.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxSEO").InnerText);

            Panel ZMCF_Area2 = (Panel)FormView1.FindControl("ZMCF_Area2");

            if (ZMCF_Area2 != null)
                ZMCF_Area2.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Area2").InnerText);

            if (!Page.IsPostBack)
                SetQueryOfID2CategoriaDropDownList(mydoc.SelectSingleNode(myXPath + "ZMCD_ZIMCategoria1").InnerText,
                    GetLang(), mydoc.SelectSingleNode(myXPath + "ZMCD_Cat1Liv").InnerText);

            TextBox ZMCD_TagDescription = (TextBox)FormView1.FindControl("ZMCD_TagDescription");

            if (ZMCD_TagDescription != null)
                ZMCD_TagDescription.Text = mydoc.SelectSingleNode(myXPath + "ZMCD_TagDescription").InnerText;

            TextBox ZMCD_TagKeywords = (TextBox)FormView1.FindControl("ZMCD_TagKeywords");

            if (ZMCD_TagKeywords != null)
                ZMCD_TagKeywords.Text = mydoc.SelectSingleNode(myXPath + "ZMCD_TagKeywords").InnerText;

            RadEditor RadEditor1 = (RadEditor)FormView1.FindControl("RadEditor1");
            if (RadEditor1 != null)
                RadEditor1.Width = Convert.ToInt32(mydoc.SelectSingleNode(myXPath + "ZMCD_ContEditor1_Width").InnerText);

            RadEditor RadEditor2 = (RadEditor)FormView1.FindControl("RadEditor2");
            if (RadEditor2 != null)
                RadEditor2.Width = Convert.ToInt32(mydoc.SelectSingleNode(myXPath + "ZMCD_ContEditor2_Width").InnerText);

            Label ZMCD_UrlSection = (Label)FormView1.FindControl("ZMCD_UrlSection");

            if (ZMCD_UrlSection != null)
                ZMCD_UrlSection.Text = mydoc.SelectSingleNode(myXPath + "ZMCD_UrlSection").InnerText;

            Label ZMCD_UrlDomain = (Label)FormView1.FindControl("ZMCD_UrlDomain");

            if (ZMCD_UrlDomain != null)
                ZMCD_UrlDomain.Text = mydoc.SelectSingleNode(myXPath + "ZMCD_UrlDomain").InnerText;


            return true;
        }
        catch (Exception) {
            return false;
        }

    }//fine ReadXML


    private bool CheckPermit(string AllRoles) {

        bool IsPermit = false;

        try {

            if (AllRoles.Length == 0)
                return false;

            char[] delimiter = { ',' };


            foreach (string roles in Roles.GetRolesForUser(Membership.GetUser().UserName)) {
                string[] PressRoles = AllRoles.Split(delimiter);

                for (int i = 0; i < PressRoles.Length; ++i) {
                    if (PressRoles[i].Trim().Equals(roles.Trim())) {
                        IsPermit = true;
                        break;
                    }
                }//fine for

            }//fine else
        }
        catch (Exception) { }

        return IsPermit;

    }//fine CheckPermit





    protected void UtenteCreazione(object sender, EventArgs e) {
        HiddenField UtenteCreazione = (HiddenField)sender;
        UtenteCreazione.Value = Membership.GetUser().ProviderUserKey.ToString();

    }

    protected void DataOggi(object sender, EventArgs e) {
        HiddenField DataCreazione = (HiddenField)sender;
        DataCreazione.Value = DateTime.Now.ToString();

    }




    protected void CreazioneGUID(object sender, EventArgs e) {
        HiddenField Guid = (HiddenField)sender;
        if (Guid.Value.Length == 0) {
            Guid.Value = System.Guid.NewGuid().ToString();
        }
    }

    protected void PWF_Area1TextBox_DataBinding(object sender, EventArgs e) {
        TextBox DataLimite = (TextBox)sender;
        if (DataLimite.Text.Length == 0) {
            DataLimite.Text = "31/12/2040";
        }
    }

    protected void PWI_Area1TextBox_DataBinding(object sender, EventArgs e) {
        DateTime Data = new DateTime();
        Data = DateTime.Now;
        TextBox DataCreazione = (TextBox)sender;
        if (DataCreazione.Text.Length == 0) {
            DataCreazione.Text = Data.ToString("d");
        }
    }







    private bool SetQueryOfID2CategoriaDropDownList(string ZeusIdModulo,
        string ZeusLangCode, string PZV_Cat1Liv) {
        try {


            SqlDataSource dsCategoria = (SqlDataSource)FormView1.FindControl("dsCategoria");
            DropDownList ID2CategoriaDropDownList = (DropDownList)FormView1.FindControl("ID2CategoriaDropDownList");
            HiddenField ID2CategoriaHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaHiddenField");
            //ID2CategoriaHiddenField.Value = "0";

            switch (PZV_Cat1Liv) {

                case "123":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv1Liv2Liv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "Catliv1Liv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;


                case "23":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[MenuLabel2]";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "MenuLabel2";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;

                default:
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;
            }


            ID2CategoriaDropDownList.SelectedIndex = ID2CategoriaDropDownList.Items.IndexOf(ID2CategoriaDropDownList.Items.FindByValue(ID2CategoriaHiddenField.Value));

            return true;
        }
        catch (Exception p) {
            //Response.Write(p.ToString());
            return false;
        }
    }//fine SetQueryOfID2CategoriaDropDownList

    protected void ID2CategoriaDropDownList_SelectIndexChange(object sender, EventArgs e) {
        HiddenField ID2CategoriaHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaHiddenField");
        DropDownList ID2CategoriaDropDownList = (DropDownList)sender;

        ID2CategoriaHiddenField.Value = ID2CategoriaDropDownList.SelectedValue;

    }//fine ID2CategoriaDropDownList_SelectIndexChange



    //##############################################################################################################
    //################################################ FILE UPLOAD #################################################
    //############################################################################################################## 


    protected void ImageRaider_DataBinding(object sender, EventArgs e) {
        ImageRaider ImageRaider1 = (ImageRaider)sender;
        ImageRaider1.SetDefaultEditBetaImage();
        ImageRaider1.SetDefaultEditGammaImage();

    }

    protected void FileUploadCustomValidator_ServerValidate(object sender, ServerValidateEventArgs args) {
        try {
            ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");
            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");

            if (ImageRaider1.isValid())
                InsertButton.CommandName = "Update";
            else
                InsertButton.CommandName = string.Empty;

        }
        catch (Exception p) {
            Response.Write(p.ToString());
        }

    }//fine FileUploadCustomValidator
    //##############################################################################################################
    //########################################### FINE FILE UPLOAD #################################################
    //##############################################################################################################


    protected void HiddenImageFile_DataBinding(object sender, EventArgs e) {
        HiddenField HiddenImageFile = (HiddenField)sender;
        if (HiddenImageFile.Value.Length == 0)
            HiddenImageFile.Value = "ImgNonDisponibile.jpg";

    }


    protected void HiddenIcona_DataBinding(object sender, EventArgs e) {
        TextBox ImgTextBox = (TextBox)sender;
        if (ImgTextBox.Text.Length == 0) {
            ImgTextBox.Text = "ImgNonDisponibile.jpg";
        }
    }


    protected void PaginaPadreDropDownList_DataBound(object sender, EventArgs e) {

        try {
            DropDownList PaginaPadreDropDownList = (DropDownList)sender;
            DropDownList ID2CategoriaDropDownList = (DropDownList)FormView1.FindControl("ID2CategoriaDropDownList");
            Panel ArchivioPanel = (Panel)FormView1.FindControl("ArchivioPanel");

            if (PaginaPadreDropDownList.SelectedValue.Equals("0")) {
                PaginaPadreDropDownList.Enabled = false;
                //ArchivioPanel.Visible = true;
            }
            else {
                ListItem li = PaginaPadreDropDownList.Items.FindByValue("0");
                if (li != null)
                    PaginaPadreDropDownList.Items.Remove(li);
                ID2CategoriaDropDownList.Visible = false;
                //ArchivioPanel.Visible = false;
            }
        }
        catch (Exception p) {

            Response.Write(p.ToString());
        }

    }//fine PaginaPadreDropDownList_DataBinding

    protected void DropDownList_SelectIndexChange(object sender, EventArgs e) {

        try {
            DropDownList DropDown = (DropDownList)sender;
            Panel ZMCF_MenuSezione = (Panel)FormView1.FindControl("ZMCF_MenuSezione");
            RequiredFieldValidator SezioneRequiredFieldValidator = (RequiredFieldValidator)FormView1.FindControl("SezioneRequiredFieldValidator");
            RequiredFieldValidator PosizioneRequiredFieldValidator = (RequiredFieldValidator)FormView1.FindControl("PosizioneRequiredFieldValidator");
            RangeValidator PosizioneRangeValidator = (RangeValidator)FormView1.FindControl("PosizioneRangeValidator");
            DropDownList ID2CategoriaDropDownList = (DropDownList)FormView1.FindControl("ID2CategoriaDropDownList");


            if (DropDown.SelectedValue.Equals("0")) {
                //ZMCF_MenuSezione.Visible = false;
                //SezioneRequiredFieldValidator.Enabled = false;
                //PosizioneRequiredFieldValidator.Enabled = false;
                //PosizioneRangeValidator.Enabled = false;
                ID2CategoriaDropDownList.Enabled = true;
            }
            else {
                ID2CategoriaDropDownList.Enabled = false;
                ID2CategoriaDropDownList.SelectedIndex = ID2CategoriaDropDownList.Items.IndexOf(ID2CategoriaDropDownList.Items.FindByValue(GetIDParentCategory(DropDown.SelectedValue)));
                HiddenField ID2CategoriaHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaHiddenField");
                ID2CategoriaHiddenField.Value = ID2CategoriaDropDownList.SelectedValue;
            }
        }
        catch (Exception p) {
            Response.Write(p.ToString());
        }

    }//fine DropDownList_SelectIndexChange

    protected void SezioneTextBoxCustomValidator_ServerValidate(object source, ServerValidateEventArgs args) {
        TextBox SezioneTextBox = (TextBox)FormView1.FindControl("SezioneTextBox");
        if (SezioneTextBox.Text.Length == 0)
            args.IsValid = false;

    }

    protected void PosizioneTextBoxCustomValidator_ServerValidate(object source, ServerValidateEventArgs args) {
        TextBox PosizioneTextBox = (TextBox)FormView1.FindControl("PosizioneTextBox");
        CustomValidator custom = (CustomValidator)source;
        if (PosizioneTextBox.Text.Length == 0) {
            args.IsValid = false;
            custom.ErrorMessage = "Obbligatorio - Inserire una posizione numerica";
        }
        else {
            try {
                int x = Convert.ToInt32(PosizioneTextBox.Text);
            }
            catch (Exception) {
                args.IsValid = false;
                custom.ErrorMessage = "Obbligatorio - Inserire una posizione numerica";
            }
        }

    }



    protected void DescrizioneCustomValidator_ServerValidate(object sender, ServerValidateEventArgs args) {
        TextArea DexcrizioneTextArea = (TextArea)FormView1.FindControl("DexcrizioneTextArea");

        if (DexcrizioneTextArea.Text.Length == 0)
            args.IsValid = false;
    }



    private bool ChangeChildAchive(string ID2ParentPage, bool Archivia) {

        SqlTransaction transaction = null;

        try {
            String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

            string sqlQuery = "UPDATE tbPageDesigner";
            sqlQuery += " SET Archivia=@Archivia WHERE ID2ParentPage=@ID2ParentPage";


            using (SqlConnection connection = new SqlConnection(strConnessione)) {

                connection.Open();
                SqlCommand command = connection.CreateCommand();

                transaction = connection.BeginTransaction();
                command.Transaction = transaction;

                command.CommandText = sqlQuery;

                command.Parameters.Add("@ID2ParentPage", System.Data.SqlDbType.Int);
                command.Parameters["@ID2ParentPage"].Value = ID2ParentPage;
                command.Parameters.Add("@Archivia", System.Data.SqlDbType.Bit);
                command.Parameters["@Archivia"].Value = Archivia;

                command.ExecuteNonQuery();

                transaction.Commit();

                return true;


            }//fine using


        }
        catch (Exception p) {
            Response.Write(p.ToString());

            if (transaction != null)
                transaction.Rollback();

            return false;
        }


    }//fine ChangeChildAchive



    protected void Save_File_Upload(object sender, EventArgs e) {
        try {
            HiddenField FileNameBetaHiddenField = (HiddenField)FormView1.FindControl("FileNameBetaHiddenField");
            HiddenField FileNameGammaHiddenField = (HiddenField)FormView1.FindControl("FileNameGammaHiddenField");
            HiddenField Immagine12AltHiddenField = (HiddenField)FormView1.FindControl("Immagine12AltHiddenField");
            ImageRaider ImageRaider1 = (ImageRaider)FormView1.FindControl("ImageRaider1");

            HiddenField FileNameBeta2HiddenField = (HiddenField)FormView1.FindControl("FileNameBeta2HiddenField");
            HiddenField FileNameGamma2HiddenField = (HiddenField)FormView1.FindControl("FileNameGamma2HiddenField");
            HiddenField Image34AltHiddenField = (HiddenField)FormView1.FindControl("Image34AltHiddenField");
            ImageRaider ImageRaider2 = (ImageRaider)FormView1.FindControl("ImageRaider2");

            LinkButton InsertButton = (LinkButton)FormView1.FindControl("InsertButton");

            if (InsertButton.CommandName != string.Empty) {

                if (ImageRaider1.GenerateBeta(string.Empty))
                    FileNameBetaHiddenField.Value = ImageRaider1.ImgBeta_FileName;
                else FileNameBetaHiddenField.Value = ImageRaider1.DefaultBetaImage;

                if (ImageRaider1.GenerateGamma(string.Empty))
                    FileNameGammaHiddenField.Value = ImageRaider1.ImgGamma_FileName;
                else FileNameGammaHiddenField.Value = ImageRaider1.DefaultGammaImage;

                if (ImageRaider2.GenerateBeta(string.Empty))
                    FileNameBeta2HiddenField.Value = ImageRaider2.ImgBeta_FileName;
                else FileNameBeta2HiddenField.Value = ImageRaider2.DefaultBetaImage;

                if (ImageRaider2.GenerateGamma(string.Empty))
                    FileNameGamma2HiddenField.Value = ImageRaider2.ImgGamma_FileName;
                else FileNameGamma2HiddenField.Value = ImageRaider2.DefaultGammaImage;

                DropDownList PaginaPadreDropDownList = (DropDownList)FormView1.FindControl("PaginaPadreDropDownList");
                DropDownList ID2CategoriaDropDownList = (DropDownList)FormView1.FindControl("ID2CategoriaDropDownList");

                if (PaginaPadreDropDownList.SelectedValue.Equals("0")) {
                    ChangeAllChildsCategory(Server.HtmlEncode(Request.QueryString["XRI"]), ID2CategoriaDropDownList.SelectedValue);

                    //CheckBox ArchivioCheckBox = (CheckBox)FormView1.FindControl("ArchivioCheckBox");
                    //ChangeChildAchive(Server.HtmlEncode(Request.QueryString["XRI"]), ArchivioCheckBox.Checked);
                }

                delinea myDelinea = new delinea();

                HiddenField UrlRewriteHiddenField = (HiddenField)FormView1.FindControl("UrlRewriteHiddenField");
                TextBox UrlPageDetailTextBox = (TextBox)FormView1.FindControl("UrlPageDetailTextBox");
                UrlRewriteHiddenField.Value = myDelinea.myHtmlEncode(UrlPageDetailTextBox.Text.TrimEnd());

            }//fine if command

        }
        catch (Exception p) {
            Response.Write(p.ToString());
        }
    }//fine Save_File_Upload    



    protected void dsDesignerNew_Selected(object sender, SqlDataSourceStatusEventArgs e) {
        if ((e.Exception != null)
             || (e.AffectedRows <= 0))
            Response.Redirect("~/Zeus/System/Message.aspx?SelErr");

    }//fine dsDesignerNew_Selected


    protected void dsDesignerNew_Updated(object sender, SqlDataSourceStatusEventArgs e) {
        if (e.Exception == null) {

            Response.Redirect("~/Zeus/PageDesigner/Content_Lst.aspx?XRI="
                + XRI.Value
                + "&ZIM=" + ZIM.Value
                + "&Lang=" + GetLang());
        }
        else Response.Redirect("~/Zeus/System/Message.aspx?UpdateErr");
    }//fine dsDesignerNew_Inserted




    protected void DeleteLinkButton_Click(object sender, EventArgs e) {
        Response.Redirect("~/Zeus/PageDesigner/Content_Del.aspx?XRI="
            + XRI.Value
            + "&Lang=" + GetLang()
            + "&ZIM=" + ZIM.Value);

    }//fine DeleteLinkButton_Click



}