﻿using ASP;
using System;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
/// <summary>
/// Descrizione di riepilogo per Content_Dtl
/// </summary>
public partial class PageDesigner_Content_Dtl : System.Web.UI.Page
{
    delinea myDelinea = new delinea();

    static string TitoloPagina = "Modifica pagina web";

    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = TitoloPagina;

        if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ZIM"))
            || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI")))
            Response.Redirect("~/Zeus/System/Message.aspx?0");

        ZIM.Value = Server.HtmlEncode(Request.QueryString["ZIM"]);
        XRI.Value = Server.HtmlEncode(Request.QueryString["XRI"]);
        ZID.Value = getZeusId();
        
        PhotoGallery_Associa PhotoGallery_Associa1 = (PhotoGallery_Associa)FormView1.FindControl("PhotoGallery_Associa1");
        PhotoGallery_Associa1.TitoloControlID = "TitoloPaginaTextBox";
        PhotoGallery_Associa1.ZeusIdModuloContenuto = ZIM.Value;
        PhotoGallery_Associa1.IDContenuto = XRI.Value;

        if (!ReadXML(ZIM.Value, GetLang()))
            Response.Redirect("~/Zeus/System/Message.aspx?1");

        ReadXML_Localization(ZIM.Value, GetLang());

        try
        {
            string myJavascript = CopyToClipBoard();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartUpScript", myJavascript, true);
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine Page_Load

    private string CopyToClipBoard()
    {
        string myJavascript = " function ClipBoard() ";
        myJavascript += " { ";
        myJavascript += " var holdtext =document.getElementById('";
        myJavascript += FormView1.FindControl("CopyHiddenField").ClientID;
        myJavascript += "'); ";
        myJavascript += " var copytext = document.getElementById('";
        myJavascript += FormView1.FindControl("ZMCD_UrlSection").ClientID;
        myJavascript += "'); ";
        myJavascript += " var copytext2 = document.getElementById('";
        myJavascript += FormView1.FindControl("ID1Pagina").ClientID;
        myJavascript += "'); ";
        myJavascript += " var copytext3 = document.getElementById('";
        myJavascript += FormView1.FindControl("UrlPageDetailLabel").ClientID;
        myJavascript += "'); ";
        myJavascript += " holdtext.value = copytext.innerHTML + copytext2.innerHTML + copytext3.innerHTML; ";
        myJavascript += " var Copied  = holdtext.createTextRange();";
        myJavascript += " Copied.select();";
        myJavascript += " Copied.execCommand('copy',false,null);";
        myJavascript += " return; ";
        myJavascript += " } ";


        return myJavascript;

    }//fine CopyToClipBoard

    private string getZeusId()
    {

        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                sqlQuery = "SELECT [ZeusId]";
                sqlQuery += " FROM [tbPageDesigner] ";
                sqlQuery += " WHERE [ID1Pagina]=@ID1Pagina";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ID1Pagina", System.Data.SqlDbType.Int);
                command.Parameters["@ID1Pagina"].Value = Server.HtmlEncode(XRI.Value.ToString());

                SqlDataReader reader = command.ExecuteReader();

                string myReturn = "";

                while (reader.Read())
                {
                    myReturn = reader["ZeusId"].ToString();
                }



                reader.Close();
                return myReturn;
            }
            catch (Exception p)
            {

                Response.Write(p.ToString());
                return "";
            }
        }//fine Using

    }//fine getZeusId

    private string GetLang()
    {
        string Lang = "ITA";

        delinea myDelinea = new delinea();

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang"))
          && (myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang")))
            Lang = Server.HtmlEncode(Request.QueryString["Lang"]);

        return Lang;

    }//fine GetLang



    private void ReadXML_Localization(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string Globalization = "it-IT";

            string myXPath = Globalization + "/" + ZeusIdModulo + "/" + ZeusLangCode;
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZML_PageDesigner.xml"));


            Label myLabel = null;
            delinea myDelinea = new delinea();
            System.Xml.XmlNodeList nodelist = mydoc.SelectNodes(myXPath);

            for (int i = 0; i < nodelist.Count; ++i)
            {

                foreach (System.Xml.XmlNode parentNode in nodelist)
                {
                    foreach (System.Xml.XmlNode childNode in parentNode)
                    {
                        try
                        {

                            if (childNode.Name.Equals("ZML_TitoloPagina_Dtl"))
                            {
                                TitleField.Value = childNode.InnerText;
                            }
                            else
                            {
                                myLabel = (Label)myDelinea.FindControlRecursive(Page, childNode.Name);

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText;
                            }

                        }
                        catch (Exception)
                        {

                        }
                    }//fine foreach
                }//fine foreach

            }//fine for
        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }

    }//fine ReadXML_Localization



    private bool ReadXML(string ZeusIdModulo, string ZeusLangCode)
    {
        try
        {
            string myXPath = "/MODULES/" + ZeusIdModulo + "/" + ZeusLangCode + "/";
            System.Xml.XmlDocument mydoc = new System.Xml.XmlDocument();
            mydoc.Load(Server.MapPath("ZMC_PageDesigner.xml"));

            LinkButton ModificaButton = (LinkButton)FormView1.FindControl("ModificaButton");

            if (ModificaButton != null)
                ModificaButton.Enabled = CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_Edt").InnerText);

            LinkButton DeleteLinkButton = (LinkButton)FormView1.FindControl("DeleteLinkButton");

            if (DeleteLinkButton != null)
                DeleteLinkButton.Enabled = CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_Del").InnerText);

            if (!CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_Dtl").InnerText))
                Response.Redirect("/Zeus/System/Message.aspx?Msg=12345957136223599");

            Panel ZMCF_BoxPhotoGallery = (Panel)FormView1.FindControl("ZMCF_BoxPhotoGallery");

            if (ZMCF_BoxPhotoGallery != null)
                ZMCF_BoxPhotoGallery.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxPhotoGallery").InnerText);

            Panel ZMCF_BoxPhotoGallerySimple = (Panel)FormView1.FindControl("ZMCF_BoxPhotoGallerySimple");

            if (ZMCF_BoxPhotoGallerySimple != null)
                ZMCF_BoxPhotoGallerySimple.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxPhotoGallerySimple").InnerText);

            Panel ZMCF_UrlRewrite = (Panel)FormView1.FindControl("ZMCF_UrlRewrite");

            if (ZMCF_UrlRewrite != null)
                ZMCF_UrlRewrite.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_UrlRewrite").InnerText);

            Panel ZMCF_Sottotitolo = (Panel)FormView1.FindControl("ZMCF_Sottotitolo");

            if (ZMCF_Sottotitolo != null)
                ZMCF_Sottotitolo.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Sottotitolo").InnerText);

            Panel ZMCF_ParentPage = (Panel)FormView1.FindControl("ZMCF_ParentPage");

            if (ZMCF_ParentPage != null)
                ZMCF_ParentPage.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_ParentPage").InnerText);

            Panel ZMCF_Categoria = (Panel)FormView1.FindControl("ZMCF_Categoria");

            if (ZMCF_Categoria != null)
                ZMCF_Categoria.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Categoria").InnerText);

            Panel ZMCF_MenuSezione = (Panel)FormView1.FindControl("ZMCF_MenuSezione");

            if (ZMCF_MenuSezione != null)
                ZMCF_MenuSezione.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_MenuSezione").InnerText);

            Panel ZMCF_DescBreve1 = (Panel)FormView1.FindControl("ZMCF_DescBreve1");

            if (ZMCF_DescBreve1 != null)
                ZMCF_DescBreve1.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_DescBreve1").InnerText);

            Panel ZMCF_Contenuto1 = (Panel)FormView1.FindControl("ZMCF_Contenuto1");

            if (ZMCF_Contenuto1 != null)
                ZMCF_Contenuto1.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Contenuto1").InnerText);

            Panel ZMCF_Contenuto2 = (Panel)FormView1.FindControl("ZMCF_Contenuto2");

            if (ZMCF_Contenuto2 != null)
                ZMCF_Contenuto2.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Contenuto2").InnerText);

            Panel ZMCF_Contenuto3 = (Panel)FormView1.FindControl("ZMCF_Contenuto3");

            if (ZMCF_Contenuto3 != null)
                ZMCF_Contenuto3.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Contenuto3").InnerText);

            Panel ZMCF_Contenuto4 = (Panel)FormView1.FindControl("ZMCF_Contenuto4");

            if (ZMCF_Contenuto4 != null)
                ZMCF_Contenuto4.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Contenuto4").InnerText);

            Panel ZMCF_TagMeta1 = (Panel)FormView1.FindControl("ZMCF_TagMeta1");

            if (ZMCF_TagMeta1 != null)
                ZMCF_TagMeta1.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_TagMeta1").InnerText);

            Panel ZMCF_TagMeta2 = (Panel)FormView1.FindControl("ZMCF_TagMeta2");

            if (ZMCF_TagMeta2 != null)
                ZMCF_TagMeta2.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_TagMeta2").InnerText);

            Panel ZMCF_Immagine1 = (Panel)FormView1.FindControl("ZMCF_Immagine1");

            if (ZMCF_Immagine1 != null)
                ZMCF_Immagine1.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Immagine1").InnerText);

            Panel ZMCF_Immagine2 = (Panel)FormView1.FindControl("ZMCF_Immagine2");

            if (ZMCF_Immagine2 != null)
                ZMCF_Immagine2.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Immagine2").InnerText);

            Panel ZMCF_ZeusJollyText = (Panel)FormView1.FindControl("ZMCF_ZeusJollyText");

            if (ZMCF_ZeusJollyText != null)
                ZMCF_ZeusJollyText.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_ZeusJollyText").InnerText);

            Panel ZMCF_BoxPlanner = (Panel)FormView1.FindControl("ZMCF_BoxPlanner");

            if (ZMCF_BoxPlanner != null)
                ZMCF_BoxPlanner.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxPlanner").InnerText);

            Panel ZMCF_BoxSEO = (Panel)FormView1.FindControl("ZMCF_BoxSEO");

            if (ZMCF_BoxSEO != null)
                ZMCF_BoxSEO.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_BoxSEO").InnerText);

            Panel ZMCF_Area2 = (Panel)FormView1.FindControl("ZMCF_Area2");

            if (ZMCF_Area2 != null)
                ZMCF_Area2.Visible =
                    Convert.ToBoolean(mydoc.SelectSingleNode(myXPath + "ZMCF_Area2").InnerText);

            if (!Page.IsPostBack)
                SetQueryOfID2CategoriaDropDownList(mydoc.SelectSingleNode(myXPath + "ZMCD_ZIMCategoria1").InnerText,
                    GetLang(), mydoc.SelectSingleNode(myXPath + "ZMCD_Cat1Liv").InnerText);

            TextBox ZMCD_TagDescription = (TextBox)FormView1.FindControl("ZMCD_TagDescription");

            if (ZMCD_TagDescription != null)
                ZMCD_TagDescription.Text = mydoc.SelectSingleNode(myXPath + "ZMCD_TagDescription").InnerText;

            TextBox ZMCD_TagKeywords = (TextBox)FormView1.FindControl("ZMCD_TagKeywords");

            if (ZMCD_TagKeywords != null)
                ZMCD_TagKeywords.Text = mydoc.SelectSingleNode(myXPath + "ZMCD_TagKeywords").InnerText;

            Label ZMCD_UrlSection = (Label)FormView1.FindControl("ZMCD_UrlSection");

            if (ZMCD_UrlSection != null)
                ZMCD_UrlSection.Text = mydoc.SelectSingleNode(myXPath + "ZMCD_UrlSection").InnerText;

            Label ZMCD_UrlDomain = (Label)FormView1.FindControl("ZMCD_UrlDomain");

            if (ZMCD_UrlDomain != null)
                ZMCD_UrlDomain.Text = mydoc.SelectSingleNode(myXPath + "ZMCD_UrlDomain").InnerText;

            PhotoGallery_Associa PhotoGallery_Associa1 = (PhotoGallery_Associa)FormView1.FindControl("PhotoGallery_Associa1");

            if (PhotoGallery_Associa1 != null)
                PhotoGallery_Associa1.ZIMGalleryAssociation =
                    mydoc.SelectSingleNode(myXPath + "ZMCD_ZIMGalleryAssociation").InnerText;

            if (!CheckPermit(mydoc.SelectSingleNode(myXPath + "ZMCD_PermitRoles_NewLang").InnerText))
            {
                PageDesignerLangButton PageDesignerLangButton1 = (PageDesignerLangButton)FormView1.FindControl("PageDesignerLangButton1");
                PageDesignerLangButton1.Visible = false;
            }

            return true;
        }
        catch (Exception)
        {
            return false;
        }

    }//fine ReadXML


    private bool CheckPermit(string AllRoles)
    {

        bool IsPermit = false;

        try
        {

            if (AllRoles.Length == 0)
                return false;

            char[] delimiter = { ',' };


            foreach (string roles in Roles.GetRolesForUser(Membership.GetUser().UserName))
            {
                string[] PressRoles = AllRoles.Split(delimiter);

                for (int i = 0; i < PressRoles.Length; ++i)
                {
                    if (PressRoles[i].Trim().Equals(roles.Trim()))
                    {
                        IsPermit = true;
                        break;
                    }
                }//fine for

            }//fine else
        }
        catch (Exception)
        { }

        return IsPermit;

    }//fine CheckPermit







    private bool SetQueryOfID2CategoriaDropDownList(string ZeusIdModulo,
        string ZeusLangCode, string PZV_Cat1Liv)
    {
        try
        {

            SqlDataSource dsCategoria = (SqlDataSource)FormView1.FindControl("dsCategoria");
            DropDownList ID2CategoriaDropDownList = (DropDownList)FormView1.FindControl("ID2CategoriaDropDownList");
            HiddenField ID2CategoriaHiddenField = (HiddenField)FormView1.FindControl("ID2CategoriaHiddenField");
            //ID2CategoriaHiddenField.Value = "0";

            switch (PZV_Cat1Liv)
            {

                case "123":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv1Liv2Liv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "Catliv1Liv2Liv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;


                case "23":
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[MenuLabel2]";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "MenuLabel2";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;

                default:
                    dsCategoria.SelectCommand = "SELECT [ID1Categoria],[CatLiv3] ";
                    dsCategoria.SelectCommand += " FROM [vwCategorie_Zeus1] ";
                    dsCategoria.SelectCommand += " WHERE [Livello] = 3 ";
                    dsCategoria.SelectCommand += "AND [ZeusIdModulo] = '" + Server.HtmlEncode(ZeusIdModulo) + "'";
                    dsCategoria.SelectCommand += " AND ZeusLangCode='" + Server.HtmlEncode(ZeusLangCode) + "'";
                    dsCategoria.SelectCommand += " ORDER BY [OrdLiv1], [OrdLiv2], [OrdLiv3]";
                    ID2CategoriaDropDownList.DataSourceID = "dsCategoria";
                    ID2CategoriaDropDownList.DataTextField = "CatLiv3";
                    ID2CategoriaDropDownList.DataValueField = "ID1Categoria";
                    ID2CategoriaDropDownList.DataBind();
                    break;
            }


            ID2CategoriaDropDownList.SelectedIndex = ID2CategoriaDropDownList.Items.IndexOf(ID2CategoriaDropDownList.Items.FindByValue(ID2CategoriaHiddenField.Value));

            Label ID2CategoriaLabel = (Label)FormView1.FindControl("ID2CategoriaLabel");
            ID2CategoriaLabel.Text = ID2CategoriaDropDownList.SelectedItem.Text;

            return true;
        }
        catch (Exception p)
        {
            // Response.Write(p.ToString());
            return false;
        }
    }//fine SetQueryOfID2CategoriaDropDownList


    //##############################################################################################################
    //################################################ FILE UPLOAD #################################################
    //############################################################################################################## 


    protected void ImageRaider_DataBinding(object sender, EventArgs e)
    {
        ImageRaider ImageRaider1 = (ImageRaider)sender;
        ImageRaider1.SetDefaultEditBetaImage();
        ImageRaider1.SetDefaultEditGammaImage();

    }
    protected void ImageRaiderCrop_DataBinding(object sender, EventArgs e)
    {
        ImageRaider ImageRaider1 = (ImageRaider)sender;
        ImageRaider1.SetDefaultEditBetaImage();
        ImageRaider1.SetDefaultEditGammaImage();

    }


    //##############################################################################################################
    //########################################### FINE FILE UPLOAD #################################################
    //##############################################################################################################


    protected void PaginaPadreDropDownList_DataBound(object sender, EventArgs e)
    {

        try
        {
            DropDownList PaginaPadreDropDownList = (DropDownList)sender;
            Label PaginaPadreLabel = (Label)FormView1.FindControl("PaginaPadreLabel");
            PaginaPadreLabel.Text = PaginaPadreDropDownList.SelectedItem.Text;
        }
        catch (Exception p)
        {

            Response.Write(p.ToString());
        }

    }//fine PaginaPadreDropDownList_DataBinding

    protected void ModificaLinkButton_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect("~/Zeus/PageDesigner/Content_Edt.aspx?XRI="
                + XRI.Value
                + "&ZIM=" + ZIM.Value
                + "&Lang=" + GetLang());

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
        }
    }//fine ModificaLinkButton_Click    

    protected void dsDesignerNew_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {
        if ((e.Exception != null)
            || (e.AffectedRows <= 0))
            Response.Redirect("~/Zeus/System/Message.aspx?SelErr");
    }//fine dsDesignerNew_Selected

    protected void DeleteLinkButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Zeus/PageDesigner/Content_Del.aspx?XRI="
            + Server.HtmlEncode(Request.QueryString["XRI"]) + "&Lang=" + GetLang()
            + "&ZIM=" + ZIM.Value);

    }//fine DeleteLinkButton_Click

    protected void PhotoGallery_Simple1_Init(object sender, EventArgs e)
    {
        Photo_Lst myGallery = (Photo_Lst)sender;
        myGallery.Lang = GetLang();
        myGallery.ZeusIdModulo = ZIM.Value.ToString();
        myGallery.XRI = XRI.Value.ToString();
        myGallery.ZID = ZID.Value.ToString();
    }
}