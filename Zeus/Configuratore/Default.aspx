﻿<%@ Page Language="C#" MasterPageFile="~/Zeus/SiteMaster/Zeus1.master" CodeFile="Default.aspx.cs" Inherits="Zeus_Configurator_Default" Theme="Zeus" EnableViewState="False" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ZeusContent" runat="Server">
    <asp:HiddenField ID="TitleField" runat="server" />

    <asp:PlaceHolder runat="server" ID="ContentSavedPlaceholder" Visible="false">
        <h3 class="LabelSkin_GridTitolo1" style="margin-bottom: 10px;">Contenuto salvato correttamente</h3>
    </asp:PlaceHolder>

    <asp:ListView runat="server" ID="ConfiguratoreListView" OnItemDataBound="ConfiguratoreListView_ItemDataBound">
        <ItemTemplate>

            <asp:PlaceHolder runat="server" ID="SingleSitePanel">
                <div style="border: 1px solid #ddd; background-color: #eee; padding: 10px; border-radius: 4px; margin-bottom: 20px;">

                    <asp:HiddenField runat="server" ID="RuoloHF" Value='<%# Eval("ruolo") %>' />
                    <asp:HiddenField runat="server" ID="MultilinguaHF" Value='<%# Eval("multilingua") %>' />

                    <h3 style="font-family: 'Raleway',sans-serif,Arial,Helvetica,sans-serif; font-size: 18px; color: #808080; margin-bottom: 10px;"><%# Eval("nome") %></h3>
                    <asp:HiddenField runat="server" ID="NomeProgettoHF" Value='<%# Bind("nome") %>' />

                    <%--IDENTIFICATIVO--%>
                    <div class="BlockBox">
                        <div class="BlockBoxHeader">
                            <asp:Label ID="IdentificativoLabel" runat="server">Dati</asp:Label>
                        </div>
                        <table>
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label runat="server" ID="NomeSitoLabel" SkinID="FieldDescription" Text="Nome sito *" />
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:TextBox runat="server" ID="NomeSitoTextBox" Columns="50" MaxLength="50" Text='<%# ((ConfiguratoreSitoWebData)Eval("Data")).NomeSito %>' />
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="NomeSitoTextBox" SkinID="ZSSM_Validazione01" Display="Dynamic"
                                        ErrorMessage="Obbligatorio" ValidationGroup="myValidation" />
                                </td>
                            </tr>
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label runat="server" ID="Label1" SkinID="FieldDescription" Text="URL *" />
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:TextBox runat="server" ID="URLTextBox" Columns="50" MaxLength="50" Text='<%# ((ConfiguratoreSitoWebData)Eval("Data")).URL %>' />
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ControlToValidate="URLTextBox" SkinID="ZSSM_Validazione01" Display="Dynamic"
                                        ErrorMessage="Obbligatorio" ValidationGroup="myValidation" />
                                </td>
                            </tr>
                        </table>
                    </div>

                    <%--HOME PAGE SEO ITA--%>
                    <div class="BlockBox">
                        <div class="BlockBoxHeader">
                            <asp:Label ID="HomePageSEOITA" runat="server">Home Page SEO ITA</asp:Label>
                        </div>
                        <table>
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label runat="server" ID="Label2" SkinID="FieldDescription" Text="Title *" />
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:TextBox runat="server" ID="SEOTitleITA" Columns="50" MaxLength="50" Text='<%# ((ConfiguratoreSitoWebHomePageSEO)Eval("HomePageSEO")).ITA.Title %>' />
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="SEOTitleITA" SkinID="ZSSM_Validazione01" Display="Dynamic"
                                        ErrorMessage="Obbligatorio" ValidationGroup="myValidation" />
                                </td>
                            </tr>
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label runat="server" ID="Label3" SkinID="FieldDescription" Text="Description" />
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:TextBox runat="server" ID="SEODescriptionITA" Columns="120" MaxLength="155" Text='<%# ((ConfiguratoreSitoWebHomePageSEO)Eval("HomePageSEO")).ITA.Description %>' />
                                </td>
                            </tr>
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label runat="server" ID="Label4" SkinID="FieldDescription" Text="Keywords" />
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:TextBox runat="server" ID="SEOKeywordsITA" Columns="120" MaxLength="155" Text='<%# ((ConfiguratoreSitoWebHomePageSEO)Eval("HomePageSEO")).ITA.Keywords %>' />
                                </td>
                            </tr>
                        </table>
                    </div>

                    <asp:PlaceHolder runat="server" ID="MultiLinguaPlaceholder">

                        <%--HOME PAGE SEO ENG--%>
                        <div class="BlockBox">
                            <div class="BlockBoxHeader">
                                <asp:Label ID="Label5" runat="server">Home Page SEO ENG</asp:Label>
                            </div>
                            <table>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label runat="server" ID="Label6" SkinID="FieldDescription" Text="Title *" />
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:TextBox runat="server" ID="SEOTitleENG" Columns="50" MaxLength="50" Text='<%# ((ConfiguratoreSitoWebHomePageSEO)Eval("HomePageSEO")).ENG.Title %>' />
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="SEOTitleENG" SkinID="ZSSM_Validazione01" Display="Dynamic"
                                            ErrorMessage="Obbligatorio" ValidationGroup="myValidation" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label runat="server" ID="Label7" SkinID="FieldDescription" Text="Description" />
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:TextBox runat="server" ID="SEODescriptionENG" Columns="120" MaxLength="155" Text='<%# ((ConfiguratoreSitoWebHomePageSEO)Eval("HomePageSEO")).ENG.Description %>' />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="BlockBoxDescription">
                                        <asp:Label runat="server" ID="Label8" SkinID="FieldDescription" Text="Keywords" />
                                    </td>
                                    <td class="BlockBoxValue">
                                        <asp:TextBox runat="server" ID="SEOKeywordsENG" Columns="120" MaxLength="155" Text='<%# ((ConfiguratoreSitoWebHomePageSEO)Eval("HomePageSEO")).ENG.Keywords %>' />
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </asp:PlaceHolder>

                    <%--MODULO CONTATTI--%>
                    <div class="BlockBox">
                        <div class="BlockBoxHeader">
                            <asp:Label ID="Label9" runat="server">Modulo contatti</asp:Label>
                        </div>
                        <table>
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label runat="server" ID="Label10" SkinID="FieldDescription" Text="Email *" ToolTip="Inserisci i destinatari separati da punto e virgola (Es. nome@dominio.it; altronome@altrodominio.com)" />
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:TextBox runat="server" ID="EmailContatti" Columns="50" MaxLength="50" Text='<%# ((ConfiguratoreSitoWebModuloContatti)Eval("ModuloContatti")).Email %>'
                                        ToolTip="Inserisci i destinatari separati da punto e virgola (Es. nome@dominio.it; altronome@altrodominio.com)" />
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="EmailContatti" SkinID="ZSSM_Validazione01" Display="Dynamic"
                                        ErrorMessage="Obbligatorio" ValidationGroup="myValidation" />
                                </td>
                            </tr>
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label runat="server" ID="Label11" SkinID="FieldDescription" Text="Nome *" />
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:TextBox runat="server" ID="NomeContatti" Columns="50" MaxLength="50" Text='<%# ((ConfiguratoreSitoWebModuloContatti)Eval("ModuloContatti")).Nome %>' />
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="NomeContatti" SkinID="ZSSM_Validazione01" Display="Dynamic"
                                        ErrorMessage="Obbligatorio" ValidationGroup="myValidation" />
                                </td>
                            </tr>
                            <tr>
                                <td class="BlockBoxDescription">
                                    <asp:Label runat="server" ID="Label12" SkinID="FieldDescription" Text="Oggetto *" />
                                </td>
                                <td class="BlockBoxValue">
                                    <asp:TextBox runat="server" ID="OggettoContatti" Columns="50" MaxLength="50" Text='<%# ((ConfiguratoreSitoWebModuloContatti)Eval("ModuloContatti")).Oggetto %>' />
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ControlToValidate="OggettoContatti" SkinID="ZSSM_Validazione01" Display="Dynamic"
                                        ErrorMessage="Obbligatorio" ValidationGroup="myValidation" />
                                </td>
                            </tr>
                        </table>
                    </div>

                </div>
            </asp:PlaceHolder>

        </ItemTemplate>
    </asp:ListView>

    <div style="text-align: center;">
        <asp:LinkButton runat="server" ID="InsertButton" SkinID="ZSSM_Button01" CausesValidation="True" Text="Salva dati" ValidationGroup="myValidation" OnClick="InsertButton_Click" />
    </div>

</asp:Content>
