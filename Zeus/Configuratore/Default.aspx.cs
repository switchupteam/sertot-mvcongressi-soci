﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

public partial class Zeus_Configurator_Default : System.Web.UI.Page
{
    protected Configuratore configuratore;
    private string XMLfilePath = "~/App_Data/configuratore.xml";
    private string[] roles = Roles.GetRolesForUser(Membership.GetUser().UserName);

    protected void Page_Load(object sender, EventArgs e)
    {
        TitleField.Value = "Configuratore";

        XmlSerializer serializer = new XmlSerializer(typeof(Configuratore));
        using (XmlReader reader = XmlReader.Create(Server.MapPath(XMLfilePath)))
        {
            configuratore = (Configuratore)serializer.Deserialize(reader);
        }

        ConfiguratoreListView.DataSource = configuratore.SitoWeb;
        ConfiguratoreListView.DataBind();
    }

    protected void InsertButton_Click(object sender, EventArgs e)
    {
        string configuratore = @"<?xml version=""1.0"" standalone=""yes""?>
                                    <Configuratore>";

        foreach (ListViewDataItem item in ConfiguratoreListView.Items)
        {
            HiddenField NomeProgettoHF = (HiddenField)item.FindControl("NomeProgettoHF");
            HiddenField RuoloHF = (HiddenField)item.FindControl("RuoloHF");
            HiddenField MultilinguaHF = (HiddenField)item.FindControl("MultilinguaHF");
            TextBox NomeSitoTextBox = (TextBox)item.FindControl("NomeSitoTextBox");
            TextBox URLTextBox = (TextBox)item.FindControl("URLTextBox");
            TextBox SEOTitleITA = (TextBox)item.FindControl("SEOTitleITA");
            TextBox SEODescriptionITA = (TextBox)item.FindControl("SEODescriptionITA");
            TextBox SEOKeywordsITA = (TextBox)item.FindControl("SEOKeywordsITA");
            TextBox SEOTitleENG = (TextBox)item.FindControl("SEOTitleENG");
            TextBox SEODescriptionENG = (TextBox)item.FindControl("SEODescriptionENG");
            TextBox SEOKeywordsENG = (TextBox)item.FindControl("SEOKeywordsENG");
            TextBox EmailContatti = (TextBox)item.FindControl("EmailContatti");
            TextBox NomeContatti = (TextBox)item.FindControl("NomeContatti");
            TextBox OggettoContatti = (TextBox)item.FindControl("OggettoContatti");

            configuratore += @"<SitoWeb nome=""" + NomeProgettoHF.Value + @""" ruolo=""" + RuoloHF.Value + @""" multilingua=""" + MultilinguaHF.Value.ToLower() + @""">
                                <Data>
                                    <NomeSito>" + NomeSitoTextBox.Text + @"</NomeSito>
                                    <URL>" + URLTextBox.Text + @"</URL>
                                </Data>
                                <HomePageSEO>
                                    <ITA>
                                        <Title>" + SEOTitleITA.Text + @"</Title>
                                        <Description>" + SEODescriptionITA.Text + @"</Description>
                                        <Keywords>" + SEOKeywordsITA.Text + @"</Keywords></ITA>
                                    <ENG>
                                        <Title>" + SEOTitleENG.Text + @"</Title>
                                        <Description>" + SEODescriptionENG.Text + @"</Description>
                                        <Keywords>" + SEOKeywordsENG.Text + @"</Keywords>
                                    </ENG>
                                </HomePageSEO>
                                <ModuloContatti>
                                    <Email>" + EmailContatti.Text + @"</Email>
                                    <Nome>" + NomeContatti.Text + @"</Nome>
                                    <Oggetto>" + OggettoContatti.Text + @"</Oggetto>
                                </ModuloContatti>
                            </SitoWeb>";
        }

        configuratore += "</Configuratore>";

        try
        {
            File.WriteAllText(Server.MapPath(XMLfilePath), configuratore);
            ContentSavedPlaceholder.Visible = true;
        }
        catch (Exception p)
        {
            Response.Redirect("/Zeus/System/Message.aspx");
        }

    }

    protected void ConfiguratoreListView_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            PlaceHolder SingleSitePanel = (PlaceHolder)e.Item.FindControl("SingleSitePanel");
            HiddenField RuoloHF = (HiddenField)e.Item.FindControl("RuoloHF");

            int position = Array.IndexOf(roles, "ZeusAdmin");
            if (position < 0)
            {
                position = Array.IndexOf(roles, RuoloHF.Value);
                if (position < 0)
                {
                    SingleSitePanel.Visible = false;
                }
            }

            PlaceHolder MultiLinguaPlaceholder = (PlaceHolder)SingleSitePanel.FindControl("MultiLinguaPlaceholder");
            HiddenField MultilinguaHF = (HiddenField)e.Item.FindControl("MultilinguaHF");

            if (!Convert.ToBoolean(MultilinguaHF.Value))
            {
                Label HomePageSEOITA = (Label)e.Item.FindControl("HomePageSEOITA");
                MultiLinguaPlaceholder.Visible = false;
                HomePageSEOITA.Text = "Home Page SEO";
            }
        }
    }
}