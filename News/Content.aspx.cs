﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.HtmlControls;

public partial class News_Content : System.Web.UI.Page
{
    protected string titolo = string.Empty;
    protected string descBreve = string.Empty;
    protected string contenuto1 = string.Empty;
    protected string contenuto2 = string.Empty;
    protected string immagine1 = string.Empty;
    protected string immagine1Alt = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Request.QueryString["XRI"]))
        {
            Response.Redirect("~/System/Message.aspx?0");
        }

        AresUtilities aresUtilities = new AresUtilities();
        string lang = aresUtilities.GetLangRewritted();

        string strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(strConnessione))
        {
            connection.Open();
            SqlCommand command = connection.CreateCommand();

                sqlQuery = @"SELECT ID1Publisher, Titolo, DescBreve1, Contenuto1, Contenuto2, Immagine1, Immagine12Alt, TitoloBrowser, TagDescription, TagKeywords, tbPZ_ImageRaider.ImgBeta_Path AS PathImmagine
                            FROM tbPublisher
	                            INNER JOIN tbPZ_ImageRaider on ZeusIdModuloIndice = ZeusIdModulo + '1'
                            WHERE ZeusIdModulo = 'PBNWS' AND tbPublisher.ZeusLangCode = @ZeusLangCode AND (PW_Area1 = 1 AND (GETDATE() BETWEEN PWI_Area1 AND PWF_Area1))
	                            AND ID1Publisher = @ID1Publisher";
                            
          
            command.CommandText = sqlQuery;
            command.Parameters.Add("@ID1Publisher", System.Data.SqlDbType.Int);
            command.Parameters["@ID1Publisher"].Value = Request.QueryString["XRI"];
            command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.NVarChar);
            command.Parameters["@ZeusLangCode"].Value = lang;

            SqlDataReader reader = command.ExecuteReader();

            if (reader.Read())
            {
                titolo = reader["Titolo"].ToString();
                descBreve = reader["DescBreve1"].ToString();
                contenuto1 = reader["Contenuto1"].ToString();
                contenuto2 = reader["Contenuto2"].ToString();
                immagine1 = reader["Immagine1"].ToString();

                if (!string.IsNullOrWhiteSpace(immagine1) && immagine1 != "ImgNonDisponibile.jpg")
                {
                    phImg.Visible = true;
                    immagine1 = reader["PathImmagine"].ToString().Replace("~", "") + immagine1;
                    immagine1Alt = reader["Immagine12Alt"].ToString();
                }


                /*****************************************************/
                /**************** INIZIO SEO *************************/
                /*****************************************************/
                HtmlMeta description = new HtmlMeta();
                HtmlMeta keywords = new HtmlMeta();
                description.Name = "description";
                keywords.Name = "keywords";
                Page.Title = reader["TitoloBrowser"].ToString() + " | " + AresConfig.NomeSito;
                description.Content = reader["TagDescription"].ToString();
                keywords.Content = reader["TagKeywords"].ToString();
                HtmlHead head = Page.Header;
                head.Controls.Add(description);
                head.Controls.Add(keywords);
                /*****************************************************/
                /**************** FINE SEO ***************************/
                /*****************************************************/

                reader.Close();
                connection.Close();
            }
            else
            {
                reader.Close();
                connection.Close();
                Response.Redirect("~/System/Message.aspx?0");
            }
        }
    }    
}