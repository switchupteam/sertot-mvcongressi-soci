﻿<%@ Page Language="C#" MasterPageFile="~/Master/LayoutMain.master" CodeFile="Content.aspx.cs" Inherits="News_Content" %>

<asp:Content ID="Content2" ContentPlaceHolderID="AreaTitle" runat="Server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="AreaContentMain" runat="Server">
    <section class="newsDettaglio">
        <div class="container">
            <div class="innerBox">
                <div class="firstTitle">
                    <h2 class="titolo"><%= titolo %></h2>
                    <hr class="left-position" />
                </div>

                <div class="row">
                    <asp:PlaceHolder runat="server" ID="phImg" Visible="false">
                        <div class="col-lg-5">
                            <div class="immagine">
                                <img src="<%= immagine1 %>" />
                            </div>
                        </div>
                    </asp:PlaceHolder>

                    <div class="col testo" runat="server">
                        <h6 class="descBreve"><%= descBreve %></h6>
                        <div class="richText">
                            <%= contenuto1 %>
                        </div>
                    </div>
                </div>

                <div class="richText">
                    <%= contenuto2 %>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
