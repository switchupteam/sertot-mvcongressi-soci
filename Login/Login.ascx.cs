﻿using System;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class Login_LoginUC : System.Web.UI.UserControl
{
    public string Lang
    {
        set { LangHiddenField.Value = value; }
        get { return LangHiddenField.Value; }
    }

    public string loginFallito = string.Empty;


    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();

        Button LoginButton = (Button)Login2.FindControl("LoginButton");
        mySetFocus(LoginButton.UniqueID);

        string myXPath = "PZ_Location/Lang[@id='" + LangHiddenField.Value.ToString().ToUpper() + "']/Record[@url='" + Request.ServerVariables["URL"].ToString().ToLower() + "']";
        ReadXml(Server.MapPath("~/App_Data/Login_Localizzazione.xml"), myXPath);

        HyperLink HyperLink1 = (HyperLink)Login2.FindControl("HyperLink1");

        //LoginButton.PostBackUrl = "~/" + LangHiddenField.Value.ToString().Substring(0, 2).ToLower() + "/Login/Login.aspx";
        HyperLink1.NavigateUrl = "~/" + LangHiddenField.Value.ToString().Substring(0, 2).ToLower() + "/Login/PasswordRecovery.aspx";
        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ReturnUrl")) && (Request.QueryString["ReturnUrl"].Length > 0))
        {
            Login2.DestinationPageUrl = "~/" + LangHiddenField.Value.ToString().Substring(0, 2).ToLower() + "/Login/Default.aspx?ReturnUrl=" + Request.QueryString["ReturnUrl"];
        }
        else
        {
            Login2.DestinationPageUrl = "~/" + LangHiddenField.Value.ToString().Substring(0, 2).ToLower() + "/Login/Default.aspx";
        }
    }

    private void mySetFocus(string ID)
    {
        Page.Form.DefaultButton = ID;
        Page.Form.DefaultFocus = ID;

    }

    private bool ReadXml(string FileName, string XPath)
    {
        try
        {

            delinea myDelinea = new delinea();
            Label myLabel = null;
            Button myButton = null;
            CheckBox myCheckBox = null;
            RequiredFieldValidator myRequiredFieldValidator = null;
            HiddenField myHiddenField = null;
            HyperLink myHyperLink = null;
            XmlDocument mydoc = new XmlDocument();
            mydoc.Load(FileName);

            XmlNodeList nodelist = mydoc.SelectNodes(XPath);
            for (int i = 0; i < nodelist.Count; ++i)
            {

                foreach (XmlNode parentNode in nodelist)
                {
                    foreach (XmlNode childNode in parentNode)
                    {
                        switch (childNode.Name)
                        {
                            case "LX01":
                                myCheckBox = (CheckBox)myDelinea.FindControlRecursive(this, "RememberMe");

                                if (myCheckBox != null)
                                    myCheckBox.Text = childNode.InnerText.ToString();

                                break;

                            case "LX02":
                                myButton = (Button)myDelinea.FindControlRecursive(this, "LoginButton");

                                if (myButton != null)
                                    myButton.Text = childNode.InnerText.ToString();

                                break;

                            case "LX03":

                                myLabel = (Label)myDelinea.FindControlRecursive(this, "InfoLabel");

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText.ToString();

                                break;

                            case "LX04":

                                myHyperLink = (HyperLink)myDelinea.FindControlRecursive(this, "HyperLink1");

                                if (myHyperLink != null)
                                    myHyperLink.Text = childNode.InnerText;


                                break;

                            case "LX05":

                                myRequiredFieldValidator = (RequiredFieldValidator)myDelinea.FindControlRecursive(this, "UserNameRequired");

                                if (myRequiredFieldValidator != null)
                                    myRequiredFieldValidator.ErrorMessage = childNode.InnerText.ToString();

                                break;

                            case "LX06":

                                myRequiredFieldValidator = (RequiredFieldValidator)myDelinea.FindControlRecursive(this, "PasswordRequired");

                                if (myRequiredFieldValidator != null)
                                    myRequiredFieldValidator.ErrorMessage = childNode.InnerText.ToString();

                                break;

                            case "LX07":

                                myHiddenField = (HiddenField)myDelinea.FindControlRecursive(this, "FailureTextHiddenField");

                                if (myHiddenField != null)
                                    myHiddenField.Value = childNode.InnerText.ToString();

                                break;

                            case "LX08":

                                myHiddenField = (HiddenField)myDelinea.FindControlRecursive(this, "FailureText2HiddenField");

                                if (myHiddenField != null)
                                    myHiddenField.Value = childNode.InnerText.ToString();

                                break;

                            case "LX09":

                                myHiddenField = (HiddenField)myDelinea.FindControlRecursive(this, "FailureText3HiddenField");

                                if (myHiddenField != null)
                                    myHiddenField.Value = childNode.InnerText.ToString();

                                break;

                            case "LX10":

                                myLabel = (Label)myDelinea.FindControlRecursive(this, "Info2Label");

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText.ToString();

                                break;

                            case "LX11":

                                myLabel = (Label)myDelinea.FindControlRecursive(Page, "TitoloPagina_Label");

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText.ToString();

                                Page.Title = childNode.InnerText.ToString() + " | " + AresConfig.NomeSito;

                                break;

                            case "LX12":

                                myLabel = (Label)myDelinea.FindControlRecursive(Page, "LoginTitle");

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText.ToString();

                                break;

                            case "LX13":

                                myLabel = (Label)myDelinea.FindControlRecursive(Page, "LoginSubTitle");

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText.ToString();

                                break;                               


                        }//fine switch

                    }//fine foreach

                }//fine foreach

                break;

            }//fine for

            return true;

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }


    }// fine ReadXml

    protected void Login2_LoginError(object sender, EventArgs e)
    {
        try
        {
            Label LoginErratoLabel = (Label)Login2.FindControl("LoginErratoLabel");
            LoginErratoLabel.Visible = false;

            TextBox UserName = (TextBox)Login2.FindControl("UserName");
            if ((Membership.GetUser(UserName.Text).IsApproved) && (Membership.GetUser(UserName.Text).IsLockedOut))
            {
                Login2.FailureText = FailureTextHiddenField.Value + UserName.Text + FailureText2HiddenField.Value;

                LoginErratoLabel.Text = Login2.FailureText;
                LoginErratoLabel.Visible = true;
            }
        }
        catch (Exception p)
        {
            Login2.FailureText = FailureText3HiddenField.Value;

            Label LoginErratoLabel = (Label)Login2.FindControl("LoginErratoLabel");
            LoginErratoLabel.Text = Login2.FailureText;
            LoginErratoLabel.Visible = true;
            //Response.Write(p.ToString());
        }


        //try
        //{
        //    TextBox UserName = (TextBox)Login2.FindControl("UserName");
        //    if ((Membership.GetUser(UserName.Text).IsApproved) && (Membership.GetUser(UserName.Text).IsLockedOut))
        //    {
        //        Login2.FailureText = FailureTextHiddenField.Value + UserName.Text + FailureText2HiddenField.Value;
        //        loginFallito = Login2.FailureText;
        //    }
        //}
        //catch (Exception p)
        //{
        //    Login2.FailureText = FailureText3HiddenField.Value;
        //    //Response.Write(p.ToString());
        //}

    } //fine Login2_LoginError

    protected void OnLoggedIn(object sender, EventArgs e)
    {

        //System.Web.UI.WebControls.Login loginControl = (System.Web.UI.WebControls.Login)sender;
        //String password = FormsAuthentication.HashPasswordForStoringInConfigFile(loginControl.Password, "md5");
        //Object userID = YAF.Classes.Data.DB.user_login(1, loginControl.UserName, password);

        //if (userID == DBNull.Value)
        //{
        //    YAF.Classes.Base.AdminPage adminPage = new YAF.Classes.Base.AdminPage();
        //    userID = YAF.Classes.Data.DB.user_register(adminPage, 1, loginControl.UserName,
        //        loginControl.Password, Membership.GetUser(loginControl.UserName).Email, "", "", "-300", false);
        //}

        //string idName = string.Format("{0};{1};{2}", userID, 1, loginControl.UserName);
        //FormsAuthentication.SetAuthCookie(idName, loginControl.RememberMeSet);
    }
}