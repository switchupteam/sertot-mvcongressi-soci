﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Login_Default" %>

<%@ Register Src="LoginRedirect.ascx" TagName="LoginRedirect" TagPrefix="uc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <uc1:LoginRedirect ID="LoginRedirect1" runat="server" />
        <br />
        <br />
        <br />
        Redirezione in corso...<br />
        <br />
        <a href="/<%= lang.Substring(0,2).ToLower() %>/Login/Login.aspx">&gt; Login &lt;</a>
    </form>
</body>
</html>
