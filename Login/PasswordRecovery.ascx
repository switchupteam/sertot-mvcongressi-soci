﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PasswordRecovery.ascx.cs" Inherits="Login_PasswordRecoveryUC" %>

<asp:HiddenField ID="LangHiddenField" runat="server" />

<asp:PasswordRecovery ID="PasswordRecovery1" runat="server"
    UserNameLabelText="UserName" UserNameInstructionText="Per ricevere la password nella casella e-mail, immettere lo UserName"
    UserNameRequiredErrorMessage="Lo UserName è obbligatorio." UserNameFailureText="Accesso aree riservate: utente bloccato (ad esempio, in seguito al superamento del numero massimo di tentativi di accesso permessi con password errata). Contattare il servizio utenti."
    OnSendingMail="PasswordRecovery1_SendingMail">
    <MailDefinition
        IsBodyHtml="true">
    </MailDefinition>
    <ValidatorTextStyle CssClass="HIC_LabelValidazione1" />
    <UserNameTemplate>
        <div style="margin: 0 0 10px 0;">

            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left">
                        <asp:Label ID="RecuperoLabel" runat="server" Text="Recupero password"
                            CssClass="HIC_LabelTitolo1"></asp:Label></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Info2Label" runat="server" Text="Inserire lo UserName per ricevere la password nella casella E-Mail associata all'account utente." ></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName" Text="UserName * "
                            CssClass="HIC_LabelFieldValue1"></asp:Label>
                        <asp:TextBox ID="UserName" runat="server" CssClass="HIC_TextBox1" Width="200px"></asp:TextBox>&nbsp;
                    <asp:Button ID="SubmitButton" runat="server" CommandName="Submit" Text="Recupera"
                        ValidationGroup="PasswordRecovery1" CssClass="HIC_Button1" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="HIC_LabelValidazione1" style="color: #FF0000;">
                            <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                        <asp:RequiredFieldValidator CssClass="HIC_Validator1" ID="UserNameRequired" runat="server"
                            ControlToValidate="UserName" ErrorMessage="Lo UserName è obbligatorio."
                            ValidationGroup="PasswordRecovery1" Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
        </div>
    </UserNameTemplate>
</asp:PasswordRecovery>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>
            <asp:Label ID="OppureLabel" runat="server" CssClass="HIC_LabelTitolo1" Font-Bold="true"
                Text="oppure"></asp:Label>
        </td>
    </tr>
    <tr>
        <td align="left">&nbsp;</td>
    </tr>
    <tr>
        <td align="left">
            <asp:Label ID="Info1Label" runat="server" Text="Recupero password e UserName"
                CssClass="HIC_LabelTitolo1"></asp:Label></td>
    </tr>
    <tr>
        <td align="left">
            <asp:Label ID="InfoEmail2Label" runat="server" 
                Text="Se non si ricordano nè la Password nè lo UserName, è possibile recuperari entrambi indicando l’indirizzo E-Mail associato all'account utente."></asp:Label>
        </td>
    </tr>
    <tr>
        <td align="left">
            <asp:Label ID="EmailLabel" CssClass="HIC_LabelFieldValue1"
                runat="server">Indirizzo E-mail *</asp:Label>
            <asp:TextBox ID="EmailTextBox" runat="server" CssClass="HIC_TextBox1" Width="250px"></asp:TextBox>&nbsp;
                            <asp:Button ID="SubmitEmailButton" ValidationGroup="PasswordRecovery2" runat="server"
                                Text="Recupera" CssClass="HIC_Button1" OnClick="SubmitButton_Click" /></td>
    </tr>
    <tr>
        <td align="left" style="color: red">
            <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="EmailTextBox"
                ErrorMessage="L'indirizzo E-mail è obbligatorio." ToolTip="L'indirizzo E-Mail è obbligatorio."
                Display="Dynamic" CssClass="HIC_Validator1" ValidationGroup="PasswordRecovery2"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="MailRegulaeExpressionValidation" runat="server"
                Display="Dynamic" ValidationExpression="^((([a-z]|[0-9]|!|#|$|%|&|'|\*|\+|\-|/|=|\?|\^|_|`|\{|\||\}|~)+(\.([a-z]|[0-9]|!|#|$|%|&|'|\*|\+|\-|/|=|\?|\^|_|`|\{|\||\}|~)+)*)@((((([a-z]|[0-9])([a-z]|[0-9]|\-){0,61}([a-z]|[0-9])\.))*([a-z]|[0-9])([a-z]|[0-9]|\-){0,61}([a-z]|[0-9])\.(af|ax|al|dz|as|ad|ao|ai|aq|ag|ar|am|aw|au|at|az|bs|bh|bd|bb|by|be|bz|bj|bm|bt|bo|ba|bw|bv|br|io|bn|bg|bf|bi|kh|cm|ca|cv|ky|cf|td|cl|cn|cx|cc|co|km|cg|cd|ck|cr|ci|hr|cu|cy|cz|dk|dj|dm|do|ec|eg|sv|gq|er|ee|et|fk|fo|fj|fi|fr|gf|pf|tf|ga|gm|ge|de|gh|gi|gr|gl|gd|gp|gu|gt| gg|gn|gw|gy|ht|hm|va|hn|hk|hu|is|in|id|ir|iq|ie|im|il|it|jm|jp|je|jo|kz|ke|ki|kp|kr|kw|kg|la|lv|lb|ls|lr|ly|li|lt|lu|mo|mk|mg|mw|my|mv|ml|mt|mh|mq|mr|mu|yt|mx|fm|md|mc|mn|ms|ma|mz|mm|na|nr|np|nl|an|nc|nz|ni|ne|ng|nu|nf|mp|no|om|pk|pw|ps|pa|pg|py|pe|ph|pn|pl|pt|pr|qa|re|ro|ru|rw|sh|kn|lc|pm|vc|ws|sm|st|sa|sn|cs|sc|sl|sg|sk|si|sb|so|za|gs|es|lk|sd|sr|sj|sz|se|ch|sy|tw|tj|tz|th|tl|tg|tk|to|tt|tn|tr|tm|tc|tv|ug|ua|ae|gb|us|um|uy|uz|vu|ve|vn|vg|vi|wf|eh|ye|zm|zw|com|edu|gov|int|mil|net|org|biz|info|name|pro|aero|coop|museum|arpa))|(((([0-9]){1,3}\.){3}([0-9]){1,3}))|(\[((([0-9]){1,3}\.){3}([0-9]){1,3})\])))$"
                ControlToValidate="EmailTextBox" ErrorMessage="Indirizzo E-Mail non corretto"
                CssClass="HIC_Validator1" ValidationGroup="PasswordRecovery2"></asp:RegularExpressionValidator>
            <asp:CustomValidator ID="EmailCustomValidator" runat="server" ValidationGroup="PasswordRecovery2"
                ErrorMessage="Indirizzo E-Mail non presente" Display="Dynamic"></asp:CustomValidator>
        </td>
    </tr>
</table>
<asp:Label ID="InfoLabel" runat="server" Visible="false" CssClass="HIC_LabelValidazione1"></asp:Label>
