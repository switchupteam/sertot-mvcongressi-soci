﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Login_Default : System.Web.UI.Page
{
    protected string lang = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AresUtilities myAresUtilities = new AresUtilities();
            lang = myAresUtilities.GetLangRewritted();
            LoginRedirect1.Lang = lang;
        }
    }
}