﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Login_LoginRedirect : System.Web.UI.UserControl
{
    public string Lang { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();

        if (!ThereIsUserLoggedIn())
        {
            Response.Redirect("~/Login/Login.aspx");
        }

        if ((myDelinea.AntiSQLInjectionLeft(Request.QueryString, "ReturnUrl")) && (Request.QueryString["ReturnUrl"].Length > 0)
            && CanUserGo(Membership.GetUser().ProviderUserKey.ToString(), Request.QueryString["ReturnUrl"]))
        {
            Response.Redirect(Request.ServerVariables["QUERY_STRING"].Replace("ReturnUrl=", "~"));
        }
        
        string path = getParentID(Membership.GetUser().ProviderUserKey.ToString());

        if (path.Length > 0)
        {
            Response.Redirect(path);
        }
    }

    private bool ThereIsUserLoggedIn()
    {
        MembershipUser loggedUser = Membership.GetUser();
        if (loggedUser != null) { return true; }
        else { return false; }
    }

    private bool CanUserGo(string UserID, string ReturnPath)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        bool myReturn = false;

        using (SqlConnection connection = new SqlConnection(strConnessione))
        {
            connection.Open();
            SqlCommand command = connection.CreateCommand();

            string sqlQuery = @"SELECT TOP 1 RoleName, DefaultHome FROM vwLoginRedirect WHERE UserId = @UserId ORDER BY SelectOrder ASC";
            command.CommandText = sqlQuery;
            command.Parameters.Add("@UserId", System.Data.DbType.String);
            command.Parameters["@UserId"].Value = UserID;

            SqlDataReader reader = command.ExecuteReader();

            string RoleName = string.Empty;
            string DefaultHome = string.Empty;

            while (reader.Read())
            {
                RoleName = reader["RoleName"].ToString();
                DefaultHome = reader["DefaultHome"].ToString();
            }

            if ((RoleName.Equals("ZeusAdmin") || ReturnPath.IndexOf(DefaultHome) > -1))
                myReturn = true;

            reader.Close();
        }
        return myReturn;
    }

    private string getParentID(string UserID)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string path = string.Empty;

        using (SqlConnection connection = new SqlConnection(strConnessione))
        {
            connection.Open();
            SqlCommand command = connection.CreateCommand();

            string sqlQuery = @"SELECT TOP 1 DefaultHome FROM vwLoginRedirect WHERE UserId = @UserId ORDER BY SelectOrder ASC";
            command.CommandText = sqlQuery;
            command.Parameters.Add("@UserId", System.Data.DbType.String);
            command.Parameters["@UserId"].Value = UserID;

            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                path = reader["DefaultHome"].ToString();
            }

            reader.Close();
        }
        return path;
    }
}