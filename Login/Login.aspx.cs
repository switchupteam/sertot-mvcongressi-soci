﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Login_Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AresUtilities myAresUtilities = new AresUtilities();
        Login1.Lang = myAresUtilities.GetLangRewritted();

        Page.Title = "Accesso area riservata | " + AresConfig.NomeSito;
    }
}