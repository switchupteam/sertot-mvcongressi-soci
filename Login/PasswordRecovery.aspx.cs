﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Login_PasswordRecovery : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AresUtilities myAresUtilities = new AresUtilities();
        string myLang = myAresUtilities.GetLangRewritted();

        PSW01_PasswordRecovery1.Lang = myLang;

        Page.Title = "Recupero password | " + AresConfig.NomeSito;

        switch (myLang)
        {
            case "ENG":
                Page.Title = "Password recovery | " + AresConfig.NomeSito;
                break;

            case "FRA":
                Page.Title = "Récupération du mot de passe | " + AresConfig.NomeSito;
                break;

            case "SPA":
                Page.Title = "Recuperación de contraseña | " + AresConfig.NomeSito;
                break;

            case "TED":
                Page.Title = "Kennwort erholung | " + AresConfig.NomeSito;
                break;
        }

    }
}