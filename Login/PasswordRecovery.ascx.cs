﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class Login_PasswordRecoveryUC : System.Web.UI.UserControl
{
    public string Lang
    {
        set { LangHiddenField.Value = value; }
        get { return LangHiddenField.Value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {




        if (!Page.IsPostBack)
        {
            try
            {
                delinea myDelinea = new delinea();


                //if (Request.QueryString["Lang"] != null && !myDelinea.AntiSQLInjectionRight(Request.QueryString, "Lang"))
                //    Response.Redirect("~/");


                string Lang = LangHiddenField.Value;

                PasswordRecovery1.SuccessPageUrl = "/" + LangHiddenField.Value.ToString().Substring(0, 2).ToLower() + "/System/Message.aspx?Msg=7867868456784588";



                Button SubmitButton = (Button)myDelinea.FindControlRecursive(Page, "SubmitButton");
                mySetFocus(SubmitButton.UniqueID);

                string myXPath = "PZ_Location/Lang[@id='" + Lang.ToUpper() + "']/Record[@url='" + Request.ServerVariables["URL"].ToString().ToLower() + "']";
                ReadXml(Server.MapPath("~/App_Data/PasswordRecovery_Localizzazione.xml"), myXPath);
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
            }
        }//fine if

    }//fine Page_Load

    private void mySetFocus(string ID)
    {
        Page.Form.DefaultButton = ID;
        Page.Form.DefaultFocus = ID;

    }//fine mymySetFocus





    private bool ReadXml(string FileName, string XPath)
    {
        try
        {

            delinea myDelinea = new delinea();
            Label myLabel = null;
            Button myButton = null;
            RequiredFieldValidator myRequiredFieldValidator = null;
            XmlDocument mydoc = new XmlDocument();
            mydoc.Load(FileName);

            XmlNodeList nodelist = mydoc.SelectNodes(XPath);
            for (int i = 0; i < nodelist.Count; i++)
            {

                foreach (XmlNode parentNode in nodelist)
                {
                    foreach (XmlNode childNode in parentNode)
                    {
                        switch (childNode.Name)
                        {
                            case "LX01":
                                myLabel = (Label)myDelinea.FindControlRecursive(this, "RecuperoLabel");

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText.ToString();

                                break;

                            case "LX02":
                                myLabel = (Label)myDelinea.FindControlRecursive(this, "Info2Label");

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText.ToString();

                                break;

                            case "LX03":

                                myRequiredFieldValidator = (RequiredFieldValidator)myDelinea.FindControlRecursive(this, "UserNameRequired");

                                if (myRequiredFieldValidator != null)
                                    myRequiredFieldValidator.ErrorMessage = childNode.InnerText.ToString();

                                break;

                            case "LX04":

                                PasswordRecovery1.UserNameFailureText = childNode.InnerText;

                                break;

                            case "LX05":

                                myButton = (Button)myDelinea.FindControlRecursive(this, "SubmitButton");

                                if (myButton != null)
                                    myButton.Text = childNode.InnerText.ToString();

                                myButton = (Button)myDelinea.FindControlRecursive(this, "SubmitEmailButton");

                                if (myButton != null)
                                    myButton.Text = childNode.InnerText.ToString();



                                break;

                            case "LX06":

                                myLabel = (Label)myDelinea.FindControlRecursive(this, "IdentificativoLabel");

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText.ToString();


                                break;

                            case "LX07":
                                myLabel = (Label)myDelinea.FindControlRecursive(this, "Info1Label");

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText.ToString();



                                break;

                            case "LX08":

                                myLabel = (Label)myDelinea.FindControlRecursive(this, "InfoEmail2Label");

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText.ToString();


                                //Response.Write("DEB childNode.InnerText.ToString():" + childNode.InnerText.ToString() + "<br />");

                                break;

                            case "LX09":

                                myLabel = (Label)myDelinea.FindControlRecursive(this, "EmailLabel");

                                if (myLabel != null)
                                    myLabel.Text = childNode.InnerText.ToString();

                                break;

                            case "LX10":


                                myRequiredFieldValidator = (RequiredFieldValidator)
                                    myDelinea.FindControlRecursive(this, "EmailRequired");

                                if (myRequiredFieldValidator != null)
                                    myRequiredFieldValidator.ErrorMessage = childNode.InnerText.ToString();

                                //Response.Write("DEB childNode.InnerText.ToString():" + childNode.InnerText.ToString() + "<br />");


                                break;


                            case "LX11":

                                RegularExpressionValidator MailRegulaeExpressionValidation =
                                    (RegularExpressionValidator)
                                    myDelinea.FindControlRecursive(this, "MailRegulaeExpressionValidation");

                                if (MailRegulaeExpressionValidation != null)
                                    MailRegulaeExpressionValidation.ErrorMessage = childNode.InnerText.ToString();


                                break;


                        }//fine switch

                    }//fine foreach

                }//fine foreach

                break;

            }//fine for

            return true;

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }


    }// fine ReadXml


    private string AccessoAreeRiservate(MembershipUser myUser)
    {
        if (myUser != null)
        {
            if (!myUser.IsApproved)
                return "negato";

            else if (!myUser.IsLockedOut)
                return "consentito";

            if ((!myUser.IsApproved) && (myUser.IsLockedOut))
                return "bloccato";


        }//fine if


        return string.Empty;
    }//fine AccessoAreeRiservate


    protected void PasswordRecovery1_SendingMail(object sender, MailMessageEventArgs e)
    {
        delinea myDelinea = new delinea();
        TextBox UserName = (TextBox)myDelinea.FindControlRecursive(Page, "UserName");



        MembershipUser myUser = Membership.GetUser(UserName.Text);

        //e.Message.From = new MailAddress("sviluppo@switchup.it", "WebAres WebSite");
        //Leggo il mittente inserito nel file /ZCF_mailSettings.config

        switch (LangHiddenField.Value.ToString().ToUpper())
        {
            case "ENG":
                e.Message.Subject = AresConfig.NomeSito + " WebSite - Password recovery";
                break;

            case "FRA":
                e.Message.Subject = AresConfig.NomeSito + " WebSite - Récupération du mot de passe";
                break;

            case "SPA":
                e.Message.Subject = AresConfig.NomeSito + " WebSite - Recuperación de contraseña";
                break;

            case "TED":
                e.Message.Subject = AresConfig.NomeSito + " WebSite - Kennwort erholung";
                break;

        }

        switch (LangHiddenField.Value.ToString().ToUpper())
        {

            default:

                e.Message.Body = Server.HtmlEncode("I dati per accedere al sito ");
                e.Message.Body += "<a href=\"" + AresConfig.GetUrl() + "\">" + AresConfig.NomeSito + "</a>, richiesti attraverso il modulo di recupero password smarrita, sono i seguenti: <br /><br />";
                e.Message.Body += "Nome utente: " + UserName.Text + "<br />";

                e.Message.Body += "Password: " + myUser.GetPassword() + "<br />";
                e.Message.Body += "Indirizzo E-Mail associato: " + myUser.Email;
                e.Message.Body += "<br />";
                e.Message.Body += "Accesso aree riservate: " + AccessoAreeRiservate(myUser);
                e.Message.Body += "<br />";
                e.Message.Body += "<br />";
                e.Message.Body += Server.HtmlEncode("Richiesta password smarrita effettuata in data ");
                e.Message.Body += DateTime.Now.ToString("d");
                e.Message.Body += " alle ore ";
                e.Message.Body += DateTime.Now.Hour + "." + DateTime.Now.Minute + "." + DateTime.Now.Second;
                e.Message.Body += Server.HtmlEncode(" - indirizzo IP ");
                e.Message.Body += Request.ServerVariables["REMOTE_ADDR"];
                e.Message.Body += "<br />";
                break;

            case "ENG":

                e.Message.Body = Server.HtmlEncode("Your login information for  ");
                e.Message.Body += "<a href=\"" + AresConfig.GetUrl() + "\">" + AresConfig.NomeSito + "</a>, requested using our password recovery form, is the following: <br /><br />";
                e.Message.Body += "User name: " + PasswordRecovery1.UserName + "<br />";
                e.Message.Body += "Password: " + myUser.GetPassword() + "<br />";
                e.Message.Body += "<br />";
                e.Message.Body += Server.HtmlEncode("Lost Password Request made on: ");
                e.Message.Body += DateTime.Now.ToString("d");
                e.Message.Body += " on ";
                e.Message.Body += DateTime.Now.Hour + "." + DateTime.Now.Minute + "." + DateTime.Now.Second;
                e.Message.Body += Server.HtmlEncode(" - IP address ");
                e.Message.Body += Request.ServerVariables["REMOTE_ADDR"];
                e.Message.Body += "<br />";
                break;

            case "FRA":

                e.Message.Body = Server.HtmlEncode("Les données pour votre site, ");
                e.Message.Body += "<a href=\"" + AresConfig.GetUrl() + "\">" + AresConfig.NomeSito + "</a> demandées via le formulaire de récupération du mot de passe sont les suivantes: <br /><br />";
                e.Message.Body += "User name: " + PasswordRecovery1.UserName + "<br />";
                e.Message.Body += "Password: " + myUser.GetPassword() + "<br />";
                e.Message.Body += "<br />";
                e.Message.Body += Server.HtmlEncode("Récupération du mot de passe: ");
                e.Message.Body += DateTime.Now.ToString("d");
                e.Message.Body += " on ";
                e.Message.Body += DateTime.Now.Hour + "." + DateTime.Now.Minute + "." + DateTime.Now.Second;
                e.Message.Body += Server.HtmlEncode(" - IP address ");
                e.Message.Body += Request.ServerVariables["REMOTE_ADDR"];
                e.Message.Body += "<br />";
                break;

            case "SPA":

                e.Message.Body = Server.HtmlEncode("Los datos correspondientes a su sitio, ");
                e.Message.Body += "<a href=\"" + AresConfig.GetUrl() + "\">" + AresConfig.NomeSito + "</a> solicitados a través de la forma de recuperación de contraseña perdida, son los siguientes: <br /><br />";
                e.Message.Body += "User name: " + PasswordRecovery1.UserName + "<br />";
                e.Message.Body += "Password: " + myUser.GetPassword() + "<br />";
                e.Message.Body += "<br />";
                e.Message.Body += Server.HtmlEncode("Recuperación de contraseña: ");
                e.Message.Body += DateTime.Now.ToString("d");
                e.Message.Body += " on ";
                e.Message.Body += DateTime.Now.Hour + "." + DateTime.Now.Minute + "." + DateTime.Now.Second;
                e.Message.Body += Server.HtmlEncode(" - IP address ");
                e.Message.Body += Request.ServerVariables["REMOTE_ADDR"];
                e.Message.Body += "<br />";
                break;


            case "TED":

                e.Message.Body = Server.HtmlEncode("Die Daten für Ihre Website, ");
                e.Message.Body += "<a href=\"" + AresConfig.GetUrl() + "\">" + AresConfig.NomeSito + "</a> durch die Form verloren Passwort-Wiederherstellung angefordert, sind die folgenden: <br /><br />";
                e.Message.Body += "User name: " + PasswordRecovery1.UserName + "<br />";
                e.Message.Body += "Password: " + myUser.GetPassword() + "<br />";
                e.Message.Body += "<br />";
                e.Message.Body += Server.HtmlEncode("Kennwort erholung: ");
                e.Message.Body += DateTime.Now.ToString("d");
                e.Message.Body += " on ";
                e.Message.Body += DateTime.Now.Hour + "." + DateTime.Now.Minute + "." + DateTime.Now.Second;
                e.Message.Body += Server.HtmlEncode(" - IP address ");
                e.Message.Body += Request.ServerVariables["REMOTE_ADDR"];
                e.Message.Body += "<br />";
                break;


        }//fine switch


        Zeus.Mail myMail = new Zeus.Mail();

        myMail.MailToSend.To.Add(new MailAddress(e.Message.To.ToString()));

        myMail.MailToSend.Subject = e.Message.Subject;
        myMail.MailToSend.Body = e.Message.Body;

        myMail.SendMail(Zeus.Mail.MailSendMode.Async);

        e.Cancel = true;
    }//fine PasswordRecovery1_SendingMail


    private bool SendMail(MembershipUserCollection AllUsers, string Email)
    {


        try
        {
            Zeus.Mail myMail = new Zeus.Mail();

            myMail.MailToSend.To.Add(new MailAddress(Email));

            myMail.MailToSend.Subject = AresConfig.NomeSito + " WebSite - Recupero password";

            switch (LangHiddenField.Value.ToString().ToUpper())
            {
                case "ENG":
                    myMail.MailToSend.Subject = AresConfig.NomeSito + " WebSite - Password recovery";
                    break;

                case "FRA":
                    myMail.MailToSend.Subject = AresConfig.NomeSito + " WebSite - Récupération du mot de passe";
                    break;

                case "SPA":
                    myMail.MailToSend.Subject = AresConfig.NomeSito + " WebSite - Recuperación de contraseña";
                    break;

                case "TED":
                    myMail.MailToSend.Subject = AresConfig.NomeSito + " WebSite - Kennwort erholung";
                    break;

            }




            string myBody = string.Empty;



            switch (LangHiddenField.Value.ToString().ToUpper())
            {

                default:
                    myBody = Server.HtmlEncode("I dati per accedere al sito ");
                    myBody += "<a href=\"" + AresConfig.GetUrl() + "\">" + AresConfig.NomeSito + "</a>, richiesti attraverso il modulo di recupero password smarrita, sono i seguenti: <br /><br />";


                    myBody += "Sono stati rilevati " + AllUsers.Count + " account utente associati all’indirizzo E-Mail " + Email + "<br /><br />";

                    foreach (MembershipUser myUser in AllUsers)
                    {

                        try
                        {
                            myBody += "Nome utente: " + myUser.UserName + "<br />";

                            myBody += "Password: " + myUser.GetPassword() + "<br />";
                            myBody += "Indirizzo E-Mail associato: " + myUser.Email;
                            myBody += "<br />";
                            myBody += "Accesso aree riservate: " + AccessoAreeRiservate(myUser);
                            myBody += "<br />";
                            myBody += "<br />";
                        }
                        catch (System.Web.Security.MembershipPasswordException)
                        {
                            myBody += "Nome utente: " + myUser.UserName + "<br />";

                            myBody += "Indirizzo E-Mail associato: " + myUser.Email;
                            myBody += "<br />";
                            myBody += "Accesso aree riservate: utente non esistente o bloccato (ad esempio, in seguito al superamento del numero massimo di tentativi di accesso permessi con password errata). Contattare il servizio utenti.";
                            myBody += "<br />";
                            myBody += "<br />";
                        }

                    }//fine foreach




                    myBody += Server.HtmlEncode("Richiesta password smarrita effettuata in data ");
                    myBody += DateTime.Now.ToString("d");
                    myBody += " alle ore ";
                    myBody += DateTime.Now.Hour + "." + DateTime.Now.Minute + "." + DateTime.Now.Second;
                    myBody += Server.HtmlEncode(" - indirizzo IP ");
                    myBody += Request.ServerVariables["REMOTE_ADDR"];
                    myBody += "<br />";
                    break;



                case "ENG":
                    myBody = Server.HtmlEncode("Your data for ");
                    myBody += "<a href=\"" + AresConfig.GetUrl() + "\">" + AresConfig.NomeSito + "</a>, requested through the lost password recovery form, are the following: <br /><br />";


                    myBody += "Were detected " + AllUsers.Count + " user accounts associated with the E-Mail " + Email + "<br /><br />";

                    foreach (MembershipUser myUser in AllUsers)
                    {

                        try
                        {
                            myBody += "User name: " + myUser.UserName + "<br />";

                            myBody += "Password: " + myUser.GetPassword() + "<br />";
                            myBody += "E-Mail address: " + myUser.Email;
                            myBody += "<br />";
                            myBody += "Access restricted areas: " + AccessoAreeRiservate(myUser);
                            myBody += "<br />";
                            myBody += "<br />";
                        }
                        catch (System.Web.Security.MembershipPasswordException)
                        {
                            myBody += "User name: " + myUser.UserName + "<br />";

                            myBody += "E-Mail: " + myUser.Email;
                            myBody += "<br />";
                            myBody += "Access restricted areas: non-existent user or blocked (eg, following an overrun of the maximum number of login attempts allowed with incorrect password). Please contact customer service.";
                            myBody += "<br />";
                            myBody += "<br />";
                        }

                    }//fine foreach




                    myBody += Server.HtmlEncode("Lost password request made ​​on ");
                    myBody += DateTime.Now.ToString("d");
                    myBody += " at  ";
                    myBody += DateTime.Now.Hour + "." + DateTime.Now.Minute + "." + DateTime.Now.Second;
                    myBody += Server.HtmlEncode(" - indirizzo IP ");
                    myBody += Request.ServerVariables["REMOTE_ADDR"];
                    myBody += "<br />";
                    break;


                case "FRA":
                    myBody = Server.HtmlEncode("Les données pour votre site ");
                    myBody += "<a href=\"" + AresConfig.GetUrl() + "\">" + AresConfig.NomeSito + "</a>, demandé par la récupération de mot de passe perdu sous forme, sont les suivants: <br /><br />";


                    myBody += "Ont été détectés " + AllUsers.Count + " compte d'utilisateur associé à l'E-Mail " + Email + "<br /><br />";

                    foreach (MembershipUser myUser in AllUsers)
                    {

                        try
                        {
                            myBody += "User name: " + myUser.UserName + "<br />";

                            myBody += "Password: " + myUser.GetPassword() + "<br />";
                            myBody += "E-Mail: " + myUser.Email;
                            myBody += "<br />";
                            myBody += "L'accès aux zones réglementées: " + AccessoAreeRiservate(myUser);
                            myBody += "<br />";
                            myBody += "<br />";
                        }
                        catch (System.Web.Security.MembershipPasswordException)
                        {
                            myBody += "User name: " + myUser.UserName + "<br />";

                            myBody += "E-Mail: " + myUser.Email;
                            myBody += "<br />";
                            myBody += "L'accès aux zones réglementées: inexistante utilisateur ou bloqué (par exemple, suite à un dépassement du nombre maximum de tentatives de connexion autorisées avec mot de passe incorrect). S'il vous plaît contacter le service client.";
                            myBody += "<br />";
                            myBody += "<br />";
                        }

                    }//fine foreach




                    myBody += Server.HtmlEncode("Demande Mot de passe oublié faite sur ");
                    myBody += DateTime.Now.ToString("d");
                    myBody += " à ";
                    myBody += DateTime.Now.Hour + "." + DateTime.Now.Minute + "." + DateTime.Now.Second;
                    myBody += Server.HtmlEncode(" - indirizzo IP ");
                    myBody += Request.ServerVariables["REMOTE_ADDR"];
                    myBody += "<br />";
                    break;



                case "TED":
                    myBody = Server.HtmlEncode("Die Daten für Ihre Website ");
                    myBody += "<a href=\"" + AresConfig.GetUrl() + "\">" + AresConfig.NomeSito + "</a>, angefordert durch die Form verloren Passwort-Wiederherstellung, sind die folgenden: <br /><br />";


                    myBody += "Nachgewiesen wurden " + AllUsers.Count + " Benutzerkonto der E-Mail verbunden " + Email + "<br /><br />";

                    foreach (MembershipUser myUser in AllUsers)
                    {

                        try
                        {
                            myBody += "User name: " + myUser.UserName + "<br />";

                            myBody += "Password: " + myUser.GetPassword() + "<br />";
                            myBody += "E-Mail: " + myUser.Email;
                            myBody += "<br />";
                            myBody += "Zugang Sperrgebieten: " + AccessoAreeRiservate(myUser);
                            myBody += "<br />";
                            myBody += "<br />";
                        }
                        catch (System.Web.Security.MembershipPasswordException)
                        {
                            myBody += "User name: " + myUser.UserName + "<br />";

                            myBody += "E-Mail: " + myUser.Email;
                            myBody += "<br />";
                            myBody += "Zugang Sperrgebieten: nicht vorhandenen Benutzer oder blockiert (zB wegen Überschreitung der maximalen Anzahl von Login-Versuchen mit falschen Passwort erlaubt). Kontaktieren Sie den Kundendienst.";
                            myBody += "<br />";
                            myBody += "<br />";
                        }

                    }//fine foreach




                    myBody += Server.HtmlEncode("Passwort vergessen Antrag gestellt auf ");
                    myBody += DateTime.Now.ToString("d");
                    myBody += " bei ";
                    myBody += DateTime.Now.Hour + "." + DateTime.Now.Minute + "." + DateTime.Now.Second;
                    myBody += Server.HtmlEncode(" - indirizzo IP ");
                    myBody += Request.ServerVariables["REMOTE_ADDR"];
                    myBody += "<br />";
                    break;


                case "SPA":
                    myBody = Server.HtmlEncode("Los datos de su sitio ");
                    myBody += "<a href=\"" + AresConfig.GetUrl() + "\">" + AresConfig.NomeSito + "</a>, solicitada a través del formulario de recuperación de contraseña perdida, son los siguientes: <br /><br />";


                    myBody += "Se detectaron " + AllUsers.Count + " cuenta de usuario asociada a la E-Mail " + Email + "<br /><br />";

                    foreach (MembershipUser myUser in AllUsers)
                    {

                        try
                        {
                            myBody += "User name: " + myUser.UserName + "<br />";

                            myBody += "Password: " + myUser.GetPassword() + "<br />";
                            myBody += "E-Mail: " + myUser.Email;
                            myBody += "<br />";
                            myBody += "Acceder a áreas restringidas: " + AccessoAreeRiservate(myUser);
                            myBody += "<br />";
                            myBody += "<br />";
                        }
                        catch (System.Web.Security.MembershipPasswordException)
                        {
                            myBody += "User name: " + myUser.UserName + "<br />";

                            myBody += "E-Mail: " + myUser.Email;
                            myBody += "<br />";
                            myBody += "Acceder a áreas restringidas: no existe el usuario o bloqueado (por ejemplo, debido al rebasamiento de la cantidad máxima de intentos de inicio de sesión permitidos con una contraseña incorrecta). Póngase en contacto con el servicio al cliente.";
                            myBody += "<br />";
                            myBody += "<br />";
                        }

                    }//fine foreach




                    myBody += Server.HtmlEncode("¿Perdió su contraseña en la solicitud presentada ");
                    myBody += DateTime.Now.ToString("d");
                    myBody += " en ";
                    myBody += DateTime.Now.Hour + "." + DateTime.Now.Minute + "." + DateTime.Now.Second;
                    myBody += Server.HtmlEncode(" - indirizzo IP ");
                    myBody += Request.ServerVariables["REMOTE_ADDR"];
                    myBody += "<br />";
                    break;



            }//fine switch


            myMail.MailToSend.Body = myBody;




            myMail.SendMail(Zeus.Mail.MailSendMode.Async);

            return true;
        }
        catch (Exception p)
        {

            Response.Write(p.ToString());
            return false;
        }

    }//fine SendMail


    protected void SubmitButton_Click(object sender, EventArgs e)
    {
        try
        {
            if (!EmailTextBox.Text.ToLower().Equals(ConfigurationManager.AppSettings["MissingMail"].ToLower()))
            {
                MembershipUserCollection Users = Membership.FindUsersByEmail(EmailTextBox.Text);

                if (Users.Count == 0)
                {
                    Response.Redirect("~/" + LangHiddenField.Value.ToString().Substring(0, 2).ToLower() + "/System/Message.aspx?Msg=7867868457567567");
                }

                if (SendMail(Users, EmailTextBox.Text))
                    Response.Redirect("~/" + LangHiddenField.Value.ToString().Substring(0, 2).ToLower() + "/System/Message.aspx?Msg=7867868456784588");
                //else Response.Redirect("~/System/Message.aspx?PasswordRecoveryError");
            }//fine if 
            else
            {

                switch (LangHiddenField.Value.ToString().ToUpper())
                {

                    case "ENG":
                        InfoLabel.Text = "No such user or blocked. Please contact customer service.<br/>";
                        break;

                    case "FRA":
                        InfoLabel.Text = "Aucun utilisateur de ce ou bloqué. S'il vous plaît contacter le service client.<br/>";
                        break;

                    case "SPA":
                        InfoLabel.Text = "Este usuario no existe o se bloquea. Por favor, póngase en contacto con el servicio al cliente.<br/>";
                        break;

                    case "TED":
                        InfoLabel.Text = "Kein solcher Benutzer oder blockiert. Bitte kontaktieren Sie den Kundendienst.<br/>";
                        break;

                    default:
                        //InfoLabel.Text = "Non è possibile effettuare il recupero dello UserName e della password utilizzando l’indirizzo E-Mail " + EmailTextBox.Text + ".<br />Contattare il servizio utenti e richiedere l’invio del proprio UserName e password.<br/>";
                        InfoLabel.Text = "Accesso aree riservate: utente non esistente o bloccato (ad esempio, in seguito al superamento del numero massimo di tentativi di accesso permessi con password errata). Contattare il servizio utenti.<br />";
                        break;
                }

                InfoLabel.Visible = true;
            }
        }
        catch (Exception p)
        {
            EmailCustomValidator.IsValid = false;
        }

    }//fine SubmitButton_Click
}