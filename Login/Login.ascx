﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Login.ascx.cs" Inherits="Login_LoginUC" %>

<asp:HiddenField ID="FailureTextHiddenField" runat="server" Value="Attenzione: raggiunto il numero massimo di tentativi di accesso. L'utente " />
<asp:HiddenField ID="FailureText2HiddenField" runat="server" Value=" è stato bloccato: contattare un amministratore di sistema per lo sblocco dell'account utente." />
<asp:HiddenField ID="FailureText3HiddenField" runat="server" Value="Tentativo di accesso non riuscito. Riprovare." />
<asp:HiddenField ID="LangHiddenField" runat="server" />
<style type="text/css">
    .panel-login {
        margin: 10px auto;
        max-width: 400px;
    }

    /***** Top content *****/

    .inner-bg {
        padding: 100px 0 170px 0;
    }

    .top-content .text {
        color: #fff;
    }

        .top-content .text h1 {
            color: #fff;
        }

    .top-content .description {
        margin: 20px 0 10px 0;
    }

        .top-content .description p {
            opacity: 0.8;
        }

        .top-content .description a {
            color: #fff;
        }

            .top-content .description a:hover,
            .top-content .description a:focus {
                border-bottom: 1px dotted #fff;
            }

    .form-top {
        overflow: hidden;
        padding: 0 25px 15px 25px;
        background: #fff;
        -moz-border-radius: 4px 4px 0 0;
        -webkit-border-radius: 4px 4px 0 0;
        border-radius: 4px 4px 0 0;
        text-align: left;
        border: 1px solid #eee;
    }

    .form-top-left {
        float: left;
        width: 75%;
        padding-top: 25px;
    }

        .form-top-left h3 {
            margin-top: 0;
        }

    .form-top-right {
        float: left;
        width: 25%;
        padding-top: 5px;
        font-size: 66px;
        color: #ddd;
        line-height: 100px;
        text-align: right;
    }

    .form-bottom {
        padding: 25px 25px 30px 25px;
        background: #eee;
        -moz-border-radius: 0 0 4px 4px;
        -webkit-border-radius: 0 0 4px 4px;
        border-radius: 0 0 4px 4px;
        text-align: left;
    }

        .form-bottom form textarea {
            height: 100px;
        }

        .form-bottom form button.btn {
            width: 100%;
        }

        .form-bottom form .input-error {
            border-color: #4aaf51;
        }

    .form-control, .btn {
        font-size: 1.8rem;
    }


    .erroreLogin {
        color: red;
        font-size: 2rem;
    }
</style>

<asp:Login ID="Login2" runat="server" CssClass="HIC_Validator1" DisplayRememberMe="true" VisibleWhenLoggedIn="True" PasswordLabelText="Password"
    UserNameLabelText="UserName" Width="100%" RememberMeSet="true" OnLoginError="Login2_LoginError"
    FailureText="Tentativo di accesso non riuscito. Riprovare." OnLoggedIn="OnLoggedIn">    
    <LayoutTemplate>
        <div class="row">
            <div class="col-sm-12 form-box">
                <%--<asp:ValidationSummary runat="server" ID="ValidationSummary1" 
                            DisplayMode="BulletList" ValidationGroup="Login2"
                            ShowMessageBox="False" ShowSummary="True" CssClass="alert alert-danger" />--%>
                <div class="form-top">
                    <div class="form-top-left">
                        <h3>
                            <asp:Label runat="server" ID="LoginTitle"></asp:Label></h3>
                        <p>
                            <asp:Label runat="server" ID="LoginSubTitle"></asp:Label>
                        </p>
                    </div>
                    <div class="form-top-right">
                        <i class="fa fa-key"></i>
                    </div>
                </div>
                <div class="form-bottom form-horizontal">
                    <div class="form-group ">
                        <label class="control-label col-sm-3" for="form-username">
                            <nobr>UserName *</nobr>
                        </label>
                        <div class="col-sm-9">
                            <asp:TextBox ID="UserName" runat="server" CssClass="form-username form-control" name="form-username" placeholder="Username"></asp:TextBox>
                            <asp:RequiredFieldValidator CssClass="required" ID="UserNameRequired" runat="server" SetFocusOnError="true" Display="Dynamic"
                                ControlToValidate="UserName" ErrorMessage="Obbligatorio." Font-Bold="True" ValidationGroup="Login2"> </asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3" for="form-password">
                            <nobr>Password *</nobr>
                        </label>
                        <div class="col-sm-9">
                            <asp:TextBox ID="Password" runat="server" TextMode="Password" name="form-password" placeholder="Password" CssClass="form-password form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator CssClass="required" ID="PasswordRequired" runat="server" SetFocusOnError="true" Display="Dynamic"
                                ControlToValidate="Password" ErrorMessage="Obbligatorio." Font-Bold="True" ValidationGroup="Login2"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <asp:CheckBox ID="RememberMe" runat="server" Text="Accedi automaticamente" CssClass="HIC_CheckBox1" Checked="false"></asp:CheckBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <asp:Button ID="LoginButton" runat="server" CommandName="Login" Text="Accedi" ValidationGroup="Login2" CssClass="btn btn-default" />
                            <br />
                            <asp:Label runat="server" ID="LoginErratoLabel" Visible="false" Text="Login Fallito" CssClass="erroreLogin"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group">
                        <br />
                        <asp:Label ID="InfoLabel" runat="server" Text="Password smarrita?" CssClass="HIC_LabelFieldValue1"></asp:Label>
                        <br />
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Login/PasswordRecovery.aspx"
                            CssClass="HIC_HyperLinkCorpoTesto1">Clicca qui</asp:HyperLink>
                        <asp:Label ID="Info2Label" runat="server" Text="per recuperare la password"></asp:Label>

                        <div>
                            <!--Se non possiedi Username e Password di accesso, <a href="/Registrazione/Utente.aspx">registrati ora</a>-->
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </LayoutTemplate>
</asp:Login>
