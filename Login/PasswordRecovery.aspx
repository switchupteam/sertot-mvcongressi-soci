﻿<%@ Page Language="C#" MasterPageFile="~/Master/LayoutAreaSoci.master" CodeFile="PasswordRecovery.aspx.cs" Inherits="Login_PasswordRecovery" Async="true" %>

<%@ Register Src="/Login/PasswordRecovery.ascx" TagName="PSW01_PasswordRecovery" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Robots" runat="Server">
    <meta name="robots" content="noindex,follow" />
</asp:Content>
<%--<asp:Content ID="Content4" ContentPlaceHolderID="InnerMenu" runat="Server">
    <li class="active">
        <a href="/Login/PasswordRecovery.aspx">Recupero password</a>
    </li>
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="AreaTitle" runat="Server">
    Recupero password
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AreaContentMain" runat="Server">

    <div class="container">
        <uc1:PSW01_PasswordRecovery ID="PSW01_PasswordRecovery1" runat="server" />
    </div>

</asp:Content>
