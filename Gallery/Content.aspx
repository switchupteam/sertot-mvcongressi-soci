﻿<%@ Page Language="C#" MasterPageFile="~/Master/LayoutMain.master" CodeFile="Content.aspx.cs" Inherits="Gallery_Content" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
    <link href="/asset/lib/lightbox2-master/css/lightbox.min.css" rel="stylesheet" />    
    <script src="/asset/lib/lightbox2-master/js/lightbox.min.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="AreaTitle" runat="Server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="AreaContentMain" runat="Server">
    <section class="galleryDettaglio">
        <div class="container">
            <div class="innerBox">
                <div class="boxTitolo">
                    <h2>Gallery - <%= titolo %></h2>
                    <hr />
                </div>

                <div class="introduzione">
                    <div class="row">
                        <asp:PlaceHolder runat="server" ID="phImg" Visible="false">
                            <div class="col-md-2">
                                <div class="immagine">
                                    <img src="<%= immagine1 %>" />
                                </div>
                            </div>
                        </asp:PlaceHolder>

                        <div class="col testo" runat="server">
                            <p class="descBreve"><%= descBreve %></p>
                        </div>
                    </div>
                </div>

                <asp:Panel runat="server" ID="NessunElemento" Visible="false">
                    <h5>Al momento non sono presenti gallery</h5>
                </asp:Panel>

                <asp:Repeater ID="Gallery_Repeater" runat="server" DataSourceID="Gallery_SqlDataSource">
                    <HeaderTemplate>
                        <div class="elencoFoto">
                            <div class="row no-gutters">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="col-md-6 col-lg-3">
                            <div class="foto">
                                <div class="immagine">
                                    <a class="fotoLinkGallery" href="/ZeusInc/PhotoGallery/Img1Photo/<%# Eval("Immagine1") %>" data-lightbox="galleria">
                                        <img src="/ZeusInc/PhotoGallery/Img1Photo/<%# Eval("Immagine1") %>" alt="<%# Eval("Immagine12Alt") %>" />
                                    </a>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                        </div>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>
                <asp:SqlDataSource runat="server" ID="Gallery_SqlDataSource" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>" />
            </div>
        </div>
    </section>
</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="script" runat="server">
    <script src="/asset/js/gallery.js"></script>
</asp:Content>
