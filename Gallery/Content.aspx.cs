﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Gallery_Content : System.Web.UI.Page
{
    protected string lang = string.Empty;
    protected string titolo = string.Empty;
    protected string immagine1 = string.Empty;
    protected string immagine1Alt = string.Empty;
    protected string descBreve = string.Empty;

    private string zeusId = string.Empty;    

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Request.QueryString["XRI"]))
            Response.Redirect("~/System/Message.aspx?0");


        AresUtilities aresUtil = new AresUtilities();
        lang = aresUtil.GetLangRewritted();

        
        string strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        using (SqlConnection connection = new SqlConnection(strConnessione))
        {
            connection.Open();
            SqlCommand command = connection.CreateCommand();

            string sqlQuery = @"SELECT ID1Gallery, Titolo, ID2Categoria, DescBreve, Descrizione, Immagine1, Immagine1Alt, REPLACE(LOWER(Titolo), ' ', '-') AS UrlRewrite, ZeusId
                                FROM tbPhotoGallery
                                WHERE ZeusIdModulo = @ZeusIdModulo AND ZeusLangCode = @ZeusLangCode 
	                                AND (PW_Area1 = 1 AND (GETDATE() BETWEEN PWI_Area1 AND PWF_Area1))
                                    AND ID1Gallery = @ID1Gallery";

            command.CommandText = sqlQuery;
            command.Parameters.Add("@ZeusIdModulo", System.Data.SqlDbType.VarChar);
            command.Parameters["@ZeusIdModulo"].Value = "PGGAL";
            command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.VarChar);
            command.Parameters["@ZeusLangCode"].Value = "ITA";
            command.Parameters.Add("@ID1Gallery", System.Data.SqlDbType.Int);
            command.Parameters["@ID1Gallery"].Value = Request.QueryString["XRI"];

            SqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                titolo = reader["Titolo"].ToString();
                immagine1 = "/ZeusInc/PhotoGallery/Img1/" + reader["Immagine1"].ToString();
                immagine1Alt = reader["Immagine1Alt"].ToString();
                descBreve = reader["DescBreve"].ToString();
                zeusId = reader["ZeusId"].ToString();

                if (!string.IsNullOrWhiteSpace(immagine1) && immagine1 != "ImgNonDisponibile.jpg")
                    phImg.Visible = true;
            }

            reader.Close();
            connection.Close();
        }



        /*****************************************************/
        /**************** INIZIO SEO *************************/
        /*****************************************************/
        HtmlMeta description = new HtmlMeta();
        HtmlMeta keywords = new HtmlMeta();
        description.Name = "description";
        keywords.Name = "keywords";
        Page.Title = titolo + " | " + AresConfig.NomeSito;
        description.Content = titolo;
        keywords.Content = titolo;
        HtmlHead head = Page.Header;
        head.Controls.Add(description);
        head.Controls.Add(keywords);
        /*****************************************************/
        /**************** FINE SEO ***************************/
        /*****************************************************/



        Gallery_SqlDataSource.SelectCommand = @"SELECT ID1Photo, ID2PhotoGallery, Ordinamento, Immagine1, Immagine12Alt
                                                FROM tbPhotoGallery_Photo
                                                WHERE ZeusIdModulo = 'PGGAL' AND ZeusLangCode = 'ITA' AND ID2PhotoGallery = @ZeusId
                                                ORDER BY Ordinamento";
        Gallery_SqlDataSource.SelectParameters.Add("ZeusId", System.Data.DbType.String, zeusId);
    }
}