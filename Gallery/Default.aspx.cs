﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Gallery_Default : System.Web.UI.Page
{
    protected string lang = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        AresUtilities aresUtil = new AresUtilities();
        lang = aresUtil.GetLangRewritted();


        Gallery_SqlDataSource.SelectCommand = @"SELECT ID1Gallery, Titolo, ID2Categoria, DescBreve, Descrizione, Immagine1, Immagine1Alt, REPLACE(LOWER(Titolo), ' ', '-') AS UrlRewrite
                                                FROM tbPhotoGallery
                                                WHERE ZeusIdModulo = 'PGGAL' AND ZeusLangCode = @ZeusLangCode
                                                    AND(PW_Area1 = 1 AND(GETDATE() BETWEEN PWI_Area1 AND PWF_Area1))
                                                ORDER BY PWI_Area1 DESC";
        Gallery_SqlDataSource.SelectParameters.Add("ZeusLangCode", System.Data.DbType.String, lang);




        /*****************************************************/
        /**************** INIZIO SEO *************************/
        /*****************************************************/
        HtmlMeta description = new HtmlMeta();
        HtmlMeta keywords = new HtmlMeta();
        description.Name = "description";
        keywords.Name = "keywords";
        Page.Title = "Gallery | " + AresConfig.NomeSito;
        description.Content = "";
        keywords.Content = "";
        HtmlHead head = Page.Header;
        head.Controls.Add(description);
        head.Controls.Add(keywords);
        /*****************************************************/
        /**************** FINE SEO ***************************/
        /*****************************************************/
    }
}