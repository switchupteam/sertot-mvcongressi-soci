﻿<%@ Page Language="C#" MasterPageFile="~/Master/LayoutMain.master" CodeFile="Default.aspx.cs" Inherits="Gallery_Default" %>


<asp:Content ID="Content2" ContentPlaceHolderID="AreaTitle" runat="Server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="AreaContentMain" runat="Server">
    <section class="gallery">
        <div class="container">
            <div class="innerBox">
                <div class="boxTitolo">
                    <h2>Gallery</h2>
                    <hr />
                </div>

                <p class="descrizione">
                    <asp:Literal ID="litDescription" runat="server"></asp:Literal>
                </p>

                 <asp:Panel runat="server" ID="NessunElemento" Visible="false">
                    <h5>Al momento non sono presenti gallery.</h5>
                </asp:Panel>

                <asp:Repeater ID="rptBooks" runat="server" DataSourceID="Gallery_SqlDataSource">
                    <ItemTemplate>
                        <div class="galleryItem">
                            <%--<div class="row align-items-end mb-5">--%>
                            <div class="row mb-5">
                                <div class="col-2">
                                    <div class="immagineLibro">
                                        <img src='/ZeusInc/PhotoGallery/Img1/<%# Eval("Immagine1") %>' alt='<%# Eval("Immagine1Alt") %>' />
                                    </div>
                                </div>
                                <div class="col-10">
                                    <div class="testoLibro">
                                        <h5 class="titolo"><%#Eval("Titolo") %></h5>
                                        <div class="autori"><%#Eval("DescBreve") %></div>                                        
                                        <div class="link">
                                            <a href='/Gallery/<%# Eval("ID1Gallery") %>/<%#Eval("UrlRewrite") %>'>Apri Dettagli</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <asp:SqlDataSource runat="server" ID="Gallery_SqlDataSource" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>" />
            </div>
        </div>
    </section>
</asp:Content>

