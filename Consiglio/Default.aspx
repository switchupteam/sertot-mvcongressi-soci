﻿<%@ Page Language="C#" MasterPageFile="~/Master/LayoutMain.master" CodeFile="Default.aspx.cs" Inherits="Consiglio_Default" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="Server">
    <style>
        body main {
            min-height: 80rem;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AreaTitle" runat="Server">
    Consiglio Direttivo
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="AreaContentMain" runat="Server">
    <section class="consiglioDirettivo">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <p><strong>Presidente</strong></p>
                    <p>
                        Dr. Luigi Milano, Alba<br />
                        milanofam@libero.it
                    </p><br />
                    <br />
                    <p><strong>Vice Presidenti</strong></p>
                    <p>
                        Dr. Carlo Montoli, Luino<br />
                        mcarlo63@libero.it
                    </p><br />
                    <p>
                        Dr.ssa Elena Manuela Samaila, Verona<br />
                        elenasamaila@yahoo.it
                    </p>
                </div>
                <div class="col-md-4">
                    <p><strong>Consiglieri</strong></p>
                    <p>
                        Dr. Alessio Canali, Carpi<br />
                        dott.canali.a@gmail.com
                    </p><br />
                    <p>
                        Dr. Antonio Carolla, Parma<br />
                        acarolla@gmail.com
                    </p><br />
                    <p>
                        Dr. Paolo Ceccarini, Perugia<br />
                        paoloceccarini84@gmail.com
                    </p><br />
                    <p>
                        Dr. Daniele Marcolli, Legnano<br />
                        daniele.marcolli@me.com
                    </p><br />
                    <p>
                        Dr. Antonio Mazzotti, Bologna<br />
                        antonio.mazzotti2@studio.unibo.it
                    </p>
                </div>
                <div class="col-md-3">
                    <div style="text-align: center; background: url('/asset/img/grey_bar_horizzontal.jpg') left top repeat-y, url('/asset/img/grey_bar_horizzontal.jpg') right top repeat-y; padding: 2rem 0; background-color: #730F11; ">
                        <a href="#" style="color: #fff; display: inline-block;">Anni 2015-2017</a><br />
                        <a href="#" style="color: #fff; display: inline-block;">Anni 2013-2015</a><br />
                        <a href="#" style="color: #fff; display: inline-block;">Anni 2015-2017</a><br />
                        <a href="#" style="color: #fff; display: inline-block;">Anni 2013-2015</a><br />
                        <a href="#" style="color: #fff; display: inline-block;">Anni 2015-2017</a><br />
                        <a href="#" style="color: #fff; display: inline-block;">Anni 2013-2015</a><br />
                        <a href="#" style="color: #fff; display: inline-block;">Anni 2015-2017</a><br />
                        <a href="#" style="color: #fff; display: inline-block;">Anni 2013-2015</a><br />
                        <a href="#" style="color: #fff; display: inline-block;">Anni 2015-2017</a><br />
                        <a href="#" style="color: #fff; display: inline-block;">Anni 2013-2015</a><br />
                        <a href="#" style="color: #fff; display: inline-block;">Tutte le pagine</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
