﻿<%@ Page Language="C#" MasterPageFile="~/Master/LayoutMain.master" CodeFile="Default.aspx.cs" Inherits="Corso_Default" %>

<asp:Content ID="Content2" ContentPlaceHolderID="AreaTitle" runat="Server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="AreaContentMain" runat="Server">
    <section class="corsoDettaglio">
        <div class="container">
            <div class="innerBox">
                <div class="firstTitle">
                    <h2 class="titolo"><%= titolo %></h2>
                    <hr />
                </div>

                <div class="introduzione">
                    <div class="row">
                        <asp:PlaceHolder runat="server" ID="phImg" Visible="true">
                            <div class="col-md-3">
                                <div class="immagine">
                                    <img src="/ZeusInc/Publisher/Img1/<%= immagine1 %>" />
                                </div>
                            </div>
                        </asp:PlaceHolder>

                        <div class="col testo" runat="server">
                            <h6 class="descBreve"><%= descBreve %></h6>
                            <div class="richText">
                                <%= contenuto1 %>
                            </div>

                            <div class="listaPulsanti">
                                <asp:PlaceHolder runat="server" ID="LinkIscrizione" Visible="false">
                                    <div class="link iscriviti">
                                        <a href="<%= urlLink %>" target="_blank">Iscriviti</a>
                                    </div>
                                </asp:PlaceHolder>
                                <asp:PlaceHolder runat="server" ID="LinkAllegato" Visible="false">
                                    <div class="link file">
                                        <a href="<%= urlFile %>" target="_blank">Scarica il programma</a>
                                    </div>
                                </asp:PlaceHolder>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="richText">
                    <%= contenuto2 %>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
