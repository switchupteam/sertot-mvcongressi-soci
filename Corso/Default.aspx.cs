﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.HtmlControls;

public partial class Corso_Default : System.Web.UI.Page
{
    protected string titolo = string.Empty;
    protected string descBreve = string.Empty;
    protected string contenuto1 = string.Empty;
    protected string contenuto2 = string.Empty;
    protected string urlLink = string.Empty;
    protected string urlFile = string.Empty;
    protected string immagine1 = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Request.QueryString["XRI"]))
        {
            Response.Redirect("~/System/Message.aspx?0");
        }

        AresUtilities aresUtilities = new AresUtilities();
        string lang = aresUtilities.GetLangRewritted();

        string strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(strConnessione))
        {
            connection.Open();
            SqlCommand command = connection.CreateCommand();

                sqlQuery = @"SELECT PUB.ID1Publisher, PUB.ID2Categoria1, PUB.Titolo, CAT.CatLiv2Liv3, PUB.PWI_Area1, PUB.PWF_Area1, PUB.PWI_Area2, 
                                PUB.PWF_Area2, PUB.Archivio, PUB.ZeusIdModulo, PUB.ZeusLangCode, PUB.PWI_Area3, PUB.PWF_Area3, PUB.Immagine1, 
                                PUB.DescBreve1, PUB.Contenuto1, PUB.Contenuto2, PUB.Contenuto3, PUB.Immagine2, PUB.TagDescription, PUB.TagKeywords, 
                                PUB.UrlRewrite, PUB.Autore, PUB.Autore_Email, PUB.Fonte, PUB.Fonte_Url, PUB.TitoloBrowser, PUB.PW_Area2 + DATEDIFF(day, 
                                PUB.PWI_Area2, GETDATE()) - ABS(DATEDIFF(day, PUB.PWI_Area2, GETDATE())) + DATEDIFF(day, GETDATE(), PUB.PWF_Area2) - ABS(DATEDIFF(day, GETDATE(), PUB.PWF_Area2)) 
                                AS AttivoArea2, PUB.Allegato1_Selettore, PUB.Allegato1_UrlEsterno, PUB.Allegato1_File, CAT.CatLiv3, CAT.ID2CategoriaParent AS ID2CatLiv2, 
                                PUB.Sottotitolo
                                , tbPZ_ImageRaider.ImgBeta_Path
                            FROM tbPublisher AS PUB
	                            LEFT OUTER JOIN vwCategorie_All AS CAT ON PUB.ID2Categoria1 = CAT.ID1Categoria
	                            INNER JOIN tbPZ_ImageRaider on ZeusIdModuloIndice = PUB.ZeusIdModulo + '1'
                            WHERE (PUB.ZeusIsAlive = 1) 
	                            AND (AttivoArea1 = 1 OR AttivoArea2 = 1)
	                            AND PUB.ZeusIdModulo = 'PBCOR'
	                            AND ID1Publisher = @ID1Publisher AND PUB.ZeusLangCode = @ZeusLangCode";
                            
          
            command.CommandText = sqlQuery;
            command.Parameters.Add("@ID1Publisher", System.Data.SqlDbType.Int);
            command.Parameters["@ID1Publisher"].Value = Request.QueryString["XRI"];
            command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.NVarChar);
            command.Parameters["@ZeusLangCode"].Value = lang;

            SqlDataReader reader = command.ExecuteReader();

            if (reader.Read())
            {
                titolo = reader["Titolo"].ToString();
                descBreve = reader["DescBreve1"].ToString();
                contenuto1 = reader["Contenuto1"].ToString();
                contenuto2 = reader["Contenuto2"].ToString();

                if (!string.IsNullOrWhiteSpace(reader["Sottotitolo"].ToString()))
                {
                    LinkIscrizione.Visible = true;

                    urlLink = reader["Sottotitolo"].ToString();
                }

                if (!string.IsNullOrWhiteSpace(reader["Allegato1_Selettore"].ToString()) && reader["Allegato1_Selettore"].ToString().Equals("FIL") && !string.IsNullOrWhiteSpace(reader["Allegato1_File"].ToString()))
                {
                    LinkAllegato.Visible = true;

                    urlFile = "/ZeusInc/Publisher/Documents/" + reader["Allegato1_File"].ToString();
                }

                immagine1 = reader["Immagine1"].ToString();
                if (string.IsNullOrWhiteSpace(immagine1))
                    immagine1 = "ImgNonDisponibile.jpg";

                if (!string.IsNullOrEmpty(immagine1) && immagine1.Substring(immagine1.LastIndexOf('/') + 1) == "ImgNonDisponibile.jpg")
                {
                    phImg.Visible = false;
                }


                /*****************************************************/
                /**************** INIZIO SEO *************************/
                /*****************************************************/
                HtmlMeta description = new HtmlMeta();
                HtmlMeta keywords = new HtmlMeta();
                description.Name = "description";
                keywords.Name = "keywords";
                Page.Title = reader["TitoloBrowser"].ToString() + " | " + AresConfig.GetBaseTitle();
                description.Content = reader["TagDescription"].ToString();
                keywords.Content = reader["TagKeywords"].ToString();
                HtmlHead head = Page.Header;
                head.Controls.Add(description);
                head.Controls.Add(keywords);
                /*****************************************************/
                /**************** FINE SEO ***************************/
                /*****************************************************/

                reader.Close();
                connection.Close();
            }
            else
            {
                reader.Close();
                connection.Close();
                Response.Redirect("~/System/Message.aspx?0");
            }
        }
    }    
}