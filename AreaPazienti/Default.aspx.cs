﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.HtmlControls;

public partial class AreaPazienti : System.Web.UI.Page
{
    string strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        /*****************************************************/
        /**************** INIZIO SEO *************************/
        /*****************************************************/
        HtmlMeta description = new HtmlMeta();
        HtmlMeta keywords = new HtmlMeta();
        description.Name = "Area Pazienti";
        keywords.Name = "Area Pazienti";
        Page.Title = "Area Pazienti | " + AresConfig.NomeSito;
        description.Content = "";
        keywords.Content = "";
        HtmlHead head = Page.Header;
        head.Controls.Add(description);
        head.Controls.Add(keywords);
        /*****************************************************/
        /**************** FINE SEO ***************************/
        /*****************************************************/


        BindPubblicazioni();
    }

    private void BindPubblicazioni()
    {
        //Load Books
        var books = new List<book>();

        string sqlQuery = string.Empty;
        //var provincie = new List<ProvinciaItem>();
        using (SqlConnection connection = new SqlConnection(strConnessione))
        {
            connection.Open();
            SqlCommand command = connection.CreateCommand();

            sqlQuery = @"SELECT ID1Publisher, Titolo, ZeusIdModulo, Immagine1, DescBreve1, Contenuto1, Sottotitolo AS Link, UrlRewrite
                          FROM [MVCongressi-sertot].[dbo].[vwPublisher_Public_DtlArea1]
                          WHERE ZeusIdModulo = @ZeusIdModulo
                          ORDER BY PWI_Area1 DESC";

            command.CommandText = sqlQuery;
            command.Parameters.Add("@ZeusIdModulo", System.Data.SqlDbType.VarChar);
            command.Parameters["@ZeusIdModulo"].Value = "PBPAZ";


            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                var book = new book()
                {
                                        Title = reader["Titolo"].ToString(),
                    Authors = reader["Contenuto1"].ToString(),
                    Desc = reader["DescBreve1"].ToString(),
                    Link = string.Format("/{0}/{1}/{2}", "AreaPazientiDet", reader["ID1Publisher"].ToString(), reader["UrlRewrite"].ToString())
                };
                books.Add(book);
            }

            if (books.Count == 0)
            {
                NessunElemento.Visible = true;
                rptBooks.Visible = false;
            }
            else
            {
                rptBooks.DataSource = books;
                rptBooks.DataBind();
            }

            reader.Close();
            connection.Close();
        }
    }



    public class book
    {
        public string Image { get; set; }
        public string Title { get; set; }
        public string Authors { get; set; }
        public string Desc { get; set; }
        public string Link { get; set; }
    }
}