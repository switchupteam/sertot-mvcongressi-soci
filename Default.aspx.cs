﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected string title = string.Empty;
    protected string lang = string.Empty;
    protected CultureInfo currentCulture;
    protected string contenutoHomeLibero = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        AresUtilities aresUtil = new AresUtilities();
        lang = aresUtil.GetLangRewritted();

        currentCulture = new CultureInfo(lang == "ITA" ? "it-IT" : "en-US");

        Header.Lang = lang;
        Footer.Lang = lang;


        SliderDataSource.SelectCommand = @"SELECT Identificativo, Link, Contenuto1, Contenuto2, Immagine1, Immagine12Alt
                                            FROM tbDigiSign
                                            WHERE ZeusIsAlive = 1 AND ZeusIdModulo = 'HPSLI' AND ZeusLangCode = @ZeusLangCode
	                                            AND PW_Area1 = 1 AND (GETDATE() BETWEEN PWI_Area1 AND PWF_Area1)
                                            ORDER BY PWI_Area1 DESC, Ordinamento";
        SliderDataSource.SelectParameters.Add("ZeusLangCode", System.Data.DbType.String, lang);
        
        
        
        BoxBannerDataSource.SelectCommand = @"SELECT TOP (4) Identificativo, Link, Contenuto1, Contenuto2, Immagine1, Immagine12Alt
                                            FROM tbDigiSign
                                            WHERE ZeusIsAlive = 1 AND ZeusIdModulo = 'HPBNN' AND ZeusLangCode = @ZeusLangCode
	                                            AND PW_Area1 = 1 AND (GETDATE() BETWEEN PWI_Area1 AND PWF_Area1)
                                            ORDER BY Ordinamento";
        BoxBannerDataSource.SelectParameters.Add("ZeusLangCode", System.Data.DbType.String, lang);



        SqlDataSource_UltimeNews.SelectCommand = @"SELECT TOP (3) ID1Publisher, Titolo, DescBreve1, Contenuto1, PWI_Area1, '/News/' + CONVERT(varchar, ID1Publisher) + '/' + UrlRewrite AS Url
                                                FROM tbPublisher
                                                WHERE ZeusIdModulo = 'PBNWS' AND ZeusLangCode = @ZeusLangCode AND (PW_Area1 = 1 AND (GETDATE() BETWEEN PWI_Area1 AND PWF_Area1))
                                                ORDER BY PWI_Area1 DESC";
        SqlDataSource_UltimeNews.SelectParameters.Add("ZeusLangCode", System.Data.DbType.String, lang);



        SqlDataSource_SocietaScentifichePartner.SelectCommand = @"SELECT ID1Brand, Brand, ID2Categoria, Immagine1, Immagine1Alt, Link1
                                                FROM tbBrands
                                                WHERE ZeusIdModulo = 'BRSPN' AND ZeusLangCode = @ZeusLangCode AND ID2Categoria = @ID2Categoria
                                                    AND(PW_Area1 = 1 AND(GETDATE() BETWEEN PWI_Area1 AND PWF_Area1))
                                                ORDER BY PWI_Area1";
        SqlDataSource_SocietaScentifichePartner.SelectParameters.Add("ZeusLangCode", System.Data.DbType.String, lang);
        SqlDataSource_SocietaScentifichePartner.SelectParameters.Add("ID2Categoria", System.Data.DbType.String, "34");



        SqlDataSource_Sponsor.SelectCommand = @"SELECT ID1Brand, Brand, ID2Categoria, Immagine1, Immagine1Alt, Link1
                                                FROM tbBrands
                                                WHERE ZeusIdModulo = 'BRSPN' AND ZeusLangCode = @ZeusLangCode AND ID2Categoria = @ID2Categoria
                                                    AND(PW_Area1 = 1 AND(GETDATE() BETWEEN PWI_Area1 AND PWF_Area1))
                                                ORDER BY PWI_Area1";
        SqlDataSource_Sponsor.SelectParameters.Add("ZeusLangCode", System.Data.DbType.String, lang);
        SqlDataSource_Sponsor.SelectParameters.Add("ID2Categoria", System.Data.DbType.String, "35");




        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        using (SqlConnection connection = new SqlConnection(strConnessione))
        {
            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                string sqlQuery = @"SELECT Contenuto1
                                    FROM tbWebComponent
                                    WHERE ZeusIsAlive = 1 AND ZeusLangCode = @ZeusLangCode AND ZeusId = @ZeusId";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.VarChar);
                command.Parameters["@ZeusLangCode"].Value = lang;
                command.Parameters.Add("@ZeusId", System.Data.SqlDbType.VarChar);
                command.Parameters["@ZeusId"].Value = "E0CB6B96-25EF-4A6A-9C6D-7DF1BF80E214";                

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                    contenutoHomeLibero = reader["Contenuto1"].ToString();

                reader.Close();
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
                return;
            }         
        }        


        /*****************************************************/
        /**************** INIZIO SEO *************************/
        /*****************************************************/
        Dictionary<string, string> homePageSEO = AresConfig.GetHomePageSEO(lang);
        title = homePageSEO["Title"];
        HtmlMeta description = new HtmlMeta();
        HtmlMeta keywords = new HtmlMeta();
        description.Name = "description";
        description.Content = homePageSEO["Description"];
        keywords.Name = "keywords";
        keywords.Content = homePageSEO["Keywords"];
        HtmlHead head = Page.Header;
        head.Controls.Add(description);
        head.Controls.Add(keywords);
        /*****************************************************/
        /**************** FINE SEO ***************************/
        /*****************************************************/
    }

    protected void MenuRepeater_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
    {
        SqlDataSource MenuChildDataSource = (SqlDataSource)e.Item.FindControl("MenuChildDataSource");

        MenuChildDataSource.SelectCommand = @"SELECT Menu, Url, TargetWindow FROM vwSiteMenu_All 
                                            WHERE ZeusIdModulo = 'SMPUB' AND ZeusLangCode = @ZeusLangCode AND AttivoArea1 = 1
                                            AND Livello = 3 AND ID2MenuParent = @ID2MenuParent
                                            ORDER BY Ordinamento ASC";
        MenuChildDataSource.SelectParameters.Add("ZeusLangCode", System.Data.DbType.String, lang);
        MenuChildDataSource.SelectParameters.Add("ID2MenuParent", DataBinder.Eval(e.Item.DataItem, "ID1Menu").ToString());

        if(e.Item.ItemIndex == 0)
        {
            HtmlGenericControl navItem = e.Item.FindControl("navItem") as HtmlGenericControl;
            navItem.Attributes.Add("class", "nav-item active");
        }
    }

    protected void MenuChildRepeater_PreRender(object sender, EventArgs e)
    {
        Repeater menuChildRepeater = (Repeater)sender;
        if (menuChildRepeater.Items.Count == 0)
        {
            menuChildRepeater.Visible = false;
            ((HtmlAnchor)(menuChildRepeater).Parent.FindControl("LinkLv1DropDown")).Visible = false;
            ((HtmlAnchor)(menuChildRepeater).Parent.FindControl("LinkLv1")).Visible = true;
        }
        else
        {
            ((HtmlGenericControl)(menuChildRepeater).Parent.FindControl("navItem")).Attributes.Add("class", "nav-item dropdown");
        }
    }

    protected void SliderDataSource_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.AffectedRows == 0)
            SliderPlaceHolder.Visible = false;
    }

    protected void BoxBannerDataSource_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.AffectedRows == 0)
            BoxBannerPlaceHolder.Visible = false;
    }
}