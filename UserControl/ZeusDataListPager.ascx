﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ZeusDataListPager.ascx.cs" Inherits="UserControl_ZeusDataListPager" %>

<div class="PAGDL">
    <div class="PAGDL_LayInnerCont">
        <div class="PAGDL_LayItemCell">
            <asp:LinkButton CssClass="PAGDL_LinkButton" ID="cmdFirst" runat="server" Text=" |< " OnClick="cmdFirst_Click"></asp:LinkButton>
            <asp:LinkButton CssClass="PAGDL_LinkButton" ID="cmdPrev" runat="server" Text=" << " OnClick="cmdPrev_Click"></asp:LinkButton>
        </div>
        <div class="PAGDL_LayItemCell">
            <asp:Label ID="lblCurrentPage" CssClass="PAGDL_LabelText" runat="server"></asp:Label>
        </div>
        <div class="PAGDL_LayItemCell" style="padding-right: 3px;">
            <asp:Label ID="lblPagina" CssClass="PAGDL_LabelText" runat="server">Vai a pagina</asp:Label>
        </div>
        <div class="PAGDL_LayItemCell" style="padding-left: 3px; padding-right: 3px;">
            <asp:DropDownList CssClass="PAGDL_DropDownList" ID="SelettoreDropDownList" runat="server" Height="18px"></asp:DropDownList>
        </div>
        <div class="PAGDL_LayItemCell" style="padding-left: 3px;">
            <asp:LinkButton CssClass="PAGDL_LinkButton" ID="LoadButton" OnClick="LoadButton_Click" runat="server" Text=">" />
        </div>
        <div class="PAGDL_LayItemCell">
            <asp:Label ID="lblTotale" CssClass="PAGDL_LabelText" runat="server"></asp:Label>
        </div>
        <div class="PAGDL_LayItemCell">
            <asp:LinkButton CssClass="PAGDL_LinkButton" OnClick="cmdNext_Click" ID="cmdNext" runat="server" Text=" >> "></asp:LinkButton>
            <asp:LinkButton CssClass="PAGDL_LinkButton" OnClick="cmdLast_Click" ID="cmdLast" runat="server" Text=" >| "></asp:LinkButton>
        </div>
    </div>
</div>
