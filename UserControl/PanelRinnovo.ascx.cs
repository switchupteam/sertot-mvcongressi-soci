﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControl_PanelRinnovo : System.Web.UI.UserControl {
    protected void Page_Load(object sender, EventArgs e) {

        if (!clsGenericUtils.IsSocioOnorario(Membership.GetUser().ProviderUserKey.ToString())) {
            RenewalButton.Visible = clsGenericUtils.NeedsRenewal(Membership.GetUser().ProviderUserKey.ToString());
        }
    }


}