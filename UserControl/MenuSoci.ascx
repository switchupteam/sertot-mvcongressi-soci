﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuSoci.ascx.cs" Inherits="UserControl_MenuSoci" %>

<div class="SMN03C">
    <span class="TFY_Testo1">
        Socio:
        <br />
        <asp:Label ID="NomeSocio" runat="server"></asp:Label>
        <asp:Label ID="CognomeSocio" runat="server"></asp:Label>
        |
        <asp:Label ID="NumSocio" runat="server"></asp:Label></span>

    <div class="SMN03C_LayTitolo">
        <h3 class="TFY_Titolo3">
            <asp:Label ID="TitleContent1Label" runat="server">Area Soci Sertot</asp:Label></h3>
    </div>
    <div class="SMN03C_LayElenco">
        <asp:Repeater ID="Repeater1" runat="server" DataSourceID="dsMenuSoci">
            <ItemTemplate>

                <div class="SMN03C_List">
                    <a href='<%# Eval("URL") %>'><%# Eval("Menu") %></a>
                </div>

            </ItemTemplate>
        </asp:Repeater>
        <asp:SqlDataSource ID="dsMenuSoci" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
            SelectCommand="SELECT ID1Menu, Menu, Ordinamento, URL FROM tbSiteMenu WHERE (ZeusIdModulo = N'SMSOC') AND (ZeusLangCode = @Lang) AND ID2MenuParent != 0 AND ZeusIsAlive > 0 AND PW_Area1 = 1 AND (GETDATE() BETWEEN PWI_Area1 AND PWF_Area1) ORDER BY Ordinamento">
            <SelectParameters>
                <asp:QueryStringParameter Name="Lang" QueryStringField="Lang" DefaultValue="ITA" />
            </SelectParameters>
        </asp:SqlDataSource>

    </div>
</div>

<br />
<br />

<asp:HyperLink NavigateUrl="/Login/Default.aspx?Lang=" ID="ToZeusButton" runat="server" Text="Area gestionale" CssClass="BTN02B" Visible="false" Width="100%" />