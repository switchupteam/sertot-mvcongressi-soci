﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SideMenu_NewsSoci.ascx.cs" Inherits="UserControl_SideMenu_NewsSoci" %>

<div class="LSS01B">
    <div class="LSS01B_LayBoxTitle">
        <h2 class="LSS01B_TitoloBox"><asp:Label ID="TitleNewsLabel" runat="server" Text="Notizie riservate ai Soci" /></h2>
    </div>
    <asp:Repeater ID="Repeater1" runat="server" DataSourceID="dsNews">
        <ItemTemplate>
            <div class="LSS01B_Row">
                <a href="<%# Eval("ID1Publisher","/AreaSoci/Notizia.aspx?XRI={0}") %>" class="LSS01B_NoUnderline">
                    <div class="LSS01B_Item">
                        <div class="LSS01B_ImgThumb">
                            <img src="<%# Eval("Immagine1","/ZeusInc/Publisher/Img1/{0}") %>" border="0" /></div>
                        <div class="LSS01B_Spc"></div>
                        <div class="LSS01B_LayText">
                            <div class="LSS01B_LayTitolo">
                                <h3 class="LSS01B_Titolo">
                                    <asp:Label ID="Contenuto1Label" runat="server" Text='<%# Eval("Titolo") %>' /></h3>
                            </div>
                            <div class="LSS01B_LayDesc"><span class="LSS01B_Testo ">
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("DescBreve1") %>' OnDataBinding="Label1_DataBinding" /></span> </div>

                        </div>
                    </div>
                </a>
            </div>

        </ItemTemplate>
    </asp:Repeater>
    <div class="LSS01B_Row">
        <asp:HyperLink ID="NewsHyperLink" runat="server"
            NavigateUrl="/AreaSoci/" 
            class="LSS01B_NoUnderline">
	<div class="LSS01B_Item">
		<div class="LSS01B_LayTextAll">
		<div class="LSS01B_LayDesc"><span class="LSS01B_Testo">&gt; <asp:Label ID="AllNewsLabel" runat="server" Text="Tutte le notizie" /></h3></span> </div>
		</div>
	</div>
        </asp:HyperLink>

    </div>
</div>
<asp:SqlDataSource ID="dsNews" runat="server"
    ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
    SelectCommand="SELECT TOP 3 [ID1Publisher], 
                        [Titolo], 
                        [DescBreve1], 
                        [UrlRewrite], 
                        [Immagine1],
                        ZeusLangCode, 
                        cast(ID1Publisher as nvarchar) +'/'+ [UrlRewrite] as LinkUrlRewrite 
                    FROM [vwPublisher_Public_DtlArea1] 
                    WHERE (ZeusIdModulo = 'PBTEM') OR (ZeusIdModulo = 'PBDCS') OR (ZeusIdModulo = 'PBFOR')
                        AND (ZeusLangCode = @Lang) 
                    ORDER BY [PWI_Area1] DESC">
    <SelectParameters>
        <asp:QueryStringParameter Name="Lang" QueryStringField="Lang" DefaultValue="ITA" />
    </SelectParameters>
</asp:SqlDataSource>
