﻿using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class UserControl_Header : System.Web.UI.UserControl
{
    public string Lang { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        MenuDataSource.SelectCommand = @"SELECT SiteMenu.ID1Menu, SiteMenu.Menu, SiteMenu.Url, SiteMenu.TargetWindow
                                        FROM vwSiteMenu_All AS SiteMenu
                                            JOIN vwSiteMenu_All AS SiteMenuParent ON SiteMenuParent.ID1Menu = SiteMenu.ID2MenuParent
                                        WHERE SiteMenu.ZeusIdModulo = 'SMPUB' AND SiteMenu.ZeusLangCode = @ZeusLangCode 
                                            AND SiteMenu.AttivoArea1 = 1 AND SiteMenu.Livello = 2
                                        ORDER BY SiteMenu.Ordinamento ASC";
        MenuDataSource.SelectParameters.Add("ZeusLangCode", System.Data.DbType.String, Lang);
    }

    protected void MenuRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        SqlDataSource MenuChildDataSource = (SqlDataSource)e.Item.FindControl("MenuChildDataSource");

        MenuChildDataSource.SelectCommand = @"SELECT Menu, Url, TargetWindow FROM vwSiteMenu_All 
                                            WHERE ZeusIdModulo = 'SMPUB' AND ZeusLangCode = @ZeusLangCode AND AttivoArea1 = 1
                                            AND Livello = 3 AND ID2MenuParent = @ID2MenuParent
                                            ORDER BY Ordinamento ASC";
        MenuChildDataSource.SelectParameters.Add("ZeusLangCode", System.Data.DbType.String, Lang);
        MenuChildDataSource.SelectParameters.Add("ID2MenuParent", DataBinder.Eval(e.Item.DataItem, "ID1Menu").ToString());

        //if (e.Item.ItemIndex == 0)
        //{
        //    HtmlGenericControl navItem = e.Item.FindControl("navItem") as HtmlGenericControl;
        //    navItem.Attributes.Add("class", "nav-item active");
        //}
    }

    protected void MenuChildRepeater_PreRender(object sender, EventArgs e)
    {
        Repeater menuChildRepeater = (Repeater)sender;
        if (menuChildRepeater.Items.Count == 0)
        {
            menuChildRepeater.Visible = false;
            ((HtmlAnchor)(menuChildRepeater).Parent.FindControl("LinkLv1DropDown")).Visible = false;
            ((HtmlAnchor)(menuChildRepeater).Parent.FindControl("LinkLv1")).Visible = true;
        }
        else
        {
            ((HtmlGenericControl)(menuChildRepeater).Parent.FindControl("navItem")).Attributes.Add("class", "nav-item dropdown");
        }
    }

}