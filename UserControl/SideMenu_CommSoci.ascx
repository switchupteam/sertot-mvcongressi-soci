﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SideMenu_CommSoci.ascx.cs" Inherits="UserControl_SideMenu_CommSoci" %>

<div class="LSS01E">
    <div class="LSS01E_LayBoxTitle">
        <h2 class="LSS01E_TitoloBox"><asp:Label ID="TitleNewsLabel" runat="server" Text="Comunicazioni ai Soci" /></h2>
    </div>
    <asp:Repeater ID="Repeater1" runat="server" DataSourceID="dsNews">
        <ItemTemplate>
            <div class="LSS01E_Row">
                <a href="<%# Eval("ID1Publisher","/AreaSoci/Comunicazione.aspx?XRI={0}") %>" class="LSS01E_NoUnderline">
                    <div class="LSS01E_Item">
                        <div class="LSS01E_LayText">
                            <div class="LSS01E_LayTitolo">
                                <h3 class="LSS01E_Titolo">
                                    <asp:Label ID="Contenuto1Label" runat="server" Text='<%# Eval("Titolo") %>' /></h3>
                            </div>
                            <div class="LSS01E_LayDesc"><span class="LSS01E_Testo ">
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("DescBreve1") %>' OnDataBinding="Label1_DataBinding" /></span> </div>

                        </div>
                    </div>
                </a>
            </div>

        </ItemTemplate>
    </asp:Repeater>
    <div class="LSS01E_Row">
        <asp:HyperLink ID="NewsHyperLink" runat="server"
            NavigateUrl="/AreaSoci/Comunicazioni.aspx" 
            class="LSS01E_NoUnderline">
	<div class="LSS01E_Item">
		<div class="LSS01E_LayTextAll">
		<div class="LSS01E_LayDesc"><span class="LSS01E_Testo">&gt; <asp:Label ID="AllNewsLabel" runat="server" Text="Tutte le comunicazioni" /></h3></span> </div>
		</div>
	</div>
        </asp:HyperLink>

    </div>
</div>
<asp:SqlDataSource ID="dsNews" runat="server"
    ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
    SelectCommand="SELECT TOP 3 [ID1Publisher], 
                        [Titolo], 
                        [DescBreve1], 
                        [UrlRewrite], 
                        [Immagine1],
                        ZeusLangCode, 
                        cast(ID1Publisher as nvarchar) +'/'+ [UrlRewrite] as LinkUrlRewrite 
                    FROM [vwPublisher_Public_DtlArea1] 
                    WHERE (ZeusIdModulo = N'PBCOS') 
                        AND (ZeusLangCode = @Lang) 
                    ORDER BY [PWI_Area1] DESC">
    <SelectParameters>
        <asp:QueryStringParameter Name="Lang" QueryStringField="Lang" DefaultValue="ITA" />
    </SelectParameters>
</asp:SqlDataSource>
