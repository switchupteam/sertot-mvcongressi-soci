﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControl_PagamentiSocio : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SqlDataSourcePagamenti.SelectCommand += " AND UserID='" + Membership.GetUser().ProviderUserKey.ToString() + "' ORDER BY Anno DESC";

        if (!Page.IsPostBack)
            GridView1.DataBind();

    } // Page_Load

    protected void ImgIco_DataBinding(object sender, EventArgs e)
    {


        Image Ico = (Image)sender;
        if (Ico.ImageUrl.ToString().Length > 0)
        {
            Ico.ImageUrl = "~/asset/img/Ico1_Attivo_On.gif";
        }
        else
        {
            Ico.ImageUrl = "~/asset/img/Ico1_Attivo_Off.gif";
        }


    }//fine ImgIco_DataBinding

    protected void BtnModificaPagamento_Click(object sender, EventArgs e)
    {
        Button myButton = (Button)sender;

        Response.Redirect("ModificaRinnovoQuota.aspx?XRI=" + myButton.ToolTip.ToString());



    }  // ModificaDettaglio

    protected void linkRicevuta_DataBinding(object sender, EventArgs e)
    {
        HyperLink hyperlink = (HyperLink)sender;

        if (!string.IsNullOrWhiteSpace(hyperlink.NavigateUrl))
            hyperlink.NavigateUrl = "/ZeusInc/Account/RicevuteSoci/" + hyperlink.NavigateUrl;
        else
            hyperlink.Visible = false;

    }
}