﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PayPal.ascx.cs" Inherits="PayPal" %>

    <div id="smart-button-container">
        <div style="text-align: center;">
            <div style="margin-bottom: 1.25rem;">
                <p>Quota Associativa Sertot</p>
                <select id="item-options">
                    <option value="Socio 2020" price="50.00">Socio 2020 - 50.00 EUR</option>
                    <option value="Socio 2021" price="50.00">Socio 2021 - 50.00 EUR</option>
                    <%--<option value="Socio specializzando" price="00.00">Socio Specializzando – Gratuito</option>--%>
                </select>
                <select style="visibility: hidden" id="quantitySelect"></select>
            </div>
            <div id="paypal-button-container"></div>
        </div>
    </div>
    <script src="https://www.paypal.com/sdk/js?client-id=sb&currency=EUR" data-sdk-integration-source="button-factory"></script>
    <script>
        function initPayPalButton() {
            var shipping = 0;
            var itemOptions = document.querySelector("#smart-button-container #item-options");
            var quantity = parseInt();
            var quantitySelect = document.querySelector("#smart-button-container #quantitySelect");
            if (!isNaN(quantity)) {
                quantitySelect.style.visibility = "visible";
            }
            var orderDescription = 'Quota Associativa Sertot';
            if (orderDescription === '') {
                orderDescription = 'Item';
            }
            paypal.Buttons({
                style: {
                    shape: 'pill',
                    color: 'silver',
                    layout: 'vertical',
                    label: 'pay',

                },
                createOrder: function (data, actions) {
                    var selectedItemDescription = itemOptions.options[itemOptions.selectedIndex].value;
                    var selectedItemPrice = parseFloat(itemOptions.options[itemOptions.selectedIndex].getAttribute("price"));
                    var tax = (0 === 0) ? 0 : (selectedItemPrice * (parseFloat(0) / 100));
                    if (quantitySelect.options.length > 0) {
                        quantity = parseInt(quantitySelect.options[quantitySelect.selectedIndex].value);
                    } else {
                        quantity = 1;
                    }

                    tax *= quantity;
                    tax = Math.round(tax * 100) / 100;
                    var priceTotal = quantity * selectedItemPrice + parseFloat(shipping) + tax;
                    priceTotal = Math.round(priceTotal * 100) / 100;
                    var itemTotalValue = Math.round((selectedItemPrice * quantity) * 100) / 100;

                    return actions.order.create({
                        purchase_units: [{
                            description: orderDescription,
                            amount: {
                                currency_code: 'EUR',
                                value: priceTotal,
                                breakdown: {
                                    item_total: {
                                        currency_code: 'EUR',
                                        value: itemTotalValue,
                                    },
                                    shipping: {
                                        currency_code: 'EUR',
                                        value: shipping,
                                    },
                                    tax_total: {
                                        currency_code: 'EUR',
                                        value: tax,
                                    }
                                }
                            },
                            items: [{
                                name: selectedItemDescription,
                                unit_amount: {
                                    currency_code: 'EUR',
                                    value: selectedItemPrice,
                                },
                                quantity: quantity
                            }]
                        }]
                    });
                },
                onApprove: function (data, actions) {
                    return actions.order.capture().then(function (details) {
                        alert('Transaction completed by ' + details.payer.name.given_name + '!');
                    });
                },
                onError: function (err) {
                    console.log(err);
                },
            }).render('#paypal-button-container');
        }
        initPayPalButton();
    </script>
