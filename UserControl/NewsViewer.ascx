﻿<%@ Control Language="C#" ClassName="NewsViewer"  CodeFile="NewsViewer.ascx.cs" Inherits="NewsViewer" %>

<%@ Register Src="~/UserControl/ZeusDataListPager.ascx" TagName="ZeusDataListPager" TagPrefix="uc1" %>

<script runat="server">
    
</script>
       <asp:Panel ID="AllPanel" runat="server">
            <div class="LSS01C">
                <div class="LSS01C_LayBoxTitle"><div class="LSS01C_TitoloBox">Ultimi contenuti riservati ai Soci</div></div>
                <asp:DataList ID="NewsEdEventiRepeater" runat="server"
                    RepeatColumns="1" RepeatDirection="Vertical" RenderOuterTable="false"
                    CellPadding="0" CellSpacing="0" BorderWidth="0"
                    OnItemDataBound="NewsEdEventiRepeater_ItemDataBound" DataKeyField="ID1Publisher">
                    <ItemTemplate>
                        <div class="LSS01C_Row">
                            <asp:HyperLink runat="server" ID="Collegamento" NavigateUrl="/AreaSoci/" class="LSS01C_NoUnderline">
                            <%--<a href="<%# Eval("ID1Publisher","/AreaSoci/Notizia.aspx?XRI={0}") %>" class="LSS01C_NoUnderline">--%>
                            <div class="LSS01C_Item">
                                <div class="LSS01C_ImgThumb">
                                    <img src="<%# Eval("Immagine1","/ZeusInc/Publisher/Img1/{0}") %>" border="0" /></div>
                                <div class="LSS01C_LayText">
                                    <div class="LSS01C_LayTitolo">
                                        <h3 class="LSS01C_Titolo bold">
                                            <%# Eval("Titolo")%>
                                        </h3>
                                    </div>
<%--                                    <div class="LSS01C_LayDesc">
                                        <span class="LSS01C_Testo">
                                            <%# Eval("PWI_Area1", "{0:d}") + " - " + Eval("CatLiv3")%>
                                        </span>
                                    </div>--%>
                                    <div class="LSS01C_LayDesc">
                                        <span class="LSS01C_Testo">
                                            <%# Eval("DescBreve1")%>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </asp:HyperLink>
                    </div>
                    </ItemTemplate>
                </asp:DataList>
            </div>
            <asp:SqlDataSource ID="NewsEdEventiSqlDataSource" runat="server"
                ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
                OnSelected="NewsEdEventiSqlDataSource_Selected"></asp:SqlDataSource>
            <uc1:ZeusDataListPager ID="ZeusDataListPager1" runat="server"
                SqlDataSourceID="NewsEdEventiSqlDataSource"
                DataListID="NewsEdEventiRepeater" PageSize="15" />
        </asp:Panel>
        <asp:Panel ID="EmptyPanel" runat="server" Visible="false">
            <div style="text-align:center; font-weight: bold; padding: 5px;">
                <asp:Label ID="ContenutoNonDisponibileLabel" runat="server">Nessun contenuto disponibile</asp:Label> .
            </div>
        </asp:Panel>
