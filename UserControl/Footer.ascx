﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Footer.ascx.cs" Inherits="UserControl_Footer" %>

<div class="upperArea">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-6 col-md-4 col-lg-3 mx-auto mr-md-auto">
                <a href="/">
                    <div class="logo">
                        <img src="/asset/img/logo-sertot-white.png" />
                    </div>
                </a>
            </div>
            <div class="col-md-8 col-lg-9 ml-md-auto">
                <ul class="menu">
                    <asp:Repeater runat="server" ID="NavbarFooter_Repeater" DataSourceID="NavbarFooter_DataSource">
                        <ItemTemplate>
                            <li class="itemLink">
                                <a href='<%# Eval("Url") %>' target='<%# Eval("TargetWindow") %>'><%# Eval("Menu") %></a>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                    <asp:SqlDataSource runat="server" ID="NavbarFooter_DataSource" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>" />
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="lowerArea">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <p class="info">MV Congressi spa - Via Marchesi 26/D - 43125 Parma, Italy - Ph +39 0521 290191 email: <a href="mailto:sertot@mvcongressi.it">sertot@mvcongressi.it</a></p>
            </div>
            <div class="col-lg-4">
                <ul class="menuPrivacy">
                    <li><a href="/Web/57/Privacy-policy/">Privacy</a></li>
                    <li><a href="/Web/59/Cookie-policy/">Cookie</a></li>
                    <li><a href="/Web/58/Legal-notices/">Legal notices</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
