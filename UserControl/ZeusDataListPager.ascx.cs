﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControl_ZeusDataListPager : System.Web.UI.UserControl
{
    private string mySqlDataSourceID = string.Empty;
    private string myDataListID = string.Empty;
    private string myPageSize = "10";

    public string SqlDataSourceID
    {
        get { return mySqlDataSourceID; }
        set { mySqlDataSourceID = value; }
    }

    public string DataListID
    {
        get { return myDataListID; }
        set { myDataListID = value; }
    }

    public string PageSize
    {
        get { return myPageSize; }
        set { myPageSize = value; }
    }

    public int CurrentPage
    {
        get
        {
            object o = this.ViewState["_CurrentPage"];
            if (o == null)
                return 0;
            else
                return (int)o;
        }
        set
        {
            this.ViewState["_CurrentPage"] = value;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            ItemsGet(false, true);
    }

    private Control FindControlRecursive(Control Root, string Id)
    {
        if (Root.ID == Id)
            return Root;

        foreach (Control Ctl in Root.Controls)
        {
            Control FoundCtl = FindControlRecursive(Ctl, Id);
            if (FoundCtl != null)
                return FoundCtl;
        }

        return null;
    }//fine FindControlRecursive


    private string GetItemCount(string mySqlQuery, string myDataKeyField)
    {
        string ItemCount = string.Empty;


        if (mySqlQuery.Length == 0)
            return ItemCount;

        if (myDataKeyField.Length == 0)
            return ItemCount;


        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        string sqlQuery = string.Empty;


        sqlQuery = mySqlQuery.Substring(mySqlQuery.ToUpper().IndexOf("FROM"));

        sqlQuery = "SELECT  COUNT(DISTINCT " + myDataKeyField + ") " + sqlQuery;

        //Response.Write("DEB "+ sqlQuery.IndexOf(" ORDER") + "<br />");

        sqlQuery =
            sqlQuery.Substring(0, sqlQuery.Length - (sqlQuery.Length - sqlQuery.IndexOf(" ORDER")));

        try
        {

            using (SqlConnection connection = new SqlConnection(strConnessione))
            {

                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = sqlQuery;

                ItemCount = ((int)command.ExecuteScalar()).ToString();

            }//fine Using
        }
        catch (Exception p)
        {

            Response.Write(p.ToString());

        }

        return ItemCount;


    }//fine ItemCount

    private bool ItemsGet(bool last, bool IsLoadingFirst)
    {

        try
        {

            if (mySqlDataSourceID.Length == 0)
                return false;

            if (myDataListID.Length == 0)
                return false;


            SqlDataSource SqlDataSource1 = (SqlDataSource)FindControlRecursive(Page, mySqlDataSourceID);

            if (SqlDataSource1 == null)
                return false;


            DataList DataList1 = (DataList)FindControlRecursive(Page, myDataListID);

            if (DataList1 == null)
                return false;



            PagedDataSource objPds = new PagedDataSource();
            DataView dv = (DataView)SqlDataSource1.Select(DataSourceSelectArguments.Empty);


            objPds.DataSource = dv;
            objPds.AllowPaging = true;
            objPds.PageSize = Convert.ToInt32(myPageSize);

            if (last)
                CurrentPage = objPds.PageCount - 1;

            objPds.CurrentPageIndex = CurrentPage;


            lblCurrentPage.Text = "Pagina: " + (CurrentPage + 1).ToString() + " di "
                + objPds.PageCount.ToString();

            if ((DataList1.DataKeyField.Length > 0)
                && (IsLoadingFirst))
                lblTotale.Text = "Risultati: " + GetItemCount(SqlDataSource1.SelectCommand, DataList1.DataKeyField);


            ArrayList myTotalPages = new ArrayList();

            for (int a = 1; a <= objPds.PageCount; a++)
                myTotalPages.Add(a);


            SelettoreDropDownList.DataSource = myTotalPages;
            SelettoreDropDownList.DataBind();

            SelettoreDropDownList.SelectedIndex = CurrentPage;

            cmdPrev.Enabled = !objPds.IsFirstPage;
            cmdFirst.Enabled = !objPds.IsFirstPage;
            cmdNext.Enabled = !objPds.IsLastPage;
            cmdLast.Enabled = !objPds.IsLastPage;

            DataList1.DataSource = objPds;
            DataList1.DataBind();

            return true;

        }
        catch (Exception p)
        {
            Response.Write(p.ToString());
            return false;
        }

    }//fine ItemsGet

    protected void LoadButton_Click(object sender, EventArgs e)
    {
        CurrentPage = Convert.ToInt32(SelettoreDropDownList.SelectedItem.Text) - 1;
        ItemsGet(false, false);

    }//fine LoadButton_Click

    protected void cmdPrev_Click(object sender, System.EventArgs e)
    {

        CurrentPage -= 1;
        ItemsGet(false, false);
    }//fine cmdPrev_Click

    protected void cmdNext_Click(object sender, System.EventArgs e)
    {

        CurrentPage += 1;
        ItemsGet(false, false);
    }//fine cmdNext_Click

    protected void cmdFirst_Click(object sender, System.EventArgs e)
    {
        CurrentPage = 0;
        ItemsGet(false, false);
    }//fine cmdFirst_Click

    protected void cmdLast_Click(object sender, System.EventArgs e)
    {
        ItemsGet(true, false);
    }//fine cmdLast_Click
    
}