﻿using System;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;

public partial class UserControl_MenuSoci : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        delinea myDelinea = new delinea();

        NumSocio.Text = HttpContext.Current.User.Identity.Name;

        SetNomeECognomeUtente();

        if (IsZeusRole())
        {
            ToZeusButton.NavigateUrl += myDelinea.GetLang(Request.QueryString);
            ToZeusButton.Visible = true;
        }
    }

    private bool IsZeusRole()
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        using (SqlConnection connection = new SqlConnection(strConnessione))
        {
            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                string sqlQuery = string.Empty;
                sqlQuery = "SELECT dbo.vw_aspnet_UsersInRoles.UserId, dbo.tbZeusRoles.IsZeusRole ";
                sqlQuery += "FROM dbo.tbZeusRoles INNER JOIN ";
                sqlQuery += " dbo.vw_aspnet_UsersInRoles ON dbo.tbZeusRoles.RoleId = dbo.vw_aspnet_UsersInRoles.RoleId ";
                sqlQuery += " WHERE (dbo.tbZeusRoles.IsZeusRole = 1) ";
                sqlQuery += " AND (UserId=@UserId) ";

                MembershipUser user = Membership.GetUser();

                command.CommandText = sqlQuery;
                command.Parameters.Add("@UserId", System.Data.SqlDbType.UniqueIdentifier);
                command.Parameters["@UserId"].Value = user.ProviderUserKey;

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    if (reader["UserId"] != null)
                        return true;
                }

                reader.Close();
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
            }
        }

        return false;
    }

    private void SetNomeECognomeUtente()
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;
        using (SqlConnection connection = new SqlConnection(strConnessione))
        {
            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                string sqlQuery = string.Empty;
                sqlQuery = @"SELECT P_PERSONALI.Nome, P_PERSONALI.Cognome
                            FROM tbProfiliPersonali AS P_PERSONALI
	                            INNER JOIN aspnet_Users AS UTENTI ON UTENTI.UserId = P_PERSONALI.UserId
                            WHERE UTENTI.UserName = @UserName";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar);
                command.Parameters["@UserName"].Value = HttpContext.Current.User.Identity.Name;

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        NomeSocio.Text = reader["Nome"].ToString();
                        CognomeSocio.Text = reader["Cognome"].ToString();
                    }
                }
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());
            }
        }
    }
}