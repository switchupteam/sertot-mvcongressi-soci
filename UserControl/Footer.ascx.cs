﻿using System;

public partial class UserControl_Footer : System.Web.UI.UserControl
{
    public string Lang { get; set; }    

    protected void Page_Load(object sender, EventArgs e)
    {
        NavbarFooter_DataSource.SelectCommand = @"SELECT SiteMenu.ID1Menu, SiteMenu.Menu, SiteMenu.Url, SiteMenu.TargetWindow
                                                FROM vwSiteMenu_All AS SiteMenu
                                                    JOIN vwSiteMenu_All AS SiteMenuParent ON SiteMenuParent.ID1Menu = SiteMenu.ID2MenuParent
                                                WHERE SiteMenu.ZeusIdModulo = 'SMFOO' AND SiteMenu.ZeusLangCode = @ZeusLangCode 
                                                    AND SiteMenu.Livello = 3
                                                ORDER BY SiteMenu.Ordinamento ASC";
        NavbarFooter_DataSource.SelectParameters.Add("ZeusLangCode", System.Data.DbType.String, Lang);
    }
}