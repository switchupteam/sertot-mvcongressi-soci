﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Header.ascx.cs" Inherits="UserControl_Header" %>



<div class="container">
    <div class="upperArea">
        <div class="row justify-content-center align-items-center">
            <div class="col-6 col-md-4 col-lg-3 mr-md-auto">
                <a href="/">
                    <div class="logo">
                        <img src="/asset/img/logo-sertot.png" />
                    </div>
                </a>
            </div>
            <div class="col-md-7 col-lg-5 col-xl-4 ml-md-auto">
                <div class="row">
                   <div class="col-6">
<%--                        <div class="areaLink pazienti">
                            <a href="">A</a>
                        </div>--%>
                    </div>
                    <div class="col-6">
                        <div class="areaLink soci">
                            <a href="/AreaSoci/">Area Soci</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<nav class="navbar navbar-expand-lg navbar-light">
    <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <asp:Repeater runat="server" ID="MenuRepeater" DataSourceID="MenuDataSource" OnItemDataBound="MenuRepeater_ItemDataBound">
                    <ItemTemplate>
                        <li runat="server" id="navItem" class="nav-item">
                            <a runat="server" id="LinkLv1" class="nav-link" visible="false" href='<%# Eval("Url") %>' target='<%# Eval("TargetWindow") %>'><%# Eval("Menu") %></a>
                            <a runat="server" id="LinkLv1DropDown" class="nav-link dropdown-toggle" href='<%# Eval("Url") %>' role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><%# Eval("Menu") %> <i class="fas fa-angle-down"></i></a>
                            <asp:Repeater runat="server" ID="MenuChildRepeater" DataSourceID="MenuChildDataSource" OnPreRender="MenuChildRepeater_PreRender">
                                <HeaderTemplate>
                                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li>
                                        <a href='<%# Eval("Url") %>' target="<%# Eval("TargetWindow") %>" class="dropdown-item"><%# Eval("Menu") %></a>
                                    </li>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </ul>
                                </FooterTemplate>
                            </asp:Repeater>
                            <asp:SqlDataSource runat="server" ID="MenuChildDataSource" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>" />
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
                <asp:SqlDataSource runat="server" ID="MenuDataSource" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>" />
            </ul>
        </div>
    </div>
</nav>
