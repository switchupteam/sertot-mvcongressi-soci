﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Descrizione di riepilogo per Default
/// </summary>
public partial class NewsViewer : System.Web.UI.UserControl
{

    #region Fields

    delinea myDelinea = new delinea();
    SertotUtility mySertotUtility = new SertotUtility();

    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {

        string str = "";
        string str2 = "";

        if ((!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "Lang") && Request.QueryString["Lang"] != null)
            || (!myDelinea.AntiSQLInjectionLeft(Request.QueryString, "XRI")
                && Request.QueryString["XRI"] != null))
            Response.Redirect("~/System/Message.aspx?0");

        if (Request.QueryString["LANG"] != null)
        {
            str = Request.QueryString["LANG"];
        }

        if (Request.QueryString["XRI"] != null)
        {
            str2 = Request.QueryString["XRI"];
        }

        if (str.Length > 0)
        {
            str = " AND (ZeusLangCode = '" + str + "') ";
        }
        else
            str = " AND (ZeusLangCode = 'ITA')";

        if (str2.Length > 0)
        {
            //TitleLabel.Text += " - " + GetCatLiv3Name(str2);
            str2 = " AND (ID2Categoria1 = '" + str2 + "') ";   

        }
        else
            str2 = "";


        NewsEdEventiSqlDataSource.SelectCommand = @"SELECT  ID1Publisher, 
                                                        Titolo, 
                                                        Immagine1, 
                                                        PWI_Area1, 
                                                        DescBreve1,
                                                        CatLiv3,
                                                        UrlRewrite, 
                                                        ID2CatLiv2,
                                                        ID1Publisher AS Expr1, 
                                                        cast(ID1Publisher as nvarchar) +'/'+ [UrlRewrite] as LinkUrlRewrite 
                                                    FROM vwPublisher_Public_DtlArea1 
                                                    WHERE (ZeusIdModulo = 'PBTEM') OR (ZeusIdModulo = 'PBDCS') OR (ZeusIdModulo = 'PBFOR') OR (ZeusIdModulo = 'DOCVD') OR (ZeusIdModulo = 'DOCVC')  OR (ZeusIdModulo = 'DOCVG') OR (ZeusIdModulo = 'DOCVU') OR (ZeusIdModulo = 'WBNAR') " + str + "  " + str2
                                                    + " ORDER BY PWI_Area1 DESC";

    }

    protected void NewsEdEventiSqlDataSource_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.AffectedRows == 0 || e.Exception != null)
        {
            AllPanel.Visible = false;
            EmptyPanel.Visible = true;
        }
    }

    #endregion

    #region Methods

    private string GetCatLiv3Name(string XRI)
    {
        String strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

        string sqlQuery = string.Empty;
        string myReturn = string.Empty;

        using (SqlConnection connection = new SqlConnection(
           strConnessione))
        {

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();


                sqlQuery = "SELECT CatLiv3 FROM vwCategorie_All WHERE ID1Categoria=@XRI";
                command.CommandText = sqlQuery;
                command.Parameters.Add("@XRI", System.Data.SqlDbType.Int);
                command.Parameters["@XRI"].Value = Convert.ToInt32(XRI);

                SqlDataReader reader = command.ExecuteReader();
                
                while (reader.Read())
                {
                    
                        return reader["CatLiv3"].ToString();
                }
                reader.Close();
            }
            catch (Exception p)
            {
                Response.Write(p.ToString());

                return "";
            }
            return "";
        }//fine Using


        //return false;

    }//fine GetIDParentCategory

    #endregion

    protected void NewsEdEventiRepeater_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        string ZeusIdModulo = string.Empty;
        
        if (e.Item.ItemType == ListItemType.Item || 
             e.Item.ItemType == ListItemType.AlternatingItem)
        {
            // --- modifico il percorso a seconda del tipo di dato (ZeusIdModulo)

            ZeusIdModulo = mySertotUtility.GetZeusIdModulo(Convert.ToInt32(((DataRowView)e.Item.DataItem).Row["ID1Publisher"].ToString()));

            HyperLink myHyperLink = (HyperLink)e.Item.FindControl("Collegamento");


            //TODO: Controllare il funzionamento di questi url, per le sezioni attive del menù laterale
            switch (ZeusIdModulo)
            {
                //case "DOCVG":
                //    myHyperLink.NavigateUrl = "/DocumentiConvegni/" + ((DataRowView)e.Item.DataItem).Row["ID1Publisher"].ToString() + "/" + ((DataRowView)e.Item.DataItem).Row["UrlRewrite"].ToString();
                //    break;
                //case "DOCVC":
                //    myHyperLink.NavigateUrl = "/VerbaliCIIP/" + ((DataRowView)e.Item.DataItem).Row["ID1Publisher"].ToString() + "/" + ((DataRowView)e.Item.DataItem).Row["UrlRewrite"].ToString();
                //    break;
                //case "DOCVD":
                //    myHyperLink.NavigateUrl = "/VerbaliDirettivo/" + ((DataRowView)e.Item.DataItem).Row["ID1Publisher"].ToString() + "/" + ((DataRowView)e.Item.DataItem).Row["UrlRewrite"].ToString();
                //    break;
                //case "DOCVU":
                //    myHyperLink.NavigateUrl = "/VerbaliPresidenza/" + ((DataRowView)e.Item.DataItem).Row["ID1Publisher"].ToString() + "/" + ((DataRowView)e.Item.DataItem).Row["UrlRewrite"].ToString();
                //    break;
                //case "WBNAR":
                //    myHyperLink.NavigateUrl = "/Webinar/" + ((DataRowView)e.Item.DataItem).Row["ID1Publisher"].ToString() + "/" + ((DataRowView)e.Item.DataItem).Row["UrlRewrite"].ToString();
                //    break;
                default:
                    myHyperLink.NavigateUrl = "/AreaSoci/Notizia.aspx?XRI=" + ((DataRowView)e.Item.DataItem).Row["ID1Publisher"].ToString();
                    break;
            }
        }
    }
}