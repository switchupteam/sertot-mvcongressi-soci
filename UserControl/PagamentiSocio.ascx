﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PagamentiSocio.ascx.cs" Inherits="UserControl_PagamentiSocio" %>

<asp:GridView ID="GridView1" runat="server" DataSourceID="SqlDataSourcePagamenti" EnableModelValidation="True" DataKeyNames="ID1QuotaAnnuale" Width="100%">
    <Columns>
        <asp:BoundField DataField="Anno" HeaderText="Anno" SortExpression="Anno">
            <ItemStyle HorizontalAlign="center" />
        </asp:BoundField>

        <asp:TemplateField HeaderText="Pag. Verif.">
            <ItemTemplate>
                <asp:Image ID="ImgIcoAccesso" runat="server" OnDataBinding="ImgIco_DataBinding" ImageUrl='<%# Eval("DataVerifica", "{0}") %>' />
            </ItemTemplate>
            <ItemStyle HorizontalAlign="Center" />
        </asp:TemplateField>

        <asp:BoundField DataField="DescrizioneQuota" HeaderText="Tipologia associazione" SortExpression="DescrizioneQuota">
            <ItemStyle HorizontalAlign="Left" />
        </asp:BoundField>

        <asp:BoundField DataField="TotaleQuota" HeaderText="Quota EUR" SortExpression="TotaleQuota">
            <ItemStyle HorizontalAlign="right" />
        </asp:BoundField>

        <asp:BoundField DataField="metodoPagamento" HeaderText="Metodo pagamento" SortExpression="metodoPagamento">
            <ItemStyle HorizontalAlign="Left" />
        </asp:BoundField>

        <asp:BoundField DataField="NoteSocio" HeaderText="Dettagli e note" SortExpression="NoteSocio">
            <ItemStyle HorizontalAlign="Left" />
        </asp:BoundField>
        <asp:TemplateField HeaderText="Modifica" ItemStyle-HorizontalAlign="Center">
            <ItemTemplate>
                <asp:Button ID="BtnModificaPagamento" runat="server" Text=" Modifica " ToolTip='<%# Eval("ID1QuotaAnnuale") %>' OnClick="BtnModificaPagamento_Click" SkinID="ZSSM_Button01" CssClass="ZSSM_Button01_Button" />
            </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="Ricevute" ItemStyle-HorizontalAlign="Center">
            <ItemTemplate>
                <asp:HyperLink ID="linkRicevuta" runat="server" Text=" Ricevuta " CssClass="ZSSM_Button01_Button" OnDataBinding="linkRicevuta_DataBinding" NavigateUrl='<%# Eval("Ricevuta") %>' Target="_blank"></asp:HyperLink>
            </ItemTemplate>
        </asp:TemplateField>

        <%--                  <asp:HyperLinkField DataNavigateUrlFields="IDPagamento" HeaderText="Modifica" DataNavigateUrlFormatString="Pagamenti_Edt.aspx?XRI={0}" 
                    Text="Modifica">
                    <ControlStyle CssClass="GridView_ZeusButton1" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:HyperLinkField>--%>

        <%--<asp:TemplateField HeaderText="Approva" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                
                <asp:Button ID="BtnModificaApprova" runat="server" Text=" Approva "  OnClick="BtnModificaApprova_Click" SkinID="ZSSM_Button01" ToolTip='<%# Eval("ID1QuotaAnnuale") %>' CssClass="ZSSM_Button01_Button"/>
   </ItemTemplate>
                                </asp:TemplateField>--%>
    </Columns>
</asp:GridView>
<asp:SqlDataSource ID="SqlDataSourcePagamenti" runat="server" ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>" 
    SelectCommand="SELECT [ID1QuotaAnnuale], [Anno], [DataVerifica], [DescrizioneQuota],[metodoPagamento], [PagamentoEur], [TotaleQuota], [NoteSocio], [Ricevuta] FROM [vwQuoteAnnuali_Lst] WHERE ID1QuotaAnnuale>0"></asp:SqlDataSource>

