﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CommViewer.ascx.cs" Inherits="UserControl_CommmViewer" %>

<%@ Register Src="~/UserControl/ZeusDataListPager.ascx" TagName="ZeusDataListPager" TagPrefix="uc1" %>


<asp:Panel ID="AllPanel" runat="server">
    <div class="LSS01D">
        <div class="LSS01D_LayBoxTitle">
            <div class="LSS01D_TitoloBox">Ultime comunicazioni per i Soci</div>
        </div>
        <asp:DataList ID="NewsEdEventiRepeater" runat="server"
            RepeatColumns="1"
            RepeatDirection="Vertical"
            RepeatLayout="Flow"
            RenderOuterTable="false"
            CellPadding="0"
            CellSpacing="0"
            BorderWidth="0"
            DataKeyField="ID1Publisher">
            <ItemTemplate>
                <div class="LSS01D_Row">
                    <%--<a href="<%# Eval("LinkUrlRewrite","/Notizia/{0}.aspx") %>" class="LSS01D_NoUnderline">--%>
                    <a href="<%# Eval("ID1Publisher","/AreaSoci/Comunicazione.aspx?XRI={0}") %>" class="LSS01D_NoUnderline">
                        <div class="LSS01D_Item">
                            <%--<div class="LSS01D_ImgThumb">
                                    <img src="<%# Eval("Immagine1","/ZeusInc/Publisher/Img1/{0}") %>" border="0" /></div>--%>
                            <div class="LSS01D_LayText">
                                <div class="LSS01D_LayTitolo">
                                    <h3 class="LSS01D_Titolo">
                                        <%# Eval("Titolo")%>
                                    </h3>
                                </div>
                                <div class="LSS01D_LayDesc">
                                    <span class="LSS01D_Testo">
                                        <%# Eval("PWI_Area1", "{0:d}") %>
                                    </span>
                                </div>
                                <div class="LSS01D_LayDesc">
                                    <span class="LSS01D_Testo">
                                        <%# Eval("DescBreve1")%>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </ItemTemplate>
        </asp:DataList>
    </div>
    <asp:SqlDataSource ID="NewsEdEventiSqlDataSource" runat="server"
        ConnectionString="<%$ ConnectionStrings:ZeusConnectionString %>"
        OnSelected="NewsEdEventiSqlDataSource_Selected"></asp:SqlDataSource>
    <uc1:ZeusDataListPager ID="ZeusDataListPager1" runat="server"
        SqlDataSourceID="NewsEdEventiSqlDataSource"
        DataListID="NewsEdEventiRepeater" PageSize="10" />
</asp:Panel>
<asp:Panel ID="EmptyPanel" runat="server" Visible="false">
    <div align="center" style="font-weight: bold; padding: 5,5,5,5;">
        <asp:Label ID="ContenutoNonDisponibileLabel" runat="server">Nessun contenuto disponibile</asp:Label>
        .
    </div>
</asp:Panel>
