﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.HtmlControls;

public partial class Corsi_Archivio : System.Web.UI.Page
{
    string strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        /*****************************************************/
        /**************** INIZIO SEO *************************/
        /*****************************************************/
        HtmlMeta description = new HtmlMeta();
        HtmlMeta keywords = new HtmlMeta();
        description.Name = "description";
        keywords.Name = "keywords";
        Page.Title = "Eventi patrocinati | " + AresConfig.GetBaseTitle();
        description.Content = "";
        keywords.Content = "";
        HtmlHead head = Page.Header;
        head.Controls.Add(description);
        head.Controls.Add(keywords);
        /*****************************************************/
        /**************** FINE SEO ***************************/
        /*****************************************************/


        BindPubblicazioni();
    }

    private void BindPubblicazioni()
    {
        //Load Books
        var books = new List<book>();

        string sqlQuery = string.Empty;
        //var provincie = new List<ProvinciaItem>();
        using (SqlConnection connection = new SqlConnection(strConnessione))
        {
            connection.Open();
            SqlCommand command = connection.CreateCommand();

            sqlQuery = @"SELECT PUB.ID1Publisher, PUB.ID2Categoria1, PUB.Titolo, CAT.CatLiv2Liv3, PUB.PWI_Area1, PUB.PWF_Area1, PUB.PWI_Area2, 
                            PUB.PWF_Area2, PUB.Archivio, PUB.ZeusIdModulo, PUB.ZeusLangCode, PUB.PWI_Area3, PUB.PWF_Area3, PUB.Immagine1, 
                            PUB.DescBreve1, PUB.Contenuto1, PUB.Contenuto2, PUB.Contenuto3, PUB.Immagine2, PUB.TagDescription, PUB.TagKeywords, 
                            PUB.UrlRewrite, PUB.Autore, PUB.Autore_Email, PUB.Fonte, PUB.Fonte_Url, PUB.TitoloBrowser, PUB.PW_Area2 + DATEDIFF(day, 
                            PUB.PWI_Area2, GETDATE()) - ABS(DATEDIFF(day, PUB.PWI_Area2, GETDATE())) + DATEDIFF(day, GETDATE(), PUB.PWF_Area2) - ABS(DATEDIFF(day, GETDATE(), PUB.PWF_Area2)) 
                            AS AttivoArea2, PUB.Allegato1_Selettore, PUB.Allegato1_UrlEsterno, PUB.Allegato1_File, CAT.CatLiv3, CAT.ID2CategoriaParent AS ID2CatLiv2, 
                            PUB.Sottotitolo
                            , tbPZ_ImageRaider.ImgBeta_Path
                        FROM tbPublisher AS PUB
	                        LEFT OUTER JOIN vwCategorie_All AS CAT ON PUB.ID2Categoria1 = CAT.ID1Categoria
	                        INNER JOIN tbPZ_ImageRaider on ZeusIdModuloIndice = PUB.ZeusIdModulo + '1'
                        WHERE (PUB.ZeusIsAlive = 1) 
	                        AND (PUB.PW_Area3 = 1 AND (GETDATE() BETWEEN PUB.PWI_Area3 AND PUB.PWF_Area3))
	                        AND PUB.ZeusIdModulo = @ZeusIdModulo
                        ORDER BY PWI_Area2 DESC, PWI_Area1 DESC";

            command.CommandText = sqlQuery;
            command.Parameters.Add("@ZeusIdModulo", System.Data.SqlDbType.VarChar);
            command.Parameters["@ZeusIdModulo"].Value = "PBCOR";

            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                var book = new book()
                {
                    Image = !string.IsNullOrEmpty(reader["Immagine1"].ToString()) ? string.Concat(reader["ImgBeta_Path"].ToString().Replace("~", ""), reader["Immagine1"].ToString()) : "",
                    Title = reader["Titolo"].ToString(),
                    Authors = reader["Contenuto1"].ToString(),
                    Desc = reader["DescBreve1"].ToString(),
                    Link = string.Format("/{0}/{1}/{2}", "Corso", reader["ID1Publisher"].ToString(), reader["UrlRewrite"].ToString())
                };
                books.Add(book);
            }

            if (books.Count == 0)
            {
                NessunElemento.Visible = true;
                rptBooks.Visible = false;
            }
            else
            {
                rptBooks.DataSource = books;
                rptBooks.DataBind();
            }

            reader.Close();
            connection.Close();
        }
    }

    private void SetCatTitle(int XRI)
    {
        string sqlQuery = string.Empty;
        using (SqlConnection connection = new SqlConnection(strConnessione))
        {
            connection.Open();
            SqlCommand command = connection.CreateCommand();

            sqlQuery = @"SELECT [Categoria], [Descrizione1] FROM tbCategorie 
                            WHERE ID1Categoria = @ID1Categoria ";

            command.CommandText = sqlQuery;
            command.Parameters.Add("@ID1Categoria", System.Data.SqlDbType.Int);
            command.Parameters["@ID1Categoria"].Value = XRI;

            SqlDataReader reader = command.ExecuteReader();

            if (reader.Read())
            {
                //litCatName.Text = reader["Categoria"].ToString();
                litDescription.Text = reader["Descrizione1"].ToString();
            };
            reader.Close();
            connection.Close();
        }

    }

    public class book
    {
        public string Image { get; set; }
        public string Title { get; set; }
        public string Authors { get; set; }
        public string Desc { get; set; }
        public string Link { get; set; }
    }
}