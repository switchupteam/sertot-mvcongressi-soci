﻿<%@ Page Language="C#" MasterPageFile="~/Master/LayoutMain.master" CodeFile="Patrocinati.aspx.cs" Inherits="Corsi_Archivio" %>

<asp:Content ID="Content2" ContentPlaceHolderID="AreaTitle" runat="Server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="AreaContentMain" runat="Server">
    <section class="corsi">
        <div class="container">
            <div class="innerBox">
                <div class="boxTitolo">
                    <h2>Eventi patrocinati</h2>
                    <hr />
                </div>

                <p class="descrizione">
                    <asp:Literal ID="litDescription" runat="server"></asp:Literal>
                </p>


                <asp:Panel runat="server" ID="NessunElemento" Visible="false">
                    <h5>Al momento non sono presenti eventi patrocinati.</h5>
                </asp:Panel>

                <asp:Repeater ID="rptBooks" runat="server">
                    <ItemTemplate>
                        <div class="corso">
                            <%--<div class="row align-items-end mb-5">--%>
                            <div class="row mb-5">
                                <div class="col-2">
                                    <div class="immagineLibro">
                                        <img src='<%# Eval("Image") %>' />
                                    </div>
                                </div>
                                <div class="col-10">
                                    <div class="testoLibro">
                                        <h6 class="volume"><%# Eval("Desc") %></h6>
                                        <h5 class="titolo"><%# Eval("Title") %></h5>
                                        <div class="autori"><%# Eval("Authors") %></div>                                        
                                        <div class="link">
                                            <a href="<%#Eval("Link") %>">Apri Dettagli</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </section>
</asp:Content>
