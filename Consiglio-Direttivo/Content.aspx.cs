﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;

public partial class Web_Content : System.Web.UI.Page
{
    protected string title = string.Empty;
    protected string contenuto1 = string.Empty;
    protected string contenuto2 = string.Empty;

    string strConnessione = System.Configuration.ConfigurationManager.ConnectionStrings["ZeusConnectionString"].ConnectionString;


    protected void Page_Load(object sender, EventArgs e)
    {

        AresUtilities aresUtilities = new AresUtilities();
        string lang = aresUtilities.GetLangRewritted();

        int XRI = 0;

        int.TryParse(Request.QueryString["XRI"], out XRI);
        var lateralItems = GetAllConsiglioDirettivo();
        rptNavRight.DataSource = lateralItems;
        rptNavRight.DataBind();
        if (XRI == 0 && lateralItems.Any())
        {
            XRI = lateralItems.FirstOrDefault().id;
        }



        BindCurrentConsiglioDirettivo(XRI);





        //rptBottoni.DataSource = new List<dynamic>() {
        //    new { imgsrc = "/asset/img/bilanci.png",  href = "#"},
        //      new { imgsrc = "/asset/img/statuto.png",  href = "#"}

        //};
        //rptBottoni.DataBind();

    }


    private List<LateralNavItem> GetAllConsiglioDirettivo()
    {
        string sqlQuery = @"SELECT [ID1Pagina]
                                  ,[TitoloPagina]
                              FROM [dbo].[vwPageDesigner_Public_Lst]
                              WHERE  [ZeusIdModulo] = 'PDCOD'  AND [ZeusLangCode] = 'ITA'
                              ORDER BY PWI_Area1 DESC";

        List<LateralNavItem> lateralNav = new List<LateralNavItem>();

        using (SqlConnection connection = new SqlConnection(strConnessione))
        {
            connection.Open();
            SqlCommand command = connection.CreateCommand();
            command.CommandText = sqlQuery;
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                lateralNav.Add(new LateralNavItem()
                {
                    id = Convert.ToInt32(reader["ID1Pagina"].ToString()),
                    Title = reader["TitoloPagina"].ToString(),
                    Link = string.Concat("Content.aspx?XRI=", reader["ID1Pagina"].ToString())
                });
            }
            reader.Close();
            connection.Close();
        }


        return lateralNav;
    }


    private void BindCurrentConsiglioDirettivo(int XRI)
    {
        string sqlQuery = string.Empty;

        using (SqlConnection connection = new SqlConnection(strConnessione))
        {
            connection.Open();


            SqlCommand command = connection.CreateCommand();
            command.CommandText = sqlQuery;

            sqlQuery = @"SELECT TitoloPagina, TitoloBrowser, Contenuto1, Contenuto2, TagDescription, TagKeywords
                        FROM tbPageDesigner 
                        LEFT OUTER JOIN tbPZ_ImageRaider on ZeusIdModuloIndice = ZeusIdModulo + '1'
                        WHERE  ID1Pagina = @ID1Pagina 
                        AND ZeusIdModulo = 'PDCOD'
                        AND tbPageDesigner.ZeusLangCode = @ZeusLangCode ";




            // ------------------- Check per Anteprima su pagine non pubblicate, controllo se la pagina chiamante è Zeus ----
            if (!AresUtilities.CheckPreviousPageContains("PageDesigner/Content_Lst.aspx"))
                sqlQuery += " AND ZeusIsAlive = 1 AND PW_Area1 = 1 AND(GETDATE() BETWEEN PWI_Area1 AND PWF_Area1) ";
            // ------------------- Fine Check per Anteprima --------------------------------------------

            command.CommandText = sqlQuery;
            command.Parameters.Add("@ID1Pagina", System.Data.SqlDbType.Int);
            command.Parameters["@ID1Pagina"].Value = XRI;
            command.Parameters.Add("@ZeusLangCode", System.Data.SqlDbType.NVarChar);
            command.Parameters["@ZeusLangCode"].Value = "ITA";

            SqlDataReader reader = command.ExecuteReader();

            if (reader.Read())
            {
                title = reader["TitoloPagina"].ToString();
                contenuto1 = reader["Contenuto1"].ToString();
                contenuto2 = reader["Contenuto2"].ToString();




                /*****************************************************/
                /**************** INIZIO SEO *************************/
                /*****************************************************/
                HtmlMeta description = new HtmlMeta();
                HtmlMeta keywords = new HtmlMeta();
                description.Name = "description";
                keywords.Name = "keywords";
                Page.Title = reader["TitoloBrowser"].ToString() + " | " + AresConfig.NomeSito;
                description.Content = reader["TagDescription"].ToString();
                keywords.Content = reader["TagKeywords"].ToString();
                HtmlHead head = Page.Header;
                head.Controls.Add(description);
                head.Controls.Add(keywords);
                /*****************************************************/
                /**************** FINE SEO ***************************/
                /*****************************************************/

                reader.Close();
                connection.Close();
            }
            else
            {
                reader.Close();
                connection.Close();
                Response.Redirect("~/System/Message.aspx?0");
            }
        }
    }


    public class LateralNavItem
    {
        public int id { get; set; }
        public string Title { get; set; }
        public string Link { get; set; }
    }

}