﻿<%@ Page Language="C#" MasterPageFile="~/Master/LayoutMain.master" CodeFile="Default.aspx.cs" Inherits="Web_Content" Async="true" %>


<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AreaTitle" runat="Server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="AreaContentMain" runat="Server">
    <section class="consiglioDirettivo">
        <div class="container">

            <div class="row">
                <div class="col-lg-8 text-std">
                    <div class="boxTitolo">
                        <h2>Consiglio Direttivo 
                            
                            <asp:PlaceHolder runat="server" ID="DataArchivioPlaceholder" Visible="false">
                                <span class="titoloArchivio"><%= title %></span>
                            </asp:PlaceHolder>
                        </h2>
                        <hr />
                    </div>

                    <div class="boxContenuto">
                        <div class="richText">
                            <%= contenuto1 %>
                        </div>

                        <div class="richText">
                            <%= contenuto2 %>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="archivio">
                        <h2>Archivio</h2>
                        <hr />

                        <asp:Repeater ID="rptNavRight" runat="server">
                            <ItemTemplate>
                                <a href='<%#Eval("Link") %>'><%#Eval("Title") %> <i class="fas fa-chevron-right"></i></a>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

