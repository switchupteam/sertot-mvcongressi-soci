﻿<%@ Page Language="C#" MasterPageFile="~/Master/LayoutMain.master" CodeFile="Content.aspx.cs" Inherits="Web_Content" Async="true" %>


<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="Server">
    <style>
        body main {
            min-height: 80rem;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AreaTitle" runat="Server">
    Consiglio Direttivo <%=title %>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="AreaContentMain" runat="Server">
    <section class="consiglioDirettivo">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                   <%= contenuto1 %>
                </div>
                <div class="col-md-4">
                   <%= contenuto1 %>
                </div>
                <div class="col-md-3">
                    <div style="text-align: center; background: url('/asset/img/grey_bar_horizzontal.jpg') left top repeat-y, url('/asset/img/grey_bar_horizzontal.jpg') right top repeat-y; padding: 2rem 0; background-color: #730F11; ">
                        <asp:Repeater ID="rptNavRight" runat="server">
                            <ItemTemplate>
                                  <a href='<%#Eval("Link") %>' style="color: #fff; display: inline-block;"><%#Eval("Title") %></a><br />
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

